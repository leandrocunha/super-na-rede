-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.1.41-3ubuntu12.10


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema supernarede
--

CREATE DATABASE IF NOT EXISTS supernarede;
USE supernarede;

--
-- Definition of table `supernarede`.`admin_assert`
--

DROP TABLE IF EXISTS `supernarede`.`admin_assert`;
CREATE TABLE  `supernarede`.`admin_assert` (
  `assert_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `assert_type` varchar(20) NOT NULL DEFAULT '',
  `assert_data` text,
  PRIMARY KEY (`assert_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ACL Asserts';

--
-- Dumping data for table `supernarede`.`admin_assert`
--

/*!40000 ALTER TABLE `admin_assert` DISABLE KEYS */;
LOCK TABLES `admin_assert` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `admin_assert` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`admin_role`
--

DROP TABLE IF EXISTS `supernarede`.`admin_role`;
CREATE TABLE  `supernarede`.`admin_role` (
  `role_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `tree_level` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `sort_order` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `role_type` char(1) NOT NULL DEFAULT '0',
  `user_id` int(11) unsigned NOT NULL DEFAULT '0',
  `role_name` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`role_id`),
  KEY `parent_id` (`parent_id`,`sort_order`),
  KEY `tree_level` (`tree_level`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='ACL Roles';

--
-- Dumping data for table `supernarede`.`admin_role`
--

/*!40000 ALTER TABLE `admin_role` DISABLE KEYS */;
LOCK TABLES `admin_role` WRITE;
INSERT INTO `supernarede`.`admin_role` VALUES  (1,0,1,1,'G',0,'Administrators'),
 (3,1,2,0,'U',1,'suporte');
UNLOCK TABLES;
/*!40000 ALTER TABLE `admin_role` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`admin_rule`
--

DROP TABLE IF EXISTS `supernarede`.`admin_rule`;
CREATE TABLE  `supernarede`.`admin_rule` (
  `rule_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(10) unsigned NOT NULL DEFAULT '0',
  `resource_id` varchar(255) NOT NULL DEFAULT '',
  `privileges` varchar(20) NOT NULL DEFAULT '',
  `assert_id` int(10) unsigned NOT NULL DEFAULT '0',
  `role_type` char(1) DEFAULT NULL,
  `permission` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`rule_id`),
  KEY `resource` (`resource_id`,`role_id`),
  KEY `role_id` (`role_id`,`resource_id`),
  CONSTRAINT `FK_admin_rule` FOREIGN KEY (`role_id`) REFERENCES `admin_role` (`role_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='ACL Rules';

--
-- Dumping data for table `supernarede`.`admin_rule`
--

/*!40000 ALTER TABLE `admin_rule` DISABLE KEYS */;
LOCK TABLES `admin_rule` WRITE;
INSERT INTO `supernarede`.`admin_rule` VALUES  (1,1,'all','',0,'G','allow');
UNLOCK TABLES;
/*!40000 ALTER TABLE `admin_rule` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`admin_user`
--

DROP TABLE IF EXISTS `supernarede`.`admin_user`;
CREATE TABLE  `supernarede`.`admin_user` (
  `user_id` mediumint(9) unsigned NOT NULL AUTO_INCREMENT,
  `firstname` varchar(32) NOT NULL DEFAULT '',
  `lastname` varchar(32) NOT NULL DEFAULT '',
  `email` varchar(128) NOT NULL DEFAULT '',
  `username` varchar(40) NOT NULL DEFAULT '',
  `password` varchar(40) NOT NULL DEFAULT '',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified` datetime DEFAULT NULL,
  `logdate` datetime DEFAULT NULL,
  `lognum` smallint(5) unsigned NOT NULL DEFAULT '0',
  `reload_acl_flag` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `extra` text,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `UNQ_ADMIN_USER_USERNAME` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Users';

--
-- Dumping data for table `supernarede`.`admin_user`
--

/*!40000 ALTER TABLE `admin_user` DISABLE KEYS */;
LOCK TABLES `admin_user` WRITE;
INSERT INTO `supernarede`.`admin_user` VALUES  (1,'suporte','jn2','suporte@jn2.com.br','root','41c8bf2ba8152277fbf4306fa98c75b8:gK','2011-08-08 12:58:32','2011-08-08 12:58:32','2011-09-22 14:23:47',33,0,1,'a:1:{s:11:\"configState\";a:58:{s:15:\"general_country\";s:1:\"0\";s:14:\"general_locale\";s:1:\"0\";s:25:\"general_store_information\";s:1:\"1\";s:14:\"design_package\";s:1:\"0\";s:12:\"design_theme\";s:1:\"1\";s:11:\"design_head\";s:1:\"0\";s:13:\"design_header\";s:1:\"0\";s:13:\"design_footer\";s:1:\"0\";s:16:\"design_watermark\";s:1:\"0\";s:17:\"design_pagination\";s:1:\"0\";s:7:\"web_url\";s:1:\"0\";s:7:\"web_seo\";s:1:\"1\";s:12:\"web_unsecure\";s:1:\"0\";s:10:\"web_secure\";s:1:\"0\";s:11:\"web_default\";s:1:\"0\";s:9:\"web_polls\";s:1:\"0\";s:10:\"web_cookie\";s:1:\"0\";s:11:\"web_session\";s:1:\"0\";s:24:\"web_browser_capabilities\";s:1:\"0\";s:17:\"contacts_contacts\";s:1:\"0\";s:14:\"contacts_email\";s:1:\"1\";s:16:\"catalog_frontend\";s:1:\"1\";s:15:\"catalog_sitemap\";s:1:\"1\";s:14:\"catalog_review\";s:1:\"0\";s:20:\"catalog_productalert\";s:1:\"0\";s:25:\"catalog_productalert_cron\";s:1:\"0\";s:19:\"catalog_placeholder\";s:1:\"1\";s:25:\"catalog_recently_products\";s:1:\"0\";s:13:\"catalog_price\";s:1:\"1\";s:26:\"catalog_layered_navigation\";s:1:\"1\";s:18:\"catalog_navigation\";s:1:\"1\";s:11:\"catalog_seo\";s:1:\"1\";s:14:\"catalog_search\";s:1:\"1\";s:20:\"catalog_downloadable\";s:1:\"0\";s:22:\"catalog_custom_options\";s:1:\"1\";s:15:\"shipping_origin\";s:1:\"1\";s:15:\"shipping_option\";s:1:\"1\";s:16:\"checkout_options\";s:1:\"1\";s:13:\"checkout_cart\";s:1:\"1\";s:18:\"checkout_cart_link\";s:1:\"1\";s:16:\"checkout_sidebar\";s:1:\"1\";s:23:\"checkout_payment_failed\";s:1:\"0\";s:17:\"ibanners_carousel\";s:1:\"1\";s:23:\"j2tajaxcheckout_default\";s:1:\"1\";s:17:\"carriers_flatrate\";s:1:\"1\";s:18:\"carriers_tablerate\";s:1:\"0\";s:21:\"carriers_freeshipping\";s:1:\"1\";s:12:\"carriers_ups\";s:1:\"0\";s:13:\"carriers_usps\";s:1:\"0\";s:14:\"carriers_fedex\";s:1:\"0\";s:12:\"carriers_dhl\";s:1:\"0\";s:22:\"customer_account_share\";s:1:\"1\";s:25:\"customer_online_customers\";s:1:\"1\";s:23:\"customer_create_account\";s:1:\"1\";s:17:\"customer_password\";s:1:\"1\";s:16:\"customer_address\";s:1:\"1\";s:16:\"customer_startup\";s:1:\"1\";s:26:\"customer_address_templates\";s:1:\"0\";}}');
UNLOCK TABLES;
/*!40000 ALTER TABLE `admin_user` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`adminnotification_inbox`
--

DROP TABLE IF EXISTS `supernarede`.`adminnotification_inbox`;
CREATE TABLE  `supernarede`.`adminnotification_inbox` (
  `notification_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `severity` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text,
  `url` varchar(255) NOT NULL,
  `is_read` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_remove` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`notification_id`),
  KEY `IDX_SEVERITY` (`severity`),
  KEY `IDX_IS_READ` (`is_read`),
  KEY `IDX_IS_REMOVE` (`is_remove`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`adminnotification_inbox`
--

/*!40000 ALTER TABLE `adminnotification_inbox` DISABLE KEYS */;
LOCK TABLES `adminnotification_inbox` WRITE;
INSERT INTO `supernarede`.`adminnotification_inbox` VALUES  (1,4,'2008-07-25 01:24:40','Magento 1.1 Production Version Now Available','We are thrilled to announce the availability of the production release of Magento 1.1. Read more about the release in the Magento Blog.','http://www.magentocommerce.com/blog/comments/magento-11-is-here-1/',0,1),
 (2,4,'2008-08-02 01:30:16','Updated iPhone Theme is now available','Updated iPhone theme for Magento 1.1 is now available on Magento Connect and for upgrade through your Magento Connect Manager.','http://www.magentocommerce.com/blog/comments/updated-iphone-theme-for-magento-11-is-now-available/',0,1),
 (3,3,'2008-08-02 01:40:27','Magento version 1.1.2 is now available','Magento version 1.1.2 is now available for download and upgrade.','http://www.magentocommerce.com/blog/comments/magento-version-112-is-now-available/',0,1),
 (4,3,'2008-08-13 17:51:46','Magento version 1.1.3 is now available','Magento version 1.1.3 is now available','http://www.magentocommerce.com/blog/comments/magento-version-113-is-now-available/',0,1),
 (5,1,'2008-09-02 21:10:31','Magento Version 1.1.4 Security Update Now Available','Magento 1.1.4 Security Update Now Available. If you are using Magento version 1.1.x, we highly recommend upgrading to this version as soon as possible.','http://www.magentocommerce.com/blog/comments/magento-version-114-security-update/',0,1),
 (6,3,'2008-09-15 22:09:54','Magento version 1.1.5 Now Available','Magento version 1.1.5 Now Available.\n\nThis release includes many bug fixes, a new category manager and a new skin for the default Magento theme.','http://www.magentocommerce.com/blog/comments/magento-version-115-now-available/',0,1),
 (7,3,'2008-09-17 20:18:35','Magento version 1.1.6 Now Available','Magento version 1.1.6 Now Available.\n\nThis version includes bug fixes for Magento 1.1.x that are listed in the release notes section.','http://www.magentocommerce.com/blog/comments/magento-version-116-now-available/',0,1),
 (8,4,'2008-11-07 23:46:42','Reminder: Change Magento`s default phone numbers and callouts before site launch','Before launching your Magento store, please remember to change Magento`s default phone numbers that appear in email templates, callouts, templates, etc.','',0,1),
 (9,3,'2008-11-20 01:31:12','Magento version 1.1.7 Now Available','Magento version 1.1.7 Now Available.\n\nThis version includes over 350 issue resolutions for Magento 1.1.x that are listed in the release notes section, and new functionality that includes:\n\n-Google Website Optimizer integration\n-Google Base integration\n-Scheduled DB logs cleaning option','http://www.magentocommerce.com/blog/comments/magento-version-117-now-available/',0,1),
 (10,3,'2008-11-26 21:24:50','Magento Version 1.1.8 Now Available','Magento version 1.1.8 now available.\n\nThis version includes some issue resolutions for Magento 1.1.x that are listed in the release notes section.','http://www.magentocommerce.com/blog/comments/magento-version-118-now-available/',0,1),
 (11,3,'2008-12-30 07:45:59','Magento version 1.2.0 is now available for download and upgrade','We are extremely happy to announce the availability of Magento version 1.2.0 for download and upgrade.\n\nThis version includes numerous issue resolutions for Magento version 1.1.x and some highly requested new features such as:\n\n    * Support for Downloadable/Digital Products. \n    * Added Layered Navigation to site search result page.\n    * Improved site search to utilize MySQL fulltext search\n    * Added support for fixed-taxes on product level.\n    * Upgraded Zend Framework to the latest stable version 1.7.2','http://www.magentocommerce.com/blog/comments/magento-version-120-is-now-available/',0,1),
 (12,2,'2008-12-30 21:59:22','Magento version 1.2.0.1 now available','Magento version 1.2.0.1 now available.This version includes some issue resolutions for Magento 1.2.x that are listed in the release notes section.','http://www.magentocommerce.com/blog/comments/magento-version-1201-available/',0,1),
 (13,2,'2009-01-12 20:41:49','Magento version 1.2.0.2 now available','Magento version 1.2.0.2 is now available for download and upgrade. This version includes an issue resolutions for Magento version 1.2.0.x as listed in the release notes.','http://www.magentocommerce.com/blog/comments/magento-version-1202-now-available/',0,1),
 (14,3,'2009-01-24 00:25:56','Magento version 1.2.0.3 now available','Magento version 1.2.0.3 is now available for download and upgrade. This version includes issue resolutions for Magento version 1.2.0.x as listed in the release notes.','http://www.magentocommerce.com/blog/comments/magento-version-1203-now-available/',0,1),
 (15,3,'2009-02-02 21:57:00','Magento version 1.2.1 is now available for download and upgrade','We are happy to announce the availability of Magento version 1.2.1 for download and upgrade.\n\nThis version includes some issue resolutions for Magento version 1.2.x. A full list of items included in this release can be found on the release notes page.','http://www.magentocommerce.com/blog/comments/magento-version-121-now-available/',0,1),
 (16,3,'2009-02-24 00:45:47','Magento version 1.2.1.1 now available','Magento version 1.2.1.1 now available.This version includes some issue resolutions for Magento 1.2.x that are listed in the release notes section.','http://www.magentocommerce.com/blog/comments/magento-version-1211-now-available/',0,1),
 (17,3,'2009-02-27 01:39:24','CSRF Attack Prevention','We have just posted a blog entry about a hypothetical CSRF attack on a Magento admin panel. Please read the post to find out if your Magento installation is at risk at http://www.magentocommerce.com/blog/comments/csrf-vulnerabilities-in-web-application-and-how-to-avoid-them-in-magento/','http://www.magentocommerce.com/blog/comments/csrf-vulnerabilities-in-web-application-and-how-to-avoid-them-in-magento/',0,1),
 (18,2,'2009-03-03 23:03:58','Magento version 1.2.1.2 now available','Magento version 1.2.1.2 is now available for download and upgrade.\nThis version includes some updates to improve admin security as described in the release notes page.','http://www.magentocommerce.com/blog/comments/magento-version-1212-now-available/',0,1),
 (19,3,'2009-03-31 02:22:40','Magento version 1.3.0 now available','Magento version 1.3.0 is now available for download and upgrade. This version includes numerous issue resolutions for Magento version 1.2.x and new features as described on the release notes page.','http://www.magentocommerce.com/blog/comments/magento-version-130-is-now-available/',0,1),
 (20,3,'2009-04-18 04:06:02','Magento version 1.3.1 now available','Magento version 1.3.1 is now available for download and upgrade. This version includes some issue resolutions for Magento version 1.3.x and new features such as Checkout By Amazon and Amazon Flexible Payment. To see a full list of updates please check the release notes page.','http://www.magentocommerce.com/blog/comments/magento-version-131-now-available/',0,1),
 (21,3,'2009-05-19 22:31:21','Magento version 1.3.1.1 now available','Magento version 1.3.1.1 is now available for download and upgrade. This version includes some issue resolutions for Magento version 1.3.x and a security update for Magento installations that run on multiple domains or sub-domains. If you are running Magento with multiple domains or sub-domains we highly recommend upgrading to this version.','http://www.magentocommerce.com/blog/comments/magento-version-1311-now-available/',0,1),
 (22,3,'2009-05-29 22:54:06','Magento version 1.3.2 now available','This version includes some improvements and issue resolutions for version 1.3.x that are listed on the release notes page. also included is a Beta version of the Compile module.','http://www.magentocommerce.com/blog/comments/magento-version-132-now-available/',0,1),
 (23,3,'2009-06-01 19:32:52','Magento version 1.3.2.1 now available','Magento version 1.3.2.1 now available for download and upgrade.\n\nThis release solves an issue for users running Magento with PHP 5.2.0, and changes to index.php to support the new Compiler Module.','http://www.magentocommerce.com/blog/comments/magento-version-1321-now-available/',0,1),
 (24,3,'2009-07-02 01:21:44','Magento version 1.3.2.2 now available','Magento version 1.3.2.2 is now available for download and upgrade.\n\nThis release includes issue resolution for Magento version 1.3.x. To see a full list of changes please visit the release notes page http://www.magentocommerce.com/download/release_notes.','http://www.magentocommerce.com/blog/comments/magento-version-1322-now-available/',0,1),
 (25,3,'2009-07-23 06:48:54','Magento version 1.3.2.3 now available','Magento version 1.3.2.3 is now available for download and upgrade.\n\nThis release includes issue resolution for Magento version 1.3.x. We recommend to upgrade to this version if PayPal payment modules are in use. To see a full list of changes please visit the release notes page http://www.magentocommerce.com/download/release_notes.','http://www.magentocommerce.com/blog/comments/magento-version-1323-now-available/',0,1),
 (26,4,'2009-08-28 18:26:28','PayPal is updating Payflow Pro and Website Payments Pro (Payflow Edition) UK.','If you are using Payflow Pro and/or Website Payments Pro (Payflow Edition) UK.  payment methods, you will need to update the URL‘s in your Magento Administrator Panel in order to process transactions after September 1, 2009. Full details are available here: http://www.magentocommerce.com/wiki/paypal_payflow_changes','http://www.magentocommerce.com/wiki/paypal_payflow_changes',0,1),
 (27,2,'2009-09-23 20:16:49','Magento Version 1.3.2.4 Security Update','Magento Version 1.3.2.4 is now available. This version includes a security updates for Magento 1.3.x that solves possible XSS vulnerability issue on customer registration page and is available through SVN, Download Page and through the Magento Connect Manager.','http://www.magentocommerce.com/blog/comments/magento-version-1324-security-update/',0,1),
 (28,4,'2009-09-25 14:57:54','Magento Preview Version 1.4.0.0-alpha2 is now available','We are happy to announce the availability of Magento Preview Version 1.4.0.0-alpha2 for download.\nAs this is a preview version it is NOT recommended in any way to be used in a production environment.','http://www.magentocommerce.com/blog/comments/magento-preview-version-1400-alpha2-now-available/',0,1),
 (29,4,'2009-10-07 00:55:40','Magento Preview Version 1.4.0.0-alpha3 is now available','We are happy to announce the availability of Magento Preview Version 1.4.0.0-alpha3 for download.\nAs this is a preview version it is NOT recommended in any way to be used in a production environment.','http://www.magentocommerce.com/blog/comments/magento-preview-version-1400-alpha3-now-available/',0,1),
 (30,4,'2009-12-08 23:30:36','Magento Preview Version 1.4.0.0-beta1 is now available','We are happy to announce the availability of Magento Preview Version 1.4.0.0-beta1 for download.\nAs this is a preview version it is NOT recommended in any way to be used in a production environment.','http://www.magentocommerce.com/blog/comments/magento-preview-version-1400-beta1-now-available/',0,1),
 (31,4,'2009-12-31 09:22:12','Magento Preview Version 1.4.0.0-rc1 is now available','We are happy to announce the availability of Magento Preview Version 1.4.0.0-rc1 for download.\nAs this is a preview version it is NOT recommended in any way to be used in a production environment.','http://www.magentocommerce.com/blog/comments/magento-preview-version-1400-rc1-now-available/',0,1),
 (32,4,'2010-02-13 03:39:53','Magento CE Version 1.4.0.0 Stable is now available','We are excited to announce the availability of Magento CE Version 1.4.0.0 Stable for upgrade and download.','http://bit.ly/c53rpK',0,1),
 (33,3,'2010-02-20 02:39:36','Magento CE Version 1.4.0.1 Stable is now available','Magento CE 1.4.0.1 Stable is now available for upgrade and download.','http://www.magentocommerce.com/blog/comments/magento-ce-version-1401-stable-now-available/',0,1),
 (34,4,'2010-04-23 20:09:03','Magento Version CE 1.3.3.0 Stable - Now Available With Support for 3-D Secure','Based on community requests, we are excited to announce the release of Magento CE 1.3.3.0-Stable with support for 3-D Secure. This release is intended for Magento merchants using version 1.3.x, who want to add support for 3-D Secure.','http://www.magentocommerce.com/blog/comments/magento-version-ce-1330-stable-now-available-with-support-for-3-d-secure/',0,1),
 (35,4,'2010-05-31 17:20:21','Announcing the Launch of Magento Mobile','The Magento team is pleased to announce the launch of Magento mobile, a new product that will allow Magento merchants to easily create branded, native mobile storefront applications that are deeply integrated with Magento’s market-leading eCommerce platform. The product includes a new administrative manager, a native iPhone app that is fully customizable, and a service where Magento manages the submission and maintenance process for the iTunes App Store.\n\nLearn more by visiting the Magento mobile product page and sign-up to be the first to launch a native mobile commerce app, fully integrated with Magento.','http://www.magentocommerce.com/product/mobile',0,1),
 (36,4,'2010-06-10 20:08:08','Magento CE Version 1.4.1.0 Stable is now available','We are excited to announce the availability of Magento CE Version 1.4.1.0 Stable for upgrade and download. Some of the highlights of this release include: Enhanced PayPal integration (more info to follow), Change of Database structure of the Sales module to no longer use EAV, and much more.','http://www.magentocommerce.com/blog/comments/magento-ce-version-1410-stable-now-available/',0,1),
 (37,4,'2010-07-26 21:37:34','Magento CE Version 1.4.1.1 Stable is now available','We are excited to announce the availability of Magento CE Version 1.4.1.1 Stable for download and upgrade.','http://www.magentocommerce.com/blog/comments/magento-ce-version-1411-stable-now-available/',0,1),
 (38,4,'2010-07-28 05:12:12','Magento CE Version 1.4.2.0-beta1 Preview Release Now Available','This release gives a preview of the new Magento Connect Manager.','http://www.magentocommerce.com/blog/comments/magento-preview-version-1420-beta1-now-available/',0,1),
 (39,4,'2010-07-28 20:15:01','Magento CE Version 1.4.1.1 Patch Available','As some users experienced issues with upgrading to CE 1.4.1.1 through PEAR channels we provided a patch for it that is available on our blog http://www.magentocommerce.com/blog/comments/magento-ce-version-1411-stable-patch/','http://www.magentocommerce.com/blog/comments/magento-ce-version-1411-stable-patch/',0,1),
 (40,4,'2010-11-08 21:52:06','Magento CE Version 1.4.2.0-RC1 Preview Release Now Available','We are happy to announce the availability of Magento Preview Version 1.4.2.0-RC1 for download.','http://www.magentocommerce.com/blog/comments/magento-preview-version-1420-rc1-now-available/',0,1),
 (41,4,'2010-12-02 20:33:00','Magento CE Version 1.4.2.0-RC2 Preview Release Now Available','We are happy to announce the availability of Magento Preview Version 1.4.2.0-RC2 for download.','http://www.magentocommerce.com/blog/comments/magento-preview-version-1420-rc2-now-available/',0,1),
 (42,4,'2010-12-08 22:29:55','Magento CE Version 1.4.2.0 Stable is now available','We are excited to announce the availability of Magento CE Version 1.4.2.0 Stable for download and upgrade.','http://www.magentocommerce.com/blog/comments/magento-ce-version-1420-stable-now-available/',0,1),
 (43,4,'2010-12-17 23:23:55','Magento Preview Version CE 1.5.0.0-alpha1 is now available','We are happy to announce the availability of Magento Preview Version CE 1.5.0.0-alpha1 for download.\nAs this is a preview version it is NOT recommended in any way to be used in a production environment.','http://www.magentocommerce.com/blog/comments/magento-preview-version-ce-1500-alpha1-now-available/',0,1),
 (44,4,'2010-12-29 23:51:08','Magento Preview Version CE 1.5.0.0-alpha2 is now available','We are happy to announce the availability of Magento Preview Version CE 1.5.0.0-alpha2 for download.\nAs this is a preview version it is NOT recommended in any way to be used in a production environment.','http://www.magentocommerce.com/blog/comments/magento-preview-version-ce-1500-alpha2-now-available/',0,1),
 (45,4,'2011-01-14 00:35:36','Magento Preview Version CE 1.5.0.0-beta1 is now available','We are happy to announce the availability of Magento Preview Version CE 1.5.0.0-beta1 for download.\nAs this is a preview version it is NOT recommended in any way to be used in a production environment.','http://www.magentocommerce.com/blog/comments/magento-preview-version-ce-1500-beta1-now-available/',0,1),
 (46,4,'2011-01-21 21:19:09','Magento Preview Version CE 1.5.0.0-beta2 is now available','We are happy to announce the availability of Magento Preview Version CE 1.5.0.0-beta2 for download.\nAs this is a preview version it is NOT recommended in any way to be used in a production environment.','http://www.magentocommerce.com/blog/comments/magento-preview-version-ce-1500-beta2-now-available/',0,1),
 (47,4,'2011-01-27 21:27:57','Magento Preview Version CE 1.5.0.0-rc1 is now available','We are happy to announce the availability of Magento Preview Version CE 1.5.0.0-rc1 for download.\nAs this is a preview version it is NOT recommended in any way to be used in a production environment.','http://www.magentocommerce.com/blog/comments/magento-preview-version-ce-1500-rc1-now-available/',0,1),
 (48,4,'2011-02-03 21:56:33','Magento Preview Version CE 1.5.0.0-rc2 is now available','We are happy to announce the availability of Magento Preview Version CE 1.5.0.0-rc2 for download.\nAs this is a preview version it is NOT recommended in any way to be used in a production environment.','http://www.magentocommerce.com/blog/comments/magento-preview-version-ce-1500-rc2-now-available/',0,1),
 (49,4,'2011-02-08 19:43:23','Magento CE Version 1.5.0.0 Stable is now available','We are excited to announce the availability of Magento CE Version 1.5.0.0 Stable for download and upgrade.','http://www.magentocommerce.com/blog/comments/magento-community-professional-and-enterprise-editions-releases-now-availab/',0,1),
 (50,4,'2011-02-09 23:42:57','Magento CE 1.5.0.1 stable Now Available','We are excited to announce the availability of Magento CE Version 1.5.0.1 Stable for download and upgrade.','http://www.magentocommerce.com/blog/comments/magento-ce-1501-stable-now-available/',0,1),
 (51,4,'2011-03-18 20:15:45','Magento CE 1.5.1.0-beta1 Now Available','We are happy to announce the availability of Magento Preview Version CE 1.5.1.0-beta1 for download and upgrade.','http://www.magentocommerce.com/blog/comments/magento-preview-version-ce-1510-beta1-now-available/',0,1),
 (52,4,'2011-03-31 18:43:02','Magento CE 1.5.1.0-rc1 Now Available','We are happy to announce the availability of Magento Preview Version CE 1.5.1.0-rc1 for download and upgrade.','http://www.magentocommerce.com/blog/comments/magento-preview-version-ce-1510-rc1-now-available/',0,1),
 (53,4,'2011-04-26 19:21:07','Magento CE 1.5.1.0-stable Now Available','We are excited to announce the availability of Magento CE Version 1.5.1.0 Stable for download and upgrade.','http://www.magentocommerce.com/blog/comments/magento-ce-version-1510-stable-now-available/',0,1),
 (54,4,'2011-05-26 19:33:23','Magento Preview Version CE 1.6.0.0-alpha1 is now available','We are happy to announce the availability of Magento Preview Version CE 1.6.0.0-alpha1 for download.\nAs this is a preview version it is NOT recommended in any way to be used in a production environment.','http://www.magentocommerce.com/blog/comments/magento-preview-version-ce-1600-alpha1-now-available/',0,1),
 (55,4,'2011-06-15 18:12:08','Magento Preview Version CE 1.6.0.0-beta1 is now available','We are happy to announce the availability of Magento Preview Version CE 1.6.0.0-beta1for download.\nAs this is a preview version it is NOT recommended in any way to be used in a production environment.','http://www.magentocommerce.com/blog/comments/magento-preview-version-ce-1600-beta1-now-available/',0,1),
 (56,4,'2011-06-30 19:03:58','Magento Preview Version CE 1.6.0.0-rc1 is now available','We are happy to announce the availability of Magento Preview Version CE 1.6.0.0-rc1 for download.\nAs this is a preview version it is NOT recommended in any way to be used in a production environment.','http://www.magentocommerce.com/blog/comments/magento-preview-version-ce-1600-rc1-now-available/',0,1),
 (57,4,'2011-07-11 19:07:39','Magento Preview Version CE 1.6.0.0-rc2 is now available','We are happy to announce the availability of Magento Preview Version CE 1.6.0.0-rc2 for download.\nAs this is a preview version it is NOT recommended in any way to be used in a production environment.','http://www.magentocommerce.com/blog/comments/magento-preview-version-ce-1600-rc2-now-available/',0,1),
 (58,4,'2011-08-19 20:45:15','Amasty\'s extension has been installed. Check the Admin > Configuration > Amasty section.','You can see versions of the installed extensions right in the admin, as well as configure notifications about major updates.','http://amasty.com/news/updates-and-notifications-configuration-9.html',0,1),
 (59,4,'2011-08-19 17:58:31','Magento CE 1.6.0.0-stable Now Available','We are excited to announce the availability of Magento CE Version 1.6.0.0 Stable for download and upgrade.','http://www.magentocommerce.com/blog/comments/magento-ce-version-1600-stable-now-available/',0,1),
 (60,3,'2011-08-23 00:00:00','New fantastic extension - Mass Product Actions!','Assign products to categories (and remove products from categories) in bulk right on the product grid. You can also\nincrease and/or decrease prices and special prices for large numbers of products in just a few clicks (either by flat amount or by percentage). Fast deletion of products.','http://amasty.com/news/new-fantastic-extension-mass-product-actions--68.html',0,1),
 (61,3,'2011-08-24 00:00:00','Another great extension - Duplicate Promotions!','As it says in the title, now you can duplicate promotion rules. But not only... In fact the extension gives you a number of promotion-related actions - you can activate and deactivate multiple shopping cart rules right on the grid, delete rules and change their priority on the grid. Also the extension adds more information columns to the rules grid, which makes it a lot more informative and easier to manage.','http://amasty.com/news/another-great-extension-duplicate-promotions--69.html',0,1),
 (62,3,'2011-09-03 00:00:00','Special Promotions 1.2.0 released!','With new version - new great features. You can make shopping cart price rules skip products with special price, so that items with already lowered prices don\'t get discounted again. Now rules based on the actions added with the extension work for admin orders as well. Also you can display discount breakdown on the front end.','http://amasty.com/news/special-promotions-1-2-0-released--71.html',0,0),
 (63,3,'2011-08-30 00:00:00','Mass Product Actions 1.0.1!','New action \'Change Attribute Set\'. With it you can replace attribute sets of your products (both for individual and for multiple products) at the grid. So choosing a wrong attribute set by mistake doesn\'t mean recreation of the product from scratch anymore - you just change the attribute set to the right one. ','http://amasty.com/news/mass-product-actions-1-0-1--70.html',0,0),
 (64,4,'2010-10-12 04:13:25','Magento Mobile is now live!','Magento Mobile is now live! Signup today to have your own native iPhone mobile-shopping app in iTunes for the holiday season! Learn more at http://www.magentomobile.com/','http://www.magentomobile.com/',0,0),
 (65,4,'2011-01-25 03:10:33','Join us for Magento\'s Imagine eCommerce Conference!','Magento\'s Imagine eCommerce Conference is a must-attend event for anyone who uses the Magento platform or is part of the Magento ecosystem. The conference will bring together over 500 retailers, merchants, developers, partners, eCommerce experts, technologists and marketing pros for a fun and intensive conversation about the future of eCommerce.\n\nThe conference is in Los Angeles and kicks off early Monday evening February 7th through Wednesday, February 9th, 2011.\n\nRegister at http://www.magento.com/imagine. First 20 registrants use discount code IMAGINE3X76 for $300 off. *This discount is sponsored by PayPal and is only valid for new registrations.\n\nHope to see you there!\n\nMagento Team','http://www.magento.com/imagine',0,0),
 (66,4,'2011-09-17 05:31:26','Magento Preview Version CE 1.6.1.0-beta1 is now available','We are happy to announce the availability of Magento Preview Version CE 1.6.1.0-beta1 for download.\nAs this is a preview version it is NOT recommended in any way to be used in a production environment.','http://www.magentocommerce.com/blog/comments/magento-preview-version-ce-1610-beta1-now-available/',0,0);
UNLOCK TABLES;
/*!40000 ALTER TABLE `adminnotification_inbox` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`api_assert`
--

DROP TABLE IF EXISTS `supernarede`.`api_assert`;
CREATE TABLE  `supernarede`.`api_assert` (
  `assert_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `assert_type` varchar(20) NOT NULL DEFAULT '',
  `assert_data` text,
  PRIMARY KEY (`assert_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Api ACL Asserts';

--
-- Dumping data for table `supernarede`.`api_assert`
--

/*!40000 ALTER TABLE `api_assert` DISABLE KEYS */;
LOCK TABLES `api_assert` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `api_assert` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`api_role`
--

DROP TABLE IF EXISTS `supernarede`.`api_role`;
CREATE TABLE  `supernarede`.`api_role` (
  `role_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `tree_level` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `sort_order` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `role_type` char(1) NOT NULL DEFAULT '0',
  `user_id` int(11) unsigned NOT NULL DEFAULT '0',
  `role_name` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`role_id`),
  KEY `parent_id` (`parent_id`,`sort_order`),
  KEY `tree_level` (`tree_level`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Api ACL Roles';

--
-- Dumping data for table `supernarede`.`api_role`
--

/*!40000 ALTER TABLE `api_role` DISABLE KEYS */;
LOCK TABLES `api_role` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `api_role` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`api_rule`
--

DROP TABLE IF EXISTS `supernarede`.`api_rule`;
CREATE TABLE  `supernarede`.`api_rule` (
  `rule_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(10) unsigned NOT NULL DEFAULT '0',
  `resource_id` varchar(255) NOT NULL DEFAULT '',
  `privileges` varchar(20) NOT NULL DEFAULT '',
  `assert_id` int(10) unsigned NOT NULL DEFAULT '0',
  `role_type` char(1) DEFAULT NULL,
  `permission` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`rule_id`),
  KEY `resource` (`resource_id`,`role_id`),
  KEY `role_id` (`role_id`,`resource_id`),
  CONSTRAINT `FK_api_rule` FOREIGN KEY (`role_id`) REFERENCES `api_role` (`role_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Api ACL Rules';

--
-- Dumping data for table `supernarede`.`api_rule`
--

/*!40000 ALTER TABLE `api_rule` DISABLE KEYS */;
LOCK TABLES `api_rule` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `api_rule` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`api_session`
--

DROP TABLE IF EXISTS `supernarede`.`api_session`;
CREATE TABLE  `supernarede`.`api_session` (
  `user_id` mediumint(9) unsigned NOT NULL,
  `logdate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `sessid` varchar(40) NOT NULL DEFAULT '',
  KEY `API_SESSION_USER` (`user_id`),
  KEY `API_SESSION_SESSID` (`sessid`),
  CONSTRAINT `FK_API_SESSION_USER` FOREIGN KEY (`user_id`) REFERENCES `api_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Api Sessions';

--
-- Dumping data for table `supernarede`.`api_session`
--

/*!40000 ALTER TABLE `api_session` DISABLE KEYS */;
LOCK TABLES `api_session` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `api_session` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`api_user`
--

DROP TABLE IF EXISTS `supernarede`.`api_user`;
CREATE TABLE  `supernarede`.`api_user` (
  `user_id` mediumint(9) unsigned NOT NULL AUTO_INCREMENT,
  `firstname` varchar(32) NOT NULL DEFAULT '',
  `lastname` varchar(32) NOT NULL DEFAULT '',
  `email` varchar(128) NOT NULL DEFAULT '',
  `username` varchar(40) NOT NULL DEFAULT '',
  `api_key` varchar(40) NOT NULL DEFAULT '',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified` datetime DEFAULT NULL,
  `lognum` smallint(5) unsigned NOT NULL DEFAULT '0',
  `reload_acl_flag` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Api Users';

--
-- Dumping data for table `supernarede`.`api_user`
--

/*!40000 ALTER TABLE `api_user` DISABLE KEYS */;
LOCK TABLES `api_user` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `api_user` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_category_anc_categs_index_idx`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_category_anc_categs_index_idx`;
CREATE TABLE  `supernarede`.`catalog_category_anc_categs_index_idx` (
  `category_id` int(10) unsigned NOT NULL DEFAULT '0',
  `path` varchar(255) NOT NULL DEFAULT '',
  KEY `IDX_CATEGORY` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalog_category_anc_categs_index_idx`
--

/*!40000 ALTER TABLE `catalog_category_anc_categs_index_idx` DISABLE KEYS */;
LOCK TABLES `catalog_category_anc_categs_index_idx` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_category_anc_categs_index_idx` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_category_anc_categs_index_tmp`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_category_anc_categs_index_tmp`;
CREATE TABLE  `supernarede`.`catalog_category_anc_categs_index_tmp` (
  `category_id` int(10) unsigned NOT NULL DEFAULT '0',
  `path` varchar(255) NOT NULL DEFAULT '',
  KEY `IDX_CATEGORY` (`category_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalog_category_anc_categs_index_tmp`
--

/*!40000 ALTER TABLE `catalog_category_anc_categs_index_tmp` DISABLE KEYS */;
LOCK TABLES `catalog_category_anc_categs_index_tmp` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_category_anc_categs_index_tmp` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_category_anc_products_index_idx`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_category_anc_products_index_idx`;
CREATE TABLE  `supernarede`.`catalog_category_anc_products_index_idx` (
  `category_id` int(10) unsigned NOT NULL DEFAULT '0',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0',
  `position` int(10) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalog_category_anc_products_index_idx`
--

/*!40000 ALTER TABLE `catalog_category_anc_products_index_idx` DISABLE KEYS */;
LOCK TABLES `catalog_category_anc_products_index_idx` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_category_anc_products_index_idx` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_category_anc_products_index_tmp`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_category_anc_products_index_tmp`;
CREATE TABLE  `supernarede`.`catalog_category_anc_products_index_tmp` (
  `category_id` int(10) unsigned NOT NULL DEFAULT '0',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0'
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalog_category_anc_products_index_tmp`
--

/*!40000 ALTER TABLE `catalog_category_anc_products_index_tmp` DISABLE KEYS */;
LOCK TABLES `catalog_category_anc_products_index_tmp` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_category_anc_products_index_tmp` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_category_entity`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_category_entity`;
CREATE TABLE  `supernarede`.`catalog_category_entity` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entity_type_id` smallint(8) unsigned NOT NULL DEFAULT '0',
  `attribute_set_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `path` varchar(255) NOT NULL,
  `position` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  `children_count` int(11) NOT NULL,
  PRIMARY KEY (`entity_id`),
  KEY `IDX_LEVEL` (`level`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COMMENT='Category Entities';

--
-- Dumping data for table `supernarede`.`catalog_category_entity`
--

/*!40000 ALTER TABLE `catalog_category_entity` DISABLE KEYS */;
LOCK TABLES `catalog_category_entity` WRITE;
INSERT INTO `supernarede`.`catalog_category_entity` VALUES  (1,3,0,0,'0000-00-00 00:00:00','2011-08-08 12:55:28','1',0,0,16),
 (2,3,3,1,'2011-08-08 12:55:28','2011-08-17 12:24:12','1/2',1,1,15),
 (3,3,3,2,'2011-08-10 18:49:03','2011-08-11 17:34:20','1/2/3',1,2,8),
 (4,3,3,2,'2011-08-11 17:44:10','2011-08-22 15:47:41','1/2/4',2,2,2),
 (5,3,3,4,'2011-08-11 17:58:24','2011-08-22 15:48:09','1/2/4/5',1,3,0),
 (6,3,3,4,'2011-08-12 17:33:00','2011-08-22 15:48:01','1/2/4/6',2,3,0),
 (7,3,3,3,'2011-08-12 17:37:10','2011-08-12 18:13:23','1/2/3/7',1,3,3),
 (8,3,3,3,'2011-08-12 18:24:06','2011-08-12 18:24:06','1/2/3/8',2,3,2),
 (9,3,3,7,'2011-08-12 18:25:57','2011-08-12 18:25:57','1/2/3/7/9',2,4,0),
 (10,3,3,7,'2011-08-12 18:26:29','2011-08-12 18:26:29','1/2/3/7/10',1,4,0),
 (11,3,3,7,'2011-08-12 18:27:04','2011-08-12 18:27:04','1/2/3/7/11',3,4,0),
 (12,3,3,8,'2011-08-12 18:27:39','2011-08-22 16:39:36','1/2/3/8/12',1,4,0),
 (13,3,3,8,'2011-08-12 18:28:19','2011-08-12 18:28:19','1/2/3/8/13',2,4,0),
 (14,3,3,3,'2011-08-12 21:13:18','2011-08-22 16:38:55','1/2/3/14',3,3,0),
 (15,3,3,2,'2011-08-17 12:23:08','2011-08-17 12:23:08','1/2/15',4,2,0),
 (18,3,3,2,'2011-09-16 13:23:30','2011-09-21 19:58:49','1/2/18',5,2,1),
 (19,3,3,18,'2011-09-16 13:24:30','2011-09-16 13:24:45','1/2/18/19',1,3,0);
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_category_entity` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_category_entity_datetime`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_category_entity_datetime`;
CREATE TABLE  `supernarede`.`catalog_category_entity_datetime` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT,
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0',
  `value` datetime DEFAULT NULL,
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `IDX_BASE` (`entity_type_id`,`entity_id`,`attribute_id`,`store_id`),
  KEY `FK_ATTRIBUTE_DATETIME_ENTITY` (`entity_id`),
  KEY `FK_CATALOG_CATEGORY_ENTITY_DATETIME_ATTRIBUTE` (`attribute_id`),
  KEY `FK_CATALOG_CATEGORY_ENTITY_DATETIME_STORE` (`store_id`),
  CONSTRAINT `FK_CATALOG_CATEGORY_ENTITY_DATETIME_ATTRIBUTE` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATALOG_CATEGORY_ENTITY_DATETIME_ENTITY` FOREIGN KEY (`entity_id`) REFERENCES `catalog_category_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATALOG_CATEGORY_ENTITY_DATETIME_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalog_category_entity_datetime`
--

/*!40000 ALTER TABLE `catalog_category_entity_datetime` DISABLE KEYS */;
LOCK TABLES `catalog_category_entity_datetime` WRITE;
INSERT INTO `supernarede`.`catalog_category_entity_datetime` VALUES  (1,3,52,0,3,NULL),
 (2,3,53,0,3,NULL),
 (9,3,52,0,4,NULL),
 (10,3,53,0,4,NULL),
 (11,3,52,0,5,NULL),
 (12,3,53,0,5,NULL),
 (13,3,52,0,6,NULL),
 (14,3,53,0,6,NULL),
 (15,3,52,0,2,NULL),
 (16,3,53,0,2,NULL),
 (23,3,52,0,18,NULL),
 (24,3,53,0,18,NULL);
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_category_entity_datetime` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_category_entity_decimal`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_category_entity_decimal`;
CREATE TABLE  `supernarede`.`catalog_category_entity_decimal` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT,
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0',
  `value` decimal(12,4) DEFAULT NULL,
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `IDX_BASE` (`entity_type_id`,`entity_id`,`attribute_id`,`store_id`),
  KEY `FK_ATTRIBUTE_DECIMAL_ENTITY` (`entity_id`),
  KEY `FK_CATALOG_CATEGORY_ENTITY_DECIMAL_ATTRIBUTE` (`attribute_id`),
  KEY `FK_CATALOG_CATEGORY_ENTITY_DECIMAL_STORE` (`store_id`),
  CONSTRAINT `FK_CATALOG_CATEGORY_ENTITY_DECIMAL_ATTRIBUTE` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATALOG_CATEGORY_ENTITY_DECIMAL_ENTITY` FOREIGN KEY (`entity_id`) REFERENCES `catalog_category_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATALOG_CATEGORY_ENTITY_DECIMAL_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalog_category_entity_decimal`
--

/*!40000 ALTER TABLE `catalog_category_entity_decimal` DISABLE KEYS */;
LOCK TABLES `catalog_category_entity_decimal` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_category_entity_decimal` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_category_entity_int`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_category_entity_int`;
CREATE TABLE  `supernarede`.`catalog_category_entity_int` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT,
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0',
  `value` int(11) DEFAULT NULL,
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `IDX_BASE` (`entity_type_id`,`entity_id`,`attribute_id`,`store_id`),
  KEY `FK_ATTRIBUTE_INT_ENTITY` (`entity_id`),
  KEY `FK_CATALOG_CATEGORY_EMTITY_INT_ATTRIBUTE` (`attribute_id`),
  KEY `FK_CATALOG_CATEGORY_EMTITY_INT_STORE` (`store_id`),
  CONSTRAINT `FK_CATALOG_CATEGORY_EMTITY_INT_ATTRIBUTE` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATALOG_CATEGORY_EMTITY_INT_ENTITY` FOREIGN KEY (`entity_id`) REFERENCES `catalog_category_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATALOG_CATEGORY_EMTITY_INT_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=121 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalog_category_entity_int`
--

/*!40000 ALTER TABLE `catalog_category_entity_int` DISABLE KEYS */;
LOCK TABLES `catalog_category_entity_int` WRITE;
INSERT INTO `supernarede`.`catalog_category_entity_int` VALUES  (1,3,34,1,2,1),
 (2,3,105,0,1,1),
 (3,3,105,0,2,1),
 (4,3,34,0,3,1),
 (5,3,105,0,3,1),
 (6,3,42,0,3,8),
 (7,3,43,0,3,1),
 (8,3,106,0,3,0),
 (9,3,107,0,3,0),
 (10,3,108,0,3,NULL),
 (11,3,34,0,4,1),
 (12,3,105,0,4,1),
 (13,3,42,0,4,10),
 (14,3,43,0,4,1),
 (15,3,106,0,4,0),
 (16,3,107,0,4,0),
 (17,3,108,0,4,NULL),
 (18,3,34,0,5,1),
 (19,3,105,0,5,1),
 (20,3,42,0,5,NULL),
 (21,3,43,0,5,1),
 (22,3,106,0,5,1),
 (23,3,107,0,5,0),
 (24,3,108,0,5,NULL),
 (25,3,34,0,6,1),
 (26,3,105,0,6,1),
 (27,3,42,0,6,NULL),
 (28,3,43,0,6,1),
 (29,3,106,0,6,1),
 (30,3,107,0,6,0),
 (31,3,108,0,6,NULL),
 (33,3,34,0,7,1),
 (34,3,105,0,7,1),
 (35,3,42,0,7,NULL),
 (36,3,43,0,7,0),
 (37,3,106,0,7,1),
 (38,3,108,0,7,NULL),
 (42,3,34,0,8,1),
 (43,3,105,0,8,1),
 (44,3,42,0,8,NULL),
 (45,3,43,0,8,0),
 (46,3,106,0,8,1),
 (47,3,108,0,8,NULL),
 (48,3,34,0,9,1),
 (49,3,105,0,9,1),
 (50,3,42,0,9,NULL),
 (51,3,43,0,9,1),
 (52,3,106,0,9,1),
 (53,3,108,0,9,NULL),
 (54,3,34,0,10,1),
 (55,3,105,0,10,1),
 (56,3,42,0,10,NULL),
 (57,3,43,0,10,1),
 (58,3,106,0,10,1),
 (59,3,108,0,10,NULL),
 (60,3,34,0,11,1),
 (61,3,105,0,11,1),
 (62,3,42,0,11,NULL),
 (63,3,43,0,11,1),
 (64,3,106,0,11,1),
 (65,3,108,0,11,NULL),
 (66,3,34,0,12,1),
 (67,3,105,0,12,1),
 (68,3,42,0,12,NULL),
 (69,3,43,0,12,1),
 (70,3,106,0,12,1),
 (71,3,108,0,12,NULL),
 (72,3,34,0,13,1),
 (73,3,105,0,13,1),
 (74,3,42,0,13,NULL),
 (75,3,43,0,13,1),
 (76,3,106,0,13,1),
 (77,3,108,0,13,NULL),
 (78,3,34,0,14,1),
 (79,3,105,0,14,1),
 (80,3,42,0,14,NULL),
 (81,3,43,0,14,0),
 (82,3,106,0,14,1),
 (83,3,108,0,14,NULL),
 (84,3,34,0,15,1),
 (85,3,105,0,15,0),
 (86,3,42,0,15,NULL),
 (87,3,43,0,15,0),
 (88,3,106,0,15,1),
 (89,3,108,0,15,NULL),
 (90,3,42,0,2,NULL),
 (91,3,43,0,2,0),
 (92,3,107,0,2,0),
 (93,3,108,0,2,NULL),
 (108,3,34,0,18,1),
 (109,3,105,0,18,1),
 (110,3,42,0,18,12),
 (111,3,43,0,18,1),
 (112,3,106,0,18,0),
 (113,3,107,0,18,1),
 (114,3,108,0,18,NULL),
 (115,3,34,0,19,1),
 (116,3,105,0,19,1),
 (117,3,42,0,19,NULL),
 (118,3,43,0,19,1),
 (119,3,106,0,19,1),
 (120,3,108,0,19,NULL);
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_category_entity_int` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_category_entity_text`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_category_entity_text`;
CREATE TABLE  `supernarede`.`catalog_category_entity_text` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT,
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0',
  `value` text NOT NULL,
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `IDX_BASE` (`entity_type_id`,`entity_id`,`attribute_id`,`store_id`),
  KEY `FK_ATTRIBUTE_TEXT_ENTITY` (`entity_id`),
  KEY `FK_CATALOG_CATEGORY_ENTITY_TEXT_ATTRIBUTE` (`attribute_id`),
  KEY `FK_CATALOG_CATEGORY_ENTITY_TEXT_STORE` (`store_id`),
  CONSTRAINT `FK_CATALOG_CATEGORY_ENTITY_TEXT_ATTRIBUTE` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATALOG_CATEGORY_ENTITY_TEXT_ENTITY` FOREIGN KEY (`entity_id`) REFERENCES `catalog_category_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATALOG_CATEGORY_ENTITY_TEXT_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalog_category_entity_text`
--

/*!40000 ALTER TABLE `catalog_category_entity_text` DISABLE KEYS */;
LOCK TABLES `catalog_category_entity_text` WRITE;
INSERT INTO `supernarede`.`catalog_category_entity_text` VALUES  (1,3,58,1,1,''),
 (2,3,58,1,2,''),
 (3,3,36,0,3,''),
 (4,3,39,0,3,''),
 (5,3,40,0,3,''),
 (6,3,55,0,3,''),
 (7,3,58,0,3,''),
 (8,3,36,0,4,'<p>Descri&ccedil;&atilde;o da categoria de Listas Prontas</p>'),
 (9,3,39,0,4,''),
 (10,3,40,0,4,''),
 (11,3,55,0,4,''),
 (12,3,58,0,4,''),
 (13,3,36,0,5,''),
 (14,3,39,0,5,''),
 (15,3,40,0,5,''),
 (16,3,55,0,5,''),
 (17,3,58,0,5,''),
 (18,3,36,0,6,''),
 (19,3,39,0,6,''),
 (20,3,40,0,6,''),
 (21,3,55,0,6,''),
 (22,3,58,0,6,''),
 (23,3,36,0,7,''),
 (24,3,39,0,7,''),
 (25,3,40,0,7,''),
 (26,3,58,0,7,''),
 (27,3,36,0,8,''),
 (28,3,39,0,8,''),
 (29,3,40,0,8,''),
 (30,3,58,0,8,''),
 (31,3,36,0,9,''),
 (32,3,39,0,9,''),
 (33,3,40,0,9,''),
 (34,3,58,0,9,''),
 (35,3,36,0,10,''),
 (36,3,39,0,10,''),
 (37,3,40,0,10,''),
 (38,3,58,0,10,''),
 (39,3,36,0,11,''),
 (40,3,39,0,11,''),
 (41,3,40,0,11,''),
 (42,3,58,0,11,''),
 (43,3,36,0,12,'<p><strong>Teste</strong></p>'),
 (44,3,39,0,12,''),
 (45,3,40,0,12,''),
 (46,3,58,0,12,''),
 (47,3,36,0,13,''),
 (48,3,39,0,13,''),
 (49,3,40,0,13,''),
 (50,3,58,0,13,''),
 (51,3,36,0,14,'<p><strong>Teste</strong></p>'),
 (52,3,39,0,14,''),
 (53,3,40,0,14,''),
 (54,3,58,0,14,''),
 (55,3,36,0,15,''),
 (56,3,39,0,15,''),
 (57,3,40,0,15,''),
 (58,3,58,0,15,''),
 (59,3,36,0,2,''),
 (60,3,39,0,2,''),
 (61,3,40,0,2,''),
 (62,3,55,0,2,''),
 (72,3,36,0,18,''),
 (73,3,39,0,18,''),
 (74,3,40,0,18,''),
 (75,3,55,0,18,''),
 (76,3,58,0,18,''),
 (77,3,36,0,19,''),
 (78,3,39,0,19,''),
 (79,3,40,0,19,''),
 (80,3,58,0,19,'');
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_category_entity_text` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_category_entity_varchar`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_category_entity_varchar`;
CREATE TABLE  `supernarede`.`catalog_category_entity_varchar` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT,
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0',
  `value` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `IDX_BASE` (`entity_type_id`,`entity_id`,`attribute_id`,`store_id`) USING BTREE,
  KEY `FK_ATTRIBUTE_VARCHAR_ENTITY` (`entity_id`),
  KEY `FK_CATALOG_CATEGORY_ENTITY_VARCHAR_ATTRIBUTE` (`attribute_id`),
  KEY `FK_CATALOG_CATEGORY_ENTITY_VARCHAR_STORE` (`store_id`),
  CONSTRAINT `FK_CATALOG_CATEGORY_ENTITY_VARCHAR_ATTRIBUTE` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATALOG_CATEGORY_ENTITY_VARCHAR_ENTITY` FOREIGN KEY (`entity_id`) REFERENCES `catalog_category_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATALOG_CATEGORY_ENTITY_VARCHAR_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=131 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalog_category_entity_varchar`
--

/*!40000 ALTER TABLE `catalog_category_entity_varchar` DISABLE KEYS */;
LOCK TABLES `catalog_category_entity_varchar` WRITE;
INSERT INTO `supernarede`.`catalog_category_entity_varchar` VALUES  (1,3,33,1,1,'Root Catalog'),
 (2,3,35,0,1,'root-catalog'),
 (4,3,41,1,2,'PRODUCTS'),
 (5,3,35,0,2,'default-category'),
 (6,3,33,0,3,'Departamentos'),
 (7,3,35,0,3,'departamentos'),
 (8,3,38,0,3,''),
 (9,3,41,0,3,'PAGE'),
 (10,3,59,0,3,'position'),
 (11,3,50,0,3,'default/default'),
 (12,3,54,0,3,'one_column'),
 (13,3,49,1,3,'departamentos'),
 (14,3,49,0,3,'departamentos'),
 (18,3,33,0,4,'Listas Prontas'),
 (19,3,35,0,4,'listas-prontas'),
 (20,3,38,0,4,''),
 (21,3,41,0,4,'PAGE'),
 (22,3,50,0,4,'default/default'),
 (23,3,54,0,4,'two_columns_left'),
 (24,3,49,1,4,'listas-prontas'),
 (25,3,49,0,4,'listas-prontas'),
 (26,3,33,0,5,'Compra do mês'),
 (27,3,35,0,5,'compra-do-mes'),
 (28,3,38,0,5,''),
 (29,3,41,0,5,'PRODUCTS'),
 (30,3,50,0,5,''),
 (31,3,54,0,5,'one_column'),
 (32,3,49,1,5,'listas-prontas/compra-do-mes'),
 (33,3,49,0,5,'listas-prontas/compra-do-mes'),
 (34,3,33,0,6,'Somente Bebidas'),
 (35,3,35,0,6,'somente-bebidas'),
 (36,3,38,0,6,''),
 (37,3,41,0,6,'PRODUCTS'),
 (38,3,50,0,6,''),
 (39,3,54,0,6,'one_column'),
 (40,3,49,1,6,'listas-prontas/somente-bebidas'),
 (41,3,49,0,6,'listas-prontas/somente-bebidas'),
 (43,3,33,0,7,'Bebida'),
 (44,3,35,0,7,'bebidas'),
 (45,3,38,0,7,''),
 (46,3,41,0,7,'PRODUCTS'),
 (47,3,49,1,7,'departamentos/bebidas'),
 (48,3,49,0,7,'departamentos/bebidas'),
 (50,3,33,0,8,'Massa'),
 (51,3,35,0,8,'massa'),
 (52,3,38,0,8,''),
 (53,3,41,0,8,'PRODUCTS'),
 (54,3,49,1,8,'departamentos/massa'),
 (55,3,49,0,8,'departamentos/massa'),
 (56,3,33,0,9,'Cerveja'),
 (57,3,35,0,9,'cerveja'),
 (58,3,38,0,9,''),
 (59,3,41,0,9,'PRODUCTS'),
 (60,3,49,1,9,'departamentos/bebidas/cerveja'),
 (61,3,49,0,9,'departamentos/bebidas/cerveja'),
 (62,3,33,0,10,'Água'),
 (63,3,35,0,10,'agua'),
 (64,3,38,0,10,''),
 (65,3,41,0,10,'PRODUCTS'),
 (66,3,49,1,10,'departamentos/bebidas/agua'),
 (67,3,49,0,10,'departamentos/bebidas/agua'),
 (68,3,33,0,11,'Refrigerante'),
 (69,3,35,0,11,'refrigerante'),
 (70,3,38,0,11,''),
 (71,3,41,0,11,'PRODUCTS'),
 (72,3,49,1,11,'departamentos/bebidas/refrigerante'),
 (73,3,49,0,11,'departamentos/bebidas/refrigerante'),
 (74,3,33,0,12,'Espaguete'),
 (75,3,35,0,12,'espaguete'),
 (76,3,38,0,12,''),
 (77,3,41,0,12,'PRODUCTS'),
 (78,3,49,1,12,'departamentos/massa/espaguete'),
 (79,3,49,0,12,'departamentos/massa/espaguete'),
 (80,3,33,0,13,'Instantânea'),
 (81,3,35,0,13,'instantanea'),
 (82,3,38,0,13,''),
 (83,3,41,0,13,'PRODUCTS'),
 (84,3,49,1,13,'departamentos/massa/instantanea'),
 (85,3,49,0,13,'departamentos/massa/instantanea'),
 (86,3,33,0,14,'Padaria'),
 (87,3,35,0,14,'padaria'),
 (88,3,38,0,14,''),
 (89,3,41,0,14,'PRODUCTS'),
 (90,3,49,1,14,'departamentos/padaria'),
 (91,3,49,0,14,'departamentos/padaria'),
 (92,3,33,0,15,'Destaques'),
 (93,3,35,0,15,'destaques'),
 (94,3,38,0,15,''),
 (95,3,41,0,15,'PRODUCTS'),
 (96,3,49,1,15,'destaques'),
 (97,3,49,0,15,'destaques'),
 (98,3,38,0,2,''),
 (99,3,50,0,2,''),
 (100,3,54,0,2,'one_column'),
 (101,3,33,0,2,'Geral'),
 (117,3,33,0,18,'Receitas'),
 (118,3,35,0,18,'receitas'),
 (119,3,38,0,18,''),
 (120,3,41,0,18,'PAGE'),
 (121,3,50,0,18,''),
 (122,3,54,0,18,'two_columns_right'),
 (123,3,49,1,18,'receitas'),
 (124,3,49,0,18,'receitas'),
 (125,3,33,0,19,'Grupo Qualquer'),
 (126,3,35,0,19,'grupo-qualquer'),
 (127,3,38,0,19,''),
 (128,3,41,0,19,'PRODUCTS'),
 (129,3,49,1,19,'receitas/grupo-qualquer'),
 (130,3,49,0,19,'receitas/grupo-qualquer');
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_category_entity_varchar` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_category_flat_store_1`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_category_flat_store_1`;
CREATE TABLE  `supernarede`.`catalog_category_flat_store_1` (
  `entity_id` int(11) unsigned NOT NULL,
  `parent_id` int(11) unsigned NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `path` varchar(255) NOT NULL,
  `position` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  `children_count` int(11) NOT NULL,
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `all_children` text,
  `available_sort_by` text,
  `children` text,
  `custom_apply_to_products` int(10) NOT NULL DEFAULT '0',
  `custom_design` varchar(255) NOT NULL DEFAULT '',
  `custom_design_from` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `custom_design_to` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `custom_layout_update` text,
  `custom_use_parent_settings` int(10) NOT NULL DEFAULT '0',
  `default_sort_by` varchar(255) NOT NULL DEFAULT '',
  `description` text,
  `display_mode` varchar(255) NOT NULL DEFAULT '',
  `filter_price_range` int(10) NOT NULL DEFAULT '0',
  `image` varchar(255) NOT NULL DEFAULT '',
  `include_in_menu` int(10) NOT NULL DEFAULT '0',
  `is_active` int(10) NOT NULL DEFAULT '0',
  `is_anchor` int(10) NOT NULL DEFAULT '0',
  `landing_page` int(10) NOT NULL DEFAULT '0',
  `meta_description` text,
  `meta_keywords` text,
  `meta_title` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `page_layout` varchar(255) NOT NULL DEFAULT '',
  `path_in_store` text,
  `thumbnail` varchar(255) NOT NULL DEFAULT '',
  `url_key` varchar(255) NOT NULL DEFAULT '',
  `url_path` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`entity_id`),
  KEY `IDX_STORE` (`store_id`),
  KEY `IDX_PATH` (`path`),
  KEY `IDX_LEVEL` (`level`),
  CONSTRAINT `FK_CATEGORY_FLAT_CATEGORY_ID_STORE_1` FOREIGN KEY (`entity_id`) REFERENCES `catalog_category_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATEGORY_FLAT_STORE_ID_STORE_1` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalog_category_flat_store_1`
--

/*!40000 ALTER TABLE `catalog_category_flat_store_1` DISABLE KEYS */;
LOCK TABLES `catalog_category_flat_store_1` WRITE;
INSERT INTO `supernarede`.`catalog_category_flat_store_1` VALUES  (1,0,'0000-00-00 00:00:00','2011-08-08 12:55:28','1',0,0,4,1,'','','',0,'','0000-00-00 00:00:00','0000-00-00 00:00:00','',0,'','','',0,'',1,0,0,0,'','','','','','','','root-catalog',''),
 (2,1,'2011-08-08 12:55:28','2011-08-08 12:55:28','1/2',1,1,3,1,'','','',0,'','0000-00-00 00:00:00','0000-00-00 00:00:00','',0,'','','',0,'',1,0,0,0,'','','','','','','','default-category',''),
 (3,2,'2011-08-10 18:49:03','2011-08-11 17:34:20','1/2/3',1,2,0,1,'','','',0,'default/default','0000-00-00 00:00:00','0000-00-00 00:00:00','',0,'position','','PAGE',0,'',1,1,1,8,'','','','Departamentos','one_column','','','departamentos','departamentos'),
 (4,2,'2011-08-11 17:44:10','2011-08-11 18:05:55','1/2/4',2,2,1,1,'','','',0,'default/default','0000-00-00 00:00:00','0000-00-00 00:00:00','',0,'','<p>Descri&ccedil;&atilde;o da categoria de Listas Prontas</p>','PAGE',0,'',1,1,1,10,'','','','Listas Prontas','one_column','','','listas-prontas','listas-prontas'),
 (5,4,'2011-08-11 17:58:24','2011-08-11 17:58:24','1/2/4/5',1,3,0,1,'','','',0,'','0000-00-00 00:00:00','0000-00-00 00:00:00','',0,'','','PAGE',0,'',1,1,1,0,'','','','Compra do mês','one_column','','','compra-do-mes','listas-prontas/compra-do-mes');
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_category_flat_store_1` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_category_product`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_category_product`;
CREATE TABLE  `supernarede`.`catalog_category_product` (
  `category_id` int(10) unsigned NOT NULL DEFAULT '0',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0',
  `position` int(10) NOT NULL DEFAULT '0',
  UNIQUE KEY `UNQ_CATEGORY_PRODUCT` (`category_id`,`product_id`),
  KEY `CATALOG_CATEGORY_PRODUCT_CATEGORY` (`category_id`),
  KEY `CATALOG_CATEGORY_PRODUCT_PRODUCT` (`product_id`),
  CONSTRAINT `CATALOG_CATEGORY_PRODUCT_CATEGORY` FOREIGN KEY (`category_id`) REFERENCES `catalog_category_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `CATALOG_CATEGORY_PRODUCT_PRODUCT` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalog_category_product`
--

/*!40000 ALTER TABLE `catalog_category_product` DISABLE KEYS */;
LOCK TABLES `catalog_category_product` WRITE;
INSERT INTO `supernarede`.`catalog_category_product` VALUES  (2,1,1),
 (2,2,1),
 (2,3,1),
 (2,4,1),
 (2,5,1),
 (2,6,1),
 (3,1,1),
 (3,2,1),
 (3,3,1),
 (3,4,1),
 (3,5,1),
 (4,6,1),
 (6,6,1),
 (7,1,1),
 (7,2,1),
 (7,3,1),
 (7,4,1),
 (7,5,1),
 (9,1,1),
 (9,2,1),
 (9,3,1),
 (9,4,1),
 (9,5,1),
 (15,1,1),
 (15,2,1),
 (15,3,1),
 (15,4,1),
 (15,5,1),
 (19,6,0);
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_category_product` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_category_product_index`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_category_product_index`;
CREATE TABLE  `supernarede`.`catalog_category_product_index` (
  `category_id` int(10) unsigned NOT NULL DEFAULT '0',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0',
  `position` int(10) unsigned DEFAULT NULL,
  `is_parent` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `visibility` tinyint(3) unsigned NOT NULL,
  UNIQUE KEY `UNQ_CATEGORY_PRODUCT` (`category_id`,`product_id`,`store_id`),
  KEY `FK_CATALOG_CATEGORY_PRODUCT_INDEX_CATEGORY_ENTITY` (`category_id`),
  KEY `IDX_JOIN` (`product_id`,`store_id`,`category_id`,`visibility`),
  KEY `IDX_BASE` (`store_id`,`category_id`,`visibility`,`is_parent`,`position`),
  CONSTRAINT `FK_CATALOG_CATEGORY_PROD_IDX_CATEGORY_ENTITY` FOREIGN KEY (`category_id`) REFERENCES `catalog_category_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATALOG_CATEGORY_PROD_IDX_PROD_ENTITY` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATEGORY_PRODUCT_INDEX_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalog_category_product_index`
--

/*!40000 ALTER TABLE `catalog_category_product_index` DISABLE KEYS */;
LOCK TABLES `catalog_category_product_index` WRITE;
INSERT INTO `supernarede`.`catalog_category_product_index` VALUES  (2,6,1,1,1,2),
 (2,1,1,1,1,4),
 (2,2,1,1,1,4),
 (2,3,1,1,1,4),
 (2,4,1,1,1,4),
 (2,5,1,1,1,4),
 (3,1,1,1,1,4),
 (3,2,1,1,1,4),
 (3,3,1,1,1,4),
 (3,4,1,1,1,4),
 (3,5,1,1,1,4),
 (4,6,1,1,1,2),
 (6,6,1,1,1,2),
 (7,1,1,1,1,4),
 (7,2,1,1,1,4),
 (7,3,1,1,1,4),
 (7,4,1,1,1,4),
 (7,5,1,1,1,4),
 (9,1,1,1,1,4),
 (9,2,1,1,1,4),
 (9,3,1,1,1,4),
 (9,4,1,1,1,4),
 (9,5,1,1,1,4),
 (15,1,1,1,1,4),
 (15,2,1,1,1,4),
 (15,3,1,1,1,4),
 (15,4,1,1,1,4),
 (15,5,1,1,1,4),
 (18,6,80000,0,1,2),
 (19,6,0,1,1,2);
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_category_product_index` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_category_product_index_enbl_idx`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_category_product_index_enbl_idx`;
CREATE TABLE  `supernarede`.`catalog_category_product_index_enbl_idx` (
  `product_id` int(10) unsigned NOT NULL DEFAULT '0',
  `visibility` int(11) unsigned NOT NULL DEFAULT '0',
  KEY `IDX_PRODUCT` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalog_category_product_index_enbl_idx`
--

/*!40000 ALTER TABLE `catalog_category_product_index_enbl_idx` DISABLE KEYS */;
LOCK TABLES `catalog_category_product_index_enbl_idx` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_category_product_index_enbl_idx` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_category_product_index_enbl_tmp`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_category_product_index_enbl_tmp`;
CREATE TABLE  `supernarede`.`catalog_category_product_index_enbl_tmp` (
  `product_id` int(10) unsigned NOT NULL DEFAULT '0',
  `visibility` int(11) unsigned NOT NULL DEFAULT '0',
  KEY `IDX_PRODUCT` (`product_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalog_category_product_index_enbl_tmp`
--

/*!40000 ALTER TABLE `catalog_category_product_index_enbl_tmp` DISABLE KEYS */;
LOCK TABLES `catalog_category_product_index_enbl_tmp` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_category_product_index_enbl_tmp` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_category_product_index_idx`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_category_product_index_idx`;
CREATE TABLE  `supernarede`.`catalog_category_product_index_idx` (
  `category_id` int(10) unsigned NOT NULL DEFAULT '0',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0',
  `position` int(10) NOT NULL DEFAULT '0',
  `is_parent` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `visibility` tinyint(3) unsigned NOT NULL,
  KEY `IDX_PRODUCT_CATEGORY_STORE` (`product_id`,`category_id`,`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalog_category_product_index_idx`
--

/*!40000 ALTER TABLE `catalog_category_product_index_idx` DISABLE KEYS */;
LOCK TABLES `catalog_category_product_index_idx` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_category_product_index_idx` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_category_product_index_tmp`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_category_product_index_tmp`;
CREATE TABLE  `supernarede`.`catalog_category_product_index_tmp` (
  `category_id` int(10) unsigned NOT NULL DEFAULT '0',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0',
  `position` int(10) NOT NULL DEFAULT '0',
  `is_parent` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `visibility` tinyint(3) unsigned NOT NULL
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalog_category_product_index_tmp`
--

/*!40000 ALTER TABLE `catalog_category_product_index_tmp` DISABLE KEYS */;
LOCK TABLES `catalog_category_product_index_tmp` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_category_product_index_tmp` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_compare_item`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_compare_item`;
CREATE TABLE  `supernarede`.`catalog_compare_item` (
  `catalog_compare_item_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `visitor_id` int(11) unsigned NOT NULL DEFAULT '0',
  `customer_id` int(11) unsigned DEFAULT NULL,
  `product_id` int(11) unsigned NOT NULL DEFAULT '0',
  `store_id` smallint(5) unsigned DEFAULT NULL,
  PRIMARY KEY (`catalog_compare_item_id`),
  KEY `FK_CATALOG_COMPARE_ITEM_CUSTOMER` (`customer_id`),
  KEY `FK_CATALOG_COMPARE_ITEM_PRODUCT` (`product_id`),
  KEY `IDX_VISITOR_PRODUCTS` (`visitor_id`,`product_id`),
  KEY `IDX_CUSTOMER_PRODUCTS` (`customer_id`,`product_id`),
  KEY `FK_CATALOG_COMPARE_ITEM_STORE` (`store_id`),
  CONSTRAINT `FK_CATALOG_COMPARE_ITEM_CUSTOMER` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATALOG_COMPARE_ITEM_PRODUCT` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATALOG_COMPARE_ITEM_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalog_compare_item`
--

/*!40000 ALTER TABLE `catalog_compare_item` DISABLE KEYS */;
LOCK TABLES `catalog_compare_item` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_compare_item` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_eav_attribute`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_eav_attribute`;
CREATE TABLE  `supernarede`.`catalog_eav_attribute` (
  `attribute_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `frontend_input_renderer` varchar(255) DEFAULT NULL,
  `is_global` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `is_visible` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `is_searchable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_filterable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_comparable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_visible_on_front` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_html_allowed_on_front` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_used_for_price_rules` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'deprecated after 1.4.0.1',
  `is_filterable_in_search` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `used_in_product_listing` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `used_for_sort_by` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_configurable` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `apply_to` varchar(255) NOT NULL,
  `is_visible_in_advanced_search` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `position` int(11) NOT NULL,
  `is_wysiwyg_enabled` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_used_for_promo_rules` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`attribute_id`),
  KEY `IDX_USED_FOR_SORT_BY` (`used_for_sort_by`),
  KEY `IDX_USED_IN_PRODUCT_LISTING` (`used_in_product_listing`),
  CONSTRAINT `FK_CATALOG_EAV_ATTRIBUTE_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=125 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalog_eav_attribute`
--

/*!40000 ALTER TABLE `catalog_eav_attribute` DISABLE KEYS */;
LOCK TABLES `catalog_eav_attribute` WRITE;
INSERT INTO `supernarede`.`catalog_eav_attribute` VALUES  (33,'',0,1,0,0,0,0,0,0,0,0,0,1,'',0,0,0,0),
 (34,'',0,1,0,0,0,0,0,0,0,0,0,1,'',0,0,0,0),
 (35,'',0,1,0,0,0,0,0,0,0,0,0,1,'',0,0,0,0),
 (36,'',0,1,0,0,0,0,1,0,0,0,0,1,'',0,0,1,0),
 (37,'',0,1,0,0,0,0,0,0,0,0,0,1,'',0,0,0,0),
 (38,'',0,1,0,0,0,0,0,0,0,0,0,1,'',0,0,0,0),
 (39,'',0,1,0,0,0,0,0,0,0,0,0,1,'',0,0,0,0),
 (40,'',0,1,0,0,0,0,0,0,0,0,0,1,'',0,0,0,0),
 (41,'',0,1,0,0,0,0,0,0,0,0,0,1,'',0,0,0,0),
 (42,'',0,1,0,0,0,0,0,0,0,0,0,1,'',0,0,0,0),
 (43,'',1,1,0,0,0,0,0,0,0,0,0,1,'',0,0,0,0),
 (44,'',1,0,0,0,0,0,0,0,0,0,0,1,'',0,0,0,0),
 (45,'',1,0,0,0,0,0,0,0,0,0,0,1,'',0,0,0,0),
 (46,'',1,0,0,0,0,0,0,0,0,0,0,1,'',0,0,0,0),
 (47,'',1,0,0,0,0,0,0,0,0,0,0,1,'',0,0,0,0),
 (48,'',1,0,0,0,0,0,0,0,0,0,0,1,'',0,0,0,0),
 (49,'',0,0,0,0,0,0,0,0,0,0,0,1,'',0,0,0,0),
 (50,'',0,1,0,0,0,0,0,0,0,0,0,1,'',0,0,0,0),
 (51,'',0,1,0,0,0,0,0,0,0,0,0,1,'',0,0,0,0),
 (52,'',0,1,0,0,0,0,0,0,0,0,0,1,'',0,0,0,0),
 (53,'',0,1,0,0,0,0,0,0,0,0,0,1,'',0,0,0,0),
 (54,'',0,1,0,0,0,0,0,0,0,0,0,1,'',0,0,0,0),
 (55,'',0,1,0,0,0,0,0,0,0,0,0,1,'',0,0,0,0),
 (56,'',1,0,0,0,0,0,0,0,0,0,0,1,'',0,0,0,0),
 (57,'',1,0,0,0,0,0,0,0,0,0,0,1,'',0,0,0,0),
 (58,'adminhtml/catalog_category_helper_sortby_available',0,1,0,0,0,0,0,0,0,0,0,1,'',0,0,0,0),
 (59,'adminhtml/catalog_category_helper_sortby_default',0,1,0,0,0,0,0,0,0,0,0,1,'',0,0,0,0),
 (60,'',0,1,1,0,0,0,0,0,0,1,1,1,'',1,0,0,0),
 (61,'',0,1,1,0,1,0,1,0,0,0,0,1,'',1,0,1,0),
 (62,'',0,1,1,0,1,0,1,0,0,1,0,1,'',1,0,1,0),
 (63,'',1,1,1,0,1,0,0,0,0,0,0,1,'',1,0,0,0),
 (64,'',2,1,1,1,0,0,0,0,0,1,1,1,'simple,configurable,virtual,bundle,downloadable',1,0,0,0),
 (65,'',2,1,0,0,0,0,0,0,0,1,0,1,'simple,configurable,virtual,bundle,downloadable',0,0,0,0),
 (66,'',2,1,0,0,0,0,0,0,0,1,0,1,'simple,configurable,virtual,bundle,downloadable',0,0,0,0),
 (67,'',2,1,0,0,0,0,0,0,0,1,0,1,'simple,configurable,virtual,bundle,downloadable',0,0,0,0),
 (68,'',2,1,0,0,0,0,0,0,0,0,0,1,'simple,virtual,downloadable',0,0,0,0),
 (69,'',1,1,0,0,0,0,0,0,0,0,0,1,'simple,bundle',0,0,0,0),
 (70,'',1,1,1,1,1,0,0,0,0,0,0,1,'simple',1,0,0,0),
 (71,'',0,1,0,0,0,0,0,0,0,0,0,1,'',0,0,0,0),
 (72,'',0,1,0,0,0,0,0,0,0,0,0,1,'',0,0,0,0),
 (73,'',0,1,0,0,0,0,0,0,0,0,0,1,'',0,0,0,0),
 (74,'',0,1,0,0,0,0,0,0,0,0,0,1,'',0,0,0,0),
 (75,'',0,1,0,0,0,0,0,0,0,1,0,1,'',0,0,0,0),
 (76,'',0,1,0,0,0,0,0,0,0,1,0,1,'',0,0,0,0),
 (77,'',1,1,0,0,0,0,0,0,0,0,0,1,'',0,0,0,0),
 (78,'',1,0,0,0,0,0,0,0,0,0,0,1,'',0,0,0,0),
 (79,'',2,1,0,0,0,0,0,0,0,0,0,1,'simple,configurable,virtual,bundle,downloadable',0,0,0,0),
 (80,'',1,1,1,1,1,0,0,0,0,0,0,1,'simple',1,0,0,0),
 (81,'',2,1,0,0,0,0,0,0,0,1,0,1,'',0,0,0,0),
 (82,'',2,1,0,0,0,0,0,0,0,1,0,1,'',0,0,0,0),
 (83,'',1,1,0,0,0,0,0,0,0,0,0,1,'',0,0,0,0),
 (84,'',2,1,1,0,0,0,0,0,0,1,0,1,'',0,0,0,0),
 (85,'',2,1,1,0,0,0,0,0,0,1,0,1,'simple,configurable,virtual,bundle,downloadable',1,0,0,0),
 (86,'',0,1,0,0,0,0,0,0,0,1,0,1,'',0,0,0,0),
 (87,'',0,0,0,0,0,0,0,0,0,0,0,1,'',0,0,0,0),
 (88,'',0,0,0,0,0,0,0,0,0,0,0,1,'simple,configurable,virtual,bundle,downloadable',0,0,0,0),
 (89,'',1,1,0,0,0,0,0,0,0,0,0,0,'simple,virtual',0,0,0,0),
 (90,'',1,1,0,0,0,0,0,0,0,0,0,0,'simple,virtual',0,0,0,0),
 (91,'',0,1,0,0,0,0,0,0,0,0,0,1,'',0,0,0,0),
 (92,'',0,1,0,0,0,0,0,0,0,0,0,1,'',0,0,0,0),
 (93,'',0,1,0,0,0,0,0,0,0,0,0,1,'',0,0,0,0),
 (94,'',0,1,0,0,0,0,0,0,0,0,0,1,'',0,0,0,0),
 (95,'',0,1,0,0,0,0,0,0,0,0,0,1,'',0,0,0,0),
 (96,'',0,1,0,0,0,0,0,0,0,0,0,1,'',0,0,0,0),
 (97,'',0,1,0,0,0,0,0,0,0,0,0,1,'',0,0,0,0),
 (98,'',1,0,0,0,0,0,0,0,0,1,0,1,'',0,0,0,0),
 (99,'',1,0,0,0,0,0,0,0,0,0,0,1,'',0,0,0,0),
 (100,'',0,0,0,0,0,0,0,0,0,1,0,0,'',0,0,0,0),
 (101,'',0,0,0,0,0,0,0,0,0,1,0,0,'',0,0,0,0),
 (102,'',0,0,0,0,0,0,0,0,0,1,0,0,'',0,0,0,0),
 (103,'',1,0,0,0,0,0,0,0,0,0,0,1,'',0,0,0,0),
 (104,'',1,0,0,0,0,0,0,0,0,0,0,1,'',0,0,0,0),
 (105,'',0,1,0,0,0,0,0,0,0,0,0,1,'',0,0,0,0),
 (106,'',0,1,0,0,0,0,0,0,0,0,0,1,'',0,0,0,0),
 (107,'',0,1,0,0,0,0,0,0,0,0,0,1,'',0,0,0,0),
 (108,'adminhtml/catalog_category_helper_pricestep',0,1,0,0,0,0,0,0,0,0,0,1,'',0,0,0,0),
 (109,'',1,1,0,0,0,0,0,0,0,0,0,0,'',0,0,0,0),
 (110,'giftmessage/adminhtml_product_helper_form_config',1,1,0,0,0,0,0,0,0,0,0,0,'',0,0,0,0),
 (111,'',1,0,0,0,0,0,0,0,0,1,0,0,'bundle',0,0,0,0),
 (112,'',1,0,0,0,0,0,0,0,0,0,0,0,'bundle',0,0,0,0),
 (113,'',1,0,0,0,0,0,0,0,0,1,0,0,'bundle',0,0,0,0),
 (114,'',1,1,0,0,0,0,0,0,0,1,0,0,'bundle',0,0,0,0),
 (115,'',1,0,0,0,0,0,0,0,0,1,0,0,'bundle',0,0,0,0),
 (116,'',1,0,0,0,0,0,0,0,0,1,0,0,'downloadable',0,0,0,0),
 (117,'',0,0,0,0,0,0,0,0,0,0,0,0,'downloadable',0,0,0,0),
 (118,'',0,0,0,0,0,0,0,0,0,0,0,0,'downloadable',0,0,0,0),
 (119,'',1,0,0,0,0,0,0,0,0,1,0,0,'downloadable',0,0,0,0),
 (120,'',0,1,0,0,0,0,0,0,0,0,0,1,'',0,0,0,0),
 (121,'',1,1,0,0,0,0,0,0,0,1,0,0,'',0,0,0,0),
 (122,NULL,1,1,1,2,0,0,1,0,1,0,0,0,'',0,0,0,0),
 (123,NULL,1,1,1,1,0,0,1,0,1,0,0,0,'',0,0,0,0),
 (124,NULL,1,1,1,1,0,0,1,0,1,0,0,1,'',0,0,0,1);
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_eav_attribute` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_product_bundle_option`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_product_bundle_option`;
CREATE TABLE  `supernarede`.`catalog_product_bundle_option` (
  `option_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned NOT NULL,
  `required` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `position` int(10) unsigned NOT NULL DEFAULT '0',
  `type` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`option_id`),
  KEY `FK_CATALOG_PRODUCT_BUNDLE_OPTION_PARENT` (`parent_id`),
  CONSTRAINT `FK_CATALOG_PRODUCT_BUNDLE_OPTION_PARENT` FOREIGN KEY (`parent_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Bundle Options';

--
-- Dumping data for table `supernarede`.`catalog_product_bundle_option`
--

/*!40000 ALTER TABLE `catalog_product_bundle_option` DISABLE KEYS */;
LOCK TABLES `catalog_product_bundle_option` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_product_bundle_option` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_product_bundle_option_value`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_product_bundle_option_value`;
CREATE TABLE  `supernarede`.`catalog_product_bundle_option_value` (
  `value_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `option_id` int(10) unsigned NOT NULL,
  `store_id` smallint(5) unsigned NOT NULL,
  `title` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `UNQ_OPTION_STORE` (`option_id`,`store_id`),
  CONSTRAINT `FK_CATALOG_PRODUCT_BUNDLE_OPTION_VALUE_OPTION` FOREIGN KEY (`option_id`) REFERENCES `catalog_product_bundle_option` (`option_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Bundle Selections';

--
-- Dumping data for table `supernarede`.`catalog_product_bundle_option_value`
--

/*!40000 ALTER TABLE `catalog_product_bundle_option_value` DISABLE KEYS */;
LOCK TABLES `catalog_product_bundle_option_value` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_product_bundle_option_value` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_product_bundle_price_index`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_product_bundle_price_index`;
CREATE TABLE  `supernarede`.`catalog_product_bundle_price_index` (
  `entity_id` int(10) unsigned NOT NULL,
  `website_id` smallint(5) unsigned NOT NULL,
  `customer_group_id` smallint(3) unsigned NOT NULL,
  `min_price` decimal(12,4) NOT NULL,
  `max_price` decimal(12,4) NOT NULL,
  PRIMARY KEY (`entity_id`,`website_id`,`customer_group_id`),
  KEY `IDX_WEBSITE` (`website_id`),
  KEY `IDX_CUSTOMER_GROUP` (`customer_group_id`),
  CONSTRAINT `CATALOG_PRODUCT_BUNDLE_PRICE_INDEX_CUSTOMER_GROUP` FOREIGN KEY (`customer_group_id`) REFERENCES `customer_group` (`customer_group_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `CATALOG_PRODUCT_BUNDLE_PRICE_INDEX_PRODUCT_ENTITY` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `CATALOG_PRODUCT_BUNDLE_PRICE_INDEX_WEBSITE` FOREIGN KEY (`website_id`) REFERENCES `core_website` (`website_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalog_product_bundle_price_index`
--

/*!40000 ALTER TABLE `catalog_product_bundle_price_index` DISABLE KEYS */;
LOCK TABLES `catalog_product_bundle_price_index` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_product_bundle_price_index` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_product_bundle_selection`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_product_bundle_selection`;
CREATE TABLE  `supernarede`.`catalog_product_bundle_selection` (
  `selection_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `option_id` int(10) unsigned NOT NULL,
  `parent_product_id` int(10) unsigned NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `position` int(10) unsigned NOT NULL DEFAULT '0',
  `is_default` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `selection_price_type` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `selection_price_value` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `selection_qty` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `selection_can_change_qty` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`selection_id`),
  KEY `FK_CATALOG_PRODUCT_BUNDLE_SELECTION_OPTION` (`option_id`),
  KEY `FK_CATALOG_PRODUCT_BUNDLE_SELECTION_PRODUCT` (`product_id`),
  CONSTRAINT `FK_CATALOG_PRODUCT_BUNDLE_SELECTION_OPTION` FOREIGN KEY (`option_id`) REFERENCES `catalog_product_bundle_option` (`option_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATALOG_PRODUCT_BUNDLE_SELECTION_PRODUCT` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Bundle Selections';

--
-- Dumping data for table `supernarede`.`catalog_product_bundle_selection`
--

/*!40000 ALTER TABLE `catalog_product_bundle_selection` DISABLE KEYS */;
LOCK TABLES `catalog_product_bundle_selection` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_product_bundle_selection` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_product_bundle_selection_price`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_product_bundle_selection_price`;
CREATE TABLE  `supernarede`.`catalog_product_bundle_selection_price` (
  `selection_id` int(10) unsigned NOT NULL,
  `website_id` smallint(5) unsigned NOT NULL,
  `selection_price_type` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `selection_price_value` decimal(12,4) NOT NULL DEFAULT '0.0000',
  PRIMARY KEY (`selection_id`,`website_id`),
  KEY `FK_BUNDLE_PRICE_SELECTION_WEBSITE` (`website_id`),
  CONSTRAINT `FK_BUNDLE_PRICE_SELECTION_ID` FOREIGN KEY (`selection_id`) REFERENCES `catalog_product_bundle_selection` (`selection_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_BUNDLE_PRICE_SELECTION_WEBSITE` FOREIGN KEY (`website_id`) REFERENCES `core_website` (`website_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalog_product_bundle_selection_price`
--

/*!40000 ALTER TABLE `catalog_product_bundle_selection_price` DISABLE KEYS */;
LOCK TABLES `catalog_product_bundle_selection_price` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_product_bundle_selection_price` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_product_bundle_stock_index`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_product_bundle_stock_index`;
CREATE TABLE  `supernarede`.`catalog_product_bundle_stock_index` (
  `entity_id` int(10) unsigned NOT NULL,
  `website_id` smallint(5) unsigned NOT NULL,
  `stock_id` smallint(5) unsigned NOT NULL,
  `option_id` int(10) unsigned NOT NULL DEFAULT '0',
  `stock_status` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`entity_id`,`stock_id`,`website_id`,`option_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalog_product_bundle_stock_index`
--

/*!40000 ALTER TABLE `catalog_product_bundle_stock_index` DISABLE KEYS */;
LOCK TABLES `catalog_product_bundle_stock_index` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_product_bundle_stock_index` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_product_enabled_index`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_product_enabled_index`;
CREATE TABLE  `supernarede`.`catalog_product_enabled_index` (
  `product_id` int(10) unsigned NOT NULL DEFAULT '0',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `visibility` smallint(5) unsigned NOT NULL DEFAULT '0',
  UNIQUE KEY `UNQ_PRODUCT_STORE` (`product_id`,`store_id`),
  KEY `IDX_PRODUCT_VISIBILITY_IN_STORE` (`product_id`,`store_id`,`visibility`),
  KEY `FK_CATALOG_PRODUCT_ENABLED_INDEX_STORE` (`store_id`),
  CONSTRAINT `FK_CATALOG_PRODUCT_ENABLED_INDEX_PRODUCT_ENTITY` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATALOG_PRODUCT_ENABLED_INDEX_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalog_product_enabled_index`
--

/*!40000 ALTER TABLE `catalog_product_enabled_index` DISABLE KEYS */;
LOCK TABLES `catalog_product_enabled_index` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_product_enabled_index` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_product_entity`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_product_entity`;
CREATE TABLE  `supernarede`.`catalog_product_entity` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entity_type_id` smallint(8) unsigned NOT NULL DEFAULT '0',
  `attribute_set_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `type_id` varchar(32) NOT NULL DEFAULT 'simple',
  `sku` varchar(64) DEFAULT NULL,
  `has_options` smallint(1) NOT NULL DEFAULT '0',
  `required_options` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`entity_id`),
  KEY `FK_CATALOG_PRODUCT_ENTITY_ENTITY_TYPE` (`entity_type_id`),
  KEY `FK_CATALOG_PRODUCT_ENTITY_ATTRIBUTE_SET_ID` (`attribute_set_id`),
  KEY `sku` (`sku`),
  CONSTRAINT `FK_CATALOG_PRODUCT_ENTITY_ATTRIBUTE_SET_ID` FOREIGN KEY (`attribute_set_id`) REFERENCES `eav_attribute_set` (`attribute_set_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATALOG_PRODUCT_ENTITY_ENTITY_TYPE` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='Product Entities';

--
-- Dumping data for table `supernarede`.`catalog_product_entity`
--

/*!40000 ALTER TABLE `catalog_product_entity` DISABLE KEYS */;
LOCK TABLES `catalog_product_entity` WRITE;
INSERT INTO `supernarede`.`catalog_product_entity` VALUES  (1,4,9,'simple','C050505',0,0,'2011-08-17 12:38:40','2011-08-17 12:47:24'),
 (2,4,9,'simple','C54654654',0,0,'2011-08-17 12:46:20','2011-08-17 12:46:55'),
 (3,4,9,'simple','C548748',0,0,'2011-08-17 12:50:17','2011-08-18 20:11:33'),
 (4,4,9,'simple','C/879846',0,0,'2011-08-17 12:52:39','2011-08-19 16:28:35'),
 (5,4,9,'simple','C7987654',0,0,'2011-08-17 13:07:19','2011-08-17 13:07:19'),
 (6,4,10,'grouped','CA123456',0,0,'2011-08-17 16:40:27','2011-09-21 20:06:51');
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_product_entity` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_product_entity_datetime`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_product_entity_datetime`;
CREATE TABLE  `supernarede`.`catalog_product_entity_datetime` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT,
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0',
  `value` datetime DEFAULT NULL,
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `IDX_ATTRIBUTE_VALUE` (`entity_id`,`attribute_id`,`store_id`),
  KEY `FK_CATALOG_PRODUCT_ENTITY_DATETIME_ATTRIBUTE` (`attribute_id`),
  KEY `FK_CATALOG_PRODUCT_ENTITY_DATETIME_STORE` (`store_id`),
  KEY `FK_CATALOG_PRODUCT_ENTITY_DATETIME_PRODUCT_ENTITY` (`entity_id`),
  CONSTRAINT `FK_CATALOG_PRODUCT_ENTITY_DATETIME_ATTRIBUTE` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATALOG_PRODUCT_ENTITY_DATETIME_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATALOG_PROD_ENTITY_DATETIME_PROD_ENTITY` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalog_product_entity_datetime`
--

/*!40000 ALTER TABLE `catalog_product_entity_datetime` DISABLE KEYS */;
LOCK TABLES `catalog_product_entity_datetime` WRITE;
INSERT INTO `supernarede`.`catalog_product_entity_datetime` VALUES  (1,4,81,0,1,NULL),
 (2,4,82,0,1,NULL),
 (3,4,66,0,1,NULL),
 (4,4,67,0,1,NULL),
 (5,4,93,0,1,NULL),
 (6,4,94,0,1,NULL),
 (13,4,81,0,2,NULL),
 (14,4,82,0,2,NULL),
 (15,4,66,0,2,NULL),
 (16,4,67,0,2,NULL),
 (17,4,93,0,2,NULL),
 (18,4,94,0,2,NULL),
 (31,4,81,0,3,NULL),
 (32,4,82,0,3,NULL),
 (33,4,66,0,3,NULL),
 (34,4,67,0,3,NULL),
 (35,4,93,0,3,NULL),
 (36,4,94,0,3,NULL),
 (37,4,66,0,4,NULL),
 (38,4,67,0,4,NULL),
 (39,4,81,0,4,NULL),
 (40,4,82,0,4,NULL),
 (41,4,93,0,4,NULL),
 (42,4,94,0,4,NULL),
 (49,4,81,0,5,NULL),
 (50,4,82,0,5,NULL),
 (51,4,66,0,5,NULL),
 (52,4,67,0,5,NULL),
 (53,4,93,0,5,NULL),
 (54,4,94,0,5,NULL),
 (61,4,81,0,6,NULL),
 (62,4,82,0,6,NULL),
 (63,4,93,0,6,NULL),
 (64,4,94,0,6,NULL);
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_product_entity_datetime` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_product_entity_decimal`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_product_entity_decimal`;
CREATE TABLE  `supernarede`.`catalog_product_entity_decimal` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT,
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0',
  `value` decimal(12,4) DEFAULT NULL,
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `IDX_ATTRIBUTE_VALUE` (`entity_id`,`attribute_id`,`store_id`),
  KEY `FK_CATALOG_PRODUCT_ENTITY_DECIMAL_STORE` (`store_id`),
  KEY `FK_CATALOG_PRODUCT_ENTITY_DECIMAL_PRODUCT_ENTITY` (`entity_id`),
  KEY `FK_CATALOG_PRODUCT_ENTITY_DECIMAL_ATTRIBUTE` (`attribute_id`),
  CONSTRAINT `FK_CATALOG_PRODUCT_ENTITY_DECIMAL_ATTRIBUTE` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATALOG_PRODUCT_ENTITY_DECIMAL_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATALOG_PROD_ENTITY_DECIMAL_PROD_ENTITY` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalog_product_entity_decimal`
--

/*!40000 ALTER TABLE `catalog_product_entity_decimal` DISABLE KEYS */;
LOCK TABLES `catalog_product_entity_decimal` WRITE;
INSERT INTO `supernarede`.`catalog_product_entity_decimal` VALUES  (1,4,69,0,1,'0.3000'),
 (2,4,64,0,1,'1.2900'),
 (3,4,65,0,1,NULL),
 (6,4,69,0,2,'0.3500'),
 (7,4,64,0,2,'1.3900'),
 (8,4,65,0,2,NULL),
 (13,4,69,0,3,'0.4000'),
 (14,4,64,0,3,'3.4900'),
 (15,4,65,0,3,NULL),
 (16,4,64,0,4,'3.5900'),
 (17,4,65,0,4,NULL),
 (18,4,69,0,4,'0.4000'),
 (21,4,69,0,5,'0.8000'),
 (22,4,64,0,5,'1.9000'),
 (23,4,65,0,5,NULL);
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_product_entity_decimal` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_product_entity_gallery`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_product_entity_gallery`;
CREATE TABLE  `supernarede`.`catalog_product_entity_gallery` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT,
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0',
  `position` int(11) NOT NULL DEFAULT '0',
  `value` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `IDX_BASE` (`entity_type_id`,`entity_id`,`attribute_id`,`store_id`),
  KEY `FK_ATTRIBUTE_GALLERY_ENTITY` (`entity_id`),
  KEY `FK_CATALOG_CATEGORY_ENTITY_GALLERY_ATTRIBUTE` (`attribute_id`),
  KEY `FK_CATALOG_CATEGORY_ENTITY_GALLERY_STORE` (`store_id`),
  CONSTRAINT `FK_CATALOG_PRODUCT_ENTITY_GALLERY_ATTRIBUTE` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATALOG_PRODUCT_ENTITY_GALLERY_ENTITY` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATALOG_PRODUCT_ENTITY_GALLERY_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalog_product_entity_gallery`
--

/*!40000 ALTER TABLE `catalog_product_entity_gallery` DISABLE KEYS */;
LOCK TABLES `catalog_product_entity_gallery` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_product_entity_gallery` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_product_entity_int`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_product_entity_int`;
CREATE TABLE  `supernarede`.`catalog_product_entity_int` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT,
  `entity_type_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0',
  `value` int(11) DEFAULT NULL,
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `IDX_ATTRIBUTE_VALUE` (`entity_id`,`attribute_id`,`store_id`),
  KEY `FK_CATALOG_PRODUCT_ENTITY_INT_ATTRIBUTE` (`attribute_id`),
  KEY `FK_CATALOG_PRODUCT_ENTITY_INT_STORE` (`store_id`),
  KEY `FK_CATALOG_PRODUCT_ENTITY_INT_PRODUCT_ENTITY` (`entity_id`),
  CONSTRAINT `FK_CATALOG_PRODUCT_ENTITY_INT_ATTRIBUTE` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATALOG_PRODUCT_ENTITY_INT_PRODUCT_ENTITY` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATALOG_PRODUCT_ENTITY_INT_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalog_product_entity_int`
--

/*!40000 ALTER TABLE `catalog_product_entity_int` DISABLE KEYS */;
LOCK TABLES `catalog_product_entity_int` WRITE;
INSERT INTO `supernarede`.`catalog_product_entity_int` VALUES  (1,4,84,0,1,1),
 (2,4,124,0,1,9),
 (3,4,122,0,1,3),
 (4,4,91,0,1,4),
 (5,4,121,0,1,0),
 (6,4,109,0,1,1),
 (7,4,85,0,1,NULL),
 (8,4,89,0,1,0),
 (10,4,84,0,2,1),
 (11,4,124,0,2,10),
 (12,4,122,0,2,3),
 (13,4,91,0,2,4),
 (14,4,121,0,2,0),
 (15,4,109,0,2,1),
 (16,4,85,0,2,NULL),
 (17,4,89,0,2,0),
 (20,4,84,0,3,1),
 (21,4,124,0,3,12),
 (22,4,122,0,3,4),
 (23,4,91,0,3,4),
 (24,4,121,0,3,0),
 (25,4,109,0,3,1),
 (26,4,85,0,3,NULL),
 (27,4,89,0,3,0),
 (28,4,84,0,4,1),
 (29,4,85,0,4,NULL),
 (30,4,89,0,4,0),
 (31,4,91,0,4,4),
 (32,4,109,0,4,1),
 (33,4,121,0,4,0),
 (34,4,122,0,4,4),
 (35,4,124,0,4,11),
 (38,4,84,0,5,1),
 (39,4,124,0,5,13),
 (40,4,122,0,5,4),
 (41,4,91,0,5,4),
 (42,4,121,0,5,0),
 (43,4,109,0,5,1),
 (44,4,85,0,5,NULL),
 (45,4,89,0,5,0),
 (47,4,84,0,6,1),
 (48,4,91,0,6,2),
 (49,4,121,0,6,0),
 (50,4,109,0,6,1);
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_product_entity_int` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_product_entity_media_gallery`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_product_entity_media_gallery`;
CREATE TABLE  `supernarede`.`catalog_product_entity_media_gallery` (
  `value_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0',
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`value_id`),
  KEY `FK_CATALOG_PRODUCT_MEDIA_GALLERY_ATTRIBUTE` (`attribute_id`),
  KEY `FK_CATALOG_PRODUCT_MEDIA_GALLERY_ENTITY` (`entity_id`),
  CONSTRAINT `FK_CATALOG_PRODUCT_MEDIA_GALLERY_ATTRIBUTE` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  CONSTRAINT `FK_CATALOG_PRODUCT_MEDIA_GALLERY_ENTITY` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='Catalog product media gallery';

--
-- Dumping data for table `supernarede`.`catalog_product_entity_media_gallery`
--

/*!40000 ALTER TABLE `catalog_product_entity_media_gallery` DISABLE KEYS */;
LOCK TABLES `catalog_product_entity_media_gallery` WRITE;
INSERT INTO `supernarede`.`catalog_product_entity_media_gallery` VALUES  (1,77,1,'/a/n/antarctica_lata1.jpg'),
 (2,77,2,'/b/a/bavaria_lata2.jpg'),
 (3,77,2,'/b/a/bavaria_lata1.jpg'),
 (4,77,3,'/h/e/heineken_ln2.jpg'),
 (5,77,3,'/h/e/heineken_ln1.jpg'),
 (6,77,4,'/h/e/heineken_ln2_1.jpg'),
 (7,77,4,'/h/e/heineken_ln1_1.jpg'),
 (8,77,5,'/s/k/skol_ln1.jpg'),
 (9,77,5,'/s/k/skol_ln2.jpg'),
 (10,77,5,'/s/k/skol_ln.jpg');
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_product_entity_media_gallery` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_product_entity_media_gallery_value`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_product_entity_media_gallery_value`;
CREATE TABLE  `supernarede`.`catalog_product_entity_media_gallery_value` (
  `value_id` int(11) unsigned NOT NULL DEFAULT '0',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `label` varchar(255) DEFAULT NULL,
  `position` int(11) unsigned DEFAULT NULL,
  `disabled` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`value_id`,`store_id`),
  KEY `FK_CATALOG_PRODUCT_MEDIA_GALLERY_VALUE_STORE` (`store_id`),
  CONSTRAINT `FK_CATALOG_PRODUCT_MEDIA_GALLERY_VALUE_GALLERY` FOREIGN KEY (`value_id`) REFERENCES `catalog_product_entity_media_gallery` (`value_id`) ON DELETE CASCADE,
  CONSTRAINT `FK_CATALOG_PRODUCT_MEDIA_GALLERY_VALUE_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog product media gallery values';

--
-- Dumping data for table `supernarede`.`catalog_product_entity_media_gallery_value`
--

/*!40000 ALTER TABLE `catalog_product_entity_media_gallery_value` DISABLE KEYS */;
LOCK TABLES `catalog_product_entity_media_gallery_value` WRITE;
INSERT INTO `supernarede`.`catalog_product_entity_media_gallery_value` VALUES  (1,0,'',1,0),
 (2,0,'',1,0),
 (3,0,'',2,0),
 (4,0,'',1,0),
 (5,0,'',2,0),
 (6,0,'',1,1),
 (7,0,'',2,0),
 (8,0,'',1,0),
 (9,0,'',2,0),
 (10,0,'',3,0);
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_product_entity_media_gallery_value` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_product_entity_text`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_product_entity_text`;
CREATE TABLE  `supernarede`.`catalog_product_entity_text` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT,
  `entity_type_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0',
  `value` text NOT NULL,
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `IDX_ATTRIBUTE_VALUE` (`entity_id`,`attribute_id`,`store_id`),
  KEY `FK_CATALOG_PRODUCT_ENTITY_TEXT_ATTRIBUTE` (`attribute_id`),
  KEY `FK_CATALOG_PRODUCT_ENTITY_TEXT_STORE` (`store_id`),
  KEY `FK_CATALOG_PRODUCT_ENTITY_TEXT_PRODUCT_ENTITY` (`entity_id`),
  CONSTRAINT `FK_CATALOG_PRODUCT_ENTITY_TEXT_ATTRIBUTE` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATALOG_PRODUCT_ENTITY_TEXT_PRODUCT_ENTITY` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATALOG_PRODUCT_ENTITY_TEXT_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalog_product_entity_text`
--

/*!40000 ALTER TABLE `catalog_product_entity_text` DISABLE KEYS */;
LOCK TABLES `catalog_product_entity_text` WRITE;
INSERT INTO `supernarede`.`catalog_product_entity_text` VALUES  (1,4,61,0,1,'<p><strong>Ingredientes: </strong><br />&Aacute;gua, malte, cereais n&atilde;o malteados, carboidratos e l&uacute;pulo. Antioxidante: INS 316, estabilizante: INS 405. Teor alco&oacute;lico: 4,6% vol. <br /><strong>CONT&Eacute;M GL&Uacute;TEN.</strong></p>'),
 (2,4,72,0,1,''),
 (3,4,95,0,1,''),
 (4,4,62,0,1,''),
 (5,4,61,0,2,'<p><strong>Ingredientes:</strong> <br />&Aacute;gua, malte, cereais n&atilde;o malteados, carboidratos e l&uacute;pulo. Antioxidante: INS 316, estabilizante: INS 405. Teor alco&oacute;lico: 4,6% vol. <br /><strong>CONT&Eacute;M GL&Uacute;TEN.</strong></p>'),
 (6,4,72,0,2,''),
 (7,4,95,0,2,''),
 (8,4,62,0,2,''),
 (9,4,61,0,3,'<p><strong>Ingredientes:</strong> <br />&Aacute;gua, malte, cereais n&atilde;o malteados, carboidratos e l&uacute;pulo. Antioxidante: INS 316, estabilizante: INS 405. Teor alco&oacute;lico: 4,6% vol. <br /><strong>CONT&Eacute;M GL&Uacute;TEN.</strong></p>'),
 (10,4,72,0,3,''),
 (11,4,95,0,3,''),
 (12,4,62,0,3,''),
 (13,4,61,0,4,'<p><strong>Ingredientes:</strong> <br />&Aacute;gua, malte, cereais n&atilde;o malteados, carboidratos e l&uacute;pulo. Antioxidante: INS 316, estabilizante: INS 405. Teor alco&oacute;lico: 4,6% vol. <br /><strong>CONT&Eacute;M GL&Uacute;TEN.</strong></p>'),
 (14,4,62,0,4,''),
 (15,4,72,0,4,''),
 (16,4,95,0,4,''),
 (17,4,61,0,5,'<p><strong>Ingredientes:</strong> <br />&Aacute;gua, malte, cereais n&atilde;o malteados, carboidratos e l&uacute;pulo. Antioxidante: INS 316, estabilizante: INS 405. Teor alco&oacute;lico: 4,6% vol. <br /><strong>CONT&Eacute;M GL&Uacute;TEN.</strong></p>'),
 (18,4,72,0,5,''),
 (19,4,95,0,5,''),
 (20,4,62,0,5,''),
 (21,4,61,0,6,'<p>ASDASDasdhasi hdoashda sihasiodh asoihdio hsdaiohasidhilash ilashdiashdiohasdihas isahd aishd</p>\r\n<p>adkashdasdkajshdkjashdksajdhakjh<br />- kaushkjahskjdhsakjdhkjas adsad as<br />- kaushkjahskjdhsakjdhkjas adsad as</p>\r\n<p>- kaushkjahskjdhsakjdhkjas adsad as<br /> - kaushkjahskjdhsakjdhkjas adsad as<br /> - kaushkjahskjdhsakjdhkjas adsad as</p>\r\n<p>lisajdliasj dlasjdlijas ildjasiljdlisjalidjlisaj ijasildji lasjdilasjdlij asiljdil sajdliasj dliasjd ilasjdilasjid jasildj ijdilsajdliasjdlijas lijsalid jaslijdli asjlidsaj dlijasdj aslidjasli djasli jdlasijd lijaslidjli jliasjd lijsalidjlisaj liasjd lijd lasijd liasjdli jalidjasjslid</p>\r\n<p>- kaushkjahskjdhsakjdhkjas adsad as<br /> - kaushkjahskjdhsakjdhkjas adsad as</p>'),
 (22,4,72,0,6,''),
 (23,4,95,0,6,''),
 (24,4,62,0,6,'');
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_product_entity_text` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_product_entity_tier_price`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_product_entity_tier_price`;
CREATE TABLE  `supernarede`.`catalog_product_entity_tier_price` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT,
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0',
  `all_groups` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `customer_group_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `qty` decimal(12,4) NOT NULL DEFAULT '1.0000',
  `value` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `website_id` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `UNQ_CATALOG_PRODUCT_TIER_PRICE` (`entity_id`,`all_groups`,`customer_group_id`,`qty`,`website_id`),
  KEY `FK_CATALOG_PRODUCT_ENTITY_TIER_PRICE_PRODUCT_ENTITY` (`entity_id`),
  KEY `FK_CATALOG_PRODUCT_ENTITY_TIER_PRICE_GROUP` (`customer_group_id`),
  KEY `FK_CATALOG_PRODUCT_TIER_WEBSITE` (`website_id`),
  CONSTRAINT `FK_CATALOG_PRODUCT_ENTITY_TIER_PRICE_GROUP` FOREIGN KEY (`customer_group_id`) REFERENCES `customer_group` (`customer_group_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATALOG_PRODUCT_TIER_WEBSITE` FOREIGN KEY (`website_id`) REFERENCES `core_website` (`website_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATALOG_PROD_ENTITY_TIER_PRICE_PROD_ENTITY` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalog_product_entity_tier_price`
--

/*!40000 ALTER TABLE `catalog_product_entity_tier_price` DISABLE KEYS */;
LOCK TABLES `catalog_product_entity_tier_price` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_product_entity_tier_price` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_product_entity_varchar`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_product_entity_varchar`;
CREATE TABLE  `supernarede`.`catalog_product_entity_varchar` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT,
  `entity_type_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0',
  `value` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `IDX_ATTRIBUTE_VALUE` (`entity_id`,`attribute_id`,`store_id`),
  KEY `FK_CATALOG_PRODUCT_ENTITY_VARCHAR_ATTRIBUTE` (`attribute_id`),
  KEY `FK_CATALOG_PRODUCT_ENTITY_VARCHAR_STORE` (`store_id`),
  KEY `FK_CATALOG_PRODUCT_ENTITY_VARCHAR_PRODUCT_ENTITY` (`entity_id`),
  CONSTRAINT `FK_CATALOG_PRODUCT_ENTITY_VARCHAR_ATTRIBUTE` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATALOG_PRODUCT_ENTITY_VARCHAR_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATALOG_PROD_ENTITY_VARCHAR_PROD_ENTITY` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=110 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalog_product_entity_varchar`
--

/*!40000 ALTER TABLE `catalog_product_entity_varchar` DISABLE KEYS */;
LOCK TABLES `catalog_product_entity_varchar` WRITE;
INSERT INTO `supernarede`.`catalog_product_entity_varchar` VALUES  (1,4,60,0,1,'Cerveja Exemplo Sem alcool'),
 (2,4,123,0,1,'8,7'),
 (3,4,74,0,1,'/a/n/antarctica_lata1.jpg'),
 (4,4,75,0,1,'/a/n/antarctica_lata1.jpg'),
 (5,4,76,0,1,'/a/n/antarctica_lata1.jpg'),
 (6,4,86,0,1,'cerveja-exemplo-sem-alcool'),
 (7,4,71,0,1,''),
 (8,4,73,0,1,''),
 (9,4,96,0,1,''),
 (10,4,97,0,1,'container2'),
 (11,4,92,0,1,''),
 (12,4,110,0,1,''),
 (13,4,87,1,1,'cerveja-exemplo-sem-alcool'),
 (14,4,87,0,1,'cerveja-exemplo-sem-alcool'),
 (15,4,60,0,2,'Cerveja Exemplo Bavaria'),
 (16,4,123,0,2,'8'),
 (17,4,74,0,2,'/b/a/bavaria_lata1.jpg'),
 (18,4,75,0,2,'/b/a/bavaria_lata1.jpg'),
 (19,4,76,0,2,'/b/a/bavaria_lata1.jpg'),
 (20,4,86,0,2,'cerveja-exemplo-bavaria'),
 (21,4,71,0,2,''),
 (22,4,73,0,2,''),
 (23,4,96,0,2,''),
 (24,4,97,0,2,'container2'),
 (25,4,92,0,2,''),
 (26,4,110,0,2,''),
 (27,4,100,0,2,''),
 (28,4,101,0,2,''),
 (29,4,102,0,2,''),
 (30,4,87,1,2,'cerveja-exemplo-bavaria'),
 (31,4,87,0,2,'cerveja-exemplo-bavaria'),
 (32,4,100,0,1,''),
 (33,4,101,0,1,''),
 (34,4,102,0,1,''),
 (38,4,60,0,3,'Cerveja Exemplo Heineken Long'),
 (39,4,123,0,3,'8'),
 (40,4,74,0,3,'/h/e/heineken_ln1.jpg'),
 (41,4,75,0,3,'/h/e/heineken_ln1.jpg'),
 (42,4,76,0,3,'/h/e/heineken_ln1.jpg'),
 (43,4,86,0,3,'cerveja-exemplo-heineken-long'),
 (44,4,71,0,3,''),
 (45,4,73,0,3,''),
 (46,4,96,0,3,''),
 (47,4,97,0,3,'container2'),
 (48,4,92,0,3,''),
 (49,4,110,0,3,''),
 (50,4,100,0,3,''),
 (51,4,101,0,3,''),
 (52,4,102,0,3,''),
 (53,4,87,1,3,'cerveja-exemplo-heineken-long'),
 (54,4,87,0,3,'cerveja-exemplo-heineken-long'),
 (55,4,60,0,4,'Cerveja Exemplo Heineken Long 2'),
 (56,4,123,0,4,'8'),
 (57,4,74,0,4,'/h/e/heineken_ln1_1.jpg'),
 (58,4,75,0,4,'/h/e/heineken_ln1_1.jpg'),
 (59,4,76,0,4,'/h/e/heineken_ln1_1.jpg'),
 (60,4,86,0,4,'cerveja-exemplo-heineken-long'),
 (61,4,71,0,4,''),
 (62,4,73,0,4,''),
 (63,4,96,0,4,''),
 (64,4,97,0,4,'container2'),
 (65,4,92,0,4,''),
 (66,4,110,0,4,''),
 (67,4,100,0,4,''),
 (68,4,101,0,4,''),
 (69,4,102,0,4,''),
 (70,4,87,0,4,'cerveja-exemplo-heineken-long-1'),
 (72,4,87,1,4,'cerveja-exemplo-heineken-long-1'),
 (80,4,60,0,5,'Cerveja Exemplo Skol Escura'),
 (81,4,123,0,5,'6'),
 (82,4,74,0,5,'/s/k/skol_ln1.jpg'),
 (83,4,75,0,5,'/s/k/skol_ln1.jpg'),
 (84,4,76,0,5,'/s/k/skol_ln1.jpg'),
 (85,4,86,0,5,'cerveja-exemplo-skol-escura'),
 (86,4,71,0,5,''),
 (87,4,73,0,5,''),
 (88,4,96,0,5,''),
 (89,4,97,0,5,'container2'),
 (90,4,92,0,5,''),
 (91,4,110,0,5,''),
 (92,4,100,0,5,''),
 (93,4,101,0,5,''),
 (94,4,102,0,5,''),
 (95,4,87,1,5,'cerveja-exemplo-skol-escura'),
 (96,4,87,0,5,'cerveja-exemplo-skol-escura'),
 (97,4,60,0,6,'Cesta Alcoológica'),
 (98,4,74,0,6,'no_selection'),
 (99,4,75,0,6,'no_selection'),
 (100,4,76,0,6,'no_selection'),
 (101,4,86,0,6,'cesta-alcoologica'),
 (102,4,71,0,6,''),
 (103,4,73,0,6,''),
 (104,4,96,0,6,''),
 (105,4,97,0,6,'container2'),
 (106,4,92,0,6,''),
 (107,4,110,0,6,''),
 (108,4,87,1,6,'cesta-alcoologica'),
 (109,4,87,0,6,'cesta-alcoologica');
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_product_entity_varchar` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_product_flat_1`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_product_flat_1`;
CREATE TABLE  `supernarede`.`catalog_product_flat_1` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `attribute_set_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `type_id` varchar(32) NOT NULL DEFAULT 'simple',
  `cost` decimal(12,4) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `enable_googlecheckout` tinyint(1) DEFAULT NULL,
  `gift_message_available` tinyint(1) DEFAULT NULL,
  `has_options` smallint(6) NOT NULL DEFAULT '0',
  `image_label` varchar(255) DEFAULT NULL,
  `is_imported` tinyint(1) DEFAULT NULL,
  `is_recurring` tinyint(1) DEFAULT NULL,
  `links_exist` int(11) DEFAULT NULL,
  `links_purchased_separately` int(11) DEFAULT NULL,
  `links_title` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `news_from_date` datetime DEFAULT NULL,
  `news_to_date` datetime DEFAULT NULL,
  `price` decimal(12,4) DEFAULT NULL,
  `price_type` int(11) DEFAULT NULL,
  `price_view` int(11) DEFAULT NULL,
  `recurring_profile` text,
  `required_options` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `shipment_type` int(11) DEFAULT NULL,
  `short_description` text,
  `sku` varchar(64) DEFAULT NULL,
  `sku_type` int(11) DEFAULT NULL,
  `small_image` varchar(255) DEFAULT NULL,
  `small_image_label` varchar(255) DEFAULT NULL,
  `special_from_date` datetime DEFAULT NULL,
  `special_price` decimal(12,4) DEFAULT NULL,
  `special_to_date` datetime DEFAULT NULL,
  `tax_class_id` int(11) DEFAULT NULL,
  `thumbnail` varchar(255) DEFAULT NULL,
  `thumbnail_label` varchar(255) DEFAULT NULL,
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `url_key` varchar(255) DEFAULT NULL,
  `url_path` varchar(255) DEFAULT NULL,
  `visibility` tinyint(3) unsigned DEFAULT NULL,
  `weight` decimal(12,4) DEFAULT NULL,
  `weight_type` int(11) DEFAULT NULL,
  PRIMARY KEY (`entity_id`),
  KEY `IDX_TYPE_ID` (`type_id`),
  KEY `IDX_ATRRIBUTE_SET` (`attribute_set_id`),
  KEY `IDX_NAME` (`name`),
  KEY `IDX_PRICE` (`price`),
  CONSTRAINT `FK_CATALOG_PRODUCT_FLAT_1_ENTITY` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalog_product_flat_1`
--

/*!40000 ALTER TABLE `catalog_product_flat_1` DISABLE KEYS */;
LOCK TABLES `catalog_product_flat_1` WRITE;
INSERT INTO `supernarede`.`catalog_product_flat_1` VALUES  (1,9,'simple',NULL,'2011-08-17 12:38:40',1,0,0,'',0,0,NULL,NULL,NULL,'Cerveja Exemplo Sem alcool',NULL,NULL,'1.2900',NULL,NULL,NULL,0,NULL,'','C050505',NULL,'/a/n/antarctica_lata1.jpg','',NULL,NULL,NULL,NULL,'/a/n/antarctica_lata1.jpg','','2011-08-17 12:47:24','cerveja-exemplo-sem-alcool','cerveja-exemplo-sem-alcool',4,'0.3000',NULL),
 (2,9,'simple',NULL,'2011-08-17 12:46:20',1,0,0,'',0,0,NULL,NULL,NULL,'Cerveja Exemplo Bavaria',NULL,NULL,'1.3900',NULL,NULL,NULL,0,NULL,'','C54654654',NULL,'/b/a/bavaria_lata1.jpg','',NULL,NULL,NULL,NULL,'/b/a/bavaria_lata1.jpg','','2011-08-17 12:46:55','cerveja-exemplo-bavaria','cerveja-exemplo-bavaria',4,'0.3500',NULL),
 (3,9,'simple',NULL,'2011-08-17 12:50:17',1,0,0,'',0,0,NULL,NULL,NULL,'Cerveja Exemplo Heineken Long',NULL,NULL,'3.4900',NULL,NULL,NULL,0,NULL,'','C548748',NULL,'/h/e/heineken_ln1.jpg','',NULL,NULL,NULL,NULL,'/h/e/heineken_ln1.jpg','','2011-08-18 20:11:33','cerveja-exemplo-heineken-long','cerveja-exemplo-heineken-long',4,'0.4000',NULL),
 (4,9,'simple',NULL,'2011-08-17 12:52:39',1,0,0,'',0,0,NULL,NULL,NULL,'Cerveja Exemplo Heineken Long 2',NULL,NULL,'3.5900',NULL,NULL,NULL,0,NULL,'','C/879846',NULL,'/h/e/heineken_ln1_1.jpg','',NULL,NULL,NULL,NULL,'/h/e/heineken_ln1_1.jpg','','2011-08-19 16:28:35','cerveja-exemplo-heineken-long','cerveja-exemplo-heineken-long-1',4,'0.4000',NULL),
 (5,9,'simple',NULL,'2011-08-17 13:07:19',1,0,0,'',0,0,NULL,NULL,NULL,'Cerveja Exemplo Skol Escura',NULL,NULL,'1.9000',NULL,NULL,NULL,0,NULL,'','C7987654',NULL,'/s/k/skol_ln1.jpg','',NULL,NULL,NULL,NULL,'/s/k/skol_ln1.jpg','','2011-08-17 13:07:19','cerveja-exemplo-skol-escura','cerveja-exemplo-skol-escura',4,'0.8000',NULL),
 (6,10,'grouped',NULL,'2011-08-17 16:40:27',1,0,0,NULL,0,NULL,NULL,NULL,NULL,'Cesta Alcoológica',NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,'','CA123456',NULL,'no_selection',NULL,NULL,NULL,NULL,NULL,'no_selection',NULL,'2011-09-21 20:06:51','cesta-alcoologica','cesta-alcoologica',2,NULL,NULL);
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_product_flat_1` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_product_index_eav`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_product_index_eav`;
CREATE TABLE  `supernarede`.`catalog_product_index_eav` (
  `entity_id` int(10) unsigned NOT NULL,
  `attribute_id` smallint(5) unsigned NOT NULL,
  `store_id` smallint(5) unsigned NOT NULL,
  `value` int(10) unsigned NOT NULL,
  PRIMARY KEY (`entity_id`,`attribute_id`,`store_id`,`value`),
  KEY `IDX_ENTITY` (`entity_id`),
  KEY `IDX_ATTRIBUTE` (`attribute_id`),
  KEY `IDX_STORE` (`store_id`),
  KEY `IDX_VALUE` (`value`),
  CONSTRAINT `FK_CATALOG_PRODUCT_INDEX_EAV_ATTRIBUTE` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATALOG_PRODUCT_INDEX_EAV_ENTITY` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATALOG_PRODUCT_INDEX_EAV_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalog_product_index_eav`
--

/*!40000 ALTER TABLE `catalog_product_index_eav` DISABLE KEYS */;
LOCK TABLES `catalog_product_index_eav` WRITE;
INSERT INTO `supernarede`.`catalog_product_index_eav` VALUES  (1,122,1,3),
 (2,122,1,3),
 (3,122,1,4),
 (4,122,1,4),
 (5,122,1,4),
 (1,123,1,7),
 (1,123,1,8),
 (2,123,1,8),
 (3,123,1,8),
 (4,123,1,8),
 (5,123,1,6),
 (1,124,1,9),
 (2,124,1,10),
 (3,124,1,12),
 (4,124,1,11),
 (5,124,1,13);
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_product_index_eav` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_product_index_eav_decimal`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_product_index_eav_decimal`;
CREATE TABLE  `supernarede`.`catalog_product_index_eav_decimal` (
  `entity_id` int(10) unsigned NOT NULL,
  `attribute_id` smallint(5) unsigned NOT NULL,
  `store_id` smallint(5) unsigned NOT NULL,
  `value` decimal(12,4) NOT NULL,
  PRIMARY KEY (`entity_id`,`attribute_id`,`store_id`,`value`),
  KEY `IDX_ENTITY` (`entity_id`),
  KEY `IDX_ATTRIBUTE` (`attribute_id`),
  KEY `IDX_STORE` (`store_id`),
  KEY `IDX_VALUE` (`value`),
  CONSTRAINT `FK_CATALOG_PRODUCT_INDEX_EAV_DECIMAL_ATTRIBUTE` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATALOG_PRODUCT_INDEX_EAV_DECIMAL_ENTITY` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATALOG_PRODUCT_INDEX_EAV_DECIMAL_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalog_product_index_eav_decimal`
--

/*!40000 ALTER TABLE `catalog_product_index_eav_decimal` DISABLE KEYS */;
LOCK TABLES `catalog_product_index_eav_decimal` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_product_index_eav_decimal` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_product_index_eav_decimal_idx`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_product_index_eav_decimal_idx`;
CREATE TABLE  `supernarede`.`catalog_product_index_eav_decimal_idx` (
  `entity_id` int(10) unsigned NOT NULL,
  `attribute_id` smallint(5) unsigned NOT NULL,
  `store_id` smallint(5) unsigned NOT NULL,
  `value` decimal(12,4) NOT NULL,
  PRIMARY KEY (`entity_id`,`attribute_id`,`store_id`,`value`),
  KEY `IDX_ENTITY` (`entity_id`),
  KEY `IDX_ATTRIBUTE` (`attribute_id`),
  KEY `IDX_STORE` (`store_id`),
  KEY `IDX_VALUE` (`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalog_product_index_eav_decimal_idx`
--

/*!40000 ALTER TABLE `catalog_product_index_eav_decimal_idx` DISABLE KEYS */;
LOCK TABLES `catalog_product_index_eav_decimal_idx` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_product_index_eav_decimal_idx` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_product_index_eav_decimal_tmp`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_product_index_eav_decimal_tmp`;
CREATE TABLE  `supernarede`.`catalog_product_index_eav_decimal_tmp` (
  `entity_id` int(10) unsigned NOT NULL,
  `attribute_id` smallint(5) unsigned NOT NULL,
  `store_id` smallint(5) unsigned NOT NULL,
  `value` decimal(12,4) NOT NULL,
  PRIMARY KEY (`entity_id`,`attribute_id`,`store_id`,`value`),
  KEY `IDX_ENTITY` (`entity_id`),
  KEY `IDX_ATTRIBUTE` (`attribute_id`),
  KEY `IDX_STORE` (`store_id`),
  KEY `IDX_VALUE` (`value`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalog_product_index_eav_decimal_tmp`
--

/*!40000 ALTER TABLE `catalog_product_index_eav_decimal_tmp` DISABLE KEYS */;
LOCK TABLES `catalog_product_index_eav_decimal_tmp` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_product_index_eav_decimal_tmp` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_product_index_eav_idx`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_product_index_eav_idx`;
CREATE TABLE  `supernarede`.`catalog_product_index_eav_idx` (
  `entity_id` int(10) unsigned NOT NULL,
  `attribute_id` smallint(5) unsigned NOT NULL,
  `store_id` smallint(5) unsigned NOT NULL,
  `value` int(10) unsigned NOT NULL,
  PRIMARY KEY (`entity_id`,`attribute_id`,`store_id`,`value`),
  KEY `IDX_ENTITY` (`entity_id`),
  KEY `IDX_ATTRIBUTE` (`attribute_id`),
  KEY `IDX_STORE` (`store_id`),
  KEY `IDX_VALUE` (`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalog_product_index_eav_idx`
--

/*!40000 ALTER TABLE `catalog_product_index_eav_idx` DISABLE KEYS */;
LOCK TABLES `catalog_product_index_eav_idx` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_product_index_eav_idx` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_product_index_eav_tmp`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_product_index_eav_tmp`;
CREATE TABLE  `supernarede`.`catalog_product_index_eav_tmp` (
  `entity_id` int(10) unsigned NOT NULL,
  `attribute_id` smallint(5) unsigned NOT NULL,
  `store_id` smallint(5) unsigned NOT NULL,
  `value` int(10) unsigned NOT NULL,
  PRIMARY KEY (`entity_id`,`attribute_id`,`store_id`,`value`),
  KEY `IDX_ENTITY` (`entity_id`),
  KEY `IDX_ATTRIBUTE` (`attribute_id`),
  KEY `IDX_STORE` (`store_id`),
  KEY `IDX_VALUE` (`value`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalog_product_index_eav_tmp`
--

/*!40000 ALTER TABLE `catalog_product_index_eav_tmp` DISABLE KEYS */;
LOCK TABLES `catalog_product_index_eav_tmp` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_product_index_eav_tmp` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_product_index_price`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_product_index_price`;
CREATE TABLE  `supernarede`.`catalog_product_index_price` (
  `entity_id` int(10) unsigned NOT NULL,
  `customer_group_id` smallint(5) unsigned NOT NULL,
  `website_id` smallint(5) unsigned NOT NULL,
  `tax_class_id` smallint(5) unsigned DEFAULT '0',
  `price` decimal(12,4) DEFAULT NULL,
  `final_price` decimal(12,4) DEFAULT NULL,
  `min_price` decimal(12,4) DEFAULT NULL,
  `max_price` decimal(12,4) DEFAULT NULL,
  `tier_price` decimal(12,4) DEFAULT NULL,
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`),
  KEY `IDX_CUSTOMER_GROUP` (`customer_group_id`),
  KEY `IDX_WEBSITE` (`website_id`),
  KEY `IDX_MIN_PRICE` (`min_price`),
  CONSTRAINT `FK_CATALOG_PRODUCT_INDEX_PRICE_CUSTOMER_GROUP` FOREIGN KEY (`customer_group_id`) REFERENCES `customer_group` (`customer_group_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATALOG_PRODUCT_INDEX_PRICE_ENTITY` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATALOG_PRODUCT_INDEX_PRICE_WEBSITE` FOREIGN KEY (`website_id`) REFERENCES `core_website` (`website_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalog_product_index_price`
--

/*!40000 ALTER TABLE `catalog_product_index_price` DISABLE KEYS */;
LOCK TABLES `catalog_product_index_price` WRITE;
INSERT INTO `supernarede`.`catalog_product_index_price` VALUES  (1,0,1,NULL,'1.2900','1.2900','1.2900','1.2900',NULL),
 (1,1,1,NULL,'1.2900','1.2900','1.2900','1.2900',NULL),
 (1,2,1,NULL,'1.2900','1.2900','1.2900','1.2900',NULL),
 (1,3,1,NULL,'1.2900','1.2900','1.2900','1.2900',NULL),
 (2,0,1,NULL,'1.3900','1.3900','1.3900','1.3900',NULL),
 (2,1,1,NULL,'1.3900','1.3900','1.3900','1.3900',NULL),
 (2,2,1,NULL,'1.3900','1.3900','1.3900','1.3900',NULL),
 (2,3,1,NULL,'1.3900','1.3900','1.3900','1.3900',NULL),
 (3,0,1,NULL,'3.4900','3.4900','3.4900','3.4900',NULL),
 (3,1,1,NULL,'3.4900','3.4900','3.4900','3.4900',NULL),
 (3,2,1,NULL,'3.4900','3.4900','3.4900','3.4900',NULL),
 (3,3,1,NULL,'3.4900','3.4900','3.4900','3.4900',NULL),
 (4,0,1,NULL,'3.5900','3.5900','3.5900','3.5900',NULL),
 (4,1,1,NULL,'3.5900','3.5900','3.5900','3.5900',NULL),
 (4,2,1,NULL,'3.5900','3.5900','3.5900','3.5900',NULL),
 (4,3,1,NULL,'3.5900','3.5900','3.5900','3.5900',NULL),
 (5,0,1,NULL,'1.9000','1.9000','1.9000','1.9000',NULL),
 (5,1,1,NULL,'1.9000','1.9000','1.9000','1.9000',NULL),
 (5,2,1,NULL,'1.9000','1.9000','1.9000','1.9000',NULL),
 (5,3,1,NULL,'1.9000','1.9000','1.9000','1.9000',NULL),
 (6,0,1,0,NULL,NULL,'1.2900','3.4900',NULL),
 (6,1,1,0,NULL,NULL,'1.2900','3.4900',NULL),
 (6,2,1,0,NULL,NULL,'1.2900','3.4900',NULL),
 (6,3,1,0,NULL,NULL,'1.2900','3.4900',NULL);
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_product_index_price` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_product_index_price_bundle_idx`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_product_index_price_bundle_idx`;
CREATE TABLE  `supernarede`.`catalog_product_index_price_bundle_idx` (
  `entity_id` int(10) unsigned NOT NULL,
  `customer_group_id` smallint(5) unsigned NOT NULL,
  `website_id` smallint(5) unsigned NOT NULL,
  `tax_class_id` smallint(5) unsigned DEFAULT '0',
  `price_type` tinyint(1) unsigned NOT NULL,
  `special_price` decimal(12,4) DEFAULT NULL,
  `tier_percent` decimal(12,4) DEFAULT NULL,
  `orig_price` decimal(12,4) DEFAULT NULL,
  `price` decimal(12,4) DEFAULT NULL,
  `min_price` decimal(12,4) DEFAULT NULL,
  `max_price` decimal(12,4) DEFAULT NULL,
  `tier_price` decimal(12,4) DEFAULT NULL,
  `base_tier` decimal(12,4) DEFAULT NULL,
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalog_product_index_price_bundle_idx`
--

/*!40000 ALTER TABLE `catalog_product_index_price_bundle_idx` DISABLE KEYS */;
LOCK TABLES `catalog_product_index_price_bundle_idx` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_product_index_price_bundle_idx` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_product_index_price_bundle_opt_idx`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_product_index_price_bundle_opt_idx`;
CREATE TABLE  `supernarede`.`catalog_product_index_price_bundle_opt_idx` (
  `entity_id` int(10) unsigned NOT NULL,
  `customer_group_id` smallint(5) unsigned NOT NULL,
  `website_id` smallint(5) unsigned NOT NULL,
  `option_id` int(10) unsigned NOT NULL DEFAULT '0',
  `min_price` decimal(12,4) DEFAULT NULL,
  `alt_price` decimal(12,4) DEFAULT NULL,
  `max_price` decimal(12,4) DEFAULT NULL,
  `tier_price` decimal(12,4) DEFAULT NULL,
  `alt_tier_price` decimal(12,4) DEFAULT NULL,
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`,`option_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalog_product_index_price_bundle_opt_idx`
--

/*!40000 ALTER TABLE `catalog_product_index_price_bundle_opt_idx` DISABLE KEYS */;
LOCK TABLES `catalog_product_index_price_bundle_opt_idx` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_product_index_price_bundle_opt_idx` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_product_index_price_bundle_opt_tmp`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_product_index_price_bundle_opt_tmp`;
CREATE TABLE  `supernarede`.`catalog_product_index_price_bundle_opt_tmp` (
  `entity_id` int(10) unsigned NOT NULL,
  `customer_group_id` smallint(5) unsigned NOT NULL,
  `website_id` smallint(5) unsigned NOT NULL,
  `option_id` int(10) unsigned NOT NULL DEFAULT '0',
  `min_price` decimal(12,4) DEFAULT NULL,
  `alt_price` decimal(12,4) DEFAULT NULL,
  `max_price` decimal(12,4) DEFAULT NULL,
  `tier_price` decimal(12,4) DEFAULT NULL,
  `alt_tier_price` decimal(12,4) DEFAULT NULL,
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`,`option_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalog_product_index_price_bundle_opt_tmp`
--

/*!40000 ALTER TABLE `catalog_product_index_price_bundle_opt_tmp` DISABLE KEYS */;
LOCK TABLES `catalog_product_index_price_bundle_opt_tmp` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_product_index_price_bundle_opt_tmp` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_product_index_price_bundle_sel_idx`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_product_index_price_bundle_sel_idx`;
CREATE TABLE  `supernarede`.`catalog_product_index_price_bundle_sel_idx` (
  `entity_id` int(10) unsigned NOT NULL,
  `customer_group_id` smallint(5) unsigned NOT NULL,
  `website_id` smallint(5) unsigned NOT NULL,
  `option_id` int(10) unsigned NOT NULL DEFAULT '0',
  `selection_id` int(10) unsigned NOT NULL DEFAULT '0',
  `group_type` tinyint(1) unsigned DEFAULT '0',
  `is_required` tinyint(1) unsigned DEFAULT '0',
  `price` decimal(12,4) DEFAULT NULL,
  `tier_price` decimal(12,4) DEFAULT NULL,
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`,`option_id`,`selection_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalog_product_index_price_bundle_sel_idx`
--

/*!40000 ALTER TABLE `catalog_product_index_price_bundle_sel_idx` DISABLE KEYS */;
LOCK TABLES `catalog_product_index_price_bundle_sel_idx` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_product_index_price_bundle_sel_idx` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_product_index_price_bundle_sel_tmp`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_product_index_price_bundle_sel_tmp`;
CREATE TABLE  `supernarede`.`catalog_product_index_price_bundle_sel_tmp` (
  `entity_id` int(10) unsigned NOT NULL,
  `customer_group_id` smallint(5) unsigned NOT NULL,
  `website_id` smallint(5) unsigned NOT NULL,
  `option_id` int(10) unsigned NOT NULL DEFAULT '0',
  `selection_id` int(10) unsigned NOT NULL DEFAULT '0',
  `group_type` tinyint(1) unsigned DEFAULT '0',
  `is_required` tinyint(1) unsigned DEFAULT '0',
  `price` decimal(12,4) DEFAULT NULL,
  `tier_price` decimal(12,4) DEFAULT NULL,
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`,`option_id`,`selection_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalog_product_index_price_bundle_sel_tmp`
--

/*!40000 ALTER TABLE `catalog_product_index_price_bundle_sel_tmp` DISABLE KEYS */;
LOCK TABLES `catalog_product_index_price_bundle_sel_tmp` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_product_index_price_bundle_sel_tmp` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_product_index_price_bundle_tmp`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_product_index_price_bundle_tmp`;
CREATE TABLE  `supernarede`.`catalog_product_index_price_bundle_tmp` (
  `entity_id` int(10) unsigned NOT NULL,
  `customer_group_id` smallint(5) unsigned NOT NULL,
  `website_id` smallint(5) unsigned NOT NULL,
  `tax_class_id` smallint(5) unsigned DEFAULT '0',
  `price_type` tinyint(1) unsigned NOT NULL,
  `special_price` decimal(12,4) DEFAULT NULL,
  `tier_percent` decimal(12,4) DEFAULT NULL,
  `orig_price` decimal(12,4) DEFAULT NULL,
  `price` decimal(12,4) DEFAULT NULL,
  `min_price` decimal(12,4) DEFAULT NULL,
  `max_price` decimal(12,4) DEFAULT NULL,
  `tier_price` decimal(12,4) DEFAULT NULL,
  `base_tier` decimal(12,4) DEFAULT NULL,
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalog_product_index_price_bundle_tmp`
--

/*!40000 ALTER TABLE `catalog_product_index_price_bundle_tmp` DISABLE KEYS */;
LOCK TABLES `catalog_product_index_price_bundle_tmp` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_product_index_price_bundle_tmp` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_product_index_price_cfg_opt_agr_idx`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_product_index_price_cfg_opt_agr_idx`;
CREATE TABLE  `supernarede`.`catalog_product_index_price_cfg_opt_agr_idx` (
  `parent_id` int(10) unsigned NOT NULL,
  `child_id` int(10) unsigned NOT NULL,
  `customer_group_id` smallint(5) unsigned NOT NULL,
  `website_id` smallint(5) unsigned NOT NULL,
  `price` decimal(12,4) DEFAULT NULL,
  `tier_price` decimal(12,4) DEFAULT NULL,
  PRIMARY KEY (`parent_id`,`child_id`,`customer_group_id`,`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalog_product_index_price_cfg_opt_agr_idx`
--

/*!40000 ALTER TABLE `catalog_product_index_price_cfg_opt_agr_idx` DISABLE KEYS */;
LOCK TABLES `catalog_product_index_price_cfg_opt_agr_idx` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_product_index_price_cfg_opt_agr_idx` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_product_index_price_cfg_opt_agr_tmp`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_product_index_price_cfg_opt_agr_tmp`;
CREATE TABLE  `supernarede`.`catalog_product_index_price_cfg_opt_agr_tmp` (
  `parent_id` int(10) unsigned NOT NULL,
  `child_id` int(10) unsigned NOT NULL,
  `customer_group_id` smallint(5) unsigned NOT NULL,
  `website_id` smallint(5) unsigned NOT NULL,
  `price` decimal(12,4) DEFAULT NULL,
  `tier_price` decimal(12,4) DEFAULT NULL,
  PRIMARY KEY (`parent_id`,`child_id`,`customer_group_id`,`website_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalog_product_index_price_cfg_opt_agr_tmp`
--

/*!40000 ALTER TABLE `catalog_product_index_price_cfg_opt_agr_tmp` DISABLE KEYS */;
LOCK TABLES `catalog_product_index_price_cfg_opt_agr_tmp` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_product_index_price_cfg_opt_agr_tmp` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_product_index_price_cfg_opt_idx`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_product_index_price_cfg_opt_idx`;
CREATE TABLE  `supernarede`.`catalog_product_index_price_cfg_opt_idx` (
  `entity_id` int(10) unsigned NOT NULL,
  `customer_group_id` smallint(5) unsigned NOT NULL,
  `website_id` smallint(5) unsigned NOT NULL,
  `min_price` decimal(12,4) DEFAULT NULL,
  `max_price` decimal(12,4) DEFAULT NULL,
  `tier_price` decimal(12,4) DEFAULT NULL,
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalog_product_index_price_cfg_opt_idx`
--

/*!40000 ALTER TABLE `catalog_product_index_price_cfg_opt_idx` DISABLE KEYS */;
LOCK TABLES `catalog_product_index_price_cfg_opt_idx` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_product_index_price_cfg_opt_idx` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_product_index_price_cfg_opt_tmp`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_product_index_price_cfg_opt_tmp`;
CREATE TABLE  `supernarede`.`catalog_product_index_price_cfg_opt_tmp` (
  `entity_id` int(10) unsigned NOT NULL,
  `customer_group_id` smallint(5) unsigned NOT NULL,
  `website_id` smallint(5) unsigned NOT NULL,
  `min_price` decimal(12,4) DEFAULT NULL,
  `max_price` decimal(12,4) DEFAULT NULL,
  `tier_price` decimal(12,4) DEFAULT NULL,
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalog_product_index_price_cfg_opt_tmp`
--

/*!40000 ALTER TABLE `catalog_product_index_price_cfg_opt_tmp` DISABLE KEYS */;
LOCK TABLES `catalog_product_index_price_cfg_opt_tmp` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_product_index_price_cfg_opt_tmp` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_product_index_price_downlod_idx`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_product_index_price_downlod_idx`;
CREATE TABLE  `supernarede`.`catalog_product_index_price_downlod_idx` (
  `entity_id` int(10) unsigned NOT NULL,
  `customer_group_id` smallint(5) unsigned NOT NULL,
  `website_id` smallint(5) unsigned NOT NULL,
  `min_price` decimal(12,4) DEFAULT NULL,
  `max_price` decimal(12,4) DEFAULT NULL,
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalog_product_index_price_downlod_idx`
--

/*!40000 ALTER TABLE `catalog_product_index_price_downlod_idx` DISABLE KEYS */;
LOCK TABLES `catalog_product_index_price_downlod_idx` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_product_index_price_downlod_idx` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_product_index_price_downlod_tmp`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_product_index_price_downlod_tmp`;
CREATE TABLE  `supernarede`.`catalog_product_index_price_downlod_tmp` (
  `entity_id` int(10) unsigned NOT NULL,
  `customer_group_id` smallint(5) unsigned NOT NULL,
  `website_id` smallint(5) unsigned NOT NULL,
  `min_price` decimal(12,4) DEFAULT NULL,
  `max_price` decimal(12,4) DEFAULT NULL,
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalog_product_index_price_downlod_tmp`
--

/*!40000 ALTER TABLE `catalog_product_index_price_downlod_tmp` DISABLE KEYS */;
LOCK TABLES `catalog_product_index_price_downlod_tmp` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_product_index_price_downlod_tmp` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_product_index_price_final_idx`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_product_index_price_final_idx`;
CREATE TABLE  `supernarede`.`catalog_product_index_price_final_idx` (
  `entity_id` int(10) unsigned NOT NULL,
  `customer_group_id` smallint(5) unsigned NOT NULL,
  `website_id` smallint(5) unsigned NOT NULL,
  `tax_class_id` smallint(5) unsigned DEFAULT '0',
  `orig_price` decimal(12,4) DEFAULT NULL,
  `price` decimal(12,4) DEFAULT NULL,
  `min_price` decimal(12,4) DEFAULT NULL,
  `max_price` decimal(12,4) DEFAULT NULL,
  `tier_price` decimal(12,4) DEFAULT NULL,
  `base_tier` decimal(12,4) DEFAULT NULL,
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalog_product_index_price_final_idx`
--

/*!40000 ALTER TABLE `catalog_product_index_price_final_idx` DISABLE KEYS */;
LOCK TABLES `catalog_product_index_price_final_idx` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_product_index_price_final_idx` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_product_index_price_final_tmp`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_product_index_price_final_tmp`;
CREATE TABLE  `supernarede`.`catalog_product_index_price_final_tmp` (
  `entity_id` int(10) unsigned NOT NULL,
  `customer_group_id` smallint(5) unsigned NOT NULL,
  `website_id` smallint(5) unsigned NOT NULL,
  `tax_class_id` smallint(5) unsigned DEFAULT '0',
  `orig_price` decimal(12,4) DEFAULT NULL,
  `price` decimal(12,4) DEFAULT NULL,
  `min_price` decimal(12,4) DEFAULT NULL,
  `max_price` decimal(12,4) DEFAULT NULL,
  `tier_price` decimal(12,4) DEFAULT NULL,
  `base_tier` decimal(12,4) DEFAULT NULL,
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalog_product_index_price_final_tmp`
--

/*!40000 ALTER TABLE `catalog_product_index_price_final_tmp` DISABLE KEYS */;
LOCK TABLES `catalog_product_index_price_final_tmp` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_product_index_price_final_tmp` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_product_index_price_idx`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_product_index_price_idx`;
CREATE TABLE  `supernarede`.`catalog_product_index_price_idx` (
  `entity_id` int(10) unsigned NOT NULL,
  `customer_group_id` smallint(5) unsigned NOT NULL,
  `website_id` smallint(5) unsigned NOT NULL,
  `tax_class_id` smallint(5) unsigned DEFAULT '0',
  `price` decimal(12,4) DEFAULT NULL,
  `final_price` decimal(12,4) DEFAULT NULL,
  `min_price` decimal(12,4) DEFAULT NULL,
  `max_price` decimal(12,4) DEFAULT NULL,
  `tier_price` decimal(12,4) DEFAULT NULL,
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`),
  KEY `IDX_CUSTOMER_GROUP` (`customer_group_id`),
  KEY `IDX_WEBSITE` (`website_id`),
  KEY `IDX_MIN_PRICE` (`min_price`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalog_product_index_price_idx`
--

/*!40000 ALTER TABLE `catalog_product_index_price_idx` DISABLE KEYS */;
LOCK TABLES `catalog_product_index_price_idx` WRITE;
INSERT INTO `supernarede`.`catalog_product_index_price_idx` VALUES  (1,0,1,NULL,'1.2900','1.2900','1.2900','1.2900',NULL),
 (1,1,1,NULL,'1.2900','1.2900','1.2900','1.2900',NULL),
 (1,2,1,NULL,'1.2900','1.2900','1.2900','1.2900',NULL),
 (1,3,1,NULL,'1.2900','1.2900','1.2900','1.2900',NULL),
 (2,0,1,NULL,'1.3900','1.3900','1.3900','1.3900',NULL),
 (2,1,1,NULL,'1.3900','1.3900','1.3900','1.3900',NULL),
 (2,2,1,NULL,'1.3900','1.3900','1.3900','1.3900',NULL),
 (2,3,1,NULL,'1.3900','1.3900','1.3900','1.3900',NULL),
 (3,0,1,NULL,'3.4900','3.4900','3.4900','3.4900',NULL),
 (3,1,1,NULL,'3.4900','3.4900','3.4900','3.4900',NULL),
 (3,2,1,NULL,'3.4900','3.4900','3.4900','3.4900',NULL),
 (3,3,1,NULL,'3.4900','3.4900','3.4900','3.4900',NULL),
 (4,0,1,NULL,'3.5900','3.5900','3.5900','3.5900',NULL),
 (4,1,1,NULL,'3.5900','3.5900','3.5900','3.5900',NULL),
 (4,2,1,NULL,'3.5900','3.5900','3.5900','3.5900',NULL),
 (4,3,1,NULL,'3.5900','3.5900','3.5900','3.5900',NULL),
 (5,0,1,NULL,'1.9000','1.9000','1.9000','1.9000',NULL),
 (5,1,1,NULL,'1.9000','1.9000','1.9000','1.9000',NULL),
 (5,2,1,NULL,'1.9000','1.9000','1.9000','1.9000',NULL),
 (5,3,1,NULL,'1.9000','1.9000','1.9000','1.9000',NULL),
 (6,0,1,0,NULL,NULL,'1.2900','3.4900',NULL),
 (6,1,1,0,NULL,NULL,'1.2900','3.4900',NULL),
 (6,2,1,0,NULL,NULL,'1.2900','3.4900',NULL),
 (6,3,1,0,NULL,NULL,'1.2900','3.4900',NULL);
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_product_index_price_idx` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_product_index_price_opt_agr_idx`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_product_index_price_opt_agr_idx`;
CREATE TABLE  `supernarede`.`catalog_product_index_price_opt_agr_idx` (
  `entity_id` int(10) unsigned NOT NULL,
  `customer_group_id` smallint(5) unsigned NOT NULL,
  `website_id` smallint(5) unsigned NOT NULL,
  `option_id` int(10) unsigned NOT NULL DEFAULT '0',
  `min_price` decimal(12,4) DEFAULT NULL,
  `max_price` decimal(12,4) DEFAULT NULL,
  `tier_price` decimal(12,4) DEFAULT NULL,
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`,`option_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalog_product_index_price_opt_agr_idx`
--

/*!40000 ALTER TABLE `catalog_product_index_price_opt_agr_idx` DISABLE KEYS */;
LOCK TABLES `catalog_product_index_price_opt_agr_idx` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_product_index_price_opt_agr_idx` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_product_index_price_opt_agr_tmp`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_product_index_price_opt_agr_tmp`;
CREATE TABLE  `supernarede`.`catalog_product_index_price_opt_agr_tmp` (
  `entity_id` int(10) unsigned NOT NULL,
  `customer_group_id` smallint(5) unsigned NOT NULL,
  `website_id` smallint(5) unsigned NOT NULL,
  `option_id` int(10) unsigned NOT NULL DEFAULT '0',
  `min_price` decimal(12,4) DEFAULT NULL,
  `max_price` decimal(12,4) DEFAULT NULL,
  `tier_price` decimal(12,4) DEFAULT NULL,
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`,`option_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalog_product_index_price_opt_agr_tmp`
--

/*!40000 ALTER TABLE `catalog_product_index_price_opt_agr_tmp` DISABLE KEYS */;
LOCK TABLES `catalog_product_index_price_opt_agr_tmp` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_product_index_price_opt_agr_tmp` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_product_index_price_opt_idx`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_product_index_price_opt_idx`;
CREATE TABLE  `supernarede`.`catalog_product_index_price_opt_idx` (
  `entity_id` int(10) unsigned NOT NULL,
  `customer_group_id` smallint(5) unsigned NOT NULL,
  `website_id` smallint(5) unsigned NOT NULL,
  `min_price` decimal(12,4) DEFAULT NULL,
  `max_price` decimal(12,4) DEFAULT NULL,
  `tier_price` decimal(12,4) DEFAULT NULL,
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalog_product_index_price_opt_idx`
--

/*!40000 ALTER TABLE `catalog_product_index_price_opt_idx` DISABLE KEYS */;
LOCK TABLES `catalog_product_index_price_opt_idx` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_product_index_price_opt_idx` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_product_index_price_opt_tmp`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_product_index_price_opt_tmp`;
CREATE TABLE  `supernarede`.`catalog_product_index_price_opt_tmp` (
  `entity_id` int(10) unsigned NOT NULL,
  `customer_group_id` smallint(5) unsigned NOT NULL,
  `website_id` smallint(5) unsigned NOT NULL,
  `min_price` decimal(12,4) DEFAULT NULL,
  `max_price` decimal(12,4) DEFAULT NULL,
  `tier_price` decimal(12,4) DEFAULT NULL,
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalog_product_index_price_opt_tmp`
--

/*!40000 ALTER TABLE `catalog_product_index_price_opt_tmp` DISABLE KEYS */;
LOCK TABLES `catalog_product_index_price_opt_tmp` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_product_index_price_opt_tmp` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_product_index_price_tmp`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_product_index_price_tmp`;
CREATE TABLE  `supernarede`.`catalog_product_index_price_tmp` (
  `entity_id` int(10) unsigned NOT NULL,
  `customer_group_id` smallint(5) unsigned NOT NULL,
  `website_id` smallint(5) unsigned NOT NULL,
  `tax_class_id` smallint(5) unsigned DEFAULT '0',
  `price` decimal(12,4) DEFAULT NULL,
  `final_price` decimal(12,4) DEFAULT NULL,
  `min_price` decimal(12,4) DEFAULT NULL,
  `max_price` decimal(12,4) DEFAULT NULL,
  `tier_price` decimal(12,4) DEFAULT NULL,
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`),
  KEY `IDX_CUSTOMER_GROUP` (`customer_group_id`),
  KEY `IDX_WEBSITE` (`website_id`),
  KEY `IDX_MIN_PRICE` (`min_price`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalog_product_index_price_tmp`
--

/*!40000 ALTER TABLE `catalog_product_index_price_tmp` DISABLE KEYS */;
LOCK TABLES `catalog_product_index_price_tmp` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_product_index_price_tmp` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_product_index_tier_price`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_product_index_tier_price`;
CREATE TABLE  `supernarede`.`catalog_product_index_tier_price` (
  `entity_id` int(10) unsigned NOT NULL,
  `customer_group_id` smallint(5) unsigned NOT NULL,
  `website_id` smallint(5) unsigned NOT NULL,
  `min_price` decimal(12,4) DEFAULT NULL,
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`),
  KEY `FK_CATALOG_PRODUCT_INDEX_TIER_PRICE_CUSTOMER` (`customer_group_id`),
  KEY `FK_CATALOG_PRODUCT_INDEX_TIER_PRICE_WEBSITE` (`website_id`),
  CONSTRAINT `FK_CATALOG_PRODUCT_INDEX_TIER_PRICE_CUSTOMER` FOREIGN KEY (`customer_group_id`) REFERENCES `customer_group` (`customer_group_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATALOG_PRODUCT_INDEX_TIER_PRICE_ENTITY` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATALOG_PRODUCT_INDEX_TIER_PRICE_WEBSITE` FOREIGN KEY (`website_id`) REFERENCES `core_website` (`website_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalog_product_index_tier_price`
--

/*!40000 ALTER TABLE `catalog_product_index_tier_price` DISABLE KEYS */;
LOCK TABLES `catalog_product_index_tier_price` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_product_index_tier_price` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_product_index_website`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_product_index_website`;
CREATE TABLE  `supernarede`.`catalog_product_index_website` (
  `website_id` smallint(5) unsigned NOT NULL,
  `date` date DEFAULT NULL,
  `rate` float(12,4) unsigned DEFAULT '1.0000',
  PRIMARY KEY (`website_id`),
  KEY `IDX_DATE` (`date`),
  CONSTRAINT `FK_CATALOG_PRODUCT_INDEX_WEBSITE` FOREIGN KEY (`website_id`) REFERENCES `core_website` (`website_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalog_product_index_website`
--

/*!40000 ALTER TABLE `catalog_product_index_website` DISABLE KEYS */;
LOCK TABLES `catalog_product_index_website` WRITE;
INSERT INTO `supernarede`.`catalog_product_index_website` VALUES  (1,'2011-09-21',1.0000);
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_product_index_website` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_product_link`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_product_link`;
CREATE TABLE  `supernarede`.`catalog_product_link` (
  `link_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(10) unsigned NOT NULL DEFAULT '0',
  `linked_product_id` int(10) unsigned NOT NULL DEFAULT '0',
  `link_type_id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`link_id`),
  UNIQUE KEY `IDX_UNIQUE` (`link_type_id`,`product_id`,`linked_product_id`),
  KEY `FK_LINK_PRODUCT` (`product_id`),
  KEY `FK_LINKED_PRODUCT` (`linked_product_id`),
  KEY `FK_PRODUCT_LINK_TYPE` (`link_type_id`),
  CONSTRAINT `FK_PRODUCT_LINK_LINKED_PRODUCT` FOREIGN KEY (`linked_product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_PRODUCT_LINK_PRODUCT` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_PRODUCT_LINK_TYPE` FOREIGN KEY (`link_type_id`) REFERENCES `catalog_product_link_type` (`link_type_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Related products';

--
-- Dumping data for table `supernarede`.`catalog_product_link`
--

/*!40000 ALTER TABLE `catalog_product_link` DISABLE KEYS */;
LOCK TABLES `catalog_product_link` WRITE;
INSERT INTO `supernarede`.`catalog_product_link` VALUES  (1,6,1,3),
 (2,6,2,3),
 (3,6,3,3);
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_product_link` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_product_link_attribute`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_product_link_attribute`;
CREATE TABLE  `supernarede`.`catalog_product_link_attribute` (
  `product_link_attribute_id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `link_type_id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `product_link_attribute_code` varchar(32) NOT NULL DEFAULT '',
  `data_type` varchar(32) NOT NULL DEFAULT '',
  PRIMARY KEY (`product_link_attribute_id`),
  KEY `FK_ATTRIBUTE_PRODUCT_LINK_TYPE` (`link_type_id`),
  CONSTRAINT `FK_ATTRIBUTE_PRODUCT_LINK_TYPE` FOREIGN KEY (`link_type_id`) REFERENCES `catalog_product_link_type` (`link_type_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='Attributes for product link';

--
-- Dumping data for table `supernarede`.`catalog_product_link_attribute`
--

/*!40000 ALTER TABLE `catalog_product_link_attribute` DISABLE KEYS */;
LOCK TABLES `catalog_product_link_attribute` WRITE;
INSERT INTO `supernarede`.`catalog_product_link_attribute` VALUES  (1,2,'qty','decimal'),
 (2,1,'position','int'),
 (3,4,'position','int'),
 (4,5,'position','int'),
 (6,1,'qty','decimal'),
 (7,3,'position','int'),
 (8,3,'qty','decimal');
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_product_link_attribute` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_product_link_attribute_decimal`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_product_link_attribute_decimal`;
CREATE TABLE  `supernarede`.`catalog_product_link_attribute_decimal` (
  `value_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `product_link_attribute_id` smallint(6) unsigned DEFAULT NULL,
  `link_id` int(11) unsigned DEFAULT NULL,
  `value` decimal(12,4) NOT NULL DEFAULT '0.0000',
  PRIMARY KEY (`value_id`),
  KEY `FK_DECIMAL_PRODUCT_LINK_ATTRIBUTE` (`product_link_attribute_id`),
  KEY `FK_DECIMAL_LINK` (`link_id`),
  CONSTRAINT `FK_DECIMAL_LINK` FOREIGN KEY (`link_id`) REFERENCES `catalog_product_link` (`link_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_DECIMAL_PRODUCT_LINK_ATTRIBUTE` FOREIGN KEY (`product_link_attribute_id`) REFERENCES `catalog_product_link_attribute` (`product_link_attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Decimal attributes values';

--
-- Dumping data for table `supernarede`.`catalog_product_link_attribute_decimal`
--

/*!40000 ALTER TABLE `catalog_product_link_attribute_decimal` DISABLE KEYS */;
LOCK TABLES `catalog_product_link_attribute_decimal` WRITE;
INSERT INTO `supernarede`.`catalog_product_link_attribute_decimal` VALUES  (1,8,1,'0.0000'),
 (2,8,2,'0.0000'),
 (3,8,3,'0.0000');
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_product_link_attribute_decimal` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_product_link_attribute_int`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_product_link_attribute_int`;
CREATE TABLE  `supernarede`.`catalog_product_link_attribute_int` (
  `value_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `product_link_attribute_id` smallint(6) unsigned DEFAULT NULL,
  `link_id` int(11) unsigned DEFAULT NULL,
  `value` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `UNQ_product_link_attribute_id_link_id` (`product_link_attribute_id`,`link_id`),
  KEY `FK_INT_PRODUCT_LINK_ATTRIBUTE` (`product_link_attribute_id`),
  KEY `FK_INT_PRODUCT_LINK` (`link_id`),
  CONSTRAINT `FK_INT_PRODUCT_LINK` FOREIGN KEY (`link_id`) REFERENCES `catalog_product_link` (`link_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_INT_PRODUCT_LINK_ATTRIBUTE` FOREIGN KEY (`product_link_attribute_id`) REFERENCES `catalog_product_link_attribute` (`product_link_attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalog_product_link_attribute_int`
--

/*!40000 ALTER TABLE `catalog_product_link_attribute_int` DISABLE KEYS */;
LOCK TABLES `catalog_product_link_attribute_int` WRITE;
INSERT INTO `supernarede`.`catalog_product_link_attribute_int` VALUES  (1,7,1,0),
 (2,7,2,0),
 (3,7,3,0);
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_product_link_attribute_int` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_product_link_attribute_varchar`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_product_link_attribute_varchar`;
CREATE TABLE  `supernarede`.`catalog_product_link_attribute_varchar` (
  `value_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `product_link_attribute_id` smallint(6) unsigned NOT NULL DEFAULT '0',
  `link_id` int(11) unsigned DEFAULT NULL,
  `value` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`value_id`),
  KEY `FK_VARCHAR_PRODUCT_LINK_ATTRIBUTE` (`product_link_attribute_id`),
  KEY `FK_VARCHAR_LINK` (`link_id`),
  CONSTRAINT `FK_VARCHAR_LINK` FOREIGN KEY (`link_id`) REFERENCES `catalog_product_link` (`link_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_VARCHAR_PRODUCT_LINK_ATTRIBUTE` FOREIGN KEY (`product_link_attribute_id`) REFERENCES `catalog_product_link_attribute` (`product_link_attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Varchar attributes values';

--
-- Dumping data for table `supernarede`.`catalog_product_link_attribute_varchar`
--

/*!40000 ALTER TABLE `catalog_product_link_attribute_varchar` DISABLE KEYS */;
LOCK TABLES `catalog_product_link_attribute_varchar` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_product_link_attribute_varchar` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_product_link_type`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_product_link_type`;
CREATE TABLE  `supernarede`.`catalog_product_link_type` (
  `link_type_id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(32) NOT NULL DEFAULT '',
  PRIMARY KEY (`link_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='Types of product link(Related, superproduct, bundles)';

--
-- Dumping data for table `supernarede`.`catalog_product_link_type`
--

/*!40000 ALTER TABLE `catalog_product_link_type` DISABLE KEYS */;
LOCK TABLES `catalog_product_link_type` WRITE;
INSERT INTO `supernarede`.`catalog_product_link_type` VALUES  (1,'relation'),
 (2,'bundle'),
 (3,'super'),
 (4,'up_sell'),
 (5,'cross_sell');
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_product_link_type` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_product_option`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_product_option`;
CREATE TABLE  `supernarede`.`catalog_product_option` (
  `option_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(10) unsigned NOT NULL DEFAULT '0',
  `type` varchar(50) NOT NULL DEFAULT '',
  `is_require` tinyint(1) NOT NULL DEFAULT '1',
  `sku` varchar(64) NOT NULL DEFAULT '',
  `max_characters` int(10) unsigned DEFAULT NULL,
  `file_extension` varchar(50) DEFAULT NULL,
  `image_size_x` smallint(5) unsigned NOT NULL,
  `image_size_y` smallint(5) unsigned NOT NULL,
  `sort_order` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`option_id`),
  KEY `CATALOG_PRODUCT_OPTION_PRODUCT` (`product_id`),
  CONSTRAINT `FK_CATALOG_PRODUCT_OPTION_PRODUCT` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalog_product_option`
--

/*!40000 ALTER TABLE `catalog_product_option` DISABLE KEYS */;
LOCK TABLES `catalog_product_option` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_product_option` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_product_option_price`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_product_option_price`;
CREATE TABLE  `supernarede`.`catalog_product_option_price` (
  `option_price_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `option_id` int(10) unsigned NOT NULL DEFAULT '0',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `price` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `price_type` enum('fixed','percent') NOT NULL DEFAULT 'fixed',
  PRIMARY KEY (`option_price_id`),
  UNIQUE KEY `UNQ_OPTION_STORE` (`option_id`,`store_id`),
  KEY `CATALOG_PRODUCT_OPTION_PRICE_OPTION` (`option_id`),
  KEY `CATALOG_PRODUCT_OPTION_TITLE_STORE` (`store_id`),
  CONSTRAINT `FK_CATALOG_PRODUCT_OPTION_PRICE_OPTION` FOREIGN KEY (`option_id`) REFERENCES `catalog_product_option` (`option_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATALOG_PRODUCT_OPTION_PRICE_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalog_product_option_price`
--

/*!40000 ALTER TABLE `catalog_product_option_price` DISABLE KEYS */;
LOCK TABLES `catalog_product_option_price` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_product_option_price` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_product_option_title`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_product_option_title`;
CREATE TABLE  `supernarede`.`catalog_product_option_title` (
  `option_title_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `option_id` int(10) unsigned NOT NULL DEFAULT '0',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`option_title_id`),
  UNIQUE KEY `UNQ_OPTION_STORE` (`option_id`,`store_id`),
  KEY `CATALOG_PRODUCT_OPTION_TITLE_OPTION` (`option_id`),
  KEY `CATALOG_PRODUCT_OPTION_TITLE_STORE` (`store_id`),
  CONSTRAINT `FK_CATALOG_PRODUCT_OPTION_TITLE_OPTION` FOREIGN KEY (`option_id`) REFERENCES `catalog_product_option` (`option_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATALOG_PRODUCT_OPTION_TITLE_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalog_product_option_title`
--

/*!40000 ALTER TABLE `catalog_product_option_title` DISABLE KEYS */;
LOCK TABLES `catalog_product_option_title` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_product_option_title` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_product_option_type_price`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_product_option_type_price`;
CREATE TABLE  `supernarede`.`catalog_product_option_type_price` (
  `option_type_price_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `option_type_id` int(10) unsigned NOT NULL DEFAULT '0',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `price` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `price_type` enum('fixed','percent') NOT NULL DEFAULT 'fixed',
  PRIMARY KEY (`option_type_price_id`),
  UNIQUE KEY `UNQ_OPTION_TYPE_STORE` (`option_type_id`,`store_id`),
  KEY `CATALOG_PRODUCT_OPTION_TYPE_PRICE_OPTION_TYPE` (`option_type_id`),
  KEY `CATALOG_PRODUCT_OPTION_TYPE_PRICE_STORE` (`store_id`),
  CONSTRAINT `FK_CATALOG_PRODUCT_OPTION_TYPE_PRICE_OPTION` FOREIGN KEY (`option_type_id`) REFERENCES `catalog_product_option_type_value` (`option_type_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATALOG_PRODUCT_OPTION_TYPE_PRICE_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalog_product_option_type_price`
--

/*!40000 ALTER TABLE `catalog_product_option_type_price` DISABLE KEYS */;
LOCK TABLES `catalog_product_option_type_price` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_product_option_type_price` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_product_option_type_title`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_product_option_type_title`;
CREATE TABLE  `supernarede`.`catalog_product_option_type_title` (
  `option_type_title_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `option_type_id` int(10) unsigned NOT NULL DEFAULT '0',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`option_type_title_id`),
  UNIQUE KEY `UNQ_OPTION_TYPE_STORE` (`option_type_id`,`store_id`),
  KEY `CATALOG_PRODUCT_OPTION_TYPE_TITLE_OPTION` (`option_type_id`),
  KEY `CATALOG_PRODUCT_OPTION_TYPE_TITLE_STORE` (`store_id`),
  CONSTRAINT `FK_CATALOG_PRODUCT_OPTION_TYPE_TITLE_OPTION` FOREIGN KEY (`option_type_id`) REFERENCES `catalog_product_option_type_value` (`option_type_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATALOG_PRODUCT_OPTION_TYPE_TITLE_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalog_product_option_type_title`
--

/*!40000 ALTER TABLE `catalog_product_option_type_title` DISABLE KEYS */;
LOCK TABLES `catalog_product_option_type_title` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_product_option_type_title` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_product_option_type_value`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_product_option_type_value`;
CREATE TABLE  `supernarede`.`catalog_product_option_type_value` (
  `option_type_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `option_id` int(10) unsigned NOT NULL DEFAULT '0',
  `sku` varchar(64) NOT NULL DEFAULT '',
  `sort_order` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`option_type_id`),
  KEY `CATALOG_PRODUCT_OPTION_TYPE_VALUE_OPTION` (`option_id`),
  CONSTRAINT `FK_CATALOG_PRODUCT_OPTION_TYPE_VALUE_OPTION` FOREIGN KEY (`option_id`) REFERENCES `catalog_product_option` (`option_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalog_product_option_type_value`
--

/*!40000 ALTER TABLE `catalog_product_option_type_value` DISABLE KEYS */;
LOCK TABLES `catalog_product_option_type_value` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_product_option_type_value` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_product_relation`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_product_relation`;
CREATE TABLE  `supernarede`.`catalog_product_relation` (
  `parent_id` int(10) unsigned NOT NULL,
  `child_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`parent_id`,`child_id`),
  KEY `IDX_CHILD` (`child_id`),
  CONSTRAINT `FK_CATALOG_PRODUCT_RELATION_CHILD` FOREIGN KEY (`child_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATALOG_PRODUCT_RELATION_PARENT` FOREIGN KEY (`parent_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

--
-- Dumping data for table `supernarede`.`catalog_product_relation`
--

/*!40000 ALTER TABLE `catalog_product_relation` DISABLE KEYS */;
LOCK TABLES `catalog_product_relation` WRITE;
INSERT INTO `supernarede`.`catalog_product_relation` VALUES  (6,1),
 (6,2),
 (6,3);
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_product_relation` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_product_super_attribute`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_product_super_attribute`;
CREATE TABLE  `supernarede`.`catalog_product_super_attribute` (
  `product_super_attribute_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(10) unsigned NOT NULL DEFAULT '0',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `position` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`product_super_attribute_id`),
  UNIQUE KEY `UNQ_product_id_attribute_id` (`product_id`,`attribute_id`),
  KEY `FK_SUPER_PRODUCT_ATTRIBUTE_PRODUCT` (`product_id`),
  CONSTRAINT `FK_SUPER_PRODUCT_ATTRIBUTE_PRODUCT` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalog_product_super_attribute`
--

/*!40000 ALTER TABLE `catalog_product_super_attribute` DISABLE KEYS */;
LOCK TABLES `catalog_product_super_attribute` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_product_super_attribute` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_product_super_attribute_label`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_product_super_attribute_label`;
CREATE TABLE  `supernarede`.`catalog_product_super_attribute_label` (
  `value_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_super_attribute_id` int(10) unsigned NOT NULL DEFAULT '0',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `use_default` tinyint(1) unsigned DEFAULT '0',
  `value` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `UNQ_ATTRIBUTE_STORE` (`product_super_attribute_id`,`store_id`),
  KEY `FK_SUPER_PRODUCT_ATTRIBUTE_LABEL` (`product_super_attribute_id`),
  KEY `FK_CATALOG_PRODUCT_SUPER_ATTRIBUTE_LABEL_STORE` (`store_id`),
  CONSTRAINT `FK_CATALOG_PROD_SUPER_ATTR_LABEL_ATTR` FOREIGN KEY (`product_super_attribute_id`) REFERENCES `catalog_product_super_attribute` (`product_super_attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATALOG_PROD_SUPER_ATTR_LABEL_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `supernarede`.`catalog_product_super_attribute_label`
--

/*!40000 ALTER TABLE `catalog_product_super_attribute_label` DISABLE KEYS */;
LOCK TABLES `catalog_product_super_attribute_label` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_product_super_attribute_label` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_product_super_attribute_pricing`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_product_super_attribute_pricing`;
CREATE TABLE  `supernarede`.`catalog_product_super_attribute_pricing` (
  `value_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_super_attribute_id` int(10) unsigned NOT NULL DEFAULT '0',
  `value_index` varchar(255) NOT NULL DEFAULT '',
  `is_percent` tinyint(1) unsigned DEFAULT '0',
  `pricing_value` decimal(12,4) DEFAULT NULL,
  `website_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `UNQ_product_super_attribute_id_value_index_website_id` (`product_super_attribute_id`,`value_index`,`website_id`),
  KEY `FK_SUPER_PRODUCT_ATTRIBUTE_PRICING` (`product_super_attribute_id`),
  KEY `FK_CATALOG_PRODUCT_SUPER_PRICE_WEBSITE` (`website_id`),
  CONSTRAINT `FK_CATALOG_PRODUCT_SUPER_PRICE_WEBSITE` FOREIGN KEY (`website_id`) REFERENCES `core_website` (`website_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_SUPER_PRODUCT_ATTRIBUTE_PRICING` FOREIGN KEY (`product_super_attribute_id`) REFERENCES `catalog_product_super_attribute` (`product_super_attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalog_product_super_attribute_pricing`
--

/*!40000 ALTER TABLE `catalog_product_super_attribute_pricing` DISABLE KEYS */;
LOCK TABLES `catalog_product_super_attribute_pricing` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_product_super_attribute_pricing` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_product_super_link`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_product_super_link`;
CREATE TABLE  `supernarede`.`catalog_product_super_link` (
  `link_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(10) unsigned NOT NULL DEFAULT '0',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`link_id`),
  UNIQUE KEY `UNQ_product_id_parent_id` (`product_id`,`parent_id`),
  KEY `FK_SUPER_PRODUCT_LINK_PARENT` (`parent_id`),
  KEY `FK_catalog_product_super_link` (`product_id`),
  CONSTRAINT `FK_SUPER_PRODUCT_LINK_ENTITY` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_SUPER_PRODUCT_LINK_PARENT` FOREIGN KEY (`parent_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalog_product_super_link`
--

/*!40000 ALTER TABLE `catalog_product_super_link` DISABLE KEYS */;
LOCK TABLES `catalog_product_super_link` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_product_super_link` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalog_product_website`
--

DROP TABLE IF EXISTS `supernarede`.`catalog_product_website`;
CREATE TABLE  `supernarede`.`catalog_product_website` (
  `product_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `website_id` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`product_id`,`website_id`),
  KEY `FK_CATALOG_PRODUCT_WEBSITE_WEBSITE` (`website_id`),
  CONSTRAINT `FK_CATALOG_PRODUCT_WEBSITE_WEBSITE` FOREIGN KEY (`website_id`) REFERENCES `core_website` (`website_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATALOG_WEBSITE_PRODUCT_PRODUCT` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

--
-- Dumping data for table `supernarede`.`catalog_product_website`
--

/*!40000 ALTER TABLE `catalog_product_website` DISABLE KEYS */;
LOCK TABLES `catalog_product_website` WRITE;
INSERT INTO `supernarede`.`catalog_product_website` VALUES  (1,1),
 (2,1),
 (3,1),
 (4,1),
 (5,1),
 (6,1);
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalog_product_website` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalogindex_aggregation`
--

DROP TABLE IF EXISTS `supernarede`.`catalogindex_aggregation`;
CREATE TABLE  `supernarede`.`catalogindex_aggregation` (
  `aggregation_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `store_id` smallint(5) unsigned NOT NULL,
  `created_at` datetime NOT NULL,
  `key` varchar(255) DEFAULT NULL,
  `data` mediumtext,
  PRIMARY KEY (`aggregation_id`),
  UNIQUE KEY `IDX_STORE_KEY` (`store_id`,`key`),
  CONSTRAINT `FK_CATALOGINDEX_AGGREGATION_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalogindex_aggregation`
--

/*!40000 ALTER TABLE `catalogindex_aggregation` DISABLE KEYS */;
LOCK TABLES `catalogindex_aggregation` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalogindex_aggregation` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalogindex_aggregation_tag`
--

DROP TABLE IF EXISTS `supernarede`.`catalogindex_aggregation_tag`;
CREATE TABLE  `supernarede`.`catalogindex_aggregation_tag` (
  `tag_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tag_code` varchar(255) NOT NULL,
  PRIMARY KEY (`tag_id`),
  UNIQUE KEY `IDX_CODE` (`tag_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalogindex_aggregation_tag`
--

/*!40000 ALTER TABLE `catalogindex_aggregation_tag` DISABLE KEYS */;
LOCK TABLES `catalogindex_aggregation_tag` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalogindex_aggregation_tag` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalogindex_aggregation_to_tag`
--

DROP TABLE IF EXISTS `supernarede`.`catalogindex_aggregation_to_tag`;
CREATE TABLE  `supernarede`.`catalogindex_aggregation_to_tag` (
  `aggregation_id` int(10) unsigned NOT NULL,
  `tag_id` int(10) unsigned NOT NULL,
  UNIQUE KEY `IDX_AGGREGATION_TAG` (`aggregation_id`,`tag_id`),
  KEY `FK_CATALOGINDEX_AGGREGATION_TO_TAG_TAG` (`tag_id`),
  CONSTRAINT `FK_CATALOGINDEX_AGGREGATION_TO_TAG_AGGREGATION` FOREIGN KEY (`aggregation_id`) REFERENCES `catalogindex_aggregation` (`aggregation_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATALOGINDEX_AGGREGATION_TO_TAG_TAG` FOREIGN KEY (`tag_id`) REFERENCES `catalogindex_aggregation_tag` (`tag_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalogindex_aggregation_to_tag`
--

/*!40000 ALTER TABLE `catalogindex_aggregation_to_tag` DISABLE KEYS */;
LOCK TABLES `catalogindex_aggregation_to_tag` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalogindex_aggregation_to_tag` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalogindex_eav`
--

DROP TABLE IF EXISTS `supernarede`.`catalogindex_eav`;
CREATE TABLE  `supernarede`.`catalogindex_eav` (
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `value` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`store_id`,`entity_id`,`attribute_id`,`value`),
  KEY `IDX_VALUE` (`value`),
  KEY `FK_CATALOGINDEX_EAV_ENTITY` (`entity_id`),
  KEY `FK_CATALOGINDEX_EAV_ATTRIBUTE` (`attribute_id`),
  KEY `FK_CATALOGINDEX_EAV_STORE` (`store_id`),
  CONSTRAINT `FK_CATALOGINDEX_EAV_ATTRIBUTE` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATALOGINDEX_EAV_ENTITY` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATALOGINDEX_EAV_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalogindex_eav`
--

/*!40000 ALTER TABLE `catalogindex_eav` DISABLE KEYS */;
LOCK TABLES `catalogindex_eav` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalogindex_eav` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalogindex_minimal_price`
--

DROP TABLE IF EXISTS `supernarede`.`catalogindex_minimal_price`;
CREATE TABLE  `supernarede`.`catalogindex_minimal_price` (
  `index_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0',
  `customer_group_id` smallint(3) unsigned NOT NULL DEFAULT '0',
  `qty` decimal(12,4) unsigned NOT NULL DEFAULT '0.0000',
  `value` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `tax_class_id` smallint(6) NOT NULL DEFAULT '0',
  `website_id` smallint(5) unsigned DEFAULT NULL,
  PRIMARY KEY (`index_id`),
  KEY `IDX_VALUE` (`value`),
  KEY `IDX_QTY` (`qty`),
  KEY `FK_CATALOGINDEX_MINIMAL_PRICE_CUSTOMER_GROUP` (`customer_group_id`),
  KEY `FK_CI_MINIMAL_PRICE_WEBSITE_ID` (`website_id`),
  KEY `IDX_FULL` (`entity_id`,`qty`,`customer_group_id`,`value`,`website_id`),
  CONSTRAINT `FK_CATALOGINDEX_MINIMAL_PRICE_CUSTOMER_GROUP` FOREIGN KEY (`customer_group_id`) REFERENCES `customer_group` (`customer_group_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATALOGINDEX_MINIMAL_PRICE_ENTITY` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CI_MINIMAL_PRICE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `core_website` (`website_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalogindex_minimal_price`
--

/*!40000 ALTER TABLE `catalogindex_minimal_price` DISABLE KEYS */;
LOCK TABLES `catalogindex_minimal_price` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalogindex_minimal_price` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalogindex_price`
--

DROP TABLE IF EXISTS `supernarede`.`catalogindex_price`;
CREATE TABLE  `supernarede`.`catalogindex_price` (
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `customer_group_id` smallint(3) unsigned NOT NULL DEFAULT '0',
  `qty` decimal(12,4) unsigned NOT NULL DEFAULT '0.0000',
  `value` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `tax_class_id` smallint(6) NOT NULL DEFAULT '0',
  `website_id` smallint(5) unsigned DEFAULT NULL,
  KEY `IDX_VALUE` (`value`),
  KEY `IDX_QTY` (`qty`),
  KEY `FK_CATALOGINDEX_PRICE_ENTITY` (`entity_id`),
  KEY `FK_CATALOGINDEX_PRICE_ATTRIBUTE` (`attribute_id`),
  KEY `FK_CATALOGINDEX_PRICE_CUSTOMER_GROUP` (`customer_group_id`),
  KEY `IDX_RANGE_VALUE` (`entity_id`,`attribute_id`,`customer_group_id`,`value`),
  KEY `FK_CI_PRICE_WEBSITE_ID` (`website_id`),
  KEY `IDX_FULL` (`entity_id`,`attribute_id`,`customer_group_id`,`value`,`website_id`),
  CONSTRAINT `FK_CATALOGINDEX_PRICE_ATTRIBUTE` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATALOGINDEX_PRICE_ENTITY` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CI_PRICE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `core_website` (`website_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalogindex_price`
--

/*!40000 ALTER TABLE `catalogindex_price` DISABLE KEYS */;
LOCK TABLES `catalogindex_price` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalogindex_price` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`cataloginventory_stock`
--

DROP TABLE IF EXISTS `supernarede`.`cataloginventory_stock`;
CREATE TABLE  `supernarede`.`cataloginventory_stock` (
  `stock_id` smallint(4) unsigned NOT NULL AUTO_INCREMENT,
  `stock_name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`stock_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Catalog inventory Stocks list';

--
-- Dumping data for table `supernarede`.`cataloginventory_stock`
--

/*!40000 ALTER TABLE `cataloginventory_stock` DISABLE KEYS */;
LOCK TABLES `cataloginventory_stock` WRITE;
INSERT INTO `supernarede`.`cataloginventory_stock` VALUES  (1,'Padrão');
UNLOCK TABLES;
/*!40000 ALTER TABLE `cataloginventory_stock` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`cataloginventory_stock_item`
--

DROP TABLE IF EXISTS `supernarede`.`cataloginventory_stock_item`;
CREATE TABLE  `supernarede`.`cataloginventory_stock_item` (
  `item_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(10) unsigned NOT NULL DEFAULT '0',
  `stock_id` smallint(4) unsigned NOT NULL DEFAULT '0',
  `qty` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `min_qty` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `use_config_min_qty` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `is_qty_decimal` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `backorders` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `use_config_backorders` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `min_sale_qty` decimal(12,4) NOT NULL DEFAULT '1.0000',
  `use_config_min_sale_qty` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `max_sale_qty` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `use_config_max_sale_qty` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `is_in_stock` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `low_stock_date` datetime DEFAULT NULL,
  `notify_stock_qty` decimal(12,4) DEFAULT NULL,
  `use_config_notify_stock_qty` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `manage_stock` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `use_config_manage_stock` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `stock_status_changed_automatically` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `use_config_qty_increments` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `qty_increments` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `use_config_enable_qty_increments` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `enable_qty_increments` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`item_id`),
  UNIQUE KEY `IDX_STOCK_PRODUCT` (`product_id`,`stock_id`),
  KEY `FK_CATALOGINVENTORY_STOCK_ITEM_PRODUCT` (`product_id`),
  KEY `FK_CATALOGINVENTORY_STOCK_ITEM_STOCK` (`stock_id`),
  CONSTRAINT `FK_CATALOGINVENTORY_STOCK_ITEM_PRODUCT` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATALOGINVENTORY_STOCK_ITEM_STOCK` FOREIGN KEY (`stock_id`) REFERENCES `cataloginventory_stock` (`stock_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='Inventory Stock Item Data';

--
-- Dumping data for table `supernarede`.`cataloginventory_stock_item`
--

/*!40000 ALTER TABLE `cataloginventory_stock_item` DISABLE KEYS */;
LOCK TABLES `cataloginventory_stock_item` WRITE;
INSERT INTO `supernarede`.`cataloginventory_stock_item` VALUES  (1,1,1,'50.0000','0.0000',1,0,0,1,'1.0000',1,'0.0000',1,1,NULL,NULL,1,1,0,0,1,'0.0000',1,0),
 (2,2,1,'30.0000','0.0000',1,0,0,1,'1.0000',1,'0.0000',1,1,NULL,NULL,1,0,1,0,1,'0.0000',1,0),
 (3,3,1,'150.0000','0.0000',1,0,0,1,'1.0000',1,'0.0000',1,1,NULL,NULL,1,0,1,0,1,'0.0000',1,0),
 (4,4,1,'10.0000','5.0000',0,0,0,1,'1.0000',1,'0.0000',1,1,NULL,NULL,1,1,0,0,1,'0.0000',1,0),
 (5,5,1,'200.0000','0.0000',1,0,0,1,'1.0000',1,'0.0000',1,1,NULL,NULL,1,0,1,0,1,'0.0000',1,0),
 (6,6,1,'0.0000','0.0000',1,0,0,1,'1.0000',1,'0.0000',1,1,NULL,NULL,1,0,0,0,1,'0.0000',1,0);
UNLOCK TABLES;
/*!40000 ALTER TABLE `cataloginventory_stock_item` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`cataloginventory_stock_status`
--

DROP TABLE IF EXISTS `supernarede`.`cataloginventory_stock_status`;
CREATE TABLE  `supernarede`.`cataloginventory_stock_status` (
  `product_id` int(10) unsigned NOT NULL,
  `website_id` smallint(5) unsigned NOT NULL,
  `stock_id` smallint(4) unsigned NOT NULL,
  `qty` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `stock_status` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`product_id`,`website_id`,`stock_id`),
  KEY `FK_CATALOGINVENTORY_STOCK_STATUS_STOCK` (`stock_id`),
  KEY `FK_CATALOGINVENTORY_STOCK_STATUS_WEBSITE` (`website_id`),
  CONSTRAINT `FK_CATALOGINVENTORY_STOCK_STATUS_PRODUCT` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATALOGINVENTORY_STOCK_STATUS_STOCK` FOREIGN KEY (`stock_id`) REFERENCES `cataloginventory_stock` (`stock_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATALOGINVENTORY_STOCK_STATUS_WEBSITE` FOREIGN KEY (`website_id`) REFERENCES `core_website` (`website_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`cataloginventory_stock_status`
--

/*!40000 ALTER TABLE `cataloginventory_stock_status` DISABLE KEYS */;
LOCK TABLES `cataloginventory_stock_status` WRITE;
INSERT INTO `supernarede`.`cataloginventory_stock_status` VALUES  (1,1,1,'50.0000',1),
 (2,1,1,'30.0000',1),
 (3,1,1,'150.0000',1),
 (4,1,1,'10.0000',1),
 (5,1,1,'200.0000',1),
 (6,1,1,'0.0000',1);
UNLOCK TABLES;
/*!40000 ALTER TABLE `cataloginventory_stock_status` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`cataloginventory_stock_status_idx`
--

DROP TABLE IF EXISTS `supernarede`.`cataloginventory_stock_status_idx`;
CREATE TABLE  `supernarede`.`cataloginventory_stock_status_idx` (
  `product_id` int(10) unsigned NOT NULL,
  `website_id` smallint(5) unsigned NOT NULL,
  `stock_id` smallint(4) unsigned NOT NULL,
  `qty` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `stock_status` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`product_id`,`website_id`,`stock_id`),
  KEY `FK_CATALOGINVENTORY_STOCK_STATUS_STOCK` (`stock_id`),
  KEY `FK_CATALOGINVENTORY_STOCK_STATUS_WEBSITE` (`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`cataloginventory_stock_status_idx`
--

/*!40000 ALTER TABLE `cataloginventory_stock_status_idx` DISABLE KEYS */;
LOCK TABLES `cataloginventory_stock_status_idx` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `cataloginventory_stock_status_idx` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`cataloginventory_stock_status_tmp`
--

DROP TABLE IF EXISTS `supernarede`.`cataloginventory_stock_status_tmp`;
CREATE TABLE  `supernarede`.`cataloginventory_stock_status_tmp` (
  `product_id` int(10) unsigned NOT NULL,
  `website_id` smallint(5) unsigned NOT NULL,
  `stock_id` smallint(4) unsigned NOT NULL,
  `qty` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `stock_status` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`product_id`,`website_id`,`stock_id`),
  KEY `FK_CATALOGINVENTORY_STOCK_STATUS_STOCK` (`stock_id`),
  KEY `FK_CATALOGINVENTORY_STOCK_STATUS_WEBSITE` (`website_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`cataloginventory_stock_status_tmp`
--

/*!40000 ALTER TABLE `cataloginventory_stock_status_tmp` DISABLE KEYS */;
LOCK TABLES `cataloginventory_stock_status_tmp` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `cataloginventory_stock_status_tmp` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalogrule`
--

DROP TABLE IF EXISTS `supernarede`.`catalogrule`;
CREATE TABLE  `supernarede`.`catalogrule` (
  `rule_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `from_date` date DEFAULT NULL,
  `to_date` date DEFAULT NULL,
  `customer_group_ids` text,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `conditions_serialized` mediumtext NOT NULL,
  `actions_serialized` mediumtext NOT NULL,
  `stop_rules_processing` tinyint(1) NOT NULL DEFAULT '1',
  `sort_order` int(10) unsigned NOT NULL DEFAULT '0',
  `simple_action` varchar(32) NOT NULL,
  `discount_amount` decimal(12,4) NOT NULL,
  `website_ids` text,
  PRIMARY KEY (`rule_id`),
  KEY `sort_order` (`is_active`,`sort_order`,`to_date`,`from_date`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalogrule`
--

/*!40000 ALTER TABLE `catalogrule` DISABLE KEYS */;
LOCK TABLES `catalogrule` WRITE;
INSERT INTO `supernarede`.`catalogrule` VALUES  (1,'Desconto 5% na breja','','2011-08-18','2011-08-31','0,1,2,3',1,'a:7:{s:4:\"type\";s:34:\"catalogrule/rule_condition_combine\";s:9:\"attribute\";N;s:8:\"operator\";N;s:5:\"value\";s:1:\"1\";s:18:\"is_value_processed\";N;s:10:\"aggregator\";s:3:\"all\";s:10:\"conditions\";a:2:{i:0;a:5:{s:4:\"type\";s:34:\"catalogrule/rule_condition_product\";s:9:\"attribute\";s:13:\"cerveja_marca\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:2:\"12\";s:18:\"is_value_processed\";b:0;}i:1;a:5:{s:4:\"type\";s:34:\"catalogrule/rule_condition_product\";s:9:\"attribute\";s:12:\"category_ids\";s:8:\"operator\";s:2:\"{}\";s:5:\"value\";s:1:\"9\";s:18:\"is_value_processed\";b:0;}}}','a:4:{s:4:\"type\";s:34:\"catalogrule/rule_action_collection\";s:9:\"attribute\";N;s:8:\"operator\";s:1:\"=\";s:5:\"value\";N;}',1,0,'by_percent','5.0000','1');
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalogrule` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalogrule_affected_product`
--

DROP TABLE IF EXISTS `supernarede`.`catalogrule_affected_product`;
CREATE TABLE  `supernarede`.`catalogrule_affected_product` (
  `product_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalogrule_affected_product`
--

/*!40000 ALTER TABLE `catalogrule_affected_product` DISABLE KEYS */;
LOCK TABLES `catalogrule_affected_product` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalogrule_affected_product` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalogrule_group_website`
--

DROP TABLE IF EXISTS `supernarede`.`catalogrule_group_website`;
CREATE TABLE  `supernarede`.`catalogrule_group_website` (
  `rule_id` int(10) unsigned NOT NULL DEFAULT '0',
  `customer_group_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `website_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`rule_id`,`customer_group_id`,`website_id`),
  KEY `rule_id` (`rule_id`),
  KEY `customer_group_id` (`customer_group_id`),
  KEY `website_id` (`website_id`),
  CONSTRAINT `FK_CATALOGRULE_GROUP_WEBSITE_GROUP` FOREIGN KEY (`customer_group_id`) REFERENCES `customer_group` (`customer_group_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATALOGRULE_GROUP_WEBSITE_RULE` FOREIGN KEY (`rule_id`) REFERENCES `catalogrule` (`rule_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATALOGRULE_GROUP_WEBSITE_WEBSITE` FOREIGN KEY (`website_id`) REFERENCES `core_website` (`website_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `supernarede`.`catalogrule_group_website`
--

/*!40000 ALTER TABLE `catalogrule_group_website` DISABLE KEYS */;
LOCK TABLES `catalogrule_group_website` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalogrule_group_website` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalogrule_product`
--

DROP TABLE IF EXISTS `supernarede`.`catalogrule_product`;
CREATE TABLE  `supernarede`.`catalogrule_product` (
  `rule_product_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rule_id` int(10) unsigned NOT NULL DEFAULT '0',
  `from_time` int(10) unsigned NOT NULL DEFAULT '0',
  `to_time` int(10) unsigned NOT NULL DEFAULT '0',
  `customer_group_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0',
  `action_operator` enum('to_fixed','to_percent','by_fixed','by_percent') NOT NULL DEFAULT 'to_fixed',
  `action_amount` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `action_stop` tinyint(1) NOT NULL DEFAULT '0',
  `sort_order` int(10) unsigned NOT NULL DEFAULT '0',
  `website_id` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`rule_product_id`),
  UNIQUE KEY `sort_order` (`rule_id`,`from_time`,`to_time`,`website_id`,`customer_group_id`,`product_id`,`sort_order`),
  KEY `FK_catalogrule_product_rule` (`rule_id`),
  KEY `FK_catalogrule_product_customergroup` (`customer_group_id`),
  KEY `FK_catalogrule_product_website` (`website_id`),
  KEY `IDX_FROM_TIME` (`from_time`),
  KEY `IDX_TO_TIME` (`to_time`),
  KEY `FK_CATALOGRULE_PRODUCT_PRODUCT` (`product_id`),
  CONSTRAINT `FK_catalogrule_product_customergroup` FOREIGN KEY (`customer_group_id`) REFERENCES `customer_group` (`customer_group_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATALOGRULE_PRODUCT_PRODUCT` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_catalogrule_product_rule` FOREIGN KEY (`rule_id`) REFERENCES `catalogrule` (`rule_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_catalogrule_product_website` FOREIGN KEY (`website_id`) REFERENCES `core_website` (`website_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalogrule_product`
--

/*!40000 ALTER TABLE `catalogrule_product` DISABLE KEYS */;
LOCK TABLES `catalogrule_product` WRITE;
INSERT INTO `supernarede`.`catalogrule_product` VALUES  (1,1,1313625600,1314835199,0,3,'by_percent','5.0000',1,0,1),
 (2,1,1313625600,1314835199,1,3,'by_percent','5.0000',1,0,1),
 (3,1,1313625600,1314835199,2,3,'by_percent','5.0000',1,0,1),
 (4,1,1313625600,1314835199,3,3,'by_percent','5.0000',1,0,1);
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalogrule_product` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalogrule_product_price`
--

DROP TABLE IF EXISTS `supernarede`.`catalogrule_product_price`;
CREATE TABLE  `supernarede`.`catalogrule_product_price` (
  `rule_product_price_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rule_date` date NOT NULL DEFAULT '0000-00-00',
  `customer_group_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0',
  `rule_price` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `website_id` smallint(5) unsigned NOT NULL,
  `latest_start_date` date DEFAULT NULL,
  `earliest_end_date` date DEFAULT NULL,
  PRIMARY KEY (`rule_product_price_id`),
  UNIQUE KEY `rule_date` (`rule_date`,`website_id`,`customer_group_id`,`product_id`),
  KEY `FK_catalogrule_product_price_customergroup` (`customer_group_id`),
  KEY `FK_catalogrule_product_price_website` (`website_id`),
  KEY `FK_CATALOGRULE_PRODUCT_PRICE_PRODUCT` (`product_id`),
  CONSTRAINT `FK_catalogrule_product_price_customergroup` FOREIGN KEY (`customer_group_id`) REFERENCES `customer_group` (`customer_group_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATALOGRULE_PRODUCT_PRICE_PRODUCT` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_catalogrule_product_price_website` FOREIGN KEY (`website_id`) REFERENCES `core_website` (`website_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalogrule_product_price`
--

/*!40000 ALTER TABLE `catalogrule_product_price` DISABLE KEYS */;
LOCK TABLES `catalogrule_product_price` WRITE;
INSERT INTO `supernarede`.`catalogrule_product_price` VALUES  (25,'2011-08-18',0,3,'3.3200',1,'2011-08-18','2011-08-31'),
 (26,'2011-08-19',0,3,'3.3200',1,'2011-08-18','2011-08-31'),
 (27,'2011-08-18',1,3,'3.3200',1,'2011-08-18','2011-08-31'),
 (28,'2011-08-19',1,3,'3.3200',1,'2011-08-18','2011-08-31'),
 (29,'2011-08-18',2,3,'3.3200',1,'2011-08-18','2011-08-31'),
 (30,'2011-08-19',2,3,'3.3200',1,'2011-08-18','2011-08-31'),
 (31,'2011-08-18',3,3,'3.3200',1,'2011-08-18','2011-08-31'),
 (32,'2011-08-19',3,3,'3.3200',1,'2011-08-18','2011-08-31');
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalogrule_product_price` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalogsearch_fulltext`
--

DROP TABLE IF EXISTS `supernarede`.`catalogsearch_fulltext`;
CREATE TABLE  `supernarede`.`catalogsearch_fulltext` (
  `product_id` int(10) unsigned NOT NULL,
  `store_id` smallint(5) unsigned NOT NULL,
  `data_index` longtext NOT NULL,
  PRIMARY KEY (`product_id`,`store_id`),
  FULLTEXT KEY `data_index` (`data_index`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalogsearch_fulltext`
--

/*!40000 ALTER TABLE `catalogsearch_fulltext` DISABLE KEYS */;
LOCK TABLES `catalogsearch_fulltext` WRITE;
INSERT INTO `supernarede`.`catalogsearch_fulltext` VALUES  (1,1,'C050505|Habilitado||Pilsen|Antarctica|Cerveja Exemplo Sem alcool|Clara|Sem Alcool|Ingredientes: &Aacute;gua, malte, cereais n&atilde;o malteados, carboidratos e l&uacute;pulo. Antioxidante: INS 316, estabilizante: INS 405. Teor alco&oacute;lico: 4,6% vol. CONT&Eacute;M GL&Uacute;TEN.||1.29|1'),
 (2,1,'C54654654|Habilitado||Pilsen|Bavaria|Cerveja Exemplo Bavaria|Clara|Ingredientes: &Aacute;gua, malte, cereais n&atilde;o malteados, carboidratos e l&uacute;pulo. Antioxidante: INS 316, estabilizante: INS 405. Teor alco&oacute;lico: 4,6% vol. CONT&Eacute;M GL&Uacute;TEN.||1.39|1'),
 (4,1,'C/879846|Habilitado||Lager|Brahma|Cerveja Exemplo Heineken Long 2|Clara|Ingredientes: &Aacute;gua, malte, cereais n&atilde;o malteados, carboidratos e l&uacute;pulo. Antioxidante: INS 316, estabilizante: INS 405. Teor alco&oacute;lico: 4,6% vol. CONT&Eacute;M GL&Uacute;TEN.||3.59|1'),
 (5,1,'C7987654|Habilitado||Lager|Skol|Cerveja Exemplo Skol Escura|Escura|Ingredientes: &Aacute;gua, malte, cereais n&atilde;o malteados, carboidratos e l&uacute;pulo. Antioxidante: INS 316, estabilizante: INS 405. Teor alco&oacute;lico: 4,6% vol. CONT&Eacute;M GL&Uacute;TEN.||1.9|1'),
 (3,1,'C548748|Habilitado|Heineken|Lager||Cerveja Exemplo Heineken Long|Clara|Ingredientes: &Aacute;gua, malte, cereais n&atilde;o malteados, carboidratos e l&uacute;pulo. Antioxidante: INS 316, estabilizante: INS 405. Teor alco&oacute;lico: 4,6% vol. CONT&Eacute;M GL&Uacute;TEN.||3.49|1');
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalogsearch_fulltext` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalogsearch_query`
--

DROP TABLE IF EXISTS `supernarede`.`catalogsearch_query`;
CREATE TABLE  `supernarede`.`catalogsearch_query` (
  `query_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `query_text` varchar(255) NOT NULL DEFAULT '',
  `num_results` int(10) unsigned NOT NULL DEFAULT '0',
  `popularity` int(10) unsigned NOT NULL DEFAULT '0',
  `redirect` varchar(255) NOT NULL DEFAULT '',
  `synonym_for` varchar(255) NOT NULL,
  `store_id` smallint(5) unsigned NOT NULL,
  `display_in_terms` tinyint(1) NOT NULL DEFAULT '1',
  `is_active` tinyint(1) DEFAULT '1',
  `is_processed` tinyint(1) DEFAULT '0',
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`query_id`),
  KEY `FK_CATALOGSEARCH_QUERY_STORE` (`store_id`),
  KEY `IDX_SEARCH_QUERY` (`query_text`,`store_id`,`popularity`),
  CONSTRAINT `FK_CATALOGSEARCH_QUERY_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalogsearch_query`
--

/*!40000 ALTER TABLE `catalogsearch_query` DISABLE KEYS */;
LOCK TABLES `catalogsearch_query` WRITE;
INSERT INTO `supernarede`.`catalogsearch_query` VALUES  (1,'skol',1,23,'','',1,1,1,0,'2011-08-19 13:25:02'),
 (2,'teste',0,15,'','',1,1,1,0,'2011-09-21 19:12:29'),
 (3,'exemplo, cerveja',6,4,'','',1,1,1,0,'2011-08-24 13:56:01'),
 (4,'bavaria',2,1,'','',0,1,1,0,'2011-09-19 18:04:37'),
 (5,'cerveja',6,1,'','',0,1,1,0,'2011-09-19 18:23:40'),
 (6,'cerveja',6,1,'','',0,1,1,0,'2011-09-19 18:23:59'),
 (7,'bavaria',2,1,'','',0,1,1,0,'2011-09-19 19:33:32'),
 (8,'bavaria',2,1,'','',0,1,1,0,'2011-09-19 19:34:03'),
 (9,'arroz',0,1,'','',0,1,1,0,'2011-09-21 19:00:36'),
 (10,'cerveja',6,1,'','',0,1,1,0,'2011-09-21 19:12:55'),
 (11,'cerveja',6,1,'','',0,1,1,0,'2011-09-21 19:44:20'),
 (12,'cerveja',6,1,'','',0,1,1,0,'2011-09-21 19:58:38'),
 (13,'cerveja',5,1,'','',0,1,1,1,'2011-09-21 20:12:59'),
 (14,'cerveja',5,1,'','',0,1,1,1,'2011-09-21 20:13:28');
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalogsearch_query` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`catalogsearch_result`
--

DROP TABLE IF EXISTS `supernarede`.`catalogsearch_result`;
CREATE TABLE  `supernarede`.`catalogsearch_result` (
  `query_id` int(10) unsigned NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `relevance` decimal(6,4) NOT NULL DEFAULT '0.0000',
  PRIMARY KEY (`query_id`,`product_id`),
  KEY `IDX_QUERY` (`query_id`),
  KEY `IDX_PRODUCT` (`product_id`),
  KEY `IDX_RELEVANCE` (`query_id`,`relevance`),
  CONSTRAINT `FK_CATALOGSEARCH_RESULT_CATALOG_PRODUCT` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATALOGSEARCH_RESULT_QUERY` FOREIGN KEY (`query_id`) REFERENCES `catalogsearch_query` (`query_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`catalogsearch_result`
--

/*!40000 ALTER TABLE `catalogsearch_result` DISABLE KEYS */;
LOCK TABLES `catalogsearch_result` WRITE;
INSERT INTO `supernarede`.`catalogsearch_result` VALUES  (13,1,'1.0000'),
 (13,2,'1.0000'),
 (13,3,'1.0000'),
 (13,4,'1.0000'),
 (13,5,'1.0000'),
 (14,1,'1.0000'),
 (14,2,'1.0000'),
 (14,3,'1.0000'),
 (14,4,'1.0000'),
 (14,5,'1.0000');
UNLOCK TABLES;
/*!40000 ALTER TABLE `catalogsearch_result` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`checkout_agreement`
--

DROP TABLE IF EXISTS `supernarede`.`checkout_agreement`;
CREATE TABLE  `supernarede`.`checkout_agreement` (
  `agreement_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `content` text NOT NULL,
  `content_height` varchar(25) DEFAULT NULL,
  `checkbox_text` text NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '0',
  `is_html` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`agreement_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`checkout_agreement`
--

/*!40000 ALTER TABLE `checkout_agreement` DISABLE KEYS */;
LOCK TABLES `checkout_agreement` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `checkout_agreement` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`checkout_agreement_store`
--

DROP TABLE IF EXISTS `supernarede`.`checkout_agreement_store`;
CREATE TABLE  `supernarede`.`checkout_agreement_store` (
  `agreement_id` int(10) unsigned NOT NULL,
  `store_id` smallint(5) unsigned NOT NULL,
  UNIQUE KEY `agreement_id` (`agreement_id`,`store_id`),
  KEY `FK_CHECKOUT_AGREEMENT_STORE` (`store_id`),
  CONSTRAINT `FK_CHECKOUT_AGREEMENT` FOREIGN KEY (`agreement_id`) REFERENCES `checkout_agreement` (`agreement_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CHECKOUT_AGREEMENT_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`checkout_agreement_store`
--

/*!40000 ALTER TABLE `checkout_agreement_store` DISABLE KEYS */;
LOCK TABLES `checkout_agreement_store` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `checkout_agreement_store` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`cms_block`
--

DROP TABLE IF EXISTS `supernarede`.`cms_block`;
CREATE TABLE  `supernarede`.`cms_block` (
  `block_id` smallint(6) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `identifier` varchar(255) NOT NULL DEFAULT '',
  `content` mediumtext,
  `creation_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`block_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COMMENT='CMS Blocks';

--
-- Dumping data for table `supernarede`.`cms_block`
--

/*!40000 ALTER TABLE `cms_block` DISABLE KEYS */;
LOCK TABLES `cms_block` WRITE;
INSERT INTO `supernarede`.`cms_block` VALUES  (5,'Links de Rodapé','links_paginas_rodape','<ul>\r\n<li><a href=\"/quem-somos\">Quem Somos</a></li>\r\n<li>|</li>\r\n<li><a href=\"/como-comprar\">Como Comprar</a></li>\r\n<li>|</li>\r\n<li><a href=\"/seguranca\">Seguran&ccedil;a</a></li>\r\n<li>|</li>\r\n<li><a href=\"/garantia-e-trocas\">Garantia e Trocas</a></li>\r\n<li>|</li>\r\n<li><a href=\"/politicas-de-privacidade\">Pol&iacute;ticas de Privacidade</a></li>\r\n<li>|</li>\r\n<li><a href=\"/politicas-de-entrega\">Pol&iacute;ticas de Entrega</a></li>\r\n</ul>','2011-08-08 09:55:00','2011-08-08 16:41:09',1),
 (6,'Links do Topo','links_topo_televendas','<ul>\r\n<li><a href=\"/\">Home</a></li>\r\n<li>|</li>\r\n<li><a href=\"/listas-prontas\">Listas Prontas</a></li>\r\n<li>|</li>\r\n<li><a href=\"/receitas\">Receitas</a></li>\r\n<li>|</li>\r\n<li><a href=\"/checkout/cart\">Ver Carrinho</a></li>\r\n<li>|</li>\r\n<li><a href=\"/faq\">FAQ</a></li>\r\n<li>|</li>\r\n<li><a href=\"/contacts\">Fale Conosco</a></li>\r\n</ul>','2011-08-08 16:36:55','2011-09-16 12:36:05',1),
 (7,'Links para mídias sociais da loja','links_redes_sociais','<ul>\r\n<li><img title=\"Nosso Facebook\" src=\"http://supernarede.local/media/wysiwyg/ico_facebook.png\" alt=\"Nosso Facebook\" /></li>\r\n<li><img title=\"Siga-nos no Twitter!\" src=\"http://supernarede.local/media/wysiwyg/ico_twitter.png\" alt=\"Twitter\" /></li>\r\n<li><img title=\"Comunidade no Orkut\" src=\"http://supernarede.local/media/wysiwyg/ico_orkut.png\" alt=\"Orkut\" /></li>\r\n</ul>','2011-08-08 16:45:05','2011-08-08 16:45:05',1),
 (8,'Departamentos','departamentos','<p>Selecione um dos departamentos</p>','2011-08-10 18:47:05','2011-08-10 18:47:05',1),
 (9,'Listas Prontas','listas_prontas','<p><span class=\"centercinza\"><strong>Para a sua comodidade nesta se&ccedil;&atilde;o voc&ecirc;  encontrar&aacute; listas prontas com produtos separados por necessidades, para  facilitar a sua compra e torn&aacute;-la mais &aacute;gil e eficiente.</strong><br /> S&atilde;o produtos selecionadas com aten&ccedil;&atilde;o e carinho para suprir a  necessidade do seu dia a dia, sem que voc&ecirc; precise busc&aacute;-los  separadamente no nosso site.<br /> Escolha a lista que mais se adeque &agrave;s suas necessidades e boas compras. (texto do concorrente, 2011)<br /></span></p>','2011-08-10 20:08:13','2011-08-10 20:08:13',1),
 (10,'Categorias','categ','<p> </p>','2011-08-11 18:03:35','2011-08-11 18:03:35',1),
 (11,'Bandeiras do rodapé','bandeiras_cartoes','<ul>\r\n<li><img src=\"http://supernarede.local/media/wysiwyg/img_bandeiras-cartoes.png\" alt=\"Cart&otilde;es\" /></li>\r\n</ul>','2011-08-16 13:10:10','2011-08-16 13:10:10',1),
 (12,'Receitas','pagina_receitas','<p>asdhasd haskjdhasjkhdkjahs kjahkjdhaskjh kjdahskjdhkjashdkjh kjhajksdhjka hskjdhaskjdh kjashkjdh kajshdkjh kjashd</p>\r\n<p>asdjasjkdhkjashdkjahskjdhs ajkhdkjshdkjashk jhaskjdh kjhajkhd akjhdkjashdkjh kjashdkhaskjdhaksjdkjashd<br />asdasdasdasd</p>','2011-09-16 12:42:26','2011-09-16 12:42:26',1);
UNLOCK TABLES;
/*!40000 ALTER TABLE `cms_block` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`cms_block_store`
--

DROP TABLE IF EXISTS `supernarede`.`cms_block_store`;
CREATE TABLE  `supernarede`.`cms_block_store` (
  `block_id` smallint(6) NOT NULL,
  `store_id` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`block_id`,`store_id`),
  KEY `FK_CMS_BLOCK_STORE_STORE` (`store_id`),
  CONSTRAINT `FK_CMS_BLOCK_STORE_BLOCK` FOREIGN KEY (`block_id`) REFERENCES `cms_block` (`block_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CMS_BLOCK_STORE_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS Blocks to Stores';

--
-- Dumping data for table `supernarede`.`cms_block_store`
--

/*!40000 ALTER TABLE `cms_block_store` DISABLE KEYS */;
LOCK TABLES `cms_block_store` WRITE;
INSERT INTO `supernarede`.`cms_block_store` VALUES  (5,1),
 (6,1),
 (7,1),
 (8,1),
 (9,1),
 (10,1),
 (11,1),
 (12,1);
UNLOCK TABLES;
/*!40000 ALTER TABLE `cms_block_store` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`cms_page`
--

DROP TABLE IF EXISTS `supernarede`.`cms_page`;
CREATE TABLE  `supernarede`.`cms_page` (
  `page_id` smallint(6) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `root_template` varchar(255) NOT NULL DEFAULT '',
  `meta_keywords` text NOT NULL,
  `meta_description` text NOT NULL,
  `identifier` varchar(100) NOT NULL DEFAULT '',
  `content_heading` varchar(255) NOT NULL DEFAULT '',
  `content` mediumtext,
  `creation_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `sort_order` tinyint(4) NOT NULL DEFAULT '0',
  `layout_update_xml` text,
  `custom_theme` varchar(100) DEFAULT NULL,
  `custom_root_template` varchar(255) NOT NULL DEFAULT '',
  `custom_layout_update_xml` text,
  `custom_theme_from` date DEFAULT NULL,
  `custom_theme_to` date DEFAULT NULL,
  PRIMARY KEY (`page_id`),
  KEY `identifier` (`identifier`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COMMENT='CMS pages';

--
-- Dumping data for table `supernarede`.`cms_page`
--

/*!40000 ALTER TABLE `cms_page` DISABLE KEYS */;
LOCK TABLES `cms_page` WRITE;
INSERT INTO `supernarede`.`cms_page` VALUES  (1,'404 Não Encontrado','one_column','Page keywords','Page description','no-route','','<div class=\"page-title\">\r\n<h1>Whoops, our bad...</h1>\r\n</div>\r\n<dl> <dt>The page you requested was not found, and we have a fine guess why.</dt> <dd> \r\n<ul class=\"disc\">\r\n<li>If you typed the URL directly, please make sure the spelling is correct.</li>\r\n<li>If you clicked on a link to get here, the link is outdated.</li>\r\n</ul>\r\n</dd> </dl> <dl> <dt>What can you do?</dt> <dd>Have no fear, help is near! There are many ways you can get back on track with Magento Store.</dd> <dd> \r\n<ul class=\"disc\">\r\n<li><a onclick=\"history.back(); return false;\" href=\"#\">Go back</a> to the previous page.</li>\r\n<li>Use the search bar at the top of the page to search for your products.</li>\r\n<li>Follow these links to get you back on track!<br /><a href=\"{{store url=\"\"}}\">Store Home</a> <span class=\"separator\">|</span> <a href=\"{{store url=\"customer/account\"}}\">My Account</a></li>\r\n</ul>\r\n</dd></dl>','2007-06-20 18:38:32','2011-08-08 17:27:11',1,0,'','','','',NULL,NULL),
 (2,'Home','one_column','','','home','','<p>&nbsp;</p>','2007-08-23 10:03:25','2011-08-17 14:31:56',1,0,'<!--<reference name=\"content\">\r\n<block type=\"catalog/product_new\" name=\"home.catalog.product.new\" alias=\"product_new\" template=\"catalog/product/new.phtml\" after=\"cms_page\"><action method=\"addPriceBlockType\"><type>bundle</type><block>bundle/catalog_product_price</block><template>bundle/catalog/product/price.phtml</template></action></block>\r\n<block type=\"reports/product_viewed\" name=\"home.reports.product.viewed\" alias=\"product_viewed\" template=\"reports/home_product_viewed.phtml\" after=\"product_new\"><action method=\"addPriceBlockType\"><type>bundle</type><block>bundle/catalog_product_price</block><template>bundle/catalog/product/price.phtml</template></action></block>\r\n<block type=\"reports/product_compared\" name=\"home.reports.product.compared\" template=\"reports/home_product_compared.phtml\" after=\"product_viewed\"><action method=\"addPriceBlockType\"><type>bundle</type><block>bundle/catalog_product_price</block><template>bundle/catalog/product/price.phtml</template></action></block>\r\n</reference><reference name=\"right\">\r\n<action method=\"unsetChild\"><alias>right.reports.product.viewed</alias></action>\r\n<action method=\"unsetChild\"><alias>right.reports.product.compared</alias></action>\r\n</reference>-->\r\n<reference name=\"content\">\r\n<block type=\"catalog/product_list\" name=\"home.destaques\" template=\"cms/home/destaques.phtml\"></block>\r\n</reference>','','','',NULL,NULL),
 (5,'Ativar Cookies','one_column','','','enable-cookies','','<div class=\"std\">\r\n    <ul class=\"messages\">\r\n        <li class=\"notice-msg\">\r\n            <ul>\r\n                <li>Please enable cookies in your web browser to continue.</li>\r\n            </ul>\r\n        </li>\r\n    </ul>\r\n    <div class=\"page-title\">\r\n        <h1><a name=\"top\"></a>What are Cookies?</h1>\r\n    </div>\r\n    <p>Cookies are short pieces of data that are sent to your computer when you visit a website. On later visits, this data is then returned to that website. Cookies allow us to recognize you automatically whenever you visit our site so that we can personalize your experience and provide you with better service. We also use cookies (and similar browser data, such as Flash cookies) for fraud prevention and other purposes. If your web browser is set to refuse cookies from our website, you will not be able to complete a purchase or take advantage of certain features of our website, such as storing items in your Shopping Cart or receiving personalized recommendations. As a result, we strongly encourage you to configure your web browser to accept cookies from our website.</p>\r\n    <h2 class=\"subtitle\">Enabling Cookies</h2>\r\n    <ul class=\"disc\">\r\n        <li><a href=\"#ie7\">Internet Explorer 7.x</a></li>\r\n        <li><a href=\"#ie6\">Internet Explorer 6.x</a></li>\r\n        <li><a href=\"#firefox\">Mozilla/Firefox</a></li>\r\n        <li><a href=\"#opera\">Opera 7.x</a></li>\r\n    </ul>\r\n    <h3><a name=\"ie7\"></a>Internet Explorer 7.x</h3>\r\n    <ol>\r\n        <li>\r\n            <p>Start Internet Explorer</p>\r\n        </li>\r\n        <li>\r\n            <p>Under the <strong>Tools</strong> menu, click <strong>Internet Options</strong></p>\r\n            <p><img src=\"{{skin url=\"images/cookies/ie7-1.gif\"}}\" alt=\"\" /></p>\r\n        </li>\r\n        <li>\r\n            <p>Click the <strong>Privacy</strong> tab</p>\r\n            <p><img src=\"{{skin url=\"images/cookies/ie7-2.gif\"}}\" alt=\"\" /></p>\r\n        </li>\r\n        <li>\r\n            <p>Click the <strong>Advanced</strong> button</p>\r\n            <p><img src=\"{{skin url=\"images/cookies/ie7-3.gif\"}}\" alt=\"\" /></p>\r\n        </li>\r\n        <li>\r\n            <p>Put a check mark in the box for <strong>Override Automatic Cookie Handling</strong>, put another check mark in the <strong>Always accept session cookies </strong>box</p>\r\n            <p><img src=\"{{skin url=\"images/cookies/ie7-4.gif\"}}\" alt=\"\" /></p>\r\n        </li>\r\n        <li>\r\n            <p>Click <strong>OK</strong></p>\r\n            <p><img src=\"{{skin url=\"images/cookies/ie7-5.gif\"}}\" alt=\"\" /></p>\r\n        </li>\r\n        <li>\r\n            <p>Click <strong>OK</strong></p>\r\n            <p><img src=\"{{skin url=\"images/cookies/ie7-6.gif\"}}\" alt=\"\" /></p>\r\n        </li>\r\n        <li>\r\n            <p>Restart Internet Explore</p>\r\n        </li>\r\n    </ol>\r\n    <p class=\"a-top\"><a href=\"#top\">Back to Top</a></p>\r\n    <h3><a name=\"ie6\"></a>Internet Explorer 6.x</h3>\r\n    <ol>\r\n        <li>\r\n            <p>Select <strong>Internet Options</strong> from the Tools menu</p>\r\n            <p><img src=\"{{skin url=\"images/cookies/ie6-1.gif\"}}\" alt=\"\" /></p>\r\n        </li>\r\n        <li>\r\n            <p>Click on the <strong>Privacy</strong> tab</p>\r\n        </li>\r\n        <li>\r\n            <p>Click the <strong>Default</strong> button (or manually slide the bar down to <strong>Medium</strong>) under <strong>Settings</strong>. Click <strong>OK</strong></p>\r\n            <p><img src=\"{{skin url=\"images/cookies/ie6-2.gif\"}}\" alt=\"\" /></p>\r\n        </li>\r\n    </ol>\r\n    <p class=\"a-top\"><a href=\"#top\">Back to Top</a></p>\r\n    <h3><a name=\"firefox\"></a>Mozilla/Firefox</h3>\r\n    <ol>\r\n        <li>\r\n            <p>Click on the <strong>Tools</strong>-menu in Mozilla</p>\r\n        </li>\r\n        <li>\r\n            <p>Click on the <strong>Options...</strong> item in the menu - a new window open</p>\r\n        </li>\r\n        <li>\r\n            <p>Click on the <strong>Privacy</strong> selection in the left part of the window. (See image below)</p>\r\n            <p><img src=\"{{skin url=\"images/cookies/firefox.png\"}}\" alt=\"\" /></p>\r\n        </li>\r\n        <li>\r\n            <p>Expand the <strong>Cookies</strong> section</p>\r\n        </li>\r\n        <li>\r\n            <p>Check the <strong>Enable cookies</strong> and <strong>Accept cookies normally</strong> checkboxes</p>\r\n        </li>\r\n        <li>\r\n            <p>Save changes by clicking <strong>Ok</strong>.</p>\r\n        </li>\r\n    </ol>\r\n    <p class=\"a-top\"><a href=\"#top\">Back to Top</a></p>\r\n    <h3><a name=\"opera\"></a>Opera 7.x</h3>\r\n    <ol>\r\n        <li>\r\n            <p>Click on the <strong>Tools</strong> menu in Opera</p>\r\n        </li>\r\n        <li>\r\n            <p>Click on the <strong>Preferences...</strong> item in the menu - a new window open</p>\r\n        </li>\r\n        <li>\r\n            <p>Click on the <strong>Privacy</strong> selection near the bottom left of the window. (See image below)</p>\r\n            <p><img src=\"{{skin url=\"images/cookies/opera.png\"}}\" alt=\"\" /></p>\r\n        </li>\r\n        <li>\r\n            <p>The <strong>Enable cookies</strong> checkbox must be checked, and <strong>Accept all cookies</strong> should be selected in the &quot;<strong>Normal cookies</strong>&quot; drop-down</p>\r\n        </li>\r\n        <li>\r\n            <p>Save changes by clicking <strong>Ok</strong></p>\r\n        </li>\r\n    </ol>\r\n    <p class=\"a-top\"><a href=\"#top\">Back to Top</a></p>\r\n</div>\r\n','2011-08-08 12:55:02','2011-08-08 12:55:02',1,0,NULL,NULL,'',NULL,NULL,NULL),
 (6,'FAQ - Perguntas Frequentes','one_column','','','faq','FAQ','<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Evidentemente, a necessidade de renova&ccedil;&atilde;o processual pode nos  levar a considerar a reestrutura&ccedil;&atilde;o das diretrizes de desenvolvimento  para o futuro. A pr&aacute;tica cotidiana prova que o desenvolvimento cont&iacute;nuo  de distintas formas de atua&ccedil;&atilde;o assume importantes posi&ccedil;&otilde;es no  estabelecimento do retorno esperado a longo prazo. Nunca &eacute; demais  lembrar o peso e o significado destes problemas, uma vez que a constante  divulga&ccedil;&atilde;o das informa&ccedil;&otilde;es acarreta um processo de reformula&ccedil;&atilde;o e  moderniza&ccedil;&atilde;o do sistema de forma&ccedil;&atilde;o de quadros que corresponde &agrave;s  necessidades. As experi&ecirc;ncias acumuladas demonstram que a mobilidade dos  capitais internacionais representa uma abertura para a melhoria do  processo de comunica&ccedil;&atilde;o como um todo.</p>','2011-08-08 17:22:38','2011-08-08 17:22:38',1,0,'','','','',NULL,NULL),
 (7,'Quem Somos','one_column','','','quem-somos','Quem Somos','<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Caros amigos, a execu&ccedil;&atilde;o dos pontos do programa estimula a  padroniza&ccedil;&atilde;o dos conhecimentos estrat&eacute;gicos para atingir a excel&ecirc;ncia.  Por outro lado, a complexidade dos estudos efetuados obstaculiza a  aprecia&ccedil;&atilde;o da import&acirc;ncia do remanejamento dos quadros funcionais. A  n&iacute;vel organizacional, a cont&iacute;nua expans&atilde;o de nossa atividade exige a  precis&atilde;o e a defini&ccedil;&atilde;o das novas proposi&ccedil;&otilde;es. No entanto, n&atilde;o podemos  esquecer que a crescente influ&ecirc;ncia da m&iacute;dia auxilia a prepara&ccedil;&atilde;o e a  composi&ccedil;&atilde;o da gest&atilde;o inovadora da qual fazemos parte. <br /><br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Evidentemente,  a necessidade de renova&ccedil;&atilde;o processual pode nos levar a considerar a  reestrutura&ccedil;&atilde;o das diretrizes de desenvolvimento para o futuro. A  pr&aacute;tica cotidiana prova que o desenvolvimento cont&iacute;nuo de distintas  formas de atua&ccedil;&atilde;o assume importantes posi&ccedil;&otilde;es no estabelecimento do  retorno esperado a longo prazo. Nunca &eacute; demais lembrar o peso e o  significado destes problemas, uma vez que a constante divulga&ccedil;&atilde;o das  informa&ccedil;&otilde;es acarreta um processo de reformula&ccedil;&atilde;o e moderniza&ccedil;&atilde;o do  sistema de forma&ccedil;&atilde;o de quadros que corresponde &agrave;s necessidades. As  experi&ecirc;ncias acumuladas demonstram que a mobilidade dos capitais  internacionais representa uma abertura para a melhoria do processo de  comunica&ccedil;&atilde;o como um todo. <br /><br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>&nbsp;O cuidado em identificar  pontos cr&iacute;ticos </strong>na consulta aos diversos militantes talvez venha a  ressaltar a relatividade dos modos de opera&ccedil;&atilde;o convencionais. O empenho  em analisar a consolida&ccedil;&atilde;o das estruturas deve passar por modifica&ccedil;&otilde;es  independentemente das formas de a&ccedil;&atilde;o. N&atilde;o obstante, o novo modelo  estrutural aqui preconizado faz parte de um processo de gerenciamento  dos relacionamentos verticais entre as hierarquias. <br /><br /></p>\r\n<ul>\r\n<li>O  que temos que ter sempre em mente &eacute; que a valoriza&ccedil;&atilde;o de fatores  subjetivos cumpre um papel essencial na formula&ccedil;&atilde;o dos &iacute;ndices  pretendidos. </li>\r\n<li>Acima de tudo, &eacute; fundamental ressaltar que a estrutura  atual da organiza&ccedil;&atilde;o n&atilde;o pode mais se dissociar das dire&ccedil;&otilde;es  preferenciais no sentido do progresso. </li>\r\n<li>Podemos j&aacute; vislumbrar o modo pelo  qual a hegemonia do ambiente pol&iacute;tico &eacute; uma das consequ&ecirc;ncias dos  m&eacute;todos utilizados na avalia&ccedil;&atilde;o de resultados. </li>\r\n</ul>\r\n<p><br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;O  incentivo ao avan&ccedil;o tecnol&oacute;gico, assim como a expans&atilde;o dos mercados  mundiais oferece uma interessante oportunidade para verifica&ccedil;&atilde;o do  sistema de participa&ccedil;&atilde;o geral. Gostaria de enfatizar que o in&iacute;cio da  atividade geral de forma&ccedil;&atilde;o de atitudes maximiza as possibilidades por  conta dos n&iacute;veis de motiva&ccedil;&atilde;o departamental. Todavia, o entendimento das  metas propostas desafia a capacidade de equaliza&ccedil;&atilde;o das posturas dos  &oacute;rg&atilde;os dirigentes com rela&ccedil;&atilde;o &agrave;s suas atribui&ccedil;&otilde;es. Ainda assim, existem  d&uacute;vidas a respeito de como o fen&ocirc;meno da Internet nos obriga &agrave; an&aacute;lise  das condi&ccedil;&otilde;es inegavelmente apropriadas. Percebemos, cada vez mais, que a  competitividade nas transa&ccedil;&otilde;es comerciais prepara-nos para enfrentar  situa&ccedil;&otilde;es at&iacute;picas decorrentes de alternativas &agrave;s solu&ccedil;&otilde;es ortodoxas.</p>','2011-08-08 17:32:34','2011-08-08 19:05:39',1,0,'','','','',NULL,NULL),
 (8,'Como Comprar','one_column','','','como-comprar','Como Comprar','<p>Podemos j&aacute; vislumbrar o modo pelo qual o surgimento do com&eacute;rcio virtual  aponta para a melhoria dos conhecimentos estrat&eacute;gicos para atingir a  excel&ecirc;ncia. Assim mesmo, a competitividade nas transa&ccedil;&otilde;es comerciais  acarreta um processo de reformula&ccedil;&atilde;o e moderniza&ccedil;&atilde;o dos paradigmas  corporativos. Todavia, o acompanhamento das prefer&ecirc;ncias de consumo  promove a alavancagem do sistema de forma&ccedil;&atilde;o de quadros que corresponde  &agrave;s necessidades. As experi&ecirc;ncias acumuladas demonstram que a revolu&ccedil;&atilde;o  dos costumes obstaculiza a aprecia&ccedil;&atilde;o da import&acirc;ncia do or&ccedil;amento  setorial. Nunca &eacute; demais lembrar o peso e o significado destes  problemas, uma vez que o julgamento imparcial das eventualidades  representa uma abertura para a melhoria dos &iacute;ndices pretendidos.</p>','2011-08-11 17:36:08','2011-08-11 17:36:08',1,0,'','','','',NULL,NULL),
 (9,'Segurança','one_column','','','seguranca','Segurança','<p>O incentivo ao avan&ccedil;o tecnol&oacute;gico, assim como o fen&ocirc;meno da Internet  ainda n&atilde;o demonstrou convincentemente que vai participar na mudan&ccedil;a do  sistema de participa&ccedil;&atilde;o geral. N&atilde;o obstante, o desafiador cen&aacute;rio  globalizado exige a precis&atilde;o e a defini&ccedil;&atilde;o do levantamento das vari&aacute;veis  envolvidas. Pensando mais a longo prazo, a mobilidade dos capitais  internacionais faz parte de um processo de gerenciamento das condi&ccedil;&otilde;es  inegavelmente apropriadas. Ainda assim, existem d&uacute;vidas a respeito de  como a consulta aos diversos militantes cumpre um papel essencial na  formula&ccedil;&atilde;o dos procedimentos normalmente adotados.</p>','2011-08-11 17:36:42','2011-08-11 17:36:42',1,0,'','','','',NULL,NULL),
 (10,'Garantia e Trocas','one_column','','','garantia-e-trocas','Garantia e Trocas','<p>O que temos que ter sempre em mente &eacute; que a execu&ccedil;&atilde;o dos pontos do  programa estimula a padroniza&ccedil;&atilde;o das regras de conduta normativas. Todas  estas quest&otilde;es, devidamente ponderadas, levantam d&uacute;vidas sobre se a  expans&atilde;o dos mercados mundiais prepara-nos para enfrentar situa&ccedil;&otilde;es  at&iacute;picas decorrentes das novas proposi&ccedil;&otilde;es. Do mesmo modo, a ado&ccedil;&atilde;o de  pol&iacute;ticas descentralizadoras maximiza as possibilidades por conta dos  n&iacute;veis de motiva&ccedil;&atilde;o departamental. Neste sentido, a complexidade dos  estudos efetuados desafia a capacidade de equaliza&ccedil;&atilde;o do impacto na  agilidade decis&oacute;ria. A n&iacute;vel organizacional, o desenvolvimento cont&iacute;nuo  de distintas formas de atua&ccedil;&atilde;o afeta positivamente a correta previs&atilde;o  dos relacionamentos verticais entre as hierarquias.</p>','2011-08-11 17:37:21','2011-08-11 17:37:21',1,0,'','','','',NULL,NULL),
 (11,'Nossa Política de Privacidade','one_column','','','politica-de-privacidade','Políticas de Privacidade','<p>O cuidado em identificar pontos cr&iacute;ticos na necessidade de renova&ccedil;&atilde;o  processual nos obriga &agrave; an&aacute;lise do investimento em reciclagem t&eacute;cnica.  Acima de tudo, &eacute; fundamental ressaltar que o consenso sobre a  necessidade de qualifica&ccedil;&atilde;o agrega valor ao estabelecimento do  remanejamento dos quadros funcionais. As experi&ecirc;ncias acumuladas  demonstram que o novo modelo estrutural aqui preconizado prepara-nos  para enfrentar situa&ccedil;&otilde;es at&iacute;picas decorrentes do sistema de forma&ccedil;&atilde;o de  quadros que corresponde &agrave;s necessidades. A certifica&ccedil;&atilde;o de metodologias  que nos auxiliam a lidar com o desafiador cen&aacute;rio globalizado garante a  contribui&ccedil;&atilde;o de um grupo importante na determina&ccedil;&atilde;o das diretrizes de  desenvolvimento para o futuro.</p>','2011-08-11 17:38:17','2011-08-11 17:40:23',1,0,'','','','',NULL,NULL),
 (12,'Políticas de Entrega','one_column','','','politicas-de-entrega','Políticas de Entrega','<p>O cuidado em identificar pontos cr&iacute;ticos na necessidade de renova&ccedil;&atilde;o  processual nos obriga &agrave; an&aacute;lise do investimento em reciclagem t&eacute;cnica.  Acima de tudo, &eacute; fundamental ressaltar que o consenso sobre a  necessidade de qualifica&ccedil;&atilde;o agrega valor ao estabelecimento do  remanejamento dos quadros funcionais. As experi&ecirc;ncias acumuladas  demonstram que o novo modelo estrutural aqui preconizado prepara-nos  para enfrentar situa&ccedil;&otilde;es at&iacute;picas decorrentes do sistema de forma&ccedil;&atilde;o de  quadros que corresponde &agrave;s necessidades. A certifica&ccedil;&atilde;o de metodologias  que nos auxiliam a lidar com o desafiador cen&aacute;rio globalizado garante a  contribui&ccedil;&atilde;o de um grupo importante na determina&ccedil;&atilde;o das diretrizes de  desenvolvimento para o futuro.</p>','2011-08-11 17:39:06','2011-08-11 17:39:06',1,0,'','','','',NULL,NULL),
 (13,'Texto de \'Cadastre-se ou Acesse sua conta\'','one_column','','','cms-texto-cadastro','Ainda não tem uma conta?','<p>Ao criar uma conta na nossa loja, voc&ecirc; ser&aacute; capaz de fechar uma compra  com mais agilidade, cadastrar diversos endere&ccedil;os de entrega, acompanhar  seus pedidos, entre outras vantagens.</p>','2011-08-19 14:42:34','2011-08-19 14:44:14',1,0,'','','','',NULL,NULL);
UNLOCK TABLES;
/*!40000 ALTER TABLE `cms_page` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`cms_page_store`
--

DROP TABLE IF EXISTS `supernarede`.`cms_page_store`;
CREATE TABLE  `supernarede`.`cms_page_store` (
  `page_id` smallint(6) NOT NULL,
  `store_id` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`page_id`,`store_id`),
  KEY `FK_CMS_PAGE_STORE_STORE` (`store_id`),
  CONSTRAINT `FK_CMS_PAGE_STORE_PAGE` FOREIGN KEY (`page_id`) REFERENCES `cms_page` (`page_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CMS_PAGE_STORE_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS Pages to Stores';

--
-- Dumping data for table `supernarede`.`cms_page_store`
--

/*!40000 ALTER TABLE `cms_page_store` DISABLE KEYS */;
LOCK TABLES `cms_page_store` WRITE;
INSERT INTO `supernarede`.`cms_page_store` VALUES  (5,0),
 (1,1),
 (2,1),
 (6,1),
 (7,1),
 (8,1),
 (9,1),
 (10,1),
 (11,1),
 (12,1),
 (13,1);
UNLOCK TABLES;
/*!40000 ALTER TABLE `cms_page_store` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`core_cache`
--

DROP TABLE IF EXISTS `supernarede`.`core_cache`;
CREATE TABLE  `supernarede`.`core_cache` (
  `id` varchar(255) NOT NULL,
  `data` mediumblob,
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  `expire_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_EXPIRE_TIME` (`expire_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`core_cache`
--

/*!40000 ALTER TABLE `core_cache` DISABLE KEYS */;
LOCK TABLES `core_cache` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `core_cache` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`core_cache_option`
--

DROP TABLE IF EXISTS `supernarede`.`core_cache_option`;
CREATE TABLE  `supernarede`.`core_cache_option` (
  `code` varchar(32) NOT NULL,
  `value` tinyint(3) DEFAULT NULL,
  PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`core_cache_option`
--

/*!40000 ALTER TABLE `core_cache_option` DISABLE KEYS */;
LOCK TABLES `core_cache_option` WRITE;
INSERT INTO `supernarede`.`core_cache_option` VALUES  ('block_html',0),
 ('collections',0),
 ('config',0),
 ('config_api',0),
 ('eav',0),
 ('layout',0),
 ('translate',0);
UNLOCK TABLES;
/*!40000 ALTER TABLE `core_cache_option` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`core_cache_tag`
--

DROP TABLE IF EXISTS `supernarede`.`core_cache_tag`;
CREATE TABLE  `supernarede`.`core_cache_tag` (
  `tag` varchar(100) NOT NULL DEFAULT '',
  `cache_id` varchar(200) NOT NULL DEFAULT '',
  PRIMARY KEY (`tag`,`cache_id`),
  KEY `IDX_CACHE_ID` (`cache_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`core_cache_tag`
--

/*!40000 ALTER TABLE `core_cache_tag` DISABLE KEYS */;
LOCK TABLES `core_cache_tag` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `core_cache_tag` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`core_config_data`
--

DROP TABLE IF EXISTS `supernarede`.`core_config_data`;
CREATE TABLE  `supernarede`.`core_config_data` (
  `config_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `scope` enum('default','websites','stores','config') NOT NULL DEFAULT 'default',
  `scope_id` int(11) NOT NULL DEFAULT '0',
  `path` varchar(255) NOT NULL DEFAULT 'general',
  `value` text NOT NULL,
  PRIMARY KEY (`config_id`),
  UNIQUE KEY `config_scope` (`scope`,`scope_id`,`path`)
) ENGINE=InnoDB AUTO_INCREMENT=347 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`core_config_data`
--

/*!40000 ALTER TABLE `core_config_data` DISABLE KEYS */;
LOCK TABLES `core_config_data` WRITE;
INSERT INTO `supernarede`.`core_config_data` VALUES  (1,'default',0,'catalog/category/root_id','2'),
 (2,'default',0,'admin/dashboard/enable_charts','1'),
 (3,'default',0,'web/unsecure/base_url','http://192.168.0.57/super_na_rede/'),
 (4,'default',0,'web/secure/base_url','http://supernarede.local/'),
 (5,'default',0,'general/locale/code','pt_BR'),
 (6,'default',0,'general/locale/timezone','America/Sao_Paulo'),
 (7,'default',0,'currency/options/base','BRL'),
 (8,'default',0,'currency/options/default','BRL'),
 (9,'default',0,'currency/options/allow','BRL'),
 (10,'default',0,'general/country/default','BR'),
 (11,'default',0,'general/country/allow','BR'),
 (12,'default',0,'general/country/optional_zip_countries','BR'),
 (13,'default',0,'general/locale/firstday','0'),
 (14,'default',0,'general/locale/weekend','0,6'),
 (15,'default',0,'general/store_information/name','Super na Rede'),
 (16,'default',0,'general/store_information/phone','0800 000 8800'),
 (17,'default',0,'general/store_information/address',''),
 (18,'default',0,'design/package/name','default'),
 (19,'default',0,'design/package/ua_regexp','a:0:{}'),
 (20,'default',0,'design/theme/locale',''),
 (21,'default',0,'design/theme/template',''),
 (22,'default',0,'design/theme/template_ua_regexp','a:0:{}'),
 (23,'default',0,'design/theme/skin',''),
 (24,'default',0,'design/theme/skin_ua_regexp','a:0:{}'),
 (25,'default',0,'design/theme/layout',''),
 (26,'default',0,'design/theme/layout_ua_regexp','a:0:{}'),
 (27,'default',0,'design/theme/default',''),
 (28,'default',0,'design/theme/default_ua_regexp','a:0:{}'),
 (29,'default',0,'design/head/default_title','Super na Rede'),
 (30,'default',0,'design/head/title_prefix',''),
 (31,'default',0,'design/head/title_suffix','- Super na Rede'),
 (32,'default',0,'design/head/default_description','Colocar descricao aki!'),
 (33,'default',0,'design/head/default_keywords','Magento, JN2, E-commerce, loja feita pela JN2'),
 (34,'default',0,'design/head/default_robots','NOINDEX,NOFOLLOW'),
 (35,'default',0,'design/head/includes',''),
 (36,'default',0,'design/head/demonotice','0'),
 (37,'default',0,'design/header/logo_src','images/template/img_logotipo.png'),
 (38,'default',0,'design/header/logo_alt','Super na Rede'),
 (39,'default',0,'design/header/welcome','Olá seja bem vindo!'),
 (40,'default',0,'design/footer/copyright','Super na Rede &copy; 2011. Todos os direitos reservados.'),
 (41,'default',0,'design/footer/absolute_footer',''),
 (42,'default',0,'design/watermark/image_size',''),
 (43,'default',0,'design/watermark/image_imageOpacity',''),
 (44,'default',0,'design/watermark/image_position','stretch'),
 (45,'default',0,'design/watermark/small_image_size',''),
 (46,'default',0,'design/watermark/small_image_imageOpacity',''),
 (47,'default',0,'design/watermark/small_image_position','stretch'),
 (48,'default',0,'design/watermark/thumbnail_size',''),
 (49,'default',0,'design/watermark/thumbnail_imageOpacity',''),
 (50,'default',0,'design/watermark/thumbnail_position','stretch'),
 (51,'default',0,'design/pagination/pagination_frame','5'),
 (52,'default',0,'design/pagination/pagination_frame_skip',''),
 (53,'default',0,'design/pagination/anchor_text_for_previous',''),
 (54,'default',0,'design/pagination/anchor_text_for_next',''),
 (55,'default',0,'web/url/use_store','0'),
 (56,'default',0,'web/url/redirect_to_base','1'),
 (57,'default',0,'web/seo/use_rewrites','1'),
 (58,'default',0,'web/unsecure/base_link_url','{{unsecure_base_url}}'),
 (59,'default',0,'web/unsecure/base_skin_url','{{unsecure_base_url}}skin/'),
 (60,'default',0,'web/unsecure/base_media_url','{{unsecure_base_url}}media/'),
 (61,'default',0,'web/unsecure/base_js_url','{{unsecure_base_url}}js/'),
 (62,'default',0,'web/secure/base_link_url','{{secure_base_url}}'),
 (63,'default',0,'web/secure/base_skin_url','{{secure_base_url}}skin/'),
 (64,'default',0,'web/secure/base_media_url','{{secure_base_url}}media/'),
 (65,'default',0,'web/secure/base_js_url','{{secure_base_url}}js/'),
 (66,'default',0,'web/secure/use_in_frontend','0'),
 (67,'default',0,'web/secure/use_in_adminhtml','0'),
 (68,'default',0,'web/default/front','cms'),
 (69,'default',0,'web/default/cms_home_page','home'),
 (70,'default',0,'web/default/no_route','cms/index/noRoute'),
 (71,'default',0,'web/default/cms_no_route','no-route'),
 (72,'default',0,'web/default/cms_no_cookies','enable-cookies'),
 (73,'default',0,'web/default/show_cms_breadcrumbs','1'),
 (74,'default',0,'web/polls/poll_check_by_ip','0'),
 (75,'default',0,'web/cookie/cookie_lifetime','3600'),
 (76,'default',0,'web/cookie/cookie_path',''),
 (77,'default',0,'web/cookie/cookie_domain',''),
 (78,'default',0,'web/cookie/cookie_httponly','1'),
 (79,'default',0,'web/session/use_remote_addr','0'),
 (80,'default',0,'web/session/use_http_via','0'),
 (81,'default',0,'web/session/use_http_x_forwarded_for','0'),
 (82,'default',0,'web/session/use_http_user_agent','0'),
 (83,'default',0,'web/session/use_frontend_sid','1'),
 (84,'default',0,'web/browser_capabilities/cookies','1'),
 (85,'default',0,'web/browser_capabilities/javascript','1'),
 (86,'default',0,'contacts/contacts/enabled','1'),
 (87,'default',0,'contacts/email/recipient_email','leandro@jn2.com.br'),
 (88,'default',0,'contacts/email/sender_email_identity','general'),
 (89,'default',0,'contacts/email/email_template','contacts_email_email_template'),
 (90,'default',0,'catalog/frontend/list_mode','list'),
 (91,'default',0,'catalog/frontend/grid_per_page_values','9,15,30'),
 (92,'default',0,'catalog/frontend/grid_per_page','9'),
 (93,'default',0,'catalog/frontend/list_per_page_values','4,8,16,28'),
 (94,'default',0,'catalog/frontend/list_per_page','8'),
 (95,'default',0,'catalog/frontend/list_allow_all','0'),
 (96,'default',0,'catalog/frontend/default_sort_by','position'),
 (97,'default',0,'catalog/frontend/flat_catalog_category','0'),
 (98,'default',0,'catalog/frontend/flat_catalog_product','0'),
 (99,'default',0,'catalog/frontend/parse_url_directives','1'),
 (100,'default',0,'catalog/sitemap/tree_mode','0'),
 (101,'default',0,'catalog/sitemap/lines_perpage','30'),
 (102,'default',0,'catalog/review/allow_guest','1'),
 (103,'default',0,'catalog/productalert/allow_price','0'),
 (104,'default',0,'catalog/productalert/email_price_template','catalog_productalert_email_price_template'),
 (105,'default',0,'catalog/productalert/allow_stock','0'),
 (106,'default',0,'catalog/productalert/email_stock_template','catalog_productalert_email_stock_template'),
 (107,'default',0,'catalog/productalert/email_identity','general'),
 (108,'default',0,'catalog/productalert_cron/frequency','D'),
 (109,'default',0,'crontab/jobs/catalog_product_alert/schedule/cron_expr','0 0 * * *'),
 (110,'default',0,'crontab/jobs/catalog_product_alert/run/model','productalert/observer::process'),
 (111,'default',0,'catalog/productalert_cron/time','00,00,00'),
 (112,'default',0,'catalog/productalert_cron/error_email',''),
 (113,'default',0,'catalog/productalert_cron/error_email_identity','general'),
 (114,'default',0,'catalog/productalert_cron/error_email_template','catalog_productalert_cron_error_email_template'),
 (115,'default',0,'catalog/recently_products/scope','website'),
 (116,'default',0,'catalog/recently_products/viewed_count','5'),
 (117,'default',0,'catalog/recently_products/compared_count','5'),
 (118,'default',0,'catalog/price/scope','0'),
 (119,'default',0,'catalog/layered_navigation/price_range_calculation','auto'),
 (120,'default',0,'catalog/navigation/max_depth','0'),
 (121,'default',0,'catalog/seo/site_map','1'),
 (122,'default',0,'catalog/seo/search_terms','1'),
 (123,'default',0,'catalog/seo/product_url_suffix',''),
 (124,'default',0,'catalog/seo/category_url_suffix',''),
 (125,'default',0,'catalog/seo/product_use_categories','1'),
 (126,'default',0,'catalog/seo/save_rewrites_history','1'),
 (127,'default',0,'catalog/seo/title_separator','-'),
 (128,'default',0,'catalog/seo/category_canonical_tag','0'),
 (129,'default',0,'catalog/seo/product_canonical_tag','0'),
 (130,'default',0,'catalog/search/min_query_length','1'),
 (131,'default',0,'catalog/search/max_query_length','128'),
 (132,'default',0,'catalog/search/max_query_words','10'),
 (133,'default',0,'catalog/search/search_type','1'),
 (134,'default',0,'catalog/search/use_layered_navigation_count','2000'),
 (135,'default',0,'catalog/downloadable/order_item_status','9'),
 (136,'default',0,'catalog/downloadable/downloads_number','0'),
 (137,'default',0,'catalog/downloadable/shareable','0'),
 (138,'default',0,'catalog/downloadable/samples_title','Samples'),
 (139,'default',0,'catalog/downloadable/links_title','Links'),
 (140,'default',0,'catalog/downloadable/links_target_new_window','1'),
 (141,'default',0,'catalog/downloadable/content_disposition','inline'),
 (142,'default',0,'catalog/downloadable/disable_guest_checkout','1'),
 (143,'default',0,'catalog/custom_options/use_calendar','0'),
 (144,'default',0,'catalog/custom_options/date_fields_order','d,m,y'),
 (145,'default',0,'catalog/custom_options/time_format','24h'),
 (146,'default',0,'catalog/custom_options/year_range',','),
 (147,'default',0,'shipping/origin/country_id','BR'),
 (148,'default',0,'shipping/origin/region_id','496'),
 (149,'default',0,'shipping/origin/postcode','30180110'),
 (150,'default',0,'shipping/origin/city',''),
 (151,'default',0,'shipping/option/checkout_multiple','0'),
 (152,'default',0,'shipping/option/checkout_multiple_maximum_qty','100'),
 (153,'default',0,'checkout/options/onepage_checkout_enabled','1'),
 (154,'default',0,'checkout/options/guest_checkout','0'),
 (155,'default',0,'checkout/options/enable_agreements','0'),
 (156,'default',0,'checkout/cart/delete_quote_after','30'),
 (157,'default',0,'checkout/cart/redirect_to_cart','1'),
 (158,'default',0,'checkout/cart/grouped_product_image','itself'),
 (159,'default',0,'checkout/cart/configurable_product_image','parent'),
 (160,'default',0,'checkout/cart_link/use_qty','1'),
 (161,'default',0,'checkout/sidebar/display','1'),
 (162,'default',0,'checkout/sidebar/count','3'),
 (163,'default',0,'checkout/payment_failed/reciever','general'),
 (164,'default',0,'checkout/payment_failed/identity','general'),
 (165,'default',0,'checkout/payment_failed/template','checkout_payment_failed_template'),
 (166,'default',0,'checkout/payment_failed/copy_to',''),
 (167,'default',0,'checkout/payment_failed/copy_method','bcc'),
 (168,'default',0,'ibanners/carousel/duration','5'),
 (169,'default',0,'ibanners/carousel/frequency','fast'),
 (170,'default',0,'ibanners/carousel/effect','fade'),
 (171,'default',0,'j2tajaxcheckout/default/j2t_ajax_cart_loading_size','260x50'),
 (172,'default',0,'j2tajaxcheckout/default/j2t_ajax_cart_confirm_size','360x110'),
 (173,'default',0,'j2tajaxcheckout/default/j2t_ajax_cart_image_size','55x55'),
 (174,'default',0,'j2tajaxcheckout/default/j2t_ajax_cart_show_popup','1'),
 (175,'default',0,'ambase/feed/installed','1313786715'),
 (176,'default',0,'carriers/flatrate/active','1'),
 (177,'default',0,'carriers/flatrate/title','Frete VIP Rede'),
 (178,'default',0,'carriers/flatrate/name','Frete VIP Rede (Em até 3h Úteis. De 30 a 40 itens via motoboy)'),
 (179,'default',0,'carriers/flatrate/type','O'),
 (180,'default',0,'carriers/flatrate/price','12'),
 (181,'default',0,'carriers/flatrate/handling_type','F'),
 (182,'default',0,'carriers/flatrate/handling_fee',''),
 (183,'default',0,'carriers/flatrate/specificerrmsg','Este meio de entrega está temporariamente indisponível. Favor tentar novamente depois.'),
 (184,'default',0,'carriers/flatrate/sallowspecific','1'),
 (185,'default',0,'carriers/flatrate/specificcountry','BR'),
 (186,'default',0,'carriers/flatrate/showmethod','0'),
 (187,'default',0,'carriers/flatrate/sort_order',''),
 (188,'default',0,'carriers/tablerate/active','0'),
 (189,'default',0,'carriers/tablerate/title','Best Way'),
 (190,'default',0,'carriers/tablerate/name','Table Rate'),
 (191,'default',0,'carriers/tablerate/condition_name','package_value'),
 (192,'default',0,'carriers/tablerate/include_virtual_price','1'),
 (193,'default',0,'carriers/tablerate/handling_type','F'),
 (194,'default',0,'carriers/tablerate/handling_fee',''),
 (195,'default',0,'carriers/tablerate/specificerrmsg','This shipping method is currently unavailable. If you would like to ship using this shipping method, please contact us.'),
 (196,'default',0,'carriers/tablerate/sallowspecific','0'),
 (197,'default',0,'carriers/tablerate/specificcountry',''),
 (198,'default',0,'carriers/tablerate/showmethod','0'),
 (199,'default',0,'carriers/tablerate/sort_order',''),
 (200,'default',0,'carriers/freeshipping/active','1'),
 (201,'default',0,'carriers/freeshipping/title','Frete Grátis'),
 (202,'default',0,'carriers/freeshipping/name','Frete Grátis para entregas acima de R$ 150,00'),
 (203,'default',0,'carriers/freeshipping/free_shipping_subtotal','150'),
 (204,'default',0,'carriers/freeshipping/specificerrmsg','Este meio de entrega está temporariamente indisponível. Favor tentar novamente depois.'),
 (205,'default',0,'carriers/freeshipping/sallowspecific','0'),
 (206,'default',0,'carriers/freeshipping/specificcountry',''),
 (207,'default',0,'carriers/freeshipping/showmethod','0'),
 (208,'default',0,'carriers/freeshipping/sort_order',''),
 (209,'default',0,'carriers/ups/active','0'),
 (210,'default',0,'carriers/ups/type','UPS'),
 (211,'default',0,'carriers/ups/password',''),
 (212,'default',0,'carriers/ups/access_license_number',''),
 (213,'default',0,'carriers/ups/username',''),
 (214,'default',0,'carriers/ups/mode_xml','1'),
 (215,'default',0,'carriers/ups/gateway_xml_url','https://onlinetools.ups.com/ups.app/xml/Rate'),
 (216,'default',0,'carriers/ups/origin_shipment','Shipments Originating in United States'),
 (217,'default',0,'carriers/ups/negotiated_active','0'),
 (218,'default',0,'carriers/ups/gateway_url','http://www.ups.com/using/services/rave/qcostcgi.cgi'),
 (219,'default',0,'carriers/ups/title','United Parcel Service'),
 (220,'default',0,'carriers/ups/shipper_number',''),
 (221,'default',0,'carriers/ups/container','CP'),
 (222,'default',0,'carriers/ups/dest_type','RES'),
 (223,'default',0,'carriers/ups/unit_of_measure','LBS'),
 (224,'default',0,'carriers/ups/tracking_xml_url','https://www.ups.com/ups.app/xml/Track'),
 (225,'default',0,'carriers/ups/pickup','CC'),
 (226,'default',0,'carriers/ups/max_package_weight','150'),
 (227,'default',0,'carriers/ups/min_package_weight','0.1'),
 (228,'default',0,'carriers/ups/handling_type','F'),
 (229,'default',0,'carriers/ups/handling_action','O'),
 (230,'default',0,'carriers/ups/handling_fee',''),
 (231,'default',0,'carriers/ups/allowed_methods','1DM,1DML,1DA,1DAL,1DAPI,1DP,1DPL,2DM,2DML,2DA,2DAL,3DS,GND,GNDCOM,GNDRES,STD,XPR,WXS,XPRL,XDM,XDML,XPD'),
 (232,'default',0,'carriers/ups/free_method','GND'),
 (233,'default',0,'carriers/ups/free_shipping_enable','0'),
 (234,'default',0,'carriers/ups/free_shipping_subtotal',''),
 (235,'default',0,'carriers/ups/specificerrmsg','This shipping method is currently unavailable. If you would like to ship using this shipping method, please contact us.'),
 (236,'default',0,'carriers/ups/sallowspecific','0'),
 (237,'default',0,'carriers/ups/specificcountry',''),
 (238,'default',0,'carriers/ups/debug','0'),
 (239,'default',0,'carriers/ups/showmethod','0'),
 (240,'default',0,'carriers/ups/sort_order',''),
 (241,'default',0,'carriers/usps/active','0'),
 (242,'default',0,'carriers/usps/gateway_url','http://production.shippingapis.com/ShippingAPI.dll'),
 (243,'default',0,'carriers/usps/title','United States Postal Service'),
 (244,'default',0,'carriers/usps/userid',''),
 (245,'default',0,'carriers/usps/container','VARIABLE'),
 (246,'default',0,'carriers/usps/size','REGULAR'),
 (247,'default',0,'carriers/usps/machinable','true'),
 (248,'default',0,'carriers/usps/max_package_weight','70'),
 (249,'default',0,'carriers/usps/handling_type','F'),
 (250,'default',0,'carriers/usps/handling_action','O'),
 (251,'default',0,'carriers/usps/handling_fee',''),
 (252,'default',0,'carriers/usps/allowed_methods','Bound Printed Matter,Express Mail,Express Mail Flat Rate Envelope,Express Mail Flat Rate Envelope Hold For Pickup,Express Mail Flat-Rate Envelope Sunday/Holiday Guarantee,Express Mail Hold For Pickup,Express Mail International,Express Mail International Flat Rate Envelope,Express Mail PO to PO,Express Mail Sunday/Holiday Guarantee,First-Class Mail International Large Envelope,First-Class Mail International Letters,First-Class Mail International Package,First-Class,First-Class Mail,First-Class Mail Flat,First-Class Mail Large Envelope,First-Class Mail International,First-Class Mail Letter,First-Class Mail Parcel,First-Class Mail Package,Global Express Guaranteed (GXG),Global Express Guaranteed Non-Document Non-Rectangular,Global Express Guaranteed Non-Document Rectangular,Library Mail,Media Mail,Parcel Post,Priority Mail,Priority Mail Small Flat Rate Box,Priority Mail Medium Flat Rate Box,Priority Mail Large Flat Rate Box,Priority Mail Flat Rate Box,Priority Mail Flat Rate Envelope,Priority Mail International,Priority Mail International Flat Rate Box,Priority Mail International Flat Rate Envelope,Priority Mail International Small Flat Rate Box,Priority Mail International Medium Flat Rate Box,Priority Mail International Large Flat Rate Box,USPS GXG Envelopes'),
 (253,'default',0,'carriers/usps/free_method',''),
 (254,'default',0,'carriers/usps/free_shipping_enable','0'),
 (255,'default',0,'carriers/usps/free_shipping_subtotal',''),
 (256,'default',0,'carriers/usps/specificerrmsg','This shipping method is currently unavailable. If you would like to ship using this shipping method, please contact us.'),
 (257,'default',0,'carriers/usps/sallowspecific','0'),
 (258,'default',0,'carriers/usps/specificcountry',''),
 (259,'default',0,'carriers/usps/showmethod','0'),
 (260,'default',0,'carriers/usps/debug','0'),
 (261,'default',0,'carriers/usps/sort_order',''),
 (262,'default',0,'carriers/fedex/active','0'),
 (263,'default',0,'carriers/fedex/title','Federal Express'),
 (264,'default',0,'carriers/fedex/gateway_url','https://gateway.fedex.com/GatewayDC'),
 (265,'default',0,'carriers/fedex/account',''),
 (266,'default',0,'carriers/fedex/packaging','YOURPACKAGING'),
 (267,'default',0,'carriers/fedex/dropoff','REGULARPICKUP'),
 (268,'default',0,'carriers/fedex/max_package_weight','150'),
 (269,'default',0,'carriers/fedex/handling_type','F'),
 (270,'default',0,'carriers/fedex/handling_action','O'),
 (271,'default',0,'carriers/fedex/residence_delivery','0'),
 (272,'default',0,'carriers/fedex/handling_fee',''),
 (273,'default',0,'carriers/fedex/allowed_methods','PRIORITYOVERNIGHT,STANDARDOVERNIGHT,FIRSTOVERNIGHT,FEDEX2DAY,FEDEXEXPRESSSAVER,INTERNATIONALPRIORITY,INTERNATIONALECONOMY,INTERNATIONALFIRST,FEDEX1DAYFREIGHT,FEDEX2DAYFREIGHT,FEDEX3DAYFREIGHT,FEDEXGROUND,GROUNDHOMEDELIVERY,INTERNATIONALPRIORITY FREIGHT,INTERNATIONALECONOMY FREIGHT,EUROPEFIRSTINTERNATIONALPRIORITY'),
 (274,'default',0,'carriers/fedex/free_method','FEDEXGROUND'),
 (275,'default',0,'carriers/fedex/free_shipping_enable','0'),
 (276,'default',0,'carriers/fedex/free_shipping_subtotal',''),
 (277,'default',0,'carriers/fedex/specificerrmsg','This shipping method is currently unavailable. If you would like to ship using this shipping method, please contact us.'),
 (278,'default',0,'carriers/fedex/sallowspecific','0'),
 (279,'default',0,'carriers/fedex/specificcountry',''),
 (280,'default',0,'carriers/fedex/debug','0'),
 (281,'default',0,'carriers/fedex/showmethod','0'),
 (282,'default',0,'carriers/fedex/sort_order',''),
 (283,'default',0,'carriers/dhl/active','0'),
 (284,'default',0,'carriers/dhl/gateway_url','https://eCommerce.airborne.com/ApiLandingTest.asp'),
 (285,'default',0,'carriers/dhl/title','DHL'),
 (286,'default',0,'carriers/dhl/id',''),
 (287,'default',0,'carriers/dhl/password',''),
 (288,'default',0,'carriers/dhl/account',''),
 (289,'default',0,'carriers/dhl/shipping_intlkey',''),
 (290,'default',0,'carriers/dhl/shipping_key',''),
 (291,'default',0,'carriers/dhl/shipment_type','P'),
 (292,'default',0,'carriers/dhl/handling_type','F'),
 (293,'default',0,'carriers/dhl/handling_action','O'),
 (294,'default',0,'carriers/dhl/contentdesc','Big Box'),
 (295,'default',0,'carriers/dhl/handling_fee',''),
 (296,'default',0,'carriers/dhl/dutiable','0'),
 (297,'default',0,'carriers/dhl/max_package_weight','150'),
 (298,'default',0,'carriers/dhl/dutypaymenttype','R'),
 (299,'default',0,'carriers/dhl/allowed_methods','IE,E SAT,E 10:30AM,E,N,S,G'),
 (300,'default',0,'carriers/dhl/specificerrmsg','This shipping method is currently unavailable. If you would like to ship using this shipping method, please contact us.'),
 (301,'default',0,'carriers/dhl/free_method','G'),
 (302,'default',0,'carriers/dhl/free_shipping_enable','0'),
 (303,'default',0,'carriers/dhl/free_shipping_subtotal',''),
 (304,'default',0,'carriers/dhl/additional_protection_enabled','0'),
 (305,'default',0,'carriers/dhl/additional_protection_min_value',''),
 (306,'default',0,'carriers/dhl/additional_protection_use_subtotal','0'),
 (307,'default',0,'carriers/dhl/additional_protection_value',''),
 (308,'default',0,'carriers/dhl/additional_protection_rounding','0'),
 (309,'default',0,'carriers/dhl/hazardous_materials','0'),
 (310,'default',0,'carriers/dhl/default_length',''),
 (311,'default',0,'carriers/dhl/default_width',''),
 (312,'default',0,'carriers/dhl/default_height',''),
 (313,'default',0,'carriers/dhl/shipment_days','1,2,3,4,5,6'),
 (314,'default',0,'carriers/dhl/intl_shipment_days','1,2,3,4,5'),
 (315,'default',0,'carriers/dhl/sallowspecific','0'),
 (316,'default',0,'carriers/dhl/specificcountry',''),
 (317,'default',0,'carriers/dhl/showmethod','0'),
 (318,'default',0,'carriers/dhl/debug','0'),
 (319,'default',0,'carriers/dhl/sort_order',''),
 (320,'default',0,'customer/account_share/scope','1'),
 (321,'default',0,'customer/online_customers/online_minutes_interval',''),
 (322,'default',0,'customer/create_account/default_group','1'),
 (323,'default',0,'customer/create_account/email_domain','supernarede.com.br'),
 (324,'default',0,'customer/create_account/email_template','customer_create_account_email_template'),
 (325,'default',0,'customer/create_account/email_identity','general'),
 (326,'default',0,'customer/create_account/confirm','0'),
 (327,'default',0,'customer/create_account/email_confirmation_template','customer_create_account_email_confirmation_template'),
 (328,'default',0,'customer/create_account/email_confirmed_template','customer_create_account_email_confirmed_template'),
 (329,'default',0,'customer/create_account/generate_human_friendly_id','0'),
 (330,'default',0,'customer/password/forgot_email_template','customer_password_forgot_email_template'),
 (331,'default',0,'customer/password/forgot_email_identity','support'),
 (332,'default',0,'customer/address/street_lines','4'),
 (333,'default',0,'customer/address/prefix_show',''),
 (334,'default',0,'customer/address/prefix_options',''),
 (335,'default',0,'customer/address/middlename_show','0'),
 (336,'default',0,'customer/address/suffix_show',''),
 (337,'default',0,'customer/address/suffix_options',''),
 (338,'default',0,'customer/address/dob_show','req'),
 (339,'default',0,'customer/address/taxvat_show','req'),
 (340,'default',0,'customer/address/gender_show','req'),
 (341,'default',0,'customer/startup/redirect_dashboard','0'),
 (342,'default',0,'customer/address_templates/text','{{depend prefix}}{{var prefix}} {{/depend}}{{var firstname}} {{depend middlename}}{{var middlename}} {{/depend}}{{var lastname}}{{depend suffix}} {{var suffix}}{{/depend}}\r\n{{depend company}}{{var company}}{{/depend}}\r\n{{if street1}}{{var street1}}\r\n{{/if}}\r\n{{depend street2}}{{var street2}}{{/depend}}\r\n{{depend street3}}{{var street3}}{{/depend}}\r\n{{depend street4}}{{var street4}}{{/depend}}\r\n{{if city}}{{var city}},  {{/if}}{{if region}}{{var region}}, {{/if}}{{if postcode}}{{var postcode}}{{/if}}\r\n{{var country}}\r\nT: {{var telephone}}\r\n{{depend fax}}F: {{var fax}}{{/depend}}'),
 (343,'default',0,'customer/address_templates/oneline','{{depend prefix}}{{var prefix}} {{/depend}}{{var firstname}} {{depend middlename}}{{var middlename}} {{/depend}}{{var lastname}}{{depend suffix}} {{var suffix}}{{/depend}}, {{var street}}, {{var city}}, {{var region}} {{var postcode}}, {{var country}}'),
 (344,'default',0,'customer/address_templates/html','{{depend prefix}}{{var prefix}} {{/depend}}{{var firstname}} {{depend middlename}}{{var middlename}} {{/depend}}{{var lastname}}{{depend suffix}} {{var suffix}}{{/depend}}<br/>\r\n{{depend company}}{{var company}}<br />{{/depend}}\r\n{{if street1}}{{var street1}}<br />{{/if}}\r\n{{depend street2}}{{var street2}}<br />{{/depend}}\r\n{{depend street3}}{{var street3}}<br />{{/depend}}\r\n{{depend street4}}{{var street4}}<br />{{/depend}}\r\n{{if city}}{{var city}},  {{/if}}{{if region}}{{var region}}, {{/if}}{{if postcode}}{{var postcode}}{{/if}}<br/>\r\n{{var country}}<br/>\r\n{{depend telephone}}T: {{var telephone}}{{/depend}}\r\n{{depend fax}}<br/>F: {{var fax}}{{/depend}}'),
 (345,'default',0,'customer/address_templates/pdf','{{depend prefix}}{{var prefix}} {{/depend}}{{var firstname}} {{depend middlename}}{{var middlename}} {{/depend}}{{var lastname}}{{depend suffix}} {{var suffix}}{{/depend}}|\r\n{{depend company}}{{var company}}|{{/depend}}\r\n{{if street1}}{{var street1}}\r\n{{/if}}\r\n{{depend street2}}{{var street2}}|{{/depend}}\r\n{{depend street3}}{{var street3}}|{{/depend}}\r\n{{depend street4}}{{var street4}}|{{/depend}}\r\n{{if city}}{{var city}},  {{/if}}{{if region}}{{var region}}, {{/if}}{{if postcode}}{{var postcode}}{{/if}}|\r\n{{var country}}|\r\n{{depend telephone}}T: {{var telephone}}{{/depend}}|\r\n{{depend fax}}<br/>F: {{var fax}}{{/depend}}|'),
 (346,'default',0,'customer/address_templates/js_template','#{prefix} #{firstname} #{middlename} #{lastname} #{suffix}<br/>#{company}<br/>#{street0}<br/>#{street1}<br/>#{street2}<br/>#{street3}<br/>#{city}, #{region}, #{postcode}<br/>#{country_id}<br/>T: #{telephone}<br/>F: #{fax}');
UNLOCK TABLES;
/*!40000 ALTER TABLE `core_config_data` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`core_email_template`
--

DROP TABLE IF EXISTS `supernarede`.`core_email_template`;
CREATE TABLE  `supernarede`.`core_email_template` (
  `template_id` int(7) unsigned NOT NULL AUTO_INCREMENT,
  `template_code` varchar(150) DEFAULT NULL,
  `template_text` text,
  `template_styles` text,
  `template_type` int(3) unsigned DEFAULT NULL,
  `template_subject` varchar(200) DEFAULT NULL,
  `template_sender_name` varchar(200) DEFAULT NULL,
  `template_sender_email` varchar(200) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `added_at` datetime DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `orig_template_code` varchar(200) DEFAULT NULL,
  `orig_template_variables` text NOT NULL,
  PRIMARY KEY (`template_id`),
  UNIQUE KEY `template_code` (`template_code`),
  KEY `added_at` (`added_at`),
  KEY `modified_at` (`modified_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Email templates';

--
-- Dumping data for table `supernarede`.`core_email_template`
--

/*!40000 ALTER TABLE `core_email_template` DISABLE KEYS */;
LOCK TABLES `core_email_template` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `core_email_template` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`core_flag`
--

DROP TABLE IF EXISTS `supernarede`.`core_flag`;
CREATE TABLE  `supernarede`.`core_flag` (
  `flag_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `flag_code` varchar(255) NOT NULL,
  `state` smallint(5) unsigned NOT NULL DEFAULT '0',
  `flag_data` text,
  `last_update` datetime NOT NULL,
  PRIMARY KEY (`flag_id`),
  KEY `last_update` (`last_update`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`core_flag`
--

/*!40000 ALTER TABLE `core_flag` DISABLE KEYS */;
LOCK TABLES `core_flag` WRITE;
INSERT INTO `supernarede`.`core_flag` VALUES  (1,'admin_notification_survey',0,'a:1:{s:13:\"survey_viewed\";b:1;}','2011-08-08 12:58:33'),
 (2,'catalog_product_flat',0,'a:1:{s:8:\"is_built\";b:1;}','2011-08-17 16:37:43');
UNLOCK TABLES;
/*!40000 ALTER TABLE `core_flag` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`core_layout_link`
--

DROP TABLE IF EXISTS `supernarede`.`core_layout_link`;
CREATE TABLE  `supernarede`.`core_layout_link` (
  `layout_link_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `area` varchar(64) NOT NULL DEFAULT '',
  `package` varchar(64) NOT NULL DEFAULT '',
  `theme` varchar(64) NOT NULL DEFAULT '',
  `layout_update_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`layout_link_id`),
  UNIQUE KEY `store_id` (`store_id`,`package`,`theme`,`layout_update_id`),
  KEY `FK_core_layout_link_update` (`layout_update_id`),
  CONSTRAINT `FK_CORE_LAYOUT_LINK_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CORE_LAYOUT_LINK_UPDATE` FOREIGN KEY (`layout_update_id`) REFERENCES `core_layout_update` (`layout_update_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`core_layout_link`
--

/*!40000 ALTER TABLE `core_layout_link` DISABLE KEYS */;
LOCK TABLES `core_layout_link` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `core_layout_link` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`core_layout_update`
--

DROP TABLE IF EXISTS `supernarede`.`core_layout_update`;
CREATE TABLE  `supernarede`.`core_layout_update` (
  `layout_update_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `handle` varchar(255) DEFAULT NULL,
  `xml` text,
  `sort_order` smallint(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`layout_update_id`),
  KEY `handle` (`handle`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`core_layout_update`
--

/*!40000 ALTER TABLE `core_layout_update` DISABLE KEYS */;
LOCK TABLES `core_layout_update` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `core_layout_update` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`core_resource`
--

DROP TABLE IF EXISTS `supernarede`.`core_resource`;
CREATE TABLE  `supernarede`.`core_resource` (
  `code` varchar(50) NOT NULL DEFAULT '',
  `version` varchar(50) NOT NULL DEFAULT '',
  `data_version` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Resource version registry';

--
-- Dumping data for table `supernarede`.`core_resource`
--

/*!40000 ALTER TABLE `core_resource` DISABLE KEYS */;
LOCK TABLES `core_resource` WRITE;
INSERT INTO `supernarede`.`core_resource` VALUES  ('adminnotification_setup','1.0.0','1.0.0'),
 ('admin_setup','0.7.2','0.7.2'),
 ('ambase_setup','1.0.2','1.0.2'),
 ('amcustomerattr_setup','2.1.2','2.1.2'),
 ('api_setup','0.8.1','0.8.1'),
 ('backup_setup','0.7.0','0.7.0'),
 ('bundle_setup','0.1.14','0.1.14'),
 ('catalogindex_setup','0.7.10','0.7.10'),
 ('cataloginventory_setup','0.7.8','0.7.8'),
 ('catalogrule_setup','0.7.10','0.7.10'),
 ('catalogsearch_setup','0.7.7','0.7.7'),
 ('catalog_setup','1.4.0.0.44','1.4.0.0.44'),
 ('checkout_setup','0.9.5','0.9.5'),
 ('cms_setup','0.7.13','0.7.13'),
 ('compiler_setup','0.1.0','0.1.0'),
 ('contacts_setup','0.8.0','0.8.0'),
 ('core_setup','0.8.28','0.8.28'),
 ('cron_setup','0.7.1','0.7.1'),
 ('customer_setup','1.4.0.0.14','1.4.0.0.14'),
 ('dataflow_setup','0.7.4','0.7.4'),
 ('directory_setup','0.8.11','0.8.11'),
 ('downloadable_setup','1.4.0.3','1.4.0.3'),
 ('eav_setup','0.7.16','0.7.16'),
 ('find_feed_setup','0.0.2','0.0.2'),
 ('giftmessage_setup','0.7.6','0.7.6'),
 ('googlebase_setup','0.1.2','0.1.2'),
 ('googlecheckout_setup','0.7.4','0.7.4'),
 ('googleoptimizer_setup','0.1.2','0.1.2'),
 ('ibanners_setup','0.9.3','0.9.3'),
 ('importexport_setup','0.1.0','0.1.0'),
 ('index_setup','1.4.0.2','1.4.0.2'),
 ('log_setup','0.7.7','0.7.7'),
 ('moneybookers_setup','1.2.0.1','1.2.0.1'),
 ('newsletter_setup','0.8.3','0.8.3'),
 ('paygate_setup','0.7.1','0.7.1'),
 ('payment_setup','0.7.0','0.7.0'),
 ('paypaluk_setup','0.7.0','0.7.0'),
 ('paypal_setup','1.4.0.2','1.4.0.2'),
 ('poll_setup','0.7.2','0.7.2'),
 ('productalert_setup','0.7.2','0.7.2'),
 ('rating_setup','0.7.2','0.7.2'),
 ('reports_setup','0.7.10','0.7.10'),
 ('review_setup','0.7.6','0.7.6'),
 ('salesrule_setup','1.4.0.0.6','1.4.0.0.6'),
 ('sales_setup','1.4.0.25','1.4.0.25'),
 ('sendfriend_setup','0.7.4','0.7.4'),
 ('shipping_setup','0.7.0','0.7.0'),
 ('sitemap_setup','0.7.2','0.7.2'),
 ('tag_setup','0.7.7','0.7.7'),
 ('tax_setup','1.4.0.1','1.4.0.1'),
 ('usa_setup','0.7.1','0.7.1'),
 ('weee_setup','0.13','0.13'),
 ('widget_setup','1.4.0.0.0','1.4.0.0.0'),
 ('wishlist_setup','0.7.9','0.7.9'),
 ('xmlconnect_setup','1.4.0.13','1.4.0.13');
UNLOCK TABLES;
/*!40000 ALTER TABLE `core_resource` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`core_session`
--

DROP TABLE IF EXISTS `supernarede`.`core_session`;
CREATE TABLE  `supernarede`.`core_session` (
  `session_id` varchar(255) NOT NULL DEFAULT '',
  `website_id` smallint(5) unsigned DEFAULT NULL,
  `session_expires` int(10) unsigned NOT NULL DEFAULT '0',
  `session_data` mediumblob NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `FK_SESSION_WEBSITE` (`website_id`),
  CONSTRAINT `FK_SESSION_WEBSITE` FOREIGN KEY (`website_id`) REFERENCES `core_website` (`website_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Session data store';

--
-- Dumping data for table `supernarede`.`core_session`
--

/*!40000 ALTER TABLE `core_session` DISABLE KEYS */;
LOCK TABLES `core_session` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `core_session` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`core_store`
--

DROP TABLE IF EXISTS `supernarede`.`core_store`;
CREATE TABLE  `supernarede`.`core_store` (
  `store_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(32) NOT NULL DEFAULT '',
  `website_id` smallint(5) unsigned DEFAULT '0',
  `group_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `sort_order` smallint(5) unsigned NOT NULL DEFAULT '0',
  `is_active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`store_id`),
  UNIQUE KEY `code` (`code`),
  KEY `FK_STORE_WEBSITE` (`website_id`),
  KEY `is_active` (`is_active`,`sort_order`),
  KEY `FK_STORE_GROUP` (`group_id`),
  CONSTRAINT `FK_STORE_GROUP_STORE` FOREIGN KEY (`group_id`) REFERENCES `core_store_group` (`group_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_STORE_WEBSITE` FOREIGN KEY (`website_id`) REFERENCES `core_website` (`website_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Stores';

--
-- Dumping data for table `supernarede`.`core_store`
--

/*!40000 ALTER TABLE `core_store` DISABLE KEYS */;
LOCK TABLES `core_store` WRITE;
INSERT INTO `supernarede`.`core_store` VALUES  (0,'admin',0,0,'Admin',0,1),
 (1,'default',1,1,'Português (Brasil)',0,1);
UNLOCK TABLES;
/*!40000 ALTER TABLE `core_store` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`core_store_group`
--

DROP TABLE IF EXISTS `supernarede`.`core_store_group`;
CREATE TABLE  `supernarede`.`core_store_group` (
  `group_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `website_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `root_category_id` int(10) unsigned NOT NULL DEFAULT '0',
  `default_store_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`group_id`),
  KEY `FK_STORE_GROUP_WEBSITE` (`website_id`),
  KEY `default_store_id` (`default_store_id`),
  CONSTRAINT `FK_STORE_GROUP_WEBSITE` FOREIGN KEY (`website_id`) REFERENCES `core_website` (`website_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`core_store_group`
--

/*!40000 ALTER TABLE `core_store_group` DISABLE KEYS */;
LOCK TABLES `core_store_group` WRITE;
INSERT INTO `supernarede`.`core_store_group` VALUES  (0,0,'Default',0,0),
 (1,1,'Loja Principal',2,1);
UNLOCK TABLES;
/*!40000 ALTER TABLE `core_store_group` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`core_translate`
--

DROP TABLE IF EXISTS `supernarede`.`core_translate`;
CREATE TABLE  `supernarede`.`core_translate` (
  `key_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `string` varchar(255) NOT NULL DEFAULT '',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `translate` varchar(255) NOT NULL DEFAULT '',
  `locale` varchar(20) NOT NULL DEFAULT 'en_US',
  PRIMARY KEY (`key_id`),
  UNIQUE KEY `IDX_CODE` (`store_id`,`locale`,`string`),
  KEY `FK_CORE_TRANSLATE_STORE` (`store_id`),
  CONSTRAINT `FK_CORE_TRANSLATE_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Translation data';

--
-- Dumping data for table `supernarede`.`core_translate`
--

/*!40000 ALTER TABLE `core_translate` DISABLE KEYS */;
LOCK TABLES `core_translate` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `core_translate` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`core_url_rewrite`
--

DROP TABLE IF EXISTS `supernarede`.`core_url_rewrite`;
CREATE TABLE  `supernarede`.`core_url_rewrite` (
  `url_rewrite_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `category_id` int(10) unsigned DEFAULT NULL,
  `product_id` int(10) unsigned DEFAULT NULL,
  `id_path` varchar(255) NOT NULL DEFAULT '',
  `request_path` varchar(255) NOT NULL DEFAULT '',
  `target_path` varchar(255) NOT NULL DEFAULT '',
  `is_system` tinyint(1) unsigned DEFAULT '1',
  `options` varchar(255) NOT NULL DEFAULT '',
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`url_rewrite_id`),
  UNIQUE KEY `UNQ_REQUEST_PATH` (`request_path`,`store_id`),
  UNIQUE KEY `UNQ_PATH` (`id_path`,`is_system`,`store_id`),
  KEY `FK_CORE_URL_REWRITE_STORE` (`store_id`),
  KEY `IDX_ID_PATH` (`id_path`),
  KEY `IDX_TARGET_PATH` (`target_path`,`store_id`),
  KEY `FK_CORE_URL_REWRITE_PRODUCT` (`product_id`),
  KEY `IDX_CATEGORY_REWRITE` (`category_id`,`is_system`,`product_id`,`store_id`,`id_path`),
  CONSTRAINT `FK_CORE_URL_REWRITE_CATEGORY` FOREIGN KEY (`category_id`) REFERENCES `catalog_category_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CORE_URL_REWRITE_PRODUCT` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CORE_URL_REWRITE_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`core_url_rewrite`
--

/*!40000 ALTER TABLE `core_url_rewrite` DISABLE KEYS */;
LOCK TABLES `core_url_rewrite` WRITE;
INSERT INTO `supernarede`.`core_url_rewrite` VALUES  (1,1,3,NULL,'category/3','departamentos','catalog/category/view/id/3',1,'',NULL),
 (2,1,3,NULL,'68446300_1313083962','departamentos.html','departamentos',0,'RP',NULL),
 (3,1,4,NULL,'category/4','listas-prontas','catalog/category/view/id/4',1,'',NULL),
 (4,1,5,NULL,'category/5','listas-prontas/compra-do-mes','catalog/category/view/id/5',1,'',NULL),
 (5,1,6,NULL,'category/6','listas-prontas/somente-bebidas','catalog/category/view/id/6',1,'',NULL),
 (6,1,7,NULL,'category/7','departamentos/bebidas','catalog/category/view/id/7',1,'',NULL),
 (7,1,8,NULL,'category/8','departamentos/massa','catalog/category/view/id/8',1,'',NULL),
 (8,1,9,NULL,'category/9','departamentos/bebidas/cerveja','catalog/category/view/id/9',1,'',NULL),
 (9,1,10,NULL,'category/10','departamentos/bebidas/agua','catalog/category/view/id/10',1,'',NULL),
 (10,1,11,NULL,'category/11','departamentos/bebidas/refrigerante','catalog/category/view/id/11',1,'',NULL),
 (11,1,12,NULL,'category/12','departamentos/massa/espaguete','catalog/category/view/id/12',1,'',NULL),
 (12,1,13,NULL,'category/13','departamentos/massa/instantanea','catalog/category/view/id/13',1,'',NULL),
 (13,1,14,NULL,'category/14','departamentos/padaria','catalog/category/view/id/14',1,'',NULL),
 (14,1,15,NULL,'category/15','destaques','catalog/category/view/id/15',1,'',NULL),
 (15,1,NULL,1,'product/1','cerveja-exemplo-sem-alcool','catalog/product/view/id/1',1,'',NULL),
 (16,1,3,1,'product/1/3','departamentos/cerveja-exemplo-sem-alcool','catalog/product/view/id/1/category/3',1,'',NULL),
 (17,1,4,1,'product/1/4','listas-prontas/cerveja-exemplo-sem-alcool','catalog/product/view/id/1/category/4',1,'',NULL),
 (18,1,6,1,'product/1/6','listas-prontas/somente-bebidas/cerveja-exemplo-sem-alcool','catalog/product/view/id/1/category/6',1,'',NULL),
 (19,1,7,1,'product/1/7','departamentos/bebidas/cerveja-exemplo-sem-alcool','catalog/product/view/id/1/category/7',1,'',NULL),
 (20,1,9,1,'product/1/9','departamentos/bebidas/cerveja/cerveja-exemplo-sem-alcool','catalog/product/view/id/1/category/9',1,'',NULL),
 (21,1,15,1,'product/1/15','destaques/cerveja-exemplo-sem-alcool','catalog/product/view/id/1/category/15',1,'',NULL),
 (22,1,NULL,2,'product/2','cerveja-exemplo-bavaria','catalog/product/view/id/2',1,'',NULL),
 (23,1,3,2,'product/2/3','departamentos/cerveja-exemplo-bavaria','catalog/product/view/id/2/category/3',1,'',NULL),
 (24,1,7,2,'product/2/7','departamentos/bebidas/cerveja-exemplo-bavaria','catalog/product/view/id/2/category/7',1,'',NULL),
 (25,1,9,2,'product/2/9','departamentos/bebidas/cerveja/cerveja-exemplo-bavaria','catalog/product/view/id/2/category/9',1,'',NULL),
 (26,1,15,2,'product/2/15','destaques/cerveja-exemplo-bavaria','catalog/product/view/id/2/category/15',1,'',NULL),
 (27,1,NULL,3,'product/3','cerveja-exemplo-heineken-long','catalog/product/view/id/3',1,'',NULL),
 (28,1,3,3,'product/3/3','departamentos/cerveja-exemplo-heineken-long','catalog/product/view/id/3/category/3',1,'',NULL),
 (29,1,7,3,'product/3/7','departamentos/bebidas/cerveja-exemplo-heineken-long','catalog/product/view/id/3/category/7',1,'',NULL),
 (30,1,9,3,'product/3/9','departamentos/bebidas/cerveja/cerveja-exemplo-heineken-long','catalog/product/view/id/3/category/9',1,'',NULL),
 (31,1,15,3,'product/3/15','destaques/cerveja-exemplo-heineken-long','catalog/product/view/id/3/category/15',1,'',NULL),
 (32,1,NULL,4,'product/4','cerveja-exemplo-heineken-long-1','catalog/product/view/id/4',1,'',NULL),
 (33,1,3,4,'product/4/3','departamentos/cerveja-exemplo-heineken-long-1','catalog/product/view/id/4/category/3',1,'',NULL),
 (34,1,7,4,'product/4/7','departamentos/bebidas/cerveja-exemplo-heineken-long-1','catalog/product/view/id/4/category/7',1,'',NULL),
 (35,1,9,4,'product/4/9','departamentos/bebidas/cerveja/cerveja-exemplo-heineken-long-1','catalog/product/view/id/4/category/9',1,'',NULL),
 (36,1,15,4,'product/4/15','destaques/cerveja-exemplo-heineken-long-1','catalog/product/view/id/4/category/15',1,'',NULL),
 (37,1,NULL,5,'product/5','cerveja-exemplo-skol-escura','catalog/product/view/id/5',1,'',NULL),
 (38,1,3,5,'product/5/3','departamentos/cerveja-exemplo-skol-escura','catalog/product/view/id/5/category/3',1,'',NULL),
 (39,1,7,5,'product/5/7','departamentos/bebidas/cerveja-exemplo-skol-escura','catalog/product/view/id/5/category/7',1,'',NULL),
 (40,1,9,5,'product/5/9','departamentos/bebidas/cerveja/cerveja-exemplo-skol-escura','catalog/product/view/id/5/category/9',1,'',NULL),
 (41,1,15,5,'product/5/15','destaques/cerveja-exemplo-skol-escura','catalog/product/view/id/5/category/15',1,'',NULL),
 (42,1,NULL,6,'product/6','cesta-alcoologica','catalog/product/view/id/6',1,'',NULL),
 (43,1,4,6,'product/6/4','listas-prontas/cesta-alcoologica','catalog/product/view/id/6/category/4',1,'',NULL),
 (44,1,6,6,'product/6/6','listas-prontas/somente-bebidas/cesta-alcoologica','catalog/product/view/id/6/category/6',1,'',NULL),
 (45,1,NULL,4,'76457700_1313612809','cerveja-exemplo-heineken-long-4','cerveja-exemplo-heineken-long-1',0,'RP',NULL),
 (46,1,15,4,'91429500_1313612809','destaques/cerveja-exemplo-heineken-long-4','destaques/cerveja-exemplo-heineken-long-1',0,'RP',NULL),
 (47,1,3,4,'19710900_1313612810','departamentos/cerveja-exemplo-heineken-long-4','departamentos/cerveja-exemplo-heineken-long-1',0,'RP',NULL),
 (48,1,7,4,'34757700_1313612810','departamentos/bebidas/cerveja-exemplo-heineken-long-4','departamentos/bebidas/cerveja-exemplo-heineken-long-1',0,'RP',NULL),
 (49,1,9,4,'48091900_1313612810','departamentos/bebidas/cerveja/cerveja-exemplo-heineken-long-4','departamentos/bebidas/cerveja/cerveja-exemplo-heineken-long-1',0,'RP',NULL),
 (53,1,18,NULL,'category/18','receitas','catalog/category/view/id/18',1,'',NULL),
 (54,1,19,NULL,'category/19','receitas/grupo-qualquer','catalog/category/view/id/19',1,'',NULL),
 (55,1,19,6,'product/6/19','receitas/grupo-qualquer/cesta-alcoologica','catalog/product/view/id/6/category/19',1,'',NULL);
UNLOCK TABLES;
/*!40000 ALTER TABLE `core_url_rewrite` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`core_variable`
--

DROP TABLE IF EXISTS `supernarede`.`core_variable`;
CREATE TABLE  `supernarede`.`core_variable` (
  `variable_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`variable_id`),
  UNIQUE KEY `IDX_CODE` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`core_variable`
--

/*!40000 ALTER TABLE `core_variable` DISABLE KEYS */;
LOCK TABLES `core_variable` WRITE;
INSERT INTO `supernarede`.`core_variable` VALUES  (1,'txt_pergunta_cep','Texto informando ao cliente sobre a faixa de CEP'),
 (2,'faixa_cep','Faixa de CEP');
UNLOCK TABLES;
/*!40000 ALTER TABLE `core_variable` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`core_variable_value`
--

DROP TABLE IF EXISTS `supernarede`.`core_variable_value`;
CREATE TABLE  `supernarede`.`core_variable_value` (
  `value_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `variable_id` int(11) unsigned NOT NULL DEFAULT '0',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `plain_value` text NOT NULL,
  `html_value` text NOT NULL,
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `IDX_VARIABLE_STORE` (`variable_id`,`store_id`),
  KEY `IDX_VARIABLE_ID` (`variable_id`),
  KEY `IDX_STORE_ID` (`store_id`),
  CONSTRAINT `FK_CORE_VARIABLE_VALUE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CORE_VARIABLE_VALUE_VARIABLE_ID` FOREIGN KEY (`variable_id`) REFERENCES `core_variable` (`variable_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`core_variable_value`
--

/*!40000 ALTER TABLE `core_variable_value` DISABLE KEYS */;
LOCK TABLES `core_variable_value` WRITE;
INSERT INTO `supernarede`.`core_variable_value` VALUES  (1,1,0,'','<p>Informe seu CEP de entrega para sabermos se podemos te atender</p>'),
 (4,2,0,'30000001-31999999\r\n36400000','Informe a mensagem de CEP inválido, utilizando asterisco no início e outro no final:\r\n*Seu CEP é inválido. Infelizmente não atendemos sua região ainda.*\r\n\r\nExemplo de faixas para o campo Variável Valor Plain (Favor não deixar linhas em branco):\r\n30000000-35000000\r\n36000000-40000000\r\n30180110');
UNLOCK TABLES;
/*!40000 ALTER TABLE `core_variable_value` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`core_website`
--

DROP TABLE IF EXISTS `supernarede`.`core_website`;
CREATE TABLE  `supernarede`.`core_website` (
  `website_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(64) NOT NULL DEFAULT '',
  `sort_order` smallint(5) unsigned NOT NULL DEFAULT '0',
  `default_group_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `is_default` tinyint(1) unsigned DEFAULT '0',
  PRIMARY KEY (`website_id`),
  UNIQUE KEY `code` (`code`),
  KEY `sort_order` (`sort_order`),
  KEY `default_group_id` (`default_group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Websites';

--
-- Dumping data for table `supernarede`.`core_website`
--

/*!40000 ALTER TABLE `core_website` DISABLE KEYS */;
LOCK TABLES `core_website` WRITE;
INSERT INTO `supernarede`.`core_website` VALUES  (0,'admin','Admin',0,0,0),
 (1,'base','Website Principal',0,1,1);
UNLOCK TABLES;
/*!40000 ALTER TABLE `core_website` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`coupon_aggregated`
--

DROP TABLE IF EXISTS `supernarede`.`coupon_aggregated`;
CREATE TABLE  `supernarede`.`coupon_aggregated` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `period` date NOT NULL DEFAULT '0000-00-00',
  `store_id` smallint(5) unsigned DEFAULT NULL,
  `order_status` varchar(50) NOT NULL DEFAULT '',
  `coupon_code` varchar(50) NOT NULL DEFAULT '',
  `coupon_uses` int(11) NOT NULL DEFAULT '0',
  `subtotal_amount` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `discount_amount` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `total_amount` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `subtotal_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `discount_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `total_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNQ_COUPON_AGGREGATED_PSOC` (`period`,`store_id`,`order_status`,`coupon_code`),
  KEY `IDX_STORE_ID` (`store_id`),
  CONSTRAINT `FK_SALESTRULE_COUPON_AGGREGATED_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`coupon_aggregated`
--

/*!40000 ALTER TABLE `coupon_aggregated` DISABLE KEYS */;
LOCK TABLES `coupon_aggregated` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `coupon_aggregated` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`coupon_aggregated_order`
--

DROP TABLE IF EXISTS `supernarede`.`coupon_aggregated_order`;
CREATE TABLE  `supernarede`.`coupon_aggregated_order` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `period` date NOT NULL DEFAULT '0000-00-00',
  `store_id` smallint(5) unsigned DEFAULT NULL,
  `order_status` varchar(50) NOT NULL DEFAULT '',
  `coupon_code` varchar(50) NOT NULL DEFAULT '',
  `coupon_uses` int(11) NOT NULL DEFAULT '0',
  `subtotal_amount` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `discount_amount` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `total_amount` decimal(12,4) NOT NULL DEFAULT '0.0000',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNQ_COUPON_AGGREGATED_ORDER_PSOC` (`period`,`store_id`,`order_status`,`coupon_code`),
  KEY `IDX_STORE_ID` (`store_id`),
  CONSTRAINT `FK_SALESTRULE_COUPON_AGGREGATED_ORDER_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`coupon_aggregated_order`
--

/*!40000 ALTER TABLE `coupon_aggregated_order` DISABLE KEYS */;
LOCK TABLES `coupon_aggregated_order` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `coupon_aggregated_order` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`cron_schedule`
--

DROP TABLE IF EXISTS `supernarede`.`cron_schedule`;
CREATE TABLE  `supernarede`.`cron_schedule` (
  `schedule_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `job_code` varchar(255) NOT NULL DEFAULT '0',
  `status` enum('pending','running','success','missed','error') NOT NULL DEFAULT 'pending',
  `messages` text,
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `scheduled_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `executed_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `finished_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`schedule_id`),
  KEY `task_name` (`job_code`),
  KEY `scheduled_at` (`scheduled_at`,`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`cron_schedule`
--

/*!40000 ALTER TABLE `cron_schedule` DISABLE KEYS */;
LOCK TABLES `cron_schedule` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `cron_schedule` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`customer_address_entity`
--

DROP TABLE IF EXISTS `supernarede`.`customer_address_entity`;
CREATE TABLE  `supernarede`.`customer_address_entity` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entity_type_id` smallint(8) unsigned NOT NULL DEFAULT '0',
  `attribute_set_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `increment_id` varchar(50) NOT NULL DEFAULT '',
  `parent_id` int(10) unsigned DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`entity_id`),
  KEY `FK_CUSTOMER_ADDRESS_CUSTOMER_ID` (`parent_id`),
  CONSTRAINT `FK_CUSTOMER_ADDRESS_CUSTOMER_ID` FOREIGN KEY (`parent_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='Customer Address Entities';

--
-- Dumping data for table `supernarede`.`customer_address_entity`
--

/*!40000 ALTER TABLE `customer_address_entity` DISABLE KEYS */;
LOCK TABLES `customer_address_entity` WRITE;
INSERT INTO `supernarede`.`customer_address_entity` VALUES  (1,2,0,'',1,'2011-09-22 15:39:11','2011-09-22 15:39:12',1);
UNLOCK TABLES;
/*!40000 ALTER TABLE `customer_address_entity` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`customer_address_entity_datetime`
--

DROP TABLE IF EXISTS `supernarede`.`customer_address_entity_datetime`;
CREATE TABLE  `supernarede`.`customer_address_entity_datetime` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT,
  `entity_type_id` smallint(8) unsigned NOT NULL DEFAULT '0',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0',
  `value` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `IDX_ATTRIBUTE_VALUE` (`entity_id`,`attribute_id`),
  KEY `FK_CUSTOMER_ADDRESS_DATETIME_ENTITY_TYPE` (`entity_type_id`),
  KEY `FK_CUSTOMER_ADDRESS_DATETIME_ATTRIBUTE` (`attribute_id`),
  KEY `FK_CUSTOMER_ADDRESS_DATETIME_ENTITY` (`entity_id`),
  KEY `IDX_VALUE` (`entity_id`,`attribute_id`,`value`),
  CONSTRAINT `FK_CUSTOMER_ADDRESS_DATETIME_ATTRIBUTE` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CUSTOMER_ADDRESS_DATETIME_ENTITY` FOREIGN KEY (`entity_id`) REFERENCES `customer_address_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CUSTOMER_ADDRESS_DATETIME_ENTITY_TYPE` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`customer_address_entity_datetime`
--

/*!40000 ALTER TABLE `customer_address_entity_datetime` DISABLE KEYS */;
LOCK TABLES `customer_address_entity_datetime` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `customer_address_entity_datetime` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`customer_address_entity_decimal`
--

DROP TABLE IF EXISTS `supernarede`.`customer_address_entity_decimal`;
CREATE TABLE  `supernarede`.`customer_address_entity_decimal` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT,
  `entity_type_id` smallint(8) unsigned NOT NULL DEFAULT '0',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0',
  `value` decimal(12,4) NOT NULL DEFAULT '0.0000',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `IDX_ATTRIBUTE_VALUE` (`entity_id`,`attribute_id`),
  KEY `FK_CUSTOMER_ADDRESS_DECIMAL_ENTITY_TYPE` (`entity_type_id`),
  KEY `FK_CUSTOMER_ADDRESS_DECIMAL_ATTRIBUTE` (`attribute_id`),
  KEY `FK_CUSTOMER_ADDRESS_DECIMAL_ENTITY` (`entity_id`),
  KEY `IDX_VALUE` (`entity_id`,`attribute_id`,`value`),
  CONSTRAINT `FK_CUSTOMER_ADDRESS_DECIMAL_ATTRIBUTE` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CUSTOMER_ADDRESS_DECIMAL_ENTITY` FOREIGN KEY (`entity_id`) REFERENCES `customer_address_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CUSTOMER_ADDRESS_DECIMAL_ENTITY_TYPE` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`customer_address_entity_decimal`
--

/*!40000 ALTER TABLE `customer_address_entity_decimal` DISABLE KEYS */;
LOCK TABLES `customer_address_entity_decimal` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `customer_address_entity_decimal` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`customer_address_entity_int`
--

DROP TABLE IF EXISTS `supernarede`.`customer_address_entity_int`;
CREATE TABLE  `supernarede`.`customer_address_entity_int` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT,
  `entity_type_id` smallint(8) unsigned NOT NULL DEFAULT '0',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0',
  `value` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `IDX_ATTRIBUTE_VALUE` (`entity_id`,`attribute_id`),
  KEY `FK_CUSTOMER_ADDRESS_INT_ENTITY_TYPE` (`entity_type_id`),
  KEY `FK_CUSTOMER_ADDRESS_INT_ATTRIBUTE` (`attribute_id`),
  KEY `FK_CUSTOMER_ADDRESS_INT_ENTITY` (`entity_id`),
  KEY `IDX_VALUE` (`entity_id`,`attribute_id`,`value`),
  CONSTRAINT `FK_CUSTOMER_ADDRESS_INT_ATTRIBUTE` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CUSTOMER_ADDRESS_INT_ENTITY` FOREIGN KEY (`entity_id`) REFERENCES `customer_address_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CUSTOMER_ADDRESS_INT_ENTITY_TYPE` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`customer_address_entity_int`
--

/*!40000 ALTER TABLE `customer_address_entity_int` DISABLE KEYS */;
LOCK TABLES `customer_address_entity_int` WRITE;
INSERT INTO `supernarede`.`customer_address_entity_int` VALUES  (1,2,28,1,496);
UNLOCK TABLES;
/*!40000 ALTER TABLE `customer_address_entity_int` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`customer_address_entity_text`
--

DROP TABLE IF EXISTS `supernarede`.`customer_address_entity_text`;
CREATE TABLE  `supernarede`.`customer_address_entity_text` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT,
  `entity_type_id` smallint(8) unsigned NOT NULL DEFAULT '0',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0',
  `value` text NOT NULL,
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `IDX_ATTRIBUTE_VALUE` (`entity_id`,`attribute_id`),
  KEY `FK_CUSTOMER_ADDRESS_TEXT_ENTITY_TYPE` (`entity_type_id`),
  KEY `FK_CUSTOMER_ADDRESS_TEXT_ATTRIBUTE` (`attribute_id`),
  KEY `FK_CUSTOMER_ADDRESS_TEXT_ENTITY` (`entity_id`),
  CONSTRAINT `FK_CUSTOMER_ADDRESS_TEXT_ATTRIBUTE` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CUSTOMER_ADDRESS_TEXT_ENTITY` FOREIGN KEY (`entity_id`) REFERENCES `customer_address_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CUSTOMER_ADDRESS_TEXT_ENTITY_TYPE` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`customer_address_entity_text`
--

/*!40000 ALTER TABLE `customer_address_entity_text` DISABLE KEYS */;
LOCK TABLES `customer_address_entity_text` WRITE;
INSERT INTO `supernarede`.`customer_address_entity_text` VALUES  (1,2,24,1,'Rua Domingos Vieira\n587\nSanta Efigenia');
UNLOCK TABLES;
/*!40000 ALTER TABLE `customer_address_entity_text` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`customer_address_entity_varchar`
--

DROP TABLE IF EXISTS `supernarede`.`customer_address_entity_varchar`;
CREATE TABLE  `supernarede`.`customer_address_entity_varchar` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT,
  `entity_type_id` smallint(8) unsigned NOT NULL DEFAULT '0',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0',
  `value` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `IDX_ATTRIBUTE_VALUE` (`entity_id`,`attribute_id`),
  KEY `FK_CUSTOMER_ADDRESS_VARCHAR_ENTITY_TYPE` (`entity_type_id`),
  KEY `FK_CUSTOMER_ADDRESS_VARCHAR_ATTRIBUTE` (`attribute_id`),
  KEY `FK_CUSTOMER_ADDRESS_VARCHAR_ENTITY` (`entity_id`),
  KEY `IDX_VALUE` (`entity_id`,`attribute_id`,`value`),
  CONSTRAINT `FK_CUSTOMER_ADDRESS_VARCHAR_ATTRIBUTE` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CUSTOMER_ADDRESS_VARCHAR_ENTITY` FOREIGN KEY (`entity_id`) REFERENCES `customer_address_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CUSTOMER_ADDRESS_VARCHAR_ENTITY_TYPE` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`customer_address_entity_varchar`
--

/*!40000 ALTER TABLE `customer_address_entity_varchar` DISABLE KEYS */;
LOCK TABLES `customer_address_entity_varchar` WRITE;
INSERT INTO `supernarede`.`customer_address_entity_varchar` VALUES  (1,2,19,1,'Leandro'),
 (2,2,21,1,'JN2'),
 (3,2,23,1,'Casa'),
 (4,2,25,1,'Belo Horizonte'),
 (5,2,26,1,'BR'),
 (6,2,27,1,''),
 (7,2,29,1,''),
 (8,2,30,1,'(33) 3333-3333'),
 (9,2,31,1,'');
UNLOCK TABLES;
/*!40000 ALTER TABLE `customer_address_entity_varchar` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`customer_eav_attribute`
--

DROP TABLE IF EXISTS `supernarede`.`customer_eav_attribute`;
CREATE TABLE  `supernarede`.`customer_eav_attribute` (
  `attribute_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `is_visible` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `input_filter` varchar(255) DEFAULT NULL,
  `multiline_count` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `validate_rules` text,
  `is_system` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `sort_order` int(10) unsigned NOT NULL DEFAULT '0',
  `data_model` varchar(255) DEFAULT NULL,
  `is_filterable_in_search` tinyint(1) unsigned NOT NULL,
  `used_in_product_listing` tinyint(1) unsigned NOT NULL,
  `store_ids` varchar(255) NOT NULL,
  `sorting_order` smallint(5) unsigned NOT NULL,
  `is_visible_on_front` tinyint(1) unsigned NOT NULL,
  `type_internal` varchar(255) NOT NULL,
  PRIMARY KEY (`attribute_id`),
  CONSTRAINT `FK_CUSTOMER_EAV_ATTRIBUTE_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=130 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`customer_eav_attribute`
--

/*!40000 ALTER TABLE `customer_eav_attribute` DISABLE KEYS */;
LOCK TABLES `customer_eav_attribute` WRITE;
INSERT INTO `supernarede`.`customer_eav_attribute` VALUES  (1,1,'',0,NULL,1,10,NULL,0,0,'',0,0,''),
 (2,0,'',0,NULL,1,0,NULL,0,0,'',0,0,''),
 (3,1,'',0,NULL,1,20,NULL,0,0,'',0,0,''),
 (4,0,'',0,NULL,0,30,NULL,0,0,'',0,0,''),
 (5,1,'',0,'a:2:{s:15:\"max_text_length\";i:255;s:15:\"min_text_length\";i:1;}',1,40,NULL,0,0,'',0,0,''),
 (6,0,'',0,NULL,0,50,NULL,0,0,'',0,0,''),
 (7,1,'',0,'a:2:{s:15:\"max_text_length\";i:255;s:15:\"min_text_length\";i:1;}',1,60,NULL,0,0,'',0,0,''),
 (8,0,'',0,NULL,0,70,NULL,0,0,'',0,0,''),
 (9,1,'',0,'a:1:{s:16:\"input_validation\";s:5:\"email\";}',1,80,NULL,0,0,'',0,0,''),
 (10,1,'',0,NULL,1,25,NULL,0,0,'',0,0,''),
 (11,1,'date',0,'a:1:{s:16:\"input_validation\";s:4:\"date\";}',0,90,NULL,0,0,'',0,0,''),
 (12,0,'',0,NULL,1,0,NULL,0,0,'',0,0,''),
 (13,0,'',0,NULL,1,0,NULL,0,0,'',0,0,''),
 (14,0,'',0,NULL,1,0,NULL,0,0,'',0,0,''),
 (15,1,'',0,'a:1:{s:15:\"max_text_length\";i:255;}',0,100,NULL,0,0,'',0,0,''),
 (16,0,'',0,NULL,1,0,NULL,0,0,'',0,0,''),
 (17,0,'',0,NULL,0,0,NULL,0,0,'',0,0,''),
 (18,0,'',0,NULL,0,10,NULL,0,0,'',0,0,''),
 (19,1,'',0,'a:2:{s:15:\"max_text_length\";i:255;s:15:\"min_text_length\";i:1;}',1,20,NULL,0,0,'',0,0,''),
 (20,0,'',0,NULL,0,30,NULL,0,0,'',0,0,''),
 (21,1,'',0,'a:2:{s:15:\"max_text_length\";i:255;s:15:\"min_text_length\";i:1;}',1,40,NULL,0,0,'',0,0,''),
 (22,0,'',0,NULL,0,50,NULL,0,0,'',0,0,''),
 (23,1,'',0,'a:2:{s:15:\"max_text_length\";i:255;s:15:\"min_text_length\";i:1;}',1,60,NULL,0,0,'',0,0,''),
 (24,1,'',4,'a:2:{s:15:\"max_text_length\";i:255;s:15:\"min_text_length\";i:1;}',1,70,NULL,0,0,'',0,0,''),
 (25,1,'',0,'a:2:{s:15:\"max_text_length\";i:255;s:15:\"min_text_length\";i:1;}',1,80,NULL,0,0,'',0,0,''),
 (26,1,'',0,NULL,1,90,NULL,0,0,'',0,0,''),
 (27,1,'',0,NULL,1,100,NULL,0,0,'',0,0,''),
 (28,1,'',0,NULL,1,100,NULL,0,0,'',0,0,''),
 (29,1,'',0,'a:0:{}',1,110,'customer/attribute_data_postcode',0,0,'',0,0,''),
 (30,1,'',0,'a:2:{s:15:\"max_text_length\";i:255;s:15:\"min_text_length\";i:1;}',1,120,NULL,0,0,'',0,0,''),
 (31,1,'',0,'a:2:{s:15:\"max_text_length\";i:255;s:15:\"min_text_length\";i:1;}',1,130,NULL,0,0,'',0,0,''),
 (32,1,'',0,'a:0:{}',0,110,NULL,0,0,'',0,0,''),
 (125,1,NULL,1,NULL,0,0,NULL,1,0,'1',0,1,'selectimg'),
 (126,1,NULL,1,NULL,0,0,NULL,1,1,'1',3,1,''),
 (127,1,NULL,1,NULL,0,0,NULL,0,1,'1',5,1,''),
 (128,1,NULL,1,NULL,0,0,NULL,0,1,'1',7,1,''),
 (129,1,NULL,1,NULL,0,0,NULL,0,1,'1',11,1,'');
UNLOCK TABLES;
/*!40000 ALTER TABLE `customer_eav_attribute` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`customer_eav_attribute_website`
--

DROP TABLE IF EXISTS `supernarede`.`customer_eav_attribute_website`;
CREATE TABLE  `supernarede`.`customer_eav_attribute_website` (
  `attribute_id` smallint(5) unsigned NOT NULL,
  `website_id` smallint(5) unsigned NOT NULL,
  `is_visible` tinyint(1) unsigned DEFAULT NULL,
  `is_required` tinyint(1) unsigned DEFAULT NULL,
  `default_value` text,
  `multiline_count` tinyint(3) unsigned DEFAULT NULL,
  PRIMARY KEY (`attribute_id`,`website_id`),
  KEY `IDX_WEBSITE` (`website_id`),
  CONSTRAINT `FK_CUST_EAV_ATTR_WEBST_ATTR_EAV_ATTR` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CUST_EAV_ATTR_WEBST_WEBST_CORE_WEBST` FOREIGN KEY (`website_id`) REFERENCES `core_website` (`website_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`customer_eav_attribute_website`
--

/*!40000 ALTER TABLE `customer_eav_attribute_website` DISABLE KEYS */;
LOCK TABLES `customer_eav_attribute_website` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `customer_eav_attribute_website` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`customer_entity`
--

DROP TABLE IF EXISTS `supernarede`.`customer_entity`;
CREATE TABLE  `supernarede`.`customer_entity` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entity_type_id` smallint(8) unsigned NOT NULL DEFAULT '0',
  `attribute_set_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `website_id` smallint(5) unsigned DEFAULT NULL,
  `email` varchar(255) NOT NULL DEFAULT '',
  `group_id` smallint(3) unsigned NOT NULL DEFAULT '0',
  `increment_id` varchar(50) NOT NULL DEFAULT '',
  `store_id` smallint(5) unsigned DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`entity_id`),
  KEY `FK_CUSTOMER_ENTITY_STORE` (`store_id`),
  KEY `IDX_ENTITY_TYPE` (`entity_type_id`),
  KEY `IDX_AUTH` (`email`,`website_id`),
  KEY `FK_CUSTOMER_WEBSITE` (`website_id`),
  CONSTRAINT `FK_CUSTOMER_ENTITY_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_CUSTOMER_WEBSITE` FOREIGN KEY (`website_id`) REFERENCES `core_website` (`website_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='Customer Entityies';

--
-- Dumping data for table `supernarede`.`customer_entity`
--

/*!40000 ALTER TABLE `customer_entity` DISABLE KEYS */;
LOCK TABLES `customer_entity` WRITE;
INSERT INTO `supernarede`.`customer_entity` VALUES  (1,1,0,1,'leandro@bhdesign.com.br',1,'',1,'2011-09-22 14:01:57','2011-09-22 15:39:12',1);
UNLOCK TABLES;
/*!40000 ALTER TABLE `customer_entity` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`customer_entity_datetime`
--

DROP TABLE IF EXISTS `supernarede`.`customer_entity_datetime`;
CREATE TABLE  `supernarede`.`customer_entity_datetime` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT,
  `entity_type_id` smallint(8) unsigned NOT NULL DEFAULT '0',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0',
  `value` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `IDX_ATTRIBUTE_VALUE` (`entity_id`,`attribute_id`),
  KEY `FK_CUSTOMER_DATETIME_ENTITY_TYPE` (`entity_type_id`),
  KEY `FK_CUSTOMER_DATETIME_ATTRIBUTE` (`attribute_id`),
  KEY `FK_CUSTOMER_DATETIME_ENTITY` (`entity_id`),
  KEY `IDX_VALUE` (`entity_id`,`attribute_id`,`value`),
  CONSTRAINT `FK_CUSTOMER_DATETIME_ATTRIBUTE` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CUSTOMER_DATETIME_ENTITY` FOREIGN KEY (`entity_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CUSTOMER_DATETIME_ENTITY_TYPE` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`customer_entity_datetime`
--

/*!40000 ALTER TABLE `customer_entity_datetime` DISABLE KEYS */;
LOCK TABLES `customer_entity_datetime` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `customer_entity_datetime` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`customer_entity_decimal`
--

DROP TABLE IF EXISTS `supernarede`.`customer_entity_decimal`;
CREATE TABLE  `supernarede`.`customer_entity_decimal` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT,
  `entity_type_id` smallint(8) unsigned NOT NULL DEFAULT '0',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0',
  `value` decimal(12,4) NOT NULL DEFAULT '0.0000',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `IDX_ATTRIBUTE_VALUE` (`entity_id`,`attribute_id`),
  KEY `FK_CUSTOMER_DECIMAL_ENTITY_TYPE` (`entity_type_id`),
  KEY `FK_CUSTOMER_DECIMAL_ATTRIBUTE` (`attribute_id`),
  KEY `FK_CUSTOMER_DECIMAL_ENTITY` (`entity_id`),
  KEY `IDX_VALUE` (`entity_id`,`attribute_id`,`value`),
  CONSTRAINT `FK_CUSTOMER_DECIMAL_ATTRIBUTE` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CUSTOMER_DECIMAL_ENTITY` FOREIGN KEY (`entity_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CUSTOMER_DECIMAL_ENTITY_TYPE` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`customer_entity_decimal`
--

/*!40000 ALTER TABLE `customer_entity_decimal` DISABLE KEYS */;
LOCK TABLES `customer_entity_decimal` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `customer_entity_decimal` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`customer_entity_int`
--

DROP TABLE IF EXISTS `supernarede`.`customer_entity_int`;
CREATE TABLE  `supernarede`.`customer_entity_int` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT,
  `entity_type_id` smallint(8) unsigned NOT NULL DEFAULT '0',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0',
  `value` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `IDX_ATTRIBUTE_VALUE` (`entity_id`,`attribute_id`),
  KEY `FK_CUSTOMER_INT_ENTITY_TYPE` (`entity_type_id`),
  KEY `FK_CUSTOMER_INT_ATTRIBUTE` (`attribute_id`),
  KEY `FK_CUSTOMER_INT_ENTITY` (`entity_id`),
  KEY `IDX_VALUE` (`entity_id`,`attribute_id`,`value`),
  CONSTRAINT `FK_CUSTOMER_INT_ATTRIBUTE` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CUSTOMER_INT_ENTITY` FOREIGN KEY (`entity_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CUSTOMER_INT_ENTITY_TYPE` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`customer_entity_int`
--

/*!40000 ALTER TABLE `customer_entity_int` DISABLE KEYS */;
LOCK TABLES `customer_entity_int` WRITE;
INSERT INTO `supernarede`.`customer_entity_int` VALUES  (1,1,125,1,14),
 (2,1,13,1,1),
 (3,1,14,1,1);
UNLOCK TABLES;
/*!40000 ALTER TABLE `customer_entity_int` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`customer_entity_text`
--

DROP TABLE IF EXISTS `supernarede`.`customer_entity_text`;
CREATE TABLE  `supernarede`.`customer_entity_text` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT,
  `entity_type_id` smallint(8) unsigned NOT NULL DEFAULT '0',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0',
  `value` text NOT NULL,
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `IDX_ATTRIBUTE_VALUE` (`entity_id`,`attribute_id`),
  KEY `FK_CUSTOMER_TEXT_ENTITY_TYPE` (`entity_type_id`),
  KEY `FK_CUSTOMER_TEXT_ATTRIBUTE` (`attribute_id`),
  KEY `FK_CUSTOMER_TEXT_ENTITY` (`entity_id`),
  CONSTRAINT `FK_CUSTOMER_TEXT_ATTRIBUTE` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CUSTOMER_TEXT_ENTITY` FOREIGN KEY (`entity_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CUSTOMER_TEXT_ENTITY_TYPE` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`customer_entity_text`
--

/*!40000 ALTER TABLE `customer_entity_text` DISABLE KEYS */;
LOCK TABLES `customer_entity_text` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `customer_entity_text` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`customer_entity_varchar`
--

DROP TABLE IF EXISTS `supernarede`.`customer_entity_varchar`;
CREATE TABLE  `supernarede`.`customer_entity_varchar` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT,
  `entity_type_id` smallint(8) unsigned NOT NULL DEFAULT '0',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0',
  `value` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `IDX_ATTRIBUTE_VALUE` (`entity_id`,`attribute_id`),
  KEY `FK_CUSTOMER_VARCHAR_ENTITY_TYPE` (`entity_type_id`),
  KEY `FK_CUSTOMER_VARCHAR_ATTRIBUTE` (`attribute_id`),
  KEY `FK_CUSTOMER_VARCHAR_ENTITY` (`entity_id`),
  KEY `IDX_VALUE` (`entity_id`,`attribute_id`,`value`),
  CONSTRAINT `FK_CUSTOMER_VARCHAR_ATTRIBUTE` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CUSTOMER_VARCHAR_ENTITY` FOREIGN KEY (`entity_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CUSTOMER_VARCHAR_ENTITY_TYPE` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`customer_entity_varchar`
--

/*!40000 ALTER TABLE `customer_entity_varchar` DISABLE KEYS */;
LOCK TABLES `customer_entity_varchar` WRITE;
INSERT INTO `supernarede`.`customer_entity_varchar` VALUES  (1,1,5,1,'Leandro'),
 (2,1,7,1,'JN2'),
 (3,1,12,1,'06a43248e35685ac31b28c38c244d34a:Hh'),
 (5,1,126,1,''),
 (6,1,127,1,''),
 (7,1,128,1,''),
 (8,1,3,1,'Português (Brasil)'),
 (10,1,129,1,'MG12346587');
UNLOCK TABLES;
/*!40000 ALTER TABLE `customer_entity_varchar` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`customer_form_attribute`
--

DROP TABLE IF EXISTS `supernarede`.`customer_form_attribute`;
CREATE TABLE  `supernarede`.`customer_form_attribute` (
  `form_code` char(32) NOT NULL,
  `attribute_id` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`form_code`,`attribute_id`),
  KEY `IDX_CUSTOMER_FORM_ATTRIBUTE_ATTRIBUTE` (`attribute_id`),
  CONSTRAINT `FK_CUSTOMER_FORM_ATTRIBUTE_ATTRIBUTE` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer attributes/forms relations';

--
-- Dumping data for table `supernarede`.`customer_form_attribute`
--

/*!40000 ALTER TABLE `customer_form_attribute` DISABLE KEYS */;
LOCK TABLES `customer_form_attribute` WRITE;
INSERT INTO `supernarede`.`customer_form_attribute` VALUES  ('adminhtml_customer',1),
 ('adminhtml_customer',3),
 ('adminhtml_customer',4),
 ('checkout_register',4),
 ('customer_account_create',4),
 ('customer_account_edit',4),
 ('adminhtml_customer',5),
 ('checkout_register',5),
 ('customer_account_create',5),
 ('customer_account_edit',5),
 ('adminhtml_customer',6),
 ('checkout_register',6),
 ('customer_account_create',6),
 ('customer_account_edit',6),
 ('adminhtml_customer',7),
 ('checkout_register',7),
 ('customer_account_create',7),
 ('customer_account_edit',7),
 ('adminhtml_customer',8),
 ('checkout_register',8),
 ('customer_account_create',8),
 ('customer_account_edit',8),
 ('adminhtml_checkout',9),
 ('adminhtml_customer',9),
 ('checkout_register',9),
 ('customer_account_create',9),
 ('customer_account_edit',9),
 ('adminhtml_checkout',10),
 ('adminhtml_customer',10),
 ('adminhtml_checkout',11),
 ('adminhtml_customer',11),
 ('checkout_register',11),
 ('customer_account_create',11),
 ('customer_account_edit',11),
 ('adminhtml_checkout',15),
 ('adminhtml_customer',15),
 ('checkout_register',15),
 ('customer_account_create',15),
 ('customer_account_edit',15),
 ('adminhtml_customer_address',18),
 ('customer_address_edit',18),
 ('customer_register_address',18),
 ('adminhtml_customer_address',19),
 ('customer_address_edit',19),
 ('customer_register_address',19),
 ('adminhtml_customer_address',20),
 ('customer_address_edit',20),
 ('customer_register_address',20),
 ('adminhtml_customer_address',21),
 ('customer_address_edit',21),
 ('customer_register_address',21),
 ('adminhtml_customer_address',22),
 ('customer_address_edit',22),
 ('customer_register_address',22),
 ('adminhtml_customer_address',23),
 ('customer_address_edit',23),
 ('customer_register_address',23),
 ('adminhtml_customer_address',24),
 ('customer_address_edit',24),
 ('customer_register_address',24),
 ('adminhtml_customer_address',25),
 ('customer_address_edit',25),
 ('customer_register_address',25),
 ('adminhtml_customer_address',26),
 ('customer_address_edit',26),
 ('customer_register_address',26),
 ('adminhtml_customer_address',27),
 ('customer_address_edit',27),
 ('customer_register_address',27),
 ('adminhtml_customer_address',28),
 ('customer_address_edit',28),
 ('customer_register_address',28),
 ('adminhtml_customer_address',29),
 ('customer_address_edit',29),
 ('customer_register_address',29),
 ('adminhtml_customer_address',30),
 ('customer_address_edit',30),
 ('customer_register_address',30),
 ('adminhtml_customer_address',31),
 ('customer_address_edit',31),
 ('customer_register_address',31),
 ('adminhtml_checkout',32),
 ('adminhtml_customer',32),
 ('checkout_register',32),
 ('customer_account_create',32),
 ('customer_account_edit',32);
UNLOCK TABLES;
/*!40000 ALTER TABLE `customer_form_attribute` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`customer_group`
--

DROP TABLE IF EXISTS `supernarede`.`customer_group`;
CREATE TABLE  `supernarede`.`customer_group` (
  `customer_group_id` smallint(3) unsigned NOT NULL AUTO_INCREMENT,
  `customer_group_code` varchar(32) NOT NULL DEFAULT '',
  `tax_class_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`customer_group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Customer groups';

--
-- Dumping data for table `supernarede`.`customer_group`
--

/*!40000 ALTER TABLE `customer_group` DISABLE KEYS */;
LOCK TABLES `customer_group` WRITE;
INSERT INTO `supernarede`.`customer_group` VALUES  (0,'Visitante',3),
 (1,'Comum',3),
 (2,'Atacado',3),
 (3,'Revenda',3);
UNLOCK TABLES;
/*!40000 ALTER TABLE `customer_group` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`dataflow_batch`
--

DROP TABLE IF EXISTS `supernarede`.`dataflow_batch`;
CREATE TABLE  `supernarede`.`dataflow_batch` (
  `batch_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `profile_id` int(10) unsigned NOT NULL DEFAULT '0',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `adapter` varchar(128) DEFAULT NULL,
  `params` text,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`batch_id`),
  KEY `FK_DATAFLOW_BATCH_PROFILE` (`profile_id`),
  KEY `FK_DATAFLOW_BATCH_STORE` (`store_id`),
  KEY `IDX_CREATED_AT` (`created_at`),
  CONSTRAINT `FK_DATAFLOW_BATCH_PROFILE` FOREIGN KEY (`profile_id`) REFERENCES `dataflow_profile` (`profile_id`) ON DELETE CASCADE,
  CONSTRAINT `FK_DATAFLOW_BATCH_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`dataflow_batch`
--

/*!40000 ALTER TABLE `dataflow_batch` DISABLE KEYS */;
LOCK TABLES `dataflow_batch` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `dataflow_batch` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`dataflow_batch_export`
--

DROP TABLE IF EXISTS `supernarede`.`dataflow_batch_export`;
CREATE TABLE  `supernarede`.`dataflow_batch_export` (
  `batch_export_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `batch_id` int(10) unsigned NOT NULL DEFAULT '0',
  `batch_data` longtext,
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`batch_export_id`),
  KEY `FK_DATAFLOW_BATCH_EXPORT_BATCH` (`batch_id`),
  CONSTRAINT `FK_DATAFLOW_BATCH_EXPORT_BATCH` FOREIGN KEY (`batch_id`) REFERENCES `dataflow_batch` (`batch_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`dataflow_batch_export`
--

/*!40000 ALTER TABLE `dataflow_batch_export` DISABLE KEYS */;
LOCK TABLES `dataflow_batch_export` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `dataflow_batch_export` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`dataflow_batch_import`
--

DROP TABLE IF EXISTS `supernarede`.`dataflow_batch_import`;
CREATE TABLE  `supernarede`.`dataflow_batch_import` (
  `batch_import_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `batch_id` int(10) unsigned NOT NULL DEFAULT '0',
  `batch_data` longtext,
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`batch_import_id`),
  KEY `FK_DATAFLOW_BATCH_IMPORT_BATCH` (`batch_id`),
  CONSTRAINT `FK_DATAFLOW_BATCH_IMPORT_BATCH` FOREIGN KEY (`batch_id`) REFERENCES `dataflow_batch` (`batch_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`dataflow_batch_import`
--

/*!40000 ALTER TABLE `dataflow_batch_import` DISABLE KEYS */;
LOCK TABLES `dataflow_batch_import` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `dataflow_batch_import` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`dataflow_import_data`
--

DROP TABLE IF EXISTS `supernarede`.`dataflow_import_data`;
CREATE TABLE  `supernarede`.`dataflow_import_data` (
  `import_id` int(11) NOT NULL AUTO_INCREMENT,
  `session_id` int(11) DEFAULT NULL,
  `serial_number` int(11) NOT NULL DEFAULT '0',
  `value` text,
  `status` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`import_id`),
  KEY `FK_dataflow_import_data` (`session_id`),
  CONSTRAINT `FK_dataflow_import_data` FOREIGN KEY (`session_id`) REFERENCES `dataflow_session` (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`dataflow_import_data`
--

/*!40000 ALTER TABLE `dataflow_import_data` DISABLE KEYS */;
LOCK TABLES `dataflow_import_data` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `dataflow_import_data` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`dataflow_profile`
--

DROP TABLE IF EXISTS `supernarede`.`dataflow_profile`;
CREATE TABLE  `supernarede`.`dataflow_profile` (
  `profile_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `actions_xml` text,
  `gui_data` text,
  `direction` enum('import','export') DEFAULT NULL,
  `entity_type` varchar(64) NOT NULL DEFAULT '',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `data_transfer` enum('file','interactive') DEFAULT NULL,
  PRIMARY KEY (`profile_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`dataflow_profile`
--

/*!40000 ALTER TABLE `dataflow_profile` DISABLE KEYS */;
LOCK TABLES `dataflow_profile` WRITE;
INSERT INTO `supernarede`.`dataflow_profile` VALUES  (1,'Exportar Todos Produtos','2011-08-08 09:54:59','2011-08-22 20:55:23','<action type=\"catalog/convert_adapter_product\" method=\"load\">\r\n    <var name=\"store\"><![CDATA[0]]></var>\r\n</action>\r\n\r\n<action type=\"catalog/convert_parser_product\" method=\"unparse\">\r\n    <var name=\"store\"><![CDATA[0]]></var>\r\n    <var name=\"url_field\"><![CDATA[0]]></var>\r\n</action>\r\n\r\n<action type=\"dataflow/convert_mapper_column\" method=\"map\">\r\n</action>\r\n\r\n<action type=\"dataflow/convert_parser_xml_excel\" method=\"unparse\">\r\n    <var name=\"single_sheet\"><![CDATA[importacao_geral]]></var>\r\n    <var name=\"fieldnames\">true</var>\r\n</action>\r\n\r\n<action type=\"dataflow/convert_adapter_io\" method=\"save\">\r\n    <var name=\"type\">file</var>\r\n    <var name=\"path\">var/export</var>\r\n    <var name=\"filename\"><![CDATA[export_all_products.csv]]></var>\r\n</action>\r\n\r\n','a:7:{s:6:\"export\";a:1:{s:13:\"add_url_field\";s:1:\"0\";}s:6:\"import\";a:2:{s:17:\"number_of_records\";s:1:\"1\";s:17:\"decimal_separator\";s:1:\".\";}s:4:\"file\";a:8:{s:4:\"type\";s:4:\"file\";s:8:\"filename\";s:23:\"export_all_products.csv\";s:4:\"path\";s:10:\"var/export\";s:4:\"host\";s:0:\"\";s:4:\"user\";s:0:\"\";s:8:\"password\";s:0:\"\";s:9:\"file_mode\";s:1:\"2\";s:7:\"passive\";s:0:\"\";}s:5:\"parse\";a:5:{s:4:\"type\";s:9:\"excel_xml\";s:12:\"single_sheet\";s:16:\"importacao_geral\";s:9:\"delimiter\";s:1:\",\";s:7:\"enclose\";s:1:\"\"\";s:10:\"fieldnames\";s:4:\"true\";}s:3:\"map\";a:3:{s:14:\"only_specified\";s:0:\"\";s:7:\"product\";a:2:{s:2:\"db\";a:0:{}s:4:\"file\";a:0:{}}s:8:\"customer\";a:2:{s:2:\"db\";a:0:{}s:4:\"file\";a:0:{}}}s:7:\"product\";a:1:{s:6:\"filter\";a:8:{s:4:\"name\";s:0:\"\";s:3:\"sku\";s:0:\"\";s:4:\"type\";s:1:\"0\";s:13:\"attribute_set\";s:0:\"\";s:5:\"price\";a:2:{s:4:\"from\";s:0:\"\";s:2:\"to\";s:0:\"\";}s:3:\"qty\";a:2:{s:4:\"from\";s:0:\"\";s:2:\"to\";s:0:\"\";}s:10:\"visibility\";s:1:\"0\";s:6:\"status\";s:1:\"0\";}}s:8:\"customer\";a:1:{s:6:\"filter\";a:10:{s:9:\"firstname\";s:0:\"\";s:8:\"lastname\";s:0:\"\";s:5:\"email\";s:0:\"\";s:5:\"group\";s:1:\"0\";s:10:\"adressType\";s:15:\"default_billing\";s:9:\"telephone\";s:0:\"\";s:8:\"postcode\";s:0:\"\";s:7:\"country\";s:0:\"\";s:6:\"region\";s:0:\"\";s:10:\"created_at\";a:2:{s:4:\"from\";s:0:\"\";s:2:\"to\";s:0:\"\";}}}}','export','product',0,'file'),
 (2,'Exportar Estoque Produtos','2011-08-08 09:54:59','2011-08-08 09:54:59','<action type=\"catalog/convert_adapter_product\" method=\"load\">\r\n    <var name=\"store\"><![CDATA[0]]></var>\r\n</action>\r\n\r\n<action type=\"catalog/convert_parser_product\" method=\"unparse\">\r\n    <var name=\"store\"><![CDATA[0]]></var>\r\n</action>\r\n\r\n<action type=\"dataflow/convert_mapper_column\" method=\"map\">\r\n    <var name=\"map\">\r\n        <map name=\"store\"><![CDATA[store]]></map>\r\n        <map name=\"sku\"><![CDATA[sku]]></map>\r\n        <map name=\"qty\"><![CDATA[qty]]></map>\r\n        <map name=\"is_in_stock\"><![CDATA[is_in_stock]]></map>\r\n    </var>\r\n    <var name=\"_only_specified\">true</var>\r\n</action>\r\n\r\n<action type=\"dataflow/convert_parser_csv\" method=\"unparse\">\r\n    <var name=\"delimiter\"><![CDATA[,]]></var>\r\n    <var name=\"enclose\"><![CDATA[\"]]></var>\r\n    <var name=\"fieldnames\">true</var>\r\n</action>\r\n\r\n<action type=\"dataflow/convert_adapter_io\" method=\"save\">\r\n    <var name=\"type\">file</var>\r\n    <var name=\"path\">var/export</var>\r\n    <var name=\"filename\"><![CDATA[export_product_stocks.csv]]></var>\r\n</action>\r\n\r\n','a:5:{s:4:\"file\";a:7:{s:4:\"type\";s:4:\"file\";s:8:\"filename\";s:25:\"export_product_stocks.csv\";s:4:\"path\";s:10:\"var/export\";s:4:\"host\";s:0:\"\";s:4:\"user\";s:0:\"\";s:8:\"password\";s:0:\"\";s:7:\"passive\";s:0:\"\";}s:5:\"parse\";a:5:{s:4:\"type\";s:3:\"csv\";s:12:\"single_sheet\";s:0:\"\";s:9:\"delimiter\";s:1:\",\";s:7:\"enclose\";s:1:\"\"\";s:10:\"fieldnames\";s:4:\"true\";}s:3:\"map\";a:3:{s:14:\"only_specified\";s:4:\"true\";s:7:\"product\";a:2:{s:2:\"db\";a:4:{i:1;s:5:\"store\";i:2;s:3:\"sku\";i:3;s:3:\"qty\";i:4;s:11:\"is_in_stock\";}s:4:\"file\";a:4:{i:1;s:5:\"store\";i:2;s:3:\"sku\";i:3;s:3:\"qty\";i:4;s:11:\"is_in_stock\";}}s:8:\"customer\";a:2:{s:2:\"db\";a:0:{}s:4:\"file\";a:0:{}}}s:7:\"product\";a:1:{s:6:\"filter\";a:8:{s:4:\"name\";s:0:\"\";s:3:\"sku\";s:0:\"\";s:4:\"type\";s:1:\"0\";s:13:\"attribute_set\";s:0:\"\";s:5:\"price\";a:2:{s:4:\"from\";s:0:\"\";s:2:\"to\";s:0:\"\";}s:3:\"qty\";a:2:{s:4:\"from\";s:0:\"\";s:2:\"to\";s:0:\"\";}s:10:\"visibility\";s:1:\"0\";s:6:\"status\";s:1:\"0\";}}s:8:\"customer\";a:1:{s:6:\"filter\";a:10:{s:9:\"firstname\";s:0:\"\";s:8:\"lastname\";s:0:\"\";s:5:\"email\";s:0:\"\";s:5:\"group\";s:1:\"0\";s:10:\"adressType\";s:15:\"default_billing\";s:9:\"telephone\";s:0:\"\";s:8:\"postcode\";s:0:\"\";s:7:\"country\";s:0:\"\";s:6:\"region\";s:0:\"\";s:10:\"created_at\";a:2:{s:4:\"from\";s:0:\"\";s:2:\"to\";s:0:\"\";}}}}','export','product',0,'file'),
 (3,'Importar Todos Produtos','2011-08-08 09:54:59','2011-08-08 09:54:59','<action type=\"dataflow/convert_parser_csv\" method=\"parse\">\r\n    <var name=\"delimiter\"><![CDATA[,]]></var>\r\n    <var name=\"enclose\"><![CDATA[\"]]></var>\r\n    <var name=\"fieldnames\">true</var>\r\n    <var name=\"store\"><![CDATA[0]]></var>\r\n    <var name=\"adapter\">catalog/convert_adapter_product</var>\r\n    <var name=\"method\">parse</var>\r\n</action>','a:5:{s:4:\"file\";a:7:{s:4:\"type\";s:4:\"file\";s:8:\"filename\";s:23:\"export_all_products.csv\";s:4:\"path\";s:10:\"var/export\";s:4:\"host\";s:0:\"\";s:4:\"user\";s:0:\"\";s:8:\"password\";s:0:\"\";s:7:\"passive\";s:0:\"\";}s:5:\"parse\";a:5:{s:4:\"type\";s:3:\"csv\";s:12:\"single_sheet\";s:0:\"\";s:9:\"delimiter\";s:1:\",\";s:7:\"enclose\";s:1:\"\"\";s:10:\"fieldnames\";s:4:\"true\";}s:3:\"map\";a:3:{s:14:\"only_specified\";s:0:\"\";s:7:\"product\";a:2:{s:2:\"db\";a:0:{}s:4:\"file\";a:0:{}}s:8:\"customer\";a:2:{s:2:\"db\";a:0:{}s:4:\"file\";a:0:{}}}s:7:\"product\";a:1:{s:6:\"filter\";a:8:{s:4:\"name\";s:0:\"\";s:3:\"sku\";s:0:\"\";s:4:\"type\";s:1:\"0\";s:13:\"attribute_set\";s:0:\"\";s:5:\"price\";a:2:{s:4:\"from\";s:0:\"\";s:2:\"to\";s:0:\"\";}s:3:\"qty\";a:2:{s:4:\"from\";s:0:\"\";s:2:\"to\";s:0:\"\";}s:10:\"visibility\";s:1:\"0\";s:6:\"status\";s:1:\"0\";}}s:8:\"customer\";a:1:{s:6:\"filter\";a:10:{s:9:\"firstname\";s:0:\"\";s:8:\"lastname\";s:0:\"\";s:5:\"email\";s:0:\"\";s:5:\"group\";s:1:\"0\";s:10:\"adressType\";s:15:\"default_billing\";s:9:\"telephone\";s:0:\"\";s:8:\"postcode\";s:0:\"\";s:7:\"country\";s:0:\"\";s:6:\"region\";s:0:\"\";s:10:\"created_at\";a:2:{s:4:\"from\";s:0:\"\";s:2:\"to\";s:0:\"\";}}}}','import','product',0,'interactive'),
 (4,'Importar Estoque Produtos','2011-08-08 09:54:59','2011-08-08 09:54:59','<action type=\"dataflow/convert_parser_csv\" method=\"parse\">\r\n    <var name=\"delimiter\"><![CDATA[,]]></var>\r\n    <var name=\"enclose\"><![CDATA[\"]]></var>\r\n    <var name=\"fieldnames\">true</var>\r\n    <var name=\"store\"><![CDATA[0]]></var>\r\n    <var name=\"adapter\">catalog/convert_adapter_product</var>\r\n    <var name=\"method\">parse</var>\r\n</action>','a:5:{s:4:\"file\";a:7:{s:4:\"type\";s:4:\"file\";s:8:\"filename\";s:18:\"export_product.csv\";s:4:\"path\";s:10:\"var/export\";s:4:\"host\";s:0:\"\";s:4:\"user\";s:0:\"\";s:8:\"password\";s:0:\"\";s:7:\"passive\";s:0:\"\";}s:5:\"parse\";a:5:{s:4:\"type\";s:3:\"csv\";s:12:\"single_sheet\";s:0:\"\";s:9:\"delimiter\";s:1:\",\";s:7:\"enclose\";s:1:\"\"\";s:10:\"fieldnames\";s:4:\"true\";}s:3:\"map\";a:3:{s:14:\"only_specified\";s:0:\"\";s:7:\"product\";a:2:{s:2:\"db\";a:0:{}s:4:\"file\";a:0:{}}s:8:\"customer\";a:2:{s:2:\"db\";a:0:{}s:4:\"file\";a:0:{}}}s:7:\"product\";a:1:{s:6:\"filter\";a:8:{s:4:\"name\";s:0:\"\";s:3:\"sku\";s:0:\"\";s:4:\"type\";s:1:\"0\";s:13:\"attribute_set\";s:0:\"\";s:5:\"price\";a:2:{s:4:\"from\";s:0:\"\";s:2:\"to\";s:0:\"\";}s:3:\"qty\";a:2:{s:4:\"from\";s:0:\"\";s:2:\"to\";s:0:\"\";}s:10:\"visibility\";s:1:\"0\";s:6:\"status\";s:1:\"0\";}}s:8:\"customer\";a:1:{s:6:\"filter\";a:10:{s:9:\"firstname\";s:0:\"\";s:8:\"lastname\";s:0:\"\";s:5:\"email\";s:0:\"\";s:5:\"group\";s:1:\"0\";s:10:\"adressType\";s:15:\"default_billing\";s:9:\"telephone\";s:0:\"\";s:8:\"postcode\";s:0:\"\";s:7:\"country\";s:0:\"\";s:6:\"region\";s:0:\"\";s:10:\"created_at\";a:2:{s:4:\"from\";s:0:\"\";s:2:\"to\";s:0:\"\";}}}}','import','product',0,'interactive'),
 (5,'Exportar Clientes','2011-08-08 09:54:59','2011-08-08 09:54:59','<action type=\"customer/convert_adapter_customer\" method=\"load\">\r\n    <var name=\"store\"><![CDATA[0]]></var>\r\n    <var name=\"filter/adressType\"><![CDATA[default_billing]]></var>\r\n</action>\r\n\r\n<action type=\"customer/convert_parser_customer\" method=\"unparse\">\r\n    <var name=\"store\"><![CDATA[0]]></var>\r\n</action>\r\n\r\n<action type=\"dataflow/convert_mapper_column\" method=\"map\">\r\n</action>\r\n\r\n<action type=\"dataflow/convert_parser_csv\" method=\"unparse\">\r\n    <var name=\"delimiter\"><![CDATA[,]]></var>\r\n    <var name=\"enclose\"><![CDATA[\"]]></var>\r\n    <var name=\"fieldnames\">true</var>\r\n</action>\r\n\r\n<action type=\"dataflow/convert_adapter_io\" method=\"save\">\r\n    <var name=\"type\">file</var>\r\n    <var name=\"path\">var/export</var>\r\n    <var name=\"filename\"><![CDATA[export_customers.csv]]></var>\r\n</action>\r\n\r\n','a:5:{s:4:\"file\";a:7:{s:4:\"type\";s:4:\"file\";s:8:\"filename\";s:20:\"export_customers.csv\";s:4:\"path\";s:10:\"var/export\";s:4:\"host\";s:0:\"\";s:4:\"user\";s:0:\"\";s:8:\"password\";s:0:\"\";s:7:\"passive\";s:0:\"\";}s:5:\"parse\";a:5:{s:4:\"type\";s:3:\"csv\";s:12:\"single_sheet\";s:0:\"\";s:9:\"delimiter\";s:1:\",\";s:7:\"enclose\";s:1:\"\"\";s:10:\"fieldnames\";s:4:\"true\";}s:3:\"map\";a:3:{s:14:\"only_specified\";s:0:\"\";s:7:\"product\";a:2:{s:2:\"db\";a:0:{}s:4:\"file\";a:0:{}}s:8:\"customer\";a:2:{s:2:\"db\";a:0:{}s:4:\"file\";a:0:{}}}s:7:\"product\";a:1:{s:6:\"filter\";a:8:{s:4:\"name\";s:0:\"\";s:3:\"sku\";s:0:\"\";s:4:\"type\";s:1:\"0\";s:13:\"attribute_set\";s:0:\"\";s:5:\"price\";a:2:{s:4:\"from\";s:0:\"\";s:2:\"to\";s:0:\"\";}s:3:\"qty\";a:2:{s:4:\"from\";s:0:\"\";s:2:\"to\";s:0:\"\";}s:10:\"visibility\";s:1:\"0\";s:6:\"status\";s:1:\"0\";}}s:8:\"customer\";a:1:{s:6:\"filter\";a:10:{s:9:\"firstname\";s:0:\"\";s:8:\"lastname\";s:0:\"\";s:5:\"email\";s:0:\"\";s:5:\"group\";s:1:\"0\";s:10:\"adressType\";s:15:\"default_billing\";s:9:\"telephone\";s:0:\"\";s:8:\"postcode\";s:0:\"\";s:7:\"country\";s:0:\"\";s:6:\"region\";s:0:\"\";s:10:\"created_at\";a:2:{s:4:\"from\";s:0:\"\";s:2:\"to\";s:0:\"\";}}}}','export','customer',0,'file'),
 (6,'Importar Clientes','2011-08-08 09:54:59','2011-08-08 09:54:59','<action type=\"dataflow/convert_parser_csv\" method=\"parse\">\r\n    <var name=\"delimiter\"><![CDATA[,]]></var>\r\n    <var name=\"enclose\"><![CDATA[\"]]></var>\r\n    <var name=\"fieldnames\">true</var>\r\n    <var name=\"store\"><![CDATA[0]]></var>\r\n    <var name=\"adapter\">customer/convert_adapter_customer</var>\r\n    <var name=\"method\">parse</var>\r\n</action>','a:5:{s:4:\"file\";a:7:{s:4:\"type\";s:4:\"file\";s:8:\"filename\";s:19:\"export_customer.csv\";s:4:\"path\";s:10:\"var/export\";s:4:\"host\";s:0:\"\";s:4:\"user\";s:0:\"\";s:8:\"password\";s:0:\"\";s:7:\"passive\";s:0:\"\";}s:5:\"parse\";a:5:{s:4:\"type\";s:3:\"csv\";s:12:\"single_sheet\";s:0:\"\";s:9:\"delimiter\";s:1:\",\";s:7:\"enclose\";s:1:\"\"\";s:10:\"fieldnames\";s:4:\"true\";}s:3:\"map\";a:3:{s:14:\"only_specified\";s:0:\"\";s:7:\"product\";a:2:{s:2:\"db\";a:0:{}s:4:\"file\";a:0:{}}s:8:\"customer\";a:2:{s:2:\"db\";a:0:{}s:4:\"file\";a:0:{}}}s:7:\"product\";a:1:{s:6:\"filter\";a:8:{s:4:\"name\";s:0:\"\";s:3:\"sku\";s:0:\"\";s:4:\"type\";s:1:\"0\";s:13:\"attribute_set\";s:0:\"\";s:5:\"price\";a:2:{s:4:\"from\";s:0:\"\";s:2:\"to\";s:0:\"\";}s:3:\"qty\";a:2:{s:4:\"from\";s:0:\"\";s:2:\"to\";s:0:\"\";}s:10:\"visibility\";s:1:\"0\";s:6:\"status\";s:1:\"0\";}}s:8:\"customer\";a:1:{s:6:\"filter\";a:10:{s:9:\"firstname\";s:0:\"\";s:8:\"lastname\";s:0:\"\";s:5:\"email\";s:0:\"\";s:5:\"group\";s:1:\"0\";s:10:\"adressType\";s:15:\"default_billing\";s:9:\"telephone\";s:0:\"\";s:8:\"postcode\";s:0:\"\";s:7:\"country\";s:0:\"\";s:6:\"region\";s:0:\"\";s:10:\"created_at\";a:2:{s:4:\"from\";s:0:\"\";s:2:\"to\";s:0:\"\";}}}}','import','customer',0,'interactive');
UNLOCK TABLES;
/*!40000 ALTER TABLE `dataflow_profile` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`dataflow_profile_history`
--

DROP TABLE IF EXISTS `supernarede`.`dataflow_profile_history`;
CREATE TABLE  `supernarede`.`dataflow_profile_history` (
  `history_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `profile_id` int(10) unsigned NOT NULL DEFAULT '0',
  `action_code` varchar(64) DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `performed_at` datetime DEFAULT NULL,
  PRIMARY KEY (`history_id`),
  KEY `FK_dataflow_profile_history` (`profile_id`),
  CONSTRAINT `FK_dataflow_profile_history` FOREIGN KEY (`profile_id`) REFERENCES `dataflow_profile` (`profile_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`dataflow_profile_history`
--

/*!40000 ALTER TABLE `dataflow_profile_history` DISABLE KEYS */;
LOCK TABLES `dataflow_profile_history` WRITE;
INSERT INTO `supernarede`.`dataflow_profile_history` VALUES  (1,1,'update',1,'2011-08-22 20:55:23'),
 (2,1,'run',1,'2011-08-22 20:55:42');
UNLOCK TABLES;
/*!40000 ALTER TABLE `dataflow_profile_history` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`dataflow_session`
--

DROP TABLE IF EXISTS `supernarede`.`dataflow_session`;
CREATE TABLE  `supernarede`.`dataflow_session` (
  `session_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `created_date` datetime DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `type` varchar(32) DEFAULT NULL,
  `direction` varchar(32) DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`dataflow_session`
--

/*!40000 ALTER TABLE `dataflow_session` DISABLE KEYS */;
LOCK TABLES `dataflow_session` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `dataflow_session` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`design_change`
--

DROP TABLE IF EXISTS `supernarede`.`design_change`;
CREATE TABLE  `supernarede`.`design_change` (
  `design_change_id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `design` varchar(255) NOT NULL DEFAULT '',
  `date_from` date DEFAULT NULL,
  `date_to` date DEFAULT NULL,
  PRIMARY KEY (`design_change_id`),
  KEY `FK_DESIGN_CHANGE_STORE` (`store_id`),
  CONSTRAINT `FK_DESIGN_CHANGE_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`design_change`
--

/*!40000 ALTER TABLE `design_change` DISABLE KEYS */;
LOCK TABLES `design_change` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `design_change` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`directory_country`
--

DROP TABLE IF EXISTS `supernarede`.`directory_country`;
CREATE TABLE  `supernarede`.`directory_country` (
  `country_id` varchar(2) NOT NULL DEFAULT '',
  `iso2_code` varchar(2) NOT NULL DEFAULT '',
  `iso3_code` varchar(3) NOT NULL DEFAULT '',
  PRIMARY KEY (`country_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Countries';

--
-- Dumping data for table `supernarede`.`directory_country`
--

/*!40000 ALTER TABLE `directory_country` DISABLE KEYS */;
LOCK TABLES `directory_country` WRITE;
INSERT INTO `supernarede`.`directory_country` VALUES  ('AD','AD','AND'),
 ('AE','AE','ARE'),
 ('AF','AF','AFG'),
 ('AG','AG','ATG'),
 ('AI','AI','AIA'),
 ('AL','AL','ALB'),
 ('AM','AM','ARM'),
 ('AN','AN','ANT'),
 ('AO','AO','AGO'),
 ('AQ','AQ','ATA'),
 ('AR','AR','ARG'),
 ('AS','AS','ASM'),
 ('AT','AT','AUT'),
 ('AU','AU','AUS'),
 ('AW','AW','ABW'),
 ('AX','AX','ALA'),
 ('AZ','AZ','AZE'),
 ('BA','BA','BIH'),
 ('BB','BB','BRB'),
 ('BD','BD','BGD'),
 ('BE','BE','BEL'),
 ('BF','BF','BFA'),
 ('BG','BG','BGR'),
 ('BH','BH','BHR'),
 ('BI','BI','BDI'),
 ('BJ','BJ','BEN'),
 ('BL','BL','BLM'),
 ('BM','BM','BMU'),
 ('BN','BN','BRN'),
 ('BO','BO','BOL'),
 ('BR','BR','BRA'),
 ('BS','BS','BHS'),
 ('BT','BT','BTN'),
 ('BV','BV','BVT'),
 ('BW','BW','BWA'),
 ('BY','BY','BLR'),
 ('BZ','BZ','BLZ'),
 ('CA','CA','CAN'),
 ('CC','CC','CCK'),
 ('CD','CD','COD'),
 ('CF','CF','CAF'),
 ('CG','CG','COG'),
 ('CH','CH','CHE'),
 ('CI','CI','CIV'),
 ('CK','CK','COK'),
 ('CL','CL','CHL'),
 ('CM','CM','CMR'),
 ('CN','CN','CHN'),
 ('CO','CO','COL'),
 ('CR','CR','CRI'),
 ('CU','CU','CUB'),
 ('CV','CV','CPV'),
 ('CX','CX','CXR'),
 ('CY','CY','CYP'),
 ('CZ','CZ','CZE'),
 ('DE','DE','DEU'),
 ('DJ','DJ','DJI'),
 ('DK','DK','DNK'),
 ('DM','DM','DMA'),
 ('DO','DO','DOM'),
 ('DZ','DZ','DZA'),
 ('EC','EC','ECU'),
 ('EE','EE','EST'),
 ('EG','EG','EGY'),
 ('EH','EH','ESH'),
 ('ER','ER','ERI'),
 ('ES','ES','ESP'),
 ('ET','ET','ETH'),
 ('FI','FI','FIN'),
 ('FJ','FJ','FJI'),
 ('FK','FK','FLK'),
 ('FM','FM','FSM'),
 ('FO','FO','FRO'),
 ('FR','FR','FRA'),
 ('GA','GA','GAB'),
 ('GB','GB','GBR'),
 ('GD','GD','GRD'),
 ('GE','GE','GEO'),
 ('GF','GF','GUF'),
 ('GG','GG','GGY'),
 ('GH','GH','GHA'),
 ('GI','GI','GIB'),
 ('GL','GL','GRL'),
 ('GM','GM','GMB'),
 ('GN','GN','GIN'),
 ('GP','GP','GLP'),
 ('GQ','GQ','GNQ'),
 ('GR','GR','GRC'),
 ('GS','GS','SGS'),
 ('GT','GT','GTM'),
 ('GU','GU','GUM'),
 ('GW','GW','GNB'),
 ('GY','GY','GUY'),
 ('HK','HK','HKG'),
 ('HM','HM','HMD'),
 ('HN','HN','HND'),
 ('HR','HR','HRV'),
 ('HT','HT','HTI'),
 ('HU','HU','HUN'),
 ('ID','ID','IDN'),
 ('IE','IE','IRL'),
 ('IL','IL','ISR'),
 ('IM','IM','IMN'),
 ('IN','IN','IND'),
 ('IO','IO','IOT'),
 ('IQ','IQ','IRQ'),
 ('IR','IR','IRN'),
 ('IS','IS','ISL'),
 ('IT','IT','ITA'),
 ('JE','JE','JEY'),
 ('JM','JM','JAM'),
 ('JO','JO','JOR'),
 ('JP','JP','JPN'),
 ('KE','KE','KEN'),
 ('KG','KG','KGZ'),
 ('KH','KH','KHM'),
 ('KI','KI','KIR'),
 ('KM','KM','COM'),
 ('KN','KN','KNA'),
 ('KP','KP','PRK'),
 ('KR','KR','KOR'),
 ('KW','KW','KWT'),
 ('KY','KY','CYM'),
 ('KZ','KZ','KAZ'),
 ('LA','LA','LAO'),
 ('LB','LB','LBN'),
 ('LC','LC','LCA'),
 ('LI','LI','LIE'),
 ('LK','LK','LKA'),
 ('LR','LR','LBR'),
 ('LS','LS','LSO'),
 ('LT','LT','LTU'),
 ('LU','LU','LUX'),
 ('LV','LV','LVA'),
 ('LY','LY','LBY'),
 ('MA','MA','MAR'),
 ('MC','MC','MCO'),
 ('MD','MD','MDA'),
 ('ME','ME','MNE'),
 ('MF','MF','MAF'),
 ('MG','MG','MDG'),
 ('MH','MH','MHL'),
 ('MK','MK','MKD'),
 ('ML','ML','MLI'),
 ('MM','MM','MMR'),
 ('MN','MN','MNG'),
 ('MO','MO','MAC'),
 ('MP','MP','MNP'),
 ('MQ','MQ','MTQ'),
 ('MR','MR','MRT'),
 ('MS','MS','MSR'),
 ('MT','MT','MLT'),
 ('MU','MU','MUS'),
 ('MV','MV','MDV'),
 ('MW','MW','MWI'),
 ('MX','MX','MEX'),
 ('MY','MY','MYS'),
 ('MZ','MZ','MOZ'),
 ('NA','NA','NAM'),
 ('NC','NC','NCL'),
 ('NE','NE','NER'),
 ('NF','NF','NFK'),
 ('NG','NG','NGA'),
 ('NI','NI','NIC'),
 ('NL','NL','NLD'),
 ('NO','NO','NOR'),
 ('NP','NP','NPL'),
 ('NR','NR','NRU'),
 ('NU','NU','NIU'),
 ('NZ','NZ','NZL'),
 ('OM','OM','OMN'),
 ('PA','PA','PAN'),
 ('PE','PE','PER'),
 ('PF','PF','PYF'),
 ('PG','PG','PNG'),
 ('PH','PH','PHL'),
 ('PK','PK','PAK'),
 ('PL','PL','POL'),
 ('PM','PM','SPM'),
 ('PN','PN','PCN'),
 ('PR','PR','PRI'),
 ('PS','PS','PSE'),
 ('PT','PT','PRT'),
 ('PW','PW','PLW'),
 ('PY','PY','PRY'),
 ('QA','QA','QAT'),
 ('RE','RE','REU'),
 ('RO','RO','ROU'),
 ('RS','RS','SRB'),
 ('RU','RU','RUS'),
 ('RW','RW','RWA'),
 ('SA','SA','SAU'),
 ('SB','SB','SLB'),
 ('SC','SC','SYC'),
 ('SD','SD','SDN'),
 ('SE','SE','SWE'),
 ('SG','SG','SGP'),
 ('SH','SH','SHN'),
 ('SI','SI','SVN'),
 ('SJ','SJ','SJM'),
 ('SK','SK','SVK'),
 ('SL','SL','SLE'),
 ('SM','SM','SMR'),
 ('SN','SN','SEN'),
 ('SO','SO','SOM'),
 ('SR','SR','SUR'),
 ('ST','ST','STP'),
 ('SV','SV','SLV'),
 ('SY','SY','SYR'),
 ('SZ','SZ','SWZ'),
 ('TC','TC','TCA'),
 ('TD','TD','TCD'),
 ('TF','TF','ATF'),
 ('TG','TG','TGO'),
 ('TH','TH','THA'),
 ('TJ','TJ','TJK'),
 ('TK','TK','TKL'),
 ('TL','TL','TLS'),
 ('TM','TM','TKM'),
 ('TN','TN','TUN'),
 ('TO','TO','TON'),
 ('TR','TR','TUR'),
 ('TT','TT','TTO'),
 ('TV','TV','TUV'),
 ('TW','TW','TWN'),
 ('TZ','TZ','TZA'),
 ('UA','UA','UKR'),
 ('UG','UG','UGA'),
 ('UM','UM','UMI'),
 ('US','US','USA'),
 ('UY','UY','URY'),
 ('UZ','UZ','UZB'),
 ('VA','VA','VAT'),
 ('VC','VC','VCT'),
 ('VE','VE','VEN'),
 ('VG','VG','VGB'),
 ('VI','VI','VIR'),
 ('VN','VN','VNM'),
 ('VU','VU','VUT'),
 ('WF','WF','WLF'),
 ('WS','WS','WSM'),
 ('YE','YE','YEM'),
 ('YT','YT','MYT'),
 ('ZA','ZA','ZAF'),
 ('ZM','ZM','ZMB'),
 ('ZW','ZW','ZWE');
UNLOCK TABLES;
/*!40000 ALTER TABLE `directory_country` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`directory_country_format`
--

DROP TABLE IF EXISTS `supernarede`.`directory_country_format`;
CREATE TABLE  `supernarede`.`directory_country_format` (
  `country_format_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `country_id` varchar(2) NOT NULL DEFAULT '',
  `type` varchar(30) NOT NULL DEFAULT '',
  `format` text NOT NULL,
  PRIMARY KEY (`country_format_id`),
  UNIQUE KEY `country_type` (`country_id`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Countries format';

--
-- Dumping data for table `supernarede`.`directory_country_format`
--

/*!40000 ALTER TABLE `directory_country_format` DISABLE KEYS */;
LOCK TABLES `directory_country_format` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `directory_country_format` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`directory_country_region`
--

DROP TABLE IF EXISTS `supernarede`.`directory_country_region`;
CREATE TABLE  `supernarede`.`directory_country_region` (
  `region_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `country_id` varchar(4) NOT NULL DEFAULT '0',
  `code` varchar(32) NOT NULL DEFAULT '',
  `default_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`region_id`),
  KEY `FK_REGION_COUNTRY` (`country_id`)
) ENGINE=InnoDB AUTO_INCREMENT=512 DEFAULT CHARSET=utf8 COMMENT='Country regions';

--
-- Dumping data for table `supernarede`.`directory_country_region`
--

/*!40000 ALTER TABLE `directory_country_region` DISABLE KEYS */;
LOCK TABLES `directory_country_region` WRITE;
INSERT INTO `supernarede`.`directory_country_region` VALUES  (1,'US','AL','Alabama'),
 (2,'US','AK','Alaska'),
 (3,'US','AS','American Samoa'),
 (4,'US','AZ','Arizona'),
 (5,'US','AR','Arkansas'),
 (6,'US','AF','Armed Forces Africa'),
 (7,'US','AA','Armed Forces Americas'),
 (8,'US','AC','Armed Forces Canada'),
 (9,'US','AE','Armed Forces Europe'),
 (10,'US','AM','Armed Forces Middle East'),
 (11,'US','AP','Armed Forces Pacific'),
 (12,'US','CA','California'),
 (13,'US','CO','Colorado'),
 (14,'US','CT','Connecticut'),
 (15,'US','DE','Delaware'),
 (16,'US','DC','District of Columbia'),
 (17,'US','FM','Federated States Of Micronesia'),
 (18,'US','FL','Florida'),
 (19,'US','GA','Georgia'),
 (20,'US','GU','Guam'),
 (21,'US','HI','Hawaii'),
 (22,'US','ID','Idaho'),
 (23,'US','IL','Illinois'),
 (24,'US','IN','Indiana'),
 (25,'US','IA','Iowa'),
 (26,'US','KS','Kansas'),
 (27,'US','KY','Kentucky'),
 (28,'US','LA','Louisiana'),
 (29,'US','ME','Maine'),
 (30,'US','MH','Marshall Islands'),
 (31,'US','MD','Maryland'),
 (32,'US','MA','Massachusetts'),
 (33,'US','MI','Michigan'),
 (34,'US','MN','Minnesota'),
 (35,'US','MS','Mississippi'),
 (36,'US','MO','Missouri'),
 (37,'US','MT','Montana'),
 (38,'US','NE','Nebraska'),
 (39,'US','NV','Nevada'),
 (40,'US','NH','New Hampshire'),
 (41,'US','NJ','New Jersey'),
 (42,'US','NM','New Mexico'),
 (43,'US','NY','New York'),
 (44,'US','NC','North Carolina'),
 (45,'US','ND','North Dakota'),
 (46,'US','MP','Northern Mariana Islands'),
 (47,'US','OH','Ohio'),
 (48,'US','OK','Oklahoma'),
 (49,'US','OR','Oregon'),
 (50,'US','PW','Palau'),
 (51,'US','PA','Pennsylvania'),
 (52,'US','PR','Puerto Rico'),
 (53,'US','RI','Rhode Island'),
 (54,'US','SC','South Carolina'),
 (55,'US','SD','South Dakota'),
 (56,'US','TN','Tennessee'),
 (57,'US','TX','Texas'),
 (58,'US','UT','Utah'),
 (59,'US','VT','Vermont'),
 (60,'US','VI','Virgin Islands'),
 (61,'US','VA','Virginia'),
 (62,'US','WA','Washington'),
 (63,'US','WV','West Virginia'),
 (64,'US','WI','Wisconsin'),
 (65,'US','WY','Wyoming'),
 (66,'CA','AB','Alberta'),
 (67,'CA','BC','British Columbia'),
 (68,'CA','MB','Manitoba'),
 (69,'CA','NL','Newfoundland and Labrador'),
 (70,'CA','NB','New Brunswick'),
 (71,'CA','NS','Nova Scotia'),
 (72,'CA','NT','Northwest Territories'),
 (73,'CA','NU','Nunavut'),
 (74,'CA','ON','Ontario'),
 (75,'CA','PE','Prince Edward Island'),
 (76,'CA','QC','Quebec'),
 (77,'CA','SK','Saskatchewan'),
 (78,'CA','YT','Yukon Territory'),
 (79,'DE','NDS','Niedersachsen'),
 (80,'DE','BAW','Baden-Württemberg'),
 (81,'DE','BAY','Bayern'),
 (82,'DE','BER','Berlin'),
 (83,'DE','BRG','Brandenburg'),
 (84,'DE','BRE','Bremen'),
 (85,'DE','HAM','Hamburg'),
 (86,'DE','HES','Hessen'),
 (87,'DE','MEC','Mecklenburg-Vorpommern'),
 (88,'DE','NRW','Nordrhein-Westfalen'),
 (89,'DE','RHE','Rheinland-Pfalz'),
 (90,'DE','SAR','Saarland'),
 (91,'DE','SAS','Sachsen'),
 (92,'DE','SAC','Sachsen-Anhalt'),
 (93,'DE','SCN','Schleswig-Holstein'),
 (94,'DE','THE','Thüringen'),
 (95,'AT','WI','Wien'),
 (96,'AT','NO','Niederösterreich'),
 (97,'AT','OO','Oberösterreich'),
 (98,'AT','SB','Salzburg'),
 (99,'AT','KN','Kärnten'),
 (100,'AT','ST','Steiermark'),
 (101,'AT','TI','Tirol'),
 (102,'AT','BL','Burgenland'),
 (103,'AT','VB','Voralberg'),
 (104,'CH','AG','Aargau'),
 (105,'CH','AI','Appenzell Innerrhoden'),
 (106,'CH','AR','Appenzell Ausserrhoden'),
 (107,'CH','BE','Bern'),
 (108,'CH','BL','Basel-Landschaft'),
 (109,'CH','BS','Basel-Stadt'),
 (110,'CH','FR','Freiburg'),
 (111,'CH','GE','Genf'),
 (112,'CH','GL','Glarus'),
 (113,'CH','GR','Graubünden'),
 (114,'CH','JU','Jura'),
 (115,'CH','LU','Luzern'),
 (116,'CH','NE','Neuenburg'),
 (117,'CH','NW','Nidwalden'),
 (118,'CH','OW','Obwalden'),
 (119,'CH','SG','St. Gallen'),
 (120,'CH','SH','Schaffhausen'),
 (121,'CH','SO','Solothurn'),
 (122,'CH','SZ','Schwyz'),
 (123,'CH','TG','Thurgau'),
 (124,'CH','TI','Tessin'),
 (125,'CH','UR','Uri'),
 (126,'CH','VD','Waadt'),
 (127,'CH','VS','Wallis'),
 (128,'CH','ZG','Zug'),
 (129,'CH','ZH','Zürich'),
 (130,'ES','A Coruсa','A Coruña'),
 (131,'ES','Alava','Alava'),
 (132,'ES','Albacete','Albacete'),
 (133,'ES','Alicante','Alicante'),
 (134,'ES','Almeria','Almeria'),
 (135,'ES','Asturias','Asturias'),
 (136,'ES','Avila','Avila'),
 (137,'ES','Badajoz','Badajoz'),
 (138,'ES','Baleares','Baleares'),
 (139,'ES','Barcelona','Barcelona'),
 (140,'ES','Burgos','Burgos'),
 (141,'ES','Caceres','Caceres'),
 (142,'ES','Cadiz','Cadiz'),
 (143,'ES','Cantabria','Cantabria'),
 (144,'ES','Castellon','Castellon'),
 (145,'ES','Ceuta','Ceuta'),
 (146,'ES','Ciudad Real','Ciudad Real'),
 (147,'ES','Cordoba','Cordoba'),
 (148,'ES','Cuenca','Cuenca'),
 (149,'ES','Girona','Girona'),
 (150,'ES','Granada','Granada'),
 (151,'ES','Guadalajara','Guadalajara'),
 (152,'ES','Guipuzcoa','Guipuzcoa'),
 (153,'ES','Huelva','Huelva'),
 (154,'ES','Huesca','Huesca'),
 (155,'ES','Jaen','Jaen'),
 (156,'ES','La Rioja','La Rioja'),
 (157,'ES','Las Palmas','Las Palmas'),
 (158,'ES','Leon','Leon'),
 (159,'ES','Lleida','Lleida'),
 (160,'ES','Lugo','Lugo'),
 (161,'ES','Madrid','Madrid'),
 (162,'ES','Malaga','Malaga'),
 (163,'ES','Melilla','Melilla'),
 (164,'ES','Murcia','Murcia'),
 (165,'ES','Navarra','Navarra'),
 (166,'ES','Ourense','Ourense'),
 (167,'ES','Palencia','Palencia'),
 (168,'ES','Pontevedra','Pontevedra'),
 (169,'ES','Salamanca','Salamanca'),
 (170,'ES','Santa Cruz de Tenerife','Santa Cruz de Tenerife'),
 (171,'ES','Segovia','Segovia'),
 (172,'ES','Sevilla','Sevilla'),
 (173,'ES','Soria','Soria'),
 (174,'ES','Tarragona','Tarragona'),
 (175,'ES','Teruel','Teruel'),
 (176,'ES','Toledo','Toledo'),
 (177,'ES','Valencia','Valencia'),
 (178,'ES','Valladolid','Valladolid'),
 (179,'ES','Vizcaya','Vizcaya'),
 (180,'ES','Zamora','Zamora'),
 (181,'ES','Zaragoza','Zaragoza'),
 (182,'FR','01','Ain'),
 (183,'FR','02','Aisne'),
 (184,'FR','03','Allier'),
 (185,'FR','04','Alpes-de-Haute-Provence'),
 (186,'FR','05','Hautes-Alpes'),
 (187,'FR','06','Alpes-Maritimes'),
 (188,'FR','07','Ardèche'),
 (189,'FR','08','Ardennes'),
 (190,'FR','09','Ariège'),
 (191,'FR','10','Aube'),
 (192,'FR','11','Aude'),
 (193,'FR','12','Aveyron'),
 (194,'FR','13','Bouches-du-Rhône'),
 (195,'FR','14','Calvados'),
 (196,'FR','15','Cantal'),
 (197,'FR','16','Charente'),
 (198,'FR','17','Charente-Maritime'),
 (199,'FR','18','Cher'),
 (200,'FR','19','Corrèze'),
 (201,'FR','2A','Corse-du-Sud'),
 (202,'FR','2B','Haute-Corse'),
 (203,'FR','21','Côte-d\'Or'),
 (204,'FR','22','Côtes-d\'Armor'),
 (205,'FR','23','Creuse'),
 (206,'FR','24','Dordogne'),
 (207,'FR','25','Doubs'),
 (208,'FR','26','Drôme'),
 (209,'FR','27','Eure'),
 (210,'FR','28','Eure-et-Loir'),
 (211,'FR','29','Finistère'),
 (212,'FR','30','Gard'),
 (213,'FR','31','Haute-Garonne'),
 (214,'FR','32','Gers'),
 (215,'FR','33','Gironde'),
 (216,'FR','34','Hérault'),
 (217,'FR','35','Ille-et-Vilaine'),
 (218,'FR','36','Indre'),
 (219,'FR','37','Indre-et-Loire'),
 (220,'FR','38','Isère'),
 (221,'FR','39','Jura'),
 (222,'FR','40','Landes'),
 (223,'FR','41','Loir-et-Cher'),
 (224,'FR','42','Loire'),
 (225,'FR','43','Haute-Loire'),
 (226,'FR','44','Loire-Atlantique'),
 (227,'FR','45','Loiret'),
 (228,'FR','46','Lot'),
 (229,'FR','47','Lot-et-Garonne'),
 (230,'FR','48','Lozère'),
 (231,'FR','49','Maine-et-Loire'),
 (232,'FR','50','Manche'),
 (233,'FR','51','Marne'),
 (234,'FR','52','Haute-Marne'),
 (235,'FR','53','Mayenne'),
 (236,'FR','54','Meurthe-et-Moselle'),
 (237,'FR','55','Meuse'),
 (238,'FR','56','Morbihan'),
 (239,'FR','57','Moselle'),
 (240,'FR','58','Nièvre'),
 (241,'FR','59','Nord'),
 (242,'FR','60','Oise'),
 (243,'FR','61','Orne'),
 (244,'FR','62','Pas-de-Calais'),
 (245,'FR','63','Puy-de-Dôme'),
 (246,'FR','64','Pyrénées-Atlantiques'),
 (247,'FR','65','Hautes-Pyrénées'),
 (248,'FR','66','Pyrénées-Orientales'),
 (249,'FR','67','Bas-Rhin'),
 (250,'FR','68','Haut-Rhin'),
 (251,'FR','69','Rhône'),
 (252,'FR','70','Haute-Saône'),
 (253,'FR','71','Saône-et-Loire'),
 (254,'FR','72','Sarthe'),
 (255,'FR','73','Savoie'),
 (256,'FR','74','Haute-Savoie'),
 (257,'FR','75','Paris'),
 (258,'FR','76','Seine-Maritime'),
 (259,'FR','77','Seine-et-Marne'),
 (260,'FR','78','Yvelines'),
 (261,'FR','79','Deux-Sèvres'),
 (262,'FR','80','Somme'),
 (263,'FR','81','Tarn'),
 (264,'FR','82','Tarn-et-Garonne'),
 (265,'FR','83','Var'),
 (266,'FR','84','Vaucluse'),
 (267,'FR','85','Vendée'),
 (268,'FR','86','Vienne'),
 (269,'FR','87','Haute-Vienne'),
 (270,'FR','88','Vosges'),
 (271,'FR','89','Yonne'),
 (272,'FR','90','Territoire-de-Belfort'),
 (273,'FR','91','Essonne'),
 (274,'FR','92','Hauts-de-Seine'),
 (275,'FR','93','Seine-Saint-Denis'),
 (276,'FR','94','Val-de-Marne'),
 (277,'FR','95','Val-d\'Oise'),
 (278,'RO','AB','Alba'),
 (279,'RO','AR','Arad'),
 (280,'RO','AG','Argeş'),
 (281,'RO','BC','Bacău'),
 (282,'RO','BH','Bihor'),
 (283,'RO','BN','Bistriţa-Năsăud'),
 (284,'RO','BT','Botoşani'),
 (285,'RO','BV','Braşov'),
 (286,'RO','BR','Brăila'),
 (287,'RO','B','Bucureşti'),
 (288,'RO','BZ','Buzău'),
 (289,'RO','CS','Caraş-Severin'),
 (290,'RO','CL','Călăraşi'),
 (291,'RO','CJ','Cluj'),
 (292,'RO','CT','Constanţa'),
 (293,'RO','CV','Covasna'),
 (294,'RO','DB','Dâmboviţa'),
 (295,'RO','DJ','Dolj'),
 (296,'RO','GL','Galaţi'),
 (297,'RO','GR','Giurgiu'),
 (298,'RO','GJ','Gorj'),
 (299,'RO','HR','Harghita'),
 (300,'RO','HD','Hunedoara'),
 (301,'RO','IL','Ialomiţa'),
 (302,'RO','IS','Iaşi'),
 (303,'RO','IF','Ilfov'),
 (304,'RO','MM','Maramureş'),
 (305,'RO','MH','Mehedinţi'),
 (306,'RO','MS','Mureş'),
 (307,'RO','NT','Neamţ'),
 (308,'RO','OT','Olt'),
 (309,'RO','PH','Prahova'),
 (310,'RO','SM','Satu-Mare'),
 (311,'RO','SJ','Sălaj'),
 (312,'RO','SB','Sibiu'),
 (313,'RO','SV','Suceava'),
 (314,'RO','TR','Teleorman'),
 (315,'RO','TM','Timiş'),
 (316,'RO','TL','Tulcea'),
 (317,'RO','VS','Vaslui'),
 (318,'RO','VL','Vâlcea'),
 (319,'RO','VN','Vrancea'),
 (320,'FI','Lappi','Lappi'),
 (321,'FI','Pohjois-Pohjanmaa','Pohjois-Pohjanmaa'),
 (322,'FI','Kainuu','Kainuu'),
 (323,'FI','Pohjois-Karjala','Pohjois-Karjala'),
 (324,'FI','Pohjois-Savo','Pohjois-Savo'),
 (325,'FI','Etelä-Savo','Etelä-Savo'),
 (326,'FI','Etelä-Pohjanmaa','Etelä-Pohjanmaa'),
 (327,'FI','Pohjanmaa','Pohjanmaa'),
 (328,'FI','Pirkanmaa','Pirkanmaa'),
 (329,'FI','Satakunta','Satakunta'),
 (330,'FI','Keski-Pohjanmaa','Keski-Pohjanmaa'),
 (331,'FI','Keski-Suomi','Keski-Suomi'),
 (332,'FI','Varsinais-Suomi','Varsinais-Suomi'),
 (333,'FI','Etelä-Karjala','Etelä-Karjala'),
 (334,'FI','Päijät-Häme','Päijät-Häme'),
 (335,'FI','Kanta-Häme','Kanta-Häme'),
 (336,'FI','Uusimaa','Uusimaa'),
 (337,'FI','Itä-Uusimaa','Itä-Uusimaa'),
 (338,'FI','Kymenlaakso','Kymenlaakso'),
 (339,'FI','Ahvenanmaa','Ahvenanmaa'),
 (340,'EE','EE-37','Harjumaa'),
 (341,'EE','EE-39','Hiiumaa'),
 (342,'EE','EE-44','Ida-Virumaa'),
 (343,'EE','EE-49','Jõgevamaa'),
 (344,'EE','EE-51','Järvamaa'),
 (345,'EE','EE-57','Läänemaa'),
 (346,'EE','EE-59','Lääne-Virumaa'),
 (347,'EE','EE-65','Põlvamaa'),
 (348,'EE','EE-67','Pärnumaa'),
 (349,'EE','EE-70','Raplamaa'),
 (350,'EE','EE-74','Saaremaa'),
 (351,'EE','EE-78','Tartumaa'),
 (352,'EE','EE-82','Valgamaa'),
 (353,'EE','EE-84','Viljandimaa'),
 (354,'EE','EE-86','Võrumaa'),
 (355,'LV','LV-DGV','Daugavpils'),
 (356,'LV','LV-JEL','Jelgava'),
 (357,'LV','Jēkabpils','Jēkabpils'),
 (358,'LV','LV-JUR','Jūrmala'),
 (359,'LV','LV-LPX','Liepāja'),
 (360,'LV','LV-LE','Liepājas novads'),
 (361,'LV','LV-REZ','Rēzekne'),
 (362,'LV','LV-RIX','Rīga'),
 (363,'LV','LV-RI','Rīgas novads'),
 (364,'LV','Valmiera','Valmiera'),
 (365,'LV','LV-VEN','Ventspils'),
 (366,'LV','Aglonas novads','Aglonas novads'),
 (367,'LV','LV-AI','Aizkraukles novads'),
 (368,'LV','Aizputes novads','Aizputes novads'),
 (369,'LV','Aknīstes novads','Aknīstes novads'),
 (370,'LV','Alojas novads','Alojas novads'),
 (371,'LV','Alsungas novads','Alsungas novads'),
 (372,'LV','LV-AL','Alūksnes novads'),
 (373,'LV','Amatas novads','Amatas novads'),
 (374,'LV','Apes novads','Apes novads'),
 (375,'LV','Auces novads','Auces novads'),
 (376,'LV','Babītes novads','Babītes novads'),
 (377,'LV','Baldones novads','Baldones novads'),
 (378,'LV','Baltinavas novads','Baltinavas novads'),
 (379,'LV','LV-BL','Balvu novads'),
 (380,'LV','LV-BU','Bauskas novads'),
 (381,'LV','Beverīnas novads','Beverīnas novads'),
 (382,'LV','Brocēnu novads','Brocēnu novads'),
 (383,'LV','Burtnieku novads','Burtnieku novads'),
 (384,'LV','Carnikavas novads','Carnikavas novads'),
 (385,'LV','Cesvaines novads','Cesvaines novads'),
 (386,'LV','Ciblas novads','Ciblas novads'),
 (387,'LV','LV-CE','Cēsu novads'),
 (388,'LV','Dagdas novads','Dagdas novads'),
 (389,'LV','LV-DA','Daugavpils novads'),
 (390,'LV','LV-DO','Dobeles novads'),
 (391,'LV','Dundagas novads','Dundagas novads'),
 (392,'LV','Durbes novads','Durbes novads'),
 (393,'LV','Engures novads','Engures novads'),
 (394,'LV','Garkalnes novads','Garkalnes novads'),
 (395,'LV','Grobiņas novads','Grobiņas novads'),
 (396,'LV','LV-GU','Gulbenes novads'),
 (397,'LV','Iecavas novads','Iecavas novads'),
 (398,'LV','Ikšķiles novads','Ikšķiles novads'),
 (399,'LV','Ilūkstes novads','Ilūkstes novads'),
 (400,'LV','Inčukalna novads','Inčukalna novads'),
 (401,'LV','Jaunjelgavas novads','Jaunjelgavas novads'),
 (402,'LV','Jaunpiebalgas novads','Jaunpiebalgas novads'),
 (403,'LV','Jaunpils novads','Jaunpils novads'),
 (404,'LV','LV-JL','Jelgavas novads'),
 (405,'LV','LV-JK','Jēkabpils novads'),
 (406,'LV','Kandavas novads','Kandavas novads'),
 (407,'LV','Kokneses novads','Kokneses novads'),
 (408,'LV','Krimuldas novads','Krimuldas novads'),
 (409,'LV','Krustpils novads','Krustpils novads'),
 (410,'LV','LV-KR','Krāslavas novads'),
 (411,'LV','LV-KU','Kuldīgas novads'),
 (412,'LV','Kārsavas novads','Kārsavas novads'),
 (413,'LV','Lielvārdes novads','Lielvārdes novads'),
 (414,'LV','LV-LM','Limbažu novads'),
 (415,'LV','Lubānas novads','Lubānas novads'),
 (416,'LV','LV-LU','Ludzas novads'),
 (417,'LV','Līgatnes novads','Līgatnes novads'),
 (418,'LV','Līvānu novads','Līvānu novads'),
 (419,'LV','LV-MA','Madonas novads'),
 (420,'LV','Mazsalacas novads','Mazsalacas novads'),
 (421,'LV','Mālpils novads','Mālpils novads'),
 (422,'LV','Mārupes novads','Mārupes novads'),
 (423,'LV','Naukšēnu novads','Naukšēnu novads'),
 (424,'LV','Neretas novads','Neretas novads'),
 (425,'LV','Nīcas novads','Nīcas novads'),
 (426,'LV','LV-OG','Ogres novads'),
 (427,'LV','Olaines novads','Olaines novads'),
 (428,'LV','Ozolnieku novads','Ozolnieku novads'),
 (429,'LV','LV-PR','Preiļu novads'),
 (430,'LV','Priekules novads','Priekules novads'),
 (431,'LV','Priekuļu novads','Priekuļu novads'),
 (432,'LV','Pārgaujas novads','Pārgaujas novads'),
 (433,'LV','Pāvilostas novads','Pāvilostas novads'),
 (434,'LV','Pļaviņu novads','Pļaviņu novads'),
 (435,'LV','Raunas novads','Raunas novads'),
 (436,'LV','Riebiņu novads','Riebiņu novads'),
 (437,'LV','Rojas novads','Rojas novads'),
 (438,'LV','Ropažu novads','Ropažu novads'),
 (439,'LV','Rucavas novads','Rucavas novads'),
 (440,'LV','Rugāju novads','Rugāju novads'),
 (441,'LV','Rundāles novads','Rundāles novads'),
 (442,'LV','LV-RE','Rēzeknes novads'),
 (443,'LV','Rūjienas novads','Rūjienas novads'),
 (444,'LV','Salacgrīvas novads','Salacgrīvas novads'),
 (445,'LV','Salas novads','Salas novads'),
 (446,'LV','Salaspils novads','Salaspils novads'),
 (447,'LV','LV-SA','Saldus novads'),
 (448,'LV','Saulkrastu novads','Saulkrastu novads'),
 (449,'LV','Siguldas novads','Siguldas novads'),
 (450,'LV','Skrundas novads','Skrundas novads'),
 (451,'LV','Skrīveru novads','Skrīveru novads'),
 (452,'LV','Smiltenes novads','Smiltenes novads'),
 (453,'LV','Stopiņu novads','Stopiņu novads'),
 (454,'LV','Strenču novads','Strenču novads'),
 (455,'LV','Sējas novads','Sējas novads'),
 (456,'LV','LV-TA','Talsu novads'),
 (457,'LV','LV-TU','Tukuma novads'),
 (458,'LV','Tērvetes novads','Tērvetes novads'),
 (459,'LV','Vaiņodes novads','Vaiņodes novads'),
 (460,'LV','LV-VK','Valkas novads'),
 (461,'LV','LV-VM','Valmieras novads'),
 (462,'LV','Varakļānu novads','Varakļānu novads'),
 (463,'LV','Vecpiebalgas novads','Vecpiebalgas novads'),
 (464,'LV','Vecumnieku novads','Vecumnieku novads'),
 (465,'LV','LV-VE','Ventspils novads'),
 (466,'LV','Viesītes novads','Viesītes novads'),
 (467,'LV','Viļakas novads','Viļakas novads'),
 (468,'LV','Viļānu novads','Viļānu novads'),
 (469,'LV','Vārkavas novads','Vārkavas novads'),
 (470,'LV','Zilupes novads','Zilupes novads'),
 (471,'LV','Ādažu novads','Ādažu novads'),
 (472,'LV','Ērgļu novads','Ērgļu novads'),
 (473,'LV','Ķeguma novads','Ķeguma novads'),
 (474,'LV','Ķekavas novads','Ķekavas novads'),
 (475,'LT','LT-AL','Alytaus Apskritis'),
 (476,'LT','LT-KU','Kauno Apskritis'),
 (477,'LT','LT-KL','Klaipėdos Apskritis'),
 (478,'LT','LT-MR','Marijampolės Apskritis'),
 (479,'LT','LT-PN','Panevėžio Apskritis'),
 (480,'LT','LT-SA','Šiaulių Apskritis'),
 (481,'LT','LT-TA','Tauragės Apskritis'),
 (482,'LT','LT-TE','Telšių Apskritis'),
 (483,'LT','LT-UT','Utenos Apskritis'),
 (484,'LT','LT-VL','Vilniaus Apskritis'),
 (485,'BR','AC','Acre'),
 (486,'BR','AL','Alagoas'),
 (487,'BR','AP','Amapá'),
 (488,'BR','AM','Amazonas'),
 (489,'BR','BA','Bahia'),
 (490,'BR','CE','Ceará'),
 (491,'BR','ES','Espírito Santo'),
 (492,'BR','GO','Goiás'),
 (493,'BR','MA','Maranhão'),
 (494,'BR','MT','Mato Grosso'),
 (495,'BR','MS','Mato Grosso do Sul'),
 (496,'BR','MG','Minas Gerais'),
 (497,'BR','PA','Pará'),
 (498,'BR','PB','Paraíba'),
 (499,'BR','PR','Paraná'),
 (500,'BR','PE','Pernambuco'),
 (501,'BR','PI','Piauí'),
 (502,'BR','RJ','Rio de Janeiro'),
 (503,'BR','RN','Rio Grande do Norte'),
 (504,'BR','RS','Rio Grande do Sul'),
 (505,'BR','RO','Rondônia'),
 (506,'BR','RR','Roraima'),
 (507,'BR','SC','Santa Catarina'),
 (508,'BR','SP','São Paulo'),
 (509,'BR','SE','Sergipe'),
 (510,'BR','TO','Tocantins'),
 (511,'BR','DF','Distrito Federal');
UNLOCK TABLES;
/*!40000 ALTER TABLE `directory_country_region` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`directory_country_region_name`
--

DROP TABLE IF EXISTS `supernarede`.`directory_country_region_name`;
CREATE TABLE  `supernarede`.`directory_country_region_name` (
  `locale` varchar(8) NOT NULL DEFAULT '',
  `region_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `name` varchar(64) NOT NULL DEFAULT '',
  PRIMARY KEY (`locale`,`region_id`),
  KEY `FK_DIRECTORY_REGION_NAME_REGION` (`region_id`),
  CONSTRAINT `FK_DIRECTORY_REGION_NAME_REGION` FOREIGN KEY (`region_id`) REFERENCES `directory_country_region` (`region_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Regions names';

--
-- Dumping data for table `supernarede`.`directory_country_region_name`
--

/*!40000 ALTER TABLE `directory_country_region_name` DISABLE KEYS */;
LOCK TABLES `directory_country_region_name` WRITE;
INSERT INTO `supernarede`.`directory_country_region_name` VALUES  ('en_US',1,'Alabama'),
 ('en_US',2,'Alaska'),
 ('en_US',3,'American Samoa'),
 ('en_US',4,'Arizona'),
 ('en_US',5,'Arkansas'),
 ('en_US',6,'Armed Forces Africa'),
 ('en_US',7,'Armed Forces Americas'),
 ('en_US',8,'Armed Forces Canada'),
 ('en_US',9,'Armed Forces Europe'),
 ('en_US',10,'Armed Forces Middle East'),
 ('en_US',11,'Armed Forces Pacific'),
 ('en_US',12,'California'),
 ('en_US',13,'Colorado'),
 ('en_US',14,'Connecticut'),
 ('en_US',15,'Delaware'),
 ('en_US',16,'District of Columbia'),
 ('en_US',17,'Federated States Of Micronesia'),
 ('en_US',18,'Florida'),
 ('en_US',19,'Georgia'),
 ('en_US',20,'Guam'),
 ('en_US',21,'Hawaii'),
 ('en_US',22,'Idaho'),
 ('en_US',23,'Illinois'),
 ('en_US',24,'Indiana'),
 ('en_US',25,'Iowa'),
 ('en_US',26,'Kansas'),
 ('en_US',27,'Kentucky'),
 ('en_US',28,'Louisiana'),
 ('en_US',29,'Maine'),
 ('en_US',30,'Marshall Islands'),
 ('en_US',31,'Maryland'),
 ('en_US',32,'Massachusetts'),
 ('en_US',33,'Michigan'),
 ('en_US',34,'Minnesota'),
 ('en_US',35,'Mississippi'),
 ('en_US',36,'Missouri'),
 ('en_US',37,'Montana'),
 ('en_US',38,'Nebraska'),
 ('en_US',39,'Nevada'),
 ('en_US',40,'New Hampshire'),
 ('en_US',41,'New Jersey'),
 ('en_US',42,'New Mexico'),
 ('en_US',43,'New York'),
 ('en_US',44,'North Carolina'),
 ('en_US',45,'North Dakota'),
 ('en_US',46,'Northern Mariana Islands'),
 ('en_US',47,'Ohio'),
 ('en_US',48,'Oklahoma'),
 ('en_US',49,'Oregon'),
 ('en_US',50,'Palau'),
 ('en_US',51,'Pennsylvania'),
 ('en_US',52,'Puerto Rico'),
 ('en_US',53,'Rhode Island'),
 ('en_US',54,'South Carolina'),
 ('en_US',55,'South Dakota'),
 ('en_US',56,'Tennessee'),
 ('en_US',57,'Texas'),
 ('en_US',58,'Utah'),
 ('en_US',59,'Vermont'),
 ('en_US',60,'Virgin Islands'),
 ('en_US',61,'Virginia'),
 ('en_US',62,'Washington'),
 ('en_US',63,'West Virginia'),
 ('en_US',64,'Wisconsin'),
 ('en_US',65,'Wyoming'),
 ('en_US',66,'Alberta'),
 ('en_US',67,'British Columbia'),
 ('en_US',68,'Manitoba'),
 ('en_US',69,'Newfoundland and Labrador'),
 ('en_US',70,'New Brunswick'),
 ('en_US',71,'Nova Scotia'),
 ('en_US',72,'Northwest Territories'),
 ('en_US',73,'Nunavut'),
 ('en_US',74,'Ontario'),
 ('en_US',75,'Prince Edward Island'),
 ('en_US',76,'Quebec'),
 ('en_US',77,'Saskatchewan'),
 ('en_US',78,'Yukon Territory'),
 ('en_US',79,'Niedersachsen'),
 ('en_US',80,'Baden-Württemberg'),
 ('en_US',81,'Bayern'),
 ('en_US',82,'Berlin'),
 ('en_US',83,'Brandenburg'),
 ('en_US',84,'Bremen'),
 ('en_US',85,'Hamburg'),
 ('en_US',86,'Hessen'),
 ('en_US',87,'Mecklenburg-Vorpommern'),
 ('en_US',88,'Nordrhein-Westfalen'),
 ('en_US',89,'Rheinland-Pfalz'),
 ('en_US',90,'Saarland'),
 ('en_US',91,'Sachsen'),
 ('en_US',92,'Sachsen-Anhalt'),
 ('en_US',93,'Schleswig-Holstein'),
 ('en_US',94,'Thüringen'),
 ('en_US',95,'Wien'),
 ('en_US',96,'Niederösterreich'),
 ('en_US',97,'Oberösterreich'),
 ('en_US',98,'Salzburg'),
 ('en_US',99,'Kärnten'),
 ('en_US',100,'Steiermark'),
 ('en_US',101,'Tirol'),
 ('en_US',102,'Burgenland'),
 ('en_US',103,'Voralberg'),
 ('en_US',104,'Aargau'),
 ('en_US',105,'Appenzell Innerrhoden'),
 ('en_US',106,'Appenzell Ausserrhoden'),
 ('en_US',107,'Bern'),
 ('en_US',108,'Basel-Landschaft'),
 ('en_US',109,'Basel-Stadt'),
 ('en_US',110,'Freiburg'),
 ('en_US',111,'Genf'),
 ('en_US',112,'Glarus'),
 ('en_US',113,'Graubünden'),
 ('en_US',114,'Jura'),
 ('en_US',115,'Luzern'),
 ('en_US',116,'Neuenburg'),
 ('en_US',117,'Nidwalden'),
 ('en_US',118,'Obwalden'),
 ('en_US',119,'St. Gallen'),
 ('en_US',120,'Schaffhausen'),
 ('en_US',121,'Solothurn'),
 ('en_US',122,'Schwyz'),
 ('en_US',123,'Thurgau'),
 ('en_US',124,'Tessin'),
 ('en_US',125,'Uri'),
 ('en_US',126,'Waadt'),
 ('en_US',127,'Wallis'),
 ('en_US',128,'Zug'),
 ('en_US',129,'Zürich'),
 ('en_US',130,'A Coruña'),
 ('en_US',131,'Alava'),
 ('en_US',132,'Albacete'),
 ('en_US',133,'Alicante'),
 ('en_US',134,'Almeria'),
 ('en_US',135,'Asturias'),
 ('en_US',136,'Avila'),
 ('en_US',137,'Badajoz'),
 ('en_US',138,'Baleares'),
 ('en_US',139,'Barcelona'),
 ('en_US',140,'Burgos'),
 ('en_US',141,'Caceres'),
 ('en_US',142,'Cadiz'),
 ('en_US',143,'Cantabria'),
 ('en_US',144,'Castellon'),
 ('en_US',145,'Ceuta'),
 ('en_US',146,'Ciudad Real'),
 ('en_US',147,'Cordoba'),
 ('en_US',148,'Cuenca'),
 ('en_US',149,'Girona'),
 ('en_US',150,'Granada'),
 ('en_US',151,'Guadalajara'),
 ('en_US',152,'Guipuzcoa'),
 ('en_US',153,'Huelva'),
 ('en_US',154,'Huesca'),
 ('en_US',155,'Jaen'),
 ('en_US',156,'La Rioja'),
 ('en_US',157,'Las Palmas'),
 ('en_US',158,'Leon'),
 ('en_US',159,'Lleida'),
 ('en_US',160,'Lugo'),
 ('en_US',161,'Madrid'),
 ('en_US',162,'Malaga'),
 ('en_US',163,'Melilla'),
 ('en_US',164,'Murcia'),
 ('en_US',165,'Navarra'),
 ('en_US',166,'Ourense'),
 ('en_US',167,'Palencia'),
 ('en_US',168,'Pontevedra'),
 ('en_US',169,'Salamanca'),
 ('en_US',170,'Santa Cruz de Tenerife'),
 ('en_US',171,'Segovia'),
 ('en_US',172,'Sevilla'),
 ('en_US',173,'Soria'),
 ('en_US',174,'Tarragona'),
 ('en_US',175,'Teruel'),
 ('en_US',176,'Toledo'),
 ('en_US',177,'Valencia'),
 ('en_US',178,'Valladolid'),
 ('en_US',179,'Vizcaya'),
 ('en_US',180,'Zamora'),
 ('en_US',181,'Zaragoza'),
 ('en_US',182,'Ain'),
 ('en_US',183,'Aisne'),
 ('en_US',184,'Allier'),
 ('en_US',185,'Alpes-de-Haute-Provence'),
 ('en_US',186,'Hautes-Alpes'),
 ('en_US',187,'Alpes-Maritimes'),
 ('en_US',188,'Ardèche'),
 ('en_US',189,'Ardennes'),
 ('en_US',190,'Ariège'),
 ('en_US',191,'Aube'),
 ('en_US',192,'Aude'),
 ('en_US',193,'Aveyron'),
 ('en_US',194,'Bouches-du-Rhône'),
 ('en_US',195,'Calvados'),
 ('en_US',196,'Cantal'),
 ('en_US',197,'Charente'),
 ('en_US',198,'Charente-Maritime'),
 ('en_US',199,'Cher'),
 ('en_US',200,'Corrèze'),
 ('en_US',201,'Corse-du-Sud'),
 ('en_US',202,'Haute-Corse'),
 ('en_US',203,'Côte-d\'Or'),
 ('en_US',204,'Côtes-d\'Armor'),
 ('en_US',205,'Creuse'),
 ('en_US',206,'Dordogne'),
 ('en_US',207,'Doubs'),
 ('en_US',208,'Drôme'),
 ('en_US',209,'Eure'),
 ('en_US',210,'Eure-et-Loir'),
 ('en_US',211,'Finistère'),
 ('en_US',212,'Gard'),
 ('en_US',213,'Haute-Garonne'),
 ('en_US',214,'Gers'),
 ('en_US',215,'Gironde'),
 ('en_US',216,'Hérault'),
 ('en_US',217,'Ille-et-Vilaine'),
 ('en_US',218,'Indre'),
 ('en_US',219,'Indre-et-Loire'),
 ('en_US',220,'Isère'),
 ('en_US',221,'Jura'),
 ('en_US',222,'Landes'),
 ('en_US',223,'Loir-et-Cher'),
 ('en_US',224,'Loire'),
 ('en_US',225,'Haute-Loire'),
 ('en_US',226,'Loire-Atlantique'),
 ('en_US',227,'Loiret'),
 ('en_US',228,'Lot'),
 ('en_US',229,'Lot-et-Garonne'),
 ('en_US',230,'Lozère'),
 ('en_US',231,'Maine-et-Loire'),
 ('en_US',232,'Manche'),
 ('en_US',233,'Marne'),
 ('en_US',234,'Haute-Marne'),
 ('en_US',235,'Mayenne'),
 ('en_US',236,'Meurthe-et-Moselle'),
 ('en_US',237,'Meuse'),
 ('en_US',238,'Morbihan'),
 ('en_US',239,'Moselle'),
 ('en_US',240,'Nièvre'),
 ('en_US',241,'Nord'),
 ('en_US',242,'Oise'),
 ('en_US',243,'Orne'),
 ('en_US',244,'Pas-de-Calais'),
 ('en_US',245,'Puy-de-Dôme'),
 ('en_US',246,'Pyrénées-Atlantiques'),
 ('en_US',247,'Hautes-Pyrénées'),
 ('en_US',248,'Pyrénées-Orientales'),
 ('en_US',249,'Bas-Rhin'),
 ('en_US',250,'Haut-Rhin'),
 ('en_US',251,'Rhône'),
 ('en_US',252,'Haute-Saône'),
 ('en_US',253,'Saône-et-Loire'),
 ('en_US',254,'Sarthe'),
 ('en_US',255,'Savoie'),
 ('en_US',256,'Haute-Savoie'),
 ('en_US',257,'Paris'),
 ('en_US',258,'Seine-Maritime'),
 ('en_US',259,'Seine-et-Marne'),
 ('en_US',260,'Yvelines'),
 ('en_US',261,'Deux-Sèvres'),
 ('en_US',262,'Somme'),
 ('en_US',263,'Tarn'),
 ('en_US',264,'Tarn-et-Garonne'),
 ('en_US',265,'Var'),
 ('en_US',266,'Vaucluse'),
 ('en_US',267,'Vendée'),
 ('en_US',268,'Vienne'),
 ('en_US',269,'Haute-Vienne'),
 ('en_US',270,'Vosges'),
 ('en_US',271,'Yonne'),
 ('en_US',272,'Territoire-de-Belfort'),
 ('en_US',273,'Essonne'),
 ('en_US',274,'Hauts-de-Seine'),
 ('en_US',275,'Seine-Saint-Denis'),
 ('en_US',276,'Val-de-Marne'),
 ('en_US',277,'Val-d\'Oise'),
 ('en_US',278,'Alba'),
 ('en_US',279,'Arad'),
 ('en_US',280,'Argeş'),
 ('en_US',281,'Bacău'),
 ('en_US',282,'Bihor'),
 ('en_US',283,'Bistriţa-Năsăud'),
 ('en_US',284,'Botoşani'),
 ('en_US',285,'Braşov'),
 ('en_US',286,'Brăila'),
 ('en_US',287,'Bucureşti'),
 ('en_US',288,'Buzău'),
 ('en_US',289,'Caraş-Severin'),
 ('en_US',290,'Călăraşi'),
 ('en_US',291,'Cluj'),
 ('en_US',292,'Constanţa'),
 ('en_US',293,'Covasna'),
 ('en_US',294,'Dâmboviţa'),
 ('en_US',295,'Dolj'),
 ('en_US',296,'Galaţi'),
 ('en_US',297,'Giurgiu'),
 ('en_US',298,'Gorj'),
 ('en_US',299,'Harghita'),
 ('en_US',300,'Hunedoara'),
 ('en_US',301,'Ialomiţa'),
 ('en_US',302,'Iaşi'),
 ('en_US',303,'Ilfov'),
 ('en_US',304,'Maramureş'),
 ('en_US',305,'Mehedinţi'),
 ('en_US',306,'Mureş'),
 ('en_US',307,'Neamţ'),
 ('en_US',308,'Olt'),
 ('en_US',309,'Prahova'),
 ('en_US',310,'Satu-Mare'),
 ('en_US',311,'Sălaj'),
 ('en_US',312,'Sibiu'),
 ('en_US',313,'Suceava'),
 ('en_US',314,'Teleorman'),
 ('en_US',315,'Timiş'),
 ('en_US',316,'Tulcea'),
 ('en_US',317,'Vaslui'),
 ('en_US',318,'Vâlcea'),
 ('en_US',319,'Vrancea'),
 ('pt_BR',320,'Acre'),
 ('pt_BR',321,'Alagoas'),
 ('pt_BR',322,'Amapá'),
 ('pt_BR',323,'Amazonas'),
 ('pt_BR',324,'Bahia'),
 ('pt_BR',325,'Ceará'),
 ('pt_BR',326,'Espírito Santo'),
 ('pt_BR',327,'Goiás'),
 ('pt_BR',328,'Maranhão'),
 ('pt_BR',329,'Mato Grosso'),
 ('pt_BR',330,'Mato Grosso do Sul'),
 ('pt_BR',331,'Minas Gerais'),
 ('pt_BR',332,'Pará'),
 ('pt_BR',333,'Paraíba'),
 ('pt_BR',334,'Paraná'),
 ('pt_BR',335,'Pernambuco'),
 ('pt_BR',336,'Piauí'),
 ('pt_BR',337,'Rio de Janeiro'),
 ('pt_BR',338,'Rio Grande do Norte'),
 ('pt_BR',339,'Rio Grande do Sul'),
 ('pt_BR',340,'Rondônia'),
 ('pt_BR',341,'Roraima'),
 ('pt_BR',342,'Santa Catarina'),
 ('pt_BR',343,'São Paulo'),
 ('pt_BR',344,'Sergipe'),
 ('pt_BR',345,'Tocantins'),
 ('pt_BR',346,'Distrito Federal');
UNLOCK TABLES;
/*!40000 ALTER TABLE `directory_country_region_name` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`directory_currency_rate`
--

DROP TABLE IF EXISTS `supernarede`.`directory_currency_rate`;
CREATE TABLE  `supernarede`.`directory_currency_rate` (
  `currency_from` char(3) NOT NULL DEFAULT '',
  `currency_to` char(3) NOT NULL DEFAULT '',
  `rate` decimal(24,12) NOT NULL DEFAULT '0.000000000000',
  PRIMARY KEY (`currency_from`,`currency_to`),
  KEY `FK_CURRENCY_RATE_TO` (`currency_to`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`directory_currency_rate`
--

/*!40000 ALTER TABLE `directory_currency_rate` DISABLE KEYS */;
LOCK TABLES `directory_currency_rate` WRITE;
INSERT INTO `supernarede`.`directory_currency_rate` VALUES  ('EUR','EUR','1.000000000000'),
 ('EUR','USD','1.415000000000'),
 ('USD','EUR','0.706700000000'),
 ('USD','USD','1.000000000000');
UNLOCK TABLES;
/*!40000 ALTER TABLE `directory_currency_rate` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`downloadable_link`
--

DROP TABLE IF EXISTS `supernarede`.`downloadable_link`;
CREATE TABLE  `supernarede`.`downloadable_link` (
  `link_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(10) unsigned NOT NULL DEFAULT '0',
  `sort_order` int(10) unsigned NOT NULL DEFAULT '0',
  `number_of_downloads` int(10) unsigned DEFAULT NULL,
  `is_shareable` smallint(1) unsigned NOT NULL DEFAULT '0',
  `link_url` varchar(255) NOT NULL DEFAULT '',
  `link_file` varchar(255) NOT NULL DEFAULT '',
  `link_type` varchar(20) NOT NULL DEFAULT '',
  `sample_url` varchar(255) NOT NULL DEFAULT '',
  `sample_file` varchar(255) NOT NULL DEFAULT '',
  `sample_type` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`link_id`),
  KEY `DOWNLODABLE_LINK_PRODUCT` (`product_id`),
  KEY `DOWNLODABLE_LINK_PRODUCT_SORT_ORDER` (`product_id`,`sort_order`),
  CONSTRAINT `FK_DOWNLODABLE_LINK_PRODUCT` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`downloadable_link`
--

/*!40000 ALTER TABLE `downloadable_link` DISABLE KEYS */;
LOCK TABLES `downloadable_link` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `downloadable_link` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`downloadable_link_price`
--

DROP TABLE IF EXISTS `supernarede`.`downloadable_link_price`;
CREATE TABLE  `supernarede`.`downloadable_link_price` (
  `price_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `link_id` int(10) unsigned NOT NULL DEFAULT '0',
  `website_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `price` decimal(12,4) NOT NULL DEFAULT '0.0000',
  PRIMARY KEY (`price_id`),
  KEY `DOWNLOADABLE_LINK_PRICE_LINK` (`link_id`),
  KEY `DOWNLOADABLE_LINK_PRICE_WEBSITE` (`website_id`),
  CONSTRAINT `FK_DOWNLOADABLE_LINK_PRICE_LINK` FOREIGN KEY (`link_id`) REFERENCES `downloadable_link` (`link_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_DOWNLOADABLE_LINK_PRICE_WEBSITE` FOREIGN KEY (`website_id`) REFERENCES `core_website` (`website_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`downloadable_link_price`
--

/*!40000 ALTER TABLE `downloadable_link_price` DISABLE KEYS */;
LOCK TABLES `downloadable_link_price` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `downloadable_link_price` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`downloadable_link_purchased`
--

DROP TABLE IF EXISTS `supernarede`.`downloadable_link_purchased`;
CREATE TABLE  `supernarede`.`downloadable_link_purchased` (
  `purchased_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(10) unsigned DEFAULT '0',
  `order_increment_id` varchar(50) NOT NULL DEFAULT '',
  `order_item_id` int(10) unsigned NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `customer_id` int(10) unsigned NOT NULL DEFAULT '0',
  `product_name` varchar(255) NOT NULL DEFAULT '',
  `product_sku` varchar(255) NOT NULL DEFAULT '',
  `link_section_title` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`purchased_id`),
  KEY `DOWNLOADABLE_ORDER_ID` (`order_id`),
  KEY `DOWNLOADABLE_CUSTOMER_ID` (`customer_id`),
  KEY `KEY_DOWNLOADABLE_ORDER_ITEM_ID` (`order_item_id`),
  CONSTRAINT `FK_DOWNLOADABLE_LINK_ORDER_ID` FOREIGN KEY (`order_id`) REFERENCES `sales_flat_order` (`entity_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`downloadable_link_purchased`
--

/*!40000 ALTER TABLE `downloadable_link_purchased` DISABLE KEYS */;
LOCK TABLES `downloadable_link_purchased` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `downloadable_link_purchased` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`downloadable_link_purchased_item`
--

DROP TABLE IF EXISTS `supernarede`.`downloadable_link_purchased_item`;
CREATE TABLE  `supernarede`.`downloadable_link_purchased_item` (
  `item_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `purchased_id` int(10) unsigned NOT NULL DEFAULT '0',
  `order_item_id` int(10) unsigned DEFAULT '0',
  `product_id` int(10) unsigned DEFAULT '0',
  `link_hash` varchar(255) NOT NULL DEFAULT '',
  `number_of_downloads_bought` int(10) unsigned NOT NULL DEFAULT '0',
  `number_of_downloads_used` int(10) unsigned NOT NULL DEFAULT '0',
  `link_id` int(20) unsigned NOT NULL DEFAULT '0',
  `link_title` varchar(255) NOT NULL DEFAULT '',
  `is_shareable` smallint(1) unsigned NOT NULL DEFAULT '0',
  `link_url` varchar(255) NOT NULL DEFAULT '',
  `link_file` varchar(255) NOT NULL DEFAULT '',
  `link_type` varchar(255) NOT NULL DEFAULT '',
  `status` varchar(50) NOT NULL DEFAULT '',
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`item_id`),
  KEY `DOWNLOADABLE_LINK_PURCHASED_ID` (`purchased_id`),
  KEY `DOWNLOADABLE_ORDER_ITEM_ID` (`order_item_id`),
  KEY `DOWNLOADALBE_LINK_HASH` (`link_hash`),
  CONSTRAINT `FK_DOWNLOADABLE_LINK_ORDER_ITEM_ID` FOREIGN KEY (`order_item_id`) REFERENCES `sales_flat_order_item` (`item_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_DOWNLOADABLE_LINK_PURCHASED_ID` FOREIGN KEY (`purchased_id`) REFERENCES `downloadable_link_purchased` (`purchased_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`downloadable_link_purchased_item`
--

/*!40000 ALTER TABLE `downloadable_link_purchased_item` DISABLE KEYS */;
LOCK TABLES `downloadable_link_purchased_item` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `downloadable_link_purchased_item` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`downloadable_link_title`
--

DROP TABLE IF EXISTS `supernarede`.`downloadable_link_title`;
CREATE TABLE  `supernarede`.`downloadable_link_title` (
  `title_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `link_id` int(10) unsigned NOT NULL DEFAULT '0',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`title_id`),
  UNIQUE KEY `UNQ_LINK_TITLE_STORE` (`link_id`,`store_id`),
  KEY `DOWNLOADABLE_LINK_TITLE_LINK` (`link_id`),
  KEY `DOWNLOADABLE_LINK_TITLE_STORE` (`store_id`),
  CONSTRAINT `FK_DOWNLOADABLE_LINK_TITLE_LINK` FOREIGN KEY (`link_id`) REFERENCES `downloadable_link` (`link_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_DOWNLOADABLE_LINK_TITLE_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`downloadable_link_title`
--

/*!40000 ALTER TABLE `downloadable_link_title` DISABLE KEYS */;
LOCK TABLES `downloadable_link_title` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `downloadable_link_title` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`downloadable_sample`
--

DROP TABLE IF EXISTS `supernarede`.`downloadable_sample`;
CREATE TABLE  `supernarede`.`downloadable_sample` (
  `sample_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(10) unsigned NOT NULL DEFAULT '0',
  `sample_url` varchar(255) NOT NULL DEFAULT '',
  `sample_file` varchar(255) NOT NULL DEFAULT '',
  `sample_type` varchar(20) NOT NULL DEFAULT '',
  `sort_order` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`sample_id`),
  KEY `DOWNLODABLE_SAMPLE_PRODUCT` (`product_id`),
  CONSTRAINT `FK_DOWNLODABLE_SAMPLE_PRODUCT` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`downloadable_sample`
--

/*!40000 ALTER TABLE `downloadable_sample` DISABLE KEYS */;
LOCK TABLES `downloadable_sample` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `downloadable_sample` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`downloadable_sample_title`
--

DROP TABLE IF EXISTS `supernarede`.`downloadable_sample_title`;
CREATE TABLE  `supernarede`.`downloadable_sample_title` (
  `title_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sample_id` int(10) unsigned NOT NULL DEFAULT '0',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`title_id`),
  UNIQUE KEY `UNQ_SAMPLE_TITLE_STORE` (`sample_id`,`store_id`),
  KEY `DOWNLOADABLE_SAMPLE_TITLE_SAMPLE` (`sample_id`),
  KEY `DOWNLOADABLE_SAMPLE_TITLE_STORE` (`store_id`),
  CONSTRAINT `FK_DOWNLOADABLE_SAMPLE_TITLE_SAMPLE` FOREIGN KEY (`sample_id`) REFERENCES `downloadable_sample` (`sample_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_DOWNLOADABLE_SAMPLE_TITLE_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`downloadable_sample_title`
--

/*!40000 ALTER TABLE `downloadable_sample_title` DISABLE KEYS */;
LOCK TABLES `downloadable_sample_title` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `downloadable_sample_title` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`eav_attribute`
--

DROP TABLE IF EXISTS `supernarede`.`eav_attribute`;
CREATE TABLE  `supernarede`.`eav_attribute` (
  `attribute_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `attribute_code` varchar(255) NOT NULL DEFAULT '',
  `attribute_model` varchar(255) DEFAULT NULL,
  `backend_model` varchar(255) DEFAULT NULL,
  `backend_type` enum('static','datetime','decimal','int','text','varchar') NOT NULL DEFAULT 'static',
  `backend_table` varchar(255) DEFAULT NULL,
  `frontend_model` varchar(255) DEFAULT NULL,
  `frontend_input` varchar(50) DEFAULT NULL,
  `frontend_label` varchar(255) DEFAULT NULL,
  `frontend_class` varchar(255) DEFAULT NULL,
  `source_model` varchar(255) DEFAULT NULL,
  `is_required` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_user_defined` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `default_value` text,
  `is_unique` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `note` varchar(255) NOT NULL,
  PRIMARY KEY (`attribute_id`),
  UNIQUE KEY `entity_type_id` (`entity_type_id`,`attribute_code`),
  KEY `IDX_USED_FOR_SORT_BY` (`entity_type_id`),
  KEY `IDX_USED_IN_PRODUCT_LISTING` (`entity_type_id`),
  CONSTRAINT `FK_eav_attribute` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=130 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`eav_attribute`
--

/*!40000 ALTER TABLE `eav_attribute` DISABLE KEYS */;
LOCK TABLES `eav_attribute` WRITE;
INSERT INTO `supernarede`.`eav_attribute` VALUES  (1,1,'website_id',NULL,'customer/customer_attribute_backend_website','static','','','select','Associar ao Website','','customer/customer_attribute_source_website',1,0,'',0,''),
 (2,1,'store_id',NULL,'customer/customer_attribute_backend_store','static','','','select','Criado Em','','customer/customer_attribute_source_store',1,0,'',0,''),
 (3,1,'created_in',NULL,'','varchar','','','text','Criado na Loja','','',0,0,'',0,''),
 (4,1,'prefix',NULL,'','varchar','','','text','Prefixo','','',0,0,'',0,''),
 (5,1,'firstname',NULL,'','varchar','','','text','Nome','','',1,0,'',0,''),
 (6,1,'middlename',NULL,'','varchar','','','text','Assinatura','','',0,0,'',0,''),
 (7,1,'lastname',NULL,'','varchar','','','text','Sobrenome','','',1,0,'',0,''),
 (8,1,'suffix',NULL,'','varchar','','','text','Sufixo','','',0,0,'',0,''),
 (9,1,'email',NULL,'','static','','','text','E-Mail','','',1,0,'',0,''),
 (10,1,'group_id',NULL,'','static','','','select','Grupo','','customer/customer_attribute_source_group',1,0,'',0,''),
 (11,1,'dob',NULL,'eav/entity_attribute_backend_datetime','datetime','','eav/entity_attribute_frontend_datetime','date','Data de Nascimento','','',1,0,'',0,''),
 (12,1,'password_hash',NULL,'customer/customer_attribute_backend_password','varchar','','','hidden','','','',0,0,'',0,''),
 (13,1,'default_billing',NULL,'customer/customer_attribute_backend_billing','int','','','text','Endereço de Cobrança Padrão','','',0,0,'',0,''),
 (14,1,'default_shipping',NULL,'customer/customer_attribute_backend_shipping','int','','','text','Endereço de Entrega Padrão','','',0,0,'',0,''),
 (15,1,'taxvat',NULL,'','varchar','','','text','CPF/CNPJ','','',1,0,'',0,''),
 (16,1,'confirmation',NULL,'','varchar','','','text','Confirmado','','',0,0,'',0,''),
 (17,1,'created_at',NULL,'','static','','','date','Criado Na','','',0,0,'',0,''),
 (18,2,'prefix',NULL,'','varchar','','','text','Prefixo','','',0,0,'',0,''),
 (19,2,'firstname',NULL,'','varchar','','','text','Nome','','',1,0,'',0,''),
 (20,2,'middlename',NULL,'','varchar','','','text','Assinatura','','',0,0,'',0,''),
 (21,2,'lastname',NULL,'','varchar','','','text','Sobrenome','','',1,0,'',0,''),
 (22,2,'suffix',NULL,'','varchar','','','text','Sufixo','','',0,0,'',0,''),
 (23,2,'company',NULL,'','varchar','','','text','Profissão','','',0,0,'',0,''),
 (24,2,'street',NULL,'customer_entity/address_attribute_backend_street','text','','','multiline','Endereço','','',1,0,'',0,''),
 (25,2,'city',NULL,'','varchar','','','text','Cidade','','',1,0,'',0,''),
 (26,2,'country_id',NULL,'','varchar','','','select','País','','customer_entity/address_attribute_source_country',1,0,'',0,''),
 (27,2,'region',NULL,'customer_entity/address_attribute_backend_region','varchar','','','text','Estado','','',0,0,'',0,''),
 (28,2,'region_id',NULL,'','int','','','hidden','Estado','','customer_entity/address_attribute_source_region',0,0,'',0,''),
 (29,2,'postcode',NULL,'','varchar','','','text','CEP','','',1,0,'',0,''),
 (30,2,'telephone',NULL,'','varchar','','','text','Telefone','','',1,0,'',0,''),
 (31,2,'fax',NULL,'','varchar','','','text','Fax','','',0,0,'',0,''),
 (32,1,'gender',NULL,'','int','','','select','Sexo','','eav/entity_attribute_source_table',1,0,'',0,''),
 (33,3,'name',NULL,'','varchar','','','text','Nome','','',1,0,'',0,''),
 (34,3,'is_active',NULL,'','int','','','select','Ativar','','eav/entity_attribute_source_boolean',1,0,'',0,''),
 (35,3,'url_key',NULL,'catalog/category_attribute_backend_urlkey','varchar','','','text','Nome na URL','','',0,0,'',0,''),
 (36,3,'description',NULL,'','text','','','textarea','Descrição','','',0,0,'',0,''),
 (37,3,'image',NULL,'catalog/category_attribute_backend_image','varchar','','','image','Imagem','','',0,0,'',0,''),
 (38,3,'meta_title',NULL,'','varchar','','','text','Título da Página','','',0,0,'',0,''),
 (39,3,'meta_keywords',NULL,'','text','','','textarea','Palavras-Chave','','',0,0,'',0,''),
 (40,3,'meta_description',NULL,'','text','','','textarea','Descrição da Página','','',0,0,'',0,''),
 (41,3,'display_mode',NULL,'','varchar','','','select','Modo de Exibição','','catalog/category_attribute_source_mode',0,0,'',0,''),
 (42,3,'landing_page',NULL,'','int','','','select','Bloco CMS','','catalog/category_attribute_source_page',0,0,'',0,''),
 (43,3,'is_anchor',NULL,'','int','','','select','Navegável','','eav/entity_attribute_source_boolean',0,0,'',0,''),
 (44,3,'path',NULL,'','static','','','','Caminho','','',0,0,'',0,''),
 (45,3,'position',NULL,'','static','','','','Situação','','',0,0,'',0,''),
 (46,3,'all_children',NULL,'','text','','','','','','',0,0,'',0,''),
 (47,3,'path_in_store',NULL,'','text','','','','','','',0,0,'',0,''),
 (48,3,'children',NULL,'','text','','','','','','',0,0,'',0,''),
 (49,3,'url_path',NULL,'','varchar','','','','','','',0,0,'',0,''),
 (50,3,'custom_design',NULL,'','varchar','','','select','Leiaute Personalizado','','core/design_source_design',0,0,'',0,''),
 (52,3,'custom_design_from',NULL,'eav/entity_attribute_backend_datetime','datetime','','','date','Ativar Em','','',0,0,'',0,''),
 (53,3,'custom_design_to',NULL,'eav/entity_attribute_backend_datetime','datetime','','','date','Ativar Até','','',0,0,'',0,''),
 (54,3,'page_layout',NULL,'','varchar','','','select','Formato da Página','','catalog/category_attribute_source_layout',0,0,'',0,''),
 (55,3,'custom_layout_update',NULL,'catalog/attribute_backend_customlayoutupdate','text','','','textarea','Atualização de Leiaute','','',0,0,'',0,''),
 (56,3,'level',NULL,'','static','','','','Nível','','',0,0,'',0,''),
 (57,3,'children_count',NULL,'','static','','','','Contagem','','',0,0,'',0,''),
 (58,3,'available_sort_by',NULL,'catalog/category_attribute_backend_sortby','text','','','multiselect','Ordem de Listagem dos Produtos Disponíveis','','catalog/category_attribute_source_sortby',1,0,'',0,''),
 (59,3,'default_sort_by',NULL,'catalog/category_attribute_backend_sortby','varchar','','','select','Ordem de Listagem Padrão dos Produtos','','catalog/category_attribute_source_sortby',1,0,'',0,''),
 (60,4,'name',NULL,'','varchar','','','text','Nome','','',1,0,'',0,''),
 (61,4,'description',NULL,'','text','','','textarea','Descrição','','',0,0,'',0,''),
 (62,4,'short_description',NULL,'','text','','','textarea','Descrição Resumida','','',0,0,'',0,''),
 (63,4,'sku',NULL,'catalog/product_attribute_backend_sku','static','','','text','Código Identificador (SKU)','','',1,0,'',1,''),
 (64,4,'price',NULL,'catalog/product_attribute_backend_price','decimal','','','price','Preço','','',1,0,'0',0,''),
 (65,4,'special_price',NULL,'catalog/product_attribute_backend_price','decimal','','','price','Preço Promocional','','',0,0,'',0,''),
 (66,4,'special_from_date',NULL,'catalog/product_attribute_backend_startdate','datetime','','','date','Preço Promocional em','','',0,0,'',0,''),
 (67,4,'special_to_date',NULL,'eav/entity_attribute_backend_datetime','datetime','','','date','Preço Promocional até','','',0,0,'',0,''),
 (68,4,'cost',NULL,'catalog/product_attribute_backend_price','decimal','','','price','Custo','','',0,1,'',0,''),
 (69,4,'weight',NULL,'','decimal','','','text','Peso','','',1,0,'',0,''),
 (70,4,'manufacturer',NULL,'','int','','','select','Fabricante','','',0,1,'',0,''),
 (71,4,'meta_title',NULL,'','varchar','','','text','Título da Página','','',0,0,'',0,''),
 (72,4,'meta_keyword',NULL,'','text','','','textarea','Palavras-Chave','','',0,0,'',0,''),
 (73,4,'meta_description',NULL,'','varchar','','','textarea','Descrição da Página','validate-length maximum-length-255','',0,0,'',0,'Maximum 255 chars'),
 (74,4,'image',NULL,'','varchar','','catalog/product_attribute_frontend_image','media_image','Imagem Base','','',0,0,'',0,''),
 (75,4,'small_image',NULL,'','varchar','','catalog/product_attribute_frontend_image','media_image','Imagem Reduzida','','',0,0,'',0,''),
 (76,4,'thumbnail',NULL,'','varchar','','catalog/product_attribute_frontend_image','media_image','Miniatura','','',0,0,'',0,''),
 (77,4,'media_gallery',NULL,'catalog/product_attribute_backend_media','varchar','','','gallery','Galeria de Mídia','','',0,0,'',0,''),
 (78,4,'old_id',NULL,'','int','','','','','','',0,0,'',0,''),
 (79,4,'tier_price',NULL,'catalog/product_attribute_backend_tierprice','decimal','','','text','Faixa de Preços','','',0,0,'',0,''),
 (80,4,'color',NULL,'','int','','','select','Cor','','',0,1,'',0,''),
 (81,4,'news_from_date',NULL,'eav/entity_attribute_backend_datetime','datetime','','','date','Novo Produto em','','',0,0,'',0,''),
 (82,4,'news_to_date',NULL,'eav/entity_attribute_backend_datetime','datetime','','','date','Novo Produto até','','',0,0,'',0,''),
 (83,4,'gallery',NULL,'','varchar','','','gallery','Galeria de Imagens','','',0,0,'',0,''),
 (84,4,'status',NULL,'','int','','','select','Situação','','catalog/product_status',1,0,'',0,''),
 (85,4,'tax_class_id',NULL,'','int','','','select','Classe de Impostos','','tax/class_source_product',0,0,'',0,''),
 (86,4,'url_key',NULL,'catalog/product_attribute_backend_urlkey','varchar','','','text','Nome na URL','','',0,0,'',0,''),
 (87,4,'url_path',NULL,'','varchar','','','','','','',0,0,'',0,''),
 (88,4,'minimal_price',NULL,'','decimal','','','price','Preço Mínimo','','',0,0,'',0,''),
 (89,4,'is_recurring',NULL,'','int','','','select','Enable Recurring Profile','','eav/entity_attribute_source_boolean',0,0,'',0,'Products with recurring profile participate in catalog as nominal items.'),
 (90,4,'recurring_profile',NULL,'catalog/product_attribute_backend_recurring','text','','','text','Recurring Payment Profile','','',0,0,'',0,''),
 (91,4,'visibility',NULL,'','int','','','select','Visibilidade','','catalog/product_visibility',1,0,'4',0,''),
 (92,4,'custom_design',NULL,'','varchar','','','select','Leiaute Personalizado','','core/design_source_design',0,0,'',0,''),
 (93,4,'custom_design_from',NULL,'eav/entity_attribute_backend_datetime','datetime','','','date','Ativar Em','','',0,0,'',0,''),
 (94,4,'custom_design_to',NULL,'eav/entity_attribute_backend_datetime','datetime','','','date','Ativar Até','','',0,0,'',0,''),
 (95,4,'custom_layout_update',NULL,'catalog/attribute_backend_customlayoutupdate','text','','','textarea','Atualização de Leiaute','','',0,0,'',0,''),
 (96,4,'page_layout',NULL,'','varchar','','','select','Formato da Página','','catalog/product_attribute_source_layout',0,0,'',0,''),
 (97,4,'options_container',NULL,'','varchar','','','select','Exibir opções do produto em','','catalog/entity_product_attribute_design_options_container',0,0,'container2',0,''),
 (98,4,'required_options',NULL,'','static','','','text','','','',0,0,'',0,''),
 (99,4,'has_options',NULL,'','static','','','text','','','',0,0,'',0,''),
 (100,4,'image_label',NULL,'','varchar','','','text','Descrição da Imagem','','',0,0,'',0,''),
 (101,4,'small_image_label',NULL,'','varchar','','','text','Descrição Imagem Reduzida','','',0,0,'',0,''),
 (102,4,'thumbnail_label',NULL,'','varchar','','','text','Descrição Miniatura','','',0,0,'',0,''),
 (103,4,'created_at',NULL,'eav/entity_attribute_backend_time_created','static','','','text','','','',1,0,'',0,''),
 (104,4,'updated_at',NULL,'eav/entity_attribute_backend_time_updated','static','','','text','','','',1,0,'',0,''),
 (105,3,'include_in_menu',NULL,'','int','','','select','Include in Navigation Menu','','eav/entity_attribute_source_boolean',1,0,'1',0,''),
 (106,3,'custom_use_parent_settings',NULL,'','int','','','select','Use Parent Category Settings','','eav/entity_attribute_source_boolean',0,0,'',0,''),
 (107,3,'custom_apply_to_products',NULL,'','int','','','select','Apply To Products','','eav/entity_attribute_source_boolean',0,0,'',0,''),
 (108,3,'filter_price_range',NULL,'','int','','','text','Layered Navigation Price Step','','',0,0,'',0,''),
 (109,4,'enable_googlecheckout',NULL,'','int','','','select','Permitir comprar usando Google Checkout','','eav/entity_attribute_source_boolean',0,0,'1',0,''),
 (110,4,'gift_message_available',NULL,'catalog/product_attribute_backend_boolean','varchar','','','select','Permitir Mensagem de Presente','','eav/entity_attribute_source_boolean',0,0,'',0,''),
 (111,4,'price_type',NULL,'','int','','','','','','',1,0,'',0,''),
 (112,4,'sku_type',NULL,'','int','','','','','','',1,0,'',0,''),
 (113,4,'weight_type',NULL,'','int','','','','','','',1,0,'',0,''),
 (114,4,'price_view',NULL,'','int','','','select','Preço a Vista','','bundle/product_attribute_source_price_view',1,0,'',0,''),
 (115,4,'shipment_type',NULL,'','int','','','','Entrega','','',1,0,'',0,''),
 (116,4,'links_purchased_separately',NULL,'','int','','','','Links podem ser adquiridos separadamente','','',1,0,'',0,''),
 (117,4,'samples_title',NULL,'','varchar','','','','Título Amostra','','',1,0,'',0,''),
 (118,4,'links_title',NULL,'','varchar','','','','Título do Link','','',1,0,'',0,''),
 (119,4,'links_exist',NULL,'','int','','','','','','',0,0,'0',0,''),
 (120,3,'thumbnail',NULL,'catalog/category_attribute_backend_image','varchar','','','image','Thumbnail Image','','',0,0,'',0,''),
 (121,4,'is_imported',NULL,'','int','','','select','In feed','','eav/entity_attribute_source_boolean',0,0,'',0,''),
 (122,4,'cerveja_categoria',NULL,NULL,'int',NULL,NULL,'select','Categoria',NULL,'eav/entity_attribute_source_table',1,1,'',0,''),
 (123,4,'cerveja_tipo',NULL,'eav/entity_attribute_backend_array','varchar',NULL,NULL,'multiselect','Tipo (Cerveja)',NULL,NULL,0,1,'',0,''),
 (124,4,'cerveja_marca',NULL,NULL,'int',NULL,NULL,'select','Marca (Cerveja)',NULL,'eav/entity_attribute_source_table',1,1,'',0,''),
 (125,1,'customer_tipo',NULL,NULL,'int',NULL,NULL,'select','Tipo de Cliente (PF / PJ)','','eav/entity_attribute_source_table',1,1,'14',0,''),
 (126,1,'customer_pj_cnpj',NULL,NULL,'varchar',NULL,NULL,'text','CNPJ','',NULL,0,1,'',1,''),
 (127,1,'customer_pj_razao',NULL,NULL,'varchar',NULL,NULL,'text','Razão','',NULL,0,1,'',0,''),
 (128,1,'customer_pj_ie',NULL,NULL,'varchar',NULL,NULL,'text','Insc. Estadual','',NULL,0,1,'',1,''),
 (129,1,'customer_pf_rg',NULL,NULL,'varchar',NULL,NULL,'text','RG','',NULL,0,1,'',1,'');
UNLOCK TABLES;
/*!40000 ALTER TABLE `eav_attribute` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`eav_attribute_group`
--

DROP TABLE IF EXISTS `supernarede`.`eav_attribute_group`;
CREATE TABLE  `supernarede`.`eav_attribute_group` (
  `attribute_group_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `attribute_set_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `attribute_group_name` varchar(255) NOT NULL DEFAULT '',
  `sort_order` smallint(6) NOT NULL DEFAULT '0',
  `default_id` smallint(5) unsigned DEFAULT '0',
  PRIMARY KEY (`attribute_group_id`),
  UNIQUE KEY `attribute_set_id` (`attribute_set_id`,`attribute_group_name`),
  KEY `attribute_set_id_2` (`attribute_set_id`,`sort_order`),
  CONSTRAINT `FK_eav_attribute_group` FOREIGN KEY (`attribute_set_id`) REFERENCES `eav_attribute_set` (`attribute_set_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`eav_attribute_group`
--

/*!40000 ALTER TABLE `eav_attribute_group` DISABLE KEYS */;
LOCK TABLES `eav_attribute_group` WRITE;
INSERT INTO `supernarede`.`eav_attribute_group` VALUES  (1,1,'General',1,1),
 (2,2,'General',1,1),
 (3,3,'General Information',10,1),
 (4,4,'General',1,1),
 (5,4,'Prices',2,0),
 (6,4,'Meta Information',4,0),
 (7,4,'Images',3,0),
 (8,4,'Outros (Sistema)',6,0),
 (9,4,'Design',5,0),
 (10,3,'Display Settings',20,0),
 (11,3,'Custom Design',30,0),
 (12,5,'General',1,1),
 (13,6,'General',1,1),
 (14,7,'General',1,1),
 (15,8,'General',1,1),
 (16,9,'General',1,1),
 (17,9,'Prices',2,0),
 (18,9,'Images',3,0),
 (19,9,'Meta Information',4,0),
 (20,9,'Design',5,0),
 (21,9,'Outros (Sistema)',6,0),
 (22,10,'General',1,1),
 (23,10,'Prices',2,0),
 (24,10,'Images',3,0),
 (25,10,'Meta Information',4,0),
 (26,10,'Design',5,0),
 (27,10,'Outros (Sistema)',6,0);
UNLOCK TABLES;
/*!40000 ALTER TABLE `eav_attribute_group` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`eav_attribute_label`
--

DROP TABLE IF EXISTS `supernarede`.`eav_attribute_label`;
CREATE TABLE  `supernarede`.`eav_attribute_label` (
  `attribute_label_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `value` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`attribute_label_id`),
  KEY `IDX_ATTRIBUTE_LABEL_ATTRIBUTE` (`attribute_id`),
  KEY `IDX_ATTRIBUTE_LABEL_STORE` (`store_id`),
  KEY `IDX_ATTRIBUTE_LABEL_ATTRIBUTE_STORE` (`attribute_id`,`store_id`),
  CONSTRAINT `FK_ATTRIBUTE_LABEL_ATTRIBUTE` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_ATTRIBUTE_LABEL_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`eav_attribute_label`
--

/*!40000 ALTER TABLE `eav_attribute_label` DISABLE KEYS */;
LOCK TABLES `eav_attribute_label` WRITE;
INSERT INTO `supernarede`.`eav_attribute_label` VALUES  (5,122,1,'Categoria'),
 (6,123,1,'Tipo'),
 (8,124,1,'Marca'),
 (9,125,1,'Tipo de pessoa'),
 (10,127,1,'Razão social'),
 (11,128,1,'Inscrição Estadual');
UNLOCK TABLES;
/*!40000 ALTER TABLE `eav_attribute_label` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`eav_attribute_option`
--

DROP TABLE IF EXISTS `supernarede`.`eav_attribute_option`;
CREATE TABLE  `supernarede`.`eav_attribute_option` (
  `option_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `sort_order` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`option_id`),
  KEY `FK_ATTRIBUTE_OPTION_ATTRIBUTE` (`attribute_id`),
  CONSTRAINT `FK_ATTRIBUTE_OPTION_ATTRIBUTE` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COMMENT='Attributes option (for source model)';

--
-- Dumping data for table `supernarede`.`eav_attribute_option`
--

/*!40000 ALTER TABLE `eav_attribute_option` DISABLE KEYS */;
LOCK TABLES `eav_attribute_option` WRITE;
INSERT INTO `supernarede`.`eav_attribute_option` VALUES  (1,32,0),
 (2,32,1),
 (3,122,0),
 (4,122,0),
 (5,122,0),
 (6,123,0),
 (7,123,0),
 (8,123,0),
 (9,124,0),
 (10,124,0),
 (11,124,0),
 (12,124,0),
 (13,124,0),
 (14,125,0),
 (15,125,0);
UNLOCK TABLES;
/*!40000 ALTER TABLE `eav_attribute_option` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`eav_attribute_option_value`
--

DROP TABLE IF EXISTS `supernarede`.`eav_attribute_option_value`;
CREATE TABLE  `supernarede`.`eav_attribute_option_value` (
  `value_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `option_id` int(10) unsigned NOT NULL DEFAULT '0',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `value` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`value_id`),
  KEY `FK_ATTRIBUTE_OPTION_VALUE_OPTION` (`option_id`),
  KEY `FK_ATTRIBUTE_OPTION_VALUE_STORE` (`store_id`),
  CONSTRAINT `FK_ATTRIBUTE_OPTION_VALUE_OPTION` FOREIGN KEY (`option_id`) REFERENCES `eav_attribute_option` (`option_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_ATTRIBUTE_OPTION_VALUE_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COMMENT='Attribute option values per store';

--
-- Dumping data for table `supernarede`.`eav_attribute_option_value`
--

/*!40000 ALTER TABLE `eav_attribute_option_value` DISABLE KEYS */;
LOCK TABLES `eav_attribute_option_value` WRITE;
INSERT INTO `supernarede`.`eav_attribute_option_value` VALUES  (1,1,0,'Masculino'),
 (2,2,0,'Feminino'),
 (3,3,0,'Pilsen'),
 (4,4,0,'Lager'),
 (5,5,0,'Abadia'),
 (6,6,0,'Escura'),
 (7,7,0,'Sem Alcool'),
 (8,8,0,'Clara'),
 (15,12,0,'Heineken'),
 (16,13,0,'Skol'),
 (17,9,0,'Antarctica'),
 (18,10,0,'Bavaria'),
 (19,11,0,'Brahma'),
 (20,14,0,'Física'),
 (21,15,0,'Jurídica');
UNLOCK TABLES;
/*!40000 ALTER TABLE `eav_attribute_option_value` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`eav_attribute_set`
--

DROP TABLE IF EXISTS `supernarede`.`eav_attribute_set`;
CREATE TABLE  `supernarede`.`eav_attribute_set` (
  `attribute_set_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `attribute_set_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_swedish_ci NOT NULL DEFAULT '',
  `sort_order` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`attribute_set_id`),
  UNIQUE KEY `entity_type_id` (`entity_type_id`,`attribute_set_name`),
  KEY `entity_type_id_2` (`entity_type_id`,`sort_order`),
  CONSTRAINT `FK_eav_attribute_set` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`eav_attribute_set`
--

/*!40000 ALTER TABLE `eav_attribute_set` DISABLE KEYS */;
LOCK TABLES `eav_attribute_set` WRITE;
INSERT INTO `supernarede`.`eav_attribute_set` VALUES  (1,1,'Default',1),
 (2,2,'Default',1),
 (3,3,'Default',1),
 (4,4,'Base',1),
 (5,5,'Default',1),
 (6,6,'Default',1),
 (7,7,'Default',1),
 (8,8,'Default',1),
 (9,4,'Cervejas',0),
 (10,4,'Cesta Pronta',0);
UNLOCK TABLES;
/*!40000 ALTER TABLE `eav_attribute_set` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`eav_entity`
--

DROP TABLE IF EXISTS `supernarede`.`eav_entity`;
CREATE TABLE  `supernarede`.`eav_entity` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entity_type_id` smallint(8) unsigned NOT NULL DEFAULT '0',
  `attribute_set_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `increment_id` varchar(50) NOT NULL DEFAULT '',
  `parent_id` int(11) unsigned NOT NULL DEFAULT '0',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`entity_id`),
  KEY `FK_ENTITY_ENTITY_TYPE` (`entity_type_id`),
  KEY `FK_ENTITY_STORE` (`store_id`),
  CONSTRAINT `FK_eav_entity` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_eav_entity_store` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Entityies';

--
-- Dumping data for table `supernarede`.`eav_entity`
--

/*!40000 ALTER TABLE `eav_entity` DISABLE KEYS */;
LOCK TABLES `eav_entity` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `eav_entity` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`eav_entity_attribute`
--

DROP TABLE IF EXISTS `supernarede`.`eav_entity_attribute`;
CREATE TABLE  `supernarede`.`eav_entity_attribute` (
  `entity_attribute_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `attribute_set_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `attribute_group_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `sort_order` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`entity_attribute_id`),
  UNIQUE KEY `attribute_set_id_2` (`attribute_set_id`,`attribute_id`),
  UNIQUE KEY `attribute_group_id` (`attribute_group_id`,`attribute_id`),
  KEY `attribute_set_id_3` (`attribute_set_id`,`sort_order`),
  KEY `FK_EAV_ENTITY_ATTRIVUTE_ATTRIBUTE` (`attribute_id`),
  CONSTRAINT `FK_EAV_ENTITY_ATTRIBUTE_ATTRIBUTE` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_EAV_ENTITY_ATTRIBUTE_GROUP` FOREIGN KEY (`attribute_group_id`) REFERENCES `eav_attribute_group` (`attribute_group_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=635 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`eav_entity_attribute`
--

/*!40000 ALTER TABLE `eav_entity_attribute` DISABLE KEYS */;
LOCK TABLES `eav_entity_attribute` WRITE;
INSERT INTO `supernarede`.`eav_entity_attribute` VALUES  (1,1,1,1,1,10),
 (2,1,1,1,2,0),
 (3,1,1,1,3,20),
 (4,1,1,1,4,30),
 (5,1,1,1,5,40),
 (6,1,1,1,6,50),
 (7,1,1,1,7,60),
 (8,1,1,1,8,70),
 (9,1,1,1,9,80),
 (10,1,1,1,10,25),
 (11,1,1,1,11,90),
 (12,1,1,1,12,0),
 (13,1,1,1,13,0),
 (14,1,1,1,14,0),
 (15,1,1,1,15,100),
 (16,1,1,1,16,0),
 (17,1,1,1,17,86),
 (18,2,2,2,18,10),
 (19,2,2,2,19,20),
 (20,2,2,2,20,30),
 (21,2,2,2,21,40),
 (22,2,2,2,22,50),
 (23,2,2,2,23,60),
 (24,2,2,2,24,70),
 (25,2,2,2,25,80),
 (26,2,2,2,26,90),
 (27,2,2,2,27,100),
 (28,2,2,2,28,100),
 (29,2,2,2,29,110),
 (30,2,2,2,30,120),
 (31,2,2,2,31,130),
 (32,1,1,1,32,110),
 (33,3,3,3,33,1),
 (34,3,3,3,34,2),
 (35,3,3,3,35,3),
 (36,3,3,3,36,4),
 (37,3,3,3,37,5),
 (38,3,3,3,38,6),
 (39,3,3,3,39,7),
 (40,3,3,3,40,8),
 (41,3,3,10,41,10),
 (42,3,3,10,42,20),
 (43,3,3,10,43,30),
 (44,3,3,3,44,12),
 (45,3,3,3,45,13),
 (46,3,3,3,46,14),
 (47,3,3,3,47,15),
 (48,3,3,3,48,16),
 (49,3,3,3,49,17),
 (50,3,3,11,50,10),
 (51,3,3,11,51,20),
 (52,3,3,11,52,30),
 (53,3,3,11,53,40),
 (54,3,3,11,54,50),
 (55,3,3,11,55,60),
 (56,3,3,3,56,24),
 (57,3,3,3,57,25),
 (58,3,3,10,58,40),
 (59,3,3,10,59,50),
 (77,4,4,4,78,6),
 (85,4,4,4,87,11),
 (86,4,4,5,88,8),
 (96,4,4,4,98,13),
 (97,4,4,4,99,14),
 (98,4,4,4,100,15),
 (99,4,4,4,101,16),
 (100,4,4,4,102,17),
 (101,4,4,4,103,18),
 (102,4,4,4,104,19),
 (103,3,3,3,105,10),
 (104,3,3,11,106,5),
 (105,3,3,11,107,6),
 (106,3,3,10,108,51),
 (109,4,4,4,111,20),
 (110,4,4,4,112,21),
 (111,4,4,4,113,22),
 (113,4,4,4,115,23),
 (114,4,4,4,116,24),
 (115,4,4,4,117,25),
 (116,4,4,4,118,26),
 (117,4,4,4,119,27),
 (118,3,3,3,120,4),
 (193,4,4,4,60,2),
 (195,4,4,4,61,4),
 (197,4,4,4,63,1),
 (199,4,4,4,69,5),
 (201,4,4,4,81,7),
 (203,4,4,4,82,8),
 (205,4,4,4,84,3),
 (207,4,4,4,91,6),
 (209,4,4,5,64,1),
 (211,4,4,5,65,2),
 (213,4,4,5,66,3),
 (215,4,4,5,67,4),
 (217,4,4,5,79,5),
 (219,4,4,5,114,6),
 (221,4,4,7,74,1),
 (223,4,4,7,75,2),
 (225,4,4,7,76,3),
 (227,4,4,7,77,4),
 (229,4,4,7,83,5),
 (231,4,4,6,71,2),
 (233,4,4,6,72,3),
 (235,4,4,6,73,4),
 (237,4,4,6,86,1),
 (239,4,4,9,92,4),
 (241,4,4,9,93,5),
 (243,4,4,9,94,6),
 (245,4,4,9,95,2),
 (247,4,4,9,96,1),
 (249,4,4,9,97,3),
 (251,4,4,8,62,1),
 (253,4,4,8,85,5),
 (255,4,4,8,89,6),
 (257,4,4,8,90,7),
 (259,4,4,8,109,4),
 (261,4,4,8,110,2),
 (263,4,4,8,121,3),
 (275,4,9,16,78,6),
 (283,4,9,16,87,11),
 (285,4,9,16,98,13),
 (287,4,9,16,99,14),
 (289,4,9,16,100,15),
 (291,4,9,16,101,16),
 (293,4,9,16,102,17),
 (295,4,9,16,103,18),
 (297,4,9,16,104,19),
 (299,4,9,16,111,20),
 (301,4,9,16,112,21),
 (303,4,9,16,113,22),
 (305,4,9,16,115,23),
 (307,4,9,16,116,24),
 (309,4,9,16,117,25),
 (311,4,9,16,118,26),
 (313,4,9,16,119,27),
 (327,4,9,17,88,8),
 (373,4,9,16,60,2),
 (375,4,9,16,61,4),
 (377,4,9,16,63,1),
 (379,4,9,16,69,8),
 (381,4,9,16,81,10),
 (383,4,9,16,82,11),
 (385,4,9,16,84,3),
 (387,4,9,16,91,9),
 (389,4,9,16,122,7),
 (391,4,9,16,123,6),
 (393,4,9,16,124,5),
 (395,4,9,17,64,1),
 (397,4,9,17,65,2),
 (399,4,9,17,66,3),
 (401,4,9,17,67,4),
 (403,4,9,17,79,5),
 (405,4,9,17,114,6),
 (407,4,9,18,74,1),
 (409,4,9,18,75,2),
 (411,4,9,18,76,3),
 (413,4,9,18,77,4),
 (415,4,9,18,83,5),
 (417,4,9,19,71,2),
 (419,4,9,19,72,3),
 (421,4,9,19,73,4),
 (423,4,9,19,86,1),
 (425,4,9,20,92,4),
 (427,4,9,20,93,5),
 (429,4,9,20,94,6),
 (431,4,9,20,95,2),
 (433,4,9,20,96,1),
 (435,4,9,20,97,3),
 (437,4,9,21,62,1),
 (439,4,9,21,85,5),
 (441,4,9,21,89,6),
 (443,4,9,21,90,7),
 (445,4,9,21,109,4),
 (447,4,9,21,110,2),
 (449,4,9,21,121,3),
 (461,4,10,22,78,6),
 (469,4,10,22,87,11),
 (471,4,10,22,98,13),
 (473,4,10,22,99,14),
 (475,4,10,22,100,15),
 (477,4,10,22,101,16),
 (479,4,10,22,102,17),
 (481,4,10,22,103,18),
 (483,4,10,22,104,19),
 (485,4,10,22,111,20),
 (487,4,10,22,112,21),
 (489,4,10,22,113,22),
 (491,4,10,22,115,23),
 (493,4,10,22,116,24),
 (495,4,10,22,117,25),
 (497,4,10,22,118,26),
 (499,4,10,22,119,27),
 (513,4,10,23,88,8),
 (559,4,10,22,60,2),
 (561,4,10,22,61,4),
 (563,4,10,22,63,1),
 (565,4,10,22,81,6),
 (567,4,10,22,82,7),
 (569,4,10,22,84,3),
 (571,4,10,22,91,5),
 (573,4,10,23,64,1),
 (575,4,10,23,65,2),
 (577,4,10,23,66,3),
 (579,4,10,23,67,4),
 (581,4,10,23,79,5),
 (583,4,10,23,114,6),
 (585,4,10,24,74,1),
 (587,4,10,24,75,2),
 (589,4,10,24,76,3),
 (591,4,10,24,77,4),
 (593,4,10,24,83,5),
 (595,4,10,25,71,2),
 (597,4,10,25,72,3),
 (599,4,10,25,73,4),
 (601,4,10,25,86,1),
 (603,4,10,26,92,4),
 (605,4,10,26,93,5),
 (607,4,10,26,94,6),
 (609,4,10,26,95,2),
 (611,4,10,26,96,1),
 (613,4,10,26,97,3),
 (615,4,10,27,62,1),
 (617,4,10,27,69,2),
 (619,4,10,27,85,6),
 (621,4,10,27,89,7),
 (623,4,10,27,90,8),
 (625,4,10,27,109,5),
 (627,4,10,27,110,3),
 (629,4,10,27,121,4),
 (630,1,1,1,125,111),
 (631,1,1,1,126,112),
 (632,1,1,1,127,113),
 (633,1,1,1,128,114),
 (634,1,1,1,129,115);
UNLOCK TABLES;
/*!40000 ALTER TABLE `eav_entity_attribute` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`eav_entity_datetime`
--

DROP TABLE IF EXISTS `supernarede`.`eav_entity_datetime`;
CREATE TABLE  `supernarede`.`eav_entity_datetime` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT,
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0',
  `value` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `UNQ_ATTRIBUTE_VALUE` (`entity_id`,`attribute_id`,`store_id`),
  KEY `FK_ATTRIBUTE_DATETIME_ENTITY_TYPE` (`entity_type_id`),
  KEY `FK_ATTRIBUTE_DATETIME_ATTRIBUTE` (`attribute_id`),
  KEY `FK_ATTRIBUTE_DATETIME_STORE` (`store_id`),
  KEY `FK_ATTRIBUTE_DATETIME_ENTITY` (`entity_id`),
  KEY `value_by_attribute` (`attribute_id`,`value`),
  KEY `value_by_entity_type` (`entity_type_id`,`value`),
  CONSTRAINT `FK_EAV_ENTITY_DATETIME_ENTITY` FOREIGN KEY (`entity_id`) REFERENCES `eav_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_EAV_ENTITY_DATETIME_ENTITY_TYPE` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_EAV_ENTITY_DATETIME_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Datetime values of attributes';

--
-- Dumping data for table `supernarede`.`eav_entity_datetime`
--

/*!40000 ALTER TABLE `eav_entity_datetime` DISABLE KEYS */;
LOCK TABLES `eav_entity_datetime` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `eav_entity_datetime` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`eav_entity_decimal`
--

DROP TABLE IF EXISTS `supernarede`.`eav_entity_decimal`;
CREATE TABLE  `supernarede`.`eav_entity_decimal` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT,
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0',
  `value` decimal(12,4) NOT NULL DEFAULT '0.0000',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `UNQ_ATTRIBUTE_VALUE` (`entity_id`,`attribute_id`,`store_id`),
  KEY `FK_ATTRIBUTE_DECIMAL_ENTITY_TYPE` (`entity_type_id`),
  KEY `FK_ATTRIBUTE_DECIMAL_ATTRIBUTE` (`attribute_id`),
  KEY `FK_ATTRIBUTE_DECIMAL_STORE` (`store_id`),
  KEY `FK_ATTRIBUTE_DECIMAL_ENTITY` (`entity_id`),
  KEY `value_by_attribute` (`attribute_id`,`value`),
  KEY `value_by_entity_type` (`entity_type_id`,`value`),
  CONSTRAINT `FK_EAV_ENTITY_DECIMAL_ENTITY` FOREIGN KEY (`entity_id`) REFERENCES `eav_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_EAV_ENTITY_DECIMAL_ENTITY_TYPE` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_EAV_ENTITY_DECIMAL_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Decimal values of attributes';

--
-- Dumping data for table `supernarede`.`eav_entity_decimal`
--

/*!40000 ALTER TABLE `eav_entity_decimal` DISABLE KEYS */;
LOCK TABLES `eav_entity_decimal` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `eav_entity_decimal` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`eav_entity_int`
--

DROP TABLE IF EXISTS `supernarede`.`eav_entity_int`;
CREATE TABLE  `supernarede`.`eav_entity_int` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT,
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0',
  `value` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `UNQ_ATTRIBUTE_VALUE` (`entity_id`,`attribute_id`,`store_id`),
  KEY `FK_ATTRIBUTE_INT_ENTITY_TYPE` (`entity_type_id`),
  KEY `FK_ATTRIBUTE_INT_ATTRIBUTE` (`attribute_id`),
  KEY `FK_ATTRIBUTE_INT_STORE` (`store_id`),
  KEY `FK_ATTRIBUTE_INT_ENTITY` (`entity_id`),
  KEY `value_by_attribute` (`attribute_id`,`value`),
  KEY `value_by_entity_type` (`entity_type_id`,`value`),
  CONSTRAINT `FK_EAV_ENTITY_INT_ENTITY` FOREIGN KEY (`entity_id`) REFERENCES `eav_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_EAV_ENTITY_INT_ENTITY_TYPE` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_EAV_ENTITY_INT_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Integer values of attributes';

--
-- Dumping data for table `supernarede`.`eav_entity_int`
--

/*!40000 ALTER TABLE `eav_entity_int` DISABLE KEYS */;
LOCK TABLES `eav_entity_int` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `eav_entity_int` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`eav_entity_store`
--

DROP TABLE IF EXISTS `supernarede`.`eav_entity_store`;
CREATE TABLE  `supernarede`.`eav_entity_store` (
  `entity_store_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `increment_prefix` varchar(20) NOT NULL DEFAULT '',
  `increment_last_id` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`entity_store_id`),
  KEY `FK_eav_entity_store_entity_type` (`entity_type_id`),
  KEY `FK_eav_entity_store_store` (`store_id`),
  CONSTRAINT `FK_eav_entity_store_entity_type` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_eav_entity_store_store` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`eav_entity_store`
--

/*!40000 ALTER TABLE `eav_entity_store` DISABLE KEYS */;
LOCK TABLES `eav_entity_store` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `eav_entity_store` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`eav_entity_text`
--

DROP TABLE IF EXISTS `supernarede`.`eav_entity_text`;
CREATE TABLE  `supernarede`.`eav_entity_text` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT,
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0',
  `value` text NOT NULL,
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `UNQ_ATTRIBUTE_VALUE` (`entity_id`,`attribute_id`,`store_id`),
  KEY `FK_ATTRIBUTE_TEXT_ENTITY_TYPE` (`entity_type_id`),
  KEY `FK_ATTRIBUTE_TEXT_ATTRIBUTE` (`attribute_id`),
  KEY `FK_ATTRIBUTE_TEXT_STORE` (`store_id`),
  KEY `FK_ATTRIBUTE_TEXT_ENTITY` (`entity_id`),
  CONSTRAINT `FK_EAV_ENTITY_TEXT_ENTITY` FOREIGN KEY (`entity_id`) REFERENCES `eav_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_EAV_ENTITY_TEXT_ENTITY_TYPE` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_EAV_ENTITY_TEXT_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Text values of attributes';

--
-- Dumping data for table `supernarede`.`eav_entity_text`
--

/*!40000 ALTER TABLE `eav_entity_text` DISABLE KEYS */;
LOCK TABLES `eav_entity_text` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `eav_entity_text` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`eav_entity_type`
--

DROP TABLE IF EXISTS `supernarede`.`eav_entity_type`;
CREATE TABLE  `supernarede`.`eav_entity_type` (
  `entity_type_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `entity_type_code` varchar(50) NOT NULL DEFAULT '',
  `entity_model` varchar(255) NOT NULL,
  `attribute_model` varchar(255) NOT NULL,
  `entity_table` varchar(255) NOT NULL DEFAULT '',
  `value_table_prefix` varchar(255) NOT NULL DEFAULT '',
  `entity_id_field` varchar(255) NOT NULL DEFAULT '',
  `is_data_sharing` tinyint(4) unsigned NOT NULL DEFAULT '1',
  `data_sharing_key` varchar(100) DEFAULT 'default',
  `default_attribute_set_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `increment_model` varchar(255) NOT NULL DEFAULT '',
  `increment_per_store` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `increment_pad_length` tinyint(8) unsigned NOT NULL DEFAULT '8',
  `increment_pad_char` char(1) NOT NULL DEFAULT '0',
  `additional_attribute_table` varchar(255) NOT NULL DEFAULT '',
  `entity_attribute_collection` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`entity_type_id`),
  KEY `entity_name` (`entity_type_code`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`eav_entity_type`
--

/*!40000 ALTER TABLE `eav_entity_type` DISABLE KEYS */;
LOCK TABLES `eav_entity_type` WRITE;
INSERT INTO `supernarede`.`eav_entity_type` VALUES  (1,'customer','customer/customer','customer/attribute','customer/entity','','',1,'default',1,'eav/entity_increment_numeric',0,8,'0','customer/eav_attribute','customer/attribute_collection'),
 (2,'customer_address','customer/address','customer/attribute','customer/address_entity','','',1,'default',2,'',0,8,'0','customer/eav_attribute','customer/address_attribute_collection'),
 (3,'catalog_category','catalog/category','catalog/resource_eav_attribute','catalog/category','','',1,'default',3,'',0,8,'0','catalog/eav_attribute','catalog/category_attribute_collection'),
 (4,'catalog_product','catalog/product','catalog/resource_eav_attribute','catalog/product','','',1,'default',4,'',0,8,'0','catalog/eav_attribute','catalog/product_attribute_collection'),
 (5,'order','sales/order','','sales/order','','',1,'default',0,'eav/entity_increment_numeric',1,8,'0','',''),
 (6,'invoice','sales/order_invoice','','sales/invoice','','',1,'default',0,'eav/entity_increment_numeric',1,8,'0','',''),
 (7,'creditmemo','sales/order_creditmemo','','sales/creditmemo','','',1,'default',0,'eav/entity_increment_numeric',1,8,'0','',''),
 (8,'shipment','sales/order_shipment','','sales/shipment','','',1,'default',0,'eav/entity_increment_numeric',1,8,'0','','');
UNLOCK TABLES;
/*!40000 ALTER TABLE `eav_entity_type` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`eav_entity_varchar`
--

DROP TABLE IF EXISTS `supernarede`.`eav_entity_varchar`;
CREATE TABLE  `supernarede`.`eav_entity_varchar` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT,
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0',
  `value` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `UNQ_ATTRIBUTE_VALUE` (`entity_id`,`attribute_id`,`store_id`),
  KEY `FK_ATTRIBUTE_VARCHAR_ENTITY_TYPE` (`entity_type_id`),
  KEY `FK_ATTRIBUTE_VARCHAR_ATTRIBUTE` (`attribute_id`),
  KEY `FK_ATTRIBUTE_VARCHAR_STORE` (`store_id`),
  KEY `FK_ATTRIBUTE_VARCHAR_ENTITY` (`entity_id`),
  KEY `value_by_attribute` (`attribute_id`,`value`),
  KEY `value_by_entity_type` (`entity_type_id`,`value`),
  CONSTRAINT `FK_EAV_ENTITY_VARCHAR_ENTITY` FOREIGN KEY (`entity_id`) REFERENCES `eav_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_EAV_ENTITY_VARCHAR_ENTITY_TYPE` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_EAV_ENTITY_VARCHAR_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Varchar values of attributes';

--
-- Dumping data for table `supernarede`.`eav_entity_varchar`
--

/*!40000 ALTER TABLE `eav_entity_varchar` DISABLE KEYS */;
LOCK TABLES `eav_entity_varchar` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `eav_entity_varchar` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`eav_form_element`
--

DROP TABLE IF EXISTS `supernarede`.`eav_form_element`;
CREATE TABLE  `supernarede`.`eav_form_element` (
  `element_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type_id` smallint(5) unsigned NOT NULL,
  `fieldset_id` smallint(5) unsigned DEFAULT NULL,
  `attribute_id` smallint(5) unsigned NOT NULL,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`element_id`),
  UNIQUE KEY `UNQ_FORM_ATTRIBUTE` (`type_id`,`attribute_id`),
  KEY `IDX_FORM_TYPE` (`type_id`),
  KEY `IDX_FORM_FIELDSET` (`fieldset_id`),
  KEY `IDX_FORM_ATTRIBUTE` (`attribute_id`),
  CONSTRAINT `FK_EAV_FORM_ELEMENT_ATTRIBUTE` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_EAV_FORM_ELEMENT_FORM_FIELDSET` FOREIGN KEY (`fieldset_id`) REFERENCES `eav_form_fieldset` (`fieldset_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_EAV_FORM_ELEMENT_FORM_TYPE` FOREIGN KEY (`type_id`) REFERENCES `eav_form_type` (`type_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`eav_form_element`
--

/*!40000 ALTER TABLE `eav_form_element` DISABLE KEYS */;
LOCK TABLES `eav_form_element` WRITE;
INSERT INTO `supernarede`.`eav_form_element` VALUES  (1,1,1,5,0),
 (2,1,1,7,1),
 (3,1,1,9,2),
 (4,2,2,5,0),
 (5,2,2,7,1),
 (6,2,2,9,2),
 (7,3,3,19,0),
 (8,3,3,21,1),
 (9,3,3,23,2),
 (10,3,3,30,3),
 (11,3,3,31,4),
 (12,3,4,24,0),
 (13,3,4,25,1),
 (14,3,4,27,2),
 (15,3,4,29,3),
 (16,3,4,26,4),
 (17,4,NULL,19,0),
 (18,4,NULL,21,1),
 (19,4,NULL,23,2),
 (20,4,NULL,9,3),
 (21,4,NULL,24,4),
 (22,4,NULL,25,5),
 (23,4,NULL,27,6),
 (24,4,NULL,29,7),
 (25,4,NULL,26,8),
 (26,4,NULL,30,9),
 (27,4,NULL,31,10),
 (28,5,NULL,19,0),
 (29,5,NULL,21,1),
 (30,5,NULL,23,2),
 (31,5,NULL,9,3),
 (32,5,NULL,24,4),
 (33,5,NULL,25,5),
 (34,5,NULL,27,6),
 (35,5,NULL,29,7),
 (36,5,NULL,26,8),
 (37,5,NULL,30,9),
 (38,5,NULL,31,10),
 (39,6,NULL,19,0),
 (40,6,NULL,21,1),
 (41,6,NULL,23,2),
 (42,6,NULL,24,3),
 (43,6,NULL,25,4),
 (44,6,NULL,27,5),
 (45,6,NULL,29,6),
 (46,6,NULL,26,7),
 (47,6,NULL,30,8),
 (48,6,NULL,31,9),
 (49,7,NULL,19,0),
 (50,7,NULL,21,1),
 (51,7,NULL,23,2),
 (52,7,NULL,24,3),
 (53,7,NULL,25,4),
 (54,7,NULL,27,5),
 (55,7,NULL,29,6),
 (56,7,NULL,26,7),
 (57,7,NULL,30,8),
 (58,7,NULL,31,9),
 (59,8,5,5,0),
 (60,8,5,7,1),
 (61,8,5,9,2),
 (62,8,6,23,0),
 (63,8,6,30,1),
 (64,8,6,24,2),
 (65,8,6,25,3),
 (66,8,6,27,4),
 (67,8,6,29,5),
 (68,8,6,26,6);
UNLOCK TABLES;
/*!40000 ALTER TABLE `eav_form_element` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`eav_form_fieldset`
--

DROP TABLE IF EXISTS `supernarede`.`eav_form_fieldset`;
CREATE TABLE  `supernarede`.`eav_form_fieldset` (
  `fieldset_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `type_id` smallint(5) unsigned NOT NULL,
  `code` char(64) NOT NULL,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`fieldset_id`),
  UNIQUE KEY `UNQ_FORM_FIELDSET_CODE` (`type_id`,`code`),
  KEY `IDX_FORM_TYPE` (`type_id`),
  CONSTRAINT `FK_EAV_FORM_FIELDSET_FORM_TYPE` FOREIGN KEY (`type_id`) REFERENCES `eav_form_type` (`type_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`eav_form_fieldset`
--

/*!40000 ALTER TABLE `eav_form_fieldset` DISABLE KEYS */;
LOCK TABLES `eav_form_fieldset` WRITE;
INSERT INTO `supernarede`.`eav_form_fieldset` VALUES  (1,1,'general',1),
 (2,2,'general',1),
 (3,3,'contact',1),
 (4,3,'address',2),
 (5,8,'general',1),
 (6,8,'address',2);
UNLOCK TABLES;
/*!40000 ALTER TABLE `eav_form_fieldset` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`eav_form_fieldset_label`
--

DROP TABLE IF EXISTS `supernarede`.`eav_form_fieldset_label`;
CREATE TABLE  `supernarede`.`eav_form_fieldset_label` (
  `fieldset_id` smallint(5) unsigned NOT NULL,
  `store_id` smallint(5) unsigned NOT NULL,
  `label` varchar(255) NOT NULL,
  PRIMARY KEY (`fieldset_id`,`store_id`),
  KEY `IDX_FORM_FIELDSET` (`fieldset_id`),
  KEY `IDX_STORE` (`store_id`),
  CONSTRAINT `FK_EAV_FORM_FIELDSET_LABEL_FORM_FIELDSET` FOREIGN KEY (`fieldset_id`) REFERENCES `eav_form_fieldset` (`fieldset_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_EAV_FORM_FIELDSET_LABEL_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`eav_form_fieldset_label`
--

/*!40000 ALTER TABLE `eav_form_fieldset_label` DISABLE KEYS */;
LOCK TABLES `eav_form_fieldset_label` WRITE;
INSERT INTO `supernarede`.`eav_form_fieldset_label` VALUES  (1,0,'Informações Pessoais'),
 (2,0,'Informações de Conta'),
 (3,0,'Informações de Contato'),
 (4,0,'Endereços'),
 (5,0,'Informações Pessoais'),
 (6,0,'Informações de Endereços');
UNLOCK TABLES;
/*!40000 ALTER TABLE `eav_form_fieldset_label` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`eav_form_type`
--

DROP TABLE IF EXISTS `supernarede`.`eav_form_type`;
CREATE TABLE  `supernarede`.`eav_form_type` (
  `type_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `code` char(64) NOT NULL,
  `label` varchar(255) NOT NULL,
  `is_system` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `theme` varchar(64) NOT NULL DEFAULT '',
  `store_id` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`type_id`),
  UNIQUE KEY `UNQ_FORM_TYPE_CODE` (`code`,`theme`,`store_id`),
  KEY `IDX_STORE` (`store_id`),
  CONSTRAINT `FK_EAV_FORM_TYPE_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`eav_form_type`
--

/*!40000 ALTER TABLE `eav_form_type` DISABLE KEYS */;
LOCK TABLES `eav_form_type` WRITE;
INSERT INTO `supernarede`.`eav_form_type` VALUES  (1,'customer_account_create','customer_account_create',1,'',0),
 (2,'customer_account_edit','customer_account_edit',1,'',0),
 (3,'customer_address_edit','customer_address_edit',1,'',0),
 (4,'checkout_onepage_register','checkout_onepage_register',1,'',0),
 (5,'checkout_onepage_register_guest','checkout_onepage_register_guest',1,'',0),
 (6,'checkout_onepage_billing_address','checkout_onepage_billing_address',1,'',0),
 (7,'checkout_onepage_shipping_address','checkout_onepage_shipping_address',1,'',0),
 (8,'checkout_multishipping_register','checkout_multishipping_register',1,'',0);
UNLOCK TABLES;
/*!40000 ALTER TABLE `eav_form_type` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`eav_form_type_entity`
--

DROP TABLE IF EXISTS `supernarede`.`eav_form_type_entity`;
CREATE TABLE  `supernarede`.`eav_form_type_entity` (
  `type_id` smallint(5) unsigned NOT NULL,
  `entity_type_id` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`type_id`,`entity_type_id`),
  KEY `IDX_EAV_ENTITY_TYPE` (`entity_type_id`),
  CONSTRAINT `FK_EAV_FORM_TYPE_ENTITY_ENTITY_TYPE` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_EAV_FORM_TYPE_ENTITY_FORM_TYPE` FOREIGN KEY (`type_id`) REFERENCES `eav_form_type` (`type_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`eav_form_type_entity`
--

/*!40000 ALTER TABLE `eav_form_type_entity` DISABLE KEYS */;
LOCK TABLES `eav_form_type_entity` WRITE;
INSERT INTO `supernarede`.`eav_form_type_entity` VALUES  (1,1),
 (2,1),
 (4,1),
 (5,1),
 (8,1),
 (3,2),
 (4,2),
 (5,2),
 (6,2),
 (7,2),
 (8,2);
UNLOCK TABLES;
/*!40000 ALTER TABLE `eav_form_type_entity` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`find_feed_import_codes`
--

DROP TABLE IF EXISTS `supernarede`.`find_feed_import_codes`;
CREATE TABLE  `supernarede`.`find_feed_import_codes` (
  `code_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `import_code` varchar(255) NOT NULL,
  `eav_code` varchar(255) NOT NULL,
  `is_imported` int(10) unsigned NOT NULL,
  PRIMARY KEY (`code_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`find_feed_import_codes`
--

/*!40000 ALTER TABLE `find_feed_import_codes` DISABLE KEYS */;
LOCK TABLES `find_feed_import_codes` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `find_feed_import_codes` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`gift_message`
--

DROP TABLE IF EXISTS `supernarede`.`gift_message`;
CREATE TABLE  `supernarede`.`gift_message` (
  `gift_message_id` int(7) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(7) unsigned NOT NULL DEFAULT '0',
  `sender` varchar(255) NOT NULL DEFAULT '',
  `recipient` varchar(255) NOT NULL DEFAULT '',
  `message` text NOT NULL,
  PRIMARY KEY (`gift_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`gift_message`
--

/*!40000 ALTER TABLE `gift_message` DISABLE KEYS */;
LOCK TABLES `gift_message` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `gift_message` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`googlebase_attributes`
--

DROP TABLE IF EXISTS `supernarede`.`googlebase_attributes`;
CREATE TABLE  `supernarede`.`googlebase_attributes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `attribute_id` smallint(5) unsigned NOT NULL,
  `gbase_attribute` varchar(255) NOT NULL,
  `type_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `GOOGLEBASE_ATTRIBUTES_ATTRIBUTE_ID` (`attribute_id`),
  KEY `GOOGLEBASE_ATTRIBUTES_TYPE_ID` (`type_id`),
  CONSTRAINT `GOOGLEBASE_ATTRIBUTES_ATTRIBUTE_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  CONSTRAINT `GOOGLEBASE_ATTRIBUTES_TYPE_ID` FOREIGN KEY (`type_id`) REFERENCES `googlebase_types` (`type_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Google Base Attributes link Product Attributes';

--
-- Dumping data for table `supernarede`.`googlebase_attributes`
--

/*!40000 ALTER TABLE `googlebase_attributes` DISABLE KEYS */;
LOCK TABLES `googlebase_attributes` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `googlebase_attributes` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`googlebase_items`
--

DROP TABLE IF EXISTS `supernarede`.`googlebase_items`;
CREATE TABLE  `supernarede`.`googlebase_items` (
  `item_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type_id` int(10) unsigned NOT NULL DEFAULT '0',
  `product_id` int(10) unsigned NOT NULL,
  `gbase_item_id` varchar(255) NOT NULL,
  `store_id` smallint(5) unsigned NOT NULL,
  `published` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `expires` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `impr` smallint(5) unsigned NOT NULL DEFAULT '0',
  `clicks` smallint(5) unsigned NOT NULL DEFAULT '0',
  `views` smallint(5) unsigned NOT NULL DEFAULT '0',
  `is_hidden` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`item_id`),
  KEY `GOOGLEBASE_ITEMS_PRODUCT_ID` (`product_id`),
  KEY `GOOGLEBASE_ITEMS_STORE_ID` (`store_id`),
  CONSTRAINT `GOOGLEBASE_ITEMS_PRODUCT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `GOOGLEBASE_ITEMS_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Google Base Items Products';

--
-- Dumping data for table `supernarede`.`googlebase_items`
--

/*!40000 ALTER TABLE `googlebase_items` DISABLE KEYS */;
LOCK TABLES `googlebase_items` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `googlebase_items` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`googlebase_types`
--

DROP TABLE IF EXISTS `supernarede`.`googlebase_types`;
CREATE TABLE  `supernarede`.`googlebase_types` (
  `type_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `attribute_set_id` smallint(5) unsigned NOT NULL,
  `gbase_itemtype` varchar(255) NOT NULL,
  `target_country` varchar(2) NOT NULL DEFAULT 'US',
  PRIMARY KEY (`type_id`),
  KEY `GOOGLEBASE_TYPES_ATTRIBUTE_SET_ID` (`attribute_set_id`),
  CONSTRAINT `GOOGLEBASE_TYPES_ATTRIBUTE_SET_ID` FOREIGN KEY (`attribute_set_id`) REFERENCES `eav_attribute_set` (`attribute_set_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Google Base Item Types link Attribute Sets';

--
-- Dumping data for table `supernarede`.`googlebase_types`
--

/*!40000 ALTER TABLE `googlebase_types` DISABLE KEYS */;
LOCK TABLES `googlebase_types` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `googlebase_types` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`googlecheckout_api_debug`
--

DROP TABLE IF EXISTS `supernarede`.`googlecheckout_api_debug`;
CREATE TABLE  `supernarede`.`googlecheckout_api_debug` (
  `debug_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `dir` enum('in','out') DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `request_body` text,
  `response_body` text,
  PRIMARY KEY (`debug_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`googlecheckout_api_debug`
--

/*!40000 ALTER TABLE `googlecheckout_api_debug` DISABLE KEYS */;
LOCK TABLES `googlecheckout_api_debug` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `googlecheckout_api_debug` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`googlecheckout_notification`
--

DROP TABLE IF EXISTS `supernarede`.`googlecheckout_notification`;
CREATE TABLE  `supernarede`.`googlecheckout_notification` (
  `serial_number` varchar(30) NOT NULL,
  `started_at` datetime DEFAULT NULL,
  `status` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`serial_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`googlecheckout_notification`
--

/*!40000 ALTER TABLE `googlecheckout_notification` DISABLE KEYS */;
LOCK TABLES `googlecheckout_notification` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `googlecheckout_notification` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`googleoptimizer_code`
--

DROP TABLE IF EXISTS `supernarede`.`googleoptimizer_code`;
CREATE TABLE  `supernarede`.`googleoptimizer_code` (
  `code_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entity_id` int(10) unsigned NOT NULL,
  `entity_type` varchar(50) NOT NULL DEFAULT '',
  `store_id` smallint(5) unsigned NOT NULL,
  `control_script` text,
  `tracking_script` text,
  `conversion_script` text,
  `conversion_page` varchar(255) NOT NULL DEFAULT '',
  `additional_data` text,
  PRIMARY KEY (`code_id`),
  KEY `GOOGLEOPTIMIZER_CODE_STORE` (`store_id`),
  CONSTRAINT `FK_GOOGLEOPTIMIZER_CODE_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`googleoptimizer_code`
--

/*!40000 ALTER TABLE `googleoptimizer_code` DISABLE KEYS */;
LOCK TABLES `googleoptimizer_code` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `googleoptimizer_code` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`ibanners_banner`
--

DROP TABLE IF EXISTS `supernarede`.`ibanners_banner`;
CREATE TABLE  `supernarede`.`ibanners_banner` (
  `banner_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` int(11) unsigned DEFAULT NULL,
  `title` varchar(255) NOT NULL DEFAULT '',
  `url` varchar(255) NOT NULL DEFAULT '',
  `image` varchar(255) NOT NULL DEFAULT '',
  `alt_text` varchar(255) NOT NULL DEFAULT '',
  `sort_order` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `is_enabled` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `html` text NOT NULL,
  PRIMARY KEY (`banner_id`),
  KEY `FK_GROUP_ID_BANNER` (`group_id`),
  CONSTRAINT `FK_GROUP_ID_BANNER` FOREIGN KEY (`group_id`) REFERENCES `ibanners_group` (`group_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`ibanners_banner`
--

/*!40000 ALTER TABLE `ibanners_banner` DISABLE KEYS */;
LOCK TABLES `ibanners_banner` WRITE;
INSERT INTO `supernarede`.`ibanners_banner` VALUES  (1,1,'Teste','#url','img_banner.png','alt teste',0,1,''),
 (2,2,'Exemplo','#teste','cargo_edinburgh_bar.jpg','teste',0,1,''),
 (3,1,'Exemplo 2','','4a1d7ea620f8.jpg','',0,0,'');
UNLOCK TABLES;
/*!40000 ALTER TABLE `ibanners_banner` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`ibanners_group`
--

DROP TABLE IF EXISTS `supernarede`.`ibanners_group`;
CREATE TABLE  `supernarede`.`ibanners_group` (
  `group_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `store_id` int(11) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `code` varchar(32) NOT NULL DEFAULT '',
  `is_enabled` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `carousel_duration` int(3) unsigned DEFAULT NULL,
  `carousel_auto` int(1) unsigned DEFAULT NULL,
  `carousel_frequency` int(3) unsigned DEFAULT NULL,
  `carousel_visible_slides` int(3) unsigned DEFAULT NULL,
  `carousel_effect` varchar(32) NOT NULL DEFAULT '',
  `carousel_transition` varchar(32) NOT NULL DEFAULT '',
  `carousel_animate` int(1) unsigned NOT NULL DEFAULT '1',
  `randomise_banners` int(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`group_id`),
  UNIQUE KEY `code` (`code`,`store_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`ibanners_group`
--

/*!40000 ALTER TABLE `ibanners_group` DISABLE KEYS */;
LOCK TABLES `ibanners_group` WRITE;
INSERT INTO `supernarede`.`ibanners_group` VALUES  (1,0,'Inicial (Topo da Home - 700x210)','topo_home',1,NULL,NULL,NULL,NULL,'','',1,0),
 (2,0,'Publicidade (Lateral Esquerda - 220x220)','esquerda',1,NULL,NULL,NULL,NULL,'','',1,0);
UNLOCK TABLES;
/*!40000 ALTER TABLE `ibanners_group` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`importexport_importdata`
--

DROP TABLE IF EXISTS `supernarede`.`importexport_importdata`;
CREATE TABLE  `supernarede`.`importexport_importdata` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entity` varchar(50) NOT NULL,
  `behavior` set('append','replace','delete') NOT NULL DEFAULT 'append',
  `data` mediumtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`importexport_importdata`
--

/*!40000 ALTER TABLE `importexport_importdata` DISABLE KEYS */;
LOCK TABLES `importexport_importdata` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `importexport_importdata` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`index_event`
--

DROP TABLE IF EXISTS `supernarede`.`index_event`;
CREATE TABLE  `supernarede`.`index_event` (
  `event_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(64) NOT NULL,
  `entity` varchar(64) NOT NULL,
  `entity_pk` bigint(20) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `old_data` mediumtext,
  `new_data` mediumtext,
  PRIMARY KEY (`event_id`),
  UNIQUE KEY `IDX_UNIQUE_EVENT` (`type`,`entity`,`entity_pk`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`index_event`
--

/*!40000 ALTER TABLE `index_event` DISABLE KEYS */;
LOCK TABLES `index_event` WRITE;
INSERT INTO `supernarede`.`index_event` VALUES  (1,'save','catalog_category',1,'2011-08-08 12:55:28','a:3:{s:30:\"Mage_Catalog_Model_Indexer_Url\";N;s:43:\"Mage_Catalog_Model_Category_Indexer_Product\";N;s:41:\"Mage_CatalogSearch_Model_Indexer_Fulltext\";N;}','a:8:{s:35:\"cataloginventory_stock_match_result\";b:0;s:34:\"catalog_product_price_match_result\";b:0;s:24:\"catalog_url_match_result\";b:1;s:30:\"Mage_Catalog_Model_Indexer_Url\";N;s:37:\"catalog_category_product_match_result\";b:1;s:43:\"Mage_Catalog_Model_Category_Indexer_Product\";N;s:35:\"catalogsearch_fulltext_match_result\";b:1;s:41:\"Mage_CatalogSearch_Model_Indexer_Fulltext\";N;}'),
 (2,'save','catalog_category',2,'2011-08-08 12:55:28','a:3:{s:30:\"Mage_Catalog_Model_Indexer_Url\";N;s:43:\"Mage_Catalog_Model_Category_Indexer_Product\";N;s:41:\"Mage_CatalogSearch_Model_Indexer_Fulltext\";N;}','a:9:{s:35:\"cataloginventory_stock_match_result\";b:0;s:34:\"catalog_product_price_match_result\";b:0;s:24:\"catalog_url_match_result\";b:1;s:30:\"Mage_Catalog_Model_Indexer_Url\";N;s:37:\"catalog_category_product_match_result\";b:1;s:43:\"Mage_Catalog_Model_Category_Indexer_Product\";N;s:35:\"catalogsearch_fulltext_match_result\";b:1;s:41:\"Mage_CatalogSearch_Model_Indexer_Fulltext\";N;s:33:\"catalog_product_flat_match_result\";b:0;}'),
 (3,'save','catalog_category',3,'2011-08-10 18:49:04','a:3:{s:30:\"Mage_Catalog_Model_Indexer_Url\";N;s:43:\"Mage_Catalog_Model_Category_Indexer_Product\";N;s:41:\"Mage_CatalogSearch_Model_Indexer_Fulltext\";N;}','a:9:{s:35:\"cataloginventory_stock_match_result\";b:0;s:34:\"catalog_product_price_match_result\";b:0;s:24:\"catalog_url_match_result\";b:1;s:30:\"Mage_Catalog_Model_Indexer_Url\";N;s:33:\"catalog_product_flat_match_result\";b:0;s:37:\"catalog_category_product_match_result\";b:1;s:43:\"Mage_Catalog_Model_Category_Indexer_Product\";N;s:35:\"catalogsearch_fulltext_match_result\";b:1;s:41:\"Mage_CatalogSearch_Model_Indexer_Fulltext\";N;}'),
 (4,'save','core_config_data',118,'2011-08-11 17:32:04','a:1:{s:40:\"Mage_Catalog_Model_Product_Indexer_Price\";N;}','a:7:{s:35:\"cataloginventory_stock_match_result\";b:0;s:34:\"catalog_product_price_match_result\";b:1;s:40:\"Mage_Catalog_Model_Product_Indexer_Price\";N;s:24:\"catalog_url_match_result\";b:0;s:33:\"catalog_product_flat_match_result\";b:0;s:37:\"catalog_category_product_match_result\";b:0;s:35:\"catalogsearch_fulltext_match_result\";b:0;}'),
 (5,'save','core_config_data',123,'2011-08-11 17:32:05','a:1:{s:30:\"Mage_Catalog_Model_Indexer_Url\";N;}','a:7:{s:35:\"cataloginventory_stock_match_result\";b:0;s:34:\"catalog_product_price_match_result\";b:0;s:24:\"catalog_url_match_result\";b:1;s:30:\"Mage_Catalog_Model_Indexer_Url\";N;s:33:\"catalog_product_flat_match_result\";b:0;s:37:\"catalog_category_product_match_result\";b:0;s:35:\"catalogsearch_fulltext_match_result\";b:0;}'),
 (6,'save','core_config_data',124,'2011-08-11 17:32:05','a:1:{s:30:\"Mage_Catalog_Model_Indexer_Url\";N;}','a:7:{s:35:\"cataloginventory_stock_match_result\";b:0;s:34:\"catalog_product_price_match_result\";b:0;s:24:\"catalog_url_match_result\";b:1;s:30:\"Mage_Catalog_Model_Indexer_Url\";N;s:33:\"catalog_product_flat_match_result\";b:0;s:37:\"catalog_category_product_match_result\";b:0;s:35:\"catalogsearch_fulltext_match_result\";b:0;}'),
 (7,'save','catalog_category',4,'2011-08-11 17:44:10','a:3:{s:30:\"Mage_Catalog_Model_Indexer_Url\";N;s:43:\"Mage_Catalog_Model_Category_Indexer_Product\";N;s:41:\"Mage_CatalogSearch_Model_Indexer_Fulltext\";N;}','a:9:{s:35:\"cataloginventory_stock_match_result\";b:0;s:34:\"catalog_product_price_match_result\";b:0;s:24:\"catalog_url_match_result\";b:1;s:30:\"Mage_Catalog_Model_Indexer_Url\";N;s:33:\"catalog_product_flat_match_result\";b:0;s:37:\"catalog_category_product_match_result\";b:1;s:43:\"Mage_Catalog_Model_Category_Indexer_Product\";N;s:35:\"catalogsearch_fulltext_match_result\";b:1;s:41:\"Mage_CatalogSearch_Model_Indexer_Fulltext\";N;}'),
 (8,'save','catalog_category',5,'2011-08-11 17:58:28','a:3:{s:30:\"Mage_Catalog_Model_Indexer_Url\";N;s:43:\"Mage_Catalog_Model_Category_Indexer_Product\";N;s:41:\"Mage_CatalogSearch_Model_Indexer_Fulltext\";N;}','a:9:{s:35:\"cataloginventory_stock_match_result\";b:0;s:34:\"catalog_product_price_match_result\";b:0;s:24:\"catalog_url_match_result\";b:1;s:30:\"Mage_Catalog_Model_Indexer_Url\";N;s:33:\"catalog_product_flat_match_result\";b:0;s:37:\"catalog_category_product_match_result\";b:1;s:43:\"Mage_Catalog_Model_Category_Indexer_Product\";N;s:35:\"catalogsearch_fulltext_match_result\";b:1;s:41:\"Mage_CatalogSearch_Model_Indexer_Fulltext\";N;}'),
 (9,'save','catalog_eav_attribute',62,'2011-08-12 13:51:18','a:2:{s:38:\"Mage_Catalog_Model_Product_Indexer_Eav\";N;s:39:\"Mage_Catalog_Model_Product_Indexer_Flat\";N;}','a:8:{s:35:\"cataloginventory_stock_match_result\";b:0;s:34:\"catalog_product_price_match_result\";b:0;s:24:\"catalog_url_match_result\";b:0;s:33:\"catalog_product_flat_match_result\";b:1;s:39:\"Mage_Catalog_Model_Product_Indexer_Flat\";N;s:37:\"catalog_category_product_match_result\";b:0;s:35:\"catalogsearch_fulltext_match_result\";b:0;s:38:\"Mage_Catalog_Model_Product_Indexer_Eav\";N;}'),
 (10,'save','catalog_eav_attribute',85,'2011-08-12 13:53:03','a:2:{s:38:\"Mage_Catalog_Model_Product_Indexer_Eav\";N;s:39:\"Mage_Catalog_Model_Product_Indexer_Flat\";N;}','a:8:{s:35:\"cataloginventory_stock_match_result\";b:0;s:34:\"catalog_product_price_match_result\";b:0;s:24:\"catalog_url_match_result\";b:0;s:33:\"catalog_product_flat_match_result\";b:1;s:39:\"Mage_Catalog_Model_Product_Indexer_Flat\";N;s:37:\"catalog_category_product_match_result\";b:0;s:35:\"catalogsearch_fulltext_match_result\";b:0;s:38:\"Mage_Catalog_Model_Product_Indexer_Eav\";N;}'),
 (11,'save','catalog_eav_attribute',122,'2011-08-12 14:19:33','a:2:{s:38:\"Mage_Catalog_Model_Product_Indexer_Eav\";N;s:41:\"Mage_CatalogSearch_Model_Indexer_Fulltext\";N;}','a:8:{s:35:\"cataloginventory_stock_match_result\";b:0;s:38:\"Mage_Catalog_Model_Product_Indexer_Eav\";N;s:34:\"catalog_product_price_match_result\";b:0;s:24:\"catalog_url_match_result\";b:0;s:33:\"catalog_product_flat_match_result\";b:0;s:37:\"catalog_category_product_match_result\";b:0;s:35:\"catalogsearch_fulltext_match_result\";b:1;s:41:\"Mage_CatalogSearch_Model_Indexer_Fulltext\";N;}'),
 (12,'save','catalog_eav_attribute',123,'2011-08-12 14:27:16','a:2:{s:38:\"Mage_Catalog_Model_Product_Indexer_Eav\";N;s:41:\"Mage_CatalogSearch_Model_Indexer_Fulltext\";N;}','a:8:{s:35:\"cataloginventory_stock_match_result\";b:0;s:38:\"Mage_Catalog_Model_Product_Indexer_Eav\";N;s:34:\"catalog_product_price_match_result\";b:0;s:24:\"catalog_url_match_result\";b:0;s:33:\"catalog_product_flat_match_result\";b:0;s:37:\"catalog_category_product_match_result\";b:0;s:35:\"catalogsearch_fulltext_match_result\";b:1;s:41:\"Mage_CatalogSearch_Model_Indexer_Fulltext\";N;}'),
 (13,'save','catalog_eav_attribute',124,'2011-08-12 16:11:36','a:2:{s:38:\"Mage_Catalog_Model_Product_Indexer_Eav\";N;s:41:\"Mage_CatalogSearch_Model_Indexer_Fulltext\";N;}','a:8:{s:35:\"cataloginventory_stock_match_result\";b:0;s:38:\"Mage_Catalog_Model_Product_Indexer_Eav\";N;s:34:\"catalog_product_price_match_result\";b:0;s:24:\"catalog_url_match_result\";b:0;s:33:\"catalog_product_flat_match_result\";b:0;s:37:\"catalog_category_product_match_result\";b:0;s:35:\"catalogsearch_fulltext_match_result\";b:0;s:41:\"Mage_CatalogSearch_Model_Indexer_Fulltext\";N;}'),
 (14,'save','catalog_category',6,'2011-08-12 17:33:03','a:3:{s:30:\"Mage_Catalog_Model_Indexer_Url\";N;s:43:\"Mage_Catalog_Model_Category_Indexer_Product\";N;s:41:\"Mage_CatalogSearch_Model_Indexer_Fulltext\";N;}','a:9:{s:35:\"cataloginventory_stock_match_result\";b:0;s:34:\"catalog_product_price_match_result\";b:0;s:24:\"catalog_url_match_result\";b:1;s:30:\"Mage_Catalog_Model_Indexer_Url\";N;s:33:\"catalog_product_flat_match_result\";b:0;s:37:\"catalog_category_product_match_result\";b:1;s:43:\"Mage_Catalog_Model_Category_Indexer_Product\";N;s:35:\"catalogsearch_fulltext_match_result\";b:1;s:41:\"Mage_CatalogSearch_Model_Indexer_Fulltext\";N;}'),
 (15,'save','catalog_category',7,'2011-08-12 17:37:12','a:3:{s:30:\"Mage_Catalog_Model_Indexer_Url\";N;s:43:\"Mage_Catalog_Model_Category_Indexer_Product\";N;s:41:\"Mage_CatalogSearch_Model_Indexer_Fulltext\";N;}','a:9:{s:35:\"cataloginventory_stock_match_result\";b:0;s:34:\"catalog_product_price_match_result\";b:0;s:24:\"catalog_url_match_result\";b:1;s:30:\"Mage_Catalog_Model_Indexer_Url\";N;s:33:\"catalog_product_flat_match_result\";b:0;s:37:\"catalog_category_product_match_result\";b:1;s:43:\"Mage_Catalog_Model_Category_Indexer_Product\";N;s:35:\"catalogsearch_fulltext_match_result\";b:1;s:41:\"Mage_CatalogSearch_Model_Indexer_Fulltext\";N;}'),
 (16,'save','catalog_category',8,'2011-08-12 18:24:11','a:3:{s:30:\"Mage_Catalog_Model_Indexer_Url\";N;s:43:\"Mage_Catalog_Model_Category_Indexer_Product\";N;s:41:\"Mage_CatalogSearch_Model_Indexer_Fulltext\";N;}','a:9:{s:35:\"cataloginventory_stock_match_result\";b:0;s:34:\"catalog_product_price_match_result\";b:0;s:24:\"catalog_url_match_result\";b:1;s:30:\"Mage_Catalog_Model_Indexer_Url\";N;s:33:\"catalog_product_flat_match_result\";b:0;s:37:\"catalog_category_product_match_result\";b:1;s:43:\"Mage_Catalog_Model_Category_Indexer_Product\";N;s:35:\"catalogsearch_fulltext_match_result\";b:1;s:41:\"Mage_CatalogSearch_Model_Indexer_Fulltext\";N;}'),
 (17,'save','catalog_category',9,'2011-08-12 18:25:57','a:3:{s:30:\"Mage_Catalog_Model_Indexer_Url\";N;s:43:\"Mage_Catalog_Model_Category_Indexer_Product\";N;s:41:\"Mage_CatalogSearch_Model_Indexer_Fulltext\";N;}','a:9:{s:35:\"cataloginventory_stock_match_result\";b:0;s:34:\"catalog_product_price_match_result\";b:0;s:24:\"catalog_url_match_result\";b:1;s:30:\"Mage_Catalog_Model_Indexer_Url\";N;s:33:\"catalog_product_flat_match_result\";b:0;s:37:\"catalog_category_product_match_result\";b:1;s:43:\"Mage_Catalog_Model_Category_Indexer_Product\";N;s:35:\"catalogsearch_fulltext_match_result\";b:1;s:41:\"Mage_CatalogSearch_Model_Indexer_Fulltext\";N;}'),
 (18,'save','catalog_category',10,'2011-08-12 18:26:29','a:3:{s:30:\"Mage_Catalog_Model_Indexer_Url\";N;s:43:\"Mage_Catalog_Model_Category_Indexer_Product\";N;s:41:\"Mage_CatalogSearch_Model_Indexer_Fulltext\";N;}','a:9:{s:35:\"cataloginventory_stock_match_result\";b:0;s:34:\"catalog_product_price_match_result\";b:0;s:24:\"catalog_url_match_result\";b:1;s:30:\"Mage_Catalog_Model_Indexer_Url\";N;s:33:\"catalog_product_flat_match_result\";b:0;s:37:\"catalog_category_product_match_result\";b:1;s:43:\"Mage_Catalog_Model_Category_Indexer_Product\";N;s:35:\"catalogsearch_fulltext_match_result\";b:1;s:41:\"Mage_CatalogSearch_Model_Indexer_Fulltext\";N;}'),
 (19,'save','catalog_category',11,'2011-08-12 18:27:04','a:3:{s:30:\"Mage_Catalog_Model_Indexer_Url\";N;s:43:\"Mage_Catalog_Model_Category_Indexer_Product\";N;s:41:\"Mage_CatalogSearch_Model_Indexer_Fulltext\";N;}','a:9:{s:35:\"cataloginventory_stock_match_result\";b:0;s:34:\"catalog_product_price_match_result\";b:0;s:24:\"catalog_url_match_result\";b:1;s:30:\"Mage_Catalog_Model_Indexer_Url\";N;s:33:\"catalog_product_flat_match_result\";b:0;s:37:\"catalog_category_product_match_result\";b:1;s:43:\"Mage_Catalog_Model_Category_Indexer_Product\";N;s:35:\"catalogsearch_fulltext_match_result\";b:1;s:41:\"Mage_CatalogSearch_Model_Indexer_Fulltext\";N;}'),
 (20,'save','catalog_category',12,'2011-08-12 18:27:39','a:3:{s:30:\"Mage_Catalog_Model_Indexer_Url\";N;s:43:\"Mage_Catalog_Model_Category_Indexer_Product\";N;s:41:\"Mage_CatalogSearch_Model_Indexer_Fulltext\";N;}','a:9:{s:35:\"cataloginventory_stock_match_result\";b:0;s:34:\"catalog_product_price_match_result\";b:0;s:24:\"catalog_url_match_result\";b:1;s:30:\"Mage_Catalog_Model_Indexer_Url\";N;s:33:\"catalog_product_flat_match_result\";b:0;s:37:\"catalog_category_product_match_result\";b:1;s:43:\"Mage_Catalog_Model_Category_Indexer_Product\";N;s:35:\"catalogsearch_fulltext_match_result\";b:1;s:41:\"Mage_CatalogSearch_Model_Indexer_Fulltext\";N;}'),
 (21,'save','catalog_category',13,'2011-08-12 18:28:20','a:3:{s:30:\"Mage_Catalog_Model_Indexer_Url\";N;s:43:\"Mage_Catalog_Model_Category_Indexer_Product\";N;s:41:\"Mage_CatalogSearch_Model_Indexer_Fulltext\";N;}','a:9:{s:35:\"cataloginventory_stock_match_result\";b:0;s:34:\"catalog_product_price_match_result\";b:0;s:24:\"catalog_url_match_result\";b:1;s:30:\"Mage_Catalog_Model_Indexer_Url\";N;s:33:\"catalog_product_flat_match_result\";b:0;s:37:\"catalog_category_product_match_result\";b:1;s:43:\"Mage_Catalog_Model_Category_Indexer_Product\";N;s:35:\"catalogsearch_fulltext_match_result\";b:1;s:41:\"Mage_CatalogSearch_Model_Indexer_Fulltext\";N;}'),
 (22,'save','catalog_category',14,'2011-08-12 21:13:23','a:3:{s:30:\"Mage_Catalog_Model_Indexer_Url\";N;s:43:\"Mage_Catalog_Model_Category_Indexer_Product\";N;s:41:\"Mage_CatalogSearch_Model_Indexer_Fulltext\";N;}','a:9:{s:35:\"cataloginventory_stock_match_result\";b:0;s:34:\"catalog_product_price_match_result\";b:0;s:24:\"catalog_url_match_result\";b:1;s:30:\"Mage_Catalog_Model_Indexer_Url\";N;s:33:\"catalog_product_flat_match_result\";b:0;s:37:\"catalog_category_product_match_result\";b:1;s:43:\"Mage_Catalog_Model_Category_Indexer_Product\";N;s:35:\"catalogsearch_fulltext_match_result\";b:1;s:41:\"Mage_CatalogSearch_Model_Indexer_Fulltext\";N;}'),
 (23,'save','catalog_category',15,'2011-08-17 12:23:09','a:3:{s:30:\"Mage_Catalog_Model_Indexer_Url\";N;s:43:\"Mage_Catalog_Model_Category_Indexer_Product\";N;s:41:\"Mage_CatalogSearch_Model_Indexer_Fulltext\";N;}','a:9:{s:35:\"cataloginventory_stock_match_result\";b:0;s:34:\"catalog_product_price_match_result\";b:0;s:24:\"catalog_url_match_result\";b:1;s:30:\"Mage_Catalog_Model_Indexer_Url\";N;s:33:\"catalog_product_flat_match_result\";b:0;s:37:\"catalog_category_product_match_result\";b:1;s:43:\"Mage_Catalog_Model_Category_Indexer_Product\";N;s:35:\"catalogsearch_fulltext_match_result\";b:1;s:41:\"Mage_CatalogSearch_Model_Indexer_Fulltext\";N;}'),
 (24,'mass_action','catalog_product',NULL,'2011-08-17 12:38:48','a:7:{s:41:\"Mage_CatalogInventory_Model_Indexer_Stock\";N;s:38:\"Mage_Catalog_Model_Product_Indexer_Eav\";N;s:40:\"Mage_Catalog_Model_Product_Indexer_Price\";N;s:39:\"Mage_Catalog_Model_Product_Indexer_Flat\";N;s:43:\"Mage_Catalog_Model_Category_Indexer_Product\";N;s:41:\"Mage_CatalogSearch_Model_Indexer_Fulltext\";N;s:30:\"Mage_Tag_Model_Indexer_Summary\";N;}','a:13:{s:35:\"cataloginventory_stock_match_result\";b:1;s:41:\"Mage_CatalogInventory_Model_Indexer_Stock\";N;s:34:\"catalog_product_price_match_result\";b:1;s:40:\"Mage_Catalog_Model_Product_Indexer_Price\";N;s:24:\"catalog_url_match_result\";b:0;s:33:\"catalog_product_flat_match_result\";b:1;s:39:\"Mage_Catalog_Model_Product_Indexer_Flat\";N;s:37:\"catalog_category_product_match_result\";b:1;s:43:\"Mage_Catalog_Model_Category_Indexer_Product\";N;s:35:\"catalogsearch_fulltext_match_result\";b:1;s:41:\"Mage_CatalogSearch_Model_Indexer_Fulltext\";N;s:38:\"Mage_Catalog_Model_Product_Indexer_Eav\";N;s:30:\"Mage_Tag_Model_Indexer_Summary\";N;}'),
 (25,'save','cataloginventory_stock_item',1,'2011-08-17 12:38:48','a:1:{s:41:\"Mage_CatalogInventory_Model_Indexer_Stock\";N;}','a:7:{s:35:\"cataloginventory_stock_match_result\";b:1;s:41:\"Mage_CatalogInventory_Model_Indexer_Stock\";N;s:34:\"catalog_product_price_match_result\";b:0;s:24:\"catalog_url_match_result\";b:0;s:33:\"catalog_product_flat_match_result\";b:0;s:37:\"catalog_category_product_match_result\";b:0;s:35:\"catalogsearch_fulltext_match_result\";b:0;}'),
 (26,'save','catalog_product',1,'2011-08-17 12:38:50','a:8:{s:41:\"Mage_CatalogInventory_Model_Indexer_Stock\";N;s:38:\"Mage_Catalog_Model_Product_Indexer_Eav\";N;s:40:\"Mage_Catalog_Model_Product_Indexer_Price\";N;s:30:\"Mage_Catalog_Model_Indexer_Url\";N;s:39:\"Mage_Catalog_Model_Product_Indexer_Flat\";N;s:43:\"Mage_Catalog_Model_Category_Indexer_Product\";N;s:41:\"Mage_CatalogSearch_Model_Indexer_Fulltext\";N;s:30:\"Mage_Tag_Model_Indexer_Summary\";N;}','a:14:{s:35:\"cataloginventory_stock_match_result\";b:1;s:41:\"Mage_CatalogInventory_Model_Indexer_Stock\";N;s:38:\"Mage_Catalog_Model_Product_Indexer_Eav\";N;s:34:\"catalog_product_price_match_result\";b:1;s:40:\"Mage_Catalog_Model_Product_Indexer_Price\";N;s:24:\"catalog_url_match_result\";b:1;s:30:\"Mage_Catalog_Model_Indexer_Url\";N;s:33:\"catalog_product_flat_match_result\";b:1;s:39:\"Mage_Catalog_Model_Product_Indexer_Flat\";N;s:37:\"catalog_category_product_match_result\";b:1;s:43:\"Mage_Catalog_Model_Category_Indexer_Product\";N;s:35:\"catalogsearch_fulltext_match_result\";b:1;s:41:\"Mage_CatalogSearch_Model_Indexer_Fulltext\";N;s:30:\"Mage_Tag_Model_Indexer_Summary\";N;}'),
 (27,'save','cataloginventory_stock_item',2,'2011-08-17 12:46:24','a:1:{s:41:\"Mage_CatalogInventory_Model_Indexer_Stock\";N;}','a:7:{s:35:\"cataloginventory_stock_match_result\";b:1;s:41:\"Mage_CatalogInventory_Model_Indexer_Stock\";N;s:34:\"catalog_product_price_match_result\";b:0;s:24:\"catalog_url_match_result\";b:0;s:33:\"catalog_product_flat_match_result\";b:0;s:37:\"catalog_category_product_match_result\";b:0;s:35:\"catalogsearch_fulltext_match_result\";b:0;}'),
 (28,'save','catalog_product',2,'2011-08-17 12:46:25','a:8:{s:41:\"Mage_CatalogInventory_Model_Indexer_Stock\";N;s:38:\"Mage_Catalog_Model_Product_Indexer_Eav\";N;s:40:\"Mage_Catalog_Model_Product_Indexer_Price\";N;s:30:\"Mage_Catalog_Model_Indexer_Url\";N;s:39:\"Mage_Catalog_Model_Product_Indexer_Flat\";N;s:43:\"Mage_Catalog_Model_Category_Indexer_Product\";N;s:41:\"Mage_CatalogSearch_Model_Indexer_Fulltext\";N;s:30:\"Mage_Tag_Model_Indexer_Summary\";N;}','a:14:{s:35:\"cataloginventory_stock_match_result\";b:1;s:41:\"Mage_CatalogInventory_Model_Indexer_Stock\";N;s:38:\"Mage_Catalog_Model_Product_Indexer_Eav\";N;s:34:\"catalog_product_price_match_result\";b:1;s:40:\"Mage_Catalog_Model_Product_Indexer_Price\";N;s:24:\"catalog_url_match_result\";b:1;s:30:\"Mage_Catalog_Model_Indexer_Url\";N;s:33:\"catalog_product_flat_match_result\";b:1;s:39:\"Mage_Catalog_Model_Product_Indexer_Flat\";N;s:37:\"catalog_category_product_match_result\";b:1;s:43:\"Mage_Catalog_Model_Category_Indexer_Product\";N;s:35:\"catalogsearch_fulltext_match_result\";b:1;s:41:\"Mage_CatalogSearch_Model_Indexer_Fulltext\";N;s:30:\"Mage_Tag_Model_Indexer_Summary\";N;}'),
 (29,'save','cataloginventory_stock_item',3,'2011-08-17 12:50:21','a:1:{s:41:\"Mage_CatalogInventory_Model_Indexer_Stock\";N;}','a:7:{s:35:\"cataloginventory_stock_match_result\";b:1;s:41:\"Mage_CatalogInventory_Model_Indexer_Stock\";N;s:34:\"catalog_product_price_match_result\";b:0;s:24:\"catalog_url_match_result\";b:0;s:33:\"catalog_product_flat_match_result\";b:0;s:37:\"catalog_category_product_match_result\";b:0;s:35:\"catalogsearch_fulltext_match_result\";b:0;}'),
 (30,'save','catalog_product',3,'2011-08-17 12:50:23','a:8:{s:41:\"Mage_CatalogInventory_Model_Indexer_Stock\";N;s:38:\"Mage_Catalog_Model_Product_Indexer_Eav\";N;s:40:\"Mage_Catalog_Model_Product_Indexer_Price\";N;s:30:\"Mage_Catalog_Model_Indexer_Url\";N;s:39:\"Mage_Catalog_Model_Product_Indexer_Flat\";N;s:43:\"Mage_Catalog_Model_Category_Indexer_Product\";N;s:41:\"Mage_CatalogSearch_Model_Indexer_Fulltext\";N;s:30:\"Mage_Tag_Model_Indexer_Summary\";N;}','a:14:{s:35:\"cataloginventory_stock_match_result\";b:1;s:41:\"Mage_CatalogInventory_Model_Indexer_Stock\";N;s:38:\"Mage_Catalog_Model_Product_Indexer_Eav\";N;s:34:\"catalog_product_price_match_result\";b:1;s:40:\"Mage_Catalog_Model_Product_Indexer_Price\";N;s:24:\"catalog_url_match_result\";b:1;s:30:\"Mage_Catalog_Model_Indexer_Url\";N;s:33:\"catalog_product_flat_match_result\";b:1;s:39:\"Mage_Catalog_Model_Product_Indexer_Flat\";N;s:37:\"catalog_category_product_match_result\";b:1;s:43:\"Mage_Catalog_Model_Category_Indexer_Product\";N;s:35:\"catalogsearch_fulltext_match_result\";b:1;s:41:\"Mage_CatalogSearch_Model_Indexer_Fulltext\";N;s:30:\"Mage_Tag_Model_Indexer_Summary\";N;}'),
 (31,'save','cataloginventory_stock_item',4,'2011-08-17 12:52:39','a:1:{s:41:\"Mage_CatalogInventory_Model_Indexer_Stock\";N;}','a:7:{s:35:\"cataloginventory_stock_match_result\";b:1;s:41:\"Mage_CatalogInventory_Model_Indexer_Stock\";N;s:34:\"catalog_product_price_match_result\";b:0;s:24:\"catalog_url_match_result\";b:0;s:33:\"catalog_product_flat_match_result\";b:0;s:37:\"catalog_category_product_match_result\";b:0;s:35:\"catalogsearch_fulltext_match_result\";b:0;}'),
 (32,'save','catalog_product',4,'2011-08-17 12:52:40','a:8:{s:41:\"Mage_CatalogInventory_Model_Indexer_Stock\";N;s:38:\"Mage_Catalog_Model_Product_Indexer_Eav\";N;s:40:\"Mage_Catalog_Model_Product_Indexer_Price\";N;s:30:\"Mage_Catalog_Model_Indexer_Url\";N;s:39:\"Mage_Catalog_Model_Product_Indexer_Flat\";N;s:43:\"Mage_Catalog_Model_Category_Indexer_Product\";N;s:41:\"Mage_CatalogSearch_Model_Indexer_Fulltext\";N;s:30:\"Mage_Tag_Model_Indexer_Summary\";N;}','a:14:{s:35:\"cataloginventory_stock_match_result\";b:1;s:41:\"Mage_CatalogInventory_Model_Indexer_Stock\";N;s:38:\"Mage_Catalog_Model_Product_Indexer_Eav\";N;s:34:\"catalog_product_price_match_result\";b:1;s:40:\"Mage_Catalog_Model_Product_Indexer_Price\";N;s:24:\"catalog_url_match_result\";b:1;s:30:\"Mage_Catalog_Model_Indexer_Url\";N;s:33:\"catalog_product_flat_match_result\";b:1;s:39:\"Mage_Catalog_Model_Product_Indexer_Flat\";N;s:37:\"catalog_category_product_match_result\";b:1;s:43:\"Mage_Catalog_Model_Category_Indexer_Product\";N;s:35:\"catalogsearch_fulltext_match_result\";b:1;s:41:\"Mage_CatalogSearch_Model_Indexer_Fulltext\";N;s:30:\"Mage_Tag_Model_Indexer_Summary\";N;}'),
 (33,'save','cataloginventory_stock_item',5,'2011-08-17 13:07:19','a:1:{s:41:\"Mage_CatalogInventory_Model_Indexer_Stock\";N;}','a:7:{s:35:\"cataloginventory_stock_match_result\";b:1;s:41:\"Mage_CatalogInventory_Model_Indexer_Stock\";N;s:34:\"catalog_product_price_match_result\";b:0;s:24:\"catalog_url_match_result\";b:0;s:33:\"catalog_product_flat_match_result\";b:0;s:37:\"catalog_category_product_match_result\";b:0;s:35:\"catalogsearch_fulltext_match_result\";b:0;}'),
 (34,'save','catalog_product',5,'2011-08-17 13:07:22','a:8:{s:41:\"Mage_CatalogInventory_Model_Indexer_Stock\";N;s:38:\"Mage_Catalog_Model_Product_Indexer_Eav\";N;s:40:\"Mage_Catalog_Model_Product_Indexer_Price\";N;s:30:\"Mage_Catalog_Model_Indexer_Url\";N;s:39:\"Mage_Catalog_Model_Product_Indexer_Flat\";N;s:43:\"Mage_Catalog_Model_Category_Indexer_Product\";N;s:41:\"Mage_CatalogSearch_Model_Indexer_Fulltext\";N;s:30:\"Mage_Tag_Model_Indexer_Summary\";N;}','a:14:{s:35:\"cataloginventory_stock_match_result\";b:1;s:41:\"Mage_CatalogInventory_Model_Indexer_Stock\";N;s:38:\"Mage_Catalog_Model_Product_Indexer_Eav\";N;s:34:\"catalog_product_price_match_result\";b:1;s:40:\"Mage_Catalog_Model_Product_Indexer_Price\";N;s:24:\"catalog_url_match_result\";b:1;s:30:\"Mage_Catalog_Model_Indexer_Url\";N;s:33:\"catalog_product_flat_match_result\";b:1;s:39:\"Mage_Catalog_Model_Product_Indexer_Flat\";N;s:37:\"catalog_category_product_match_result\";b:1;s:43:\"Mage_Catalog_Model_Category_Indexer_Product\";N;s:35:\"catalogsearch_fulltext_match_result\";b:1;s:41:\"Mage_CatalogSearch_Model_Indexer_Fulltext\";N;s:30:\"Mage_Tag_Model_Indexer_Summary\";N;}'),
 (35,'save','catalog_eav_attribute',61,'2011-08-17 16:36:51','a:1:{s:38:\"Mage_Catalog_Model_Product_Indexer_Eav\";N;}','a:7:{s:35:\"cataloginventory_stock_match_result\";b:0;s:34:\"catalog_product_price_match_result\";b:0;s:24:\"catalog_url_match_result\";b:0;s:33:\"catalog_product_flat_match_result\";b:0;s:37:\"catalog_category_product_match_result\";b:0;s:35:\"catalogsearch_fulltext_match_result\";b:0;s:38:\"Mage_Catalog_Model_Product_Indexer_Eav\";N;}'),
 (36,'save','catalog_eav_attribute',64,'2011-08-17 16:37:30','a:2:{s:38:\"Mage_Catalog_Model_Product_Indexer_Eav\";N;s:39:\"Mage_Catalog_Model_Product_Indexer_Flat\";N;}','a:8:{s:35:\"cataloginventory_stock_match_result\";b:0;s:34:\"catalog_product_price_match_result\";b:0;s:24:\"catalog_url_match_result\";b:0;s:33:\"catalog_product_flat_match_result\";b:1;s:39:\"Mage_Catalog_Model_Product_Indexer_Flat\";N;s:37:\"catalog_category_product_match_result\";b:0;s:35:\"catalogsearch_fulltext_match_result\";b:0;s:38:\"Mage_Catalog_Model_Product_Indexer_Eav\";N;}'),
 (37,'save','cataloginventory_stock_item',6,'2011-08-17 16:40:29','a:1:{s:41:\"Mage_CatalogInventory_Model_Indexer_Stock\";N;}','a:7:{s:35:\"cataloginventory_stock_match_result\";b:1;s:41:\"Mage_CatalogInventory_Model_Indexer_Stock\";N;s:34:\"catalog_product_price_match_result\";b:0;s:24:\"catalog_url_match_result\";b:0;s:33:\"catalog_product_flat_match_result\";b:0;s:37:\"catalog_category_product_match_result\";b:0;s:35:\"catalogsearch_fulltext_match_result\";b:0;}'),
 (38,'save','catalog_product',6,'2011-08-17 16:40:31','a:8:{s:41:\"Mage_CatalogInventory_Model_Indexer_Stock\";N;s:38:\"Mage_Catalog_Model_Product_Indexer_Eav\";N;s:40:\"Mage_Catalog_Model_Product_Indexer_Price\";N;s:30:\"Mage_Catalog_Model_Indexer_Url\";N;s:39:\"Mage_Catalog_Model_Product_Indexer_Flat\";N;s:43:\"Mage_Catalog_Model_Category_Indexer_Product\";N;s:41:\"Mage_CatalogSearch_Model_Indexer_Fulltext\";N;s:30:\"Mage_Tag_Model_Indexer_Summary\";N;}','a:14:{s:35:\"cataloginventory_stock_match_result\";b:1;s:41:\"Mage_CatalogInventory_Model_Indexer_Stock\";N;s:38:\"Mage_Catalog_Model_Product_Indexer_Eav\";N;s:34:\"catalog_product_price_match_result\";b:1;s:40:\"Mage_Catalog_Model_Product_Indexer_Price\";N;s:24:\"catalog_url_match_result\";b:1;s:30:\"Mage_Catalog_Model_Indexer_Url\";N;s:33:\"catalog_product_flat_match_result\";b:1;s:39:\"Mage_Catalog_Model_Product_Indexer_Flat\";N;s:37:\"catalog_category_product_match_result\";b:1;s:43:\"Mage_Catalog_Model_Category_Indexer_Product\";N;s:35:\"catalogsearch_fulltext_match_result\";b:1;s:41:\"Mage_CatalogSearch_Model_Indexer_Fulltext\";N;s:30:\"Mage_Tag_Model_Indexer_Summary\";N;}'),
 (39,'save','catalog_category',16,'2011-09-16 12:38:56','a:3:{s:30:\"Mage_Catalog_Model_Indexer_Url\";N;s:43:\"Mage_Catalog_Model_Category_Indexer_Product\";N;s:41:\"Mage_CatalogSearch_Model_Indexer_Fulltext\";N;}','a:9:{s:35:\"cataloginventory_stock_match_result\";b:0;s:34:\"catalog_product_price_match_result\";b:0;s:24:\"catalog_url_match_result\";b:1;s:30:\"Mage_Catalog_Model_Indexer_Url\";N;s:33:\"catalog_product_flat_match_result\";b:0;s:37:\"catalog_category_product_match_result\";b:1;s:43:\"Mage_Catalog_Model_Category_Indexer_Product\";N;s:35:\"catalogsearch_fulltext_match_result\";b:1;s:41:\"Mage_CatalogSearch_Model_Indexer_Fulltext\";N;}'),
 (40,'save','catalog_category',17,'2011-09-16 12:40:46','a:3:{s:30:\"Mage_Catalog_Model_Indexer_Url\";N;s:43:\"Mage_Catalog_Model_Category_Indexer_Product\";N;s:41:\"Mage_CatalogSearch_Model_Indexer_Fulltext\";N;}','a:9:{s:35:\"cataloginventory_stock_match_result\";b:0;s:34:\"catalog_product_price_match_result\";b:0;s:24:\"catalog_url_match_result\";b:1;s:30:\"Mage_Catalog_Model_Indexer_Url\";N;s:33:\"catalog_product_flat_match_result\";b:0;s:37:\"catalog_category_product_match_result\";b:1;s:43:\"Mage_Catalog_Model_Category_Indexer_Product\";N;s:35:\"catalogsearch_fulltext_match_result\";b:1;s:41:\"Mage_CatalogSearch_Model_Indexer_Fulltext\";N;}'),
 (41,'save','catalog_category',18,'2011-09-16 13:23:31','a:3:{s:30:\"Mage_Catalog_Model_Indexer_Url\";N;s:43:\"Mage_Catalog_Model_Category_Indexer_Product\";N;s:41:\"Mage_CatalogSearch_Model_Indexer_Fulltext\";N;}','a:9:{s:35:\"cataloginventory_stock_match_result\";b:0;s:34:\"catalog_product_price_match_result\";b:0;s:24:\"catalog_url_match_result\";b:1;s:30:\"Mage_Catalog_Model_Indexer_Url\";N;s:33:\"catalog_product_flat_match_result\";b:0;s:37:\"catalog_category_product_match_result\";b:1;s:43:\"Mage_Catalog_Model_Category_Indexer_Product\";N;s:35:\"catalogsearch_fulltext_match_result\";b:1;s:41:\"Mage_CatalogSearch_Model_Indexer_Fulltext\";N;}'),
 (42,'save','catalog_category',19,'2011-09-16 13:24:30','a:3:{s:30:\"Mage_Catalog_Model_Indexer_Url\";N;s:43:\"Mage_Catalog_Model_Category_Indexer_Product\";N;s:41:\"Mage_CatalogSearch_Model_Indexer_Fulltext\";N;}','a:9:{s:35:\"cataloginventory_stock_match_result\";b:0;s:34:\"catalog_product_price_match_result\";b:0;s:24:\"catalog_url_match_result\";b:1;s:30:\"Mage_Catalog_Model_Indexer_Url\";N;s:33:\"catalog_product_flat_match_result\";b:0;s:37:\"catalog_category_product_match_result\";b:1;s:43:\"Mage_Catalog_Model_Category_Indexer_Product\";N;s:35:\"catalogsearch_fulltext_match_result\";b:1;s:41:\"Mage_CatalogSearch_Model_Indexer_Fulltext\";N;}');
UNLOCK TABLES;
/*!40000 ALTER TABLE `index_event` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`index_process`
--

DROP TABLE IF EXISTS `supernarede`.`index_process`;
CREATE TABLE  `supernarede`.`index_process` (
  `process_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `indexer_code` varchar(32) NOT NULL,
  `status` enum('pending','working','require_reindex') NOT NULL DEFAULT 'pending',
  `started_at` datetime DEFAULT NULL,
  `ended_at` datetime DEFAULT NULL,
  `mode` enum('real_time','manual') NOT NULL DEFAULT 'real_time',
  PRIMARY KEY (`process_id`),
  UNIQUE KEY `IDX_CODE` (`indexer_code`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`index_process`
--

/*!40000 ALTER TABLE `index_process` DISABLE KEYS */;
LOCK TABLES `index_process` WRITE;
INSERT INTO `supernarede`.`index_process` VALUES  (1,'catalog_product_attribute','pending','2011-08-12 16:13:58','2011-08-12 16:13:58','real_time'),
 (2,'catalog_product_price','pending','2011-09-21 20:07:04','2011-09-21 20:07:06','real_time'),
 (3,'catalog_url','pending','2011-08-12 16:14:02','2011-08-12 16:14:02','real_time'),
 (4,'catalog_product_flat','pending','2011-08-17 16:37:40','2011-08-17 16:37:43','real_time'),
 (5,'catalog_category_flat','pending','2011-08-12 16:14:03','2011-08-12 16:14:04','real_time'),
 (6,'catalog_category_product','pending','2011-08-12 16:14:04','2011-08-12 16:14:05','real_time'),
 (7,'catalogsearch_fulltext','pending','2011-08-12 16:14:05','2011-08-12 16:14:06','real_time'),
 (8,'cataloginventory_stock','pending','2011-08-12 16:13:57','2011-08-12 16:13:58','real_time'),
 (9,'tag_summary','pending','2011-08-12 16:14:06','2011-08-12 16:14:06','real_time');
UNLOCK TABLES;
/*!40000 ALTER TABLE `index_process` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`index_process_event`
--

DROP TABLE IF EXISTS `supernarede`.`index_process_event`;
CREATE TABLE  `supernarede`.`index_process_event` (
  `process_id` int(10) unsigned NOT NULL,
  `event_id` bigint(20) unsigned NOT NULL,
  `status` enum('new','working','done','error') NOT NULL DEFAULT 'new',
  PRIMARY KEY (`process_id`,`event_id`),
  KEY `FK_INDEX_EVNT_PROCESS` (`event_id`),
  CONSTRAINT `FK_INDEX_EVNT_PROCESS` FOREIGN KEY (`event_id`) REFERENCES `index_event` (`event_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_INDEX_PROCESS_EVENT` FOREIGN KEY (`process_id`) REFERENCES `index_process` (`process_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`index_process_event`
--

/*!40000 ALTER TABLE `index_process_event` DISABLE KEYS */;
LOCK TABLES `index_process_event` WRITE;
INSERT INTO `supernarede`.`index_process_event` VALUES  (1,9,'done'),
 (1,10,'done'),
 (1,11,'done'),
 (1,12,'done'),
 (1,13,'done'),
 (1,24,'done'),
 (1,26,'done'),
 (1,28,'done'),
 (1,30,'done'),
 (1,32,'done'),
 (1,34,'done'),
 (1,35,'done'),
 (1,36,'done'),
 (1,38,'done'),
 (2,4,'done'),
 (2,24,'done'),
 (2,26,'done'),
 (2,28,'done'),
 (2,30,'done'),
 (2,32,'done'),
 (2,34,'done'),
 (2,38,'done'),
 (3,1,'done'),
 (3,2,'done'),
 (3,3,'done'),
 (3,5,'done'),
 (3,6,'done'),
 (3,7,'done'),
 (3,8,'done'),
 (3,14,'done'),
 (3,15,'done'),
 (3,16,'done'),
 (3,17,'done'),
 (3,18,'done'),
 (3,19,'done'),
 (3,20,'done'),
 (3,21,'done'),
 (3,22,'done'),
 (3,23,'done'),
 (3,26,'done'),
 (3,28,'done'),
 (3,30,'done'),
 (3,32,'done'),
 (3,34,'done'),
 (3,38,'done'),
 (3,39,'done'),
 (3,40,'done'),
 (3,41,'done'),
 (3,42,'done'),
 (4,9,'done'),
 (4,10,'done'),
 (4,24,'done'),
 (4,26,'done'),
 (4,28,'done'),
 (4,30,'done'),
 (4,32,'done'),
 (4,34,'done'),
 (4,36,'done'),
 (4,38,'done'),
 (6,1,'done'),
 (6,2,'done'),
 (6,3,'done'),
 (6,7,'done'),
 (6,8,'done'),
 (6,14,'done'),
 (6,15,'done'),
 (6,16,'done'),
 (6,17,'done'),
 (6,18,'done'),
 (6,19,'done'),
 (6,20,'done'),
 (6,21,'done'),
 (6,22,'done'),
 (6,23,'done'),
 (6,24,'done'),
 (6,26,'done'),
 (6,28,'done'),
 (6,30,'done'),
 (6,32,'done'),
 (6,34,'done'),
 (6,38,'done'),
 (6,39,'done'),
 (6,40,'done'),
 (6,41,'done'),
 (6,42,'done'),
 (7,1,'done'),
 (7,2,'done'),
 (7,3,'done'),
 (7,7,'done'),
 (7,8,'done'),
 (7,11,'done'),
 (7,12,'done'),
 (7,13,'done'),
 (7,14,'done'),
 (7,15,'done'),
 (7,16,'done'),
 (7,17,'done'),
 (7,18,'done'),
 (7,19,'done'),
 (7,20,'done'),
 (7,21,'done'),
 (7,22,'done'),
 (7,23,'done'),
 (7,24,'done'),
 (7,26,'done'),
 (7,28,'done'),
 (7,30,'done'),
 (7,32,'done'),
 (7,34,'done'),
 (7,38,'done'),
 (7,39,'done'),
 (7,40,'done'),
 (7,41,'done'),
 (7,42,'done'),
 (8,24,'done'),
 (8,25,'done'),
 (8,26,'done'),
 (8,27,'done'),
 (8,28,'done'),
 (8,29,'done'),
 (8,30,'done'),
 (8,31,'done'),
 (8,32,'done'),
 (8,33,'done'),
 (8,34,'done'),
 (8,37,'done'),
 (8,38,'done'),
 (9,24,'done'),
 (9,26,'done'),
 (9,28,'done'),
 (9,30,'done'),
 (9,32,'done'),
 (9,34,'done'),
 (9,38,'done');
UNLOCK TABLES;
/*!40000 ALTER TABLE `index_process_event` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`log_customer`
--

DROP TABLE IF EXISTS `supernarede`.`log_customer`;
CREATE TABLE  `supernarede`.`log_customer` (
  `log_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `visitor_id` bigint(20) unsigned DEFAULT NULL,
  `customer_id` int(11) NOT NULL DEFAULT '0',
  `login_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `logout_at` datetime DEFAULT NULL,
  `store_id` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`log_id`),
  KEY `IDX_VISITOR` (`visitor_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Customers log information';

--
-- Dumping data for table `supernarede`.`log_customer`
--

/*!40000 ALTER TABLE `log_customer` DISABLE KEYS */;
LOCK TABLES `log_customer` WRITE;
INSERT INTO `supernarede`.`log_customer` VALUES  (1,53,1,'2011-09-22 14:02:08',NULL,1),
 (2,58,1,'2011-09-22 18:44:59',NULL,1),
 (3,59,1,'2011-09-22 18:45:46',NULL,1);
UNLOCK TABLES;
/*!40000 ALTER TABLE `log_customer` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`log_quote`
--

DROP TABLE IF EXISTS `supernarede`.`log_quote`;
CREATE TABLE  `supernarede`.`log_quote` (
  `quote_id` int(10) unsigned NOT NULL DEFAULT '0',
  `visitor_id` bigint(20) unsigned DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`quote_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Quote log data';

--
-- Dumping data for table `supernarede`.`log_quote`
--

/*!40000 ALTER TABLE `log_quote` DISABLE KEYS */;
LOCK TABLES `log_quote` WRITE;
INSERT INTO `supernarede`.`log_quote` VALUES  (1,22,'2011-08-17 14:32:19',NULL),
 (2,29,'2011-08-18 20:45:57',NULL),
 (3,30,'2011-08-19 12:27:56',NULL),
 (4,31,'2011-08-19 16:01:19',NULL),
 (5,32,'2011-08-19 18:06:37',NULL),
 (6,40,'2011-08-24 12:41:48',NULL);
UNLOCK TABLES;
/*!40000 ALTER TABLE `log_quote` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`log_summary`
--

DROP TABLE IF EXISTS `supernarede`.`log_summary`;
CREATE TABLE  `supernarede`.`log_summary` (
  `summary_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `store_id` smallint(5) unsigned NOT NULL,
  `type_id` smallint(5) unsigned DEFAULT NULL,
  `visitor_count` int(11) NOT NULL DEFAULT '0',
  `customer_count` int(11) NOT NULL DEFAULT '0',
  `add_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`summary_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Summary log information';

--
-- Dumping data for table `supernarede`.`log_summary`
--

/*!40000 ALTER TABLE `log_summary` DISABLE KEYS */;
LOCK TABLES `log_summary` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `log_summary` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`log_summary_type`
--

DROP TABLE IF EXISTS `supernarede`.`log_summary_type`;
CREATE TABLE  `supernarede`.`log_summary_type` (
  `type_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `type_code` varchar(64) NOT NULL DEFAULT '',
  `period` smallint(5) unsigned NOT NULL DEFAULT '0',
  `period_type` enum('MINUTE','HOUR','DAY','WEEK','MONTH') NOT NULL DEFAULT 'MINUTE',
  PRIMARY KEY (`type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='Type of summary information';

--
-- Dumping data for table `supernarede`.`log_summary_type`
--

/*!40000 ALTER TABLE `log_summary_type` DISABLE KEYS */;
LOCK TABLES `log_summary_type` WRITE;
INSERT INTO `supernarede`.`log_summary_type` VALUES  (1,'hour',1,'HOUR'),
 (2,'day',1,'DAY');
UNLOCK TABLES;
/*!40000 ALTER TABLE `log_summary_type` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`log_url`
--

DROP TABLE IF EXISTS `supernarede`.`log_url`;
CREATE TABLE  `supernarede`.`log_url` (
  `url_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `visitor_id` bigint(20) unsigned DEFAULT NULL,
  `visit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`url_id`),
  KEY `IDX_VISITOR` (`visitor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='URL visiting history';

--
-- Dumping data for table `supernarede`.`log_url`
--

/*!40000 ALTER TABLE `log_url` DISABLE KEYS */;
LOCK TABLES `log_url` WRITE;
INSERT INTO `supernarede`.`log_url` VALUES  (1,1,'2011-08-08 12:59:11'),
 (2,2,'2011-08-08 14:38:29'),
 (3,2,'2011-08-08 14:47:01'),
 (4,2,'2011-08-08 14:47:03'),
 (5,2,'2011-08-08 14:47:04'),
 (6,2,'2011-08-08 14:47:05'),
 (7,2,'2011-08-08 14:47:06'),
 (8,2,'2011-08-08 14:47:07'),
 (9,2,'2011-08-08 14:47:08'),
 (10,2,'2011-08-08 14:47:09'),
 (11,2,'2011-08-08 14:47:10'),
 (12,2,'2011-08-08 14:47:11'),
 (13,2,'2011-08-08 14:47:19'),
 (14,2,'2011-08-08 14:47:21'),
 (15,2,'2011-08-08 14:47:21'),
 (16,2,'2011-08-08 14:47:22'),
 (17,2,'2011-08-08 14:47:23'),
 (18,2,'2011-08-08 14:47:23'),
 (19,2,'2011-08-08 14:47:25'),
 (20,2,'2011-08-08 14:47:26'),
 (21,2,'2011-08-08 14:47:26'),
 (22,2,'2011-08-08 14:48:44'),
 (23,2,'2011-08-08 14:49:20'),
 (24,2,'2011-08-08 14:49:21'),
 (25,2,'2011-08-08 14:49:22'),
 (26,2,'2011-08-08 14:49:23'),
 (27,2,'2011-08-08 14:49:23'),
 (28,2,'2011-08-08 14:49:24'),
 (29,2,'2011-08-08 14:49:25'),
 (30,2,'2011-08-08 14:49:26'),
 (31,2,'2011-08-08 14:49:26'),
 (32,2,'2011-08-08 15:03:34'),
 (33,2,'2011-08-08 15:03:36'),
 (34,2,'2011-08-08 15:03:36'),
 (35,2,'2011-08-08 15:03:37'),
 (36,2,'2011-08-08 15:03:37'),
 (37,2,'2011-08-08 15:03:38'),
 (38,2,'2011-08-08 15:03:38'),
 (39,2,'2011-08-08 15:03:39'),
 (40,2,'2011-08-08 15:03:40'),
 (41,3,'2011-08-08 15:07:06'),
 (42,4,'2011-08-08 16:29:42'),
 (43,4,'2011-08-08 16:45:13'),
 (44,4,'2011-08-08 16:46:28'),
 (45,4,'2011-08-08 16:50:54'),
 (46,4,'2011-08-08 16:53:40'),
 (47,4,'2011-08-08 17:01:18'),
 (48,4,'2011-08-08 17:03:35'),
 (49,4,'2011-08-08 17:33:10'),
 (50,4,'2011-08-08 17:35:01'),
 (51,4,'2011-08-08 17:41:15'),
 (52,4,'2011-08-08 17:42:20'),
 (53,4,'2011-08-08 17:44:11'),
 (54,4,'2011-08-08 17:46:36'),
 (55,4,'2011-08-08 17:48:42'),
 (56,4,'2011-08-08 17:48:44'),
 (57,4,'2011-08-08 17:49:36'),
 (58,4,'2011-08-08 17:49:37'),
 (59,4,'2011-08-08 17:50:05'),
 (60,4,'2011-08-08 17:52:15'),
 (61,4,'2011-08-08 17:53:31'),
 (62,4,'2011-08-08 17:55:42'),
 (63,4,'2011-08-08 17:59:31'),
 (64,4,'2011-08-08 18:00:45'),
 (65,4,'2011-08-08 18:16:21'),
 (66,4,'2011-08-08 18:46:52'),
 (67,4,'2011-08-08 19:05:53'),
 (68,4,'2011-08-08 19:31:00'),
 (69,4,'2011-08-08 19:31:52'),
 (70,4,'2011-08-08 19:33:29'),
 (71,4,'2011-08-08 20:00:54'),
 (72,4,'2011-08-08 20:06:59'),
 (73,4,'2011-08-08 20:08:45'),
 (74,4,'2011-08-08 20:28:51'),
 (75,4,'2011-08-08 20:29:38'),
 (76,4,'2011-08-08 20:30:25'),
 (77,4,'2011-08-08 20:30:54'),
 (78,4,'2011-08-08 20:45:34'),
 (79,4,'2011-08-08 20:47:14'),
 (80,4,'2011-08-08 20:47:46'),
 (81,4,'2011-08-08 20:51:58'),
 (82,4,'2011-08-08 20:52:42'),
 (83,4,'2011-08-08 20:57:01'),
 (84,4,'2011-08-08 20:58:14'),
 (85,4,'2011-08-08 21:13:42'),
 (86,4,'2011-08-08 21:14:23'),
 (87,4,'2011-08-08 21:14:48'),
 (88,4,'2011-08-08 21:14:59'),
 (89,4,'2011-08-08 21:15:29'),
 (90,4,'2011-08-08 21:17:47'),
 (91,4,'2011-08-08 21:18:04'),
 (92,5,'2011-08-09 12:20:56'),
 (93,5,'2011-08-09 12:31:32'),
 (94,5,'2011-08-09 12:32:25'),
 (95,5,'2011-08-09 12:32:26'),
 (96,5,'2011-08-09 12:32:29'),
 (97,5,'2011-08-09 12:32:52'),
 (98,5,'2011-08-09 12:33:14'),
 (99,5,'2011-08-09 12:33:33'),
 (100,5,'2011-08-09 13:07:48'),
 (101,5,'2011-08-09 13:07:50'),
 (102,5,'2011-08-09 13:07:51'),
 (103,5,'2011-08-09 13:07:58'),
 (104,5,'2011-08-09 13:08:22'),
 (105,5,'2011-08-09 13:08:25'),
 (106,5,'2011-08-09 13:08:25'),
 (107,5,'2011-08-09 13:08:26'),
 (108,5,'2011-08-09 13:08:26'),
 (109,5,'2011-08-09 13:08:27'),
 (110,5,'2011-08-09 13:08:27'),
 (111,5,'2011-08-09 13:09:13'),
 (112,5,'2011-08-09 13:09:15'),
 (113,5,'2011-08-09 13:11:04'),
 (114,5,'2011-08-09 13:11:06'),
 (115,6,'2011-08-09 14:29:39'),
 (116,6,'2011-08-09 14:29:41'),
 (117,6,'2011-08-09 14:40:14'),
 (118,6,'2011-08-09 14:40:15'),
 (119,6,'2011-08-09 14:41:03'),
 (120,6,'2011-08-09 14:41:04'),
 (121,6,'2011-08-09 14:42:02'),
 (122,6,'2011-08-09 14:42:04'),
 (123,6,'2011-08-09 15:28:50'),
 (124,6,'2011-08-09 15:28:51'),
 (125,7,'2011-08-10 17:56:13'),
 (126,7,'2011-08-10 17:56:18'),
 (127,7,'2011-08-10 17:56:33'),
 (128,7,'2011-08-10 17:56:35'),
 (129,7,'2011-08-10 18:06:41'),
 (130,7,'2011-08-10 18:06:45'),
 (131,7,'2011-08-10 18:20:48'),
 (132,7,'2011-08-10 18:22:27'),
 (133,7,'2011-08-10 18:23:09'),
 (134,11,'2011-08-11 13:17:13'),
 (135,11,'2011-08-11 13:17:15'),
 (136,11,'2011-08-11 13:17:16'),
 (137,11,'2011-08-11 13:19:26'),
 (138,11,'2011-08-11 13:19:28'),
 (139,11,'2011-08-11 13:19:30'),
 (140,11,'2011-08-11 13:19:37'),
 (141,11,'2011-08-11 13:19:40'),
 (142,11,'2011-08-11 13:19:40'),
 (143,11,'2011-08-11 13:20:31'),
 (144,11,'2011-08-11 13:20:33'),
 (145,11,'2011-08-11 13:20:34'),
 (146,11,'2011-08-11 13:20:39'),
 (147,11,'2011-08-11 13:20:41'),
 (148,11,'2011-08-11 13:20:42'),
 (149,11,'2011-08-11 13:20:46'),
 (150,11,'2011-08-11 13:20:47'),
 (151,11,'2011-08-11 13:20:48'),
 (152,11,'2011-08-11 13:22:05'),
 (153,11,'2011-08-11 13:22:10'),
 (154,11,'2011-08-11 13:22:30'),
 (155,11,'2011-08-11 13:22:31'),
 (156,11,'2011-08-11 13:22:32'),
 (157,11,'2011-08-11 13:25:43'),
 (158,11,'2011-08-11 13:25:45'),
 (159,11,'2011-08-11 13:25:45'),
 (160,12,'2011-08-11 17:28:29'),
 (161,12,'2011-08-11 17:28:31'),
 (162,12,'2011-08-11 17:28:34'),
 (163,12,'2011-08-11 17:34:26'),
 (164,12,'2011-08-11 17:34:27'),
 (165,12,'2011-08-11 17:34:28'),
 (166,12,'2011-08-11 17:34:36'),
 (167,12,'2011-08-11 17:34:37'),
 (168,12,'2011-08-11 17:34:38'),
 (169,12,'2011-08-11 17:39:34'),
 (170,12,'2011-08-11 17:39:35'),
 (171,12,'2011-08-11 17:39:36'),
 (172,12,'2011-08-11 17:39:42'),
 (173,12,'2011-08-11 17:39:45'),
 (174,12,'2011-08-11 17:39:45'),
 (175,12,'2011-08-11 17:40:05'),
 (176,12,'2011-08-11 17:40:06'),
 (177,12,'2011-08-11 17:40:07'),
 (178,12,'2011-08-11 17:45:48'),
 (179,12,'2011-08-11 17:45:49'),
 (180,12,'2011-08-11 17:45:50'),
 (181,12,'2011-08-11 17:52:22'),
 (182,12,'2011-08-11 17:52:26'),
 (183,12,'2011-08-11 17:52:27'),
 (184,12,'2011-08-11 17:53:14'),
 (185,12,'2011-08-11 17:53:15'),
 (186,12,'2011-08-11 17:53:16'),
 (187,12,'2011-08-11 17:54:13'),
 (188,12,'2011-08-11 17:54:15'),
 (189,12,'2011-08-11 17:54:16'),
 (190,12,'2011-08-11 17:58:34'),
 (191,12,'2011-08-11 17:58:36'),
 (192,12,'2011-08-11 17:58:37'),
 (193,12,'2011-08-11 18:04:23'),
 (194,12,'2011-08-11 18:04:24'),
 (195,12,'2011-08-11 18:04:25'),
 (196,12,'2011-08-11 18:06:07'),
 (197,12,'2011-08-11 18:06:09'),
 (198,12,'2011-08-11 18:06:10'),
 (199,12,'2011-08-11 18:06:54'),
 (200,12,'2011-08-11 18:07:09'),
 (201,12,'2011-08-11 18:07:11'),
 (202,12,'2011-08-11 18:07:12'),
 (203,12,'2011-08-11 18:10:54'),
 (204,12,'2011-08-11 18:10:55'),
 (205,12,'2011-08-11 18:10:56'),
 (206,12,'2011-08-11 18:12:15'),
 (207,12,'2011-08-11 18:12:16'),
 (208,12,'2011-08-11 18:12:17'),
 (209,12,'2011-08-11 18:12:38'),
 (210,12,'2011-08-11 18:12:40'),
 (211,12,'2011-08-11 18:12:40'),
 (212,13,'2011-08-12 12:35:18'),
 (213,13,'2011-08-12 12:35:20'),
 (214,13,'2011-08-12 12:35:20'),
 (215,14,'2011-08-12 18:29:04'),
 (216,14,'2011-08-12 18:29:06'),
 (217,14,'2011-08-12 18:29:07'),
 (218,14,'2011-08-12 18:30:20'),
 (219,14,'2011-08-12 18:30:22'),
 (220,14,'2011-08-12 18:30:23'),
 (221,14,'2011-08-12 18:30:49'),
 (222,14,'2011-08-12 18:30:55'),
 (223,14,'2011-08-12 18:30:56'),
 (224,14,'2011-08-12 18:31:37'),
 (225,14,'2011-08-12 18:31:38'),
 (226,14,'2011-08-12 18:31:39'),
 (227,14,'2011-08-12 18:48:10'),
 (228,14,'2011-08-12 18:48:12'),
 (229,14,'2011-08-12 18:48:13'),
 (230,14,'2011-08-12 18:48:36'),
 (231,14,'2011-08-12 18:48:38'),
 (232,14,'2011-08-12 18:48:41'),
 (233,14,'2011-08-12 18:58:24'),
 (234,14,'2011-08-12 18:58:25'),
 (235,14,'2011-08-12 18:58:26'),
 (236,14,'2011-08-12 19:09:16'),
 (237,14,'2011-08-12 19:09:18'),
 (238,14,'2011-08-12 19:09:19'),
 (239,14,'2011-08-12 19:20:32'),
 (240,14,'2011-08-12 19:26:54'),
 (241,14,'2011-08-12 19:27:25'),
 (242,14,'2011-08-12 19:28:49'),
 (243,14,'2011-08-12 19:29:11'),
 (244,14,'2011-08-12 19:30:54'),
 (245,14,'2011-08-12 19:32:41'),
 (246,14,'2011-08-12 19:36:02'),
 (247,14,'2011-08-12 19:40:55'),
 (248,14,'2011-08-12 19:41:14'),
 (249,14,'2011-08-12 19:42:17'),
 (250,14,'2011-08-12 19:42:51'),
 (251,14,'2011-08-12 19:47:18'),
 (252,14,'2011-08-12 19:48:11'),
 (253,14,'2011-08-12 19:49:07'),
 (254,14,'2011-08-12 19:49:56'),
 (255,14,'2011-08-12 20:15:52'),
 (256,14,'2011-08-12 20:16:35'),
 (257,14,'2011-08-12 20:16:46'),
 (258,14,'2011-08-12 20:18:16'),
 (259,14,'2011-08-12 20:23:19'),
 (260,14,'2011-08-12 20:25:43'),
 (261,14,'2011-08-12 20:36:06'),
 (262,14,'2011-08-12 20:36:33'),
 (263,14,'2011-08-12 20:38:05'),
 (264,14,'2011-08-12 20:39:14'),
 (265,14,'2011-08-12 20:39:26'),
 (266,14,'2011-08-12 20:45:09'),
 (267,14,'2011-08-12 20:46:41'),
 (268,14,'2011-08-12 20:47:56'),
 (269,14,'2011-08-12 20:48:35'),
 (270,14,'2011-08-12 20:49:37'),
 (271,14,'2011-08-12 20:51:34'),
 (272,14,'2011-08-12 20:53:00'),
 (273,14,'2011-08-12 20:54:22'),
 (274,14,'2011-08-12 20:54:57'),
 (275,14,'2011-08-12 20:57:16'),
 (276,14,'2011-08-12 21:07:20'),
 (277,14,'2011-08-12 21:12:15'),
 (278,14,'2011-08-12 21:13:31'),
 (279,14,'2011-08-12 21:18:19'),
 (280,14,'2011-08-12 21:19:10'),
 (281,14,'2011-08-12 21:20:32'),
 (282,14,'2011-08-12 21:21:19'),
 (283,14,'2011-08-12 21:22:53'),
 (284,14,'2011-08-12 21:23:27'),
 (285,14,'2011-08-12 21:24:14'),
 (286,14,'2011-08-12 21:37:21'),
 (287,14,'2011-08-12 21:39:02'),
 (288,14,'2011-08-12 21:39:19'),
 (289,14,'2011-08-12 21:39:36'),
 (290,14,'2011-08-12 21:39:45'),
 (291,14,'2011-08-12 21:39:55'),
 (292,15,'2011-08-16 12:49:30'),
 (293,15,'2011-08-16 13:12:32'),
 (294,15,'2011-08-16 13:13:05'),
 (295,15,'2011-08-16 13:13:59'),
 (296,15,'2011-08-16 13:16:02'),
 (297,15,'2011-08-16 13:16:15'),
 (298,15,'2011-08-16 13:21:30'),
 (299,15,'2011-08-16 13:22:41'),
 (300,15,'2011-08-16 13:23:42'),
 (301,15,'2011-08-16 13:37:29'),
 (302,15,'2011-08-16 13:38:18'),
 (303,15,'2011-08-16 13:38:29'),
 (304,15,'2011-08-16 13:38:44'),
 (305,15,'2011-08-16 14:00:49'),
 (306,15,'2011-08-16 14:02:09'),
 (307,15,'2011-08-16 14:02:36'),
 (308,15,'2011-08-16 14:17:47'),
 (309,15,'2011-08-16 14:17:49'),
 (310,15,'2011-08-16 14:18:07'),
 (311,15,'2011-08-16 14:19:16'),
 (312,15,'2011-08-16 14:19:20'),
 (313,15,'2011-08-16 14:19:21'),
 (314,15,'2011-08-16 14:30:10'),
 (315,15,'2011-08-16 14:31:25'),
 (316,15,'2011-08-16 14:31:26'),
 (317,15,'2011-08-16 14:41:04'),
 (318,15,'2011-08-16 14:57:45'),
 (319,15,'2011-08-16 15:04:57'),
 (320,15,'2011-08-16 15:04:58'),
 (321,15,'2011-08-16 15:07:56'),
 (322,15,'2011-08-16 15:08:47'),
 (323,15,'2011-08-16 15:09:31'),
 (324,15,'2011-08-16 15:10:00'),
 (325,15,'2011-08-16 15:11:55'),
 (326,15,'2011-08-16 15:15:04'),
 (327,15,'2011-08-16 15:41:34'),
 (328,15,'2011-08-16 15:41:58'),
 (329,15,'2011-08-16 15:42:14'),
 (330,15,'2011-08-16 15:42:24'),
 (331,15,'2011-08-16 15:43:00'),
 (332,15,'2011-08-16 15:43:32'),
 (333,15,'2011-08-16 15:44:29'),
 (334,15,'2011-08-16 15:44:53'),
 (335,15,'2011-08-16 15:45:32'),
 (336,15,'2011-08-16 15:46:01'),
 (337,15,'2011-08-16 15:46:35'),
 (338,15,'2011-08-16 15:47:40'),
 (339,19,'2011-08-16 16:57:58'),
 (340,19,'2011-08-16 17:06:06'),
 (341,19,'2011-08-16 17:06:19'),
 (342,19,'2011-08-16 17:06:53'),
 (343,19,'2011-08-16 18:02:37'),
 (344,19,'2011-08-16 18:08:19'),
 (345,19,'2011-08-16 18:09:46'),
 (346,19,'2011-08-16 18:31:38'),
 (347,19,'2011-08-16 18:31:54'),
 (348,19,'2011-08-16 19:27:19'),
 (349,19,'2011-08-16 19:27:41'),
 (350,19,'2011-08-16 19:32:24'),
 (351,19,'2011-08-16 19:33:16'),
 (352,19,'2011-08-16 19:34:26'),
 (353,19,'2011-08-16 19:36:07'),
 (354,19,'2011-08-16 19:38:35'),
 (355,19,'2011-08-16 19:38:51'),
 (356,19,'2011-08-16 19:46:47'),
 (357,19,'2011-08-16 20:00:08'),
 (358,19,'2011-08-16 20:13:39'),
 (359,19,'2011-08-16 20:14:38'),
 (360,19,'2011-08-16 20:20:05'),
 (361,19,'2011-08-16 20:20:55'),
 (362,19,'2011-08-16 20:22:02'),
 (363,19,'2011-08-16 20:23:08'),
 (364,19,'2011-08-16 20:25:47'),
 (365,19,'2011-08-16 20:29:11'),
 (366,19,'2011-08-16 20:31:20'),
 (367,19,'2011-08-16 20:57:04'),
 (368,20,'2011-08-17 12:21:04'),
 (369,22,'2011-08-17 14:08:48'),
 (370,22,'2011-08-17 14:08:52'),
 (371,22,'2011-08-17 14:08:54'),
 (372,22,'2011-08-17 14:10:48'),
 (373,22,'2011-08-17 14:11:44'),
 (374,22,'2011-08-17 14:13:01'),
 (375,22,'2011-08-17 14:15:27'),
 (376,22,'2011-08-17 14:15:34'),
 (377,22,'2011-08-17 14:19:47'),
 (378,22,'2011-08-17 14:22:40'),
 (379,22,'2011-08-17 14:32:04'),
 (380,22,'2011-08-17 14:32:19'),
 (381,22,'2011-08-17 14:32:24'),
 (382,22,'2011-08-17 14:32:31'),
 (383,22,'2011-08-17 14:32:36'),
 (384,22,'2011-08-17 14:33:28'),
 (385,22,'2011-08-17 14:34:14'),
 (386,22,'2011-08-17 14:34:54'),
 (387,22,'2011-08-17 14:39:42'),
 (388,22,'2011-08-17 14:39:59'),
 (389,22,'2011-08-17 14:52:22'),
 (390,22,'2011-08-17 14:52:45'),
 (391,22,'2011-08-17 14:53:15'),
 (392,22,'2011-08-17 14:53:28'),
 (393,22,'2011-08-17 14:54:02'),
 (394,22,'2011-08-17 14:54:04'),
 (395,22,'2011-08-17 14:54:14'),
 (396,22,'2011-08-17 14:54:31'),
 (397,22,'2011-08-17 14:58:38'),
 (398,22,'2011-08-17 14:59:47'),
 (399,22,'2011-08-17 15:02:12'),
 (400,22,'2011-08-17 15:03:48'),
 (401,22,'2011-08-17 15:04:36'),
 (402,22,'2011-08-17 15:04:52'),
 (403,22,'2011-08-17 15:05:08'),
 (404,22,'2011-08-17 15:05:18'),
 (405,22,'2011-08-17 15:05:21'),
 (406,22,'2011-08-17 15:05:29'),
 (407,22,'2011-08-17 15:05:34'),
 (408,22,'2011-08-17 15:05:37'),
 (409,22,'2011-08-17 15:05:41'),
 (410,22,'2011-08-17 15:07:18'),
 (411,22,'2011-08-17 15:14:46'),
 (412,22,'2011-08-17 15:19:39'),
 (413,22,'2011-08-17 15:24:47'),
 (414,22,'2011-08-17 15:24:55'),
 (415,22,'2011-08-17 16:20:35'),
 (416,22,'2011-08-17 16:22:40'),
 (417,23,'2011-08-17 19:31:33'),
 (418,23,'2011-08-17 19:34:02'),
 (419,23,'2011-08-17 19:34:07'),
 (420,23,'2011-08-17 19:34:20'),
 (421,23,'2011-08-17 19:37:53'),
 (422,23,'2011-08-17 19:52:48'),
 (423,23,'2011-08-17 19:53:23'),
 (424,23,'2011-08-17 20:04:43'),
 (425,23,'2011-08-17 20:04:54'),
 (426,23,'2011-08-17 20:08:15'),
 (427,23,'2011-08-17 20:25:47'),
 (428,23,'2011-08-17 20:38:06'),
 (429,23,'2011-08-17 20:38:15'),
 (430,23,'2011-08-17 20:38:18'),
 (431,23,'2011-08-17 20:38:21'),
 (432,23,'2011-08-17 20:38:30'),
 (433,23,'2011-08-17 20:38:49'),
 (434,23,'2011-08-17 20:38:53'),
 (435,23,'2011-08-17 20:38:55'),
 (436,23,'2011-08-17 20:38:56'),
 (437,23,'2011-08-17 20:39:26'),
 (438,23,'2011-08-17 20:39:30'),
 (439,23,'2011-08-17 20:39:32'),
 (440,23,'2011-08-17 20:39:34'),
 (441,23,'2011-08-17 20:39:36'),
 (442,23,'2011-08-17 20:39:54'),
 (443,23,'2011-08-17 20:40:00'),
 (444,23,'2011-08-17 20:40:04'),
 (445,23,'2011-08-17 20:40:06'),
 (446,23,'2011-08-17 20:40:10'),
 (447,23,'2011-08-17 20:40:57'),
 (448,23,'2011-08-17 20:40:59'),
 (449,23,'2011-08-17 20:41:00'),
 (450,23,'2011-08-17 20:41:01'),
 (451,23,'2011-08-17 20:41:46'),
 (452,23,'2011-08-17 20:44:10'),
 (453,23,'2011-08-17 20:44:15'),
 (454,23,'2011-08-17 20:44:16'),
 (455,23,'2011-08-17 20:44:21'),
 (456,26,'2011-08-17 21:02:39'),
 (457,26,'2011-08-17 21:02:48'),
 (458,26,'2011-08-17 21:02:49'),
 (459,26,'2011-08-17 21:02:50'),
 (460,23,'2011-08-17 21:14:20'),
 (461,23,'2011-08-17 21:19:26'),
 (462,27,'2011-08-18 12:28:58'),
 (463,27,'2011-08-18 12:29:22'),
 (464,27,'2011-08-18 12:31:32'),
 (465,27,'2011-08-18 12:31:35'),
 (466,27,'2011-08-18 12:31:38'),
 (467,27,'2011-08-18 12:31:40'),
 (468,27,'2011-08-18 12:31:41'),
 (469,27,'2011-08-18 12:31:42'),
 (470,27,'2011-08-18 12:31:44'),
 (471,27,'2011-08-18 12:31:46'),
 (472,27,'2011-08-18 12:31:46'),
 (473,27,'2011-08-18 12:31:48'),
 (474,27,'2011-08-18 12:32:17'),
 (475,27,'2011-08-18 12:37:13'),
 (476,27,'2011-08-18 12:44:02'),
 (477,28,'2011-08-18 17:12:36'),
 (478,29,'2011-08-18 19:35:35'),
 (479,29,'2011-08-18 19:35:56'),
 (480,29,'2011-08-18 19:36:22'),
 (481,29,'2011-08-18 19:36:40'),
 (482,29,'2011-08-18 19:43:01'),
 (483,29,'2011-08-18 19:46:19'),
 (484,29,'2011-08-18 19:47:43'),
 (485,29,'2011-08-18 19:55:03'),
 (486,29,'2011-08-18 19:55:12'),
 (487,29,'2011-08-18 19:56:55'),
 (488,29,'2011-08-18 20:01:17'),
 (489,29,'2011-08-18 20:01:28'),
 (490,29,'2011-08-18 20:01:33'),
 (491,29,'2011-08-18 20:02:46'),
 (492,29,'2011-08-18 20:04:44'),
 (493,29,'2011-08-18 20:06:37'),
 (494,29,'2011-08-18 20:09:02'),
 (495,29,'2011-08-18 20:09:21'),
 (496,29,'2011-08-18 20:09:50'),
 (497,29,'2011-08-18 20:10:00'),
 (498,29,'2011-08-18 20:10:15'),
 (499,29,'2011-08-18 20:10:23'),
 (500,29,'2011-08-18 20:10:29'),
 (501,29,'2011-08-18 20:11:50'),
 (502,29,'2011-08-18 20:12:00'),
 (503,29,'2011-08-18 20:14:51'),
 (504,29,'2011-08-18 20:15:40'),
 (505,29,'2011-08-18 20:18:04'),
 (506,29,'2011-08-18 20:18:58'),
 (507,29,'2011-08-18 20:20:09'),
 (508,29,'2011-08-18 20:21:58'),
 (509,29,'2011-08-18 20:23:01'),
 (510,29,'2011-08-18 20:23:24'),
 (511,29,'2011-08-18 20:23:45'),
 (512,29,'2011-08-18 20:33:23'),
 (513,29,'2011-08-18 20:34:32'),
 (514,29,'2011-08-18 20:35:10'),
 (515,29,'2011-08-18 20:36:44'),
 (516,29,'2011-08-18 20:37:39'),
 (517,29,'2011-08-18 20:38:29'),
 (518,29,'2011-08-18 20:38:53'),
 (519,29,'2011-08-18 20:42:20'),
 (520,29,'2011-08-18 20:42:28'),
 (521,29,'2011-08-18 20:42:41'),
 (522,29,'2011-08-18 20:42:48'),
 (523,29,'2011-08-18 20:44:01'),
 (524,29,'2011-08-18 20:44:49'),
 (525,29,'2011-08-18 20:44:53'),
 (526,29,'2011-08-18 20:45:19'),
 (527,29,'2011-08-18 20:45:50'),
 (528,29,'2011-08-18 20:45:57'),
 (529,29,'2011-08-18 20:47:13'),
 (530,29,'2011-08-18 21:07:50'),
 (531,29,'2011-08-18 21:12:53'),
 (532,29,'2011-08-18 21:14:33'),
 (533,29,'2011-08-18 21:15:16'),
 (534,29,'2011-08-18 21:15:35'),
 (535,29,'2011-08-18 21:17:01'),
 (536,29,'2011-08-18 21:18:00'),
 (537,29,'2011-08-18 21:18:38'),
 (538,29,'2011-08-18 21:19:54'),
 (539,29,'2011-08-18 21:23:13'),
 (540,29,'2011-08-18 21:24:23'),
 (541,29,'2011-08-18 21:27:58'),
 (542,29,'2011-08-18 21:28:14'),
 (543,29,'2011-08-18 21:28:21'),
 (544,29,'2011-08-18 21:28:23'),
 (545,29,'2011-08-18 21:28:23'),
 (546,29,'2011-08-18 21:28:25'),
 (547,29,'2011-08-18 21:33:41'),
 (548,29,'2011-08-18 21:33:42'),
 (549,29,'2011-08-18 21:33:43'),
 (550,29,'2011-08-18 21:34:51'),
 (551,29,'2011-08-18 21:34:53'),
 (552,29,'2011-08-18 21:34:54'),
 (553,29,'2011-08-18 21:34:54'),
 (554,29,'2011-08-18 21:37:53'),
 (555,29,'2011-08-18 21:37:55'),
 (556,29,'2011-08-18 21:37:56'),
 (557,29,'2011-08-18 21:37:57'),
 (558,29,'2011-08-18 21:38:58'),
 (559,29,'2011-08-18 21:39:00'),
 (560,29,'2011-08-18 21:39:01'),
 (561,29,'2011-08-18 21:39:02'),
 (562,29,'2011-08-18 21:39:30'),
 (563,29,'2011-08-18 21:39:32'),
 (564,29,'2011-08-18 21:39:33'),
 (565,29,'2011-08-18 21:39:34'),
 (566,29,'2011-08-18 21:40:19'),
 (567,29,'2011-08-18 21:40:21'),
 (568,29,'2011-08-18 21:40:22'),
 (569,29,'2011-08-18 21:40:23'),
 (570,29,'2011-08-18 21:45:02'),
 (571,29,'2011-08-18 21:45:04'),
 (572,29,'2011-08-18 21:48:06'),
 (573,29,'2011-08-18 21:48:40'),
 (574,29,'2011-08-18 21:48:41'),
 (575,29,'2011-08-18 21:48:47'),
 (576,29,'2011-08-18 21:48:48'),
 (577,29,'2011-08-18 21:50:11'),
 (578,29,'2011-08-18 21:54:29'),
 (579,29,'2011-08-18 21:56:13'),
 (580,29,'2011-08-18 21:59:39'),
 (581,29,'2011-08-18 22:00:25'),
 (582,29,'2011-08-18 22:00:38'),
 (583,29,'2011-08-18 22:01:20'),
 (584,29,'2011-08-18 22:01:57'),
 (585,30,'2011-08-19 12:14:40'),
 (586,30,'2011-08-19 12:27:32'),
 (587,30,'2011-08-19 12:27:46'),
 (588,30,'2011-08-19 12:27:56'),
 (589,30,'2011-08-19 12:28:02'),
 (590,30,'2011-08-19 12:35:16'),
 (591,30,'2011-08-19 12:35:48'),
 (592,30,'2011-08-19 12:36:03'),
 (593,30,'2011-08-19 12:36:39'),
 (594,30,'2011-08-19 12:39:29'),
 (595,30,'2011-08-19 12:48:20'),
 (596,30,'2011-08-19 12:48:41'),
 (597,30,'2011-08-19 12:49:16'),
 (598,30,'2011-08-19 12:49:25'),
 (599,30,'2011-08-19 12:49:30'),
 (600,30,'2011-08-19 12:51:53'),
 (601,30,'2011-08-19 12:54:20'),
 (602,30,'2011-08-19 12:54:59'),
 (603,30,'2011-08-19 12:55:34'),
 (604,30,'2011-08-19 13:10:23'),
 (605,30,'2011-08-19 13:10:36'),
 (606,30,'2011-08-19 13:11:10'),
 (607,30,'2011-08-19 13:11:22'),
 (608,30,'2011-08-19 13:11:38'),
 (609,30,'2011-08-19 13:11:54'),
 (610,30,'2011-08-19 13:14:21'),
 (611,30,'2011-08-19 13:16:54'),
 (612,30,'2011-08-19 13:23:28'),
 (613,30,'2011-08-19 13:23:44'),
 (614,30,'2011-08-19 13:23:57'),
 (615,30,'2011-08-19 13:24:02'),
 (616,30,'2011-08-19 13:24:05'),
 (617,30,'2011-08-19 13:24:55'),
 (618,30,'2011-08-19 13:25:02'),
 (619,30,'2011-08-19 13:25:12'),
 (620,30,'2011-08-19 13:25:17'),
 (621,30,'2011-08-19 13:29:46'),
 (622,30,'2011-08-19 13:29:57'),
 (623,30,'2011-08-19 13:33:57'),
 (624,30,'2011-08-19 13:36:10'),
 (625,30,'2011-08-19 13:37:28'),
 (626,30,'2011-08-19 13:38:55'),
 (627,30,'2011-08-19 13:40:35'),
 (628,30,'2011-08-19 13:40:51'),
 (629,30,'2011-08-19 13:47:43'),
 (630,30,'2011-08-19 13:47:56'),
 (631,30,'2011-08-19 13:48:00'),
 (632,30,'2011-08-19 13:48:48'),
 (633,30,'2011-08-19 13:50:57'),
 (634,30,'2011-08-19 13:52:15'),
 (635,30,'2011-08-19 13:53:34'),
 (636,30,'2011-08-19 13:53:40'),
 (637,30,'2011-08-19 13:55:23'),
 (638,30,'2011-08-19 13:55:51'),
 (639,31,'2011-08-19 15:54:51'),
 (640,31,'2011-08-19 15:56:13'),
 (641,31,'2011-08-19 15:58:21'),
 (642,31,'2011-08-19 15:58:35'),
 (643,31,'2011-08-19 15:59:04'),
 (644,31,'2011-08-19 16:00:11'),
 (645,31,'2011-08-19 16:00:14'),
 (646,31,'2011-08-19 16:00:49'),
 (647,31,'2011-08-19 16:01:00'),
 (648,31,'2011-08-19 16:01:04'),
 (649,31,'2011-08-19 16:01:19'),
 (650,31,'2011-08-19 16:14:56'),
 (651,31,'2011-08-19 16:15:02'),
 (652,31,'2011-08-19 16:15:52'),
 (653,31,'2011-08-19 16:18:24'),
 (654,31,'2011-08-19 16:18:31'),
 (655,31,'2011-08-19 16:21:51'),
 (656,31,'2011-08-19 16:22:21'),
 (657,31,'2011-08-19 16:23:06'),
 (658,31,'2011-08-19 16:23:33'),
 (659,31,'2011-08-19 16:24:19'),
 (660,31,'2011-08-19 16:24:27'),
 (661,31,'2011-08-19 16:27:17'),
 (662,31,'2011-08-19 16:27:22'),
 (663,31,'2011-08-19 16:27:27'),
 (664,31,'2011-08-19 16:27:34'),
 (665,31,'2011-08-19 16:27:40'),
 (666,31,'2011-08-19 16:27:49'),
 (667,31,'2011-08-19 16:29:43'),
 (668,31,'2011-08-19 16:32:01'),
 (669,31,'2011-08-19 16:34:59'),
 (670,31,'2011-08-19 16:37:03'),
 (671,31,'2011-08-19 16:38:04'),
 (672,31,'2011-08-19 16:40:26'),
 (673,31,'2011-08-19 16:40:42'),
 (674,31,'2011-08-19 16:40:44'),
 (675,31,'2011-08-19 16:43:25'),
 (676,31,'2011-08-19 16:46:14'),
 (677,31,'2011-08-19 16:46:25'),
 (678,31,'2011-08-19 16:46:26'),
 (679,31,'2011-08-19 16:51:24'),
 (680,31,'2011-08-19 16:51:38'),
 (681,31,'2011-08-19 16:51:44'),
 (682,31,'2011-08-19 16:56:28'),
 (683,31,'2011-08-19 16:56:35'),
 (684,31,'2011-08-19 16:58:20'),
 (685,31,'2011-08-19 17:04:20'),
 (686,31,'2011-08-19 17:10:05'),
 (687,31,'2011-08-19 17:10:43'),
 (688,31,'2011-08-19 17:11:45'),
 (689,31,'2011-08-19 17:12:09'),
 (690,31,'2011-08-19 17:13:48'),
 (691,31,'2011-08-19 17:14:37'),
 (692,31,'2011-08-19 17:15:57'),
 (693,31,'2011-08-19 17:18:03'),
 (694,31,'2011-08-19 17:18:11'),
 (695,31,'2011-08-19 17:18:19'),
 (696,31,'2011-08-19 17:19:17'),
 (697,31,'2011-08-19 17:23:06'),
 (698,31,'2011-08-19 17:23:46'),
 (699,31,'2011-08-19 17:30:26'),
 (700,31,'2011-08-19 17:50:55'),
 (701,31,'2011-08-19 17:52:06'),
 (702,31,'2011-08-19 17:53:53'),
 (703,31,'2011-08-19 17:53:57'),
 (704,31,'2011-08-19 17:54:17'),
 (705,31,'2011-08-19 17:54:19'),
 (706,31,'2011-08-19 17:54:33'),
 (707,31,'2011-08-19 17:56:12'),
 (708,31,'2011-08-19 18:02:53'),
 (709,31,'2011-08-19 18:03:01'),
 (710,31,'2011-08-19 18:05:58'),
 (711,32,'2011-08-19 18:06:08'),
 (712,32,'2011-08-19 18:06:10'),
 (713,32,'2011-08-19 18:06:25'),
 (714,32,'2011-08-19 18:06:37'),
 (715,32,'2011-08-19 18:06:41'),
 (716,32,'2011-08-19 18:06:50'),
 (717,32,'2011-08-19 18:06:57'),
 (718,31,'2011-08-19 18:08:01'),
 (719,31,'2011-08-19 18:17:17'),
 (720,31,'2011-08-19 18:21:48'),
 (721,31,'2011-08-19 18:25:31'),
 (722,31,'2011-08-19 18:25:44'),
 (723,31,'2011-08-19 18:26:19'),
 (724,31,'2011-08-19 18:26:22'),
 (725,31,'2011-08-19 18:28:36'),
 (726,31,'2011-08-19 18:28:40'),
 (727,31,'2011-08-19 18:32:12'),
 (728,31,'2011-08-19 18:32:19'),
 (729,31,'2011-08-19 18:54:00'),
 (730,31,'2011-08-19 18:54:21'),
 (731,31,'2011-08-19 18:56:36'),
 (732,31,'2011-08-19 19:23:28'),
 (733,33,'2011-08-19 20:45:29'),
 (734,33,'2011-08-19 20:45:41'),
 (735,33,'2011-08-19 20:46:05'),
 (736,33,'2011-08-19 20:59:31'),
 (737,33,'2011-08-19 21:07:20'),
 (738,34,'2011-08-22 12:57:23'),
 (739,35,'2011-08-22 15:37:10'),
 (740,35,'2011-08-22 15:37:53'),
 (741,35,'2011-08-22 15:46:23'),
 (742,35,'2011-08-22 15:46:37'),
 (743,35,'2011-08-22 15:48:16'),
 (744,35,'2011-08-22 15:48:23'),
 (745,35,'2011-08-22 15:49:24'),
 (746,35,'2011-08-22 16:39:14'),
 (747,35,'2011-08-22 16:39:46'),
 (748,35,'2011-08-22 16:39:51'),
 (749,36,'2011-08-23 12:27:45'),
 (750,37,'2011-08-23 15:28:42'),
 (751,37,'2011-08-23 15:30:51'),
 (752,37,'2011-08-23 15:31:49'),
 (753,37,'2011-08-23 15:34:24'),
 (754,37,'2011-08-23 15:34:35'),
 (755,37,'2011-08-23 15:36:24'),
 (756,37,'2011-08-23 15:36:32'),
 (757,37,'2011-08-23 15:36:45'),
 (758,37,'2011-08-23 15:40:58'),
 (759,38,'2011-08-23 15:41:18'),
 (760,38,'2011-08-23 15:41:35'),
 (761,38,'2011-08-23 15:41:48'),
 (762,38,'2011-08-23 15:43:08'),
 (763,38,'2011-08-23 15:43:13'),
 (764,38,'2011-08-23 15:43:39'),
 (765,38,'2011-08-23 15:43:43'),
 (766,37,'2011-08-23 15:44:06'),
 (767,37,'2011-08-23 15:44:11'),
 (768,37,'2011-08-23 15:44:26'),
 (769,37,'2011-08-23 15:44:31'),
 (770,39,'2011-08-23 17:27:37'),
 (771,40,'2011-08-24 12:17:13'),
 (772,40,'2011-08-24 12:35:35'),
 (773,40,'2011-08-24 12:35:51'),
 (774,40,'2011-08-24 12:39:02'),
 (775,40,'2011-08-24 12:41:48'),
 (776,40,'2011-08-24 12:43:31'),
 (777,40,'2011-08-24 12:46:20'),
 (778,40,'2011-08-24 12:48:15'),
 (779,40,'2011-08-24 12:49:56'),
 (780,40,'2011-08-24 12:50:46'),
 (781,40,'2011-08-24 13:45:56'),
 (782,40,'2011-08-24 13:46:53'),
 (783,40,'2011-08-24 13:50:19'),
 (784,40,'2011-08-24 13:56:01'),
 (785,40,'2011-08-24 13:56:05'),
 (786,41,'2011-08-25 13:01:39'),
 (787,41,'2011-08-25 13:03:38'),
 (788,42,'2011-09-15 19:59:21'),
 (789,42,'2011-09-15 20:13:02'),
 (790,42,'2011-09-15 20:15:53'),
 (791,42,'2011-09-15 20:16:16'),
 (792,42,'2011-09-15 20:17:55'),
 (793,42,'2011-09-15 20:19:06'),
 (794,42,'2011-09-15 20:19:39'),
 (795,42,'2011-09-15 20:19:46'),
 (796,42,'2011-09-15 20:22:07'),
 (797,42,'2011-09-15 20:22:27'),
 (798,42,'2011-09-15 20:23:03'),
 (799,42,'2011-09-15 20:23:13'),
 (800,42,'2011-09-15 20:23:45'),
 (801,42,'2011-09-15 20:25:18'),
 (802,42,'2011-09-15 20:34:40'),
 (803,42,'2011-09-15 20:34:48'),
 (804,42,'2011-09-15 20:35:54'),
 (805,42,'2011-09-15 20:36:03'),
 (806,42,'2011-09-15 20:39:12'),
 (807,42,'2011-09-15 20:42:21'),
 (808,42,'2011-09-15 20:44:04'),
 (809,42,'2011-09-15 20:44:39'),
 (810,42,'2011-09-15 20:44:55'),
 (811,43,'2011-09-16 12:20:42'),
 (812,44,'2011-09-16 12:20:44'),
 (813,44,'2011-09-16 12:31:40'),
 (814,44,'2011-09-16 12:32:28'),
 (815,44,'2011-09-16 12:32:35'),
 (816,44,'2011-09-16 12:42:38'),
 (817,44,'2011-09-16 12:43:01'),
 (818,44,'2011-09-16 12:49:06'),
 (819,44,'2011-09-16 12:49:19'),
 (820,44,'2011-09-16 12:52:06'),
 (821,44,'2011-09-16 12:52:55'),
 (822,44,'2011-09-16 12:52:58'),
 (823,44,'2011-09-16 13:04:04'),
 (824,44,'2011-09-16 13:04:25'),
 (825,44,'2011-09-16 13:09:53'),
 (826,44,'2011-09-16 13:29:13'),
 (827,44,'2011-09-16 13:29:45'),
 (828,44,'2011-09-16 13:29:52'),
 (829,44,'2011-09-16 13:30:05'),
 (830,45,'2011-09-19 17:56:43'),
 (831,45,'2011-09-19 17:56:46'),
 (832,46,'2011-09-19 17:58:31'),
 (833,46,'2011-09-19 17:58:32'),
 (834,45,'2011-09-19 17:58:35'),
 (835,45,'2011-09-19 17:58:37'),
 (836,46,'2011-09-19 17:59:22'),
 (837,46,'2011-09-19 17:59:24'),
 (838,46,'2011-09-19 17:59:37'),
 (839,46,'2011-09-19 17:59:39'),
 (840,46,'2011-09-19 17:59:42'),
 (841,46,'2011-09-19 18:00:00'),
 (842,46,'2011-09-19 18:00:01'),
 (843,46,'2011-09-19 18:00:07'),
 (844,46,'2011-09-19 18:00:09'),
 (845,46,'2011-09-19 18:00:55'),
 (846,46,'2011-09-19 18:00:57'),
 (847,46,'2011-09-19 18:01:36'),
 (848,46,'2011-09-19 18:01:43'),
 (849,45,'2011-09-19 18:01:45'),
 (850,45,'2011-09-19 18:01:49'),
 (851,46,'2011-09-19 18:03:08'),
 (852,46,'2011-09-19 18:03:10'),
 (853,46,'2011-09-19 18:03:11'),
 (854,46,'2011-09-19 18:03:26'),
 (855,46,'2011-09-19 18:03:27'),
 (856,46,'2011-09-19 18:03:47'),
 (857,46,'2011-09-19 18:03:47'),
 (858,46,'2011-09-19 18:03:48'),
 (859,46,'2011-09-19 18:04:14'),
 (860,46,'2011-09-19 18:04:15'),
 (861,46,'2011-09-19 18:04:37'),
 (862,46,'2011-09-19 18:04:39'),
 (863,46,'2011-09-19 18:05:00'),
 (864,46,'2011-09-19 18:05:02'),
 (865,47,'2011-09-19 18:05:33'),
 (866,47,'2011-09-19 18:05:36'),
 (867,47,'2011-09-19 18:08:27'),
 (868,47,'2011-09-19 18:08:28'),
 (869,47,'2011-09-19 18:08:32'),
 (870,47,'2011-09-19 18:08:35'),
 (871,47,'2011-09-19 18:08:36'),
 (872,47,'2011-09-19 18:08:51'),
 (873,47,'2011-09-19 18:08:52'),
 (874,47,'2011-09-19 18:09:09'),
 (875,47,'2011-09-19 18:09:11'),
 (876,48,'2011-09-19 18:11:45'),
 (877,48,'2011-09-19 18:11:48'),
 (878,48,'2011-09-19 18:19:57'),
 (879,48,'2011-09-19 18:19:58'),
 (880,48,'2011-09-19 18:20:55'),
 (881,48,'2011-09-19 18:20:57'),
 (882,48,'2011-09-19 18:21:30'),
 (883,48,'2011-09-19 18:21:31'),
 (884,47,'2011-09-19 18:22:20'),
 (885,47,'2011-09-19 18:22:22'),
 (886,47,'2011-09-19 18:23:40'),
 (887,47,'2011-09-19 18:23:41'),
 (888,47,'2011-09-19 18:24:00'),
 (889,47,'2011-09-19 18:24:01'),
 (890,46,'2011-09-19 18:30:03'),
 (891,46,'2011-09-19 18:30:05'),
 (892,47,'2011-09-19 18:42:02'),
 (893,47,'2011-09-19 18:42:04'),
 (894,47,'2011-09-19 18:42:53'),
 (895,47,'2011-09-19 18:42:57'),
 (896,47,'2011-09-19 18:42:58'),
 (897,47,'2011-09-19 18:48:19'),
 (898,47,'2011-09-19 18:48:23'),
 (899,47,'2011-09-19 18:53:26'),
 (900,47,'2011-09-19 18:53:28'),
 (901,47,'2011-09-19 18:54:17'),
 (902,47,'2011-09-19 18:54:18'),
 (903,47,'2011-09-19 18:55:32'),
 (904,47,'2011-09-19 18:55:34'),
 (905,48,'2011-09-19 18:58:36'),
 (906,48,'2011-09-19 18:58:37'),
 (907,48,'2011-09-19 18:59:06'),
 (908,48,'2011-09-19 18:59:08'),
 (909,47,'2011-09-19 18:59:24'),
 (910,47,'2011-09-19 18:59:25'),
 (911,47,'2011-09-19 18:59:44'),
 (912,47,'2011-09-19 18:59:46'),
 (913,47,'2011-09-19 18:59:49'),
 (914,47,'2011-09-19 18:59:52'),
 (915,47,'2011-09-19 18:59:55'),
 (916,47,'2011-09-19 18:59:57'),
 (917,48,'2011-09-19 19:00:51'),
 (918,48,'2011-09-19 19:00:52'),
 (919,47,'2011-09-19 19:02:54'),
 (920,48,'2011-09-19 19:02:55'),
 (921,47,'2011-09-19 19:02:56'),
 (922,47,'2011-09-19 19:02:57'),
 (923,48,'2011-09-19 19:02:57'),
 (924,47,'2011-09-19 19:02:58'),
 (925,48,'2011-09-19 19:07:21'),
 (926,48,'2011-09-19 19:07:24'),
 (927,47,'2011-09-19 19:07:35'),
 (928,47,'2011-09-19 19:07:36'),
 (929,47,'2011-09-19 19:07:46'),
 (930,47,'2011-09-19 19:07:49'),
 (931,47,'2011-09-19 19:08:23'),
 (932,47,'2011-09-19 19:08:24'),
 (933,47,'2011-09-19 19:08:30'),
 (934,47,'2011-09-19 19:08:31'),
 (935,47,'2011-09-19 19:08:35'),
 (936,47,'2011-09-19 19:08:36'),
 (937,47,'2011-09-19 19:08:45'),
 (938,47,'2011-09-19 19:08:47'),
 (939,47,'2011-09-19 19:08:54'),
 (940,47,'2011-09-19 19:08:55'),
 (941,47,'2011-09-19 19:09:00'),
 (942,47,'2011-09-19 19:09:02'),
 (943,47,'2011-09-19 19:09:38'),
 (944,47,'2011-09-19 19:09:39'),
 (945,47,'2011-09-19 19:09:41'),
 (946,47,'2011-09-19 19:09:49'),
 (947,47,'2011-09-19 19:09:51'),
 (948,47,'2011-09-19 19:10:18'),
 (949,47,'2011-09-19 19:10:19'),
 (950,47,'2011-09-19 19:10:22'),
 (951,47,'2011-09-19 19:12:11'),
 (952,47,'2011-09-19 19:12:13'),
 (953,47,'2011-09-19 19:12:28'),
 (954,47,'2011-09-19 19:12:30'),
 (955,47,'2011-09-19 19:15:16'),
 (956,48,'2011-09-19 19:15:16'),
 (957,47,'2011-09-19 19:15:19'),
 (958,48,'2011-09-19 19:15:19'),
 (959,48,'2011-09-19 19:17:14'),
 (960,48,'2011-09-19 19:17:15'),
 (961,48,'2011-09-19 19:17:38'),
 (962,47,'2011-09-19 19:17:40'),
 (963,48,'2011-09-19 19:17:40'),
 (964,47,'2011-09-19 19:17:42'),
 (965,47,'2011-09-19 19:20:01'),
 (966,47,'2011-09-19 19:20:03'),
 (967,47,'2011-09-19 19:20:07'),
 (968,47,'2011-09-19 19:20:09'),
 (969,48,'2011-09-19 19:20:09'),
 (970,48,'2011-09-19 19:20:11'),
 (971,47,'2011-09-19 19:21:53'),
 (972,47,'2011-09-19 19:21:54'),
 (973,48,'2011-09-19 19:22:00'),
 (974,48,'2011-09-19 19:22:02'),
 (975,48,'2011-09-19 19:22:44'),
 (976,48,'2011-09-19 19:22:44'),
 (977,48,'2011-09-19 19:22:46'),
 (978,47,'2011-09-19 19:22:47'),
 (979,47,'2011-09-19 19:22:48'),
 (980,47,'2011-09-19 19:22:50'),
 (981,48,'2011-09-19 19:23:05'),
 (982,48,'2011-09-19 19:23:07'),
 (983,47,'2011-09-19 19:23:07'),
 (984,47,'2011-09-19 19:23:07'),
 (985,47,'2011-09-19 19:23:09'),
 (986,47,'2011-09-19 19:23:25'),
 (987,47,'2011-09-19 19:23:26'),
 (988,47,'2011-09-19 19:23:30'),
 (989,47,'2011-09-19 19:23:32'),
 (990,47,'2011-09-19 19:24:13'),
 (991,47,'2011-09-19 19:24:14'),
 (992,47,'2011-09-19 19:24:23'),
 (993,47,'2011-09-19 19:24:24'),
 (994,47,'2011-09-19 19:24:35'),
 (995,47,'2011-09-19 19:24:36'),
 (996,47,'2011-09-19 19:24:40'),
 (997,47,'2011-09-19 19:24:41'),
 (998,46,'2011-09-19 19:25:35'),
 (999,46,'2011-09-19 19:25:36'),
 (1000,49,'2011-09-19 19:25:45'),
 (1001,49,'2011-09-19 19:25:47'),
 (1002,49,'2011-09-19 19:26:26'),
 (1003,49,'2011-09-19 19:26:27'),
 (1004,49,'2011-09-19 19:26:37'),
 (1005,49,'2011-09-19 19:26:38'),
 (1006,49,'2011-09-19 19:26:40'),
 (1007,49,'2011-09-19 19:26:50'),
 (1008,49,'2011-09-19 19:26:51'),
 (1009,49,'2011-09-19 19:26:52'),
 (1010,49,'2011-09-19 19:26:59'),
 (1011,49,'2011-09-19 19:27:00'),
 (1012,49,'2011-09-19 19:27:01'),
 (1013,49,'2011-09-19 19:27:10'),
 (1014,49,'2011-09-19 19:27:11'),
 (1015,47,'2011-09-19 19:33:32'),
 (1016,47,'2011-09-19 19:33:34'),
 (1017,48,'2011-09-19 19:34:04'),
 (1018,48,'2011-09-19 19:34:06'),
 (1019,50,'2011-09-20 12:23:39'),
 (1020,50,'2011-09-20 12:23:41'),
 (1021,51,'2011-09-21 18:41:22'),
 (1022,51,'2011-09-21 18:41:26'),
 (1023,51,'2011-09-21 19:00:36'),
 (1024,51,'2011-09-21 19:00:39'),
 (1025,51,'2011-09-21 19:12:29'),
 (1026,51,'2011-09-21 19:12:31'),
 (1027,51,'2011-09-21 19:12:55'),
 (1028,51,'2011-09-21 19:12:57'),
 (1029,51,'2011-09-21 19:44:21'),
 (1030,51,'2011-09-21 19:44:23'),
 (1031,51,'2011-09-21 19:55:44'),
 (1032,51,'2011-09-21 19:56:43'),
 (1033,51,'2011-09-21 19:57:02'),
 (1034,51,'2011-09-21 19:58:38'),
 (1035,51,'2011-09-21 19:58:39'),
 (1036,51,'2011-09-21 20:01:04'),
 (1037,51,'2011-09-21 20:06:18'),
 (1038,51,'2011-09-21 20:07:18'),
 (1039,51,'2011-09-21 20:12:59'),
 (1040,51,'2011-09-21 20:13:03'),
 (1041,51,'2011-09-21 20:13:28'),
 (1042,51,'2011-09-21 20:13:29'),
 (1043,51,'2011-09-21 20:29:19'),
 (1044,51,'2011-09-21 20:30:22'),
 (1045,51,'2011-09-21 20:32:19'),
 (1046,51,'2011-09-21 20:32:51'),
 (1047,51,'2011-09-21 20:38:06'),
 (1048,51,'2011-09-21 20:38:26'),
 (1049,51,'2011-09-21 20:38:39'),
 (1050,51,'2011-09-21 20:39:18'),
 (1051,51,'2011-09-21 20:39:19'),
 (1052,51,'2011-09-21 20:43:09'),
 (1053,51,'2011-09-21 20:44:07'),
 (1054,51,'2011-09-21 20:58:32'),
 (1055,51,'2011-09-21 20:59:00'),
 (1056,51,'2011-09-21 21:00:12'),
 (1057,51,'2011-09-21 21:00:16'),
 (1058,51,'2011-09-21 21:02:45'),
 (1059,51,'2011-09-21 21:02:46'),
 (1060,51,'2011-09-21 21:03:30'),
 (1061,51,'2011-09-21 21:03:32'),
 (1062,51,'2011-09-21 21:04:11'),
 (1063,51,'2011-09-21 21:04:12'),
 (1064,52,'2011-09-22 12:33:20'),
 (1065,52,'2011-09-22 12:33:23'),
 (1066,53,'2011-09-22 13:48:39'),
 (1067,53,'2011-09-22 13:48:47'),
 (1068,53,'2011-09-22 13:49:50'),
 (1069,53,'2011-09-22 13:49:52'),
 (1070,53,'2011-09-22 13:51:08'),
 (1071,53,'2011-09-22 13:51:11'),
 (1072,53,'2011-09-22 14:00:41'),
 (1073,53,'2011-09-22 14:00:48'),
 (1074,53,'2011-09-22 14:02:08'),
 (1075,53,'2011-09-22 14:02:12'),
 (1076,53,'2011-09-22 14:02:15'),
 (1077,53,'2011-09-22 14:07:44'),
 (1078,53,'2011-09-22 14:07:46'),
 (1079,53,'2011-09-22 14:07:49'),
 (1080,53,'2011-09-22 14:07:51'),
 (1081,53,'2011-09-22 14:11:08'),
 (1082,53,'2011-09-22 14:11:11'),
 (1083,53,'2011-09-22 14:11:17'),
 (1084,53,'2011-09-22 14:11:20'),
 (1085,53,'2011-09-22 14:11:22'),
 (1086,53,'2011-09-22 14:11:43'),
 (1087,53,'2011-09-22 14:11:44'),
 (1088,53,'2011-09-22 14:11:51'),
 (1089,53,'2011-09-22 14:11:53'),
 (1090,53,'2011-09-22 14:12:00'),
 (1091,53,'2011-09-22 14:12:01'),
 (1092,53,'2011-09-22 14:17:05'),
 (1093,53,'2011-09-22 14:17:06'),
 (1094,53,'2011-09-22 14:17:27'),
 (1095,53,'2011-09-22 14:17:29'),
 (1096,53,'2011-09-22 14:22:36'),
 (1097,53,'2011-09-22 14:22:37'),
 (1098,53,'2011-09-22 14:24:20'),
 (1099,53,'2011-09-22 14:25:40'),
 (1100,53,'2011-09-22 14:26:15'),
 (1101,53,'2011-09-22 14:27:27'),
 (1102,53,'2011-09-22 14:27:32'),
 (1103,53,'2011-09-22 14:27:35'),
 (1104,53,'2011-09-22 14:27:52'),
 (1105,53,'2011-09-22 14:27:54'),
 (1106,53,'2011-09-22 14:27:56'),
 (1107,53,'2011-09-22 14:28:11'),
 (1108,53,'2011-09-22 14:28:12'),
 (1109,53,'2011-09-22 14:28:37'),
 (1110,53,'2011-09-22 14:28:38'),
 (1111,53,'2011-09-22 14:28:39'),
 (1112,53,'2011-09-22 14:28:41'),
 (1113,53,'2011-09-22 14:30:30'),
 (1114,53,'2011-09-22 14:30:32'),
 (1115,53,'2011-09-22 14:30:34'),
 (1116,53,'2011-09-22 14:30:40'),
 (1117,53,'2011-09-22 14:30:43'),
 (1118,53,'2011-09-22 14:30:47'),
 (1119,53,'2011-09-22 14:30:48'),
 (1120,53,'2011-09-22 14:30:51'),
 (1121,53,'2011-09-22 14:30:52'),
 (1122,53,'2011-09-22 14:30:53'),
 (1123,53,'2011-09-22 14:30:54'),
 (1124,53,'2011-09-22 14:31:15'),
 (1125,53,'2011-09-22 14:31:16'),
 (1126,53,'2011-09-22 14:31:20'),
 (1127,53,'2011-09-22 14:31:22'),
 (1128,53,'2011-09-22 14:33:03'),
 (1129,53,'2011-09-22 14:33:05'),
 (1130,53,'2011-09-22 14:33:36'),
 (1131,53,'2011-09-22 14:33:37'),
 (1132,53,'2011-09-22 14:33:38'),
 (1133,53,'2011-09-22 14:33:39'),
 (1134,53,'2011-09-22 14:37:18'),
 (1135,53,'2011-09-22 14:37:20'),
 (1136,53,'2011-09-22 14:37:22'),
 (1137,53,'2011-09-22 14:38:29'),
 (1138,53,'2011-09-22 14:38:30'),
 (1139,53,'2011-09-22 14:38:32'),
 (1140,53,'2011-09-22 14:41:02'),
 (1141,53,'2011-09-22 14:41:03'),
 (1142,53,'2011-09-22 14:41:05'),
 (1143,53,'2011-09-22 14:42:04'),
 (1144,53,'2011-09-22 14:42:04'),
 (1145,53,'2011-09-22 14:42:06'),
 (1146,53,'2011-09-22 14:44:58'),
 (1147,53,'2011-09-22 14:44:59'),
 (1148,53,'2011-09-22 14:45:00'),
 (1149,53,'2011-09-22 14:50:05'),
 (1150,53,'2011-09-22 14:50:07'),
 (1151,53,'2011-09-22 14:50:32'),
 (1152,53,'2011-09-22 14:51:34'),
 (1153,53,'2011-09-22 14:52:38'),
 (1154,53,'2011-09-22 14:52:43'),
 (1155,53,'2011-09-22 14:52:45'),
 (1156,53,'2011-09-22 14:56:35'),
 (1157,53,'2011-09-22 14:56:36'),
 (1158,53,'2011-09-22 14:56:58'),
 (1159,53,'2011-09-22 14:56:59'),
 (1160,53,'2011-09-22 14:57:00'),
 (1161,53,'2011-09-22 14:57:02'),
 (1162,53,'2011-09-22 15:06:31'),
 (1163,53,'2011-09-22 15:06:34'),
 (1164,53,'2011-09-22 15:06:39'),
 (1165,53,'2011-09-22 15:06:56'),
 (1166,53,'2011-09-22 15:06:56'),
 (1167,53,'2011-09-22 15:06:58'),
 (1168,53,'2011-09-22 15:07:44'),
 (1169,53,'2011-09-22 15:07:44'),
 (1170,53,'2011-09-22 15:07:46'),
 (1171,53,'2011-09-22 15:08:46'),
 (1172,53,'2011-09-22 15:08:46'),
 (1173,53,'2011-09-22 15:08:48'),
 (1174,53,'2011-09-22 15:09:05'),
 (1175,53,'2011-09-22 15:09:06'),
 (1176,53,'2011-09-22 15:09:08'),
 (1177,53,'2011-09-22 15:09:56'),
 (1178,53,'2011-09-22 15:09:57'),
 (1179,53,'2011-09-22 15:09:58'),
 (1180,53,'2011-09-22 15:10:38'),
 (1181,53,'2011-09-22 15:10:38'),
 (1182,53,'2011-09-22 15:10:40'),
 (1183,53,'2011-09-22 15:11:03'),
 (1184,53,'2011-09-22 15:11:04'),
 (1185,53,'2011-09-22 15:11:05'),
 (1186,53,'2011-09-22 15:12:24'),
 (1187,53,'2011-09-22 15:12:24'),
 (1188,53,'2011-09-22 15:12:26'),
 (1189,53,'2011-09-22 15:12:47'),
 (1190,53,'2011-09-22 15:12:48'),
 (1191,53,'2011-09-22 15:12:50'),
 (1192,53,'2011-09-22 15:14:12'),
 (1193,53,'2011-09-22 15:14:12'),
 (1194,53,'2011-09-22 15:14:14'),
 (1195,53,'2011-09-22 15:16:40'),
 (1196,53,'2011-09-22 15:16:41'),
 (1197,53,'2011-09-22 15:16:42'),
 (1198,53,'2011-09-22 15:17:24'),
 (1199,53,'2011-09-22 15:17:25'),
 (1200,53,'2011-09-22 15:17:26'),
 (1201,53,'2011-09-22 15:18:43'),
 (1202,53,'2011-09-22 15:18:44'),
 (1203,53,'2011-09-22 15:18:45'),
 (1204,53,'2011-09-22 15:30:31'),
 (1205,53,'2011-09-22 15:30:33'),
 (1206,53,'2011-09-22 15:30:36'),
 (1207,53,'2011-09-22 15:31:16'),
 (1208,53,'2011-09-22 15:31:17'),
 (1209,53,'2011-09-22 15:31:19'),
 (1210,53,'2011-09-22 15:33:34'),
 (1211,53,'2011-09-22 15:33:35'),
 (1212,53,'2011-09-22 15:33:37'),
 (1213,53,'2011-09-22 15:33:50'),
 (1214,53,'2011-09-22 15:35:09'),
 (1215,53,'2011-09-22 15:35:09'),
 (1216,53,'2011-09-22 15:35:11'),
 (1217,53,'2011-09-22 15:35:15'),
 (1218,53,'2011-09-22 15:35:16'),
 (1219,53,'2011-09-22 15:35:16'),
 (1220,53,'2011-09-22 15:35:25'),
 (1221,53,'2011-09-22 15:35:26'),
 (1222,53,'2011-09-22 15:35:27'),
 (1223,53,'2011-09-22 15:37:06'),
 (1224,53,'2011-09-22 15:37:07'),
 (1225,53,'2011-09-22 15:37:08'),
 (1226,53,'2011-09-22 15:37:18'),
 (1227,53,'2011-09-22 15:37:18'),
 (1228,53,'2011-09-22 15:37:19'),
 (1229,53,'2011-09-22 15:37:21'),
 (1230,53,'2011-09-22 15:38:23'),
 (1231,53,'2011-09-22 15:38:24'),
 (1232,53,'2011-09-22 15:38:25'),
 (1233,53,'2011-09-22 15:39:12'),
 (1234,53,'2011-09-22 15:39:14'),
 (1235,53,'2011-09-22 15:39:16'),
 (1236,53,'2011-09-22 15:39:26'),
 (1237,53,'2011-09-22 15:39:26'),
 (1238,53,'2011-09-22 15:39:28'),
 (1239,53,'2011-09-22 15:41:06'),
 (1240,53,'2011-09-22 15:41:07'),
 (1241,53,'2011-09-22 15:41:08'),
 (1242,53,'2011-09-22 15:42:27'),
 (1243,53,'2011-09-22 15:42:28'),
 (1244,53,'2011-09-22 15:42:43'),
 (1245,53,'2011-09-22 15:42:44'),
 (1246,53,'2011-09-22 15:42:48'),
 (1247,53,'2011-09-22 15:43:04'),
 (1248,53,'2011-09-22 15:43:05'),
 (1249,53,'2011-09-22 15:43:07'),
 (1250,53,'2011-09-22 15:47:17'),
 (1251,53,'2011-09-22 15:47:17'),
 (1252,53,'2011-09-22 15:47:19'),
 (1253,53,'2011-09-22 15:49:15'),
 (1254,53,'2011-09-22 15:49:16'),
 (1255,53,'2011-09-22 15:49:17'),
 (1256,53,'2011-09-22 15:58:15'),
 (1257,53,'2011-09-22 15:58:17'),
 (1258,53,'2011-09-22 16:02:35'),
 (1259,53,'2011-09-22 16:02:37'),
 (1260,53,'2011-09-22 16:02:39'),
 (1261,53,'2011-09-22 16:04:02'),
 (1262,53,'2011-09-22 16:04:03'),
 (1263,53,'2011-09-22 16:04:05'),
 (1264,53,'2011-09-22 16:04:34'),
 (1265,53,'2011-09-22 16:04:35'),
 (1266,53,'2011-09-22 16:04:38'),
 (1267,53,'2011-09-22 16:06:03'),
 (1268,53,'2011-09-22 16:06:03'),
 (1269,53,'2011-09-22 16:06:06'),
 (1270,53,'2011-09-22 16:11:08'),
 (1271,53,'2011-09-22 16:11:09'),
 (1272,55,'2011-09-22 17:36:25'),
 (1273,55,'2011-09-22 17:36:29'),
 (1274,56,'2011-09-22 17:36:46'),
 (1275,56,'2011-09-22 17:36:49'),
 (1276,55,'2011-09-22 17:37:00'),
 (1277,55,'2011-09-22 17:37:07'),
 (1278,55,'2011-09-22 17:37:08'),
 (1279,58,'2011-09-22 18:06:56'),
 (1280,58,'2011-09-22 18:07:04'),
 (1281,58,'2011-09-22 18:43:14'),
 (1282,58,'2011-09-22 18:43:23'),
 (1283,58,'2011-09-22 18:43:25'),
 (1284,58,'2011-09-22 18:43:51'),
 (1285,58,'2011-09-22 18:43:53'),
 (1286,58,'2011-09-22 18:43:55'),
 (1287,58,'2011-09-22 18:44:59'),
 (1288,58,'2011-09-22 18:45:01'),
 (1289,58,'2011-09-22 18:45:04'),
 (1290,58,'2011-09-22 18:45:11'),
 (1291,58,'2011-09-22 18:45:13'),
 (1292,59,'2011-09-22 18:45:46'),
 (1293,58,'2011-09-22 18:45:47'),
 (1294,59,'2011-09-22 18:45:47'),
 (1295,58,'2011-09-22 18:45:50'),
 (1296,59,'2011-09-22 18:45:50'),
 (1297,58,'2011-09-22 18:45:54'),
 (1298,58,'2011-09-22 18:45:55'),
 (1299,59,'2011-09-22 18:46:05'),
 (1300,59,'2011-09-22 18:46:06'),
 (1301,59,'2011-09-22 18:46:20'),
 (1302,59,'2011-09-22 18:46:22'),
 (1303,59,'2011-09-22 18:46:26'),
 (1304,59,'2011-09-22 18:46:27'),
 (1305,59,'2011-09-22 18:46:34'),
 (1306,59,'2011-09-22 18:46:36'),
 (1307,59,'2011-09-22 18:47:00'),
 (1308,59,'2011-09-22 18:47:01'),
 (1309,59,'2011-09-22 18:47:03'),
 (1310,59,'2011-09-22 18:47:09'),
 (1311,58,'2011-09-22 18:47:20'),
 (1312,58,'2011-09-22 18:47:22'),
 (1313,59,'2011-09-22 19:43:42'),
 (1314,59,'2011-09-22 19:43:45'),
 (1315,59,'2011-09-22 19:45:49'),
 (1316,59,'2011-09-22 19:45:50'),
 (1317,59,'2011-09-22 20:21:30'),
 (1318,59,'2011-09-22 20:21:33'),
 (1319,59,'2011-09-22 20:22:18'),
 (1320,59,'2011-09-22 20:22:19'),
 (1321,59,'2011-09-22 20:22:21'),
 (1322,59,'2011-09-22 20:22:30'),
 (1323,59,'2011-09-22 20:22:31'),
 (1324,59,'2011-09-22 21:04:08'),
 (1325,59,'2011-09-22 21:04:11'),
 (1326,59,'2011-09-22 21:04:15'),
 (1327,59,'2011-09-22 21:04:16');
UNLOCK TABLES;
/*!40000 ALTER TABLE `log_url` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`log_url_info`
--

DROP TABLE IF EXISTS `supernarede`.`log_url_info`;
CREATE TABLE  `supernarede`.`log_url_info` (
  `url_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `url` varchar(255) NOT NULL DEFAULT '',
  `referer` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`url_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1328 DEFAULT CHARSET=utf8 COMMENT='Detale information about url visit';

--
-- Dumping data for table `supernarede`.`log_url_info`
--

/*!40000 ALTER TABLE `log_url_info` DISABLE KEYS */;
LOCK TABLES `log_url_info` WRITE;
INSERT INTO `supernarede`.`log_url_info` VALUES  (1,'http://local.host/super_na_rede/',''),
 (2,'http://local.host/super_na_rede/',''),
 (3,'http://local.host/super_na_rede/',''),
 (4,'http://local.host/super_na_rede/js/jquery/jquery.noconflict.js','http://local.host/super_na_rede/'),
 (5,'http://local.host/super_na_rede/skin/frontend/base/default/images/logo.gif','http://local.host/super_na_rede/'),
 (6,'http://local.host/super_na_rede/js/cycle/jquery.cycle.lite.js','http://local.host/super_na_rede/'),
 (7,'http://local.host/super_na_rede/skin/frontend/base/default/images/media/footer_callout.png','http://local.host/super_na_rede/'),
 (8,'http://local.host/super_na_rede/js/jquery/jquery.js','http://local.host/super_na_rede/'),
 (9,'http://local.host/super_na_rede/js/jquery/jquery.noconflict.js','http://local.host/super_na_rede/'),
 (10,'http://local.host/super_na_rede/js/cycle/jquery.cycle.lite.js','http://local.host/super_na_rede/'),
 (11,'http://local.host/super_na_rede/skin/frontend/base/default/images/media/footer_callout.png','http://local.host/super_na_rede/'),
 (12,'http://local.host/super_na_rede/skin/frontend/base/default/images/logo.gif','http://local.host/super_na_rede/'),
 (13,'http://local.host/super_na_rede/',''),
 (14,'http://local.host/super_na_rede/js/cycle/jquery.cycle.lite.js','http://local.host/super_na_rede/'),
 (15,'http://local.host/super_na_rede/skin/frontend/base/default/images/logo.gif','http://local.host/super_na_rede/'),
 (16,'http://local.host/super_na_rede/js/jquery/jquery.js','http://local.host/super_na_rede/'),
 (17,'http://local.host/super_na_rede/skin/frontend/base/default/images/media/footer_callout.png','http://local.host/super_na_rede/'),
 (18,'http://local.host/super_na_rede/js/jquery/jquery.noconflict.js','http://local.host/super_na_rede/'),
 (19,'http://local.host/super_na_rede/js/cycle/jquery.cycle.lite.js','http://local.host/super_na_rede/'),
 (20,'http://local.host/super_na_rede/skin/frontend/base/default/images/media/footer_callout.png','http://local.host/super_na_rede/'),
 (21,'http://local.host/super_na_rede/skin/frontend/base/default/images/logo.gif','http://local.host/super_na_rede/'),
 (22,'http://local.host/super_na_rede/skin/frontend/base/default/images/logo.gif',''),
 (23,'http://local.host/super_na_rede/',''),
 (24,'http://local.host/super_na_rede/js/cycle/jquery.cycle.lite.js','http://local.host/super_na_rede/'),
 (25,'http://local.host/super_na_rede/js/jquery/jquery.js','http://local.host/super_na_rede/'),
 (26,'http://local.host/super_na_rede/skin/frontend/base/default/images/logo.gif','http://local.host/super_na_rede/'),
 (27,'http://local.host/super_na_rede/skin/frontend/base/default/images/media/footer_callout.png','http://local.host/super_na_rede/'),
 (28,'http://local.host/super_na_rede/js/jquery/jquery.noconflict.js','http://local.host/super_na_rede/'),
 (29,'http://local.host/super_na_rede/js/cycle/jquery.cycle.lite.js','http://local.host/super_na_rede/'),
 (30,'http://local.host/super_na_rede/skin/frontend/base/default/images/logo.gif','http://local.host/super_na_rede/'),
 (31,'http://local.host/super_na_rede/skin/frontend/base/default/images/media/footer_callout.png','http://local.host/super_na_rede/'),
 (32,'http://local.host/super_na_rede/',''),
 (33,'http://local.host/super_na_rede/js/jquery/jquery.noconflict.js','http://local.host/super_na_rede/'),
 (34,'http://local.host/super_na_rede/js/jquery/jquery.js','http://local.host/super_na_rede/'),
 (35,'http://local.host/super_na_rede/skin/frontend/base/default/images/logo.gif','http://local.host/super_na_rede/'),
 (36,'http://local.host/super_na_rede/skin/frontend/base/default/images/media/footer_callout.png','http://local.host/super_na_rede/'),
 (37,'http://local.host/super_na_rede/js/jquery/jquery.noconflict.js','http://local.host/super_na_rede/'),
 (38,'http://local.host/super_na_rede/js/cycle/jquery.cycle.lite.js','http://local.host/super_na_rede/'),
 (39,'http://local.host/super_na_rede/skin/frontend/base/default/images/logo.gif','http://local.host/super_na_rede/'),
 (40,'http://local.host/super_na_rede/skin/frontend/base/default/images/media/footer_callout.png','http://local.host/super_na_rede/'),
 (41,'http://supernarede.local/',''),
 (42,'http://supernarede.local/',''),
 (43,'http://supernarede.local/',''),
 (44,'http://supernarede.local/',''),
 (45,'http://supernarede.local/',''),
 (46,'http://supernarede.local/',''),
 (47,'http://supernarede.local/',''),
 (48,'http://supernarede.local/',''),
 (49,'http://supernarede.local/quem-somos',''),
 (50,'http://supernarede.local/quem-somos',''),
 (51,'http://supernarede.local/quem-somos',''),
 (52,'http://supernarede.local/quem-somos',''),
 (53,'http://supernarede.local/quem-somos',''),
 (54,'http://supernarede.local/quem-somos',''),
 (55,'http://supernarede.local/quem-somos',''),
 (56,'http://supernarede.local/images/caminho-pagina/ico_seta.png','http://supernarede.local/quem-somos'),
 (57,'http://supernarede.local/quem-somos',''),
 (58,'http://supernarede.local/images/caminho-pagina/ico_seta.png','http://supernarede.local/quem-somos'),
 (59,'http://supernarede.local/quem-somos',''),
 (60,'http://supernarede.local/quem-somos',''),
 (61,'http://supernarede.local/quem-somos',''),
 (62,'http://supernarede.local/quem-somos',''),
 (63,'http://supernarede.local/quem-somos',''),
 (64,'http://supernarede.local/quem-somos',''),
 (65,'http://supernarede.local/quem-somos',''),
 (66,'http://supernarede.local/quem-somos',''),
 (67,'http://supernarede.local/quem-somos',''),
 (68,'http://supernarede.local/index.php/','http://supernarede.local/quem-somos'),
 (69,'http://supernarede.local/index.php/quem-somos',''),
 (70,'http://supernarede.local/quem-somos',''),
 (71,'http://supernarede.local/quem-somos',''),
 (72,'http://supernarede.local/quem-somos',''),
 (73,'http://supernarede.local/quem-somos',''),
 (74,'http://supernarede.local/quem-somos',''),
 (75,'http://supernarede.local/quem-somos',''),
 (76,'http://supernarede.local/quem-somos',''),
 (77,'http://supernarede.local/quem-somos',''),
 (78,'http://supernarede.local/quem-somos',''),
 (79,'http://supernarede.local/quem-somos',''),
 (80,'http://supernarede.local/quem-somos',''),
 (81,'http://supernarede.local/quem-somos',''),
 (82,'http://supernarede.local/quem-somos',''),
 (83,'http://supernarede.local/quem-somos',''),
 (84,'http://supernarede.local/quem-somos',''),
 (85,'http://supernarede.local/quem-somos',''),
 (86,'http://supernarede.local/quem-somos',''),
 (87,'http://supernarede.local/quem-somos',''),
 (88,'http://supernarede.local/quem-somos',''),
 (89,'http://supernarede.local/quem-somos',''),
 (90,'http://supernarede.local/quem-somos',''),
 (91,'http://supernarede.local/quem-somos',''),
 (92,'http://supernarede.local/quem-somos',''),
 (93,'http://supernarede.local/quem-somos',''),
 (94,'http://supernarede.local/quem-somos',''),
 (95,'http://supernarede.local/quem-somos',''),
 (96,'http://supernarede.local/quem-somos',''),
 (97,'http://supernarede.local/quem-somos',''),
 (98,'http://supernarede.local/quem-somos',''),
 (99,'http://supernarede.local/quem-somos',''),
 (100,'http://supernarede.local/quem-somos',''),
 (101,'http://supernarede.local/','http://supernarede.local/quem-somos'),
 (102,'http://supernarede.local/quem-somos','http://supernarede.local/quem-somos'),
 (103,'http://supernarede.local/',''),
 (104,'http://supernarede.local/quem-somos',''),
 (105,'http://supernarede.local/images/template/bt_todos-os-departamentos.png/','http://supernarede.local/quem-somos'),
 (106,'http://supernarede.local/quem-somos','http://supernarede.local/quem-somos'),
 (107,'http://supernarede.local/images/template/bt_carrinho.png/','http://supernarede.local/quem-somos'),
 (108,'http://supernarede.local/images/template/bt_buscar.png/','http://supernarede.local/quem-somos'),
 (109,'http://supernarede.local/images/template/bt_compra-facil.png/','http://supernarede.local/quem-somos'),
 (110,'http://supernarede.local/images/template/bt_todos-os-departamentos.png/',''),
 (111,'http://supernarede.local/quem-somos',''),
 (112,'http://supernarede.local/quem-somos','http://supernarede.local/quem-somos'),
 (113,'http://supernarede.local/quem-somos',''),
 (114,'http://supernarede.local/quem-somos','http://supernarede.local/quem-somos'),
 (115,'http://supernarede.local/quem-somos',''),
 (116,'http://supernarede.local/quem-somos','http://supernarede.local/quem-somos'),
 (117,'http://supernarede.local/quem-somos',''),
 (118,'http://supernarede.local/quem-somos','http://supernarede.local/quem-somos'),
 (119,'http://supernarede.local/quem-somos',''),
 (120,'http://supernarede.local/quem-somos','http://supernarede.local/quem-somos'),
 (121,'http://supernarede.local/quem-somos',''),
 (122,'http://supernarede.local/quem-somos','http://supernarede.local/quem-somos'),
 (123,'http://supernarede.local/quem-somos',''),
 (124,'http://supernarede.local/quem-somos','http://supernarede.local/quem-somos'),
 (125,'http://supernarede.local/',''),
 (126,'http://supernarede.local/','http://supernarede.local/'),
 (127,'http://supernarede.local/faq','http://supernarede.local/'),
 (128,'http://supernarede.local/faq','http://supernarede.local/faq'),
 (129,'http://supernarede.local/faq','http://supernarede.local/'),
 (130,'http://supernarede.local/faq','http://supernarede.local/faq'),
 (131,'http://supernarede.local/faq','http://supernarede.local/'),
 (132,'http://supernarede.local/faq','http://supernarede.local/'),
 (133,'http://supernarede.local/faq','http://supernarede.local/'),
 (134,'http://supernarede.local/departamentos',''),
 (135,'http://supernarede.local/images/template/bt_fechar-compra-facil.png','http://supernarede.local/departamentos'),
 (136,'http://supernarede.local/images/template/bt_buscar-compra-facil.png','http://supernarede.local/departamentos'),
 (137,'http://supernarede.local/departamentos',''),
 (138,'http://supernarede.local/images/template/bt_fechar-compra-facil.png','http://supernarede.local/departamentos'),
 (139,'http://supernarede.local/images/template/bt_buscar-compra-facil.png','http://supernarede.local/departamentos'),
 (140,'http://supernarede.local/',''),
 (141,'http://supernarede.local/images/template/bt_buscar-compra-facil.png','http://supernarede.local/'),
 (142,'http://supernarede.local/images/template/bt_fechar-compra-facil.png','http://supernarede.local/'),
 (143,'http://supernarede.local/departamentos',''),
 (144,'http://supernarede.local/images/template/bt_buscar-compra-facil.png','http://supernarede.local/departamentos'),
 (145,'http://supernarede.local/images/template/bt_fechar-compra-facil.png','http://supernarede.local/departamentos'),
 (146,'http://supernarede.local/index.php/departamentos',''),
 (147,'http://supernarede.local/index.php/images/template/bt_buscar-compra-facil.png','http://supernarede.local/index.php/departamentos'),
 (148,'http://supernarede.local/index.php/images/template/bt_fechar-compra-facil.png','http://supernarede.local/index.php/departamentos'),
 (149,'http://supernarede.local/',''),
 (150,'http://supernarede.local/images/template/bt_fechar-compra-facil.png','http://supernarede.local/'),
 (151,'http://supernarede.local/images/template/bt_buscar-compra-facil.png','http://supernarede.local/'),
 (152,'http://supernarede.local/',''),
 (153,'http://supernarede.local/catalog/category/view/id/3','http://supernarede.local/'),
 (154,'http://supernarede.local/catalog/category/view/id/3','http://supernarede.local/'),
 (155,'http://supernarede.local/images/template/bt_fechar-compra-facil.png','http://supernarede.local/departamentos.html'),
 (156,'http://supernarede.local/images/template/bt_buscar-compra-facil.png','http://supernarede.local/departamentos.html'),
 (157,'http://supernarede.local/catalog/category/view/id/3','http://supernarede.local/'),
 (158,'http://supernarede.local/images/template/bt_fechar-compra-facil.png','http://supernarede.local/departamentos.html'),
 (159,'http://supernarede.local/images/template/bt_buscar-compra-facil.png','http://supernarede.local/departamentos.html'),
 (160,'http://supernarede.local/catalog/category/view/id/3',''),
 (161,'http://supernarede.local/images/template/bt_buscar-compra-facil.png','http://supernarede.local/departamentos.html'),
 (162,'http://supernarede.local/images/template/bt_fechar-compra-facil.png','http://supernarede.local/departamentos.html'),
 (163,'http://supernarede.local/catalog/category/view/id/3',''),
 (164,'http://supernarede.local/images/template/bt_buscar-compra-facil.png','http://supernarede.local/departamentos'),
 (165,'http://supernarede.local/images/template/bt_fechar-compra-facil.png','http://supernarede.local/departamentos'),
 (166,'http://supernarede.local/faq','http://supernarede.local/departamentos'),
 (167,'http://supernarede.local/images/template/bt_fechar-compra-facil.png','http://supernarede.local/faq'),
 (168,'http://supernarede.local/images/template/bt_buscar-compra-facil.png','http://supernarede.local/faq'),
 (169,'http://supernarede.local/politica-de-entrega',''),
 (170,'http://supernarede.local/images/template/bt_fechar-compra-facil.png','http://supernarede.local/politica-de-entrega'),
 (171,'http://supernarede.local/images/template/bt_buscar-compra-facil.png','http://supernarede.local/politica-de-entrega'),
 (172,'http://supernarede.local/politica-de-seguranca',''),
 (173,'http://supernarede.local/images/template/bt_fechar-compra-facil.png','http://supernarede.local/politica-de-seguranca'),
 (174,'http://supernarede.local/images/template/bt_buscar-compra-facil.png','http://supernarede.local/politica-de-seguranca'),
 (175,'http://supernarede.local/politica-de-privacidade',''),
 (176,'http://supernarede.local/images/template/bt_fechar-compra-facil.png','http://supernarede.local/politica-de-privacidade'),
 (177,'http://supernarede.local/images/template/bt_buscar-compra-facil.png','http://supernarede.local/politica-de-privacidade'),
 (178,'http://supernarede.local/catalog/category/view/id/4',''),
 (179,'http://supernarede.local/images/template/bt_fechar-compra-facil.png','http://supernarede.local/listas-prontas'),
 (180,'http://supernarede.local/images/template/bt_buscar-compra-facil.png','http://supernarede.local/listas-prontas'),
 (181,'http://supernarede.local/catalog/category/view/id/4',''),
 (182,'http://supernarede.local/images/template/bt_fechar-compra-facil.png','http://supernarede.local/listas-prontas'),
 (183,'http://supernarede.local/images/template/bt_buscar-compra-facil.png','http://supernarede.local/listas-prontas'),
 (184,'http://supernarede.local/faq','http://supernarede.local/listas-prontas'),
 (185,'http://supernarede.local/images/template/bt_buscar-compra-facil.png','http://supernarede.local/faq'),
 (186,'http://supernarede.local/images/template/bt_fechar-compra-facil.png','http://supernarede.local/faq'),
 (187,'http://supernarede.local/catalog/category/view/id/4',''),
 (188,'http://supernarede.local/images/template/bt_fechar-compra-facil.png','http://supernarede.local/listas-prontas'),
 (189,'http://supernarede.local/images/template/bt_buscar-compra-facil.png','http://supernarede.local/listas-prontas'),
 (190,'http://supernarede.local/catalog/category/view/id/4',''),
 (191,'http://supernarede.local/images/template/bt_fechar-compra-facil.png','http://supernarede.local/listas-prontas'),
 (192,'http://supernarede.local/images/template/bt_buscar-compra-facil.png','http://supernarede.local/listas-prontas'),
 (193,'http://supernarede.local/catalog/category/view/id/4',''),
 (194,'http://supernarede.local/images/template/bt_buscar-compra-facil.png','http://supernarede.local/listas-prontas'),
 (195,'http://supernarede.local/images/template/bt_fechar-compra-facil.png','http://supernarede.local/listas-prontas'),
 (196,'http://supernarede.local/catalog/category/view/id/4',''),
 (197,'http://supernarede.local/images/template/bt_fechar-compra-facil.png','http://supernarede.local/listas-prontas'),
 (198,'http://supernarede.local/images/template/bt_buscar-compra-facil.png','http://supernarede.local/listas-prontas'),
 (199,'http://supernarede.local/catalog/category/view/id/4',''),
 (200,'http://supernarede.local/catalog/category/view/id/4',''),
 (201,'http://supernarede.local/images/template/bt_fechar-compra-facil.png','http://supernarede.local/listas-prontas'),
 (202,'http://supernarede.local/images/template/bt_buscar-compra-facil.png','http://supernarede.local/listas-prontas'),
 (203,'http://supernarede.local/catalog/category/view/id/4',''),
 (204,'http://supernarede.local/images/template/bt_fechar-compra-facil.png','http://supernarede.local/listas-prontas'),
 (205,'http://supernarede.local/images/template/bt_buscar-compra-facil.png','http://supernarede.local/listas-prontas'),
 (206,'http://supernarede.local/faq','http://supernarede.local/listas-prontas'),
 (207,'http://supernarede.local/images/template/bt_fechar-compra-facil.png','http://supernarede.local/faq'),
 (208,'http://supernarede.local/images/template/bt_buscar-compra-facil.png','http://supernarede.local/faq'),
 (209,'http://supernarede.local/catalog/category/view/id/4',''),
 (210,'http://supernarede.local/images/template/bt_fechar-compra-facil.png','http://supernarede.local/listas-prontas'),
 (211,'http://supernarede.local/images/template/bt_buscar-compra-facil.png','http://supernarede.local/listas-prontas'),
 (212,'http://supernarede.local/catalog/category/view/id/4',''),
 (213,'http://supernarede.local/images/template/bt_fechar-compra-facil.png','http://supernarede.local/listas-prontas'),
 (214,'http://supernarede.local/images/template/bt_buscar-compra-facil.png','http://supernarede.local/listas-prontas'),
 (215,'http://supernarede.local/faq','http://supernarede.local/listas-prontas'),
 (216,'http://supernarede.local/images/template/bt_fechar-compra-facil.png','http://supernarede.local/faq'),
 (217,'http://supernarede.local/images/template/bt_buscar-compra-facil.png','http://supernarede.local/faq'),
 (218,'http://supernarede.local/catalog/category/view/id/4','http://supernarede.local/faq'),
 (219,'http://supernarede.local/images/template/bt_buscar-compra-facil.png','http://supernarede.local/listas-prontas'),
 (220,'http://supernarede.local/images/template/bt_fechar-compra-facil.png','http://supernarede.local/listas-prontas'),
 (221,'http://supernarede.local/catalog/category/view/id/4','http://supernarede.local/faq'),
 (222,'http://supernarede.local/images/template/bt_fechar-compra-facil.png','http://supernarede.local/listas-prontas'),
 (223,'http://supernarede.local/images/template/bt_buscar-compra-facil.png','http://supernarede.local/listas-prontas'),
 (224,'http://supernarede.local/catalog/category/view/id/4','http://supernarede.local/faq'),
 (225,'http://supernarede.local/images/template/bt_fechar-compra-facil.png','http://supernarede.local/listas-prontas'),
 (226,'http://supernarede.local/images/template/bt_buscar-compra-facil.png','http://supernarede.local/listas-prontas'),
 (227,'http://supernarede.local/catalog/category/view/id/4','http://supernarede.local/faq'),
 (228,'http://supernarede.local/images/template/bt_fechar-compra-facil.png','http://supernarede.local/listas-prontas'),
 (229,'http://supernarede.local/images/template/bt_buscar-compra-facil.png','http://supernarede.local/listas-prontas'),
 (230,'http://supernarede.local/faq','http://supernarede.local/listas-prontas'),
 (231,'http://supernarede.local/images/template/bt_fechar-compra-facil.png','http://supernarede.local/faq'),
 (232,'http://supernarede.local/images/template/bt_buscar-compra-facil.png','http://supernarede.local/faq'),
 (233,'http://supernarede.local/faq','http://supernarede.local/listas-prontas'),
 (234,'http://supernarede.local/images/template/bt_fechar-compra-facil.png','http://supernarede.local/faq'),
 (235,'http://supernarede.local/images/template/bt_buscar-compra-facil.png','http://supernarede.local/faq'),
 (236,'http://supernarede.local/faq','http://supernarede.local/listas-prontas'),
 (237,'http://supernarede.local/images/template/bt_fechar-compra-facil.png','http://supernarede.local/faq'),
 (238,'http://supernarede.local/images/template/bt_buscar-compra-facil.png','http://supernarede.local/faq'),
 (239,'http://supernarede.local/faq','http://supernarede.local/listas-prontas'),
 (240,'http://supernarede.local/faq','http://supernarede.local/listas-prontas'),
 (241,'http://supernarede.local/faq','http://supernarede.local/listas-prontas'),
 (242,'http://supernarede.local/faq','http://supernarede.local/listas-prontas'),
 (243,'http://supernarede.local/faq','http://supernarede.local/listas-prontas'),
 (244,'http://supernarede.local/faq','http://supernarede.local/listas-prontas'),
 (245,'http://supernarede.local/faq','http://supernarede.local/listas-prontas'),
 (246,'http://supernarede.local/faq','http://supernarede.local/listas-prontas'),
 (247,'http://supernarede.local/faq','http://supernarede.local/listas-prontas'),
 (248,'http://supernarede.local/faq','http://supernarede.local/listas-prontas'),
 (249,'http://supernarede.local/faq','http://supernarede.local/listas-prontas'),
 (250,'http://supernarede.local/faq','http://supernarede.local/listas-prontas'),
 (251,'http://supernarede.local/faq','http://supernarede.local/listas-prontas'),
 (252,'http://supernarede.local/faq','http://supernarede.local/listas-prontas'),
 (253,'http://supernarede.local/faq','http://supernarede.local/listas-prontas'),
 (254,'http://supernarede.local/faq','http://supernarede.local/listas-prontas'),
 (255,'http://supernarede.local/faq','http://supernarede.local/listas-prontas'),
 (256,'http://supernarede.local/faq','http://supernarede.local/listas-prontas'),
 (257,'http://supernarede.local/faq','http://supernarede.local/listas-prontas'),
 (258,'http://supernarede.local/faq','http://supernarede.local/listas-prontas'),
 (259,'http://supernarede.local/faq','http://supernarede.local/listas-prontas'),
 (260,'http://supernarede.local/faq','http://supernarede.local/listas-prontas'),
 (261,'http://supernarede.local/faq','http://supernarede.local/listas-prontas'),
 (262,'http://supernarede.local/faq','http://supernarede.local/listas-prontas'),
 (263,'http://supernarede.local/faq','http://supernarede.local/listas-prontas'),
 (264,'http://supernarede.local/faq','http://supernarede.local/listas-prontas'),
 (265,'http://supernarede.local/faq','http://supernarede.local/listas-prontas'),
 (266,'http://supernarede.local/faq','http://supernarede.local/listas-prontas'),
 (267,'http://supernarede.local/faq','http://supernarede.local/listas-prontas'),
 (268,'http://supernarede.local/faq','http://supernarede.local/listas-prontas'),
 (269,'http://supernarede.local/faq','http://supernarede.local/listas-prontas'),
 (270,'http://supernarede.local/faq','http://supernarede.local/listas-prontas'),
 (271,'http://supernarede.local/faq','http://supernarede.local/listas-prontas'),
 (272,'http://supernarede.local/faq','http://supernarede.local/listas-prontas'),
 (273,'http://supernarede.local/faq','http://supernarede.local/listas-prontas'),
 (274,'http://supernarede.local/faq','http://supernarede.local/listas-prontas'),
 (275,'http://supernarede.local/faq','http://supernarede.local/listas-prontas'),
 (276,'http://supernarede.local/faq','http://supernarede.local/listas-prontas'),
 (277,'http://supernarede.local/faq','http://supernarede.local/listas-prontas'),
 (278,'http://supernarede.local/faq','http://supernarede.local/listas-prontas'),
 (279,'http://supernarede.local/faq','http://supernarede.local/listas-prontas'),
 (280,'http://supernarede.local/faq','http://supernarede.local/listas-prontas'),
 (281,'http://supernarede.local/faq','http://supernarede.local/listas-prontas'),
 (282,'http://supernarede.local/faq','http://supernarede.local/listas-prontas'),
 (283,'http://supernarede.local/faq','http://supernarede.local/listas-prontas'),
 (284,'http://supernarede.local/faq','http://supernarede.local/listas-prontas'),
 (285,'http://supernarede.local/faq','http://supernarede.local/listas-prontas'),
 (286,'http://supernarede.local/faq','http://supernarede.local/listas-prontas'),
 (287,'http://supernarede.local/customer/account/create/','http://supernarede.local/faq'),
 (288,'http://supernarede.local/','http://supernarede.local/customer/account/create/'),
 (289,'http://supernarede.local/','http://supernarede.local/customer/account/create/'),
 (290,'http://supernarede.local/faq','http://supernarede.local/'),
 (291,'http://supernarede.local/catalog/category/view/id/4','http://supernarede.local/faq'),
 (292,'http://supernarede.local/catalog/category/view/id/4','http://supernarede.local/faq'),
 (293,'http://supernarede.local/catalog/category/view/id/4','http://supernarede.local/faq'),
 (294,'http://supernarede.local/catalog/category/view/id/4','http://supernarede.local/faq'),
 (295,'http://supernarede.local/catalog/category/view/id/4','http://supernarede.local/faq'),
 (296,'http://supernarede.local/faq','http://supernarede.local/listas-prontas'),
 (297,'http://supernarede.local/','http://supernarede.local/faq'),
 (298,'http://supernarede.local/','http://supernarede.local/faq'),
 (299,'http://supernarede.local/','http://supernarede.local/faq'),
 (300,'http://supernarede.local/','http://supernarede.local/faq'),
 (301,'http://supernarede.local/','http://supernarede.local/faq'),
 (302,'http://supernarede.local/','http://supernarede.local/faq'),
 (303,'http://supernarede.local/','http://supernarede.local/faq'),
 (304,'http://supernarede.local/','http://supernarede.local/faq'),
 (305,'http://supernarede.local/','http://supernarede.local/faq'),
 (306,'http://supernarede.local/','http://supernarede.local/faq'),
 (307,'http://supernarede.local/','http://supernarede.local/faq'),
 (308,'http://supernarede.local/','http://supernarede.local/faq'),
 (309,'http://supernarede.local/images/template/bt_news-remover.png','http://supernarede.local/'),
 (310,'http://supernarede.local/','http://supernarede.local/faq'),
 (311,'http://supernarede.local/',''),
 (312,'http://supernarede.local/newsletter/subscriber/new/','http://supernarede.local/'),
 (313,'http://supernarede.local/','http://supernarede.local/'),
 (314,'http://supernarede.local/',''),
 (315,'http://supernarede.local/newsletter/subscriber/new/','http://supernarede.local/'),
 (316,'http://supernarede.local/','http://supernarede.local/'),
 (317,'http://supernarede.local/',''),
 (318,'http://supernarede.local/',''),
 (319,'http://supernarede.local/newsletter/subscriber/new','http://supernarede.local/'),
 (320,'http://supernarede.local/','http://supernarede.local/'),
 (321,'http://supernarede.local/newsletter/subscriber/new','http://supernarede.local/'),
 (322,'http://supernarede.local/newsletter/subscriber/new','http://supernarede.local/'),
 (323,'http://supernarede.local/newsletter/subscriber/new','http://supernarede.local/'),
 (324,'http://supernarede.local/newsletter/subscriber/new','http://supernarede.local/'),
 (325,'http://supernarede.local/newsletter/subscriber/new','http://supernarede.local/'),
 (326,'http://supernarede.local/',''),
 (327,'http://supernarede.local/',''),
 (328,'http://supernarede.local/',''),
 (329,'http://supernarede.local/',''),
 (330,'http://supernarede.local/',''),
 (331,'http://supernarede.local/',''),
 (332,'http://supernarede.local/',''),
 (333,'http://supernarede.local/',''),
 (334,'http://supernarede.local/',''),
 (335,'http://supernarede.local/',''),
 (336,'http://supernarede.local/',''),
 (337,'http://supernarede.local/',''),
 (338,'http://supernarede.local/',''),
 (339,'http://supernarede.local/',''),
 (340,'http://supernarede.local/',''),
 (341,'http://supernarede.local/',''),
 (342,'http://supernarede.local/',''),
 (343,'http://supernarede.local/',''),
 (344,'http://supernarede.local/',''),
 (345,'http://supernarede.local/',''),
 (346,'http://supernarede.local/',''),
 (347,'http://supernarede.local/',''),
 (348,'http://supernarede.local/',''),
 (349,'http://supernarede.local/',''),
 (350,'http://supernarede.local/',''),
 (351,'http://supernarede.local/',''),
 (352,'http://supernarede.local/',''),
 (353,'http://supernarede.local/',''),
 (354,'http://supernarede.local/',''),
 (355,'http://supernarede.local/',''),
 (356,'http://supernarede.local/',''),
 (357,'http://supernarede.local/',''),
 (358,'http://supernarede.local/',''),
 (359,'http://supernarede.local/',''),
 (360,'http://supernarede.local/',''),
 (361,'http://supernarede.local/',''),
 (362,'http://supernarede.local/',''),
 (363,'http://supernarede.local/',''),
 (364,'http://supernarede.local/',''),
 (365,'http://supernarede.local/',''),
 (366,'http://supernarede.local/',''),
 (367,'http://supernarede.local/',''),
 (368,'http://supernarede.local/',''),
 (369,'http://supernarede.local/',''),
 (370,'http://supernarede.local/images/destaques/bt_comprar.png','http://supernarede.local/'),
 (371,'http://supernarede.local/images/destaques/bt_quantidade.png','http://supernarede.local/'),
 (372,'http://supernarede.local/',''),
 (373,'http://supernarede.local/',''),
 (374,'http://supernarede.local/',''),
 (375,'http://supernarede.local/',''),
 (376,'http://supernarede.local/',''),
 (377,'http://supernarede.local/',''),
 (378,'http://supernarede.local/',''),
 (379,'http://supernarede.local/',''),
 (380,'http://supernarede.local/checkout/cart/add/uenc/aHR0cDovL3N1cGVybmFyZWRlLmxvY2FsLw,,/product/5/?qty=1','http://supernarede.local/'),
 (381,'http://supernarede.local/checkout/cart/','http://supernarede.local/'),
 (382,'http://supernarede.local/j2tajaxcheckout/index/cartdelete/cart/delete/id/1/uenc/aHR0cDovL3N1cGVybmFyZWRlLmxvY2FsL2NoZWNrb3V0L2NhcnQv/','http://supernarede.local/checkout/cart/'),
 (383,'http://supernarede.local/','http://supernarede.local/checkout/cart/'),
 (384,'http://supernarede.local/','http://supernarede.local/checkout/cart/'),
 (385,'http://supernarede.local/','http://supernarede.local/checkout/cart/'),
 (386,'http://supernarede.local/j2tajaxcheckout/index/cart/cart/add/uenc/aHR0cDovL3N1cGVybmFyZWRlLmxvY2FsLw,,/product/5/','http://supernarede.local/'),
 (387,'http://supernarede.local/','http://supernarede.local/checkout/cart/'),
 (388,'http://supernarede.local/j2tajaxcheckout/index/cart/cart/add/uenc/aHR0cDovL3N1cGVybmFyZWRlLmxvY2FsLw,,/product/5/','http://supernarede.local/'),
 (389,'http://supernarede.local/','http://supernarede.local/checkout/cart/'),
 (390,'http://supernarede.local/','http://supernarede.local/checkout/cart/'),
 (391,'http://supernarede.local/j2tajaxcheckout/index/cart/cart/add/uenc/aHR0cDovL3N1cGVybmFyZWRlLmxvY2FsLw,,/product/5/qty/15','http://supernarede.local/'),
 (392,'http://supernarede.local/checkout/cart/','http://supernarede.local/'),
 (393,'http://supernarede.local/checkout/cart/updatePost/','http://supernarede.local/checkout/cart/'),
 (394,'http://supernarede.local/checkout/cart/','http://supernarede.local/checkout/cart/'),
 (395,'http://supernarede.local/checkout/cart/configure/id/2/','http://supernarede.local/checkout/cart/'),
 (396,'http://supernarede.local/','http://supernarede.local/checkout/cart/configure/id/2/'),
 (397,'http://supernarede.local/','http://supernarede.local/checkout/cart/configure/id/2/'),
 (398,'http://supernarede.local/','http://supernarede.local/checkout/cart/configure/id/2/'),
 (399,'http://supernarede.local/','http://supernarede.local/checkout/cart/configure/id/2/'),
 (400,'http://supernarede.local/','http://supernarede.local/checkout/cart/configure/id/2/'),
 (401,'http://supernarede.local/','http://supernarede.local/checkout/cart/configure/id/2/'),
 (402,'http://supernarede.local/j2tajaxcheckout/index/cart/cart/add/uenc/aHR0cDovL3N1cGVybmFyZWRlLmxvY2FsLw,,/product/3/qty/3','http://supernarede.local/'),
 (403,'http://supernarede.local/checkout/cart/','http://supernarede.local/'),
 (404,'http://supernarede.local/j2tajaxcheckout/index/cartdelete/cart/delete/id/3/uenc/aHR0cDovL3N1cGVybmFyZWRlLmxvY2FsL2NoZWNrb3V0L2NhcnQv/','http://supernarede.local/checkout/cart/'),
 (405,'http://supernarede.local/','http://supernarede.local/checkout/cart/'),
 (406,'http://supernarede.local/j2tajaxcheckout/index/cart/cart/add/uenc/aHR0cDovL3N1cGVybmFyZWRlLmxvY2FsLw,,/product/3/qty/7','http://supernarede.local/'),
 (407,'http://supernarede.local/checkout/cart/','http://supernarede.local/'),
 (408,'http://supernarede.local/j2tajaxcheckout/index/cartdelete/cart/delete/id/4/uenc/aHR0cDovL3N1cGVybmFyZWRlLmxvY2FsL2NoZWNrb3V0L2NhcnQv/','http://supernarede.local/checkout/cart/'),
 (409,'http://supernarede.local/','http://supernarede.local/checkout/cart/'),
 (410,'http://supernarede.local/','http://supernarede.local/checkout/cart/'),
 (411,'http://supernarede.local/','http://supernarede.local/checkout/cart/'),
 (412,'http://supernarede.local/','http://supernarede.local/checkout/cart/'),
 (413,'http://supernarede.local/','http://supernarede.local/checkout/cart/'),
 (414,'http://supernarede.local/j2tajaxcheckout/index/cart/cart/add/uenc/aHR0cDovL3N1cGVybmFyZWRlLmxvY2FsLw,,/product/3/qty/1','http://supernarede.local/'),
 (415,'http://supernarede.local/j2tajaxcheckout/index/cart/cart/add/uenc/aHR0cDovL3N1cGVybmFyZWRlLmxvY2FsLw,,/product/3/qty/1','http://supernarede.local/'),
 (416,'http://supernarede.local/','http://supernarede.local/checkout/cart/'),
 (417,'http://supernarede.local/catalog/product/view/id/5','http://supernarede.local/'),
 (418,'http://supernarede.local/catalog/product/gallery/id/5/image/8/','http://supernarede.local/cerveja-exemplo-skol-escura'),
 (419,'http://supernarede.local/catalog/product/gallery/id/5/image/10/','http://supernarede.local/cerveja-exemplo-skol-escura'),
 (420,'http://supernarede.local/catalog/product/gallery/id/5/image/9/','http://supernarede.local/cerveja-exemplo-skol-escura'),
 (421,'http://supernarede.local/catalog/product/view/id/5','http://supernarede.local/'),
 (422,'http://supernarede.local/catalog/product/view/id/5','http://supernarede.local/'),
 (423,'http://supernarede.local/catalog/product/view/id/5','http://supernarede.local/'),
 (424,'http://supernarede.local/catalog/product/view/id/5','http://supernarede.local/'),
 (425,'http://supernarede.local/catalog/product/view/id/5','http://supernarede.local/'),
 (426,'http://supernarede.local/catalog/product/view/id/5','http://supernarede.local/'),
 (427,'http://supernarede.local/catalog/category/view/id/9','http://supernarede.local/cerveja-exemplo-skol-escura'),
 (428,'http://supernarede.local/catalog/product/view/id/1/category/9','http://supernarede.local/departamentos/bebidas/cerveja'),
 (429,'http://supernarede.local/departamentos/bebidas/cerveja/images/destaques/bt_finalizar-compra.png','http://supernarede.local/departamentos/bebidas/cerveja/cerveja-exemplo-sem-alcool'),
 (430,'http://supernarede.local/departamentos/bebidas/cerveja/images/destaques/bt_comprar.png','http://supernarede.local/departamentos/bebidas/cerveja/cerveja-exemplo-sem-alcool'),
 (431,'http://supernarede.local/departamentos/bebidas/cerveja/images/destaques/bt_quantidade.png','http://supernarede.local/departamentos/bebidas/cerveja/cerveja-exemplo-sem-alcool'),
 (432,'http://supernarede.local/','http://supernarede.local/departamentos/bebidas/cerveja/cerveja-exemplo-sem-alcool'),
 (433,'http://supernarede.local/catalog/product/view/id/5','http://supernarede.local/'),
 (434,'http://supernarede.local/images/destaques/bt_quantidade.png','http://supernarede.local/cerveja-exemplo-skol-escura'),
 (435,'http://supernarede.local/images/destaques/bt_finalizar-compra.png','http://supernarede.local/cerveja-exemplo-skol-escura'),
 (436,'http://supernarede.local/images/destaques/bt_comprar.png','http://supernarede.local/cerveja-exemplo-skol-escura'),
 (437,'http://supernarede.local/catalogsearch/result/?cat=7&q=skol&x=0&y=0&cf=0&q=','http://supernarede.local/cerveja-exemplo-skol-escura'),
 (438,'http://supernarede.local/catalog/product/view/id/5','http://supernarede.local/cerveja-exemplo-skol-escura'),
 (439,'http://supernarede.local/images/destaques/bt_quantidade.png','http://supernarede.local/cerveja-exemplo-skol-escura'),
 (440,'http://supernarede.local/images/destaques/bt_finalizar-compra.png','http://supernarede.local/cerveja-exemplo-skol-escura'),
 (441,'http://supernarede.local/images/destaques/bt_comprar.png','http://supernarede.local/cerveja-exemplo-skol-escura'),
 (442,'http://supernarede.local/catalogsearch/result/?cat=7&q=skol&x=43&y=48&cf=0&q=','http://supernarede.local/cerveja-exemplo-skol-escura'),
 (443,'http://supernarede.local/catalog/product/view/id/5','http://supernarede.local/cerveja-exemplo-skol-escura'),
 (444,'http://supernarede.local/images/destaques/bt_quantidade.png','http://supernarede.local/cerveja-exemplo-skol-escura'),
 (445,'http://supernarede.local/images/destaques/bt_finalizar-compra.png','http://supernarede.local/cerveja-exemplo-skol-escura'),
 (446,'http://supernarede.local/images/destaques/bt_comprar.png','http://supernarede.local/cerveja-exemplo-skol-escura'),
 (447,'http://supernarede.local/catalog/product/view/id/5','http://supernarede.local/cerveja-exemplo-skol-escura'),
 (448,'http://supernarede.local/images/destaques/bt_finalizar-compra.png','http://supernarede.local/cerveja-exemplo-skol-escura'),
 (449,'http://supernarede.local/images/destaques/bt_quantidade.png','http://supernarede.local/cerveja-exemplo-skol-escura'),
 (450,'http://supernarede.local/images/destaques/bt_comprar.png','http://supernarede.local/cerveja-exemplo-skol-escura'),
 (451,'http://supernarede.local/catalogsearch/result/?cat=7&q=skol&x=18&y=55','http://supernarede.local/cerveja-exemplo-skol-escura'),
 (452,'http://supernarede.local/catalog/product/view/id/5','http://supernarede.local/catalogsearch/result/?cat=7&q=skol&x=18&y=55'),
 (453,'http://supernarede.local/images/destaques/bt_finalizar-compra.png','http://supernarede.local/cerveja-exemplo-skol-escura'),
 (454,'http://supernarede.local/images/destaques/bt_comprar.png','http://supernarede.local/cerveja-exemplo-skol-escura'),
 (455,'http://supernarede.local/images/destaques/bt_quantidade.png','http://supernarede.local/cerveja-exemplo-skol-escura'),
 (456,'http://supernarede.local/catalog/product/view/id/5',''),
 (457,'http://supernarede.local/images/destaques/bt_comprar.png','http://supernarede.local/cerveja-exemplo-skol-escura'),
 (458,'http://supernarede.local/images/destaques/bt_quantidade.png','http://supernarede.local/cerveja-exemplo-skol-escura'),
 (459,'http://supernarede.local/images/destaques/bt_finalizar-compra.png','http://supernarede.local/cerveja-exemplo-skol-escura'),
 (460,'http://supernarede.local/catalog/product/view/id/5','http://supernarede.local/catalogsearch/result/?cat=7&q=skol&x=18&y=55'),
 (461,'http://supernarede.local/catalog/product/view/id/5','http://supernarede.local/catalogsearch/result/?cat=7&q=skol&x=18&y=55'),
 (462,'http://supernarede.local/catalog/product/view/id/5','http://supernarede.local/catalogsearch/result/?cat=7&q=skol&x=18&y=55'),
 (463,'http://supernarede.local/','http://supernarede.local/cerveja-exemplo-skol-escura'),
 (464,'http://supernarede.local/','http://supernarede.local/'),
 (465,'http://supernarede.local/','http://supernarede.local/'),
 (466,'http://supernarede.local/','http://supernarede.local/'),
 (467,'http://supernarede.local/image','http://supernarede.local/'),
 (468,'http://supernarede.local/images','http://supernarede.local/'),
 (469,'http://supernarede.local/images/','http://supernarede.local/'),
 (470,'http://supernarede.local/images/destaques','http://supernarede.local/'),
 (471,'http://supernarede.local/images/destaques/','http://supernarede.local/'),
 (472,'http://supernarede.local/images/destaques/img_produto-indisponivel','http://supernarede.local/'),
 (473,'http://supernarede.local/images/destaques/img_produto-indisponivel.png','http://supernarede.local/'),
 (474,'http://supernarede.local/images/destaques/img_produto-indisponivel.png',''),
 (475,'http://supernarede.local/','http://supernarede.local/cerveja-exemplo-skol-escura'),
 (476,'http://supernarede.local/','http://supernarede.local/cerveja-exemplo-skol-escura'),
 (477,'http://supernarede.local/','http://supernarede.local/cerveja-exemplo-skol-escura'),
 (478,'http://supernarede.local/','http://supernarede.local/cerveja-exemplo-skol-escura'),
 (479,'http://supernarede.local/catalog/category/view/id/9','http://supernarede.local/'),
 (480,'http://supernarede.local/contacts','http://supernarede.local/departamentos/bebidas/cerveja'),
 (481,'http://supernarede.local/','http://supernarede.local/contacts'),
 (482,'http://supernarede.local/','http://supernarede.local/contacts'),
 (483,'http://supernarede.local/','http://supernarede.local/contacts'),
 (484,'http://supernarede.local/','http://supernarede.local/contacts'),
 (485,'http://supernarede.local/','http://supernarede.local/contacts'),
 (486,'http://supernarede.local/catalog/product/view/id/4','http://supernarede.local/'),
 (487,'http://supernarede.local/catalog/product/view/id/4','http://supernarede.local/'),
 (488,'http://supernarede.local/catalog/category/view/id/9','http://supernarede.local/cerveja-exemplo-heineken-long-1'),
 (489,'http://supernarede.local/catalog/category/view/id/7','http://supernarede.local/departamentos/bebidas/cerveja'),
 (490,'http://supernarede.local/catalog/category/view/id/3','http://supernarede.local/departamentos/bebidas/cerveja'),
 (491,'http://supernarede.local/catalog/category/view/id/9','http://supernarede.local/cerveja-exemplo-heineken-long-1'),
 (492,'http://supernarede.local/catalog/category/view/id/9','http://supernarede.local/cerveja-exemplo-heineken-long-1'),
 (493,'http://supernarede.local/','http://supernarede.local/departamentos/bebidas/cerveja'),
 (494,'http://supernarede.local/catalog/category/view/id/9','http://supernarede.local/cerveja-exemplo-heineken-long-1'),
 (495,'http://supernarede.local/catalog/category/view/id/9','http://supernarede.local/cerveja-exemplo-heineken-long-1'),
 (496,'http://supernarede.local/','http://supernarede.local/departamentos/bebidas/cerveja'),
 (497,'http://supernarede.local/','http://supernarede.local/departamentos/bebidas/cerveja'),
 (498,'http://supernarede.local/catalog/product/view/id/4','http://supernarede.local/'),
 (499,'http://supernarede.local/','http://supernarede.local/departamentos/bebidas/cerveja'),
 (500,'http://supernarede.local/catalog/product/view/id/3','http://supernarede.local/'),
 (501,'http://supernarede.local/catalog/product/view/id/3','http://supernarede.local/'),
 (502,'http://supernarede.local/','http://supernarede.local/cerveja-exemplo-heineken-long'),
 (503,'http://supernarede.local/','http://supernarede.local/cerveja-exemplo-heineken-long'),
 (504,'http://supernarede.local/','http://supernarede.local/cerveja-exemplo-heineken-long'),
 (505,'http://supernarede.local/','http://supernarede.local/cerveja-exemplo-heineken-long'),
 (506,'http://supernarede.local/','http://supernarede.local/cerveja-exemplo-heineken-long'),
 (507,'http://supernarede.local/','http://supernarede.local/cerveja-exemplo-heineken-long'),
 (508,'http://supernarede.local/','http://supernarede.local/cerveja-exemplo-heineken-long'),
 (509,'http://supernarede.local/','http://supernarede.local/cerveja-exemplo-heineken-long'),
 (510,'http://supernarede.local/','http://supernarede.local/cerveja-exemplo-heineken-long'),
 (511,'http://supernarede.local/','http://supernarede.local/cerveja-exemplo-heineken-long'),
 (512,'http://supernarede.local/','http://supernarede.local/cerveja-exemplo-heineken-long'),
 (513,'http://supernarede.local/','http://supernarede.local/cerveja-exemplo-heineken-long'),
 (514,'http://supernarede.local/','http://supernarede.local/cerveja-exemplo-heineken-long'),
 (515,'http://supernarede.local/','http://supernarede.local/cerveja-exemplo-heineken-long'),
 (516,'http://supernarede.local/','http://supernarede.local/cerveja-exemplo-heineken-long'),
 (517,'http://supernarede.local/','http://supernarede.local/cerveja-exemplo-heineken-long'),
 (518,'http://supernarede.local/','http://supernarede.local/cerveja-exemplo-heineken-long'),
 (519,'http://supernarede.local/','http://supernarede.local/'),
 (520,'http://supernarede.local/faq','http://supernarede.local/'),
 (521,'http://supernarede.local/catalog/category/view/id/9','http://supernarede.local/faq'),
 (522,'http://supernarede.local/catalog/category/view/id/10',''),
 (523,'http://supernarede.local/faq','http://supernarede.local/departamentos/bebidas/agua'),
 (524,'http://supernarede.local/faq','http://supernarede.local/departamentos/bebidas/agua'),
 (525,'http://supernarede.local/catalog/category/view/id/10',''),
 (526,'http://supernarede.local/catalog/category/view/id/10',''),
 (527,'http://supernarede.local/catalog/category/view/id/9','http://supernarede.local/departamentos/bebidas/agua'),
 (528,'http://supernarede.local/j2tajaxcheckout/index/cart/cart/add/uenc/aHR0cDovL3N1cGVybmFyZWRlLmxvY2FsL2RlcGFydGFtZW50b3MvYmViaWRhcy9jZXJ2ZWph/product/2/qty/1','http://supernarede.local/departamentos/bebidas/cerveja'),
 (529,'http://supernarede.local/checkout/cart/','http://supernarede.local/departamentos/bebidas/cerveja'),
 (530,'http://supernarede.local/checkout/cart/updatePost/','http://supernarede.local/checkout/cart/'),
 (531,'http://supernarede.local/checkout/cart/','http://supernarede.local/departamentos/bebidas/cerveja'),
 (532,'http://supernarede.local/checkout/cart/','http://supernarede.local/departamentos/bebidas/cerveja'),
 (533,'http://supernarede.local/checkout/cart/','http://supernarede.local/departamentos/bebidas/cerveja'),
 (534,'http://supernarede.local/checkout/cart/','http://supernarede.local/departamentos/bebidas/cerveja'),
 (535,'http://supernarede.local/checkout/cart/','http://supernarede.local/departamentos/bebidas/cerveja'),
 (536,'http://supernarede.local/checkout/cart/','http://supernarede.local/departamentos/bebidas/cerveja'),
 (537,'http://supernarede.local/checkout/cart/','http://supernarede.local/departamentos/bebidas/cerveja'),
 (538,'http://supernarede.local/checkout/cart/','http://supernarede.local/departamentos/bebidas/cerveja'),
 (539,'http://supernarede.local/checkout/cart/','http://supernarede.local/departamentos/bebidas/cerveja'),
 (540,'http://supernarede.local/checkout/cart/','http://supernarede.local/departamentos/bebidas/cerveja'),
 (541,'http://supernarede.local/','http://supernarede.local/checkout/cart/'),
 (542,'http://supernarede.local/j2tajaxcheckout/index/cart/cart/add/uenc/aHR0cDovL3N1cGVybmFyZWRlLmxvY2FsLw,,/product/3/qty/3','http://supernarede.local/'),
 (543,'http://supernarede.local/checkout/cart/','http://supernarede.local/'),
 (544,'http://supernarede.local/checkout/cart/images/destaques/bt_quantidade.png','http://supernarede.local/checkout/cart/'),
 (545,'http://supernarede.local/checkout/cart/images/template/bt_fechar-compra-facil.png','http://supernarede.local/checkout/cart/'),
 (546,'http://supernarede.local/checkout/cart/images/destaques/img_produto.png','http://supernarede.local/checkout/cart/'),
 (547,'http://supernarede.local/checkout/cart/images/destaques/img_produto.png','http://supernarede.local/checkout/cart/'),
 (548,'http://supernarede.local/checkout/cart/images/template/bt_fechar-compra-facil.png','http://supernarede.local/checkout/cart/'),
 (549,'http://supernarede.local/checkout/cart/images/destaques/bt_quantidade.png','http://supernarede.local/checkout/cart/'),
 (550,'http://supernarede.local/checkout/cart/','http://supernarede.local/'),
 (551,'http://supernarede.local/checkout/cart/images/template/bt_fechar-compra-facil.png','http://supernarede.local/checkout/cart/'),
 (552,'http://supernarede.local/checkout/cart/images/destaques/bt_quantidade.png','http://supernarede.local/checkout/cart/'),
 (553,'http://supernarede.local/checkout/cart/images/destaques/img_produto.png','http://supernarede.local/checkout/cart/'),
 (554,'http://supernarede.local/checkout/cart/','http://supernarede.local/'),
 (555,'http://supernarede.local/checkout/cart/images/destaques/bt_quantidade.png','http://supernarede.local/checkout/cart/'),
 (556,'http://supernarede.local/checkout/cart/images/destaques/img_produto.png','http://supernarede.local/checkout/cart/'),
 (557,'http://supernarede.local/checkout/cart/images/template/bt_fechar-compra-facil.png','http://supernarede.local/checkout/cart/'),
 (558,'http://supernarede.local/checkout/cart/','http://supernarede.local/'),
 (559,'http://supernarede.local/checkout/cart/images/template/bt_fechar-compra-facil.png','http://supernarede.local/checkout/cart/'),
 (560,'http://supernarede.local/checkout/cart/images/destaques/img_produto.png','http://supernarede.local/checkout/cart/'),
 (561,'http://supernarede.local/checkout/cart/images/destaques/bt_quantidade.png','http://supernarede.local/checkout/cart/'),
 (562,'http://supernarede.local/checkout/cart/','http://supernarede.local/'),
 (563,'http://supernarede.local/checkout/cart/images/template/bt_fechar-compra-facil.png','http://supernarede.local/checkout/cart/'),
 (564,'http://supernarede.local/checkout/cart/images/destaques/img_produto.png','http://supernarede.local/checkout/cart/'),
 (565,'http://supernarede.local/checkout/cart/images/destaques/bt_quantidade.png','http://supernarede.local/checkout/cart/'),
 (566,'http://supernarede.local/checkout/cart/','http://supernarede.local/'),
 (567,'http://supernarede.local/checkout/cart/images/destaques/img_produto.png','http://supernarede.local/checkout/cart/'),
 (568,'http://supernarede.local/checkout/cart/images/destaques/bt_quantidade.png','http://supernarede.local/checkout/cart/'),
 (569,'http://supernarede.local/checkout/cart/images/template/bt_fechar-compra-facil.png','http://supernarede.local/checkout/cart/'),
 (570,'http://supernarede.local/checkout/cart/','http://supernarede.local/'),
 (571,'http://supernarede.local/checkout/cart/images/template/bt_fechar-compra-facil.png','http://supernarede.local/checkout/cart/'),
 (572,'http://supernarede.local/checkout/cart/','http://supernarede.local/'),
 (573,'http://supernarede.local/checkout/cart/updatePost/','http://supernarede.local/checkout/cart/'),
 (574,'http://supernarede.local/checkout/cart/','http://supernarede.local/checkout/cart/'),
 (575,'http://supernarede.local/checkout/cart/updatePost/','http://supernarede.local/checkout/cart/'),
 (576,'http://supernarede.local/checkout/cart/','http://supernarede.local/checkout/cart/'),
 (577,'http://supernarede.local/checkout/cart/','http://supernarede.local/checkout/cart/'),
 (578,'http://supernarede.local/checkout/cart/','http://supernarede.local/checkout/cart/'),
 (579,'http://supernarede.local/checkout/cart/','http://supernarede.local/checkout/cart/'),
 (580,'http://supernarede.local/checkout/cart/','http://supernarede.local/checkout/cart/'),
 (581,'http://supernarede.local/checkout/cart/','http://supernarede.local/checkout/cart/'),
 (582,'http://supernarede.local/checkout/cart/','http://supernarede.local/checkout/cart/'),
 (583,'http://supernarede.local/checkout/cart/','http://supernarede.local/checkout/cart/'),
 (584,'http://supernarede.local/checkout/cart/','http://supernarede.local/checkout/cart/'),
 (585,'http://supernarede.local/checkout/cart/','http://supernarede.local/checkout/cart/'),
 (586,'http://supernarede.local/checkout/cart/','http://supernarede.local/checkout/cart/'),
 (587,'http://supernarede.local/','http://supernarede.local/checkout/cart/'),
 (588,'http://supernarede.local/j2tajaxcheckout/index/cart/cart/add/uenc/aHR0cDovL3N1cGVybmFyZWRlLmxvY2FsLw,,/product/5/qty/1','http://supernarede.local/'),
 (589,'http://supernarede.local/checkout/cart/','http://supernarede.local/'),
 (590,'http://supernarede.local/checkout/cart/','http://supernarede.local/'),
 (591,'http://supernarede.local/','http://supernarede.local/checkout/cart/'),
 (592,'http://supernarede.local/catalog/category/view/id/9','http://supernarede.local/'),
 (593,'http://supernarede.local/checkout/cart/','http://supernarede.local/departamentos/bebidas/cerveja'),
 (594,'http://supernarede.local/catalogsearch/result/?cat=&q=&x=43&y=39','http://supernarede.local/checkout/cart/'),
 (595,'http://supernarede.local/catalogsearch/result/?cat=&q=&x=43&y=39','http://supernarede.local/checkout/cart/'),
 (596,'http://supernarede.local/catalogsearch/result/?cat=&q=&x=43&y=39','http://supernarede.local/checkout/cart/'),
 (597,'http://supernarede.local/catalogsearch/result/?cat=&q=skol&x=0&y=0','http://supernarede.local/catalogsearch/result/?cat=&q=&x=43&y=39'),
 (598,'http://supernarede.local/catalogsearch/result/?cat=8&q=skol&x=21&y=39','http://supernarede.local/catalogsearch/result/?cat=&q=skol&x=0&y=0'),
 (599,'http://supernarede.local/catalogsearch/result/?cat=7&q=skol&x=32&y=30','http://supernarede.local/catalogsearch/result/?cat=8&q=skol&x=21&y=39'),
 (600,'http://supernarede.local/catalogsearch/result/?cat=7&q=skol&x=32&y=30','http://supernarede.local/catalogsearch/result/?cat=8&q=skol&x=21&y=39'),
 (601,'http://supernarede.local/catalogsearch/result/?cat=7&q=skol&x=32&y=30','http://supernarede.local/catalogsearch/result/?cat=8&q=skol&x=21&y=39'),
 (602,'http://supernarede.local/catalogsearch/result/?cat=7&q=skol&x=32&y=30','http://supernarede.local/catalogsearch/result/?cat=8&q=skol&x=21&y=39'),
 (603,'http://supernarede.local/catalogsearch/result/?cat=14&q=skol&x=40&y=33','http://supernarede.local/catalogsearch/result/?cat=7&q=skol&x=32&y=30'),
 (604,'http://supernarede.local/catalogsearch/result/?cat=14&q=skol&x=40&y=33','http://supernarede.local/catalogsearch/result/?cat=7&q=skol&x=32&y=30'),
 (605,'http://supernarede.local/catalogsearch/result/?cat=14&q=skol&x=40&y=33','http://supernarede.local/catalogsearch/result/?cat=7&q=skol&x=32&y=30'),
 (606,'http://supernarede.local/catalogsearch/result/?cat=14&q=skol&x=40&y=33','http://supernarede.local/catalogsearch/result/?cat=7&q=skol&x=32&y=30'),
 (607,'http://supernarede.local/catalogsearch/result/?cat=14&q=skol&x=40&y=33','http://supernarede.local/catalogsearch/result/?cat=7&q=skol&x=32&y=30'),
 (608,'http://supernarede.local/catalogsearch/result/?cat=14&q=skol&x=40&y=33','http://supernarede.local/catalogsearch/result/?cat=7&q=skol&x=32&y=30'),
 (609,'http://supernarede.local/catalogsearch/result/?cat=14&q=skol&x=40&y=33','http://supernarede.local/catalogsearch/result/?cat=7&q=skol&x=32&y=30'),
 (610,'http://supernarede.local/catalogsearch/result/?cat=14&q=skol&x=40&y=33','http://supernarede.local/catalogsearch/result/?cat=7&q=skol&x=32&y=30'),
 (611,'http://supernarede.local/catalogsearch/result/index/?cat=3&q=skol&x=40&y=33','http://supernarede.local/catalogsearch/result/?cat=14&q=skol&x=40&y=33'),
 (612,'http://supernarede.local/catalogsearch/result/index/?cat=3&q=skol&x=40&y=33','http://supernarede.local/catalogsearch/result/?cat=14&q=skol&x=40&y=33'),
 (613,'http://supernarede.local/catalogsearch/result/index/?q=skol&x=40&y=33','http://supernarede.local/catalogsearch/result/index/?cat=3&q=skol&x=40&y=33'),
 (614,'http://supernarede.local/catalogsearch/result/?cat=7&q=skol&x=32&y=42','http://supernarede.local/catalogsearch/result/index/?q=skol&x=40&y=33'),
 (615,'http://supernarede.local/catalogsearch/result/index/?cat=3&q=skol&x=32&y=42','http://supernarede.local/catalogsearch/result/?cat=7&q=skol&x=32&y=42'),
 (616,'http://supernarede.local/catalogsearch/result/index/?q=skol&x=32&y=42','http://supernarede.local/catalogsearch/result/index/?cat=3&q=skol&x=32&y=42'),
 (617,'http://supernarede.local/catalogsearch/result/index/?q=skol&x=32&y=42','http://supernarede.local/catalogsearch/result/index/?cat=3&q=skol&x=32&y=42'),
 (618,'http://supernarede.local/catalogsearch/result/?cat=7&q=skol&x=14&y=48','http://supernarede.local/catalogsearch/result/index/?q=skol&x=32&y=42'),
 (619,'http://supernarede.local/catalog/product/view/id/5','http://supernarede.local/catalogsearch/result/?cat=7&q=skol&x=14&y=48'),
 (620,'http://supernarede.local/catalog/category/view/id/9','http://supernarede.local/cerveja-exemplo-skol-escura'),
 (621,'http://supernarede.local/j2tajaxcheckout/index/cart/cart/add/uenc/aHR0cDovL3N1cGVybmFyZWRlLmxvY2FsL2RlcGFydGFtZW50b3MvYmViaWRhcy9jZXJ2ZWph/product/2/qty/1','http://supernarede.local/departamentos/bebidas/cerveja'),
 (622,'http://supernarede.local/checkout/cart/','http://supernarede.local/departamentos/bebidas/cerveja'),
 (623,'http://supernarede.local/checkout/cart/','http://supernarede.local/departamentos/bebidas/cerveja'),
 (624,'http://supernarede.local/checkout/cart/','http://supernarede.local/departamentos/bebidas/cerveja'),
 (625,'http://supernarede.local/checkout/cart/','http://supernarede.local/departamentos/bebidas/cerveja'),
 (626,'http://supernarede.local/checkout/cart/','http://supernarede.local/departamentos/bebidas/cerveja'),
 (627,'http://supernarede.local/j2tajaxcheckout/index/cartdelete/cart/delete/id/8/uenc/aHR0cDovL3N1cGVybmFyZWRlLmxvY2FsL2NoZWNrb3V0L2NhcnQv/','http://supernarede.local/checkout/cart/'),
 (628,'http://supernarede.local/catalog/category/view/id/9','http://supernarede.local/checkout/cart/'),
 (629,'http://supernarede.local/catalog/category/view/id/9','http://supernarede.local/checkout/cart/'),
 (630,'http://supernarede.local/catalog/category/view/id/9?dir=desc&order=position','http://supernarede.local/departamentos/bebidas/cerveja'),
 (631,'http://supernarede.local/catalog/category/view/id/9?dir=asc&order=position','http://supernarede.local/departamentos/bebidas/cerveja?dir=desc&order=position'),
 (632,'http://supernarede.local/catalog/category/view/id/9?dir=asc&order=position','http://supernarede.local/departamentos/bebidas/cerveja?dir=desc&order=position'),
 (633,'http://supernarede.local/catalog/category/view/id/9?dir=asc&order=position','http://supernarede.local/departamentos/bebidas/cerveja?dir=desc&order=position'),
 (634,'http://supernarede.local/catalog/category/view/id/9?dir=asc&order=position','http://supernarede.local/departamentos/bebidas/cerveja?dir=desc&order=position'),
 (635,'http://supernarede.local/checkout/cart/','http://supernarede.local/departamentos/bebidas/cerveja?dir=asc&order=position'),
 (636,'http://supernarede.local/checkout/onepage/','http://supernarede.local/checkout/cart/'),
 (637,'http://supernarede.local/customer/account/create/','http://supernarede.local/checkout/onepage/'),
 (638,'http://supernarede.local/customer/account/login/','http://supernarede.local/customer/account/create/'),
 (639,'http://supernarede.local/customer/account/login/','http://supernarede.local/customer/account/create/'),
 (640,'http://supernarede.local/customer/account/login/','http://supernarede.local/customer/account/create/'),
 (641,'http://supernarede.local/customer/account/login/','http://supernarede.local/customer/account/create/'),
 (642,'http://supernarede.local/customer/account/login/','http://supernarede.local/customer/account/create/'),
 (643,'http://supernarede.local/customer/account/login/','http://supernarede.local/customer/account/create/'),
 (644,'http://supernarede.local/customer/account/login/',''),
 (645,'http://supernarede.local/customer/account/login/',''),
 (646,'http://supernarede.local/customer/account/create/','http://supernarede.local/customer/account/login/'),
 (647,'http://supernarede.local/customer/account/login/','http://supernarede.local/customer/account/create/'),
 (648,'http://supernarede.local/','http://supernarede.local/customer/account/login/'),
 (649,'http://supernarede.local/j2tajaxcheckout/index/cart/cart/add/uenc/aHR0cDovL3N1cGVybmFyZWRlLmxvY2FsLw,,/product/3/qty/2','http://supernarede.local/'),
 (650,'http://supernarede.local/',''),
 (651,'http://supernarede.local/j2tajaxcheckout/index/cart/cart/add/uenc/aHR0cDovL3N1cGVybmFyZWRlLmxvY2FsLw,,/product/5/qty/1','http://supernarede.local/'),
 (652,'http://supernarede.local/j2tajaxcheckout/index/cart/cart/add/uenc/aHR0cDovL3N1cGVybmFyZWRlLmxvY2FsLw,,/product/2/qty/1','http://supernarede.local/'),
 (653,'http://supernarede.local/',''),
 (654,'http://supernarede.local/j2tajaxcheckout/index/cart/cart/add/uenc/aHR0cDovL3N1cGVybmFyZWRlLmxvY2FsLw,,/product/3/qty/1','http://supernarede.local/'),
 (655,'http://supernarede.local/',''),
 (656,'http://supernarede.local/',''),
 (657,'http://supernarede.local/',''),
 (658,'http://supernarede.local/j2tajaxcheckout/index/cart/cart/add/uenc/aHR0cDovL3N1cGVybmFyZWRlLmxvY2FsLw,,/product/4/qty/1','http://supernarede.local/'),
 (659,'http://supernarede.local/j2tajaxcheckout/index/cart/cart/add/uenc/aHR0cDovL3N1cGVybmFyZWRlLmxvY2FsLw,,/product/4/qty/1','http://supernarede.local/'),
 (660,'http://supernarede.local/checkout/cart/','http://supernarede.local/'),
 (661,'http://supernarede.local/catalog/product/view/id/2','http://supernarede.local/checkout/cart/'),
 (662,'http://supernarede.local/','http://supernarede.local/cerveja-exemplo-bavaria'),
 (663,'http://supernarede.local/catalog/product/view/id/4','http://supernarede.local/'),
 (664,'http://supernarede.local/j2tajaxcheckout/index/cart/cart/add/uenc/aHR0cDovL3N1cGVybmFyZWRlLmxvY2FsL2NlcnZlamEtZXhlbXBsby1oZWluZWtlbi1sb25nLTE,/product/4/','http://supernarede.local/cerveja-exemplo-heineken-long-1'),
 (665,'http://supernarede.local/checkout/cart/','http://supernarede.local/cerveja-exemplo-heineken-long-1'),
 (666,'http://supernarede.local/catalog/product/view/id/4','http://supernarede.local/checkout/cart/'),
 (667,'http://supernarede.local/customer/account/login/','http://supernarede.local/cerveja-exemplo-heineken-long-1'),
 (668,'http://supernarede.local/customer/account/forgotpassword/','http://supernarede.local/customer/account/login/'),
 (669,'http://supernarede.local/customer/account/forgotpassword/','http://supernarede.local/customer/account/login/'),
 (670,'http://supernarede.local/customer/account/forgotpassword/','http://supernarede.local/customer/account/login/'),
 (671,'http://supernarede.local/customer/account/forgotpassword/','http://supernarede.local/customer/account/login/'),
 (672,'http://supernarede.local/customer/account/forgotpassword/','http://supernarede.local/customer/account/login/'),
 (673,'http://supernarede.local/customer/account/forgotpasswordpost/','http://supernarede.local/customer/account/forgotpassword/'),
 (674,'http://supernarede.local/customer/account/forgotpassword/','http://supernarede.local/customer/account/forgotpassword/'),
 (675,'http://supernarede.local/customer/account/forgotpassword/','http://supernarede.local/customer/account/forgotpassword/'),
 (676,'http://supernarede.local/customer/account/forgotpassword/','http://supernarede.local/customer/account/forgotpassword/'),
 (677,'http://supernarede.local/customer/account/forgotpasswordpost/','http://supernarede.local/customer/account/forgotpassword/'),
 (678,'http://supernarede.local/customer/account/forgotpassword/','http://supernarede.local/customer/account/forgotpassword/'),
 (679,'http://supernarede.local/customer/account/login/','http://supernarede.local/customer/account/forgotpassword/'),
 (680,'http://supernarede.local/contacts','http://supernarede.local/customer/account/login/'),
 (681,'http://supernarede.local/customer/account/login/','http://supernarede.local/contacts'),
 (682,'http://supernarede.local/checkout/cart/','http://supernarede.local/customer/account/login/'),
 (683,'http://supernarede.local/checkout/onepage/','http://supernarede.local/checkout/cart/'),
 (684,'http://supernarede.local/checkout/onepage/','http://supernarede.local/checkout/cart/'),
 (685,'http://supernarede.local/checkout/onepage/','http://supernarede.local/checkout/cart/'),
 (686,'http://supernarede.local/checkout/onepage/','http://supernarede.local/checkout/cart/'),
 (687,'http://supernarede.local/checkout/onepage/','http://supernarede.local/checkout/cart/'),
 (688,'http://supernarede.local/checkout/onepage/','http://supernarede.local/checkout/cart/'),
 (689,'http://supernarede.local/checkout/onepage/','http://supernarede.local/checkout/cart/'),
 (690,'http://supernarede.local/checkout/onepage/','http://supernarede.local/checkout/cart/'),
 (691,'http://supernarede.local/checkout/onepage/','http://supernarede.local/checkout/cart/'),
 (692,'http://supernarede.local/checkout/onepage/','http://supernarede.local/checkout/cart/'),
 (693,'http://supernarede.local/checkout/onepage/','http://supernarede.local/checkout/cart/'),
 (694,'http://supernarede.local/checkout/onepage/','http://supernarede.local/checkout/cart/');
INSERT INTO `supernarede`.`log_url_info` VALUES  (695,'http://supernarede.local/checkout/onepage/saveMethod/','http://supernarede.local/checkout/onepage/'),
 (696,'http://supernarede.local/checkout/onepage/','http://supernarede.local/checkout/cart/'),
 (697,'http://supernarede.local/checkout/onepage/','http://supernarede.local/checkout/cart/'),
 (698,'http://supernarede.local/checkout/onepage/','http://supernarede.local/checkout/cart/'),
 (699,'http://supernarede.local/checkout/onepage/','http://supernarede.local/checkout/cart/'),
 (700,'http://supernarede.local/checkout/onepage/','http://supernarede.local/checkout/cart/'),
 (701,'http://supernarede.local/checkout/onepage/','http://supernarede.local/checkout/cart/'),
 (702,'http://supernarede.local/checkout/onepage/','http://supernarede.local/checkout/cart/'),
 (703,'http://supernarede.local/checkout/onepage/','http://supernarede.local/checkout/cart/'),
 (704,'http://supernarede.local/customer/account/loginPost/','http://supernarede.local/checkout/onepage/'),
 (705,'http://supernarede.local/checkout/onepage/index/','http://supernarede.local/checkout/onepage/'),
 (706,'http://supernarede.local/checkout/onepage/saveMethod/','http://supernarede.local/checkout/onepage/index/'),
 (707,'http://supernarede.local/checkout/onepage/saveMethod/','http://supernarede.local/checkout/onepage/index/'),
 (708,'http://supernarede.local/checkout/onepage/index/','http://supernarede.local/checkout/onepage/'),
 (709,'http://supernarede.local/checkout/onepage/saveMethod/','http://supernarede.local/checkout/onepage/index/'),
 (710,'http://supernarede.local/checkout/onepage/index/','http://supernarede.local/checkout/onepage/'),
 (711,'http://supernarede.local/checkout/onepage/index/',''),
 (712,'http://supernarede.local/checkout/cart/',''),
 (713,'http://supernarede.local/','http://supernarede.local/checkout/cart/'),
 (714,'http://supernarede.local/j2tajaxcheckout/index/cart/cart/add/uenc/aHR0cDovL3N1cGVybmFyZWRlLmxvY2FsLw,,/product/5/qty/1','http://supernarede.local/'),
 (715,'http://supernarede.local/checkout/cart/','http://supernarede.local/'),
 (716,'http://supernarede.local/checkout/onepage/','http://supernarede.local/checkout/cart/'),
 (717,'http://supernarede.local/checkout/onepage/saveMethod/','http://supernarede.local/checkout/onepage/'),
 (718,'http://supernarede.local/checkout/onepage/index/','http://supernarede.local/checkout/onepage/'),
 (719,'http://supernarede.local/checkout/onepage/saveMethod/','http://supernarede.local/checkout/onepage/index/'),
 (720,'http://supernarede.local/checkout/onepage/index/','http://supernarede.local/checkout/onepage/'),
 (721,'http://supernarede.local/checkout/onepage/index/','http://supernarede.local/checkout/onepage/'),
 (722,'http://supernarede.local/checkout/onepage/saveMethod/','http://supernarede.local/checkout/onepage/index/'),
 (723,'http://supernarede.local/checkout/onepage/index/','http://supernarede.local/checkout/onepage/'),
 (724,'http://supernarede.local/checkout/onepage/saveMethod/','http://supernarede.local/checkout/onepage/index/'),
 (725,'http://supernarede.local/checkout/onepage/index/','http://supernarede.local/checkout/onepage/'),
 (726,'http://supernarede.local/checkout/onepage/saveMethod/','http://supernarede.local/checkout/onepage/index/'),
 (727,'http://supernarede.local/checkout/onepage/index/','http://supernarede.local/checkout/onepage/'),
 (728,'http://supernarede.local/checkout/onepage/saveMethod/','http://supernarede.local/checkout/onepage/index/'),
 (729,'http://supernarede.local/',''),
 (730,'http://supernarede.local/',''),
 (731,'http://supernarede.local/',''),
 (732,'http://supernarede.local/checkout/onepage/saveMethod/','http://supernarede.local/checkout/onepage/index/'),
 (733,'http://supernarede.local/checkout/onepage/index/','http://supernarede.local/checkout/onepage/'),
 (734,'http://supernarede.local/checkout/cart/','http://supernarede.local/checkout/onepage/'),
 (735,'http://supernarede.local/','http://supernarede.local/checkout/cart/'),
 (736,'http://supernarede.local/customer/account/create/','http://supernarede.local/'),
 (737,'http://supernarede.local/customer/account/create/','http://supernarede.local/'),
 (738,'http://supernarede.local/customer/account/create/','http://supernarede.local/'),
 (739,'http://supernarede.local/customer/account/create/','http://supernarede.local/'),
 (740,'http://supernarede.local/catalog/category/view/id/4','http://supernarede.local/customer/account/create/'),
 (741,'http://supernarede.local/catalog/category/view/id/4','http://supernarede.local/customer/account/create/'),
 (742,'http://supernarede.local/catalog/category/view/id/4','http://supernarede.local/customer/account/create/'),
 (743,'http://supernarede.local/catalog/category/view/id/4','http://supernarede.local/customer/account/create/'),
 (744,'http://supernarede.local/catalog/category/view/id/6','http://supernarede.local/listas-prontas'),
 (745,'http://supernarede.local/catalog/product/view/id/6/category/6','http://supernarede.local/listas-prontas/somente-bebidas'),
 (746,'http://supernarede.local/padaria',''),
 (747,'http://supernarede.local/catalog/category/view/id/13','http://supernarede.local/padaria'),
 (748,'http://supernarede.local/catalog/category/view/id/12','http://supernarede.local/departamentos/massa/instantanea'),
 (749,'http://supernarede.local/catalog/category/view/id/12','http://supernarede.local/departamentos/massa/instantanea'),
 (750,'http://supernarede.local/catalog/category/view/id/12','http://supernarede.local/departamentos/massa/instantanea'),
 (751,'http://supernarede.local/catalog/category/view/id/12','http://supernarede.local/departamentos/massa/instantanea'),
 (752,'http://supernarede.local/catalog/category/view/id/12','http://supernarede.local/departamentos/massa/instantanea'),
 (753,'http://supernarede.local/catalog/category/view/id/12','http://supernarede.local/departamentos/massa/instantanea'),
 (754,'http://supernarede.local/catalogsearch/result/?cat=&q=teste&x=0&y=0','http://supernarede.local/departamentos/massa/espaguete'),
 (755,'http://supernarede.local/catalogsearch/result/?cat=&q=teste&x=0&y=0','http://supernarede.local/departamentos/massa/espaguete'),
 (756,'http://supernarede.local/catalogsearch/result/?cat=&q=teste&x=0&y=0','http://supernarede.local/departamentos/massa/espaguete'),
 (757,'http://supernarede.local/catalogsearch/result/?cat=&q=teste&x=0&y=0','http://supernarede.local/departamentos/massa/espaguete'),
 (758,'http://supernarede.local/catalogsearch/result/?cat=&q=teste&x=0&y=0','http://supernarede.local/departamentos/massa/espaguete'),
 (759,'http://supernarede.local/',''),
 (760,'http://supernarede.local/catalogsearch/result/?cat=&q=teste&x=0&y=0','http://supernarede.local/'),
 (761,'http://supernarede.local/faq','http://supernarede.local/catalogsearch/result/?cat=&q=teste&x=0&y=0'),
 (762,'http://supernarede.local/faq','http://supernarede.local/catalogsearch/result/?cat=&q=teste&x=0&y=0'),
 (763,'http://supernarede.local/faq','http://supernarede.local/catalogsearch/result/?cat=&q=teste&x=0&y=0'),
 (764,'http://supernarede.local/faq','http://supernarede.local/catalogsearch/result/?cat=&q=teste&x=0&y=0'),
 (765,'http://supernarede.local/faq','http://supernarede.local/catalogsearch/result/?cat=&q=teste&x=0&y=0'),
 (766,'http://supernarede.local/catalogsearch/result/?cat=&q=teste&x=0&y=0','http://supernarede.local/departamentos/massa/espaguete'),
 (767,'http://supernarede.local/catalogsearch/result/?cat=&q=teste&x=0&y=0','http://supernarede.local/departamentos/massa/espaguete'),
 (768,'http://supernarede.local/catalogsearch/result/?cat=&q=teste&x=0&y=0','http://supernarede.local/departamentos/massa/espaguete'),
 (769,'http://supernarede.local/catalogsearch/result/?cat=&q=teste&x=0&y=0','http://supernarede.local/departamentos/massa/espaguete'),
 (770,'http://supernarede.local/catalogsearch/result/?cat=&q=teste&x=0&y=0','http://supernarede.local/departamentos/massa/espaguete'),
 (771,'http://supernarede.local/catalogsearch/result/?cat=&q=teste&x=0&y=0','http://supernarede.local/departamentos/massa/espaguete'),
 (772,'http://supernarede.local/catalogsearch/result/?cf=1&q=teste&x=17&y=37','http://supernarede.local/catalogsearch/result/?cat=&q=teste&x=0&y=0'),
 (773,'http://supernarede.local/customer/account/create/','http://supernarede.local/catalogsearch/result/?cf=1&q=teste&x=17&y=37'),
 (774,'http://supernarede.local/catalog/category/view/id/9','http://supernarede.local/customer/account/create/'),
 (775,'http://supernarede.local/j2tajaxcheckout/index/cart/cart/add/uenc/aHR0cDovL3N1cGVybmFyZWRlLmxvY2FsL2RlcGFydGFtZW50b3MvYmViaWRhcy9jZXJ2ZWph/product/1/qty/1','http://supernarede.local/departamentos/bebidas/cerveja'),
 (776,'http://supernarede.local/catalog/category/view/id/9','http://supernarede.local/customer/account/create/'),
 (777,'http://supernarede.local/catalog/category/view/id/9','http://supernarede.local/customer/account/create/'),
 (778,'http://supernarede.local/catalog/category/view/id/9','http://supernarede.local/customer/account/create/'),
 (779,'http://supernarede.local/catalog/category/view/id/9','http://supernarede.local/customer/account/create/'),
 (780,'http://supernarede.local/catalog/category/view/id/9','http://supernarede.local/customer/account/create/'),
 (781,'http://supernarede.local/catalogsearch/result/?cf=1&q=teste&x=0&y=0','http://supernarede.local/departamentos/bebidas/cerveja'),
 (782,'http://supernarede.local/catalogsearch/result/?cf=1&q=exemplo%2C+cerveja&x=0&y=0','http://supernarede.local/catalogsearch/result/?cf=1&q=teste&x=0&y=0'),
 (783,'http://supernarede.local/catalogsearch/result/?cf=1&q=exemplo%2C+cerveja&x=0&y=0','http://supernarede.local/catalogsearch/result/?cf=1&q=teste&x=0&y=0'),
 (784,'http://supernarede.local/catalogsearch/result/?cf=1&q=exemplo%2C+cerveja&x=0&y=0','http://supernarede.local/catalogsearch/result/?cf=1&q=teste&x=0&y=0'),
 (785,'http://supernarede.local/catalogsearch/result/?cf=1&l=exemplo%2C+cerveja&x=0&y=0',''),
 (786,'http://supernarede.local/',''),
 (787,'http://supernarede.local/customer/account/login/','http://supernarede.local/'),
 (788,'http://supernarede.local/',''),
 (789,'http://supernarede.local/',''),
 (790,'http://supernarede.local/',''),
 (791,'http://supernarede.local/catalog/category/view/id/9','http://supernarede.local/'),
 (792,'http://supernarede.local/catalog/category/view/id/9','http://supernarede.local/'),
 (793,'http://supernarede.local/catalog/product/view/id/1/category/9','http://supernarede.local/departamentos/bebidas/cerveja'),
 (794,'http://supernarede.local/catalog/category/view/id/11','http://supernarede.local/departamentos/bebidas/cerveja/cerveja-exemplo-sem-alcool'),
 (795,'http://supernarede.local/catalog/category/view/id/9','http://supernarede.local/departamentos/bebidas/refrigerante'),
 (796,'http://supernarede.local/customer/account/login/','http://supernarede.local/departamentos/bebidas/cerveja'),
 (797,'http://supernarede.local/customer/account/login/','http://supernarede.local/departamentos/bebidas/cerveja'),
 (798,'http://supernarede.local/',''),
 (799,'http://supernarede.local/catalog/product/view/id/5','http://supernarede.local/'),
 (800,'http://supernarede.local/catalog/product/view/id/5','http://supernarede.local/'),
 (801,'http://supernarede.local/catalog/product/view/id/5','http://supernarede.local/'),
 (802,'http://supernarede.local/catalog/product/view/id/5','http://supernarede.local/'),
 (803,'http://supernarede.local/catalog/product/view/id/5','http://supernarede.local/'),
 (804,'http://supernarede.local/catalog/product/view/id/5','http://supernarede.local/'),
 (805,'http://supernarede.local/catalog/product/view/id/5','http://supernarede.local/'),
 (806,'http://supernarede.local/catalog/category/view/id/9','http://supernarede.local/cerveja-exemplo-skol-escura'),
 (807,'http://supernarede.local/catalog/category/view/id/9','http://supernarede.local/cerveja-exemplo-skol-escura'),
 (808,'http://supernarede.local/catalog/category/view/id/9','http://supernarede.local/cerveja-exemplo-skol-escura'),
 (809,'http://supernarede.local/catalog/category/view/id/9','http://supernarede.local/cerveja-exemplo-skol-escura'),
 (810,'http://supernarede.local/catalog/category/view/id/9','http://supernarede.local/departamentos/bebidas/cerveja'),
 (811,'http://supernarede.local/customer/account/login/','http://supernarede.local/departamentos/bebidas/cerveja'),
 (812,'http://supernarede.local/catalog/category/view/id/9','http://supernarede.local/departamentos/bebidas/cerveja'),
 (813,'http://supernarede.local/catalog/category/view/id/4','http://supernarede.local/departamentos/bebidas/cerveja'),
 (814,'http://supernarede.local/catalog/category/view/id/6','http://supernarede.local/listas-prontas'),
 (815,'http://supernarede.local/catalog/product/view/id/6/category/6','http://supernarede.local/listas-prontas/somente-bebidas'),
 (816,'http://supernarede.local/catalog/product/view/id/6/category/6','http://supernarede.local/listas-prontas/somente-bebidas'),
 (817,'http://supernarede.local/catalog/category/view/id/16','http://supernarede.local/listas-prontas/somente-bebidas/cesta-alcoologica'),
 (818,'http://supernarede.local/catalog/category/view/id/6','http://supernarede.local/receitas'),
 (819,'http://supernarede.local/catalog/product/view/id/6/category/6','http://supernarede.local/listas-prontas/somente-bebidas'),
 (820,'http://supernarede.local/catalog/category/view/id/16','http://supernarede.local/listas-prontas/somente-bebidas/cesta-alcoologica'),
 (821,'http://supernarede.local/catalog/category/view/id/6','http://supernarede.local/receitas'),
 (822,'http://supernarede.local/catalog/product/view/id/6/category/6','http://supernarede.local/listas-prontas/somente-bebidas'),
 (823,'http://supernarede.local/catalog/product/view/id/6/category/6','http://supernarede.local/listas-prontas/somente-bebidas'),
 (824,'http://supernarede.local/catalog/product/view/id/6/category/6','http://supernarede.local/listas-prontas/somente-bebidas'),
 (825,'http://supernarede.local/catalog/category/view/id/6','http://supernarede.local/listas-prontas/somente-bebidas/cesta-alcoologica'),
 (826,'http://supernarede.local/catalog/category/view/id/18','http://supernarede.local/listas-prontas/somente-bebidas'),
 (827,'http://supernarede.local/catalog/category/view/id/18','http://supernarede.local/listas-prontas/somente-bebidas'),
 (828,'http://supernarede.local/catalog/category/view/id/19','http://supernarede.local/receitas'),
 (829,'http://supernarede.local/catalog/product/view/id/6/category/19','http://supernarede.local/receitas/grupo-qualquer'),
 (830,'http://192.168.0.57/super_na_rede/',''),
 (831,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/'),
 (832,'http://192.168.0.57/super_na_rede/',''),
 (833,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/'),
 (834,'http://192.168.0.57/super_na_rede/catalog/product/view/id/1','http://192.168.0.57/super_na_rede/'),
 (835,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/cerveja-exemplo-sem-alcool'),
 (836,'http://192.168.0.57/super_na_rede/catalog/category/view/id/7','http://192.168.0.57/super_na_rede/'),
 (837,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/departamentos/bebidas'),
 (838,'http://192.168.0.57/super_na_rede/catalog/category/view/id/11','http://192.168.0.57/super_na_rede/departamentos/bebidas'),
 (839,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/departamentos/bebidas/refrigerante'),
 (840,'http://192.168.0.57/super_na_rede/catalog/category/view/id/7','http://192.168.0.57/super_na_rede/'),
 (841,'http://192.168.0.57/super_na_rede/catalog/category/view/id/7?dir=desc&order=position','http://192.168.0.57/super_na_rede/departamentos/bebidas'),
 (842,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/departamentos/bebidas?dir=desc&order=position'),
 (843,'http://192.168.0.57/super_na_rede/catalog/category/view/id/7?dir=asc&order=name','http://192.168.0.57/super_na_rede/departamentos/bebidas?dir=desc&order=position'),
 (844,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/departamentos/bebidas?dir=asc&order=name'),
 (845,'http://192.168.0.57/super_na_rede/customer/account/login/','http://192.168.0.57/super_na_rede/departamentos/bebidas?dir=asc&order=name'),
 (846,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/account/login/'),
 (847,'http://192.168.0.57/super_na_rede/customer/account/create/','http://192.168.0.57/super_na_rede/customer/account/login/'),
 (848,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/account/create/'),
 (849,'http://192.168.0.57/super_na_rede/catalog/product/view/id/1','http://192.168.0.57/super_na_rede/'),
 (850,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/cerveja-exemplo-sem-alcool'),
 (851,'http://192.168.0.57/super_na_rede/catalog/category/view/id/4','http://192.168.0.57/super_na_rede/customer/account/create/'),
 (852,'http://192.168.0.57/super_na_rede/','http://192.168.0.57/super_na_rede/customer/account/create/'),
 (853,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/listas-prontas'),
 (854,'http://192.168.0.57/super_na_rede/catalog/category/view/id/6','http://192.168.0.57/super_na_rede/listas-prontas'),
 (855,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/listas-prontas/somente-bebidas'),
 (856,'http://192.168.0.57/super_na_rede/contacts','http://192.168.0.57/super_na_rede/listas-prontas/somente-bebidas'),
 (857,'http://192.168.0.57/super_na_rede/contacts','http://192.168.0.57/super_na_rede/listas-prontas/somente-bebidas'),
 (858,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/contacts'),
 (859,'http://192.168.0.57/super_na_rede/catalog/category/view/id/12','http://192.168.0.57/super_na_rede/contacts'),
 (860,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/departamentos/massa/espaguete'),
 (861,'http://192.168.0.57/super_na_rede/catalogsearch/result/?cf=1&cf=bavaria%2C+skol&x=0&y=0','http://192.168.0.57/super_na_rede/departamentos/massa/espaguete'),
 (862,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/catalogsearch/result/?cf=1&cf=bavaria%2C+skol&x=0&y=0'),
 (863,'http://192.168.0.57/super_na_rede/catalog/product/view/id/6','http://192.168.0.57/super_na_rede/catalogsearch/result/?cf=1&cf=bavaria%2C+skol&x=0&y=0'),
 (864,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/cesta-alcoologica'),
 (865,'http://192.168.0.57/super_na_rede/catalog/product/view/id/6',''),
 (866,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/cesta-alcoologica'),
 (867,'http://192.168.0.57/super_na_rede/checkout/cart','http://192.168.0.57/super_na_rede/cesta-alcoologica'),
 (868,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/checkout/cart'),
 (869,'http://192.168.0.57/super_na_rede/catalog/category/view/id/7','http://192.168.0.57/super_na_rede/checkout/cart'),
 (870,'http://192.168.0.57/super_na_rede/catalog/category/view/id/10','http://192.168.0.57/super_na_rede/checkout/cart'),
 (871,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/departamentos/bebidas/agua'),
 (872,'http://192.168.0.57/super_na_rede/catalog/category/view/id/14','http://192.168.0.57/super_na_rede/departamentos/bebidas/agua'),
 (873,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/departamentos/padaria'),
 (874,'http://192.168.0.57/super_na_rede/catalog/category/view/id/12','http://192.168.0.57/super_na_rede/departamentos/padaria'),
 (875,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/departamentos/massa/espaguete'),
 (876,'http://192.168.0.57/super_na_rede/',''),
 (877,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/'),
 (878,'http://192.168.0.57/super_na_rede/',''),
 (879,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/'),
 (880,'http://192.168.0.57/super_na_rede/',''),
 (881,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/'),
 (882,'http://192.168.0.57/super_na_rede/catalog/category/view/id/4','http://192.168.0.57/super_na_rede/'),
 (883,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/listas-prontas'),
 (884,'http://192.168.0.57/super_na_rede/catalog/category/view/id/14','http://192.168.0.57/super_na_rede/departamentos/bebidas/agua'),
 (885,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/departamentos/padaria'),
 (886,'http://192.168.0.57/super_na_rede/catalogsearch/result/?cf=1&cf=cerveja%2C+bacardi%2C+chapagne%2C+sab%C3%A3o','http://192.168.0.57/super_na_rede/departamentos/padaria'),
 (887,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/catalogsearch/result/?cf=1&cf=cerveja%2C+bacardi%2C+chapagne%2C+sab%C3%A3o'),
 (888,'http://192.168.0.57/super_na_rede/catalogsearch/result/?cf=1&cf=cerveja%2C+bacardi%2C+chapagne%2C+sab%C3%A3o&x=30&y=33','http://192.168.0.57/super_na_rede/catalogsearch/result/?cf=1&cf=cerveja%2C+bacardi%2C+chapagne%2C+sab%C3%A3o'),
 (889,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/catalogsearch/result/?cf=1&cf=cerveja%2C+bacardi%2C+chapagne%2C+sab%C3%A3o&x=30&y=33'),
 (890,'http://192.168.0.57/super_na_rede/catalogsearch/result/?cat=&q=&x=49&y=40','http://192.168.0.57/super_na_rede/cesta-alcoologica'),
 (891,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/catalogsearch/result/?cat=&q=&x=49&y=40'),
 (892,'http://192.168.0.57/super_na_rede/',''),
 (893,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/'),
 (894,'http://192.168.0.57/super_na_rede/',''),
 (895,'http://192.168.0.57/super_na_rede/',''),
 (896,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/'),
 (897,'http://192.168.0.57/super_na_rede/',''),
 (898,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/'),
 (899,'http://192.168.0.57/super_na_rede/',''),
 (900,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/'),
 (901,'http://192.168.0.57/super_na_rede/',''),
 (902,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/'),
 (903,'http://192.168.0.57/super_na_rede/',''),
 (904,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/'),
 (905,'http://192.168.0.57/super_na_rede/catalog/category/view/id/4','http://192.168.0.57/super_na_rede/'),
 (906,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/listas-prontas'),
 (907,'http://192.168.0.57/super_na_rede/catalog/category/view/id/4','http://192.168.0.57/super_na_rede/'),
 (908,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/listas-prontas'),
 (909,'http://192.168.0.57/super_na_rede/',''),
 (910,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/'),
 (911,'http://192.168.0.57/super_na_rede/',''),
 (912,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/'),
 (913,'http://192.168.0.57/super_na_rede/',''),
 (914,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/'),
 (915,'http://192.168.0.57/super_na_rede/',''),
 (916,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/'),
 (917,'http://192.168.0.57/super_na_rede/catalog/category/view/id/4','http://192.168.0.57/super_na_rede/'),
 (918,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/listas-prontas'),
 (919,'http://192.168.0.57/super_na_rede/',''),
 (920,'http://192.168.0.57/super_na_rede/catalog/category/view/id/4','http://192.168.0.57/super_na_rede/'),
 (921,'http://192.168.0.57/super_na_rede/',''),
 (922,'http://192.168.0.57/super_na_rede/',''),
 (923,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/listas-prontas'),
 (924,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/'),
 (925,'http://192.168.0.57/super_na_rede/catalog/category/view/id/4','http://192.168.0.57/super_na_rede/'),
 (926,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/listas-prontas'),
 (927,'http://192.168.0.57/super_na_rede/',''),
 (928,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/'),
 (929,'http://192.168.0.57/super_na_rede/',''),
 (930,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/'),
 (931,'http://192.168.0.57/super_na_rede/catalog/category/view/id/10','http://192.168.0.57/super_na_rede/'),
 (932,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/departamentos/bebidas/agua'),
 (933,'http://192.168.0.57/super_na_rede/catalog/category/view/id/7','http://192.168.0.57/super_na_rede/departamentos/bebidas/agua'),
 (934,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/departamentos/bebidas'),
 (935,'http://192.168.0.57/super_na_rede/customer/account/login/',''),
 (936,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/account/login/'),
 (937,'http://192.168.0.57/super_na_rede/catalogsearch/result/?cat=7&q=&x=29&y=35','http://192.168.0.57/super_na_rede/customer/account/login/'),
 (938,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/catalogsearch/result/?cat=7&q=&x=29&y=35'),
 (939,'http://192.168.0.57/super_na_rede/catalog/category/view/id/18','http://192.168.0.57/super_na_rede/catalogsearch/result/?cat=7&q=&x=29&y=35'),
 (940,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/receitas'),
 (941,'http://192.168.0.57/super_na_rede/catalog/category/view/id/4','http://192.168.0.57/super_na_rede/receitas'),
 (942,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/listas-prontas'),
 (943,'http://192.168.0.57/super_na_rede/catalog/category/view/id/6','http://192.168.0.57/super_na_rede/listas-prontas'),
 (944,'http://192.168.0.57/super_na_rede/catalog/category/view/id/5','http://192.168.0.57/super_na_rede/listas-prontas'),
 (945,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/listas-prontas/compra-do-mes'),
 (946,'http://192.168.0.57/super_na_rede/catalog/category/view/id/4','http://192.168.0.57/super_na_rede/listas-prontas/compra-do-mes'),
 (947,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/listas-prontas'),
 (948,'http://192.168.0.57/super_na_rede/catalog/category/view/id/4','http://192.168.0.57/super_na_rede/listas-prontas/compra-do-mes'),
 (949,'http://192.168.0.57/super_na_rede/catalog/category/view/id/4','http://192.168.0.57/super_na_rede/listas-prontas/compra-do-mes'),
 (950,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/listas-prontas'),
 (951,'http://192.168.0.57/super_na_rede/catalog/category/view/id/4','http://192.168.0.57/super_na_rede/listas-prontas/compra-do-mes'),
 (952,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/listas-prontas'),
 (953,'http://192.168.0.57/super_na_rede/catalog/category/view/id/4','http://192.168.0.57/super_na_rede/listas-prontas/compra-do-mes'),
 (954,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/listas-prontas'),
 (955,'http://192.168.0.57/super_na_rede/catalog/category/view/id/4','http://192.168.0.57/super_na_rede/listas-prontas/compra-do-mes'),
 (956,'http://192.168.0.57/super_na_rede/catalog/category/view/id/4','http://192.168.0.57/super_na_rede/'),
 (957,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/listas-prontas'),
 (958,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/listas-prontas'),
 (959,'http://192.168.0.57/super_na_rede/catalog/category/view/id/4','http://192.168.0.57/super_na_rede/'),
 (960,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/listas-prontas'),
 (961,'http://192.168.0.57/super_na_rede/catalog/category/view/id/4','http://192.168.0.57/super_na_rede/'),
 (962,'http://192.168.0.57/super_na_rede/catalog/category/view/id/4','http://192.168.0.57/super_na_rede/listas-prontas/compra-do-mes'),
 (963,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/listas-prontas'),
 (964,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/listas-prontas'),
 (965,'http://192.168.0.57/super_na_rede/catalog/category/view/id/4','http://192.168.0.57/super_na_rede/listas-prontas/compra-do-mes'),
 (966,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/listas-prontas'),
 (967,'http://192.168.0.57/super_na_rede/catalog/category/view/id/4','http://192.168.0.57/super_na_rede/listas-prontas/compra-do-mes'),
 (968,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/listas-prontas'),
 (969,'http://192.168.0.57/super_na_rede/catalog/category/view/id/4','http://192.168.0.57/super_na_rede/'),
 (970,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/listas-prontas'),
 (971,'http://192.168.0.57/super_na_rede/catalog/category/view/id/4','http://192.168.0.57/super_na_rede/listas-prontas/compra-do-mes'),
 (972,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/listas-prontas'),
 (973,'http://192.168.0.57/super_na_rede/catalog/category/view/id/4','http://192.168.0.57/super_na_rede/'),
 (974,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/listas-prontas'),
 (975,'http://192.168.0.57/super_na_rede/catalog/category/view/id/4','http://192.168.0.57/super_na_rede/'),
 (976,'http://192.168.0.57/super_na_rede/catalog/category/view/id/4','http://192.168.0.57/super_na_rede/'),
 (977,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/listas-prontas'),
 (978,'http://192.168.0.57/super_na_rede/catalog/category/view/id/4','http://192.168.0.57/super_na_rede/listas-prontas/compra-do-mes'),
 (979,'http://192.168.0.57/super_na_rede/catalog/category/view/id/4','http://192.168.0.57/super_na_rede/listas-prontas/compra-do-mes'),
 (980,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/listas-prontas'),
 (981,'http://192.168.0.57/super_na_rede/catalog/category/view/id/4','http://192.168.0.57/super_na_rede/'),
 (982,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/listas-prontas'),
 (983,'http://192.168.0.57/super_na_rede/catalog/category/view/id/4','http://192.168.0.57/super_na_rede/listas-prontas/compra-do-mes'),
 (984,'http://192.168.0.57/super_na_rede/catalog/category/view/id/4','http://192.168.0.57/super_na_rede/listas-prontas/compra-do-mes'),
 (985,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/listas-prontas'),
 (986,'http://192.168.0.57/super_na_rede/catalog/category/view/id/4','http://192.168.0.57/super_na_rede/listas-prontas/compra-do-mes'),
 (987,'http://192.168.0.57/super_na_rede/catalog/category/view/id/4','http://192.168.0.57/super_na_rede/listas-prontas/compra-do-mes'),
 (988,'http://192.168.0.57/super_na_rede/catalog/category/view/id/4','http://192.168.0.57/super_na_rede/listas-prontas/compra-do-mes'),
 (989,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/listas-prontas'),
 (990,'http://192.168.0.57/super_na_rede/catalog/category/view/id/7','http://192.168.0.57/super_na_rede/listas-prontas'),
 (991,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/departamentos/bebidas'),
 (992,'http://192.168.0.57/super_na_rede/customer/account/login/',''),
 (993,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/account/login/'),
 (994,'http://192.168.0.57/super_na_rede/customer/account/create/','http://192.168.0.57/super_na_rede/customer/account/login/'),
 (995,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/account/create/'),
 (996,'http://192.168.0.57/super_na_rede/customer/account/login/',''),
 (997,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/account/login/'),
 (998,'http://192.168.0.57/super_na_rede/',''),
 (999,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/'),
 (1000,'http://192.168.0.57/super_na_rede/',''),
 (1001,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/'),
 (1002,'http://192.168.0.57/super_na_rede/customer/account/login/','http://192.168.0.57/super_na_rede/'),
 (1003,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/account/login/'),
 (1004,'http://192.168.0.57/super_na_rede/customer/account/loginPost/','http://192.168.0.57/super_na_rede/customer/account/login/'),
 (1005,'http://192.168.0.57/super_na_rede/customer/account/login/','http://192.168.0.57/super_na_rede/customer/account/login/'),
 (1006,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/account/login/'),
 (1007,'http://192.168.0.57/super_na_rede/customer/account/loginPost/','http://192.168.0.57/super_na_rede/customer/account/login/'),
 (1008,'http://192.168.0.57/super_na_rede/customer/account/login/','http://192.168.0.57/super_na_rede/customer/account/login/'),
 (1009,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/account/login/'),
 (1010,'http://192.168.0.57/super_na_rede/customer/account/loginPost/','http://192.168.0.57/super_na_rede/customer/account/login/'),
 (1011,'http://192.168.0.57/super_na_rede/customer/account/login/','http://192.168.0.57/super_na_rede/customer/account/login/'),
 (1012,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/account/login/'),
 (1013,'http://192.168.0.57/super_na_rede/customer/account/create/','http://192.168.0.57/super_na_rede/customer/account/login/'),
 (1014,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/account/create/'),
 (1015,'http://192.168.0.57/super_na_rede/catalogsearch/result/?cat=7&q=bavaria','http://192.168.0.57/super_na_rede/customer/account/login/'),
 (1016,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/catalogsearch/result/?cat=7&q=bavaria'),
 (1017,'http://192.168.0.57/super_na_rede/catalogsearch/result/?cat=7&q=bavaria',''),
 (1018,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/catalogsearch/result/?cat=7&q=bavaria'),
 (1019,'http://192.168.0.57/super_na_rede/customer/account/create/','http://192.168.0.57/super_na_rede/customer/account/login/'),
 (1020,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/account/create/'),
 (1021,'http://192.168.0.57/super_na_rede/',''),
 (1022,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/'),
 (1023,'http://192.168.0.57/super_na_rede/catalogsearch/result/?cf=1&cf=arroz%2C+feij%C3%A3o&x=31&y=51','http://192.168.0.57/super_na_rede/'),
 (1024,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/catalogsearch/result/?cf=1&cf=arroz%2C+feij%C3%A3o&x=31&y=51'),
 (1025,'http://192.168.0.57/super_na_rede/catalogsearch/result/?cf=1&cf=teste%2C+cerveja&x=0&y=0','http://192.168.0.57/super_na_rede/catalogsearch/result/?cf=1&cf=arroz%2C+feij%C3%A3o&x=31&y=51'),
 (1026,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/catalogsearch/result/?cf=1&cf=teste%2C+cerveja&x=0&y=0'),
 (1027,'http://192.168.0.57/super_na_rede/catalogsearch/result/index/?cf=teste%2C+cerveja&termo=cerveja','http://192.168.0.57/super_na_rede/catalogsearch/result/?cf=1&cf=teste%2C+cerveja&x=0&y=0'),
 (1028,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/catalogsearch/result/index/?cf=teste%2C+cerveja&termo=cerveja'),
 (1029,'http://192.168.0.57/super_na_rede/catalogsearch/result/index/?cf=teste%2C+cerveja&limit=4&termo=cerveja','http://192.168.0.57/super_na_rede/catalogsearch/result/index/?cf=teste%2C+cerveja&termo=cerveja'),
 (1030,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/catalogsearch/result/index/?cf=teste%2C+cerveja&limit=4&termo=cerveja'),
 (1031,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/index.php/admin/dashboard/index/key/e6f012816fdb04e194f332cd5065cc22/'),
 (1032,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/index.php/admin/system_config/index/key/1cd3dc7f196091a89063c310b186367d/'),
 (1033,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/index.php/admin/catalog_category/index/key/f54661ba0556162a64d1ce66ab9b2b99/'),
 (1034,'http://192.168.0.57/super_na_rede/catalogsearch/result/index/?cf=teste%2C+cerveja&limit=4&termo=cerveja','http://192.168.0.57/super_na_rede/catalogsearch/result/index/?cf=teste%2C+cerveja&termo=cerveja'),
 (1035,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/catalogsearch/result/index/?cf=teste%2C+cerveja&limit=4&termo=cerveja'),
 (1036,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/index.php/admin/catalog_product/index/key/f3507c64862b65f93fbc6e52dea08089/'),
 (1037,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/index.php/admin/catalog_product/edit/id/6/key/d35c4e2bfad309cec13a568766350694/'),
 (1038,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/index.php/admin/catalog_product/edit/id/6/back/edit/tab/product_info_tabs_group_22/key/d35c4e2bfad309cec13a568766350694/'),
 (1039,'http://192.168.0.57/super_na_rede/catalogsearch/result/index/?cf=teste%2C+cerveja&limit=4&termo=cerveja','http://192.168.0.57/super_na_rede/catalogsearch/result/index/?cf=teste%2C+cerveja&termo=cerveja'),
 (1040,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/catalogsearch/result/index/?cf=teste%2C+cerveja&limit=4&termo=cerveja'),
 (1041,'http://192.168.0.57/super_na_rede/catalogsearch/result/index/?cf=teste%2C+cerveja&limit=8&termo=cerveja','http://192.168.0.57/super_na_rede/catalogsearch/result/index/?cf=teste%2C+cerveja&limit=4&termo=cerveja'),
 (1042,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/catalogsearch/result/index/?cf=teste%2C+cerveja&limit=8&termo=cerveja'),
 (1043,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/index.php/admin/system_config/index/key/1cd3dc7f196091a89063c310b186367d/'),
 (1044,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/index.php/admin/system_config/edit/section/general/key/ef19b822cf84c11e79389c6a96b9d4d2/'),
 (1045,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/index.php/admin/system_variable/index/key/71da1553fe75c86e54402e41eb8d8906/'),
 (1046,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/index.php/admin/system_variable/edit/variable_id/1/key/72f02af783edebedfcf9e38c1b01e8d8/'),
 (1047,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/index.php/admin/system_variable/index/key/71da1553fe75c86e54402e41eb8d8906/'),
 (1048,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/index.php/admin/system_variable/edit/variable_id/1/key/72f02af783edebedfcf9e38c1b01e8d8/'),
 (1049,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/index.php/admin/system_variable/index/key/71da1553fe75c86e54402e41eb8d8906/'),
 (1050,'http://192.168.0.57/super_na_rede/customer/account/create/','http://192.168.0.57/super_na_rede/catalogsearch/result/index/?cf=teste%2C+cerveja&limit=8&termo=cerveja'),
 (1051,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/account/create/'),
 (1052,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/index.php/admin/system_variable/new/key/0a2f467e319332d1e14ab000ba020d4f/'),
 (1053,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/index.php/admin/system_variable/edit/variable_id/2/back/edit/key/72f02af783edebedfcf9e38c1b01e8d8/'),
 (1054,'http://192.168.0.57/super_na_rede/customer/account/create/','http://192.168.0.57/super_na_rede/catalogsearch/result/index/?cf=teste%2C+cerveja&limit=8&termo=cerveja'),
 (1055,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/account/create/'),
 (1056,'http://192.168.0.57/super_na_rede/customer/account/create/','http://192.168.0.57/super_na_rede/catalogsearch/result/index/?cf=teste%2C+cerveja&limit=8&termo=cerveja'),
 (1057,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/account/create/'),
 (1058,'http://192.168.0.57/super_na_rede/customer/account/create/','http://192.168.0.57/super_na_rede/catalogsearch/result/index/?cf=teste%2C+cerveja&limit=8&termo=cerveja'),
 (1059,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/account/create/'),
 (1060,'http://192.168.0.57/super_na_rede/customer/account/create/','http://192.168.0.57/super_na_rede/catalogsearch/result/index/?cf=teste%2C+cerveja&limit=8&termo=cerveja'),
 (1061,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/account/create/'),
 (1062,'http://192.168.0.57/super_na_rede/customer/account/create/',''),
 (1063,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/account/create/'),
 (1064,'http://192.168.0.57/super_na_rede/customer/account/create/',''),
 (1065,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/account/create/'),
 (1066,'http://192.168.0.57/super_na_rede/customer/account/create/',''),
 (1067,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/account/create/'),
 (1068,'http://192.168.0.57/super_na_rede/customer/account/create/',''),
 (1069,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/account/create/'),
 (1070,'http://192.168.0.57/super_na_rede/customer/account/create/',''),
 (1071,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/account/create/'),
 (1072,'http://192.168.0.57/super_na_rede/customer/account/create/',''),
 (1073,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/account/create/'),
 (1074,'http://192.168.0.57/super_na_rede/customer/account/createpost/','http://192.168.0.57/super_na_rede/customer/account/create/'),
 (1075,'http://192.168.0.57/super_na_rede/customer/account/index/','http://192.168.0.57/super_na_rede/customer/account/create/'),
 (1076,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/account/index/'),
 (1077,'http://192.168.0.57/super_na_rede/checkout/','http://192.168.0.57/super_na_rede/customer/account/index/'),
 (1078,'http://192.168.0.57/super_na_rede/checkout/onepage/','http://192.168.0.57/super_na_rede/customer/account/index/'),
 (1079,'http://192.168.0.57/super_na_rede/checkout/cart/','http://192.168.0.57/super_na_rede/customer/account/index/'),
 (1080,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/checkout/cart/'),
 (1081,'http://192.168.0.57/super_na_rede/contacts','http://192.168.0.57/super_na_rede/checkout/cart/'),
 (1082,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/contacts'),
 (1083,'http://192.168.0.57/super_na_rede/contacts/index/post/','http://192.168.0.57/super_na_rede/contacts'),
 (1084,'http://192.168.0.57/super_na_rede/contacts/index/','http://192.168.0.57/super_na_rede/contacts'),
 (1085,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/contacts/index/'),
 (1086,'http://192.168.0.57/super_na_rede/customer/account/','http://192.168.0.57/super_na_rede/contacts/index/'),
 (1087,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/account/'),
 (1088,'http://192.168.0.57/super_na_rede/newsletter/manage/','http://192.168.0.57/super_na_rede/customer/account/'),
 (1089,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/newsletter/manage/'),
 (1090,'http://192.168.0.57/super_na_rede/customer/account/','http://192.168.0.57/super_na_rede/newsletter/manage/'),
 (1091,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/account/'),
 (1092,'http://192.168.0.57/super_na_rede/checkout/cart/','http://192.168.0.57/super_na_rede/customer/account/'),
 (1093,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/checkout/cart/'),
 (1094,'http://192.168.0.57/super_na_rede/customer/account/','http://192.168.0.57/super_na_rede/checkout/cart/'),
 (1095,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/account/'),
 (1096,'http://192.168.0.57/super_na_rede/customer/account/edit/','http://192.168.0.57/super_na_rede/customer/account/'),
 (1097,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/account/edit/'),
 (1098,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/index.php/admin/system_variable/edit/variable_id/2/back/edit/key/67c8432ad524b6bfb6a2aadd1cf38989/'),
 (1099,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/index.php/customerattr/adminhtml_manage/index/key/1a6fa0b03274926afcade43a4becae19/'),
 (1100,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/index.php/customerattr/adminhtml_manage/new/key/8713349c47401b6203c4280ec2d8324e/'),
 (1101,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/index.php/customerattr/adminhtml_manage/index/key/1a6fa0b03274926afcade43a4becae19/'),
 (1102,'http://192.168.0.57/super_na_rede/customer/account/edit/','http://192.168.0.57/super_na_rede/customer/account/'),
 (1103,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/account/edit/'),
 (1104,'http://192.168.0.57/super_na_rede/customer/account/editPost/','http://192.168.0.57/super_na_rede/customer/account/edit/'),
 (1105,'http://192.168.0.57/super_na_rede/customer/account/','http://192.168.0.57/super_na_rede/customer/account/edit/'),
 (1106,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/account/'),
 (1107,'http://192.168.0.57/super_na_rede/customer/account/edit/','http://192.168.0.57/super_na_rede/customer/account/'),
 (1108,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/account/edit/'),
 (1109,'http://192.168.0.57/super_na_rede/customer/address/','http://192.168.0.57/super_na_rede/customer/account/edit/'),
 (1110,'http://192.168.0.57/super_na_rede/customer/address/new/','http://192.168.0.57/super_na_rede/customer/account/edit/'),
 (1111,'http://192.168.0.57/super_na_rede/customer/address/new/','http://192.168.0.57/super_na_rede/customer/account/edit/'),
 (1112,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/address/new/'),
 (1113,'http://192.168.0.57/super_na_rede/customer/address/new/','http://192.168.0.57/super_na_rede/customer/account/edit/'),
 (1114,'http://192.168.0.57/super_na_rede/customer/address/new/','http://192.168.0.57/super_na_rede/customer/account/edit/'),
 (1115,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/address/new/'),
 (1116,'http://192.168.0.57/super_na_rede/customer/account/edit/','http://192.168.0.57/super_na_rede/customer/address/new/'),
 (1117,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/account/edit/'),
 (1118,'http://192.168.0.57/super_na_rede/customer/account/','http://192.168.0.57/super_na_rede/customer/account/edit/'),
 (1119,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/account/'),
 (1120,'http://192.168.0.57/super_na_rede/customer/address/','http://192.168.0.57/super_na_rede/customer/account/'),
 (1121,'http://192.168.0.57/super_na_rede/customer/address/new/','http://192.168.0.57/super_na_rede/customer/account/'),
 (1122,'http://192.168.0.57/super_na_rede/customer/address/new/','http://192.168.0.57/super_na_rede/customer/account/'),
 (1123,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/address/new/'),
 (1124,'http://192.168.0.57/super_na_rede/customer/account/edit/','http://192.168.0.57/super_na_rede/customer/address/new/'),
 (1125,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/account/edit/'),
 (1126,'http://192.168.0.57/super_na_rede/customer/account/','http://192.168.0.57/super_na_rede/customer/account/edit/'),
 (1127,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/account/'),
 (1128,'http://192.168.0.57/super_na_rede/customer/account/edit/','http://192.168.0.57/super_na_rede/customer/account/'),
 (1129,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/account/edit/'),
 (1130,'http://192.168.0.57/super_na_rede/customer/address/','http://192.168.0.57/super_na_rede/customer/account/'),
 (1131,'http://192.168.0.57/super_na_rede/customer/address/new/','http://192.168.0.57/super_na_rede/customer/account/'),
 (1132,'http://192.168.0.57/super_na_rede/customer/address/new/','http://192.168.0.57/super_na_rede/customer/account/'),
 (1133,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/address/new/'),
 (1134,'http://192.168.0.57/super_na_rede/customer/address/new/','http://192.168.0.57/super_na_rede/customer/account/'),
 (1135,'http://192.168.0.57/super_na_rede/customer/address/new/','http://192.168.0.57/super_na_rede/customer/account/'),
 (1136,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/address/new/'),
 (1137,'http://192.168.0.57/super_na_rede/customer/address/new/','http://192.168.0.57/super_na_rede/customer/account/'),
 (1138,'http://192.168.0.57/super_na_rede/customer/address/new/','http://192.168.0.57/super_na_rede/customer/account/'),
 (1139,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/address/new/'),
 (1140,'http://192.168.0.57/super_na_rede/customer/address/new/','http://192.168.0.57/super_na_rede/customer/account/'),
 (1141,'http://192.168.0.57/super_na_rede/customer/address/new/','http://192.168.0.57/super_na_rede/customer/account/'),
 (1142,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/address/new/'),
 (1143,'http://192.168.0.57/super_na_rede/customer/address/new/','http://192.168.0.57/super_na_rede/customer/account/'),
 (1144,'http://192.168.0.57/super_na_rede/customer/address/new/','http://192.168.0.57/super_na_rede/customer/account/'),
 (1145,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/address/new/'),
 (1146,'http://192.168.0.57/super_na_rede/customer/address/new/','http://192.168.0.57/super_na_rede/customer/account/'),
 (1147,'http://192.168.0.57/super_na_rede/customer/address/new/','http://192.168.0.57/super_na_rede/customer/account/'),
 (1148,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/address/new/'),
 (1149,'http://192.168.0.57/super_na_rede/customer/account/edit/','http://192.168.0.57/super_na_rede/customer/address/new/'),
 (1150,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/account/edit/'),
 (1151,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/index.php/admin/system_config/index/key/de7a66385d62a4b2aeed05602b740fbb/'),
 (1152,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/index.php/admin/system_config/edit/section/customer/key/bf64ec507ec630653fea1b18dba4fa55/'),
 (1153,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/index.php/admin/system_config/edit/section/customer/key/bf64ec507ec630653fea1b18dba4fa55/'),
 (1154,'http://192.168.0.57/super_na_rede/customer/account/edit/','http://192.168.0.57/super_na_rede/customer/address/new/'),
 (1155,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/account/edit/'),
 (1156,'http://192.168.0.57/super_na_rede/customer/account/edit/','http://192.168.0.57/super_na_rede/customer/address/new/'),
 (1157,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/account/edit/'),
 (1158,'http://192.168.0.57/super_na_rede/customer/address/','http://192.168.0.57/super_na_rede/customer/account/edit/'),
 (1159,'http://192.168.0.57/super_na_rede/customer/address/new/','http://192.168.0.57/super_na_rede/customer/account/edit/'),
 (1160,'http://192.168.0.57/super_na_rede/customer/address/new/','http://192.168.0.57/super_na_rede/customer/account/edit/'),
 (1161,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/address/new/'),
 (1162,'http://192.168.0.57/super_na_rede/customer/address/new/','http://192.168.0.57/super_na_rede/customer/account/edit/'),
 (1163,'http://192.168.0.57/super_na_rede/customer/address/new/','http://192.168.0.57/super_na_rede/customer/account/edit/'),
 (1164,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/address/new/'),
 (1165,'http://192.168.0.57/super_na_rede/customer/address/new/','http://192.168.0.57/super_na_rede/customer/account/edit/'),
 (1166,'http://192.168.0.57/super_na_rede/customer/address/new/','http://192.168.0.57/super_na_rede/customer/account/edit/'),
 (1167,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/address/new/'),
 (1168,'http://192.168.0.57/super_na_rede/customer/address/new/','http://192.168.0.57/super_na_rede/customer/account/edit/'),
 (1169,'http://192.168.0.57/super_na_rede/customer/address/new/','http://192.168.0.57/super_na_rede/customer/account/edit/'),
 (1170,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/address/new/'),
 (1171,'http://192.168.0.57/super_na_rede/customer/address/new/','http://192.168.0.57/super_na_rede/customer/account/edit/'),
 (1172,'http://192.168.0.57/super_na_rede/customer/address/new/','http://192.168.0.57/super_na_rede/customer/account/edit/'),
 (1173,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/address/new/'),
 (1174,'http://192.168.0.57/super_na_rede/customer/address/new/','http://192.168.0.57/super_na_rede/customer/account/edit/'),
 (1175,'http://192.168.0.57/super_na_rede/customer/address/new/','http://192.168.0.57/super_na_rede/customer/account/edit/'),
 (1176,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/address/new/'),
 (1177,'http://192.168.0.57/super_na_rede/customer/address/new/','http://192.168.0.57/super_na_rede/customer/account/edit/'),
 (1178,'http://192.168.0.57/super_na_rede/customer/address/new/','http://192.168.0.57/super_na_rede/customer/account/edit/'),
 (1179,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/address/new/'),
 (1180,'http://192.168.0.57/super_na_rede/customer/address/new/','http://192.168.0.57/super_na_rede/customer/account/edit/'),
 (1181,'http://192.168.0.57/super_na_rede/customer/address/new/','http://192.168.0.57/super_na_rede/customer/account/edit/'),
 (1182,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/address/new/'),
 (1183,'http://192.168.0.57/super_na_rede/customer/address/new/','http://192.168.0.57/super_na_rede/customer/account/edit/'),
 (1184,'http://192.168.0.57/super_na_rede/customer/address/new/','http://192.168.0.57/super_na_rede/customer/account/edit/'),
 (1185,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/address/new/'),
 (1186,'http://192.168.0.57/super_na_rede/customer/address/new/','http://192.168.0.57/super_na_rede/customer/account/edit/'),
 (1187,'http://192.168.0.57/super_na_rede/customer/address/new/','http://192.168.0.57/super_na_rede/customer/account/edit/'),
 (1188,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/address/new/'),
 (1189,'http://192.168.0.57/super_na_rede/customer/address/new/','http://192.168.0.57/super_na_rede/customer/account/edit/'),
 (1190,'http://192.168.0.57/super_na_rede/customer/address/new/','http://192.168.0.57/super_na_rede/customer/account/edit/'),
 (1191,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/address/new/'),
 (1192,'http://192.168.0.57/super_na_rede/customer/address/new/','http://192.168.0.57/super_na_rede/customer/account/edit/'),
 (1193,'http://192.168.0.57/super_na_rede/customer/address/new/','http://192.168.0.57/super_na_rede/customer/account/edit/'),
 (1194,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/address/new/'),
 (1195,'http://192.168.0.57/super_na_rede/customer/address/new/','http://192.168.0.57/super_na_rede/customer/account/edit/'),
 (1196,'http://192.168.0.57/super_na_rede/customer/address/new/','http://192.168.0.57/super_na_rede/customer/account/edit/'),
 (1197,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/address/new/'),
 (1198,'http://192.168.0.57/super_na_rede/customer/address/new/','http://192.168.0.57/super_na_rede/customer/account/edit/'),
 (1199,'http://192.168.0.57/super_na_rede/customer/address/new/','http://192.168.0.57/super_na_rede/customer/account/edit/'),
 (1200,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/address/new/'),
 (1201,'http://192.168.0.57/super_na_rede/customer/address/new/','http://192.168.0.57/super_na_rede/customer/account/edit/'),
 (1202,'http://192.168.0.57/super_na_rede/customer/address/new/','http://192.168.0.57/super_na_rede/customer/account/edit/'),
 (1203,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/address/new/'),
 (1204,'http://192.168.0.57/super_na_rede/customer/address/new/','http://192.168.0.57/super_na_rede/customer/account/edit/'),
 (1205,'http://192.168.0.57/super_na_rede/customer/address/new/','http://192.168.0.57/super_na_rede/customer/account/edit/'),
 (1206,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/address/new/'),
 (1207,'http://192.168.0.57/super_na_rede/customer/address/new/','http://192.168.0.57/super_na_rede/customer/account/edit/'),
 (1208,'http://192.168.0.57/super_na_rede/customer/address/new/','http://192.168.0.57/super_na_rede/customer/account/edit/'),
 (1209,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/address/new/'),
 (1210,'http://192.168.0.57/super_na_rede/customer/address/new/','http://192.168.0.57/super_na_rede/customer/account/edit/'),
 (1211,'http://192.168.0.57/super_na_rede/customer/address/new/','http://192.168.0.57/super_na_rede/customer/account/edit/'),
 (1212,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/address/new/'),
 (1213,'http://192.168.0.57/super_na_rede/customer/address/formPost/','http://192.168.0.57/super_na_rede/customer/address/new/'),
 (1214,'http://192.168.0.57/super_na_rede/customer/address/new/','http://192.168.0.57/super_na_rede/customer/account/edit/'),
 (1215,'http://192.168.0.57/super_na_rede/customer/address/new/','http://192.168.0.57/super_na_rede/customer/account/edit/'),
 (1216,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/address/new/'),
 (1217,'http://192.168.0.57/super_na_rede/customer/address/formPost/','http://192.168.0.57/super_na_rede/customer/address/new/'),
 (1218,'http://192.168.0.57/super_na_rede/customer/address/edit/','http://192.168.0.57/super_na_rede/customer/address/new/'),
 (1219,'http://192.168.0.57/super_na_rede/customer/address/edit/','http://192.168.0.57/super_na_rede/customer/address/new/'),
 (1220,'http://192.168.0.57/super_na_rede/customer/address/formPost/','http://192.168.0.57/super_na_rede/customer/address/new/');
INSERT INTO `supernarede`.`log_url_info` VALUES  (1221,'http://192.168.0.57/super_na_rede/customer/address/edit/','http://192.168.0.57/super_na_rede/customer/address/new/'),
 (1222,'http://192.168.0.57/super_na_rede/customer/address/edit/','http://192.168.0.57/super_na_rede/customer/address/new/'),
 (1223,'http://192.168.0.57/super_na_rede/customer/address/new/','http://192.168.0.57/super_na_rede/customer/account/edit/'),
 (1224,'http://192.168.0.57/super_na_rede/customer/address/new/','http://192.168.0.57/super_na_rede/customer/account/edit/'),
 (1225,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/address/new/'),
 (1226,'http://192.168.0.57/super_na_rede/customer/address/formPost/','http://192.168.0.57/super_na_rede/customer/address/new/'),
 (1227,'http://192.168.0.57/super_na_rede/customer/address/edit/','http://192.168.0.57/super_na_rede/customer/address/new/'),
 (1228,'http://192.168.0.57/super_na_rede/customer/address/edit/','http://192.168.0.57/super_na_rede/customer/address/new/'),
 (1229,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/address/edit/'),
 (1230,'http://192.168.0.57/super_na_rede/customer/address/edit/',''),
 (1231,'http://192.168.0.57/super_na_rede/customer/address/edit/',''),
 (1232,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/address/edit/'),
 (1233,'http://192.168.0.57/super_na_rede/customer/address/formPost/','http://192.168.0.57/super_na_rede/customer/address/edit/'),
 (1234,'http://192.168.0.57/super_na_rede/customer/address/index/','http://192.168.0.57/super_na_rede/customer/address/edit/'),
 (1235,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/address/index/'),
 (1236,'http://192.168.0.57/super_na_rede/customer/address/edit/id/1/','http://192.168.0.57/super_na_rede/customer/address/index/'),
 (1237,'http://192.168.0.57/super_na_rede/customer/address/edit/id/1/','http://192.168.0.57/super_na_rede/customer/address/index/'),
 (1238,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/address/edit/id/1/'),
 (1239,'http://192.168.0.57/super_na_rede/customer/address/edit/',''),
 (1240,'http://192.168.0.57/super_na_rede/customer/address/edit/',''),
 (1241,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/address/edit/'),
 (1242,'http://192.168.0.57/super_na_rede/customer/address/edit/',''),
 (1243,'http://192.168.0.57/super_na_rede/customer/address/edit/',''),
 (1244,'http://192.168.0.57/super_na_rede/customer/address/edit/',''),
 (1245,'http://192.168.0.57/super_na_rede/customer/address/edit/',''),
 (1246,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/address/edit/'),
 (1247,'http://192.168.0.57/super_na_rede/customer/address/edit/',''),
 (1248,'http://192.168.0.57/super_na_rede/customer/address/edit/',''),
 (1249,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/address/edit/'),
 (1250,'http://192.168.0.57/super_na_rede/customer/address/edit/',''),
 (1251,'http://192.168.0.57/super_na_rede/customer/address/edit/',''),
 (1252,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/address/edit/'),
 (1253,'http://192.168.0.57/super_na_rede/customer/address/edit/',''),
 (1254,'http://192.168.0.57/super_na_rede/customer/address/edit/',''),
 (1255,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/address/edit/'),
 (1256,'http://192.168.0.57/super_na_rede/customer/address/edit/',''),
 (1257,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/address/edit/'),
 (1258,'http://192.168.0.57/super_na_rede/customer/address/edit/',''),
 (1259,'http://192.168.0.57/super_na_rede/customer/address/edit/',''),
 (1260,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/address/edit/'),
 (1261,'http://192.168.0.57/super_na_rede/customer/address/edit/',''),
 (1262,'http://192.168.0.57/super_na_rede/customer/address/edit/',''),
 (1263,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/address/edit/'),
 (1264,'http://192.168.0.57/super_na_rede/customer/address/edit/',''),
 (1265,'http://192.168.0.57/super_na_rede/customer/address/edit/',''),
 (1266,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/address/edit/'),
 (1267,'http://192.168.0.57/super_na_rede/customer/address/edit/',''),
 (1268,'http://192.168.0.57/super_na_rede/customer/address/edit/',''),
 (1269,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/address/edit/'),
 (1270,'http://192.168.0.57/super_na_rede/sales/order/history/','http://192.168.0.57/super_na_rede/customer/address/edit/'),
 (1271,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/sales/order/history/'),
 (1272,'http://192.168.0.57/super_na_rede/customer/account/login/referer/aHR0cDovLzE5Mi4xNjguMC41Ny9zdXBlcl9uYV9yZWRlL2N1c3RvbWVyL2FjY291bnQvaW5kZXgv/',''),
 (1273,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/account/login/referer/aHR0cDovLzE5Mi4xNjguMC41Ny9zdXBlcl9uYV9yZWRlL2N1c3RvbWVyL2FjY291bnQvaW5kZXgv/'),
 (1274,'http://192.168.0.57/super_na_rede/checkout/cart/',''),
 (1275,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/checkout/cart/'),
 (1276,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/account/login/referer/aHR0cDovLzE5Mi4xNjguMC41Ny9zdXBlcl9uYV9yZWRlL2N1c3RvbWVyL2FjY291bnQvaW5kZXgv/'),
 (1277,'http://192.168.0.57/super_na_rede/checkout/cart/',''),
 (1278,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/checkout/cart/'),
 (1279,'http://192.168.0.57/super_na_rede/customer/account/login/referer/aHR0cDovLzE5Mi4xNjguMC41Ny9zdXBlcl9uYV9yZWRlL3NhbGVzL29yZGVyL2hpc3Rvcnkv/','http://192.168.0.57/super_na_rede/customer/address/edit/'),
 (1280,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/account/login/referer/aHR0cDovLzE5Mi4xNjguMC41Ny9zdXBlcl9uYV9yZWRlL3NhbGVzL29yZGVyL2hpc3Rvcnkv/'),
 (1281,'http://192.168.0.57/super_na_rede/customer/account/loginPost/referer/aHR0cDovLzE5Mi4xNjguMC41Ny9zdXBlcl9uYV9yZWRlL3NhbGVzL29yZGVyL2hpc3Rvcnkv/','http://192.168.0.57/super_na_rede/customer/account/login/referer/aHR0cDovLzE5Mi4xNjguMC41Ny9zdXBlcl9uYV9yZWRlL3NhbGVzL29yZGVyL2hpc3Rvcnkv/'),
 (1282,'http://192.168.0.57/super_na_rede/customer/account/login/referer/aHR0cDovLzE5Mi4xNjguMC41Ny9zdXBlcl9uYV9yZWRlL3NhbGVzL29yZGVyL2hpc3Rvcnkv/','http://192.168.0.57/super_na_rede/customer/account/login/referer/aHR0cDovLzE5Mi4xNjguMC41Ny9zdXBlcl9uYV9yZWRlL3NhbGVzL29yZGVyL2hpc3Rvcnkv/'),
 (1283,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/account/login/referer/aHR0cDovLzE5Mi4xNjguMC41Ny9zdXBlcl9uYV9yZWRlL3NhbGVzL29yZGVyL2hpc3Rvcnkv/'),
 (1284,'http://192.168.0.57/super_na_rede/customer/account/loginPost/referer/aHR0cDovLzE5Mi4xNjguMC41Ny9zdXBlcl9uYV9yZWRlL3NhbGVzL29yZGVyL2hpc3Rvcnkv/','http://192.168.0.57/super_na_rede/customer/account/login/referer/aHR0cDovLzE5Mi4xNjguMC41Ny9zdXBlcl9uYV9yZWRlL3NhbGVzL29yZGVyL2hpc3Rvcnkv/'),
 (1285,'http://192.168.0.57/super_na_rede/customer/account/login/referer/aHR0cDovLzE5Mi4xNjguMC41Ny9zdXBlcl9uYV9yZWRlL3NhbGVzL29yZGVyL2hpc3Rvcnkv/','http://192.168.0.57/super_na_rede/customer/account/login/referer/aHR0cDovLzE5Mi4xNjguMC41Ny9zdXBlcl9uYV9yZWRlL3NhbGVzL29yZGVyL2hpc3Rvcnkv/'),
 (1286,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/account/login/referer/aHR0cDovLzE5Mi4xNjguMC41Ny9zdXBlcl9uYV9yZWRlL3NhbGVzL29yZGVyL2hpc3Rvcnkv/'),
 (1287,'http://192.168.0.57/super_na_rede/customer/account/loginPost/referer/aHR0cDovLzE5Mi4xNjguMC41Ny9zdXBlcl9uYV9yZWRlL3NhbGVzL29yZGVyL2hpc3Rvcnkv/','http://192.168.0.57/super_na_rede/customer/account/login/referer/aHR0cDovLzE5Mi4xNjguMC41Ny9zdXBlcl9uYV9yZWRlL3NhbGVzL29yZGVyL2hpc3Rvcnkv/'),
 (1288,'http://192.168.0.57/super_na_rede/sales/order/history/','http://192.168.0.57/super_na_rede/customer/account/login/referer/aHR0cDovLzE5Mi4xNjguMC41Ny9zdXBlcl9uYV9yZWRlL3NhbGVzL29yZGVyL2hpc3Rvcnkv/'),
 (1289,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/sales/order/history/'),
 (1290,'http://192.168.0.57/super_na_rede/customer/account/','http://192.168.0.57/super_na_rede/sales/order/history/'),
 (1291,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/account/'),
 (1292,'http://192.168.0.57/super_na_rede/customer/account/loginPost/referer/aHR0cDovLzE5Mi4xNjguMC41Ny9zdXBlcl9uYV9yZWRlL2N1c3RvbWVyL2FjY291bnQvaW5kZXgv/','http://192.168.0.57/super_na_rede/customer/account/login/referer/aHR0cDovLzE5Mi4xNjguMC41Ny9zdXBlcl9uYV9yZWRlL2N1c3RvbWVyL2FjY291bnQvaW5kZXgv/'),
 (1293,'http://192.168.0.57/super_na_rede/sales/order/history/','http://192.168.0.57/super_na_rede/customer/account/'),
 (1294,'http://192.168.0.57/super_na_rede/customer/account/index/','http://192.168.0.57/super_na_rede/customer/account/login/referer/aHR0cDovLzE5Mi4xNjguMC41Ny9zdXBlcl9uYV9yZWRlL2N1c3RvbWVyL2FjY291bnQvaW5kZXgv/'),
 (1295,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/sales/order/history/'),
 (1296,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/account/index/'),
 (1297,'http://192.168.0.57/super_na_rede/newsletter/manage/','http://192.168.0.57/super_na_rede/sales/order/history/'),
 (1298,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/newsletter/manage/'),
 (1299,'http://192.168.0.57/super_na_rede/customer/account/edit/','http://192.168.0.57/super_na_rede/customer/account/index/'),
 (1300,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/account/edit/'),
 (1301,'http://192.168.0.57/super_na_rede/customer/account/','http://192.168.0.57/super_na_rede/customer/account/edit/'),
 (1302,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/account/'),
 (1303,'http://192.168.0.57/super_na_rede/customer/address/','http://192.168.0.57/super_na_rede/customer/account/'),
 (1304,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/address/'),
 (1305,'http://192.168.0.57/super_na_rede/sales/order/history/','http://192.168.0.57/super_na_rede/customer/address/'),
 (1306,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/sales/order/history/'),
 (1307,'http://192.168.0.57/super_na_rede/newsletter/manage/','http://192.168.0.57/super_na_rede/sales/order/history/'),
 (1308,'http://192.168.0.57/super_na_rede/newsletter/manage/','http://192.168.0.57/super_na_rede/sales/order/history/'),
 (1309,'http://192.168.0.57/super_na_rede/customer/account/','http://192.168.0.57/super_na_rede/sales/order/history/'),
 (1310,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/account/'),
 (1311,'http://192.168.0.57/super_na_rede/newsletter/manage/','http://192.168.0.57/super_na_rede/sales/order/history/'),
 (1312,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/newsletter/manage/'),
 (1313,'http://192.168.0.57/super_na_rede/customer/account/edit/','http://192.168.0.57/super_na_rede/customer/account/'),
 (1314,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/account/edit/'),
 (1315,'http://192.168.0.57/super_na_rede/customer/account/','http://192.168.0.57/super_na_rede/customer/account/edit/'),
 (1316,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/account/'),
 (1317,'http://192.168.0.57/super_na_rede/customer/address/','http://192.168.0.57/super_na_rede/customer/account/'),
 (1318,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/address/'),
 (1319,'http://192.168.0.57/super_na_rede/customer/address/new/','http://192.168.0.57/super_na_rede/customer/address/'),
 (1320,'http://192.168.0.57/super_na_rede/customer/address/new/','http://192.168.0.57/super_na_rede/customer/address/'),
 (1321,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/address/new/'),
 (1322,'http://192.168.0.57/super_na_rede/customer/address/','http://192.168.0.57/super_na_rede/customer/account/'),
 (1323,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/customer/address/'),
 (1324,'http://192.168.0.57/super_na_rede/sales/order/history/','http://192.168.0.57/super_na_rede/customer/address/'),
 (1325,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/sales/order/history/'),
 (1326,'http://192.168.0.57/super_na_rede/newsletter/manage/','http://192.168.0.57/super_na_rede/sales/order/history/'),
 (1327,'http://192.168.0.57/super_na_rede/js/scriptaculous/slider.js','http://192.168.0.57/super_na_rede/newsletter/manage/');
UNLOCK TABLES;
/*!40000 ALTER TABLE `log_url_info` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`log_visitor`
--

DROP TABLE IF EXISTS `supernarede`.`log_visitor`;
CREATE TABLE  `supernarede`.`log_visitor` (
  `visitor_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `session_id` char(64) NOT NULL DEFAULT '',
  `first_visit_at` datetime DEFAULT NULL,
  `last_visit_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_url_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `store_id` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`visitor_id`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8 COMMENT='System visitors log';

--
-- Dumping data for table `supernarede`.`log_visitor`
--

/*!40000 ALTER TABLE `log_visitor` DISABLE KEYS */;
LOCK TABLES `log_visitor` WRITE;
INSERT INTO `supernarede`.`log_visitor` VALUES  (1,'t5cb5oj6b8svtpsojlos06mr12','2011-08-08 12:59:10','2011-08-08 12:59:11',1,1),
 (2,'vjjscovoehds3hm51jrv7o64l0','2011-08-08 14:38:26','2011-08-08 15:03:40',40,1),
 (3,'fdqngqsts8nbkn6bsmqh1ifgk4','2011-08-08 15:07:06','2011-08-08 15:07:06',41,1),
 (4,'hjp79ps7m5stsj83m3aq4mbt96','2011-08-08 16:29:41','2011-08-08 21:18:04',91,1),
 (5,'s8oet34dh6c8qf38ihv0r2rng0','2011-08-09 12:20:54','2011-08-09 13:11:06',114,1),
 (6,'2m1kqvipsjngm90i0ulevshh25','2011-08-09 14:29:39','2011-08-09 15:28:51',124,1),
 (7,'kf2ed7e2684t77qn72fg2vjq22','2011-08-10 17:56:10','2011-08-10 18:23:09',133,1),
 (8,'9i36vefpuji01cp0l3p1gch6d7','2011-08-10 19:55:46','2011-08-10 19:55:46',0,1),
 (9,'volrl0iesr08oud3su3ikf6cm7','2011-08-11 12:28:36','2011-08-11 12:28:36',0,1),
 (10,'volrl0iesr08oud3su3ikf6cm7','2011-08-11 13:15:40','2011-08-11 13:15:40',0,1),
 (11,'volrl0iesr08oud3su3ikf6cm7','2011-08-11 13:17:11','2011-08-11 13:25:45',159,1),
 (12,'2m3qrjd7tue674h81ecvbhk555','2011-08-11 17:28:27','2011-08-11 18:12:40',211,1),
 (13,'vnmkc6jgmqnc6fut5lbf453a43','2011-08-12 12:35:13','2011-08-12 12:35:20',214,1),
 (14,'o57nptarsnns5a43kbeh7mmlg3','2011-08-12 18:29:01','2011-08-12 21:39:55',291,1),
 (15,'6h31b4nqt6s1keotqgq9k1s4c0','2011-08-16 12:49:26','2011-08-16 15:47:40',338,1),
 (16,'dfbats790n8el5slv43nubhkh4','2011-08-16 16:56:59','2011-08-16 16:56:59',0,1),
 (17,'dfbats790n8el5slv43nubhkh4','2011-08-16 16:57:10','2011-08-16 16:57:10',0,1),
 (18,'dfbats790n8el5slv43nubhkh4','2011-08-16 16:57:10','2011-08-16 16:57:10',0,1),
 (19,'dfbats790n8el5slv43nubhkh4','2011-08-16 16:57:56','2011-08-16 20:57:04',367,1),
 (20,'8k1267fabasfq4gen5s9tifg80','2011-08-17 12:20:59','2011-08-17 12:21:04',368,1),
 (21,'utpk71f0su2g02f3rpjjdl3np7','2011-08-17 14:08:05','2011-08-17 14:08:05',0,1),
 (22,'utpk71f0su2g02f3rpjjdl3np7','2011-08-17 14:08:46','2011-08-17 16:22:40',416,1),
 (23,'9dvbdkvd4hg10e5vqounfn0cb0','2011-08-17 19:30:59','2011-08-17 21:19:26',461,1),
 (24,'a6pj1o7vf3erol12j1ve7je7o5','2011-08-17 20:45:12','2011-08-17 20:45:12',0,1),
 (25,'a6pj1o7vf3erol12j1ve7je7o5','2011-08-17 20:52:14','2011-08-17 20:52:14',0,1),
 (26,'a6pj1o7vf3erol12j1ve7je7o5','2011-08-17 21:02:29','2011-08-17 21:02:50',459,1),
 (27,'h0h2voseje6tcmvi0ai39jare4','2011-08-18 12:28:45','2011-08-18 12:44:02',476,1),
 (28,'n3ldlboctsdj5j751kmhi63fr0','2011-08-18 17:12:24','2011-08-18 17:12:36',477,1),
 (29,'j021p73fclma8v3no0569aqur6','2011-08-18 19:35:31','2011-08-18 22:01:57',584,1),
 (30,'931eju7g04irpg12jfvl2re8q3','2011-08-19 12:14:34','2011-08-19 13:55:51',638,1),
 (31,'nb5h8ojvsnhprrp6htl7ujt4a4','2011-08-19 15:54:48','2011-08-19 19:23:28',732,1),
 (32,'fuegek71vjmpdc240057siv4u0','2011-08-19 18:06:08','2011-08-19 18:06:57',717,1),
 (33,'q02mm95dl3dhq65jheikgkd312','2011-08-19 20:45:25','2011-08-19 21:07:20',737,1),
 (34,'dlk9k1rgics12ntgir058vem55','2011-08-22 12:57:04','2011-08-22 12:57:23',738,1),
 (35,'sedjim9en8lmqh9080p10mkvi0','2011-08-22 15:37:00','2011-08-22 16:39:51',748,1),
 (36,'le5ickkc2jnrb2vhituntvqmg5','2011-08-23 12:27:39','2011-08-23 12:27:45',749,1),
 (37,'16dmh770dfajdj2shjg8dpmf80','2011-08-23 15:28:37','2011-08-23 15:44:31',769,1),
 (38,'sij3btkn66srfnifsgq59og605','2011-08-23 15:41:12','2011-08-23 15:43:43',765,1),
 (39,'uo667242nrsbirrjuv8rbiatj2','2011-08-23 17:27:30','2011-08-23 17:27:37',770,1),
 (40,'623bfnefb75oo4m1s3nervlmd2','2011-08-24 12:17:05','2011-08-24 13:56:05',785,1),
 (41,'t5hkjke3rcs6g5cplo63coqrv3','2011-08-25 13:01:30','2011-08-25 13:03:38',787,1),
 (42,'e02c94b5erc102g7uj64oj88u5','2011-09-15 19:59:15','2011-09-15 20:44:55',810,1),
 (43,'kt0scrqpvgvun6ptk471i5hvh5','2011-09-16 12:20:36','2011-09-16 12:20:42',811,1),
 (44,'48hcm62ili1m5ang6sn48ngma7','2011-09-16 12:20:36','2011-09-16 13:30:05',829,1),
 (45,'9quj65cr6reg9d477ur35iuh37','2011-09-19 17:56:37','2011-09-19 18:01:49',850,1),
 (46,'db0a09ucu2mjfe95lvdm37blp3','2011-09-19 17:58:30','2011-09-19 19:25:36',999,1),
 (47,'7j9bg95rl0me8suqg7k6l17qh1','2011-09-19 18:05:31','2011-09-19 19:33:34',1016,1),
 (48,'7hkc6i2kj9lq17rr6fli9quqk0','2011-09-19 18:11:42','2011-09-19 19:34:06',1018,1),
 (49,'pl9n41tb8b8n19calommh9mhv1','2011-09-19 19:25:44','2011-09-19 19:27:11',1014,1),
 (50,'km9o3eparlatmsok6p1hk9e9i6','2011-09-20 12:23:31','2011-09-20 12:23:41',1020,1),
 (51,'lhc0iv9pk52uipdbf8tphvls73','2011-09-21 18:41:06','2011-09-21 21:04:12',1063,1),
 (52,'sbmgrcjvu1ff9c1robo398i5r4','2011-09-22 12:33:12','2011-09-22 12:33:23',1065,1),
 (53,'80rgsnmtrunffkloigv4fm50k5','2011-09-22 13:48:34','2011-09-22 16:11:09',1271,1),
 (54,'gralldsovh1nmnom3g16o9sg60','2011-09-22 17:36:18','2011-09-22 17:36:18',0,1),
 (55,'gralldsovh1nmnom3g16o9sg60','2011-09-22 17:36:19','2011-09-22 17:37:08',1278,1),
 (56,'00vsi2pkt3lc94t820qa0mf4r4','2011-09-22 17:36:45','2011-09-22 17:36:49',1275,1),
 (57,'5l3aluldn4ibac3af7jv2ad7f7','2011-09-22 18:06:29','2011-09-22 18:06:29',0,1),
 (58,'2g52gndp0sb4hvchlqo8qhucn7','2011-09-22 18:06:44','2011-09-22 18:47:22',1312,1),
 (59,'fuh15gkdhj41i40pb8edmgeu93','2011-09-22 18:45:45','2011-09-22 21:04:16',1327,1);
UNLOCK TABLES;
/*!40000 ALTER TABLE `log_visitor` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`log_visitor_info`
--

DROP TABLE IF EXISTS `supernarede`.`log_visitor_info`;
CREATE TABLE  `supernarede`.`log_visitor_info` (
  `visitor_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `http_referer` varchar(255) DEFAULT NULL,
  `http_user_agent` varchar(255) DEFAULT NULL,
  `http_accept_charset` varchar(255) DEFAULT NULL,
  `http_accept_language` varchar(255) DEFAULT NULL,
  `server_addr` bigint(20) DEFAULT NULL,
  `remote_addr` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`visitor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Additional information by visitor';

--
-- Dumping data for table `supernarede`.`log_visitor_info`
--

/*!40000 ALTER TABLE `log_visitor_info` DISABLE KEYS */;
LOCK TABLES `log_visitor_info` WRITE;
INSERT INTO `supernarede`.`log_visitor_info` VALUES  (1,'','Mozilla/5.0 (X11; U; Linux x86_64; pt-BR; rv:1.9.2.18) Gecko/20110628 Ubuntu/10.04 (lucid) Firefox/3.6.18','ISO-8859-1,utf-8;q=0.7,*;q=0.7','pt-br,pt;q=0.8,en-us;q=0.5,en;q=0.3',2130706433,2130706433),
 (2,'','Mozilla/5.0 (X11; U; Linux x86_64; pt-BR; rv:1.9.2.18) Gecko/20110628 Ubuntu/10.04 (lucid) Firefox/3.6.18','ISO-8859-1,utf-8;q=0.7,*;q=0.7','pt-br,pt;q=0.8,en-us;q=0.5,en;q=0.3',2130706433,2130706433),
 (3,'','Mozilla/5.0 (X11; U; Linux x86_64; pt-BR; rv:1.9.2.18) Gecko/20110628 Ubuntu/10.04 (lucid) Firefox/3.6.18','ISO-8859-1,utf-8;q=0.7,*;q=0.7','pt-br,pt;q=0.8,en-us;q=0.5,en;q=0.3',2130706689,2130706689),
 (4,'','Mozilla/5.0 (X11; U; Linux x86_64; pt-BR; rv:1.9.2.18) Gecko/20110628 Ubuntu/10.04 (lucid) Firefox/3.6.18','ISO-8859-1,utf-8;q=0.7,*;q=0.7','pt-br,pt;q=0.8,en-us;q=0.5,en;q=0.3',2130706689,2130706689),
 (5,'','Mozilla/5.0 (X11; U; Linux x86_64; pt-BR; rv:1.9.2.18) Gecko/20110628 Ubuntu/10.04 (lucid) Firefox/3.6.18','ISO-8859-1,utf-8;q=0.7,*;q=0.7','pt-br,pt;q=0.8,en-us;q=0.5,en;q=0.3',2130706689,2130706689),
 (6,'','Mozilla/5.0 (X11; U; Linux x86_64; pt-BR; rv:1.9.2.18) Gecko/20110628 Ubuntu/10.04 (lucid) Firefox/3.6.18','ISO-8859-1,utf-8;q=0.7,*;q=0.7','pt-br,pt;q=0.8,en-us;q=0.5,en;q=0.3',2130706689,2130706689),
 (7,'','Mozilla/5.0 (X11; U; Linux x86_64; pt-BR; rv:1.9.2.18) Gecko/20110628 Ubuntu/10.04 (lucid) Firefox/3.6.18','ISO-8859-1,utf-8;q=0.7,*;q=0.7','pt-br,pt;q=0.8,en-us;q=0.5,en;q=0.3',2130706689,2130706689),
 (8,'','Mozilla/5.0 (X11; U; Linux x86_64; pt-BR; rv:1.9.2.18) Gecko/20110628 Ubuntu/10.04 (lucid) Firefox/3.6.18','ISO-8859-1,utf-8;q=0.7,*;q=0.7','pt-br,pt;q=0.8,en-us;q=0.5,en;q=0.3',2130706689,2130706689),
 (9,'','Mozilla/5.0 (X11; U; Linux x86_64; pt-BR; rv:1.9.2.18) Gecko/20110628 Ubuntu/10.04 (lucid) Firefox/3.6.18','ISO-8859-1,utf-8;q=0.7,*;q=0.7','pt-br,pt;q=0.8,en-us;q=0.5,en;q=0.3',2130706689,2130706689),
 (10,'','Mozilla/5.0 (X11; U; Linux x86_64; pt-BR; rv:1.9.2.18) Gecko/20110628 Ubuntu/10.04 (lucid) Firefox/3.6.18','ISO-8859-1,utf-8;q=0.7,*;q=0.7','pt-br,pt;q=0.8,en-us;q=0.5,en;q=0.3',2130706689,2130706689),
 (11,'','Mozilla/5.0 (X11; U; Linux x86_64; pt-BR; rv:1.9.2.18) Gecko/20110628 Ubuntu/10.04 (lucid) Firefox/3.6.18','ISO-8859-1,utf-8;q=0.7,*;q=0.7','pt-br,pt;q=0.8,en-us;q=0.5,en;q=0.3',2130706689,2130706689),
 (12,'','Mozilla/5.0 (X11; U; Linux x86_64; pt-BR; rv:1.9.2.18) Gecko/20110628 Ubuntu/10.04 (lucid) Firefox/3.6.18','ISO-8859-1,utf-8;q=0.7,*;q=0.7','pt-br,pt;q=0.8,en-us;q=0.5,en;q=0.3',2130706689,2130706689),
 (13,'','Mozilla/5.0 (X11; U; Linux x86_64; pt-BR; rv:1.9.2.18) Gecko/20110628 Ubuntu/10.04 (lucid) Firefox/3.6.18','ISO-8859-1,utf-8;q=0.7,*;q=0.7','pt-br,pt;q=0.8,en-us;q=0.5,en;q=0.3',2130706689,2130706689),
 (14,'http://supernarede.local/listas-prontas','Mozilla/5.0 (X11; U; Linux x86_64; pt-BR; rv:1.9.2.18) Gecko/20110628 Ubuntu/10.04 (lucid) Firefox/3.6.18','ISO-8859-1,utf-8;q=0.7,*;q=0.7','pt-br,pt;q=0.8,en-us;q=0.5,en;q=0.3',2130706689,2130706689),
 (15,'http://supernarede.local/faq','Mozilla/5.0 (X11; U; Linux x86_64; pt-BR; rv:1.9.2.18) Gecko/20110628 Ubuntu/10.04 (lucid) Firefox/3.6.18','ISO-8859-1,utf-8;q=0.7,*;q=0.7','pt-br,pt;q=0.8,en-us;q=0.5,en;q=0.3',2130706689,2130706689),
 (16,'http://supernarede.local/','Mozilla/5.0 (X11; U; Linux x86_64; pt-BR; rv:1.9.2.18) Gecko/20110628 Ubuntu/10.04 (lucid) Firefox/3.6.18','ISO-8859-1,utf-8;q=0.7,*;q=0.7','pt-br,pt;q=0.8,en-us;q=0.5,en;q=0.3',2130706689,2130706689),
 (17,'http://supernarede.local/','Mozilla/5.0 (X11; U; Linux x86_64; pt-BR; rv:1.9.2.18) Gecko/20110628 Ubuntu/10.04 (lucid) Firefox/3.6.18','ISO-8859-1,utf-8;q=0.7,*;q=0.7','pt-br,pt;q=0.8,en-us;q=0.5,en;q=0.3',2130706689,2130706689),
 (18,'http://supernarede.local/','Mozilla/5.0 (X11; U; Linux x86_64; pt-BR; rv:1.9.2.18) Gecko/20110628 Ubuntu/10.04 (lucid) Firefox/3.6.18','ISO-8859-1,utf-8;q=0.7,*;q=0.7','pt-br,pt;q=0.8,en-us;q=0.5,en;q=0.3',2130706689,2130706689),
 (19,'','Mozilla/5.0 (X11; U; Linux x86_64; pt-BR; rv:1.9.2.18) Gecko/20110628 Ubuntu/10.04 (lucid) Firefox/3.6.18','ISO-8859-1,utf-8;q=0.7,*;q=0.7','pt-br,pt;q=0.8,en-us;q=0.5,en;q=0.3',2130706689,2130706689),
 (20,'','Mozilla/5.0 (X11; U; Linux x86_64; pt-BR; rv:1.9.2.18) Gecko/20110628 Ubuntu/10.04 (lucid) Firefox/3.6.18','ISO-8859-1,utf-8;q=0.7,*;q=0.7','pt-br,pt;q=0.8,en-us;q=0.5,en;q=0.3',2130706689,2130706689),
 (21,'','Mozilla/5.0 (X11; U; Linux x86_64; pt-BR; rv:1.9.2.18) Gecko/20110628 Ubuntu/10.04 (lucid) Firefox/3.6.18','ISO-8859-1,utf-8;q=0.7,*;q=0.7','pt-br,pt;q=0.8,en-us;q=0.5,en;q=0.3',2130706689,2130706689),
 (22,'','Mozilla/5.0 (X11; U; Linux x86_64; pt-BR; rv:1.9.2.18) Gecko/20110628 Ubuntu/10.04 (lucid) Firefox/3.6.18','ISO-8859-1,utf-8;q=0.7,*;q=0.7','pt-br,pt;q=0.8,en-us;q=0.5,en;q=0.3',2130706689,2130706689),
 (23,'http://supernarede.local/','Mozilla/5.0 (X11; U; Linux x86_64; pt-BR; rv:1.9.2.18) Gecko/20110628 Ubuntu/10.04 (lucid) Firefox/3.6.18','ISO-8859-1,utf-8;q=0.7,*;q=0.7','pt-br,pt;q=0.8,en-us;q=0.5,en;q=0.3',2130706689,2130706689),
 (24,'','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/13.0.782.112 Safari/535.1','ISO-8859-1,utf-8;q=0.7,*;q=0.3','pt-BR,pt;q=0.8,en-US;q=0.6,en;q=0.4',2130706689,2130706689),
 (25,'','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/13.0.782.112 Safari/535.1','ISO-8859-1,utf-8;q=0.7,*;q=0.3','pt-BR,pt;q=0.8,en-US;q=0.6,en;q=0.4',2130706689,2130706689),
 (26,'','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/13.0.782.112 Safari/535.1','ISO-8859-1,utf-8;q=0.7,*;q=0.3','pt-BR,pt;q=0.8,en-US;q=0.6,en;q=0.4',2130706689,2130706689),
 (27,'http://supernarede.local/catalogsearch/result/?cat=7&q=skol&x=18&y=55','Mozilla/5.0 (X11; U; Linux x86_64; pt-BR; rv:1.9.2.18) Gecko/20110628 Ubuntu/10.04 (lucid) Firefox/3.6.18','ISO-8859-1,utf-8;q=0.7,*;q=0.7','pt-br,pt;q=0.8,en-us;q=0.5,en;q=0.3',2130706689,2130706689),
 (28,'http://supernarede.local/cerveja-exemplo-skol-escura','Mozilla/5.0 (X11; U; Linux x86_64; pt-BR; rv:1.9.2.18) Gecko/20110628 Ubuntu/10.04 (lucid) Firefox/3.6.18','ISO-8859-1,utf-8;q=0.7,*;q=0.7','pt-br,pt;q=0.8,en-us;q=0.5,en;q=0.3',2130706689,2130706689),
 (29,'http://supernarede.local/cerveja-exemplo-skol-escura','Mozilla/5.0 (X11; U; Linux x86_64; pt-BR; rv:1.9.2.18) Gecko/20110628 Ubuntu/10.04 (lucid) Firefox/3.6.18','ISO-8859-1,utf-8;q=0.7,*;q=0.7','pt-br,pt;q=0.8,en-us;q=0.5,en;q=0.3',2130706689,2130706689),
 (30,'http://supernarede.local/checkout/cart/','Mozilla/5.0 (X11; U; Linux x86_64; pt-BR; rv:1.9.2.18) Gecko/20110628 Ubuntu/10.04 (lucid) Firefox/3.6.18','ISO-8859-1,utf-8;q=0.7,*;q=0.7','pt-br,pt;q=0.8,en-us;q=0.5,en;q=0.3',2130706689,2130706689),
 (31,'http://supernarede.local/customer/account/create/','Mozilla/5.0 (X11; U; Linux x86_64; pt-BR; rv:1.9.2.20) Gecko/20110805 Ubuntu/10.04 (lucid) Firefox/3.6.20','ISO-8859-1,utf-8;q=0.7,*;q=0.7','pt-br,pt;q=0.8,en-us;q=0.5,en;q=0.3',2130706689,2130706689),
 (32,'','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/13.0.782.112 Safari/535.1','ISO-8859-1,utf-8;q=0.7,*;q=0.3','pt-BR,pt;q=0.8,en-US;q=0.6,en;q=0.4',2130706689,2130706689),
 (33,'http://supernarede.local/checkout/onepage/','Mozilla/5.0 (X11; U; Linux x86_64; pt-BR; rv:1.9.2.20) Gecko/20110805 Ubuntu/10.04 (lucid) Firefox/3.6.20','ISO-8859-1,utf-8;q=0.7,*;q=0.7','pt-br,pt;q=0.8,en-us;q=0.5,en;q=0.3',2130706689,2130706689),
 (34,'http://supernarede.local/','Mozilla/5.0 (X11; U; Linux x86_64; pt-BR; rv:1.9.2.20) Gecko/20110805 Ubuntu/10.04 (lucid) Firefox/3.6.20','ISO-8859-1,utf-8;q=0.7,*;q=0.7','pt-br,pt;q=0.8,en-us;q=0.5,en;q=0.3',2130706689,2130706689),
 (35,'http://supernarede.local/','Mozilla/5.0 (X11; U; Linux x86_64; pt-BR; rv:1.9.2.20) Gecko/20110805 Ubuntu/10.04 (lucid) Firefox/3.6.20','ISO-8859-1,utf-8;q=0.7,*;q=0.7','pt-br,pt;q=0.8,en-us;q=0.5,en;q=0.3',2130706689,2130706689),
 (36,'http://supernarede.local/departamentos/massa/instantanea','Mozilla/5.0 (X11; U; Linux x86_64; pt-BR; rv:1.9.2.20) Gecko/20110805 Ubuntu/10.04 (lucid) Firefox/3.6.20','ISO-8859-1,utf-8;q=0.7,*;q=0.7','pt-br,pt;q=0.8,en-us;q=0.5,en;q=0.3',2130706689,2130706689),
 (37,'http://supernarede.local/departamentos/massa/instantanea','Mozilla/5.0 (X11; U; Linux x86_64; pt-BR; rv:1.9.2.20) Gecko/20110805 Ubuntu/10.04 (lucid) Firefox/3.6.20','ISO-8859-1,utf-8;q=0.7,*;q=0.7','pt-br,pt;q=0.8,en-us;q=0.5,en;q=0.3',2130706689,2130706689),
 (38,'','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/13.0.782.112 Safari/535.1','ISO-8859-1,utf-8;q=0.7,*;q=0.3','pt-BR,pt;q=0.8,en-US;q=0.6,en;q=0.4',2130706689,2130706689),
 (39,'http://supernarede.local/departamentos/massa/espaguete','Mozilla/5.0 (X11; U; Linux x86_64; pt-BR; rv:1.9.2.20) Gecko/20110805 Ubuntu/10.04 (lucid) Firefox/3.6.20','ISO-8859-1,utf-8;q=0.7,*;q=0.7','pt-br,pt;q=0.8,en-us;q=0.5,en;q=0.3',2130706689,2130706689),
 (40,'http://supernarede.local/departamentos/massa/espaguete','Mozilla/5.0 (X11; U; Linux x86_64; pt-BR; rv:1.9.2.20) Gecko/20110805 Ubuntu/10.04 (lucid) Firefox/3.6.20','ISO-8859-1,utf-8;q=0.7,*;q=0.7','pt-br,pt;q=0.8,en-us;q=0.5,en;q=0.3',2130706689,2130706689),
 (41,'','Mozilla/5.0 (X11; U; Linux x86_64; pt-BR; rv:1.9.2.20) Gecko/20110805 Ubuntu/10.04 (lucid) Firefox/3.6.20','ISO-8859-1,utf-8;q=0.7,*;q=0.7','pt-br,pt;q=0.8,en-us;q=0.5,en;q=0.3',2130706689,2130706689),
 (42,'','Mozilla/5.0 (X11; U; Linux x86_64; pt-BR; rv:1.9.2.22) Gecko/20110905 Ubuntu/10.04 (lucid) Firefox/3.6.22','ISO-8859-1,utf-8;q=0.7,*;q=0.7','pt-br,pt;q=0.8,en-us;q=0.5,en;q=0.3',2130706689,2130706689),
 (43,'http://supernarede.local/departamentos/bebidas/cerveja','Mozilla/5.0 (X11; U; Linux x86_64; pt-BR; rv:1.9.2.22) Gecko/20110905 Ubuntu/10.04 (lucid) Firefox/3.6.22','ISO-8859-1,utf-8;q=0.7,*;q=0.7','pt-br,pt;q=0.8,en-us;q=0.5,en;q=0.3',2130706689,2130706689),
 (44,'http://supernarede.local/departamentos/bebidas/cerveja','Mozilla/5.0 (X11; U; Linux x86_64; pt-BR; rv:1.9.2.22) Gecko/20110905 Ubuntu/10.04 (lucid) Firefox/3.6.22','ISO-8859-1,utf-8;q=0.7,*;q=0.7','pt-br,pt;q=0.8,en-us;q=0.5,en;q=0.3',2130706689,2130706689),
 (45,'','Mozilla/5.0 (X11; U; Linux x86_64; pt-BR; rv:1.9.2.22) Gecko/20110905 Ubuntu/10.04 (lucid) Firefox/3.6.22','ISO-8859-1,utf-8;q=0.7,*;q=0.7','pt-br,pt;q=0.8,en-us;q=0.5,en;q=0.3',3232235577,3232235577),
 (46,'','Mozilla/5.0 (Windows NT 5.1) AppleWebKit/535.1 (KHTML, like Gecko) RockMelt/0.9.64.347 Chrome/13.0.782.218 Safari/535.1','ISO-8859-1,utf-8;q=0.7,*;q=0.3','en-US,en;q=0.8',3232235577,3232235640),
 (47,'','Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; .NET CLR 2.0.50727; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729)','','pt-br',3232235577,3232235640),
 (48,'','Mozilla/5.0 (Windows NT 5.1; rv:6.0.2) Gecko/20100101 Firefox/6.0.2','ISO-8859-1,utf-8;q=0.7,*;q=0.7','pt-br,pt;q=0.8,en-us;q=0.5,en;q=0.3',3232235577,3232235640),
 (49,'','Mozilla/5.0 (X11; U; Linux x86_64; pt-BR; rv:1.9.2.22) Gecko/20110905 Ubuntu/10.04 (lucid) Firefox/3.6.22','ISO-8859-1,utf-8;q=0.7,*;q=0.7','pt-br,pt;q=0.8,en-us;q=0.5,en;q=0.3',3232235577,3232235577),
 (50,'http://192.168.0.57/super_na_rede/customer/account/login/','Mozilla/5.0 (X11; U; Linux x86_64; pt-BR; rv:1.9.2.22) Gecko/20110905 Ubuntu/10.04 (lucid) Firefox/3.6.22','ISO-8859-1,utf-8;q=0.7,*;q=0.7','pt-br,pt;q=0.8,en-us;q=0.5,en;q=0.3',3232235577,3232235577),
 (51,'','Mozilla/5.0 (X11; U; Linux x86_64; pt-BR; rv:1.9.2.22) Gecko/20110905 Ubuntu/10.04 (lucid) Firefox/3.6.22','ISO-8859-1,utf-8;q=0.7,*;q=0.7','pt-br,pt;q=0.8,en-us;q=0.5,en;q=0.3',3232235577,3232235577),
 (52,'','Mozilla/5.0 (X11; U; Linux x86_64; pt-BR; rv:1.9.2.22) Gecko/20110905 Ubuntu/10.04 (lucid) Firefox/3.6.22','ISO-8859-1,utf-8;q=0.7,*;q=0.7','pt-br,pt;q=0.8,en-us;q=0.5,en;q=0.3',3232235577,3232235577),
 (53,'','Mozilla/5.0 (X11; U; Linux x86_64; pt-BR; rv:1.9.2.22) Gecko/20110905 Ubuntu/10.04 (lucid) Firefox/3.6.22','ISO-8859-1,utf-8;q=0.7,*;q=0.7','pt-br,pt;q=0.8,en-us;q=0.5,en;q=0.3',3232235577,3232235577),
 (54,'','Mozilla/5.0 (Windows NT 5.1; rv:6.0.2) Gecko/20100101 Firefox/6.0.2','ISO-8859-1,utf-8;q=0.7,*;q=0.7','pt-br,pt;q=0.8,en-us;q=0.5,en;q=0.3',3232235577,3232235640),
 (55,'','Mozilla/5.0 (Windows NT 5.1; rv:6.0.2) Gecko/20100101 Firefox/6.0.2','ISO-8859-1,utf-8;q=0.7,*;q=0.7','pt-br,pt;q=0.8,en-us;q=0.5,en;q=0.3',3232235577,3232235640),
 (56,'','Mozilla/5.0 (Windows NT 5.1) AppleWebKit/535.1 (KHTML, like Gecko) RockMelt/0.9.64.347 Chrome/13.0.782.218 Safari/535.1','ISO-8859-1,utf-8;q=0.7,*;q=0.3','en-US,en;q=0.8',3232235577,3232235640),
 (57,'http://192.168.0.57/super_na_rede/customer/address/edit/','Mozilla/5.0 (X11; U; Linux x86_64; pt-BR; rv:1.9.2.22) Gecko/20110905 Ubuntu/10.04 (lucid) Firefox/3.6.22','ISO-8859-1,utf-8;q=0.7,*;q=0.7','pt-br,pt;q=0.8,en-us;q=0.5,en;q=0.3',3232235577,3232235577),
 (58,'http://192.168.0.57/super_na_rede/customer/address/edit/','Mozilla/5.0 (X11; U; Linux x86_64; pt-BR; rv:1.9.2.22) Gecko/20110905 Ubuntu/10.04 (lucid) Firefox/3.6.22','ISO-8859-1,utf-8;q=0.7,*;q=0.7','pt-br,pt;q=0.8,en-us;q=0.5,en;q=0.3',3232235577,3232235577),
 (59,'http://192.168.0.57/super_na_rede/customer/account/login/referer/aHR0cDovLzE5Mi4xNjguMC41Ny9zdXBlcl9uYV9yZWRlL2N1c3RvbWVyL2FjY291bnQvaW5kZXgv/','Mozilla/5.0 (Windows NT 5.1; rv:6.0.2) Gecko/20100101 Firefox/6.0.2','ISO-8859-1,utf-8;q=0.7,*;q=0.7','pt-br,pt;q=0.8,en-us;q=0.5,en;q=0.3',3232235577,3232235640);
UNLOCK TABLES;
/*!40000 ALTER TABLE `log_visitor_info` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`log_visitor_online`
--

DROP TABLE IF EXISTS `supernarede`.`log_visitor_online`;
CREATE TABLE  `supernarede`.`log_visitor_online` (
  `visitor_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `visitor_type` char(1) NOT NULL,
  `remote_addr` bigint(20) NOT NULL,
  `first_visit_at` datetime DEFAULT NULL,
  `last_visit_at` datetime DEFAULT NULL,
  `customer_id` int(10) unsigned DEFAULT NULL,
  `last_url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`visitor_id`),
  KEY `IDX_VISITOR_TYPE` (`visitor_type`),
  KEY `IDX_VISIT_TIME` (`first_visit_at`,`last_visit_at`),
  KEY `IDX_CUSTOMER` (`customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`log_visitor_online`
--

/*!40000 ALTER TABLE `log_visitor_online` DISABLE KEYS */;
LOCK TABLES `log_visitor_online` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `log_visitor_online` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`newsletter_problem`
--

DROP TABLE IF EXISTS `supernarede`.`newsletter_problem`;
CREATE TABLE  `supernarede`.`newsletter_problem` (
  `problem_id` int(7) unsigned NOT NULL AUTO_INCREMENT,
  `subscriber_id` int(7) unsigned DEFAULT NULL,
  `queue_id` int(7) unsigned NOT NULL DEFAULT '0',
  `problem_error_code` int(3) unsigned DEFAULT '0',
  `problem_error_text` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`problem_id`),
  KEY `FK_PROBLEM_SUBSCRIBER` (`subscriber_id`),
  KEY `FK_PROBLEM_QUEUE` (`queue_id`),
  CONSTRAINT `FK_PROBLEM_QUEUE` FOREIGN KEY (`queue_id`) REFERENCES `newsletter_queue` (`queue_id`),
  CONSTRAINT `FK_PROBLEM_SUBSCRIBER` FOREIGN KEY (`subscriber_id`) REFERENCES `newsletter_subscriber` (`subscriber_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Newsletter problems';

--
-- Dumping data for table `supernarede`.`newsletter_problem`
--

/*!40000 ALTER TABLE `newsletter_problem` DISABLE KEYS */;
LOCK TABLES `newsletter_problem` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `newsletter_problem` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`newsletter_queue`
--

DROP TABLE IF EXISTS `supernarede`.`newsletter_queue`;
CREATE TABLE  `supernarede`.`newsletter_queue` (
  `queue_id` int(7) unsigned NOT NULL AUTO_INCREMENT,
  `template_id` int(7) unsigned NOT NULL DEFAULT '0',
  `newsletter_type` int(3) DEFAULT NULL,
  `newsletter_text` text,
  `newsletter_styles` text,
  `newsletter_subject` varchar(200) DEFAULT NULL,
  `newsletter_sender_name` varchar(200) DEFAULT NULL,
  `newsletter_sender_email` varchar(200) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `queue_status` int(3) unsigned NOT NULL DEFAULT '0',
  `queue_start_at` datetime DEFAULT NULL,
  `queue_finish_at` datetime DEFAULT NULL,
  PRIMARY KEY (`queue_id`),
  KEY `FK_QUEUE_TEMPLATE` (`template_id`),
  CONSTRAINT `FK_QUEUE_TEMPLATE` FOREIGN KEY (`template_id`) REFERENCES `newsletter_template` (`template_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Newsletter queue';

--
-- Dumping data for table `supernarede`.`newsletter_queue`
--

/*!40000 ALTER TABLE `newsletter_queue` DISABLE KEYS */;
LOCK TABLES `newsletter_queue` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `newsletter_queue` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`newsletter_queue_link`
--

DROP TABLE IF EXISTS `supernarede`.`newsletter_queue_link`;
CREATE TABLE  `supernarede`.`newsletter_queue_link` (
  `queue_link_id` int(9) unsigned NOT NULL AUTO_INCREMENT,
  `queue_id` int(7) unsigned NOT NULL DEFAULT '0',
  `subscriber_id` int(7) unsigned NOT NULL DEFAULT '0',
  `letter_sent_at` datetime DEFAULT NULL,
  PRIMARY KEY (`queue_link_id`),
  KEY `FK_QUEUE_LINK_SUBSCRIBER` (`subscriber_id`),
  KEY `FK_QUEUE_LINK_QUEUE` (`queue_id`),
  KEY `IDX_NEWSLETTER_QUEUE_LINK_SEND_AT` (`queue_id`,`letter_sent_at`),
  CONSTRAINT `FK_QUEUE_LINK_QUEUE` FOREIGN KEY (`queue_id`) REFERENCES `newsletter_queue` (`queue_id`) ON DELETE CASCADE,
  CONSTRAINT `FK_QUEUE_LINK_SUBSCRIBER` FOREIGN KEY (`subscriber_id`) REFERENCES `newsletter_subscriber` (`subscriber_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Newsletter queue to subscriber link';

--
-- Dumping data for table `supernarede`.`newsletter_queue_link`
--

/*!40000 ALTER TABLE `newsletter_queue_link` DISABLE KEYS */;
LOCK TABLES `newsletter_queue_link` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `newsletter_queue_link` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`newsletter_queue_store_link`
--

DROP TABLE IF EXISTS `supernarede`.`newsletter_queue_store_link`;
CREATE TABLE  `supernarede`.`newsletter_queue_store_link` (
  `queue_id` int(7) unsigned NOT NULL DEFAULT '0',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`queue_id`,`store_id`),
  KEY `FK_NEWSLETTER_QUEUE_STORE_LINK_STORE` (`store_id`),
  CONSTRAINT `FK_LINK_QUEUE` FOREIGN KEY (`queue_id`) REFERENCES `newsletter_queue` (`queue_id`) ON DELETE CASCADE,
  CONSTRAINT `FK_NEWSLETTER_QUEUE_STORE_LINK_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`newsletter_queue_store_link`
--

/*!40000 ALTER TABLE `newsletter_queue_store_link` DISABLE KEYS */;
LOCK TABLES `newsletter_queue_store_link` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `newsletter_queue_store_link` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`newsletter_subscriber`
--

DROP TABLE IF EXISTS `supernarede`.`newsletter_subscriber`;
CREATE TABLE  `supernarede`.`newsletter_subscriber` (
  `subscriber_id` int(7) unsigned NOT NULL AUTO_INCREMENT,
  `store_id` smallint(5) unsigned DEFAULT '0',
  `change_status_at` datetime DEFAULT NULL,
  `customer_id` int(11) unsigned NOT NULL DEFAULT '0',
  `subscriber_email` varchar(150) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `subscriber_status` int(3) NOT NULL DEFAULT '0',
  `subscriber_confirm_code` varchar(32) DEFAULT 'NULL',
  PRIMARY KEY (`subscriber_id`),
  KEY `FK_SUBSCRIBER_CUSTOMER` (`customer_id`),
  KEY `FK_NEWSLETTER_SUBSCRIBER_STORE` (`store_id`),
  CONSTRAINT `FK_NEWSLETTER_SUBSCRIBER_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='Newsletter subscribers';

--
-- Dumping data for table `supernarede`.`newsletter_subscriber`
--

/*!40000 ALTER TABLE `newsletter_subscriber` DISABLE KEYS */;
LOCK TABLES `newsletter_subscriber` WRITE;
INSERT INTO `supernarede`.`newsletter_subscriber` VALUES  (5,1,NULL,0,'teste@jn2.com.br',1,'92ifumue34y4bbprw9o60kkgwzcpr49x'),
 (6,1,NULL,1,'leandro@bhdesign.com.br',1,'14snx6pms9lg5pcsrk2b0m77hplts4d9');
UNLOCK TABLES;
/*!40000 ALTER TABLE `newsletter_subscriber` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`newsletter_template`
--

DROP TABLE IF EXISTS `supernarede`.`newsletter_template`;
CREATE TABLE  `supernarede`.`newsletter_template` (
  `template_id` int(7) unsigned NOT NULL AUTO_INCREMENT,
  `template_code` varchar(150) DEFAULT NULL,
  `template_text` text,
  `template_text_preprocessed` text COMMENT 'deprecated since 1.4.0.1',
  `template_styles` text,
  `template_type` int(3) unsigned DEFAULT NULL,
  `template_subject` varchar(200) DEFAULT NULL,
  `template_sender_name` varchar(200) DEFAULT NULL,
  `template_sender_email` varchar(200) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `template_actual` tinyint(1) unsigned DEFAULT '1',
  `added_at` datetime DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  PRIMARY KEY (`template_id`),
  KEY `template_actual` (`template_actual`),
  KEY `added_at` (`added_at`),
  KEY `modified_at` (`modified_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Newsletter templates';

--
-- Dumping data for table `supernarede`.`newsletter_template`
--

/*!40000 ALTER TABLE `newsletter_template` DISABLE KEYS */;
LOCK TABLES `newsletter_template` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `newsletter_template` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`paygate_authorizenet_debug`
--

DROP TABLE IF EXISTS `supernarede`.`paygate_authorizenet_debug`;
CREATE TABLE  `supernarede`.`paygate_authorizenet_debug` (
  `debug_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `request_body` text,
  `response_body` text,
  `request_serialized` text,
  `result_serialized` text,
  `request_dump` text,
  `result_dump` text,
  PRIMARY KEY (`debug_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`paygate_authorizenet_debug`
--

/*!40000 ALTER TABLE `paygate_authorizenet_debug` DISABLE KEYS */;
LOCK TABLES `paygate_authorizenet_debug` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `paygate_authorizenet_debug` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`paypal_api_debug`
--

DROP TABLE IF EXISTS `supernarede`.`paypal_api_debug`;
CREATE TABLE  `supernarede`.`paypal_api_debug` (
  `debug_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `debug_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `request_body` text,
  `response_body` text,
  PRIMARY KEY (`debug_id`),
  KEY `debug_at` (`debug_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`paypal_api_debug`
--

/*!40000 ALTER TABLE `paypal_api_debug` DISABLE KEYS */;
LOCK TABLES `paypal_api_debug` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `paypal_api_debug` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`paypal_cert`
--

DROP TABLE IF EXISTS `supernarede`.`paypal_cert`;
CREATE TABLE  `supernarede`.`paypal_cert` (
  `cert_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `website_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `content` mediumblob NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`cert_id`),
  KEY `IDX_PAYPAL_CERT_WEBSITE` (`website_id`),
  CONSTRAINT `FK_PAYPAL_CERT_WEBSITE` FOREIGN KEY (`website_id`) REFERENCES `core_website` (`website_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`paypal_cert`
--

/*!40000 ALTER TABLE `paypal_cert` DISABLE KEYS */;
LOCK TABLES `paypal_cert` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `paypal_cert` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`paypal_settlement_report`
--

DROP TABLE IF EXISTS `supernarede`.`paypal_settlement_report`;
CREATE TABLE  `supernarede`.`paypal_settlement_report` (
  `report_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `report_date` date NOT NULL,
  `account_id` varchar(64) NOT NULL,
  `filename` varchar(24) NOT NULL,
  `last_modified` datetime NOT NULL,
  PRIMARY KEY (`report_id`),
  UNIQUE KEY `UNQ_REPORT_DATE_ACCOUNT` (`report_date`,`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`paypal_settlement_report`
--

/*!40000 ALTER TABLE `paypal_settlement_report` DISABLE KEYS */;
LOCK TABLES `paypal_settlement_report` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `paypal_settlement_report` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`paypal_settlement_report_row`
--

DROP TABLE IF EXISTS `supernarede`.`paypal_settlement_report_row`;
CREATE TABLE  `supernarede`.`paypal_settlement_report_row` (
  `row_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `report_id` int(10) unsigned NOT NULL,
  `transaction_id` varchar(19) NOT NULL,
  `invoice_id` varchar(127) DEFAULT NULL,
  `paypal_reference_id` varchar(19) NOT NULL,
  `paypal_reference_id_type` enum('ODR','TXN','SUB','PAP','') NOT NULL,
  `transaction_event_code` char(5) NOT NULL DEFAULT '',
  `transaction_initiation_date` datetime DEFAULT NULL,
  `transaction_completion_date` datetime DEFAULT NULL,
  `transaction_debit_or_credit` enum('CR','DR') NOT NULL DEFAULT 'CR',
  `gross_transaction_amount` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `gross_transaction_currency` char(3) NOT NULL DEFAULT '',
  `fee_debit_or_credit` enum('CR','DR') NOT NULL,
  `fee_amount` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `fee_currency` char(3) NOT NULL,
  `custom_field` varchar(255) DEFAULT NULL,
  `consumer_id` varchar(127) NOT NULL DEFAULT '',
  PRIMARY KEY (`row_id`),
  KEY `IDX_REPORT_ID` (`report_id`),
  CONSTRAINT `FK_PAYPAL_SETTLEMENT_ROW_REPORT` FOREIGN KEY (`report_id`) REFERENCES `paypal_settlement_report` (`report_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`paypal_settlement_report_row`
--

/*!40000 ALTER TABLE `paypal_settlement_report_row` DISABLE KEYS */;
LOCK TABLES `paypal_settlement_report_row` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `paypal_settlement_report_row` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`paypaluk_api_debug`
--

DROP TABLE IF EXISTS `supernarede`.`paypaluk_api_debug`;
CREATE TABLE  `supernarede`.`paypaluk_api_debug` (
  `debug_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `debug_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `request_body` text,
  `response_body` text,
  PRIMARY KEY (`debug_id`),
  KEY `debug_at` (`debug_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`paypaluk_api_debug`
--

/*!40000 ALTER TABLE `paypaluk_api_debug` DISABLE KEYS */;
LOCK TABLES `paypaluk_api_debug` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `paypaluk_api_debug` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`poll`
--

DROP TABLE IF EXISTS `supernarede`.`poll`;
CREATE TABLE  `supernarede`.`poll` (
  `poll_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `poll_title` varchar(255) NOT NULL DEFAULT '',
  `votes_count` int(10) unsigned NOT NULL DEFAULT '0',
  `store_id` smallint(5) unsigned DEFAULT '0',
  `date_posted` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_closed` datetime DEFAULT NULL,
  `active` smallint(6) NOT NULL DEFAULT '1',
  `closed` tinyint(1) NOT NULL DEFAULT '0',
  `answers_display` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`poll_id`),
  KEY `FK_POLL_STORE` (`store_id`),
  CONSTRAINT `FK_POLL_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`poll`
--

/*!40000 ALTER TABLE `poll` DISABLE KEYS */;
LOCK TABLES `poll` WRITE;
INSERT INTO `supernarede`.`poll` VALUES  (1,'Qual sua cor favorita',5,1,'2011-08-08 09:56:57',NULL,1,0,NULL);
UNLOCK TABLES;
/*!40000 ALTER TABLE `poll` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`poll_answer`
--

DROP TABLE IF EXISTS `supernarede`.`poll_answer`;
CREATE TABLE  `supernarede`.`poll_answer` (
  `answer_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `poll_id` int(10) unsigned NOT NULL DEFAULT '0',
  `answer_title` varchar(255) NOT NULL DEFAULT '',
  `votes_count` int(10) unsigned NOT NULL DEFAULT '0',
  `answer_order` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`answer_id`),
  KEY `FK_POLL_PARENT` (`poll_id`),
  CONSTRAINT `FK_POLL_PARENT` FOREIGN KEY (`poll_id`) REFERENCES `poll` (`poll_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`poll_answer`
--

/*!40000 ALTER TABLE `poll_answer` DISABLE KEYS */;
LOCK TABLES `poll_answer` WRITE;
INSERT INTO `supernarede`.`poll_answer` VALUES  (1,1,'Verde',4,0),
 (2,1,'Vermelho',1,0),
 (3,1,'Preto',0,0),
 (4,1,'Magenta',0,0);
UNLOCK TABLES;
/*!40000 ALTER TABLE `poll_answer` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`poll_store`
--

DROP TABLE IF EXISTS `supernarede`.`poll_store`;
CREATE TABLE  `supernarede`.`poll_store` (
  `poll_id` int(10) unsigned NOT NULL DEFAULT '0',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`poll_id`,`store_id`),
  KEY `FK_POLL_STORE_STORE` (`store_id`),
  CONSTRAINT `FK_POLL_STORE_POLL` FOREIGN KEY (`poll_id`) REFERENCES `poll` (`poll_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_POLL_STORE_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`poll_store`
--

/*!40000 ALTER TABLE `poll_store` DISABLE KEYS */;
LOCK TABLES `poll_store` WRITE;
INSERT INTO `supernarede`.`poll_store` VALUES  (1,1);
UNLOCK TABLES;
/*!40000 ALTER TABLE `poll_store` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`poll_vote`
--

DROP TABLE IF EXISTS `supernarede`.`poll_vote`;
CREATE TABLE  `supernarede`.`poll_vote` (
  `vote_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `poll_id` int(10) unsigned NOT NULL DEFAULT '0',
  `poll_answer_id` int(10) unsigned NOT NULL DEFAULT '0',
  `ip_address` bigint(20) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `vote_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`vote_id`),
  KEY `FK_POLL_ANSWER` (`poll_answer_id`),
  CONSTRAINT `FK_POLL_ANSWER` FOREIGN KEY (`poll_answer_id`) REFERENCES `poll_answer` (`answer_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`poll_vote`
--

/*!40000 ALTER TABLE `poll_vote` DISABLE KEYS */;
LOCK TABLES `poll_vote` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `poll_vote` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`product_alert_price`
--

DROP TABLE IF EXISTS `supernarede`.`product_alert_price`;
CREATE TABLE  `supernarede`.`product_alert_price` (
  `alert_price_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(10) unsigned NOT NULL DEFAULT '0',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0',
  `price` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `website_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `add_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_send_date` datetime DEFAULT NULL,
  `send_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`alert_price_id`),
  KEY `FK_PRODUCT_ALERT_PRICE_CUSTOMER` (`customer_id`),
  KEY `FK_PRODUCT_ALERT_PRICE_PRODUCT` (`product_id`),
  KEY `FK_PRODUCT_ALERT_PRICE_WEBSITE` (`website_id`),
  CONSTRAINT `FK_PRODUCT_ALERT_PRICE_CUSTOMER` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_PRODUCT_ALERT_PRICE_PRODUCT` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_PRODUCT_ALERT_PRICE_WEBSITE` FOREIGN KEY (`website_id`) REFERENCES `core_website` (`website_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`product_alert_price`
--

/*!40000 ALTER TABLE `product_alert_price` DISABLE KEYS */;
LOCK TABLES `product_alert_price` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `product_alert_price` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`product_alert_stock`
--

DROP TABLE IF EXISTS `supernarede`.`product_alert_stock`;
CREATE TABLE  `supernarede`.`product_alert_stock` (
  `alert_stock_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(10) unsigned NOT NULL DEFAULT '0',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0',
  `website_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `add_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `send_date` datetime DEFAULT NULL,
  `send_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`alert_stock_id`),
  KEY `FK_PRODUCT_ALERT_STOCK_CUSTOMER` (`customer_id`),
  KEY `FK_PRODUCT_ALERT_STOCK_PRODUCT` (`product_id`),
  KEY `FK_PRODUCT_ALERT_STOCK_WEBSITE` (`website_id`),
  CONSTRAINT `FK_PRODUCT_ALERT_STOCK_CUSTOMER` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_PRODUCT_ALERT_STOCK_PRODUCT` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_PRODUCT_ALERT_STOCK_WEBSITE` FOREIGN KEY (`website_id`) REFERENCES `core_website` (`website_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`product_alert_stock`
--

/*!40000 ALTER TABLE `product_alert_stock` DISABLE KEYS */;
LOCK TABLES `product_alert_stock` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `product_alert_stock` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`rating`
--

DROP TABLE IF EXISTS `supernarede`.`rating`;
CREATE TABLE  `supernarede`.`rating` (
  `rating_id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `entity_id` smallint(6) unsigned NOT NULL DEFAULT '0',
  `rating_code` varchar(64) NOT NULL DEFAULT '',
  `position` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`rating_id`),
  UNIQUE KEY `IDX_CODE` (`rating_code`),
  KEY `FK_RATING_ENTITY` (`entity_id`),
  CONSTRAINT `FK_RATING_ENTITY_KEY` FOREIGN KEY (`entity_id`) REFERENCES `rating_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='ratings';

--
-- Dumping data for table `supernarede`.`rating`
--

/*!40000 ALTER TABLE `rating` DISABLE KEYS */;
LOCK TABLES `rating` WRITE;
INSERT INTO `supernarede`.`rating` VALUES  (1,1,'Qualidade',0),
 (2,1,'Pontuação',0),
 (3,1,'Preço',0);
UNLOCK TABLES;
/*!40000 ALTER TABLE `rating` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`rating_entity`
--

DROP TABLE IF EXISTS `supernarede`.`rating_entity`;
CREATE TABLE  `supernarede`.`rating_entity` (
  `entity_id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `entity_code` varchar(64) NOT NULL DEFAULT '',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `IDX_CODE` (`entity_code`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Rating entities';

--
-- Dumping data for table `supernarede`.`rating_entity`
--

/*!40000 ALTER TABLE `rating_entity` DISABLE KEYS */;
LOCK TABLES `rating_entity` WRITE;
INSERT INTO `supernarede`.`rating_entity` VALUES  (1,'product'),
 (2,'product_review'),
 (3,'review');
UNLOCK TABLES;
/*!40000 ALTER TABLE `rating_entity` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`rating_option`
--

DROP TABLE IF EXISTS `supernarede`.`rating_option`;
CREATE TABLE  `supernarede`.`rating_option` (
  `option_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rating_id` smallint(6) unsigned NOT NULL DEFAULT '0',
  `code` varchar(32) NOT NULL DEFAULT '',
  `value` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `position` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`option_id`),
  KEY `FK_RATING_OPTION_RATING` (`rating_id`),
  CONSTRAINT `FK_RATING_OPTION_RATING` FOREIGN KEY (`rating_id`) REFERENCES `rating` (`rating_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COMMENT='Rating options';

--
-- Dumping data for table `supernarede`.`rating_option`
--

/*!40000 ALTER TABLE `rating_option` DISABLE KEYS */;
LOCK TABLES `rating_option` WRITE;
INSERT INTO `supernarede`.`rating_option` VALUES  (1,1,'1',1,1),
 (2,1,'2',2,2),
 (3,1,'3',3,3),
 (4,1,'4',4,4),
 (5,1,'5',5,5),
 (6,2,'1',1,1),
 (7,2,'2',2,2),
 (8,2,'3',3,3),
 (9,2,'4',4,4),
 (10,2,'5',5,5),
 (11,3,'1',1,1),
 (12,3,'2',2,2),
 (13,3,'3',3,3),
 (14,3,'4',4,4),
 (15,3,'5',5,5);
UNLOCK TABLES;
/*!40000 ALTER TABLE `rating_option` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`rating_option_vote`
--

DROP TABLE IF EXISTS `supernarede`.`rating_option_vote`;
CREATE TABLE  `supernarede`.`rating_option_vote` (
  `vote_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `option_id` int(10) unsigned NOT NULL DEFAULT '0',
  `remote_ip` varchar(16) NOT NULL DEFAULT '',
  `remote_ip_long` int(11) NOT NULL DEFAULT '0',
  `customer_id` int(11) unsigned DEFAULT '0',
  `entity_pk_value` bigint(20) unsigned NOT NULL DEFAULT '0',
  `rating_id` smallint(6) unsigned NOT NULL DEFAULT '0',
  `review_id` bigint(20) unsigned DEFAULT NULL,
  `percent` tinyint(3) NOT NULL DEFAULT '0',
  `value` tinyint(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`vote_id`),
  KEY `FK_RATING_OPTION_VALUE_OPTION` (`option_id`),
  KEY `FK_RATING_OPTION_REVIEW_ID` (`review_id`),
  CONSTRAINT `FK_RATING_OPTION_REVIEW_ID` FOREIGN KEY (`review_id`) REFERENCES `review` (`review_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_RATING_OPTION_VALUE_OPTION` FOREIGN KEY (`option_id`) REFERENCES `rating_option` (`option_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Rating option values';

--
-- Dumping data for table `supernarede`.`rating_option_vote`
--

/*!40000 ALTER TABLE `rating_option_vote` DISABLE KEYS */;
LOCK TABLES `rating_option_vote` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `rating_option_vote` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`rating_option_vote_aggregated`
--

DROP TABLE IF EXISTS `supernarede`.`rating_option_vote_aggregated`;
CREATE TABLE  `supernarede`.`rating_option_vote_aggregated` (
  `primary_id` int(11) NOT NULL AUTO_INCREMENT,
  `rating_id` smallint(6) unsigned NOT NULL DEFAULT '0',
  `entity_pk_value` bigint(20) unsigned NOT NULL DEFAULT '0',
  `vote_count` int(10) unsigned NOT NULL DEFAULT '0',
  `vote_value_sum` int(10) unsigned NOT NULL DEFAULT '0',
  `percent` tinyint(3) NOT NULL DEFAULT '0',
  `percent_approved` tinyint(3) DEFAULT '0',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`primary_id`),
  KEY `FK_RATING_OPTION_VALUE_AGGREGATE` (`rating_id`),
  KEY `FK_RATING_OPTION_VOTE_AGGREGATED_STORE` (`store_id`),
  CONSTRAINT `FK_RATING_OPTION_VALUE_AGGREGATE` FOREIGN KEY (`rating_id`) REFERENCES `rating` (`rating_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_RATING_OPTION_VOTE_AGGREGATED_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`rating_option_vote_aggregated`
--

/*!40000 ALTER TABLE `rating_option_vote_aggregated` DISABLE KEYS */;
LOCK TABLES `rating_option_vote_aggregated` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `rating_option_vote_aggregated` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`rating_store`
--

DROP TABLE IF EXISTS `supernarede`.`rating_store`;
CREATE TABLE  `supernarede`.`rating_store` (
  `rating_id` smallint(6) unsigned NOT NULL DEFAULT '0',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`rating_id`,`store_id`),
  KEY `FK_RATING_STORE_STORE` (`store_id`),
  CONSTRAINT `FK_RATING_STORE_RATING` FOREIGN KEY (`rating_id`) REFERENCES `rating` (`rating_id`) ON DELETE CASCADE,
  CONSTRAINT `FK_RATING_STORE_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`rating_store`
--

/*!40000 ALTER TABLE `rating_store` DISABLE KEYS */;
LOCK TABLES `rating_store` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `rating_store` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`rating_title`
--

DROP TABLE IF EXISTS `supernarede`.`rating_title`;
CREATE TABLE  `supernarede`.`rating_title` (
  `rating_id` smallint(6) unsigned NOT NULL DEFAULT '0',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `value` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`rating_id`,`store_id`),
  KEY `FK_RATING_TITLE_STORE` (`store_id`),
  CONSTRAINT `FK_RATING_TITLE` FOREIGN KEY (`rating_id`) REFERENCES `rating` (`rating_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_RATING_TITLE_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`rating_title`
--

/*!40000 ALTER TABLE `rating_title` DISABLE KEYS */;
LOCK TABLES `rating_title` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `rating_title` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`report_compared_product_index`
--

DROP TABLE IF EXISTS `supernarede`.`report_compared_product_index`;
CREATE TABLE  `supernarede`.`report_compared_product_index` (
  `index_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `visitor_id` int(10) unsigned DEFAULT NULL,
  `customer_id` int(10) unsigned DEFAULT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `store_id` smallint(5) unsigned DEFAULT NULL,
  `added_at` datetime NOT NULL,
  PRIMARY KEY (`index_id`),
  UNIQUE KEY `UNQ_BY_VISITOR` (`visitor_id`,`product_id`),
  UNIQUE KEY `UNQ_BY_CUSTOMER` (`customer_id`,`product_id`),
  KEY `IDX_STORE` (`store_id`),
  KEY `IDX_SORT_ADDED_AT` (`added_at`),
  KEY `product_id` (`product_id`),
  CONSTRAINT `FK_REPORT_COMPARED_PRODUCT_INDEX_CUSTOMER` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_REPORT_COMPARED_PRODUCT_INDEX_PRODUCT` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_REPORT_COMPARED_PRODUCT_INDEX_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`report_compared_product_index`
--

/*!40000 ALTER TABLE `report_compared_product_index` DISABLE KEYS */;
LOCK TABLES `report_compared_product_index` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `report_compared_product_index` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`report_event`
--

DROP TABLE IF EXISTS `supernarede`.`report_event`;
CREATE TABLE  `supernarede`.`report_event` (
  `event_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `logged_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `event_type_id` smallint(6) unsigned NOT NULL DEFAULT '0',
  `object_id` int(10) unsigned NOT NULL DEFAULT '0',
  `subject_id` int(10) unsigned NOT NULL DEFAULT '0',
  `subtype` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `store_id` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`event_id`),
  KEY `IDX_EVENT_TYPE` (`event_type_id`),
  KEY `IDX_SUBJECT` (`subject_id`),
  KEY `IDX_OBJECT` (`object_id`),
  KEY `IDX_SUBTYPE` (`subtype`),
  KEY `FK_REPORT_EVENT_STORE` (`store_id`),
  CONSTRAINT `FK_REPORT_EVENT_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_REPORT_EVENT_TYPE` FOREIGN KEY (`event_type_id`) REFERENCES `report_event_types` (`event_type_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`report_event`
--

/*!40000 ALTER TABLE `report_event` DISABLE KEYS */;
LOCK TABLES `report_event` WRITE;
INSERT INTO `supernarede`.`report_event` VALUES  (1,'2011-08-17 14:32:19',4,5,22,1,1),
 (2,'2011-08-17 14:34:54',4,5,22,1,1),
 (3,'2011-08-17 14:54:11',1,5,22,1,1),
 (4,'2011-08-17 15:04:52',4,3,22,1,1),
 (5,'2011-08-17 15:05:29',4,3,22,1,1),
 (6,'2011-08-17 15:24:54',4,3,22,1,1),
 (7,'2011-08-17 19:31:13',1,5,23,1,1),
 (8,'2011-08-17 19:37:50',1,5,23,1,1),
 (9,'2011-08-17 19:52:35',1,5,23,1,1),
 (10,'2011-08-17 19:53:21',1,5,23,1,1),
 (11,'2011-08-17 20:04:42',1,5,23,1,1),
 (12,'2011-08-17 20:04:53',1,5,23,1,1),
 (13,'2011-08-17 20:07:30',1,5,23,1,1),
 (14,'2011-08-17 20:08:05',1,5,23,1,1),
 (15,'2011-08-17 20:24:08',1,5,23,1,1),
 (16,'2011-08-17 20:25:55',1,1,23,1,1),
 (17,'2011-08-17 20:32:08',1,1,23,1,1),
 (18,'2011-08-17 20:37:52',1,1,23,1,1),
 (19,'2011-08-17 20:38:44',1,5,23,1,1),
 (20,'2011-08-17 20:39:28',1,5,23,1,1),
 (21,'2011-08-17 20:39:55',1,5,23,1,1),
 (22,'2011-08-17 20:40:56',1,5,23,1,1),
 (23,'2011-08-17 20:44:09',1,5,23,1,1),
 (24,'2011-08-17 20:45:14',1,5,24,1,1),
 (25,'2011-08-17 20:52:18',1,5,25,1,1),
 (26,'2011-08-17 21:02:32',1,5,26,1,1),
 (27,'2011-08-17 21:14:19',1,5,23,1,1),
 (28,'2011-08-17 21:19:24',1,5,23,1,1),
 (29,'2011-08-18 12:28:50',1,5,27,1,1),
 (30,'2011-08-18 19:55:08',1,4,29,1,1),
 (31,'2011-08-18 19:56:54',1,4,29,1,1),
 (32,'2011-08-18 20:10:11',1,4,29,1,1),
 (33,'2011-08-18 20:10:27',1,3,29,1,1),
 (34,'2011-08-18 20:11:49',1,3,29,1,1),
 (35,'2011-08-18 20:45:56',4,2,29,1,1),
 (36,'2011-08-18 21:28:13',4,3,29,1,1),
 (37,'2011-08-19 12:27:54',4,5,30,1,1),
 (38,'2011-08-19 13:25:10',1,5,30,1,1),
 (39,'2011-08-19 13:29:44',4,2,30,1,1),
 (40,'2011-08-19 16:01:18',4,3,31,1,1),
 (41,'2011-08-19 16:15:02',4,5,31,1,1),
 (42,'2011-08-19 16:15:51',4,2,31,1,1),
 (43,'2011-08-19 16:23:32',4,4,31,1,1),
 (44,'2011-08-19 16:27:16',1,2,31,1,1),
 (45,'2011-08-19 16:27:26',1,4,31,1,1),
 (46,'2011-08-19 16:27:48',1,4,31,1,1),
 (47,'2011-08-19 18:06:36',4,5,32,1,1),
 (48,'2011-08-22 15:49:22',1,6,35,1,1),
 (49,'2011-08-24 12:41:47',4,1,40,1,1),
 (50,'2011-09-15 20:19:05',1,1,42,1,1),
 (51,'2011-09-15 20:23:12',1,5,42,1,1),
 (52,'2011-09-15 20:23:45',1,5,42,1,1),
 (53,'2011-09-15 20:25:16',1,5,42,1,1),
 (54,'2011-09-15 20:34:38',1,5,42,1,1),
 (55,'2011-09-15 20:34:47',1,5,42,1,1),
 (56,'2011-09-15 20:35:53',1,5,42,1,1),
 (57,'2011-09-15 20:36:02',1,5,42,1,1),
 (58,'2011-09-16 12:32:34',1,6,44,1,1),
 (59,'2011-09-16 12:42:35',1,6,44,1,1),
 (60,'2011-09-16 12:49:18',1,6,44,1,1),
 (61,'2011-09-16 12:52:57',1,6,44,1,1),
 (62,'2011-09-16 13:04:01',1,6,44,1,1),
 (63,'2011-09-16 13:04:24',1,6,44,1,1),
 (64,'2011-09-16 13:30:04',1,6,44,1,1),
 (65,'2011-09-19 17:58:33',1,1,45,1,1),
 (66,'2011-09-19 18:01:41',1,1,45,1,1),
 (67,'2011-09-19 18:04:59',1,6,46,1,1),
 (68,'2011-09-19 18:05:32',1,6,47,1,1);
UNLOCK TABLES;
/*!40000 ALTER TABLE `report_event` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`report_event_types`
--

DROP TABLE IF EXISTS `supernarede`.`report_event_types`;
CREATE TABLE  `supernarede`.`report_event_types` (
  `event_type_id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `event_name` varchar(64) NOT NULL,
  `customer_login` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`event_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`report_event_types`
--

/*!40000 ALTER TABLE `report_event_types` DISABLE KEYS */;
LOCK TABLES `report_event_types` WRITE;
INSERT INTO `supernarede`.`report_event_types` VALUES  (1,'catalog_product_view',1),
 (2,'sendfriend_product',1),
 (3,'catalog_product_compare_add_product',1),
 (4,'checkout_cart_add_product',1),
 (5,'wishlist_add_product',1),
 (6,'wishlist_share',1);
UNLOCK TABLES;
/*!40000 ALTER TABLE `report_event_types` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`report_viewed_product_index`
--

DROP TABLE IF EXISTS `supernarede`.`report_viewed_product_index`;
CREATE TABLE  `supernarede`.`report_viewed_product_index` (
  `index_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `visitor_id` int(10) unsigned DEFAULT NULL,
  `customer_id` int(10) unsigned DEFAULT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `store_id` smallint(5) unsigned DEFAULT NULL,
  `added_at` datetime NOT NULL,
  PRIMARY KEY (`index_id`),
  UNIQUE KEY `UNQ_BY_VISITOR` (`visitor_id`,`product_id`),
  UNIQUE KEY `UNQ_BY_CUSTOMER` (`customer_id`,`product_id`),
  KEY `IDX_STORE` (`store_id`),
  KEY `IDX_SORT_ADDED_AT` (`added_at`),
  KEY `product_id` (`product_id`),
  CONSTRAINT `FK_REPORT_VIEWED_PRODUCT_INDEX_CUSTOMER` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_REPORT_VIEWED_PRODUCT_INDEX_PRODUCT` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_REPORT_VIEWED_PRODUCT_INDEX_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`report_viewed_product_index`
--

/*!40000 ALTER TABLE `report_viewed_product_index` DISABLE KEYS */;
LOCK TABLES `report_viewed_product_index` WRITE;
INSERT INTO `supernarede`.`report_viewed_product_index` VALUES  (1,22,NULL,5,1,'2011-08-17 14:54:11'),
 (2,23,NULL,5,1,'2011-08-17 21:19:24'),
 (11,23,NULL,1,1,'2011-08-17 20:37:52'),
 (19,24,NULL,5,1,'2011-08-17 20:45:13'),
 (20,25,NULL,5,1,'2011-08-17 20:52:17'),
 (21,26,NULL,5,1,'2011-08-17 21:02:31'),
 (22,27,NULL,5,1,'2011-08-18 12:28:50'),
 (23,29,NULL,4,1,'2011-08-18 20:10:11'),
 (26,29,NULL,3,1,'2011-08-18 20:11:48'),
 (27,30,NULL,5,1,'2011-08-19 13:25:10'),
 (28,31,NULL,2,1,'2011-08-19 16:27:15'),
 (29,31,NULL,4,1,'2011-08-19 16:27:48'),
 (30,35,NULL,6,1,'2011-08-22 15:49:21'),
 (31,42,NULL,1,1,'2011-09-15 20:19:04'),
 (32,42,NULL,5,1,'2011-09-15 20:36:02'),
 (33,44,NULL,6,1,'2011-09-16 13:30:04'),
 (34,45,NULL,1,1,'2011-09-19 18:01:40'),
 (36,46,NULL,6,1,'2011-09-19 18:04:59'),
 (37,47,NULL,6,1,'2011-09-19 18:05:32');
UNLOCK TABLES;
/*!40000 ALTER TABLE `report_viewed_product_index` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`review`
--

DROP TABLE IF EXISTS `supernarede`.`review`;
CREATE TABLE  `supernarede`.`review` (
  `review_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `entity_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `entity_pk_value` int(10) unsigned NOT NULL DEFAULT '0',
  `status_id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`review_id`),
  KEY `FK_REVIEW_ENTITY` (`entity_id`),
  KEY `FK_REVIEW_STATUS` (`status_id`),
  KEY `FK_REVIEW_PARENT_PRODUCT` (`entity_pk_value`),
  CONSTRAINT `FK_REVIEW_ENTITY` FOREIGN KEY (`entity_id`) REFERENCES `review_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_REVIEW_STATUS` FOREIGN KEY (`status_id`) REFERENCES `review_status` (`status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Review base information';

--
-- Dumping data for table `supernarede`.`review`
--

/*!40000 ALTER TABLE `review` DISABLE KEYS */;
LOCK TABLES `review` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `review` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`review_detail`
--

DROP TABLE IF EXISTS `supernarede`.`review_detail`;
CREATE TABLE  `supernarede`.`review_detail` (
  `detail_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `review_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `store_id` smallint(5) unsigned DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `detail` text NOT NULL,
  `nickname` varchar(128) NOT NULL DEFAULT '',
  `customer_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`detail_id`),
  KEY `FK_REVIEW_DETAIL_REVIEW` (`review_id`),
  KEY `FK_REVIEW_DETAIL_STORE` (`store_id`),
  KEY `FK_REVIEW_DETAIL_CUSTOMER` (`customer_id`),
  CONSTRAINT `FK_REVIEW_DETAIL_CUSTOMER` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_REVIEW_DETAIL_REVIEW` FOREIGN KEY (`review_id`) REFERENCES `review` (`review_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_REVIEW_DETAIL_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Review detail information';

--
-- Dumping data for table `supernarede`.`review_detail`
--

/*!40000 ALTER TABLE `review_detail` DISABLE KEYS */;
LOCK TABLES `review_detail` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `review_detail` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`review_entity`
--

DROP TABLE IF EXISTS `supernarede`.`review_entity`;
CREATE TABLE  `supernarede`.`review_entity` (
  `entity_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `entity_code` varchar(32) NOT NULL DEFAULT '',
  PRIMARY KEY (`entity_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Review entities';

--
-- Dumping data for table `supernarede`.`review_entity`
--

/*!40000 ALTER TABLE `review_entity` DISABLE KEYS */;
LOCK TABLES `review_entity` WRITE;
INSERT INTO `supernarede`.`review_entity` VALUES  (1,'product'),
 (2,'customer'),
 (3,'category');
UNLOCK TABLES;
/*!40000 ALTER TABLE `review_entity` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`review_entity_summary`
--

DROP TABLE IF EXISTS `supernarede`.`review_entity_summary`;
CREATE TABLE  `supernarede`.`review_entity_summary` (
  `primary_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `entity_pk_value` bigint(20) NOT NULL DEFAULT '0',
  `entity_type` tinyint(4) NOT NULL DEFAULT '0',
  `reviews_count` smallint(6) NOT NULL DEFAULT '0',
  `rating_summary` tinyint(4) NOT NULL DEFAULT '0',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`primary_id`),
  KEY `FK_REVIEW_ENTITY_SUMMARY_STORE` (`store_id`),
  CONSTRAINT `FK_REVIEW_ENTITY_SUMMARY_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`review_entity_summary`
--

/*!40000 ALTER TABLE `review_entity_summary` DISABLE KEYS */;
LOCK TABLES `review_entity_summary` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `review_entity_summary` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`review_status`
--

DROP TABLE IF EXISTS `supernarede`.`review_status`;
CREATE TABLE  `supernarede`.`review_status` (
  `status_id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `status_code` varchar(32) NOT NULL DEFAULT '',
  PRIMARY KEY (`status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Review statuses';

--
-- Dumping data for table `supernarede`.`review_status`
--

/*!40000 ALTER TABLE `review_status` DISABLE KEYS */;
LOCK TABLES `review_status` WRITE;
INSERT INTO `supernarede`.`review_status` VALUES  (1,'Approved'),
 (2,'Pending'),
 (3,'Not Approved');
UNLOCK TABLES;
/*!40000 ALTER TABLE `review_status` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`review_store`
--

DROP TABLE IF EXISTS `supernarede`.`review_store`;
CREATE TABLE  `supernarede`.`review_store` (
  `review_id` bigint(20) unsigned NOT NULL,
  `store_id` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`review_id`,`store_id`),
  KEY `FK_REVIEW_STORE_STORE` (`store_id`),
  CONSTRAINT `FK_REVIEW_STORE_REVIEW` FOREIGN KEY (`review_id`) REFERENCES `review` (`review_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_REVIEW_STORE_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`review_store`
--

/*!40000 ALTER TABLE `review_store` DISABLE KEYS */;
LOCK TABLES `review_store` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `review_store` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`sales_bestsellers_aggregated_daily`
--

DROP TABLE IF EXISTS `supernarede`.`sales_bestsellers_aggregated_daily`;
CREATE TABLE  `supernarede`.`sales_bestsellers_aggregated_daily` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `period` date NOT NULL DEFAULT '0000-00-00',
  `store_id` smallint(5) unsigned DEFAULT NULL,
  `product_id` int(10) unsigned DEFAULT NULL,
  `product_name` varchar(255) NOT NULL DEFAULT '',
  `product_price` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `qty_ordered` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `rating_pos` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNQ_PERIOD_STORE_PRODUCT` (`period`,`store_id`,`product_id`),
  KEY `IDX_STORE_ID` (`store_id`),
  KEY `IDX_PRODUCT_ID` (`product_id`),
  CONSTRAINT `FK_PRODUCT_ORDERED_AGGREGATED_DAILY_PRODUCT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_PRODUCT_ORDERED_AGGREGATED_DAILY_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`sales_bestsellers_aggregated_daily`
--

/*!40000 ALTER TABLE `sales_bestsellers_aggregated_daily` DISABLE KEYS */;
LOCK TABLES `sales_bestsellers_aggregated_daily` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `sales_bestsellers_aggregated_daily` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`sales_bestsellers_aggregated_monthly`
--

DROP TABLE IF EXISTS `supernarede`.`sales_bestsellers_aggregated_monthly`;
CREATE TABLE  `supernarede`.`sales_bestsellers_aggregated_monthly` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `period` date NOT NULL DEFAULT '0000-00-00',
  `store_id` smallint(5) unsigned DEFAULT NULL,
  `product_id` int(10) unsigned DEFAULT NULL,
  `product_name` varchar(255) NOT NULL DEFAULT '',
  `product_price` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `qty_ordered` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `rating_pos` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNQ_PERIOD_STORE_PRODUCT` (`period`,`store_id`,`product_id`),
  KEY `IDX_STORE_ID` (`store_id`),
  KEY `IDX_PRODUCT_ID` (`product_id`),
  CONSTRAINT `FK_PRODUCT_ORDERED_AGGREGATED_MONTHLY_PRODUCT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_PRODUCT_ORDERED_AGGREGATED_MONTHLY_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`sales_bestsellers_aggregated_monthly`
--

/*!40000 ALTER TABLE `sales_bestsellers_aggregated_monthly` DISABLE KEYS */;
LOCK TABLES `sales_bestsellers_aggregated_monthly` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `sales_bestsellers_aggregated_monthly` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`sales_bestsellers_aggregated_yearly`
--

DROP TABLE IF EXISTS `supernarede`.`sales_bestsellers_aggregated_yearly`;
CREATE TABLE  `supernarede`.`sales_bestsellers_aggregated_yearly` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `period` date NOT NULL DEFAULT '0000-00-00',
  `store_id` smallint(5) unsigned DEFAULT NULL,
  `product_id` int(10) unsigned DEFAULT NULL,
  `product_name` varchar(255) NOT NULL DEFAULT '',
  `product_price` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `qty_ordered` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `rating_pos` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNQ_PERIOD_STORE_PRODUCT` (`period`,`store_id`,`product_id`),
  KEY `IDX_STORE_ID` (`store_id`),
  KEY `IDX_PRODUCT_ID` (`product_id`),
  CONSTRAINT `FK_PRODUCT_ORDERED_AGGREGATED_YEARLY_PRODUCT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_PRODUCT_ORDERED_AGGREGATED_YEARLY_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`sales_bestsellers_aggregated_yearly`
--

/*!40000 ALTER TABLE `sales_bestsellers_aggregated_yearly` DISABLE KEYS */;
LOCK TABLES `sales_bestsellers_aggregated_yearly` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `sales_bestsellers_aggregated_yearly` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`sales_billing_agreement`
--

DROP TABLE IF EXISTS `supernarede`.`sales_billing_agreement`;
CREATE TABLE  `supernarede`.`sales_billing_agreement` (
  `agreement_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(10) unsigned NOT NULL,
  `method_code` varchar(32) NOT NULL,
  `reference_id` varchar(32) NOT NULL,
  `status` varchar(20) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `store_id` smallint(5) unsigned DEFAULT NULL,
  `agreement_label` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`agreement_id`),
  KEY `IDX_CUSTOMER` (`customer_id`),
  KEY `FK_BILLING_AGREEMENT_STORE` (`store_id`),
  CONSTRAINT `FK_BILLING_AGREEMENT_CUSTOMER` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_BILLING_AGREEMENT_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`sales_billing_agreement`
--

/*!40000 ALTER TABLE `sales_billing_agreement` DISABLE KEYS */;
LOCK TABLES `sales_billing_agreement` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `sales_billing_agreement` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`sales_billing_agreement_order`
--

DROP TABLE IF EXISTS `supernarede`.`sales_billing_agreement_order`;
CREATE TABLE  `supernarede`.`sales_billing_agreement_order` (
  `agreement_id` int(10) unsigned NOT NULL,
  `order_id` int(10) unsigned NOT NULL,
  UNIQUE KEY `UNQ_BILLING_AGREEMENT_ORDER` (`agreement_id`,`order_id`),
  KEY `FK_BILLING_AGREEMENT_ORDER_ORDER` (`order_id`),
  CONSTRAINT `FK_BILLING_AGREEMENT_ORDER_AGREEMENT` FOREIGN KEY (`agreement_id`) REFERENCES `sales_billing_agreement` (`agreement_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_BILLING_AGREEMENT_ORDER_ORDER` FOREIGN KEY (`order_id`) REFERENCES `sales_flat_order` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`sales_billing_agreement_order`
--

/*!40000 ALTER TABLE `sales_billing_agreement_order` DISABLE KEYS */;
LOCK TABLES `sales_billing_agreement_order` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `sales_billing_agreement_order` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`sales_flat_creditmemo`
--

DROP TABLE IF EXISTS `supernarede`.`sales_flat_creditmemo`;
CREATE TABLE  `supernarede`.`sales_flat_creditmemo` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `store_id` smallint(5) unsigned DEFAULT NULL,
  `adjustment_positive` decimal(12,4) DEFAULT NULL,
  `base_shipping_tax_amount` decimal(12,4) DEFAULT NULL,
  `store_to_order_rate` decimal(12,4) DEFAULT NULL,
  `base_discount_amount` decimal(12,4) DEFAULT NULL,
  `base_to_order_rate` decimal(12,4) DEFAULT NULL,
  `grand_total` decimal(12,4) DEFAULT NULL,
  `base_adjustment_negative` decimal(12,4) DEFAULT NULL,
  `base_subtotal_incl_tax` decimal(12,4) DEFAULT NULL,
  `shipping_amount` decimal(12,4) DEFAULT NULL,
  `subtotal_incl_tax` decimal(12,4) DEFAULT NULL,
  `adjustment_negative` decimal(12,4) DEFAULT NULL,
  `base_shipping_amount` decimal(12,4) DEFAULT NULL,
  `store_to_base_rate` decimal(12,4) DEFAULT NULL,
  `base_to_global_rate` decimal(12,4) DEFAULT NULL,
  `base_adjustment` decimal(12,4) DEFAULT NULL,
  `base_subtotal` decimal(12,4) DEFAULT NULL,
  `discount_amount` decimal(12,4) DEFAULT NULL,
  `subtotal` decimal(12,4) DEFAULT NULL,
  `adjustment` decimal(12,4) DEFAULT NULL,
  `base_grand_total` decimal(12,4) DEFAULT NULL,
  `base_adjustment_positive` decimal(12,4) DEFAULT NULL,
  `base_tax_amount` decimal(12,4) DEFAULT NULL,
  `shipping_tax_amount` decimal(12,4) DEFAULT NULL,
  `tax_amount` decimal(12,4) DEFAULT NULL,
  `order_id` int(10) unsigned NOT NULL,
  `email_sent` tinyint(1) unsigned DEFAULT NULL,
  `creditmemo_status` int(10) DEFAULT NULL,
  `state` int(10) DEFAULT NULL,
  `shipping_address_id` int(10) DEFAULT NULL,
  `billing_address_id` int(10) DEFAULT NULL,
  `invoice_id` int(10) DEFAULT NULL,
  `cybersource_token` varchar(255) DEFAULT NULL,
  `store_currency_code` char(3) DEFAULT NULL,
  `order_currency_code` char(3) DEFAULT NULL,
  `base_currency_code` char(3) DEFAULT NULL,
  `global_currency_code` char(3) DEFAULT NULL,
  `transaction_id` varchar(255) DEFAULT NULL,
  `increment_id` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `hidden_tax_amount` decimal(12,4) DEFAULT NULL,
  `base_hidden_tax_amount` decimal(12,4) DEFAULT NULL,
  `shipping_hidden_tax_amount` decimal(12,4) DEFAULT NULL,
  `base_shipping_hidden_tax_amount` decimal(12,4) DEFAULT NULL,
  `shipping_incl_tax` decimal(12,4) DEFAULT NULL,
  `base_shipping_incl_tax` decimal(12,4) DEFAULT NULL,
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `UNQ_INCREMENT_ID` (`increment_id`),
  KEY `IDX_STORE_ID` (`store_id`),
  KEY `IDX_ORDER_ID` (`order_id`),
  KEY `IDX_CREDITMEMO_STATUS` (`creditmemo_status`),
  KEY `IDX_STATE` (`state`),
  KEY `IDX_CREATED_AT` (`created_at`),
  CONSTRAINT `FK_SALES_FLAT_CREDITMEMO_PARENT` FOREIGN KEY (`order_id`) REFERENCES `sales_flat_order` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_SALES_FLAT_CREDITMEMO_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`sales_flat_creditmemo`
--

/*!40000 ALTER TABLE `sales_flat_creditmemo` DISABLE KEYS */;
LOCK TABLES `sales_flat_creditmemo` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `sales_flat_creditmemo` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`sales_flat_creditmemo_comment`
--

DROP TABLE IF EXISTS `supernarede`.`sales_flat_creditmemo_comment`;
CREATE TABLE  `supernarede`.`sales_flat_creditmemo_comment` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned NOT NULL,
  `is_customer_notified` int(10) DEFAULT NULL,
  `is_visible_on_front` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `comment` text,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`entity_id`),
  KEY `IDX_CREATED_AT` (`created_at`),
  KEY `IDX_PARENT_ID` (`parent_id`),
  CONSTRAINT `FK_SALES_FLAT_CREDITMEMO_COMMENT_PARENT` FOREIGN KEY (`parent_id`) REFERENCES `sales_flat_creditmemo` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`sales_flat_creditmemo_comment`
--

/*!40000 ALTER TABLE `sales_flat_creditmemo_comment` DISABLE KEYS */;
LOCK TABLES `sales_flat_creditmemo_comment` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `sales_flat_creditmemo_comment` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`sales_flat_creditmemo_grid`
--

DROP TABLE IF EXISTS `supernarede`.`sales_flat_creditmemo_grid`;
CREATE TABLE  `supernarede`.`sales_flat_creditmemo_grid` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `store_id` smallint(5) unsigned DEFAULT NULL,
  `store_to_order_rate` decimal(12,4) DEFAULT NULL,
  `base_to_order_rate` decimal(12,4) DEFAULT NULL,
  `grand_total` decimal(12,4) DEFAULT NULL,
  `store_to_base_rate` decimal(12,4) DEFAULT NULL,
  `base_to_global_rate` decimal(12,4) DEFAULT NULL,
  `base_grand_total` decimal(12,4) DEFAULT NULL,
  `order_id` int(10) unsigned NOT NULL,
  `creditmemo_status` int(10) DEFAULT NULL,
  `state` int(10) DEFAULT NULL,
  `invoice_id` int(10) DEFAULT NULL,
  `store_currency_code` char(3) DEFAULT NULL,
  `order_currency_code` char(3) DEFAULT NULL,
  `base_currency_code` char(3) DEFAULT NULL,
  `global_currency_code` char(3) DEFAULT NULL,
  `increment_id` varchar(50) DEFAULT NULL,
  `order_increment_id` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `order_created_at` datetime DEFAULT NULL,
  `billing_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `UNQ_INCREMENT_ID` (`increment_id`),
  KEY `IDX_STORE_ID` (`store_id`),
  KEY `IDX_GRAND_TOTAL` (`grand_total`),
  KEY `IDX_BASE_GRAND_TOTAL` (`base_grand_total`),
  KEY `IDX_ORDER_ID` (`order_id`),
  KEY `IDX_CREDITMEMO_STATUS` (`creditmemo_status`),
  KEY `IDX_STATE` (`state`),
  KEY `IDX_ORDER_INCREMENT_ID` (`order_increment_id`),
  KEY `IDX_CREATED_AT` (`created_at`),
  KEY `IDX_ORDER_CREATED_AT` (`order_created_at`),
  KEY `IDX_BILLING_NAME` (`billing_name`),
  CONSTRAINT `FK_SALES_FLAT_CREDITMEMO_GRID_PARENT` FOREIGN KEY (`entity_id`) REFERENCES `sales_flat_creditmemo` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_SALES_FLAT_CREDITMEMO_GRID_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`sales_flat_creditmemo_grid`
--

/*!40000 ALTER TABLE `sales_flat_creditmemo_grid` DISABLE KEYS */;
LOCK TABLES `sales_flat_creditmemo_grid` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `sales_flat_creditmemo_grid` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`sales_flat_creditmemo_item`
--

DROP TABLE IF EXISTS `supernarede`.`sales_flat_creditmemo_item`;
CREATE TABLE  `supernarede`.`sales_flat_creditmemo_item` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned NOT NULL,
  `weee_tax_applied_row_amount` decimal(12,4) DEFAULT NULL,
  `base_price` decimal(12,4) DEFAULT NULL,
  `base_weee_tax_row_disposition` decimal(12,4) DEFAULT NULL,
  `tax_amount` decimal(12,4) DEFAULT NULL,
  `base_weee_tax_applied_amount` decimal(12,4) DEFAULT NULL,
  `weee_tax_row_disposition` decimal(12,4) DEFAULT NULL,
  `base_row_total` decimal(12,4) DEFAULT NULL,
  `discount_amount` decimal(12,4) DEFAULT NULL,
  `row_total` decimal(12,4) DEFAULT NULL,
  `weee_tax_applied_amount` decimal(12,4) DEFAULT NULL,
  `base_discount_amount` decimal(12,4) DEFAULT NULL,
  `base_weee_tax_disposition` decimal(12,4) DEFAULT NULL,
  `price_incl_tax` decimal(12,4) DEFAULT NULL,
  `base_tax_amount` decimal(12,4) DEFAULT NULL,
  `weee_tax_disposition` decimal(12,4) DEFAULT NULL,
  `base_price_incl_tax` decimal(12,4) DEFAULT NULL,
  `qty` decimal(12,4) DEFAULT NULL,
  `base_cost` decimal(12,4) DEFAULT NULL,
  `base_weee_tax_applied_row_amount` decimal(12,4) DEFAULT NULL,
  `price` decimal(12,4) DEFAULT NULL,
  `base_row_total_incl_tax` decimal(12,4) DEFAULT NULL,
  `row_total_incl_tax` decimal(12,4) DEFAULT NULL,
  `product_id` int(10) DEFAULT NULL,
  `order_item_id` int(10) DEFAULT NULL,
  `additional_data` text,
  `description` text,
  `weee_tax_applied` text,
  `sku` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `hidden_tax_amount` decimal(12,4) DEFAULT NULL,
  `base_hidden_tax_amount` decimal(12,4) DEFAULT NULL,
  PRIMARY KEY (`entity_id`),
  KEY `IDX_PARENT_ID` (`parent_id`),
  CONSTRAINT `FK_SALES_FLAT_CREDITMEMO_ITEM_PARENT` FOREIGN KEY (`parent_id`) REFERENCES `sales_flat_creditmemo` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`sales_flat_creditmemo_item`
--

/*!40000 ALTER TABLE `sales_flat_creditmemo_item` DISABLE KEYS */;
LOCK TABLES `sales_flat_creditmemo_item` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `sales_flat_creditmemo_item` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`sales_flat_invoice`
--

DROP TABLE IF EXISTS `supernarede`.`sales_flat_invoice`;
CREATE TABLE  `supernarede`.`sales_flat_invoice` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `store_id` smallint(5) unsigned DEFAULT NULL,
  `base_grand_total` decimal(12,4) DEFAULT NULL,
  `shipping_tax_amount` decimal(12,4) DEFAULT NULL,
  `tax_amount` decimal(12,4) DEFAULT NULL,
  `base_tax_amount` decimal(12,4) DEFAULT NULL,
  `store_to_order_rate` decimal(12,4) DEFAULT NULL,
  `base_shipping_tax_amount` decimal(12,4) DEFAULT NULL,
  `base_discount_amount` decimal(12,4) DEFAULT NULL,
  `base_to_order_rate` decimal(12,4) DEFAULT NULL,
  `grand_total` decimal(12,4) DEFAULT NULL,
  `shipping_amount` decimal(12,4) DEFAULT NULL,
  `subtotal_incl_tax` decimal(12,4) DEFAULT NULL,
  `base_subtotal_incl_tax` decimal(12,4) DEFAULT NULL,
  `store_to_base_rate` decimal(12,4) DEFAULT NULL,
  `base_shipping_amount` decimal(12,4) DEFAULT NULL,
  `total_qty` decimal(12,4) DEFAULT NULL,
  `base_to_global_rate` decimal(12,4) DEFAULT NULL,
  `subtotal` decimal(12,4) DEFAULT NULL,
  `base_subtotal` decimal(12,4) DEFAULT NULL,
  `discount_amount` decimal(12,4) DEFAULT NULL,
  `billing_address_id` int(10) DEFAULT NULL,
  `is_used_for_refund` tinyint(1) unsigned DEFAULT NULL,
  `order_id` int(10) unsigned NOT NULL,
  `email_sent` tinyint(1) unsigned DEFAULT NULL,
  `can_void_flag` tinyint(1) unsigned DEFAULT NULL,
  `state` int(10) DEFAULT NULL,
  `shipping_address_id` int(10) DEFAULT NULL,
  `cybersource_token` varchar(255) DEFAULT NULL,
  `store_currency_code` char(3) DEFAULT NULL,
  `transaction_id` varchar(255) DEFAULT NULL,
  `order_currency_code` char(3) DEFAULT NULL,
  `base_currency_code` char(3) DEFAULT NULL,
  `global_currency_code` char(3) DEFAULT NULL,
  `increment_id` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `hidden_tax_amount` decimal(12,4) DEFAULT NULL,
  `base_hidden_tax_amount` decimal(12,4) DEFAULT NULL,
  `shipping_hidden_tax_amount` decimal(12,4) DEFAULT NULL,
  `base_shipping_hidden_tax_amount` decimal(12,4) DEFAULT NULL,
  `shipping_incl_tax` decimal(12,4) DEFAULT NULL,
  `base_shipping_incl_tax` decimal(12,4) DEFAULT NULL,
  `base_total_refunded` decimal(12,4) DEFAULT NULL,
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `UNQ_INCREMENT_ID` (`increment_id`),
  KEY `IDX_STORE_ID` (`store_id`),
  KEY `IDX_GRAND_TOTAL` (`grand_total`),
  KEY `IDX_ORDER_ID` (`order_id`),
  KEY `IDX_STATE` (`state`),
  KEY `IDX_CREATED_AT` (`created_at`),
  CONSTRAINT `FK_SALES_FLAT_INVOICE_PARENT` FOREIGN KEY (`order_id`) REFERENCES `sales_flat_order` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_SALES_FLAT_INVOICE_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`sales_flat_invoice`
--

/*!40000 ALTER TABLE `sales_flat_invoice` DISABLE KEYS */;
LOCK TABLES `sales_flat_invoice` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `sales_flat_invoice` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`sales_flat_invoice_comment`
--

DROP TABLE IF EXISTS `supernarede`.`sales_flat_invoice_comment`;
CREATE TABLE  `supernarede`.`sales_flat_invoice_comment` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned NOT NULL,
  `is_customer_notified` tinyint(1) unsigned DEFAULT NULL,
  `is_visible_on_front` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `comment` text,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`entity_id`),
  KEY `IDX_CREATED_AT` (`created_at`),
  KEY `IDX_PARENT_ID` (`parent_id`),
  CONSTRAINT `FK_SALES_FLAT_INVOICE_COMMENT_PARENT` FOREIGN KEY (`parent_id`) REFERENCES `sales_flat_invoice` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`sales_flat_invoice_comment`
--

/*!40000 ALTER TABLE `sales_flat_invoice_comment` DISABLE KEYS */;
LOCK TABLES `sales_flat_invoice_comment` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `sales_flat_invoice_comment` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`sales_flat_invoice_grid`
--

DROP TABLE IF EXISTS `supernarede`.`sales_flat_invoice_grid`;
CREATE TABLE  `supernarede`.`sales_flat_invoice_grid` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `store_id` smallint(5) unsigned DEFAULT NULL,
  `base_grand_total` decimal(12,4) DEFAULT NULL,
  `grand_total` decimal(12,4) DEFAULT NULL,
  `order_id` int(10) unsigned NOT NULL,
  `state` int(10) DEFAULT NULL,
  `store_currency_code` char(3) DEFAULT NULL,
  `order_currency_code` char(3) DEFAULT NULL,
  `base_currency_code` char(3) DEFAULT NULL,
  `global_currency_code` char(3) DEFAULT NULL,
  `increment_id` varchar(50) DEFAULT NULL,
  `order_increment_id` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `order_created_at` datetime DEFAULT NULL,
  `billing_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `UNQ_INCREMENT_ID` (`increment_id`),
  KEY `IDX_STORE_ID` (`store_id`),
  KEY `IDX_GRAND_TOTAL` (`grand_total`),
  KEY `IDX_ORDER_ID` (`order_id`),
  KEY `IDX_STATE` (`state`),
  KEY `IDX_ORDER_INCREMENT_ID` (`order_increment_id`),
  KEY `IDX_CREATED_AT` (`created_at`),
  KEY `IDX_ORDER_CREATED_AT` (`order_created_at`),
  KEY `IDX_BILLING_NAME` (`billing_name`),
  CONSTRAINT `FK_SALES_FLAT_INVOICE_GRID_PARENT` FOREIGN KEY (`entity_id`) REFERENCES `sales_flat_invoice` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_SALES_FLAT_INVOICE_GRID_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`sales_flat_invoice_grid`
--

/*!40000 ALTER TABLE `sales_flat_invoice_grid` DISABLE KEYS */;
LOCK TABLES `sales_flat_invoice_grid` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `sales_flat_invoice_grid` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`sales_flat_invoice_item`
--

DROP TABLE IF EXISTS `supernarede`.`sales_flat_invoice_item`;
CREATE TABLE  `supernarede`.`sales_flat_invoice_item` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned NOT NULL,
  `base_price` decimal(12,4) DEFAULT NULL,
  `base_weee_tax_row_disposition` decimal(12,4) DEFAULT NULL,
  `weee_tax_applied_row_amount` decimal(12,4) DEFAULT NULL,
  `base_weee_tax_applied_amount` decimal(12,4) DEFAULT NULL,
  `tax_amount` decimal(12,4) DEFAULT NULL,
  `base_row_total` decimal(12,4) DEFAULT NULL,
  `discount_amount` decimal(12,4) DEFAULT NULL,
  `row_total` decimal(12,4) DEFAULT NULL,
  `weee_tax_row_disposition` decimal(12,4) DEFAULT NULL,
  `base_discount_amount` decimal(12,4) DEFAULT NULL,
  `base_weee_tax_disposition` decimal(12,4) DEFAULT NULL,
  `price_incl_tax` decimal(12,4) DEFAULT NULL,
  `weee_tax_applied_amount` decimal(12,4) DEFAULT NULL,
  `base_tax_amount` decimal(12,4) DEFAULT NULL,
  `base_price_incl_tax` decimal(12,4) DEFAULT NULL,
  `qty` decimal(12,4) DEFAULT NULL,
  `weee_tax_disposition` decimal(12,4) DEFAULT NULL,
  `base_cost` decimal(12,4) DEFAULT NULL,
  `base_weee_tax_applied_row_amount` decimal(12,4) DEFAULT NULL,
  `price` decimal(12,4) DEFAULT NULL,
  `base_row_total_incl_tax` decimal(12,4) DEFAULT NULL,
  `row_total_incl_tax` decimal(12,4) DEFAULT NULL,
  `product_id` int(10) DEFAULT NULL,
  `order_item_id` int(10) DEFAULT NULL,
  `additional_data` text,
  `description` text,
  `weee_tax_applied` text,
  `sku` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `hidden_tax_amount` decimal(12,4) DEFAULT NULL,
  `base_hidden_tax_amount` decimal(12,4) DEFAULT NULL,
  PRIMARY KEY (`entity_id`),
  KEY `IDX_PARENT_ID` (`parent_id`),
  CONSTRAINT `FK_SALES_FLAT_INVOICE_ITEM_PARENT` FOREIGN KEY (`parent_id`) REFERENCES `sales_flat_invoice` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`sales_flat_invoice_item`
--

/*!40000 ALTER TABLE `sales_flat_invoice_item` DISABLE KEYS */;
LOCK TABLES `sales_flat_invoice_item` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `sales_flat_invoice_item` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`sales_flat_order`
--

DROP TABLE IF EXISTS `supernarede`.`sales_flat_order`;
CREATE TABLE  `supernarede`.`sales_flat_order` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `state` varchar(32) DEFAULT NULL,
  `status` varchar(32) DEFAULT NULL,
  `coupon_code` varchar(255) DEFAULT NULL,
  `protect_code` varchar(255) DEFAULT NULL,
  `shipping_description` varchar(255) DEFAULT NULL,
  `is_virtual` tinyint(1) unsigned DEFAULT NULL,
  `store_id` smallint(5) unsigned DEFAULT NULL,
  `customer_id` int(10) unsigned DEFAULT NULL,
  `base_discount_amount` decimal(12,4) DEFAULT NULL,
  `base_discount_canceled` decimal(12,4) DEFAULT NULL,
  `base_discount_invoiced` decimal(12,4) DEFAULT NULL,
  `base_discount_refunded` decimal(12,4) DEFAULT NULL,
  `base_grand_total` decimal(12,4) DEFAULT NULL,
  `base_shipping_amount` decimal(12,4) DEFAULT NULL,
  `base_shipping_canceled` decimal(12,4) DEFAULT NULL,
  `base_shipping_invoiced` decimal(12,4) DEFAULT NULL,
  `base_shipping_refunded` decimal(12,4) DEFAULT NULL,
  `base_shipping_tax_amount` decimal(12,4) DEFAULT NULL,
  `base_shipping_tax_refunded` decimal(12,4) DEFAULT NULL,
  `base_subtotal` decimal(12,4) DEFAULT NULL,
  `base_subtotal_canceled` decimal(12,4) DEFAULT NULL,
  `base_subtotal_invoiced` decimal(12,4) DEFAULT NULL,
  `base_subtotal_refunded` decimal(12,4) DEFAULT NULL,
  `base_tax_amount` decimal(12,4) DEFAULT NULL,
  `base_tax_canceled` decimal(12,4) DEFAULT NULL,
  `base_tax_invoiced` decimal(12,4) DEFAULT NULL,
  `base_tax_refunded` decimal(12,4) DEFAULT NULL,
  `base_to_global_rate` decimal(12,4) DEFAULT NULL,
  `base_to_order_rate` decimal(12,4) DEFAULT NULL,
  `base_total_canceled` decimal(12,4) DEFAULT NULL,
  `base_total_invoiced` decimal(12,4) DEFAULT NULL,
  `base_total_invoiced_cost` decimal(12,4) DEFAULT NULL,
  `base_total_offline_refunded` decimal(12,4) DEFAULT NULL,
  `base_total_online_refunded` decimal(12,4) DEFAULT NULL,
  `base_total_paid` decimal(12,4) DEFAULT NULL,
  `base_total_qty_ordered` decimal(12,4) DEFAULT NULL,
  `base_total_refunded` decimal(12,4) DEFAULT NULL,
  `discount_amount` decimal(12,4) DEFAULT NULL,
  `discount_canceled` decimal(12,4) DEFAULT NULL,
  `discount_invoiced` decimal(12,4) DEFAULT NULL,
  `discount_refunded` decimal(12,4) DEFAULT NULL,
  `grand_total` decimal(12,4) DEFAULT NULL,
  `shipping_amount` decimal(12,4) DEFAULT NULL,
  `shipping_canceled` decimal(12,4) DEFAULT NULL,
  `shipping_invoiced` decimal(12,4) DEFAULT NULL,
  `shipping_refunded` decimal(12,4) DEFAULT NULL,
  `shipping_tax_amount` decimal(12,4) DEFAULT NULL,
  `shipping_tax_refunded` decimal(12,4) DEFAULT NULL,
  `store_to_base_rate` decimal(12,4) DEFAULT NULL,
  `store_to_order_rate` decimal(12,4) DEFAULT NULL,
  `subtotal` decimal(12,4) DEFAULT NULL,
  `subtotal_canceled` decimal(12,4) DEFAULT NULL,
  `subtotal_invoiced` decimal(12,4) DEFAULT NULL,
  `subtotal_refunded` decimal(12,4) DEFAULT NULL,
  `tax_amount` decimal(12,4) DEFAULT NULL,
  `tax_canceled` decimal(12,4) DEFAULT NULL,
  `tax_invoiced` decimal(12,4) DEFAULT NULL,
  `tax_refunded` decimal(12,4) DEFAULT NULL,
  `total_canceled` decimal(12,4) DEFAULT NULL,
  `total_invoiced` decimal(12,4) DEFAULT NULL,
  `total_offline_refunded` decimal(12,4) DEFAULT NULL,
  `total_online_refunded` decimal(12,4) DEFAULT NULL,
  `total_paid` decimal(12,4) DEFAULT NULL,
  `total_qty_ordered` decimal(12,4) DEFAULT NULL,
  `total_refunded` decimal(12,4) DEFAULT NULL,
  `can_ship_partially` tinyint(1) unsigned DEFAULT NULL,
  `can_ship_partially_item` tinyint(1) unsigned DEFAULT NULL,
  `customer_is_guest` tinyint(1) unsigned DEFAULT NULL,
  `customer_note_notify` tinyint(1) unsigned DEFAULT NULL,
  `billing_address_id` int(10) DEFAULT NULL,
  `customer_group_id` smallint(5) DEFAULT NULL,
  `edit_increment` int(10) DEFAULT NULL,
  `email_sent` tinyint(1) unsigned DEFAULT NULL,
  `forced_do_shipment_with_invoice` tinyint(1) unsigned DEFAULT NULL,
  `gift_message_id` int(10) DEFAULT NULL,
  `payment_authorization_expiration` int(10) DEFAULT NULL,
  `paypal_ipn_customer_notified` int(10) DEFAULT NULL,
  `quote_address_id` int(10) DEFAULT NULL,
  `quote_id` int(10) DEFAULT NULL,
  `shipping_address_id` int(10) DEFAULT NULL,
  `adjustment_negative` decimal(12,4) DEFAULT NULL,
  `adjustment_positive` decimal(12,4) DEFAULT NULL,
  `base_adjustment_negative` decimal(12,4) DEFAULT NULL,
  `base_adjustment_positive` decimal(12,4) DEFAULT NULL,
  `base_shipping_discount_amount` decimal(12,4) DEFAULT NULL,
  `base_subtotal_incl_tax` decimal(12,4) DEFAULT NULL,
  `base_total_due` decimal(12,4) DEFAULT NULL,
  `payment_authorization_amount` decimal(12,4) DEFAULT NULL,
  `shipping_discount_amount` decimal(12,4) DEFAULT NULL,
  `subtotal_incl_tax` decimal(12,4) DEFAULT NULL,
  `total_due` decimal(12,4) DEFAULT NULL,
  `weight` decimal(12,4) DEFAULT NULL,
  `customer_dob` datetime DEFAULT NULL,
  `increment_id` varchar(50) DEFAULT NULL,
  `applied_rule_ids` varchar(255) DEFAULT NULL,
  `base_currency_code` char(3) DEFAULT NULL,
  `customer_email` varchar(255) DEFAULT NULL,
  `customer_firstname` varchar(255) DEFAULT NULL,
  `customer_lastname` varchar(255) DEFAULT NULL,
  `customer_middlename` varchar(255) DEFAULT NULL,
  `customer_prefix` varchar(255) DEFAULT NULL,
  `customer_suffix` varchar(255) DEFAULT NULL,
  `customer_taxvat` varchar(255) DEFAULT NULL,
  `discount_description` varchar(255) DEFAULT NULL,
  `ext_customer_id` varchar(255) DEFAULT NULL,
  `ext_order_id` varchar(255) DEFAULT NULL,
  `global_currency_code` char(3) DEFAULT NULL,
  `hold_before_state` varchar(255) DEFAULT NULL,
  `hold_before_status` varchar(255) DEFAULT NULL,
  `order_currency_code` varchar(255) DEFAULT NULL,
  `original_increment_id` varchar(50) DEFAULT NULL,
  `relation_child_id` varchar(32) DEFAULT NULL,
  `relation_child_real_id` varchar(32) DEFAULT NULL,
  `relation_parent_id` varchar(32) DEFAULT NULL,
  `relation_parent_real_id` varchar(32) DEFAULT NULL,
  `remote_ip` varchar(255) DEFAULT NULL,
  `shipping_method` varchar(255) DEFAULT NULL,
  `store_currency_code` char(3) DEFAULT NULL,
  `store_name` varchar(255) DEFAULT NULL,
  `x_forwarded_for` varchar(255) DEFAULT NULL,
  `customer_note` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `total_item_count` smallint(5) unsigned DEFAULT '0',
  `customer_gender` int(11) DEFAULT NULL,
  `hidden_tax_amount` decimal(12,4) DEFAULT NULL,
  `base_hidden_tax_amount` decimal(12,4) DEFAULT NULL,
  `shipping_hidden_tax_amount` decimal(12,4) DEFAULT NULL,
  `base_shipping_hidden_tax_amount` decimal(12,4) DEFAULT NULL,
  `hidden_tax_invoiced` decimal(12,4) DEFAULT NULL,
  `base_hidden_tax_invoiced` decimal(12,4) DEFAULT NULL,
  `hidden_tax_refunded` decimal(12,4) DEFAULT NULL,
  `base_hidden_tax_refunded` decimal(12,4) DEFAULT NULL,
  `shipping_incl_tax` decimal(12,4) DEFAULT NULL,
  `base_shipping_incl_tax` decimal(12,4) DEFAULT NULL,
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `UNQ_INCREMENT_ID` (`increment_id`),
  KEY `IDX_STATUS` (`status`),
  KEY `IDX_STATE` (`state`),
  KEY `IDX_STORE_ID` (`store_id`),
  KEY `IDX_CREATED_AT` (`created_at`),
  KEY `IDX_CUSTOMER_ID` (`customer_id`),
  KEY `IDX_EXT_ORDER_ID` (`ext_order_id`),
  KEY `IDX_UPDATED_AT` (`updated_at`),
  KEY `IDX_QUOTE_ID` (`quote_id`),
  CONSTRAINT `FK_SALES_FLAT_ORDER_CUSTOMER` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_SALES_FLAT_ORDER_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`sales_flat_order`
--

/*!40000 ALTER TABLE `sales_flat_order` DISABLE KEYS */;
LOCK TABLES `sales_flat_order` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `sales_flat_order` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`sales_flat_order_address`
--

DROP TABLE IF EXISTS `supernarede`.`sales_flat_order_address`;
CREATE TABLE  `supernarede`.`sales_flat_order_address` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `customer_address_id` int(10) DEFAULT NULL,
  `quote_address_id` int(10) DEFAULT NULL,
  `region_id` int(10) DEFAULT NULL,
  `customer_id` int(10) DEFAULT NULL,
  `fax` varchar(255) DEFAULT NULL,
  `region` varchar(255) DEFAULT NULL,
  `postcode` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `street` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `telephone` varchar(255) DEFAULT NULL,
  `country_id` char(2) DEFAULT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `address_type` varchar(255) DEFAULT NULL,
  `prefix` varchar(255) DEFAULT NULL,
  `middlename` varchar(255) DEFAULT NULL,
  `suffix` varchar(255) DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`entity_id`),
  KEY `IDX_PARENT_ID` (`parent_id`),
  CONSTRAINT `FK_SALES_FLAT_ORDER_ADDRESS_PARENT` FOREIGN KEY (`parent_id`) REFERENCES `sales_flat_order` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`sales_flat_order_address`
--

/*!40000 ALTER TABLE `sales_flat_order_address` DISABLE KEYS */;
LOCK TABLES `sales_flat_order_address` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `sales_flat_order_address` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`sales_flat_order_grid`
--

DROP TABLE IF EXISTS `supernarede`.`sales_flat_order_grid`;
CREATE TABLE  `supernarede`.`sales_flat_order_grid` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `status` varchar(32) DEFAULT NULL,
  `store_id` smallint(5) unsigned DEFAULT NULL,
  `store_name` varchar(255) DEFAULT NULL,
  `customer_id` int(10) unsigned DEFAULT NULL,
  `base_grand_total` decimal(12,4) DEFAULT NULL,
  `base_total_paid` decimal(12,4) DEFAULT NULL,
  `grand_total` decimal(12,4) DEFAULT NULL,
  `total_paid` decimal(12,4) DEFAULT NULL,
  `increment_id` varchar(50) DEFAULT NULL,
  `base_currency_code` char(3) DEFAULT NULL,
  `order_currency_code` varchar(255) DEFAULT NULL,
  `shipping_name` varchar(255) DEFAULT NULL,
  `billing_name` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `UNQ_INCREMENT_ID` (`increment_id`),
  KEY `IDX_STATUS` (`status`),
  KEY `IDX_STORE_ID` (`store_id`),
  KEY `IDX_BASE_GRAND_TOTAL` (`base_grand_total`),
  KEY `IDX_BASE_TOTAL_PAID` (`base_total_paid`),
  KEY `IDX_GRAND_TOTAL` (`grand_total`),
  KEY `IDX_TOTAL_PAID` (`total_paid`),
  KEY `IDX_SHIPPING_NAME` (`shipping_name`),
  KEY `IDX_BILLING_NAME` (`billing_name`),
  KEY `IDX_CREATED_AT` (`created_at`),
  KEY `IDX_CUSTOMER_ID` (`customer_id`),
  KEY `IDX_UPDATED_AT` (`updated_at`),
  CONSTRAINT `FK_SALES_FLAT_ORDER_GRID_CUSTOMER` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_SALES_FLAT_ORDER_GRID_PARENT` FOREIGN KEY (`entity_id`) REFERENCES `sales_flat_order` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_SALES_FLAT_ORDER_GRID_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`sales_flat_order_grid`
--

/*!40000 ALTER TABLE `sales_flat_order_grid` DISABLE KEYS */;
LOCK TABLES `sales_flat_order_grid` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `sales_flat_order_grid` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`sales_flat_order_item`
--

DROP TABLE IF EXISTS `supernarede`.`sales_flat_order_item`;
CREATE TABLE  `supernarede`.`sales_flat_order_item` (
  `item_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(10) unsigned NOT NULL DEFAULT '0',
  `parent_item_id` int(10) unsigned DEFAULT NULL,
  `quote_item_id` int(10) unsigned DEFAULT NULL,
  `store_id` smallint(5) unsigned DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `product_id` int(10) unsigned DEFAULT NULL,
  `product_type` varchar(255) DEFAULT NULL,
  `product_options` text,
  `weight` decimal(12,4) DEFAULT '0.0000',
  `is_virtual` tinyint(1) unsigned DEFAULT NULL,
  `sku` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `applied_rule_ids` text,
  `additional_data` text,
  `free_shipping` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_qty_decimal` tinyint(1) unsigned DEFAULT NULL,
  `no_discount` tinyint(1) unsigned DEFAULT '0',
  `qty_backordered` decimal(12,4) DEFAULT '0.0000',
  `qty_canceled` decimal(12,4) DEFAULT '0.0000',
  `qty_invoiced` decimal(12,4) DEFAULT '0.0000',
  `qty_ordered` decimal(12,4) DEFAULT '0.0000',
  `qty_refunded` decimal(12,4) DEFAULT '0.0000',
  `qty_shipped` decimal(12,4) DEFAULT '0.0000',
  `base_cost` decimal(12,4) DEFAULT '0.0000',
  `price` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `base_price` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `original_price` decimal(12,4) DEFAULT NULL,
  `base_original_price` decimal(12,4) DEFAULT NULL,
  `tax_percent` decimal(12,4) DEFAULT '0.0000',
  `tax_amount` decimal(12,4) DEFAULT '0.0000',
  `base_tax_amount` decimal(12,4) DEFAULT '0.0000',
  `tax_invoiced` decimal(12,4) DEFAULT '0.0000',
  `base_tax_invoiced` decimal(12,4) DEFAULT '0.0000',
  `discount_percent` decimal(12,4) DEFAULT '0.0000',
  `discount_amount` decimal(12,4) DEFAULT '0.0000',
  `base_discount_amount` decimal(12,4) DEFAULT '0.0000',
  `discount_invoiced` decimal(12,4) DEFAULT '0.0000',
  `base_discount_invoiced` decimal(12,4) DEFAULT '0.0000',
  `amount_refunded` decimal(12,4) DEFAULT '0.0000',
  `base_amount_refunded` decimal(12,4) DEFAULT '0.0000',
  `row_total` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `base_row_total` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `row_invoiced` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `base_row_invoiced` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `row_weight` decimal(12,4) DEFAULT '0.0000',
  `gift_message_id` int(10) DEFAULT NULL,
  `gift_message_available` int(10) DEFAULT NULL,
  `base_tax_before_discount` decimal(12,4) DEFAULT NULL,
  `tax_before_discount` decimal(12,4) DEFAULT NULL,
  `ext_order_item_id` varchar(255) DEFAULT NULL,
  `weee_tax_applied` text,
  `weee_tax_applied_amount` decimal(12,4) DEFAULT NULL,
  `weee_tax_applied_row_amount` decimal(12,4) DEFAULT NULL,
  `base_weee_tax_applied_amount` decimal(12,4) DEFAULT NULL,
  `base_weee_tax_applied_row_amount` decimal(12,4) DEFAULT NULL,
  `weee_tax_disposition` decimal(12,4) DEFAULT NULL,
  `weee_tax_row_disposition` decimal(12,4) DEFAULT NULL,
  `base_weee_tax_disposition` decimal(12,4) DEFAULT NULL,
  `base_weee_tax_row_disposition` decimal(12,4) DEFAULT NULL,
  `locked_do_invoice` tinyint(1) unsigned DEFAULT NULL,
  `locked_do_ship` tinyint(1) unsigned DEFAULT NULL,
  `price_incl_tax` decimal(12,4) DEFAULT NULL,
  `base_price_incl_tax` decimal(12,4) DEFAULT NULL,
  `row_total_incl_tax` decimal(12,4) DEFAULT NULL,
  `base_row_total_incl_tax` decimal(12,4) DEFAULT NULL,
  `hidden_tax_amount` decimal(12,4) DEFAULT NULL,
  `base_hidden_tax_amount` decimal(12,4) DEFAULT NULL,
  `hidden_tax_invoiced` decimal(12,4) DEFAULT NULL,
  `base_hidden_tax_invoiced` decimal(12,4) DEFAULT NULL,
  `hidden_tax_refunded` decimal(12,4) DEFAULT NULL,
  `base_hidden_tax_refunded` decimal(12,4) DEFAULT NULL,
  `is_nominal` int(11) NOT NULL DEFAULT '0',
  `tax_canceled` decimal(12,4) DEFAULT NULL,
  `hidden_tax_canceled` decimal(12,4) DEFAULT NULL,
  `tax_refunded` decimal(12,4) DEFAULT NULL,
  PRIMARY KEY (`item_id`),
  KEY `IDX_ORDER` (`order_id`),
  KEY `IDX_STORE_ID` (`store_id`),
  KEY `IDX_PRODUCT_ID` (`product_id`),
  CONSTRAINT `FK_SALES_FLAT_ORDER_ITEM_PARENT` FOREIGN KEY (`order_id`) REFERENCES `sales_flat_order` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_SALES_FLAT_ORDER_ITEM_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`sales_flat_order_item`
--

/*!40000 ALTER TABLE `sales_flat_order_item` DISABLE KEYS */;
LOCK TABLES `sales_flat_order_item` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `sales_flat_order_item` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`sales_flat_order_payment`
--

DROP TABLE IF EXISTS `supernarede`.`sales_flat_order_payment`;
CREATE TABLE  `supernarede`.`sales_flat_order_payment` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned NOT NULL,
  `base_shipping_captured` decimal(12,4) DEFAULT NULL,
  `shipping_captured` decimal(12,4) DEFAULT NULL,
  `amount_refunded` decimal(12,4) DEFAULT NULL,
  `base_amount_paid` decimal(12,4) DEFAULT NULL,
  `amount_canceled` decimal(12,4) DEFAULT NULL,
  `base_amount_authorized` decimal(12,4) DEFAULT NULL,
  `base_amount_paid_online` decimal(12,4) DEFAULT NULL,
  `base_amount_refunded_online` decimal(12,4) DEFAULT NULL,
  `base_shipping_amount` decimal(12,4) DEFAULT NULL,
  `shipping_amount` decimal(12,4) DEFAULT NULL,
  `amount_paid` decimal(12,4) DEFAULT NULL,
  `amount_authorized` decimal(12,4) DEFAULT NULL,
  `base_amount_ordered` decimal(12,4) DEFAULT NULL,
  `base_shipping_refunded` decimal(12,4) DEFAULT NULL,
  `shipping_refunded` decimal(12,4) DEFAULT NULL,
  `base_amount_refunded` decimal(12,4) DEFAULT NULL,
  `amount_ordered` decimal(12,4) DEFAULT NULL,
  `base_amount_canceled` decimal(12,4) DEFAULT NULL,
  `ideal_transaction_checked` tinyint(1) unsigned DEFAULT NULL,
  `quote_payment_id` int(10) DEFAULT NULL,
  `additional_data` text,
  `cc_exp_month` varchar(255) DEFAULT NULL,
  `cc_ss_start_year` varchar(255) DEFAULT NULL,
  `echeck_bank_name` varchar(255) DEFAULT NULL,
  `method` varchar(255) DEFAULT NULL,
  `cc_debug_request_body` varchar(255) DEFAULT NULL,
  `cc_secure_verify` varchar(255) DEFAULT NULL,
  `cybersource_token` varchar(255) DEFAULT NULL,
  `ideal_issuer_title` varchar(255) DEFAULT NULL,
  `protection_eligibility` varchar(255) DEFAULT NULL,
  `cc_approval` varchar(255) DEFAULT NULL,
  `cc_last4` varchar(255) DEFAULT NULL,
  `cc_status_description` varchar(255) DEFAULT NULL,
  `echeck_type` varchar(255) DEFAULT NULL,
  `paybox_question_number` varchar(255) DEFAULT NULL,
  `cc_debug_response_serialized` varchar(255) DEFAULT NULL,
  `cc_ss_start_month` varchar(255) DEFAULT NULL,
  `echeck_account_type` varchar(255) DEFAULT NULL,
  `last_trans_id` varchar(255) DEFAULT NULL,
  `cc_cid_status` varchar(255) DEFAULT NULL,
  `cc_owner` varchar(255) DEFAULT NULL,
  `cc_type` varchar(255) DEFAULT NULL,
  `ideal_issuer_id` varchar(255) DEFAULT NULL,
  `po_number` varchar(255) DEFAULT NULL,
  `cc_exp_year` varchar(255) DEFAULT NULL,
  `cc_status` varchar(255) DEFAULT NULL,
  `echeck_routing_number` varchar(255) DEFAULT NULL,
  `account_status` varchar(255) DEFAULT NULL,
  `anet_trans_method` varchar(255) DEFAULT NULL,
  `cc_debug_response_body` varchar(255) DEFAULT NULL,
  `cc_ss_issue` varchar(255) DEFAULT NULL,
  `echeck_account_name` varchar(255) DEFAULT NULL,
  `cc_avs_status` varchar(255) DEFAULT NULL,
  `cc_number_enc` varchar(255) DEFAULT NULL,
  `cc_trans_id` varchar(255) DEFAULT NULL,
  `flo2cash_account_id` varchar(255) DEFAULT NULL,
  `paybox_request_number` varchar(255) DEFAULT NULL,
  `address_status` varchar(255) DEFAULT NULL,
  `additional_information` text,
  PRIMARY KEY (`entity_id`),
  KEY `IDX_PARENT_ID` (`parent_id`),
  CONSTRAINT `FK_SALES_FLAT_ORDER_PAYMENT_PARENT` FOREIGN KEY (`parent_id`) REFERENCES `sales_flat_order` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`sales_flat_order_payment`
--

/*!40000 ALTER TABLE `sales_flat_order_payment` DISABLE KEYS */;
LOCK TABLES `sales_flat_order_payment` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `sales_flat_order_payment` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`sales_flat_order_status_history`
--

DROP TABLE IF EXISTS `supernarede`.`sales_flat_order_status_history`;
CREATE TABLE  `supernarede`.`sales_flat_order_status_history` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned NOT NULL,
  `is_customer_notified` int(10) DEFAULT NULL,
  `is_visible_on_front` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `comment` text,
  `status` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`entity_id`),
  KEY `IDX_PARENT_ID` (`parent_id`),
  KEY `IDX_CREATED_AT` (`created_at`),
  CONSTRAINT `FK_SALES_FLAT_ORDER_STATUS_HISTORY_PARENT` FOREIGN KEY (`parent_id`) REFERENCES `sales_flat_order` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`sales_flat_order_status_history`
--

/*!40000 ALTER TABLE `sales_flat_order_status_history` DISABLE KEYS */;
LOCK TABLES `sales_flat_order_status_history` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `sales_flat_order_status_history` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`sales_flat_quote`
--

DROP TABLE IF EXISTS `supernarede`.`sales_flat_quote`;
CREATE TABLE  `supernarede`.`sales_flat_quote` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `converted_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_active` tinyint(1) unsigned DEFAULT '1',
  `is_virtual` tinyint(1) unsigned DEFAULT '0',
  `is_multi_shipping` tinyint(1) unsigned DEFAULT '0',
  `items_count` int(10) unsigned DEFAULT '0',
  `items_qty` decimal(12,4) DEFAULT '0.0000',
  `orig_order_id` int(10) unsigned DEFAULT '0',
  `store_to_base_rate` decimal(12,4) DEFAULT '0.0000',
  `store_to_quote_rate` decimal(12,4) DEFAULT '0.0000',
  `base_currency_code` varchar(255) DEFAULT NULL,
  `store_currency_code` varchar(255) DEFAULT NULL,
  `quote_currency_code` varchar(255) DEFAULT NULL,
  `grand_total` decimal(12,4) DEFAULT '0.0000',
  `base_grand_total` decimal(12,4) DEFAULT '0.0000',
  `checkout_method` varchar(255) DEFAULT NULL,
  `customer_id` int(10) unsigned DEFAULT '0',
  `customer_tax_class_id` int(10) unsigned DEFAULT '0',
  `customer_group_id` int(10) unsigned DEFAULT '0',
  `customer_email` varchar(255) DEFAULT NULL,
  `customer_prefix` varchar(40) DEFAULT NULL,
  `customer_firstname` varchar(255) DEFAULT NULL,
  `customer_middlename` varchar(40) DEFAULT NULL,
  `customer_lastname` varchar(255) DEFAULT NULL,
  `customer_suffix` varchar(40) DEFAULT NULL,
  `customer_dob` datetime DEFAULT NULL,
  `customer_note` varchar(255) DEFAULT NULL,
  `customer_note_notify` tinyint(1) unsigned DEFAULT '1',
  `customer_is_guest` tinyint(1) unsigned DEFAULT '0',
  `remote_ip` varchar(32) DEFAULT NULL,
  `applied_rule_ids` varchar(255) DEFAULT NULL,
  `reserved_order_id` varchar(64) DEFAULT '',
  `password_hash` varchar(255) DEFAULT NULL,
  `coupon_code` varchar(255) DEFAULT NULL,
  `global_currency_code` varchar(255) DEFAULT NULL,
  `base_to_global_rate` decimal(12,4) DEFAULT NULL,
  `base_to_quote_rate` decimal(12,4) DEFAULT NULL,
  `customer_taxvat` varchar(255) DEFAULT NULL,
  `customer_gender` varchar(255) DEFAULT NULL,
  `subtotal` decimal(12,4) DEFAULT NULL,
  `base_subtotal` decimal(12,4) DEFAULT NULL,
  `subtotal_with_discount` decimal(12,4) DEFAULT NULL,
  `base_subtotal_with_discount` decimal(12,4) DEFAULT NULL,
  `is_changed` int(10) unsigned DEFAULT NULL,
  `trigger_recollect` tinyint(1) NOT NULL DEFAULT '0',
  `ext_shipping_info` text,
  `gift_message_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`entity_id`),
  KEY `IDX_CUSTOMER` (`customer_id`,`store_id`,`is_active`),
  KEY `FK_SALES_QUOTE_STORE` (`store_id`),
  KEY `IDX_IS_ACTIVE` (`is_active`),
  CONSTRAINT `FK_SALES_QUOTE_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`sales_flat_quote`
--

/*!40000 ALTER TABLE `sales_flat_quote` DISABLE KEYS */;
LOCK TABLES `sales_flat_quote` WRITE;
INSERT INTO `supernarede`.`sales_flat_quote` VALUES  (1,1,'2011-08-17 14:32:19','2011-08-17 16:20:32','0000-00-00 00:00:00',1,0,0,2,'8.0000',0,'1.0000','1.0000','BRL','BRL','BRL','18.3800','18.3800','',NULL,3,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,'127.0.1.1','','',NULL,NULL,'BRL','1.0000','1.0000',NULL,NULL,'18.3800','18.3800','18.3800','18.3800',1,1,NULL,NULL),
 (2,1,'2011-08-18 20:45:56','2011-08-18 22:01:56','0000-00-00 00:00:00',1,0,0,2,'3.0000',0,'1.0000','1.0000','BRL','BRL','BRL','4.7100','4.7100','',NULL,3,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,'127.0.1.1','1','',NULL,NULL,'BRL','1.0000','1.0000',NULL,NULL,'6.1000','6.1000','4.7100','4.7100',1,1,NULL,NULL),
 (3,1,'2011-08-19 12:27:54','2011-08-19 13:53:39','0000-00-00 00:00:00',1,0,0,1,'1.0000',0,'1.0000','1.0000','BRL','BRL','BRL','0.6900','0.6900','',NULL,3,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,'127.0.1.1','1','',NULL,NULL,'BRL','1.0000','1.0000',NULL,NULL,'1.3900','1.3900','0.6900','0.6900',1,0,NULL,NULL),
 (4,1,'2011-08-19 16:01:17','2011-08-19 19:23:28','0000-00-00 00:00:00',1,0,0,4,'10.0000',0,'1.0000','1.0000','BRL','BRL','BRL','30.5000','30.5000','register',NULL,3,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,'127.0.1.1','1','',NULL,NULL,'BRL','1.0000','1.0000',NULL,NULL,'31.2000','31.2000','30.5000','30.5000',1,1,NULL,NULL),
 (5,1,'2011-08-19 18:06:36','2011-08-19 18:06:57','0000-00-00 00:00:00',1,0,0,1,'1.0000',0,'1.0000','1.0000','BRL','BRL','BRL','1.9000','1.9000','register',NULL,3,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,'127.0.1.1','','',NULL,NULL,'BRL','1.0000','1.0000',NULL,NULL,'1.9000','1.9000','1.9000','1.9000',1,0,NULL,NULL),
 (6,1,'2011-08-24 12:41:47','2011-08-24 12:41:47','0000-00-00 00:00:00',1,0,0,1,'1.0000',0,'1.0000','1.0000','BRL','BRL','BRL','1.2900','1.2900',NULL,NULL,3,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,'127.0.1.1','','',NULL,NULL,'BRL','1.0000','1.0000',NULL,NULL,'1.2900','1.2900','1.2900','1.2900',1,0,NULL,NULL),
 (7,1,'2011-09-22 14:02:04','2011-09-22 14:02:04','0000-00-00 00:00:00',1,0,0,0,'0.0000',0,'1.0000','1.0000','BRL','BRL','BRL','0.0000','0.0000',NULL,1,3,1,'leandro@bhdesign.com.br',NULL,'Leandro',NULL,'JN2',NULL,NULL,NULL,1,0,'192.168.0.57',NULL,'',NULL,NULL,'BRL','1.0000','1.0000',NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL);
UNLOCK TABLES;
/*!40000 ALTER TABLE `sales_flat_quote` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`sales_flat_quote_address`
--

DROP TABLE IF EXISTS `supernarede`.`sales_flat_quote_address`;
CREATE TABLE  `supernarede`.`sales_flat_quote_address` (
  `address_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `quote_id` int(10) unsigned NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `customer_id` int(10) unsigned DEFAULT NULL,
  `save_in_address_book` tinyint(1) DEFAULT '0',
  `customer_address_id` int(10) unsigned DEFAULT NULL,
  `address_type` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `prefix` varchar(40) DEFAULT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `middlename` varchar(40) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `suffix` varchar(40) DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  `street` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `region` varchar(255) DEFAULT NULL,
  `region_id` int(10) unsigned DEFAULT NULL,
  `postcode` varchar(255) DEFAULT NULL,
  `country_id` varchar(255) DEFAULT NULL,
  `telephone` varchar(255) DEFAULT NULL,
  `fax` varchar(255) DEFAULT NULL,
  `same_as_billing` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `free_shipping` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `collect_shipping_rates` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `shipping_method` varchar(255) NOT NULL DEFAULT '',
  `shipping_description` varchar(255) NOT NULL DEFAULT '',
  `weight` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `subtotal` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `base_subtotal` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `subtotal_with_discount` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `base_subtotal_with_discount` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `tax_amount` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `base_tax_amount` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `shipping_amount` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `base_shipping_amount` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `shipping_tax_amount` decimal(12,4) DEFAULT NULL,
  `base_shipping_tax_amount` decimal(12,4) DEFAULT NULL,
  `discount_amount` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `base_discount_amount` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `grand_total` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `base_grand_total` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `customer_notes` text,
  `applied_taxes` text,
  `discount_description` varchar(255) DEFAULT NULL,
  `shipping_discount_amount` decimal(12,4) DEFAULT NULL,
  `base_shipping_discount_amount` decimal(12,4) DEFAULT NULL,
  `subtotal_incl_tax` decimal(12,4) DEFAULT NULL,
  `base_subtotal_total_incl_tax` decimal(12,4) DEFAULT NULL,
  `gift_message_id` int(10) unsigned DEFAULT NULL,
  `hidden_tax_amount` decimal(12,4) DEFAULT NULL,
  `base_hidden_tax_amount` decimal(12,4) DEFAULT NULL,
  `shipping_hidden_tax_amount` decimal(12,4) DEFAULT NULL,
  `base_shipping_hidden_tax_amount` decimal(12,4) DEFAULT NULL,
  `shipping_incl_tax` decimal(12,4) DEFAULT NULL,
  `base_shipping_incl_tax` decimal(12,4) DEFAULT NULL,
  PRIMARY KEY (`address_id`),
  KEY `FK_SALES_QUOTE_ADDRESS_SALES_QUOTE` (`quote_id`),
  CONSTRAINT `FK_SALES_QUOTE_ADDRESS_SALES_QUOTE` FOREIGN KEY (`quote_id`) REFERENCES `sales_flat_quote` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`sales_flat_quote_address`
--

/*!40000 ALTER TABLE `sales_flat_quote_address` DISABLE KEYS */;
LOCK TABLES `sales_flat_quote_address` WRITE;
INSERT INTO `supernarede`.`sales_flat_quote_address` VALUES  (3,1,'2011-08-17 14:32:21','2011-08-17 16:20:33',NULL,0,NULL,'billing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,'','','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000',NULL,'a:0:{}',NULL,NULL,NULL,'0.0000',NULL,NULL,NULL,NULL,NULL,NULL,'0.0000','0.0000'),
 (4,1,'2011-08-17 14:32:21','2011-08-17 16:20:33',NULL,0,NULL,'shipping',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,0,'','','5.6000','18.3800','18.3800','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','18.3800','18.3800',NULL,'a:0:{}','','0.0000','0.0000','18.3800',NULL,NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000'),
 (7,2,'2011-08-18 20:47:01','2011-08-18 22:01:56',NULL,0,NULL,'billing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,'','','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000',NULL,'a:0:{}',NULL,NULL,NULL,'0.0000',NULL,NULL,NULL,NULL,NULL,NULL,'0.0000','0.0000'),
 (8,2,'2011-08-18 20:47:02','2011-08-18 22:01:56',NULL,0,NULL,'shipping',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,0,'','','1.1000','6.1000','6.1000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','-1.3900','-1.3900','4.7100','4.7100',NULL,'a:0:{}','Promo Bavaria','0.0000','0.0000','6.1000',NULL,NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000'),
 (11,3,'2011-08-19 12:28:01','2011-08-19 13:53:39',NULL,0,NULL,'billing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,'','','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000',NULL,'a:0:{}',NULL,NULL,NULL,'0.0000',NULL,NULL,NULL,NULL,NULL,NULL,'0.0000','0.0000'),
 (12,3,'2011-08-19 12:28:01','2011-08-19 13:53:39',NULL,0,NULL,'shipping',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,0,'','','0.3500','1.3900','1.3900','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','-0.7000','-0.7000','0.6900','0.6900',NULL,'a:0:{}','Promo Bavaria','0.0000','0.0000','1.3900',NULL,NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000'),
 (15,4,'2011-08-19 16:24:26','2011-08-19 18:32:11',NULL,0,NULL,'billing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,'','','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000',NULL,'a:0:{}',NULL,NULL,NULL,'0.0000',NULL,NULL,NULL,NULL,NULL,NULL,'0.0000','0.0000'),
 (16,4,'2011-08-19 16:24:26','2011-08-19 18:32:11',NULL,0,NULL,'shipping',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,0,'','','4.3500','31.2000','31.2000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','-0.7000','-0.7000','30.5000','30.5000',NULL,'a:0:{}','Promo Bavaria','0.0000','0.0000','31.2000',NULL,NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000'),
 (19,5,'2011-08-19 18:06:41','2011-08-19 18:06:48',NULL,0,NULL,'billing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,'','','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000',NULL,'a:0:{}',NULL,NULL,NULL,'0.0000',NULL,NULL,NULL,NULL,NULL,NULL,'0.0000','0.0000'),
 (20,5,'2011-08-19 18:06:41','2011-08-19 18:06:48',NULL,0,NULL,'shipping',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,0,'','','0.8000','1.9000','1.9000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','1.9000','1.9000',NULL,'a:0:{}','','0.0000','0.0000','1.9000',NULL,NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000'),
 (21,6,'2011-08-24 12:41:47','2011-08-24 12:41:47',NULL,0,NULL,'billing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,'','','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000',NULL,'a:0:{}',NULL,NULL,NULL,'0.0000',NULL,NULL,NULL,NULL,NULL,NULL,'0.0000','0.0000'),
 (22,6,'2011-08-24 12:41:47','2011-08-24 12:41:47',NULL,0,NULL,'shipping',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,0,'','','0.3000','1.2900','1.2900','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','1.2900','1.2900',NULL,'a:0:{}','','0.0000','0.0000','1.2900',NULL,NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000');
UNLOCK TABLES;
/*!40000 ALTER TABLE `sales_flat_quote_address` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`sales_flat_quote_address_item`
--

DROP TABLE IF EXISTS `supernarede`.`sales_flat_quote_address_item`;
CREATE TABLE  `supernarede`.`sales_flat_quote_address_item` (
  `address_item_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_item_id` int(10) unsigned DEFAULT NULL,
  `quote_address_id` int(10) unsigned NOT NULL DEFAULT '0',
  `quote_item_id` int(10) unsigned NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `applied_rule_ids` text,
  `additional_data` text,
  `weight` decimal(12,4) DEFAULT '0.0000',
  `qty` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `discount_amount` decimal(12,4) DEFAULT '0.0000',
  `tax_amount` decimal(12,4) DEFAULT '0.0000',
  `row_total` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `base_row_total` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `row_total_with_discount` decimal(12,4) DEFAULT '0.0000',
  `base_discount_amount` decimal(12,4) DEFAULT '0.0000',
  `base_tax_amount` decimal(12,4) DEFAULT '0.0000',
  `row_weight` decimal(12,4) DEFAULT '0.0000',
  `product_id` int(10) unsigned DEFAULT NULL,
  `super_product_id` int(10) unsigned DEFAULT NULL,
  `parent_product_id` int(10) unsigned DEFAULT NULL,
  `sku` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `free_shipping` int(10) unsigned DEFAULT NULL,
  `is_qty_decimal` int(10) unsigned DEFAULT NULL,
  `price` decimal(12,4) DEFAULT NULL,
  `discount_percent` decimal(12,4) DEFAULT NULL,
  `no_discount` int(10) unsigned DEFAULT NULL,
  `tax_percent` decimal(12,4) DEFAULT NULL,
  `base_price` decimal(12,4) DEFAULT NULL,
  `base_cost` decimal(12,4) DEFAULT NULL,
  `price_incl_tax` decimal(12,4) DEFAULT NULL,
  `base_price_incl_tax` decimal(12,4) DEFAULT NULL,
  `row_total_incl_tax` decimal(12,4) DEFAULT NULL,
  `base_row_total_incl_tax` decimal(12,4) DEFAULT NULL,
  `gift_message_id` int(10) unsigned DEFAULT NULL,
  `hidden_tax_amount` decimal(12,4) DEFAULT NULL,
  `base_hidden_tax_amount` decimal(12,4) DEFAULT NULL,
  PRIMARY KEY (`address_item_id`),
  KEY `FK_QUOTE_ADDRESS_ITEM_QUOTE_ADDRESS` (`quote_address_id`),
  KEY `FK_SALES_FLAT_QUOTE_ADDRESS_ITEM_PARENT` (`parent_item_id`),
  KEY `FK_SALES_QUOTE_ADDRESS_ITEM_QUOTE_ITEM` (`quote_item_id`),
  CONSTRAINT `FK_QUOTE_ADDRESS_ITEM_QUOTE_ADDRESS` FOREIGN KEY (`quote_address_id`) REFERENCES `sales_flat_quote_address` (`address_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_SALES_FLAT_QUOTE_ADDRESS_ITEM_PARENT` FOREIGN KEY (`parent_item_id`) REFERENCES `sales_flat_quote_address_item` (`address_item_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_SALES_QUOTE_ADDRESS_ITEM_QUOTE_ITEM` FOREIGN KEY (`quote_item_id`) REFERENCES `sales_flat_quote_item` (`item_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`sales_flat_quote_address_item`
--

/*!40000 ALTER TABLE `sales_flat_quote_address_item` DISABLE KEYS */;
LOCK TABLES `sales_flat_quote_address_item` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `sales_flat_quote_address_item` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`sales_flat_quote_item`
--

DROP TABLE IF EXISTS `supernarede`.`sales_flat_quote_item`;
CREATE TABLE  `supernarede`.`sales_flat_quote_item` (
  `item_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `quote_id` int(10) unsigned NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `product_id` int(10) unsigned DEFAULT NULL,
  `store_id` smallint(5) unsigned DEFAULT NULL,
  `parent_item_id` int(10) unsigned DEFAULT NULL,
  `is_virtual` tinyint(1) unsigned DEFAULT NULL,
  `sku` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `applied_rule_ids` text,
  `additional_data` text,
  `free_shipping` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_qty_decimal` tinyint(1) unsigned DEFAULT NULL,
  `no_discount` tinyint(1) unsigned DEFAULT '0',
  `weight` decimal(12,4) DEFAULT '0.0000',
  `qty` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `price` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `base_price` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `custom_price` decimal(12,4) DEFAULT NULL,
  `discount_percent` decimal(12,4) DEFAULT '0.0000',
  `discount_amount` decimal(12,4) DEFAULT '0.0000',
  `base_discount_amount` decimal(12,4) DEFAULT '0.0000',
  `tax_percent` decimal(12,4) DEFAULT '0.0000',
  `tax_amount` decimal(12,4) DEFAULT '0.0000',
  `base_tax_amount` decimal(12,4) DEFAULT '0.0000',
  `row_total` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `base_row_total` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `row_total_with_discount` decimal(12,4) DEFAULT '0.0000',
  `row_weight` decimal(12,4) DEFAULT '0.0000',
  `product_type` varchar(255) DEFAULT NULL,
  `base_tax_before_discount` decimal(12,4) DEFAULT NULL,
  `tax_before_discount` decimal(12,4) DEFAULT NULL,
  `original_custom_price` decimal(12,4) DEFAULT NULL,
  `redirect_url` varchar(255) DEFAULT NULL,
  `base_cost` decimal(12,4) DEFAULT NULL,
  `price_incl_tax` decimal(12,4) DEFAULT NULL,
  `base_price_incl_tax` decimal(12,4) DEFAULT NULL,
  `row_total_incl_tax` decimal(12,4) DEFAULT NULL,
  `base_row_total_incl_tax` decimal(12,4) DEFAULT NULL,
  `gift_message_id` int(10) unsigned DEFAULT NULL,
  `weee_tax_applied` text,
  `weee_tax_applied_amount` decimal(12,4) DEFAULT NULL,
  `weee_tax_applied_row_amount` decimal(12,4) DEFAULT NULL,
  `base_weee_tax_applied_amount` decimal(12,4) DEFAULT NULL,
  `base_weee_tax_applied_row_amount` decimal(12,4) DEFAULT NULL,
  `weee_tax_disposition` decimal(12,4) DEFAULT NULL,
  `weee_tax_row_disposition` decimal(12,4) DEFAULT NULL,
  `base_weee_tax_disposition` decimal(12,4) DEFAULT NULL,
  `base_weee_tax_row_disposition` decimal(12,4) DEFAULT NULL,
  `hidden_tax_amount` decimal(12,4) DEFAULT NULL,
  `base_hidden_tax_amount` decimal(12,4) DEFAULT NULL,
  PRIMARY KEY (`item_id`),
  KEY `FK_SALES_FLAT_QUOTE_ITEM_PARENT_ITEM` (`parent_item_id`),
  KEY `FK_SALES_QUOTE_ITEM_CATALOG_PRODUCT_ENTITY` (`product_id`),
  KEY `FK_SALES_QUOTE_ITEM_SALES_QUOTE` (`quote_id`),
  KEY `FK_SALES_QUOTE_ITEM_STORE` (`store_id`),
  CONSTRAINT `FK_SALES_FLAT_QUOTE_ITEM_PARENT_ITEM` FOREIGN KEY (`parent_item_id`) REFERENCES `sales_flat_quote_item` (`item_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_SALES_QUOTE_ITEM_CATALOG_PRODUCT_ENTITY` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_SALES_QUOTE_ITEM_SALES_QUOTE` FOREIGN KEY (`quote_id`) REFERENCES `sales_flat_quote` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_SALES_QUOTE_ITEM_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`sales_flat_quote_item`
--

/*!40000 ALTER TABLE `sales_flat_quote_item` DISABLE KEYS */;
LOCK TABLES `sales_flat_quote_item` WRITE;
INSERT INTO `supernarede`.`sales_flat_quote_item` VALUES  (2,1,'2011-08-17 14:34:54','2011-08-17 16:20:33',5,1,NULL,0,'C7987654','Cerveja Exemplo Skol Escura',NULL,'',NULL,0,0,0,'0.8000','6.0000','1.9000','1.9000',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','11.4000','11.4000','0.0000','4.8000','simple',NULL,NULL,NULL,NULL,NULL,'1.9000','1.9000','11.4000','11.4000',NULL,'a:0:{}','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000',NULL,NULL),
 (5,1,'2011-08-17 15:24:54','2011-08-17 16:20:33',3,1,NULL,0,'C548748','Cerveja Exemplo Heineken Long',NULL,'',NULL,0,0,0,'0.4000','2.0000','3.4900','3.4900',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','6.9800','6.9800','0.0000','0.8000','simple',NULL,NULL,NULL,NULL,NULL,'3.4900','3.4900','6.9800','6.9800',NULL,'a:0:{}','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000',NULL,NULL),
 (6,2,'2011-08-18 20:45:56','2011-08-18 21:45:01',2,1,NULL,0,'C54654654','Cerveja Exemplo Bavaria',NULL,'1',NULL,0,0,0,'0.3500','2.0000','1.3900','1.3900',NULL,'50.0000','1.3900','1.3900','0.0000','0.0000','0.0000','2.7800','2.7800','0.0000','0.7000','simple',NULL,NULL,NULL,NULL,NULL,'1.3900','1.3900','2.7800','2.7800',NULL,'a:0:{}','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000',NULL,NULL),
 (7,2,'2011-08-18 21:28:13','2011-08-18 21:48:47',3,1,NULL,0,'C548748','Cerveja Exemplo Heineken Long',NULL,'',NULL,0,0,0,'0.4000','1.0000','3.3200','3.3200',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','3.3200','3.3200','0.0000','0.4000','simple',NULL,NULL,NULL,NULL,NULL,'3.3200','3.3200','3.3200','3.3200',NULL,'a:0:{}','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000',NULL,NULL),
 (9,3,'2011-08-19 13:29:44','2011-08-19 13:29:44',2,1,NULL,0,'C54654654','Cerveja Exemplo Bavaria',NULL,'1',NULL,0,0,0,'0.3500','1.0000','1.3900','1.3900',NULL,'50.0000','0.7000','0.7000','0.0000','0.0000','0.0000','1.3900','1.3900','0.0000','0.3500','simple',NULL,NULL,NULL,NULL,NULL,'1.3900','1.3900','1.3900','1.3900',NULL,'a:0:{}','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000',NULL,NULL),
 (10,4,'2011-08-19 16:01:18','2011-08-19 18:32:11',3,1,NULL,0,'C548748','Cerveja Exemplo Heineken Long',NULL,'',NULL,0,0,0,'0.4000','3.0000','3.3200','3.3200',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','9.9600','9.9600','0.0000','1.2000','simple',NULL,NULL,NULL,NULL,NULL,'3.3200','3.3200','9.9600','9.9600',NULL,'a:0:{}','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000',NULL,NULL),
 (11,4,'2011-08-19 16:15:02','2011-08-19 16:15:02',5,1,NULL,0,'C7987654','Cerveja Exemplo Skol Escura',NULL,'',NULL,0,0,0,'0.8000','1.0000','1.9000','1.9000',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','1.9000','1.9000','0.0000','0.8000','simple',NULL,NULL,NULL,NULL,NULL,'1.9000','1.9000','1.9000','1.9000',NULL,'a:0:{}','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000',NULL,NULL),
 (12,4,'2011-08-19 16:15:51','2011-08-19 16:15:51',2,1,NULL,0,'C54654654','Cerveja Exemplo Bavaria',NULL,'1',NULL,0,0,0,'0.3500','1.0000','1.3900','1.3900',NULL,'50.0000','0.7000','0.7000','0.0000','0.0000','0.0000','1.3900','1.3900','0.0000','0.3500','simple',NULL,NULL,NULL,NULL,NULL,'1.3900','1.3900','1.3900','1.3900',NULL,'a:0:{}','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000',NULL,NULL),
 (13,4,'2011-08-19 16:23:32','2011-08-19 16:27:34',4,1,NULL,0,'C/879846','Cerveja Exemplo Heineken Long 2',NULL,'',NULL,0,0,0,'0.4000','5.0000','3.5900','3.5900',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','17.9500','17.9500','0.0000','2.0000','simple',NULL,NULL,NULL,NULL,NULL,'3.5900','3.5900','17.9500','17.9500',NULL,'a:0:{}','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000',NULL,NULL),
 (14,5,'2011-08-19 18:06:37','2011-08-19 18:06:37',5,1,NULL,0,'C7987654','Cerveja Exemplo Skol Escura',NULL,'',NULL,0,0,0,'0.8000','1.0000','1.9000','1.9000',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','1.9000','1.9000','0.0000','0.8000','simple',NULL,NULL,NULL,NULL,NULL,'1.9000','1.9000','1.9000','1.9000',NULL,'a:0:{}','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000',NULL,NULL),
 (15,6,'2011-08-24 12:41:47','2011-08-24 12:41:47',1,1,NULL,0,'C050505','Cerveja Exemplo Sem alcool',NULL,'',NULL,0,0,0,'0.3000','1.0000','1.2900','1.2900',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','1.2900','1.2900','0.0000','0.3000','simple',NULL,NULL,NULL,NULL,NULL,'1.2900','1.2900','1.2900','1.2900',NULL,'a:0:{}','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000',NULL,NULL);
UNLOCK TABLES;
/*!40000 ALTER TABLE `sales_flat_quote_item` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`sales_flat_quote_item_option`
--

DROP TABLE IF EXISTS `supernarede`.`sales_flat_quote_item_option`;
CREATE TABLE  `supernarede`.`sales_flat_quote_item_option` (
  `option_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item_id` int(10) unsigned NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `code` varchar(255) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`option_id`),
  KEY `FK_SALES_QUOTE_ITEM_OPTION_ITEM_ID` (`item_id`),
  CONSTRAINT `FK_SALES_QUOTE_ITEM_OPTION_ITEM_ID` FOREIGN KEY (`item_id`) REFERENCES `sales_flat_quote_item` (`item_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COMMENT='Additional options for quote item';

--
-- Dumping data for table `supernarede`.`sales_flat_quote_item_option`
--

/*!40000 ALTER TABLE `sales_flat_quote_item_option` DISABLE KEYS */;
LOCK TABLES `sales_flat_quote_item_option` WRITE;
INSERT INTO `supernarede`.`sales_flat_quote_item_option` VALUES  (2,2,5,'info_buyRequest','a:4:{s:4:\"cart\";s:3:\"add\";s:4:\"uenc\";s:36:\"aHR0cDovL3N1cGVybmFyZWRlLmxvY2FsLw,,\";s:7:\"product\";s:1:\"5\";s:3:\"qty\";s:2:\"15\";}'),
 (5,5,3,'info_buyRequest','a:4:{s:4:\"cart\";s:3:\"add\";s:4:\"uenc\";s:36:\"aHR0cDovL3N1cGVybmFyZWRlLmxvY2FsLw,,\";s:7:\"product\";s:1:\"3\";s:3:\"qty\";s:1:\"1\";}'),
 (6,6,2,'info_buyRequest','a:4:{s:4:\"cart\";s:3:\"add\";s:4:\"uenc\";s:72:\"aHR0cDovL3N1cGVybmFyZWRlLmxvY2FsL2RlcGFydGFtZW50b3MvYmViaWRhcy9jZXJ2ZWph\";s:7:\"product\";s:1:\"2\";s:3:\"qty\";s:1:\"1\";}'),
 (7,7,3,'info_buyRequest','a:4:{s:4:\"cart\";s:3:\"add\";s:4:\"uenc\";s:36:\"aHR0cDovL3N1cGVybmFyZWRlLmxvY2FsLw,,\";s:7:\"product\";s:1:\"3\";s:3:\"qty\";s:1:\"3\";}'),
 (9,9,2,'info_buyRequest','a:4:{s:4:\"cart\";s:3:\"add\";s:4:\"uenc\";s:72:\"aHR0cDovL3N1cGVybmFyZWRlLmxvY2FsL2RlcGFydGFtZW50b3MvYmViaWRhcy9jZXJ2ZWph\";s:7:\"product\";s:1:\"2\";s:3:\"qty\";s:1:\"1\";}'),
 (10,10,3,'info_buyRequest','a:4:{s:4:\"cart\";s:3:\"add\";s:4:\"uenc\";s:36:\"aHR0cDovL3N1cGVybmFyZWRlLmxvY2FsLw,,\";s:7:\"product\";s:1:\"3\";s:3:\"qty\";s:1:\"1\";}'),
 (11,11,5,'info_buyRequest','a:4:{s:4:\"cart\";s:3:\"add\";s:4:\"uenc\";s:36:\"aHR0cDovL3N1cGVybmFyZWRlLmxvY2FsLw,,\";s:7:\"product\";s:1:\"5\";s:3:\"qty\";s:1:\"1\";}'),
 (12,12,2,'info_buyRequest','a:4:{s:4:\"cart\";s:3:\"add\";s:4:\"uenc\";s:36:\"aHR0cDovL3N1cGVybmFyZWRlLmxvY2FsLw,,\";s:7:\"product\";s:1:\"2\";s:3:\"qty\";s:1:\"1\";}'),
 (13,13,4,'info_buyRequest','a:5:{s:4:\"cart\";s:3:\"add\";s:4:\"uenc\";s:76:\"aHR0cDovL3N1cGVybmFyZWRlLmxvY2FsL2NlcnZlamEtZXhlbXBsby1oZWluZWtlbi1sb25nLTE,\";s:7:\"product\";s:1:\"4\";s:15:\"related_product\";s:0:\"\";s:3:\"qty\";s:1:\"3\";}'),
 (14,14,5,'info_buyRequest','a:4:{s:4:\"cart\";s:3:\"add\";s:4:\"uenc\";s:36:\"aHR0cDovL3N1cGVybmFyZWRlLmxvY2FsLw,,\";s:7:\"product\";s:1:\"5\";s:3:\"qty\";s:1:\"1\";}'),
 (15,15,1,'info_buyRequest','a:4:{s:4:\"cart\";s:3:\"add\";s:4:\"uenc\";s:72:\"aHR0cDovL3N1cGVybmFyZWRlLmxvY2FsL2RlcGFydGFtZW50b3MvYmViaWRhcy9jZXJ2ZWph\";s:7:\"product\";s:1:\"1\";s:3:\"qty\";s:1:\"1\";}');
UNLOCK TABLES;
/*!40000 ALTER TABLE `sales_flat_quote_item_option` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`sales_flat_quote_payment`
--

DROP TABLE IF EXISTS `supernarede`.`sales_flat_quote_payment`;
CREATE TABLE  `supernarede`.`sales_flat_quote_payment` (
  `payment_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `quote_id` int(10) unsigned NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `method` varchar(255) DEFAULT '',
  `cc_type` varchar(255) DEFAULT '',
  `cc_number_enc` varchar(255) DEFAULT '',
  `cc_last4` varchar(255) DEFAULT '',
  `cc_cid_enc` varchar(255) DEFAULT '',
  `cc_owner` varchar(255) DEFAULT '',
  `cc_exp_month` tinyint(2) unsigned DEFAULT '0',
  `cc_exp_year` smallint(4) unsigned DEFAULT '0',
  `cc_ss_owner` varchar(255) DEFAULT '',
  `cc_ss_start_month` tinyint(2) unsigned DEFAULT '0',
  `cc_ss_start_year` smallint(4) unsigned DEFAULT '0',
  `cybersource_token` varchar(255) DEFAULT '',
  `paypal_correlation_id` varchar(255) DEFAULT '',
  `paypal_payer_id` varchar(255) DEFAULT '',
  `paypal_payer_status` varchar(255) DEFAULT '',
  `po_number` varchar(255) DEFAULT '',
  `additional_data` text,
  `cc_ss_issue` varchar(255) DEFAULT NULL,
  `additional_information` text,
  `ideal_issuer_id` varchar(255) DEFAULT NULL,
  `ideal_issuer_list` text,
  PRIMARY KEY (`payment_id`),
  KEY `FK_SALES_QUOTE_PAYMENT_SALES_QUOTE` (`quote_id`),
  CONSTRAINT `FK_SALES_QUOTE_PAYMENT_SALES_QUOTE` FOREIGN KEY (`quote_id`) REFERENCES `sales_flat_quote` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`sales_flat_quote_payment`
--

/*!40000 ALTER TABLE `sales_flat_quote_payment` DISABLE KEYS */;
LOCK TABLES `sales_flat_quote_payment` WRITE;
INSERT INTO `supernarede`.`sales_flat_quote_payment` VALUES  (1,3,'2011-08-19 13:53:39','2011-08-19 13:53:39','','','','','','',0,0,'',0,0,'','','','','',NULL,NULL,NULL,NULL,NULL),
 (2,4,'2011-08-19 16:56:34','2011-08-19 18:32:11','','','','','','',0,0,'',0,0,'','','','','',NULL,NULL,NULL,NULL,NULL),
 (3,5,'2011-08-19 18:06:48','2011-08-19 18:06:48','','','','','','',0,0,'',0,0,'','','','','',NULL,NULL,NULL,NULL,NULL);
UNLOCK TABLES;
/*!40000 ALTER TABLE `sales_flat_quote_payment` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`sales_flat_quote_shipping_rate`
--

DROP TABLE IF EXISTS `supernarede`.`sales_flat_quote_shipping_rate`;
CREATE TABLE  `supernarede`.`sales_flat_quote_shipping_rate` (
  `rate_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `address_id` int(10) unsigned NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `carrier` varchar(255) DEFAULT NULL,
  `carrier_title` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `method` varchar(255) DEFAULT NULL,
  `method_description` text,
  `price` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `error_message` text,
  `method_title` text,
  PRIMARY KEY (`rate_id`),
  KEY `FK_SALES_QUOTE_SHIPPING_RATE_ADDRESS` (`address_id`),
  CONSTRAINT `FK_SALES_QUOTE_SHIPPING_RATE_ADDRESS` FOREIGN KEY (`address_id`) REFERENCES `sales_flat_quote_address` (`address_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`sales_flat_quote_shipping_rate`
--

/*!40000 ALTER TABLE `sales_flat_quote_shipping_rate` DISABLE KEYS */;
LOCK TABLES `sales_flat_quote_shipping_rate` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `sales_flat_quote_shipping_rate` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`sales_flat_shipment`
--

DROP TABLE IF EXISTS `supernarede`.`sales_flat_shipment`;
CREATE TABLE  `supernarede`.`sales_flat_shipment` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `store_id` smallint(5) unsigned DEFAULT NULL,
  `total_weight` decimal(12,4) DEFAULT NULL,
  `total_qty` decimal(12,4) DEFAULT NULL,
  `email_sent` tinyint(1) unsigned DEFAULT NULL,
  `order_id` int(10) unsigned NOT NULL,
  `customer_id` int(10) DEFAULT NULL,
  `shipping_address_id` int(10) DEFAULT NULL,
  `billing_address_id` int(10) DEFAULT NULL,
  `shipment_status` int(10) DEFAULT NULL,
  `increment_id` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `UNQ_INCREMENT_ID` (`increment_id`),
  KEY `IDX_STORE_ID` (`store_id`),
  KEY `IDX_TOTAL_QTY` (`total_qty`),
  KEY `IDX_ORDER_ID` (`order_id`),
  KEY `IDX_CREATED_AT` (`created_at`),
  KEY `IDX_UPDATED_AT` (`updated_at`),
  CONSTRAINT `FK_SALES_FLAT_SHIPMENT_PARENT` FOREIGN KEY (`order_id`) REFERENCES `sales_flat_order` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_SALES_FLAT_SHIPMENT_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`sales_flat_shipment`
--

/*!40000 ALTER TABLE `sales_flat_shipment` DISABLE KEYS */;
LOCK TABLES `sales_flat_shipment` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `sales_flat_shipment` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`sales_flat_shipment_comment`
--

DROP TABLE IF EXISTS `supernarede`.`sales_flat_shipment_comment`;
CREATE TABLE  `supernarede`.`sales_flat_shipment_comment` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned NOT NULL,
  `is_customer_notified` int(10) DEFAULT NULL,
  `is_visible_on_front` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `comment` text,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`entity_id`),
  KEY `IDX_CREATED_AT` (`created_at`),
  KEY `IDX_PARENT_ID` (`parent_id`),
  CONSTRAINT `FK_SALES_FLAT_SHIPMENT_COMMENT_PARENT` FOREIGN KEY (`parent_id`) REFERENCES `sales_flat_shipment` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`sales_flat_shipment_comment`
--

/*!40000 ALTER TABLE `sales_flat_shipment_comment` DISABLE KEYS */;
LOCK TABLES `sales_flat_shipment_comment` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `sales_flat_shipment_comment` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`sales_flat_shipment_grid`
--

DROP TABLE IF EXISTS `supernarede`.`sales_flat_shipment_grid`;
CREATE TABLE  `supernarede`.`sales_flat_shipment_grid` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `store_id` smallint(5) unsigned DEFAULT NULL,
  `total_qty` decimal(12,4) DEFAULT NULL,
  `order_id` int(10) unsigned NOT NULL,
  `shipment_status` int(10) DEFAULT NULL,
  `increment_id` varchar(50) DEFAULT NULL,
  `order_increment_id` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `order_created_at` datetime DEFAULT NULL,
  `shipping_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `UNQ_INCREMENT_ID` (`increment_id`),
  KEY `IDX_STORE_ID` (`store_id`),
  KEY `IDX_TOTAL_QTY` (`total_qty`),
  KEY `IDX_ORDER_ID` (`order_id`),
  KEY `IDX_SHIPMENT_STATUS` (`shipment_status`),
  KEY `IDX_ORDER_INCREMENT_ID` (`order_increment_id`),
  KEY `IDX_CREATED_AT` (`created_at`),
  KEY `IDX_ORDER_CREATED_AT` (`order_created_at`),
  KEY `IDX_SHIPPING_NAME` (`shipping_name`),
  CONSTRAINT `FK_SALES_FLAT_SHIPMENT_GRID_PARENT` FOREIGN KEY (`entity_id`) REFERENCES `sales_flat_shipment` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_SALES_FLAT_SHIPMENT_GRID_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`sales_flat_shipment_grid`
--

/*!40000 ALTER TABLE `sales_flat_shipment_grid` DISABLE KEYS */;
LOCK TABLES `sales_flat_shipment_grid` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `sales_flat_shipment_grid` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`sales_flat_shipment_item`
--

DROP TABLE IF EXISTS `supernarede`.`sales_flat_shipment_item`;
CREATE TABLE  `supernarede`.`sales_flat_shipment_item` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned NOT NULL,
  `row_total` decimal(12,4) DEFAULT NULL,
  `price` decimal(12,4) DEFAULT NULL,
  `weight` decimal(12,4) DEFAULT NULL,
  `qty` decimal(12,4) DEFAULT NULL,
  `product_id` int(10) DEFAULT NULL,
  `order_item_id` int(10) DEFAULT NULL,
  `additional_data` text,
  `description` text,
  `name` varchar(255) DEFAULT NULL,
  `sku` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`entity_id`),
  KEY `IDX_PARENT_ID` (`parent_id`),
  CONSTRAINT `FK_SALES_FLAT_SHIPMENT_ITEM_PARENT` FOREIGN KEY (`parent_id`) REFERENCES `sales_flat_shipment` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`sales_flat_shipment_item`
--

/*!40000 ALTER TABLE `sales_flat_shipment_item` DISABLE KEYS */;
LOCK TABLES `sales_flat_shipment_item` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `sales_flat_shipment_item` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`sales_flat_shipment_track`
--

DROP TABLE IF EXISTS `supernarede`.`sales_flat_shipment_track`;
CREATE TABLE  `supernarede`.`sales_flat_shipment_track` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned NOT NULL,
  `weight` decimal(12,4) DEFAULT NULL,
  `qty` decimal(12,4) DEFAULT NULL,
  `order_id` int(10) unsigned NOT NULL,
  `number` text,
  `description` text,
  `title` varchar(255) DEFAULT NULL,
  `carrier_code` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`entity_id`),
  KEY `IDX_PARENT_ID` (`parent_id`),
  KEY `IDX_ORDER_ID` (`order_id`),
  KEY `IDX_CREATED_AT` (`created_at`),
  CONSTRAINT `FK_SALES_FLAT_SHIPMENT_TRACK_PARENT` FOREIGN KEY (`parent_id`) REFERENCES `sales_flat_shipment` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`sales_flat_shipment_track`
--

/*!40000 ALTER TABLE `sales_flat_shipment_track` DISABLE KEYS */;
LOCK TABLES `sales_flat_shipment_track` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `sales_flat_shipment_track` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`sales_invoiced_aggregated`
--

DROP TABLE IF EXISTS `supernarede`.`sales_invoiced_aggregated`;
CREATE TABLE  `supernarede`.`sales_invoiced_aggregated` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `period` date NOT NULL DEFAULT '0000-00-00',
  `store_id` smallint(5) unsigned DEFAULT NULL,
  `order_status` varchar(50) NOT NULL DEFAULT '',
  `orders_count` int(11) NOT NULL DEFAULT '0',
  `orders_invoiced` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `invoiced` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `invoiced_captured` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `invoiced_not_captured` decimal(12,4) NOT NULL DEFAULT '0.0000',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNQ_PERIOD_STORE_ORDER_STATUS` (`period`,`store_id`,`order_status`),
  KEY `IDX_STORE_ID` (`store_id`),
  CONSTRAINT `FK_SALES_INVOICED_AGGREGATED_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`sales_invoiced_aggregated`
--

/*!40000 ALTER TABLE `sales_invoiced_aggregated` DISABLE KEYS */;
LOCK TABLES `sales_invoiced_aggregated` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `sales_invoiced_aggregated` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`sales_invoiced_aggregated_order`
--

DROP TABLE IF EXISTS `supernarede`.`sales_invoiced_aggregated_order`;
CREATE TABLE  `supernarede`.`sales_invoiced_aggregated_order` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `period` date NOT NULL DEFAULT '0000-00-00',
  `store_id` smallint(5) unsigned DEFAULT NULL,
  `order_status` varchar(50) NOT NULL DEFAULT '',
  `orders_count` int(11) NOT NULL DEFAULT '0',
  `orders_invoiced` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `invoiced` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `invoiced_captured` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `invoiced_not_captured` decimal(12,4) NOT NULL DEFAULT '0.0000',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNQ_PERIOD_STORE_ORDER_STATUS` (`period`,`store_id`,`order_status`),
  KEY `IDX_STORE_ID` (`store_id`),
  CONSTRAINT `FK_SALES_INVOICED_AGGREGATED_ORDER_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`sales_invoiced_aggregated_order`
--

/*!40000 ALTER TABLE `sales_invoiced_aggregated_order` DISABLE KEYS */;
LOCK TABLES `sales_invoiced_aggregated_order` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `sales_invoiced_aggregated_order` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`sales_order_aggregated_created`
--

DROP TABLE IF EXISTS `supernarede`.`sales_order_aggregated_created`;
CREATE TABLE  `supernarede`.`sales_order_aggregated_created` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `period` date NOT NULL DEFAULT '0000-00-00',
  `store_id` smallint(5) unsigned DEFAULT NULL,
  `order_status` varchar(50) NOT NULL DEFAULT '',
  `orders_count` int(11) NOT NULL DEFAULT '0',
  `total_qty_ordered` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `total_qty_invoiced` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `total_income_amount` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `total_revenue_amount` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `total_profit_amount` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `total_invoiced_amount` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `total_canceled_amount` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `total_paid_amount` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `total_refunded_amount` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `total_tax_amount` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `total_tax_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `total_shipping_amount` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `total_shipping_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `total_discount_amount` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `total_discount_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNQ_PERIOD_STORE_ORDER_STATUS` (`period`,`store_id`,`order_status`),
  KEY `IDX_STORE_ID` (`store_id`),
  CONSTRAINT `FK_SALES_ORDER_AGGREGATED_CREATED` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`sales_order_aggregated_created`
--

/*!40000 ALTER TABLE `sales_order_aggregated_created` DISABLE KEYS */;
LOCK TABLES `sales_order_aggregated_created` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `sales_order_aggregated_created` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`sales_order_status`
--

DROP TABLE IF EXISTS `supernarede`.`sales_order_status`;
CREATE TABLE  `supernarede`.`sales_order_status` (
  `status` varchar(32) NOT NULL,
  `label` varchar(128) NOT NULL,
  PRIMARY KEY (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`sales_order_status`
--

/*!40000 ALTER TABLE `sales_order_status` DISABLE KEYS */;
LOCK TABLES `sales_order_status` WRITE;
INSERT INTO `supernarede`.`sales_order_status` VALUES  ('canceled','Cancelado'),
 ('closed','Fechado'),
 ('complete','Completo'),
 ('fraud','Suspeita de Fraude'),
 ('holded','Segurado'),
 ('payment_review','Análise de Pagamento'),
 ('pending','Pendente'),
 ('pending_payment','Pagamento Pendente'),
 ('pending_paypal','PayPal Pendente'),
 ('processing','Processando');
UNLOCK TABLES;
/*!40000 ALTER TABLE `sales_order_status` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`sales_order_status_label`
--

DROP TABLE IF EXISTS `supernarede`.`sales_order_status_label`;
CREATE TABLE  `supernarede`.`sales_order_status_label` (
  `status` varchar(32) NOT NULL,
  `store_id` smallint(5) unsigned NOT NULL,
  `label` varchar(128) NOT NULL,
  PRIMARY KEY (`status`,`store_id`),
  KEY `FK_SALES_ORDER_STATUS_LABEL_STORE` (`store_id`),
  CONSTRAINT `FK_SALES_ORDER_STATUS_LABEL_STATUS` FOREIGN KEY (`status`) REFERENCES `sales_order_status` (`status`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_SALES_ORDER_STATUS_LABEL_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`sales_order_status_label`
--

/*!40000 ALTER TABLE `sales_order_status_label` DISABLE KEYS */;
LOCK TABLES `sales_order_status_label` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `sales_order_status_label` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`sales_order_status_state`
--

DROP TABLE IF EXISTS `supernarede`.`sales_order_status_state`;
CREATE TABLE  `supernarede`.`sales_order_status_state` (
  `status` varchar(32) NOT NULL,
  `state` varchar(32) NOT NULL,
  `is_default` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`status`,`state`),
  CONSTRAINT `FK_SALES_ORDER_STATUS_STATE_STATUS` FOREIGN KEY (`status`) REFERENCES `sales_order_status` (`status`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`sales_order_status_state`
--

/*!40000 ALTER TABLE `sales_order_status_state` DISABLE KEYS */;
LOCK TABLES `sales_order_status_state` WRITE;
INSERT INTO `supernarede`.`sales_order_status_state` VALUES  ('canceled','canceled',1),
 ('closed','closed',1),
 ('complete','complete',1),
 ('fraud','payment_review',0),
 ('holded','holded',1),
 ('payment_review','payment_review',1),
 ('pending','new',1),
 ('pending_payment','pending_payment',1),
 ('processing','processing',1);
UNLOCK TABLES;
/*!40000 ALTER TABLE `sales_order_status_state` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`sales_order_tax`
--

DROP TABLE IF EXISTS `supernarede`.`sales_order_tax`;
CREATE TABLE  `supernarede`.`sales_order_tax` (
  `tax_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(10) unsigned NOT NULL,
  `code` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `percent` decimal(12,4) NOT NULL,
  `amount` decimal(12,4) NOT NULL,
  `priority` int(11) NOT NULL,
  `position` int(11) NOT NULL,
  `base_amount` decimal(12,4) NOT NULL,
  `process` smallint(6) NOT NULL,
  `base_real_amount` decimal(12,4) NOT NULL,
  `hidden` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`tax_id`),
  KEY `IDX_ORDER_TAX` (`order_id`,`priority`,`position`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`sales_order_tax`
--

/*!40000 ALTER TABLE `sales_order_tax` DISABLE KEYS */;
LOCK TABLES `sales_order_tax` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `sales_order_tax` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`sales_payment_transaction`
--

DROP TABLE IF EXISTS `supernarede`.`sales_payment_transaction`;
CREATE TABLE  `supernarede`.`sales_payment_transaction` (
  `transaction_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `order_id` int(10) unsigned NOT NULL DEFAULT '0',
  `payment_id` int(10) unsigned NOT NULL DEFAULT '0',
  `txn_id` varchar(100) NOT NULL DEFAULT '',
  `parent_txn_id` varchar(100) DEFAULT NULL,
  `txn_type` varchar(15) NOT NULL DEFAULT '',
  `is_closed` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `additional_information` blob,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`transaction_id`),
  UNIQUE KEY `UNQ_ORDER_PAYMENT_TXN` (`order_id`,`payment_id`,`txn_id`),
  KEY `IDX_ORDER_ID` (`order_id`),
  KEY `IDX_PARENT_ID` (`parent_id`),
  KEY `IDX_PAYMENT_ID` (`payment_id`),
  CONSTRAINT `FK_SALES_PAYMENT_TRANSACTION_ORDER` FOREIGN KEY (`order_id`) REFERENCES `sales_flat_order` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_SALES_PAYMENT_TRANSACTION_PARENT` FOREIGN KEY (`parent_id`) REFERENCES `sales_payment_transaction` (`transaction_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_SALES_PAYMENT_TRANSACTION_PAYMENT` FOREIGN KEY (`payment_id`) REFERENCES `sales_flat_order_payment` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`sales_payment_transaction`
--

/*!40000 ALTER TABLE `sales_payment_transaction` DISABLE KEYS */;
LOCK TABLES `sales_payment_transaction` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `sales_payment_transaction` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`sales_recurring_profile`
--

DROP TABLE IF EXISTS `supernarede`.`sales_recurring_profile`;
CREATE TABLE  `supernarede`.`sales_recurring_profile` (
  `profile_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `state` varchar(20) NOT NULL,
  `customer_id` int(10) unsigned DEFAULT NULL,
  `store_id` smallint(5) unsigned DEFAULT NULL,
  `method_code` varchar(32) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `reference_id` varchar(32) DEFAULT NULL,
  `subscriber_name` varchar(150) DEFAULT NULL,
  `start_datetime` datetime NOT NULL,
  `internal_reference_id` varchar(42) NOT NULL,
  `schedule_description` varchar(255) NOT NULL,
  `suspension_threshold` smallint(6) unsigned DEFAULT NULL,
  `bill_failed_later` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `period_unit` varchar(20) NOT NULL,
  `period_frequency` tinyint(3) unsigned DEFAULT NULL,
  `period_max_cycles` tinyint(3) unsigned DEFAULT NULL,
  `billing_amount` double(12,4) unsigned NOT NULL DEFAULT '0.0000',
  `trial_period_unit` varchar(20) DEFAULT NULL,
  `trial_period_frequency` tinyint(3) unsigned DEFAULT NULL,
  `trial_period_max_cycles` tinyint(3) unsigned DEFAULT NULL,
  `trial_billing_amount` double(12,4) unsigned DEFAULT NULL,
  `currency_code` char(3) NOT NULL,
  `shipping_amount` decimal(12,4) unsigned DEFAULT NULL,
  `tax_amount` decimal(12,4) unsigned DEFAULT NULL,
  `init_amount` decimal(12,4) unsigned DEFAULT NULL,
  `init_may_fail` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `order_info` text NOT NULL,
  `order_item_info` text NOT NULL,
  `billing_address_info` text NOT NULL,
  `shipping_address_info` text,
  `profile_vendor_info` text,
  `additional_info` text,
  PRIMARY KEY (`profile_id`),
  UNIQUE KEY `UNQ_INTERNAL_REF_ID` (`internal_reference_id`),
  KEY `IDX_RECURRING_PROFILE_CUSTOMER` (`customer_id`),
  KEY `IDX_RECURRING_PROFILE_STORE` (`store_id`),
  CONSTRAINT `FK_RECURRING_PROFILE_CUSTOMER` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_RECURRING_PROFILE_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`sales_recurring_profile`
--

/*!40000 ALTER TABLE `sales_recurring_profile` DISABLE KEYS */;
LOCK TABLES `sales_recurring_profile` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `sales_recurring_profile` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`sales_recurring_profile_order`
--

DROP TABLE IF EXISTS `supernarede`.`sales_recurring_profile_order`;
CREATE TABLE  `supernarede`.`sales_recurring_profile_order` (
  `link_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `profile_id` int(10) unsigned NOT NULL DEFAULT '0',
  `order_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`link_id`),
  UNIQUE KEY `UNQ_PROFILE_ORDER` (`profile_id`,`order_id`),
  KEY `IDX_ORDER` (`order_id`),
  CONSTRAINT `FK_RECURRING_PROFILE_ORDER_ORDER` FOREIGN KEY (`order_id`) REFERENCES `sales_flat_order` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_RECURRING_PROFILE_ORDER_PROFILE` FOREIGN KEY (`profile_id`) REFERENCES `sales_recurring_profile` (`profile_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`sales_recurring_profile_order`
--

/*!40000 ALTER TABLE `sales_recurring_profile_order` DISABLE KEYS */;
LOCK TABLES `sales_recurring_profile_order` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `sales_recurring_profile_order` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`sales_refunded_aggregated`
--

DROP TABLE IF EXISTS `supernarede`.`sales_refunded_aggregated`;
CREATE TABLE  `supernarede`.`sales_refunded_aggregated` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `period` date NOT NULL DEFAULT '0000-00-00',
  `store_id` smallint(5) unsigned DEFAULT NULL,
  `order_status` varchar(50) NOT NULL DEFAULT '',
  `orders_count` int(11) NOT NULL DEFAULT '0',
  `refunded` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `online_refunded` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `offline_refunded` decimal(12,4) NOT NULL DEFAULT '0.0000',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNQ_PERIOD_STORE_ORDER_STATUS` (`period`,`store_id`,`order_status`),
  KEY `IDX_STORE_ID` (`store_id`),
  CONSTRAINT `FK_SALES_REFUNDED_AGGREGATED_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`sales_refunded_aggregated`
--

/*!40000 ALTER TABLE `sales_refunded_aggregated` DISABLE KEYS */;
LOCK TABLES `sales_refunded_aggregated` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `sales_refunded_aggregated` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`sales_refunded_aggregated_order`
--

DROP TABLE IF EXISTS `supernarede`.`sales_refunded_aggregated_order`;
CREATE TABLE  `supernarede`.`sales_refunded_aggregated_order` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `period` date NOT NULL DEFAULT '0000-00-00',
  `store_id` smallint(5) unsigned DEFAULT NULL,
  `order_status` varchar(50) NOT NULL DEFAULT '',
  `orders_count` int(11) NOT NULL DEFAULT '0',
  `refunded` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `online_refunded` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `offline_refunded` decimal(12,4) NOT NULL DEFAULT '0.0000',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNQ_PERIOD_STORE_ORDER_STATUS` (`period`,`store_id`,`order_status`),
  KEY `IDX_STORE_ID` (`store_id`),
  CONSTRAINT `FK_SALES_REFUNDED_AGGREGATED_ORDER_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`sales_refunded_aggregated_order`
--

/*!40000 ALTER TABLE `sales_refunded_aggregated_order` DISABLE KEYS */;
LOCK TABLES `sales_refunded_aggregated_order` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `sales_refunded_aggregated_order` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`sales_shipping_aggregated`
--

DROP TABLE IF EXISTS `supernarede`.`sales_shipping_aggregated`;
CREATE TABLE  `supernarede`.`sales_shipping_aggregated` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `period` date NOT NULL DEFAULT '0000-00-00',
  `store_id` smallint(5) unsigned DEFAULT NULL,
  `order_status` varchar(50) NOT NULL DEFAULT '',
  `shipping_description` varchar(255) NOT NULL DEFAULT '',
  `orders_count` int(11) NOT NULL DEFAULT '0',
  `total_shipping` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `total_shipping_actual` decimal(12,4) NOT NULL DEFAULT '0.0000',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNQ_PERIOD_STORE_ORDER_STATUS` (`period`,`store_id`,`order_status`,`shipping_description`),
  KEY `IDX_STORE_ID` (`store_id`),
  CONSTRAINT `FK_SALES_SHIPPING_AGGREGATED_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`sales_shipping_aggregated`
--

/*!40000 ALTER TABLE `sales_shipping_aggregated` DISABLE KEYS */;
LOCK TABLES `sales_shipping_aggregated` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `sales_shipping_aggregated` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`sales_shipping_aggregated_order`
--

DROP TABLE IF EXISTS `supernarede`.`sales_shipping_aggregated_order`;
CREATE TABLE  `supernarede`.`sales_shipping_aggregated_order` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `period` date NOT NULL DEFAULT '0000-00-00',
  `store_id` smallint(5) unsigned DEFAULT NULL,
  `order_status` varchar(50) NOT NULL DEFAULT '',
  `shipping_description` varchar(255) NOT NULL DEFAULT '',
  `orders_count` int(11) NOT NULL DEFAULT '0',
  `total_shipping` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `total_shipping_actual` decimal(12,4) NOT NULL DEFAULT '0.0000',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNQ_PERIOD_STORE_ORDER_STATUS` (`period`,`store_id`,`order_status`,`shipping_description`),
  KEY `IDX_STORE_ID` (`store_id`),
  CONSTRAINT `FK_SALES_SHIPPING_AGGREGATED_ORDER_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`sales_shipping_aggregated_order`
--

/*!40000 ALTER TABLE `sales_shipping_aggregated_order` DISABLE KEYS */;
LOCK TABLES `sales_shipping_aggregated_order` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `sales_shipping_aggregated_order` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`salesrule`
--

DROP TABLE IF EXISTS `supernarede`.`salesrule`;
CREATE TABLE  `supernarede`.`salesrule` (
  `rule_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `from_date` date DEFAULT '0000-00-00',
  `to_date` date DEFAULT '0000-00-00',
  `uses_per_customer` int(11) NOT NULL DEFAULT '0',
  `customer_group_ids` text,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `conditions_serialized` mediumtext NOT NULL,
  `actions_serialized` mediumtext NOT NULL,
  `stop_rules_processing` tinyint(1) NOT NULL DEFAULT '1',
  `is_advanced` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `product_ids` text,
  `sort_order` int(10) unsigned NOT NULL DEFAULT '0',
  `simple_action` varchar(32) NOT NULL DEFAULT '',
  `discount_amount` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `discount_qty` decimal(12,4) unsigned DEFAULT NULL,
  `discount_step` int(10) unsigned NOT NULL,
  `simple_free_shipping` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `apply_to_shipping` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `times_used` int(11) unsigned NOT NULL DEFAULT '0',
  `is_rss` tinyint(4) NOT NULL DEFAULT '0',
  `website_ids` text,
  `coupon_type` smallint(5) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`rule_id`),
  KEY `sort_order` (`is_active`,`sort_order`,`to_date`,`from_date`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`salesrule`
--

/*!40000 ALTER TABLE `salesrule` DISABLE KEYS */;
LOCK TABLES `salesrule` WRITE;
INSERT INTO `supernarede`.`salesrule` VALUES  (1,'Leva Bavaria!','','2011-08-18','2011-08-25',0,'0,1,2,3',1,'a:6:{s:4:\"type\";s:32:\"salesrule/rule_condition_combine\";s:9:\"attribute\";N;s:8:\"operator\";N;s:5:\"value\";s:1:\"1\";s:18:\"is_value_processed\";N;s:10:\"aggregator\";s:3:\"all\";}','a:7:{s:4:\"type\";s:40:\"salesrule/rule_condition_product_combine\";s:9:\"attribute\";N;s:8:\"operator\";N;s:5:\"value\";s:1:\"1\";s:18:\"is_value_processed\";N;s:10:\"aggregator\";s:3:\"all\";s:10:\"conditions\";a:1:{i:0;a:5:{s:4:\"type\";s:32:\"salesrule/rule_condition_product\";s:9:\"attribute\";s:13:\"cerveja_marca\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:2:\"10\";s:18:\"is_value_processed\";b:0;}}}',1,1,'',0,'by_percent','50.0000',NULL,0,0,0,0,1,'1',1);
UNLOCK TABLES;
/*!40000 ALTER TABLE `salesrule` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`salesrule_coupon`
--

DROP TABLE IF EXISTS `supernarede`.`salesrule_coupon`;
CREATE TABLE  `supernarede`.`salesrule_coupon` (
  `coupon_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rule_id` int(10) unsigned NOT NULL,
  `code` varchar(255) NOT NULL,
  `usage_limit` int(10) unsigned DEFAULT NULL,
  `usage_per_customer` int(10) unsigned DEFAULT NULL,
  `times_used` int(10) unsigned NOT NULL DEFAULT '0',
  `expiration_date` datetime DEFAULT NULL,
  `is_primary` tinyint(1) unsigned DEFAULT NULL,
  PRIMARY KEY (`coupon_id`),
  UNIQUE KEY `UNQ_COUPON_CODE` (`code`),
  UNIQUE KEY `UNQ_RULE_MAIN_COUPON` (`rule_id`,`is_primary`),
  KEY `FK_SALESRULE_COUPON_RULE_ID_SALESRULE` (`rule_id`),
  CONSTRAINT `FK_SALESRULE_COUPON_RULE_ID_SALESRULE` FOREIGN KEY (`rule_id`) REFERENCES `salesrule` (`rule_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`salesrule_coupon`
--

/*!40000 ALTER TABLE `salesrule_coupon` DISABLE KEYS */;
LOCK TABLES `salesrule_coupon` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `salesrule_coupon` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`salesrule_coupon_usage`
--

DROP TABLE IF EXISTS `supernarede`.`salesrule_coupon_usage`;
CREATE TABLE  `supernarede`.`salesrule_coupon_usage` (
  `coupon_id` int(10) unsigned NOT NULL,
  `customer_id` int(10) unsigned NOT NULL,
  `times_used` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`coupon_id`,`customer_id`),
  KEY `FK_SALESRULE_COUPON_CUSTOMER_COUPON_ID_CUSTOMER_ENTITY` (`coupon_id`),
  KEY `FK_SALESRULE_COUPON_CUSTOMER_CUSTOMER_ID_CUSTOMER_ENTITY` (`customer_id`),
  CONSTRAINT `FK_SALESRULE_CPN_CUST_CPN_ID_CUST_ENTITY` FOREIGN KEY (`coupon_id`) REFERENCES `salesrule_coupon` (`coupon_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_SALESRULE_CPN_CUST_CUST_ID_CUST_ENTITY` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`salesrule_coupon_usage`
--

/*!40000 ALTER TABLE `salesrule_coupon_usage` DISABLE KEYS */;
LOCK TABLES `salesrule_coupon_usage` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `salesrule_coupon_usage` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`salesrule_customer`
--

DROP TABLE IF EXISTS `supernarede`.`salesrule_customer`;
CREATE TABLE  `supernarede`.`salesrule_customer` (
  `rule_customer_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rule_id` int(10) unsigned NOT NULL DEFAULT '0',
  `customer_id` int(10) unsigned NOT NULL DEFAULT '0',
  `times_used` smallint(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`rule_customer_id`),
  KEY `rule_id` (`rule_id`,`customer_id`),
  KEY `customer_id` (`customer_id`,`rule_id`),
  CONSTRAINT `FK_salesrule_customer_id` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_salesrule_customer_rule` FOREIGN KEY (`rule_id`) REFERENCES `salesrule` (`rule_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`salesrule_customer`
--

/*!40000 ALTER TABLE `salesrule_customer` DISABLE KEYS */;
LOCK TABLES `salesrule_customer` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `salesrule_customer` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`salesrule_label`
--

DROP TABLE IF EXISTS `supernarede`.`salesrule_label`;
CREATE TABLE  `supernarede`.`salesrule_label` (
  `label_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rule_id` int(10) unsigned NOT NULL,
  `store_id` smallint(5) unsigned NOT NULL,
  `label` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`label_id`),
  UNIQUE KEY `IDX_RULE_STORE` (`rule_id`,`store_id`),
  KEY `FK_SALESRULE_LABEL_STORE` (`store_id`),
  KEY `FK_SALESRULE_LABEL_RULE` (`rule_id`),
  CONSTRAINT `FK_SALESRULE_LABEL_RULE` FOREIGN KEY (`rule_id`) REFERENCES `salesrule` (`rule_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_SALESRULE_LABEL_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`salesrule_label`
--

/*!40000 ALTER TABLE `salesrule_label` DISABLE KEYS */;
LOCK TABLES `salesrule_label` WRITE;
INSERT INTO `supernarede`.`salesrule_label` VALUES  (1,1,0,'Promo Bavaria'),
 (2,1,1,'Promo Bavaria');
UNLOCK TABLES;
/*!40000 ALTER TABLE `salesrule_label` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`salesrule_product_attribute`
--

DROP TABLE IF EXISTS `supernarede`.`salesrule_product_attribute`;
CREATE TABLE  `supernarede`.`salesrule_product_attribute` (
  `rule_id` int(10) unsigned NOT NULL,
  `website_id` smallint(5) unsigned NOT NULL,
  `customer_group_id` smallint(5) unsigned NOT NULL,
  `attribute_id` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`rule_id`,`website_id`,`customer_group_id`,`attribute_id`),
  KEY `IDX_WEBSITE` (`website_id`),
  KEY `IDX_CUSTOMER_GROUP` (`customer_group_id`),
  KEY `IDX_ATTRIBUTE` (`attribute_id`),
  CONSTRAINT `FK_SALESRULE_PRODUCT_ATTRIBUTE_ATTRIBUTE` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  CONSTRAINT `FK_SALESRULE_PRODUCT_ATTRIBUTE_CUSTOMER_GROUP` FOREIGN KEY (`customer_group_id`) REFERENCES `customer_group` (`customer_group_id`) ON DELETE CASCADE,
  CONSTRAINT `FK_SALESRULE_PRODUCT_ATTRIBUTE_RULE` FOREIGN KEY (`rule_id`) REFERENCES `salesrule` (`rule_id`) ON DELETE CASCADE,
  CONSTRAINT `FK_SALESRULE_PRODUCT_ATTRIBUTE_WEBSITE` FOREIGN KEY (`website_id`) REFERENCES `core_website` (`website_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`salesrule_product_attribute`
--

/*!40000 ALTER TABLE `salesrule_product_attribute` DISABLE KEYS */;
LOCK TABLES `salesrule_product_attribute` WRITE;
INSERT INTO `supernarede`.`salesrule_product_attribute` VALUES  (1,1,0,124),
 (1,1,1,124),
 (1,1,2,124),
 (1,1,3,124);
UNLOCK TABLES;
/*!40000 ALTER TABLE `salesrule_product_attribute` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`sendfriend_log`
--

DROP TABLE IF EXISTS `supernarede`.`sendfriend_log`;
CREATE TABLE  `supernarede`.`sendfriend_log` (
  `log_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ip` bigint(20) NOT NULL DEFAULT '0',
  `time` int(10) unsigned NOT NULL,
  `website_id` smallint(5) NOT NULL,
  PRIMARY KEY (`log_id`),
  KEY `IDX_REMOTE_ADDR` (`ip`),
  KEY `IDX_LOG_TIME` (`time`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Send to friend function log storage table';

--
-- Dumping data for table `supernarede`.`sendfriend_log`
--

/*!40000 ALTER TABLE `sendfriend_log` DISABLE KEYS */;
LOCK TABLES `sendfriend_log` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `sendfriend_log` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`shipping_tablerate`
--

DROP TABLE IF EXISTS `supernarede`.`shipping_tablerate`;
CREATE TABLE  `supernarede`.`shipping_tablerate` (
  `pk` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `website_id` int(11) NOT NULL DEFAULT '0',
  `dest_country_id` varchar(4) NOT NULL DEFAULT '0',
  `dest_region_id` int(10) NOT NULL DEFAULT '0',
  `dest_zip` varchar(10) NOT NULL DEFAULT '',
  `condition_name` varchar(20) NOT NULL DEFAULT '',
  `condition_value` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `price` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `cost` decimal(12,4) NOT NULL DEFAULT '0.0000',
  PRIMARY KEY (`pk`),
  UNIQUE KEY `dest_country` (`website_id`,`dest_country_id`,`dest_region_id`,`dest_zip`,`condition_name`,`condition_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`shipping_tablerate`
--

/*!40000 ALTER TABLE `shipping_tablerate` DISABLE KEYS */;
LOCK TABLES `shipping_tablerate` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `shipping_tablerate` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`sitemap`
--

DROP TABLE IF EXISTS `supernarede`.`sitemap`;
CREATE TABLE  `supernarede`.`sitemap` (
  `sitemap_id` int(11) NOT NULL AUTO_INCREMENT,
  `sitemap_type` varchar(32) DEFAULT NULL,
  `sitemap_filename` varchar(32) DEFAULT NULL,
  `sitemap_path` tinytext,
  `sitemap_time` timestamp NULL DEFAULT NULL,
  `store_id` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`sitemap_id`),
  KEY `FK_SITEMAP_STORE` (`store_id`),
  CONSTRAINT `FK_SITEMAP_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`sitemap`
--

/*!40000 ALTER TABLE `sitemap` DISABLE KEYS */;
LOCK TABLES `sitemap` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `sitemap` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`tag`
--

DROP TABLE IF EXISTS `supernarede`.`tag`;
CREATE TABLE  `supernarede`.`tag` (
  `tag_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `status` smallint(6) NOT NULL DEFAULT '0',
  `first_customer_id` int(10) unsigned NOT NULL DEFAULT '0',
  `first_store_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`tag`
--

/*!40000 ALTER TABLE `tag` DISABLE KEYS */;
LOCK TABLES `tag` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `tag` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`tag_properties`
--

DROP TABLE IF EXISTS `supernarede`.`tag_properties`;
CREATE TABLE  `supernarede`.`tag_properties` (
  `tag_id` int(11) unsigned NOT NULL DEFAULT '0',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `base_popularity` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`tag_id`,`store_id`),
  KEY `FK_TAG_PROPERTIES_STORE` (`store_id`),
  CONSTRAINT `FK_TAG_PROPERTIES_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_TAG_PROPERTIES_TAG` FOREIGN KEY (`tag_id`) REFERENCES `tag` (`tag_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`tag_properties`
--

/*!40000 ALTER TABLE `tag_properties` DISABLE KEYS */;
LOCK TABLES `tag_properties` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `tag_properties` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`tag_relation`
--

DROP TABLE IF EXISTS `supernarede`.`tag_relation`;
CREATE TABLE  `supernarede`.`tag_relation` (
  `tag_relation_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tag_id` int(11) unsigned NOT NULL DEFAULT '0',
  `customer_id` int(10) unsigned DEFAULT NULL,
  `product_id` int(11) unsigned NOT NULL DEFAULT '0',
  `store_id` smallint(6) unsigned NOT NULL DEFAULT '1',
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`tag_relation_id`),
  UNIQUE KEY `UNQ_TAG_CUSTOMER_PRODUCT_STORE` (`tag_id`,`customer_id`,`product_id`,`store_id`),
  KEY `IDX_PRODUCT` (`product_id`),
  KEY `IDX_TAG` (`tag_id`),
  KEY `IDX_CUSTOMER` (`customer_id`),
  KEY `IDX_STORE` (`store_id`),
  CONSTRAINT `FK_TAG_RELATION_CUSTOMER` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_TAG_RELATION_PRODUCT` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_TAG_RELATION_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_TAG_RELATION_TAG` FOREIGN KEY (`tag_id`) REFERENCES `tag` (`tag_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`tag_relation`
--

/*!40000 ALTER TABLE `tag_relation` DISABLE KEYS */;
LOCK TABLES `tag_relation` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `tag_relation` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`tag_summary`
--

DROP TABLE IF EXISTS `supernarede`.`tag_summary`;
CREATE TABLE  `supernarede`.`tag_summary` (
  `tag_id` int(11) unsigned NOT NULL DEFAULT '0',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `customers` int(11) unsigned NOT NULL DEFAULT '0',
  `products` int(11) unsigned NOT NULL DEFAULT '0',
  `uses` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'deprecated since 1.4.0.1',
  `historical_uses` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'deprecated since 1.4.0.1',
  `popularity` int(11) unsigned NOT NULL DEFAULT '0',
  `base_popularity` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'deprecated since 1.4.0.1',
  PRIMARY KEY (`tag_id`,`store_id`),
  KEY `FK_TAG_SUMMARY_STORE` (`store_id`),
  KEY `IDX_TAG` (`tag_id`),
  CONSTRAINT `FK_TAG_SUMMARY_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_TAG_SUMMARY_TAG` FOREIGN KEY (`tag_id`) REFERENCES `tag` (`tag_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`tag_summary`
--

/*!40000 ALTER TABLE `tag_summary` DISABLE KEYS */;
LOCK TABLES `tag_summary` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `tag_summary` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`tax_calculation`
--

DROP TABLE IF EXISTS `supernarede`.`tax_calculation`;
CREATE TABLE  `supernarede`.`tax_calculation` (
  `tax_calculation_rate_id` int(11) NOT NULL,
  `tax_calculation_rule_id` int(11) NOT NULL,
  `customer_tax_class_id` smallint(6) NOT NULL,
  `product_tax_class_id` smallint(6) NOT NULL,
  KEY `FK_TAX_CALCULATION_RULE` (`tax_calculation_rule_id`),
  KEY `FK_TAX_CALCULATION_RATE` (`tax_calculation_rate_id`),
  KEY `FK_TAX_CALCULATION_CTC` (`customer_tax_class_id`),
  KEY `FK_TAX_CALCULATION_PTC` (`product_tax_class_id`),
  KEY `IDX_TAX_CALCULATION` (`tax_calculation_rate_id`,`customer_tax_class_id`,`product_tax_class_id`),
  CONSTRAINT `FK_TAX_CALCULATION_CTC` FOREIGN KEY (`customer_tax_class_id`) REFERENCES `tax_class` (`class_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_TAX_CALCULATION_PTC` FOREIGN KEY (`product_tax_class_id`) REFERENCES `tax_class` (`class_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_TAX_CALCULATION_RATE` FOREIGN KEY (`tax_calculation_rate_id`) REFERENCES `tax_calculation_rate` (`tax_calculation_rate_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_TAX_CALCULATION_RULE` FOREIGN KEY (`tax_calculation_rule_id`) REFERENCES `tax_calculation_rule` (`tax_calculation_rule_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`tax_calculation`
--

/*!40000 ALTER TABLE `tax_calculation` DISABLE KEYS */;
LOCK TABLES `tax_calculation` WRITE;
INSERT INTO `supernarede`.`tax_calculation` VALUES  (1,1,3,2),
 (2,1,3,2);
UNLOCK TABLES;
/*!40000 ALTER TABLE `tax_calculation` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`tax_calculation_rate`
--

DROP TABLE IF EXISTS `supernarede`.`tax_calculation_rate`;
CREATE TABLE  `supernarede`.`tax_calculation_rate` (
  `tax_calculation_rate_id` int(11) NOT NULL AUTO_INCREMENT,
  `tax_country_id` char(2) NOT NULL,
  `tax_region_id` mediumint(9) NOT NULL,
  `tax_postcode` varchar(21) NOT NULL,
  `code` varchar(255) NOT NULL,
  `rate` decimal(12,4) NOT NULL,
  `zip_is_range` tinyint(1) DEFAULT NULL,
  `zip_from` int(11) unsigned DEFAULT NULL,
  `zip_to` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`tax_calculation_rate_id`),
  KEY `IDX_TAX_CALCULATION_RATE` (`tax_country_id`,`tax_region_id`,`tax_postcode`),
  KEY `IDX_TAX_CALCULATION_RATE_CODE` (`code`),
  KEY `IDX_TAX_CALCULATION_RATE_RANGE` (`tax_calculation_rate_id`,`tax_country_id`,`tax_region_id`,`zip_is_range`,`tax_postcode`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`tax_calculation_rate`
--

/*!40000 ALTER TABLE `tax_calculation_rate` DISABLE KEYS */;
LOCK TABLES `tax_calculation_rate` WRITE;
INSERT INTO `supernarede`.`tax_calculation_rate` VALUES  (1,'US',12,'*','US-CA-*-Rate 1','8.2500',NULL,NULL,NULL),
 (2,'US',43,'*','US-NY-*-Rate 1','8.3750',NULL,NULL,NULL);
UNLOCK TABLES;
/*!40000 ALTER TABLE `tax_calculation_rate` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`tax_calculation_rate_title`
--

DROP TABLE IF EXISTS `supernarede`.`tax_calculation_rate_title`;
CREATE TABLE  `supernarede`.`tax_calculation_rate_title` (
  `tax_calculation_rate_title_id` int(11) NOT NULL AUTO_INCREMENT,
  `tax_calculation_rate_id` int(11) NOT NULL,
  `store_id` smallint(5) unsigned NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`tax_calculation_rate_title_id`),
  KEY `IDX_TAX_CALCULATION_RATE_TITLE` (`tax_calculation_rate_id`,`store_id`),
  KEY `FK_TAX_CALCULATION_RATE_TITLE_RATE` (`tax_calculation_rate_id`),
  KEY `FK_TAX_CALCULATION_RATE_TITLE_STORE` (`store_id`),
  CONSTRAINT `FK_TAX_CALCULATION_RATE_TITLE_RATE` FOREIGN KEY (`tax_calculation_rate_id`) REFERENCES `tax_calculation_rate` (`tax_calculation_rate_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_TAX_CALCULATION_RATE_TITLE_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`tax_calculation_rate_title`
--

/*!40000 ALTER TABLE `tax_calculation_rate_title` DISABLE KEYS */;
LOCK TABLES `tax_calculation_rate_title` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `tax_calculation_rate_title` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`tax_calculation_rule`
--

DROP TABLE IF EXISTS `supernarede`.`tax_calculation_rule`;
CREATE TABLE  `supernarede`.`tax_calculation_rule` (
  `tax_calculation_rule_id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) NOT NULL,
  `priority` mediumint(9) NOT NULL,
  `position` mediumint(9) NOT NULL,
  PRIMARY KEY (`tax_calculation_rule_id`),
  KEY `IDX_TAX_CALCULATION_RULE` (`priority`,`position`,`tax_calculation_rule_id`),
  KEY `IDX_TAX_CALCULATION_RULE_CODE` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`tax_calculation_rule`
--

/*!40000 ALTER TABLE `tax_calculation_rule` DISABLE KEYS */;
LOCK TABLES `tax_calculation_rule` WRITE;
INSERT INTO `supernarede`.`tax_calculation_rule` VALUES  (1,'Retail Customer-Taxable Goods-Rate 1',1,1);
UNLOCK TABLES;
/*!40000 ALTER TABLE `tax_calculation_rule` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`tax_class`
--

DROP TABLE IF EXISTS `supernarede`.`tax_class`;
CREATE TABLE  `supernarede`.`tax_class` (
  `class_id` smallint(6) NOT NULL AUTO_INCREMENT,
  `class_name` varchar(255) NOT NULL DEFAULT '',
  `class_type` enum('CUSTOMER','PRODUCT') NOT NULL DEFAULT 'CUSTOMER',
  PRIMARY KEY (`class_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`tax_class`
--

/*!40000 ALTER TABLE `tax_class` DISABLE KEYS */;
LOCK TABLES `tax_class` WRITE;
INSERT INTO `supernarede`.`tax_class` VALUES  (2,'Taxable Goods','PRODUCT'),
 (3,'Retail Customer','CUSTOMER'),
 (4,'Shipping','PRODUCT');
UNLOCK TABLES;
/*!40000 ALTER TABLE `tax_class` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`tax_order_aggregated_created`
--

DROP TABLE IF EXISTS `supernarede`.`tax_order_aggregated_created`;
CREATE TABLE  `supernarede`.`tax_order_aggregated_created` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `period` date NOT NULL DEFAULT '0000-00-00',
  `store_id` smallint(5) unsigned DEFAULT NULL,
  `code` varchar(255) NOT NULL DEFAULT '',
  `order_status` varchar(50) NOT NULL DEFAULT '',
  `percent` float(12,4) NOT NULL DEFAULT '0.0000',
  `orders_count` int(11) unsigned NOT NULL DEFAULT '0',
  `tax_base_amount_sum` float(12,4) NOT NULL DEFAULT '0.0000',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNQ_PERIOD_STORE_CODE_ORDER_STATUS` (`period`,`store_id`,`code`,`percent`,`order_status`),
  KEY `IDX_STORE_ID` (`store_id`),
  CONSTRAINT `FK_TAX_ORDER_AGGREGATED_CREATED_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`tax_order_aggregated_created`
--

/*!40000 ALTER TABLE `tax_order_aggregated_created` DISABLE KEYS */;
LOCK TABLES `tax_order_aggregated_created` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `tax_order_aggregated_created` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`weee_discount`
--

DROP TABLE IF EXISTS `supernarede`.`weee_discount`;
CREATE TABLE  `supernarede`.`weee_discount` (
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0',
  `website_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `customer_group_id` smallint(5) unsigned NOT NULL,
  `value` decimal(12,4) NOT NULL DEFAULT '0.0000',
  KEY `FK_CATALOG_PRODUCT_ENTITY_WEEE_DISCOUNT_WEBSITE` (`website_id`),
  KEY `FK_CATALOG_PRODUCT_ENTITY_WEEE_DISCOUNT_PRODUCT_ENTITY` (`entity_id`),
  KEY `FK_CATALOG_PRODUCT_ENTITY_WEEE_DISCOUNT_GROUP` (`customer_group_id`),
  CONSTRAINT `FK_CATALOG_PRODUCT_ENTITY_WEEE_DISCOUNT_GROUP` FOREIGN KEY (`customer_group_id`) REFERENCES `customer_group` (`customer_group_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATALOG_PRODUCT_ENTITY_WEEE_DISCOUNT_PRODUCT_ENTITY` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATALOG_PRODUCT_ENTITY_WEEE_DISCOUNT_WEBSITE` FOREIGN KEY (`website_id`) REFERENCES `core_website` (`website_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`weee_discount`
--

/*!40000 ALTER TABLE `weee_discount` DISABLE KEYS */;
LOCK TABLES `weee_discount` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `weee_discount` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`weee_tax`
--

DROP TABLE IF EXISTS `supernarede`.`weee_tax`;
CREATE TABLE  `supernarede`.`weee_tax` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT,
  `website_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0',
  `country` varchar(2) NOT NULL DEFAULT '',
  `value` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `state` varchar(255) NOT NULL DEFAULT '*',
  `attribute_id` smallint(5) unsigned NOT NULL,
  `entity_type_id` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`value_id`),
  KEY `FK_CATALOG_PRODUCT_ENTITY_WEEE_TAX_WEBSITE` (`website_id`),
  KEY `FK_CATALOG_PRODUCT_ENTITY_WEEE_TAX_PRODUCT_ENTITY` (`entity_id`),
  KEY `FK_CATALOG_PRODUCT_ENTITY_WEEE_TAX_COUNTRY` (`country`),
  KEY `FK_WEEE_TAX_ATTRIBUTE_ID` (`attribute_id`),
  CONSTRAINT `FK_CATALOG_PRODUCT_ENTITY_WEEE_TAX_COUNTRY` FOREIGN KEY (`country`) REFERENCES `directory_country` (`country_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATALOG_PRODUCT_ENTITY_WEEE_TAX_PRODUCT_ENTITY` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATALOG_PRODUCT_ENTITY_WEEE_TAX_WEBSITE` FOREIGN KEY (`website_id`) REFERENCES `core_website` (`website_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_WEEE_TAX_ATTRIBUTE_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`weee_tax`
--

/*!40000 ALTER TABLE `weee_tax` DISABLE KEYS */;
LOCK TABLES `weee_tax` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `weee_tax` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`widget`
--

DROP TABLE IF EXISTS `supernarede`.`widget`;
CREATE TABLE  `supernarede`.`widget` (
  `widget_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `parameters` text,
  PRIMARY KEY (`widget_id`),
  KEY `IDX_CODE` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Preconfigured Widgets';

--
-- Dumping data for table `supernarede`.`widget`
--

/*!40000 ALTER TABLE `widget` DISABLE KEYS */;
LOCK TABLES `widget` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `widget` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`widget_instance`
--

DROP TABLE IF EXISTS `supernarede`.`widget_instance`;
CREATE TABLE  `supernarede`.`widget_instance` (
  `instance_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(255) NOT NULL DEFAULT '',
  `package_theme` varchar(255) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL DEFAULT '',
  `store_ids` varchar(255) NOT NULL DEFAULT '0',
  `widget_parameters` text,
  `sort_order` smallint(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`instance_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`widget_instance`
--

/*!40000 ALTER TABLE `widget_instance` DISABLE KEYS */;
LOCK TABLES `widget_instance` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `widget_instance` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`widget_instance_page`
--

DROP TABLE IF EXISTS `supernarede`.`widget_instance_page`;
CREATE TABLE  `supernarede`.`widget_instance_page` (
  `page_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `instance_id` int(11) unsigned NOT NULL DEFAULT '0',
  `group` varchar(25) NOT NULL DEFAULT '',
  `layout_handle` varchar(255) NOT NULL DEFAULT '',
  `block_reference` varchar(255) NOT NULL DEFAULT '',
  `for` varchar(25) NOT NULL DEFAULT '',
  `entities` text,
  `template` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`page_id`),
  KEY `IDX_WIDGET_WIDGET_INSTANCE_ID` (`instance_id`),
  CONSTRAINT `FK_WIDGET_WIDGET_INSTANCE_ID` FOREIGN KEY (`instance_id`) REFERENCES `widget_instance` (`instance_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`widget_instance_page`
--

/*!40000 ALTER TABLE `widget_instance_page` DISABLE KEYS */;
LOCK TABLES `widget_instance_page` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `widget_instance_page` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`widget_instance_page_layout`
--

DROP TABLE IF EXISTS `supernarede`.`widget_instance_page_layout`;
CREATE TABLE  `supernarede`.`widget_instance_page_layout` (
  `page_id` int(11) unsigned NOT NULL DEFAULT '0',
  `layout_update_id` int(10) unsigned NOT NULL DEFAULT '0',
  UNIQUE KEY `page_id` (`page_id`,`layout_update_id`),
  KEY `IDX_WIDGET_WIDGET_INSTANCE_PAGE_ID` (`page_id`),
  KEY `IDX_WIDGET_WIDGET_INSTANCE_LAYOUT_UPDATE_ID` (`layout_update_id`),
  CONSTRAINT `FK_WIDGET_WIDGET_INSTANCE_LAYOUT_UPDATE_ID` FOREIGN KEY (`layout_update_id`) REFERENCES `core_layout_update` (`layout_update_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_WIDGET_WIDGET_INSTANCE_PAGE_ID` FOREIGN KEY (`page_id`) REFERENCES `widget_instance_page` (`page_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`widget_instance_page_layout`
--

/*!40000 ALTER TABLE `widget_instance_page_layout` DISABLE KEYS */;
LOCK TABLES `widget_instance_page_layout` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `widget_instance_page_layout` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`wishlist`
--

DROP TABLE IF EXISTS `supernarede`.`wishlist`;
CREATE TABLE  `supernarede`.`wishlist` (
  `wishlist_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(10) unsigned NOT NULL DEFAULT '0',
  `shared` tinyint(1) unsigned DEFAULT '0',
  `sharing_code` varchar(32) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`wishlist_id`),
  UNIQUE KEY `UNQ_CUSTOMER` (`customer_id`),
  KEY `IDX_IS_SHARED` (`shared`),
  CONSTRAINT `FK_WISHLIST_CUSTOMER` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='Wishlist main';

--
-- Dumping data for table `supernarede`.`wishlist`
--

/*!40000 ALTER TABLE `wishlist` DISABLE KEYS */;
LOCK TABLES `wishlist` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `wishlist` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`wishlist_item`
--

DROP TABLE IF EXISTS `supernarede`.`wishlist_item`;
CREATE TABLE  `supernarede`.`wishlist_item` (
  `wishlist_item_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `wishlist_id` int(10) unsigned NOT NULL DEFAULT '0',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0',
  `store_id` smallint(5) unsigned DEFAULT NULL,
  `added_at` datetime DEFAULT NULL,
  `description` text,
  `qty` decimal(12,4) NOT NULL,
  PRIMARY KEY (`wishlist_item_id`),
  KEY `IDX_WISHLIST` (`wishlist_id`),
  KEY `IDX_PRODUCT` (`product_id`),
  KEY `IDX_STORE` (`store_id`),
  CONSTRAINT `FK_WISHLIST_ITEM_PRODUCT` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_WISHLIST_ITEM_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_WISHLIST_ITEM_WISHLIST` FOREIGN KEY (`wishlist_id`) REFERENCES `wishlist` (`wishlist_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Wishlist items';

--
-- Dumping data for table `supernarede`.`wishlist_item`
--

/*!40000 ALTER TABLE `wishlist_item` DISABLE KEYS */;
LOCK TABLES `wishlist_item` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `wishlist_item` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`wishlist_item_option`
--

DROP TABLE IF EXISTS `supernarede`.`wishlist_item_option`;
CREATE TABLE  `supernarede`.`wishlist_item_option` (
  `option_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `wishlist_item_id` int(10) unsigned NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `code` varchar(255) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`option_id`),
  KEY `FK_WISHLIST_ITEM_OPTION_ITEM_ID` (`wishlist_item_id`),
  CONSTRAINT `FK_WISHLIST_ITEM_OPTION_ITEM_ID` FOREIGN KEY (`wishlist_item_id`) REFERENCES `wishlist_item` (`wishlist_item_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Additional options for wishlist item';

--
-- Dumping data for table `supernarede`.`wishlist_item_option`
--

/*!40000 ALTER TABLE `wishlist_item_option` DISABLE KEYS */;
LOCK TABLES `wishlist_item_option` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `wishlist_item_option` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`xmlconnect_application`
--

DROP TABLE IF EXISTS `supernarede`.`xmlconnect_application`;
CREATE TABLE  `supernarede`.`xmlconnect_application` (
  `application_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `code` varchar(32) NOT NULL,
  `type` varchar(32) NOT NULL,
  `store_id` smallint(5) unsigned DEFAULT NULL,
  `active_from` date DEFAULT NULL,
  `active_to` date DEFAULT NULL,
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `configuration` blob,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `browsing_mode` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`application_id`),
  UNIQUE KEY `UNQ_XMLCONNECT_APPLICATION_CODE` (`code`),
  KEY `FK_XMLCONNECT_APPLICAION_STORE` (`store_id`),
  CONSTRAINT `FK_XMLCONNECT_APPLICAION_STORE` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`xmlconnect_application`
--

/*!40000 ALTER TABLE `xmlconnect_application` DISABLE KEYS */;
LOCK TABLES `xmlconnect_application` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `xmlconnect_application` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`xmlconnect_history`
--

DROP TABLE IF EXISTS `supernarede`.`xmlconnect_history`;
CREATE TABLE  `supernarede`.`xmlconnect_history` (
  `history_id` int(11) NOT NULL AUTO_INCREMENT,
  `application_id` smallint(5) unsigned NOT NULL,
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `store_id` smallint(5) unsigned DEFAULT NULL,
  `params` blob,
  `title` varchar(200) NOT NULL,
  `activation_key` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  PRIMARY KEY (`history_id`),
  KEY `FK_XMLCONNECT_HISTORY_APPLICATION` (`application_id`),
  CONSTRAINT `FK_XMLCONNECT_HISTORY_APPLICATION` FOREIGN KEY (`application_id`) REFERENCES `xmlconnect_application` (`application_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`xmlconnect_history`
--

/*!40000 ALTER TABLE `xmlconnect_history` DISABLE KEYS */;
LOCK TABLES `xmlconnect_history` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `xmlconnect_history` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`xmlconnect_notification_template`
--

DROP TABLE IF EXISTS `supernarede`.`xmlconnect_notification_template`;
CREATE TABLE  `supernarede`.`xmlconnect_notification_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_code` varchar(32) NOT NULL,
  `name` varchar(255) NOT NULL,
  `push_title` varchar(141) NOT NULL,
  `message_title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `FK_APP_CODE` (`app_code`),
  CONSTRAINT `FK_APP_CODE` FOREIGN KEY (`app_code`) REFERENCES `xmlconnect_application` (`code`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`xmlconnect_notification_template`
--

/*!40000 ALTER TABLE `xmlconnect_notification_template` DISABLE KEYS */;
LOCK TABLES `xmlconnect_notification_template` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `xmlconnect_notification_template` ENABLE KEYS */;


--
-- Definition of table `supernarede`.`xmlconnect_queue`
--

DROP TABLE IF EXISTS `supernarede`.`xmlconnect_queue`;
CREATE TABLE  `supernarede`.`xmlconnect_queue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `exec_time` timestamp NULL DEFAULT NULL,
  `template_id` int(11) NOT NULL,
  `push_title` varchar(140) NOT NULL,
  `message_title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `type` varchar(12) NOT NULL,
  `app_code` varchar(32) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_TEMPLATE_ID` (`template_id`),
  CONSTRAINT `FK_TEMPLATE_ID` FOREIGN KEY (`template_id`) REFERENCES `xmlconnect_notification_template` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supernarede`.`xmlconnect_queue`
--

/*!40000 ALTER TABLE `xmlconnect_queue` DISABLE KEYS */;
LOCK TABLES `xmlconnect_queue` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `xmlconnect_queue` ENABLE KEYS */;




/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
