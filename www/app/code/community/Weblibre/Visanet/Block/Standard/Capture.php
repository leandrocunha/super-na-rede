<?php

class Weblibre_Visanet_Block_Standard_Capture extends Mage_Core_Block_Abstract
{
    protected function _construct()
    {
        parent::_construct();
    }

    protected function _toHtml()
    {
        /*$xml  = '<?xml version="1.0" encoding="UTF-8"?>';
        $xml .= '<ars>' . $_REQUEST['ars'] . '</ars>';
        $xml .= '<tid>' . $_REQUEST['tid'] . '</tid>';
        $xml .= '<lr>'  . $_REQUEST['lr'] . '</lr>';
        $xml .= '<cap>' . $_REQUEST['cap'] . '</cap>';
        $xml .= '<free>' . $_REQUEST['free'] . '</free>';

        return $xml;*/
		$html  = '<html>';
		$html .= '<head>';
		$html .= '<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />';

		if (!array_key_exists('lr', $_REQUEST)) { //significa que e um pedido de captura
			$html .= $this->getScriptCode();
			$html .= '</head>';
			$html .= '<body>';
	        $html .= '<p align="center">' . $this->__('Você será redirecionado para o Visanet em alguns instantes.') . '</p>';
	        $html .= '<p align="center">' . $this->getFormCode() . '</p>';
	        $html .= '<script type="text/javascript">document.getElementById("visanet_standard_checkout").submit();</script>';
		}
		else { //significa que e o retorno da captura
			$standard = Mage::getSingleton('visanet/standard');

			$orderId = $_REQUEST['free'];

			$html = "Mensagem da captura: <b>" . $_REQUEST['ars'] . "</b><br /><br />";

			$html .= "Código da transação: " . $_REQUEST['tid'] . "<br />";
			$html .= "Descritivo da capturado: " . $_REQUEST['cap'] . "<br />";
			$html .= "Código da resposta: " . $_REQUEST['lr'] . "<br />";
			
			//$html .= '<b>' . $standard->captura($orderId, $_REQUEST['tid'], $_REQUEST['lr'], $_REQUEST['ars'], $_REQUEST['cap']) . '</b><br /><br />';
			
			$html .= '<br /><button onclick="window.close()">Fechar</button>';
		}

		$html .= '</body></html>';

        return $html;
    }

	private function getFormCode() {
		$standard = Mage::getModel('visanet/standard');

		//$form = '<form id = "visanet_standard_checkout" name="visanet_standard_checkout" action="' . $standard->getVisanetUrl() . '" method="post" target="mpg_popup" onSubmit="javascript:fabrewin()">';
		$form = '<form id = "visanet_standard_checkout" name="visanet_standard_checkout" action="' . $standard->getVisanetUrl() . '" method="post">';

		foreach ($standard->getStandardCaptureFormFields($_REQUEST['order_id'] ) as $field=>$value) {
				$form .= '<input type="hidden" name="' . $field . '" value="' . $value . '" />';
		}
		$form .= '</form>';

		return($form);
	}

	private function getScriptCode() {
		$script = '<script language="JavaScript" type="text/javascript">';
		$script .= 'var retorno;';
		$script .= 'var mpg_popup;';
	
		$script .= 'window.name="loja";';
		$script .= 'function fabrewin() {';
		$script .= '	if(navigator.appName.indexOf("Netscape") != -1) {';
		$script .= '		mpg_popup = window.open("", "mpg_popup","toolbar=0,location=0,directories=0,status=1,menubar=0,scrollbars=1,resizable=0,screenX=0,screenY=0,left=0,top=0,width=765,height=440");';
		$script .= '	}';
		$script .= '	else {';
		$script .= '		mpg_popup = window.open("", "mpg_popup","toolbar=0,location=0,directories=0,status=1,menubar=0,scrollbars=1,resizable=1,screenX=0,screenY=0,left=0,top=0,width=765,height=440");';
		$script .= '	}';
		$script .= '	window.location = "' . Mage::getUrl("Visanet/standard/success") . '";';
		$script .= '	return true;';
		$script .= '}';
		$script .= '</script>';

		return($script);
	}
}