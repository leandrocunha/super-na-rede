<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category   Mage
 * @package    Visanet
 * @copyright  Copyright (c) 2008 Irubin Consulting Inc. DBA Varien (http://www.varien.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


class Weblibre_Visanet_Block_Standard_Autoriza extends Mage_Payment_Block_Form
{
    public function getModel() {
		return Mage::getSingleton('visanet/standard');
    }

    protected function _construct()
    {
        parent::_construct();
    }

    protected function _prepareLayout()
    {
		parent::_construct();
    }

    protected function _toHtml() {
        $standard = Mage::getModel('visanet/standard');
        $urlGo = $standard->getUrlFailure();

        $objResposta = $standard->getConsultaTransacao();

        //print_r($objResposta);
        //exit;

		$html  = '<html>';
		$html .= '<head>';
		$html .= '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
		$html .= '</head>';
		$html .= '<body>';

        $html .= $this->getScriptRetorno();

		$autorizacao = $objResposta->autorizacao;

        if (is_object($autorizacao)) {
            $mensagem = $autorizacao->mensagem;
            //$html .= "<br /><br />Mensagem da transa&ccedil;&atilde;o: <b>" . utf8_encode($mensagem) . "</b><br /><br />";

            if (!empty($autorizacao->lr) && $autorizacao->lr == "00") {
                $urlGo = $standard->getUrlSuccess();
        	}
        }

        $html .= '<script language="javascript">redirect("' . $urlGo . '", 10)</script>';
        $html .= '</body></html>';
        return $html;
    }

    protected function getScriptRetorno() {
		$htmlString = '<script language="Javascript">';
		$htmlString .= 'function redirect(page, time) {';
		$htmlString .= '	window.setTimeout("window.location.href = \'" + page + "\'", time);';
		$htmlString .= '}';
		$htmlString .= '</script>';

		return($htmlString);
	}
}
