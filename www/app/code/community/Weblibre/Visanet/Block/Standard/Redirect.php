<?php

class Weblibre_Visanet_Block_Standard_Redirect extends Mage_Core_Block_Template
{
    /*protected function _construct()
    {
        $this->setTemplate('visanet/standard/redirect.phtml');
        parent::_construct();
    }*/

    /*protected function _toHtml()
    {
        $html  = '<html>';
		$html .= '<head>';
		$html .= '<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />';
		$html .= $this->getScriptCode();
		$html .= '</head>';
		$html .= '<body>';
        $html .= '<p align="center">' . $this->__('Voc&ecirc; ser&aacute; redirecionado para a Visanet em alguns instantes. Se isso n&atilde;o acontecer, clique no bot&atilde;o abaixo.') . '</p>';
        $html .= '<p align="center">' . $this->getFormCode() . '</p>';

        $html .= '</body></html>';

        return $html;
    }*/

	private function getFormCode() {
		$standard = Mage::getModel('visanet/standard');
		$formFields = $standard->getStandardCheckoutFormFields();
		
		$total = $formFields['damount'];			
		$total = number_format($total, 2, '.', '');

		if (!Mage::getModel('wlpayment/wlpayment')->saveReg($formFields['orderid'], $formFields['tid'], $formFields['parcelas'], $total, $formFields['bandeira'])) {
			echo 'Falha ao gravar transa&ccedil;$atilde;o.';
		}
		
		$urlAction = $standard->getVisanetUrl();
		//$urlAction = 'http://librepag.weblibre.com.br/visanet/visanet.php';
		//$urlAction = 'http://192.168.0.100/librepag/visanet/visanet.php';
		
		$popup = $standard->getConfigData('popup');
		//$popup = true;
		if ($popup) { // vers�o popup
			$form = '<form id = "visanet_standard_checkout" name="visanet_standard_checkout" action="' . $urlAction . '" method="post" target="mpg_popup" onSubmit="javascript:fabrewin()">';
		}
		else { //vers�o sem popup
			$form = '<form id = "visanet_standard_checkout" name="visanet_standard_checkout" action="' . $urlAction . '" method="post">';
		}

		foreach ($formFields as $field=>$value) {
				$form .= '<input type="hidden" name="' . $field . '" value="' . $value . '" />';
		}

		$form .= '<p align="center">';
		$form .= '<input type="image" src="' . $standard->getSkinUrl('images/visa.jpg') . '" /> . <br />';
		$form .= '<input type="submit" value="Clique aqui para entrar na p&aacute;gina da Visa" />';
		$form .= '</p>';
		$form .= '</form>';
		
		//automaticamente, faz o post ao form via javascript
		//a Visanet n�o est� mais homologando essa pr�tica. 
		//Al�m disso, caso o IE bloqueie o Popup, se o usu�rio o desbloquear, os dados n�o s�o enviados corretamente.
		/*if (!$popup) {
			$form .= '<script type="text/javascript">document.getElementById("visanet_standard_checkout").submit();</script>';
		}*/

		return($form);
	}

	private function getScriptCode() {
		$script = '<script language="JavaScript" type="text/javascript">';
		$script .= 'var retorno;';
		$script .= 'var mpg_popup;';
	
		$script .= 'window.name="loja";';
		$script .= 'function fabrewin() {';
		$script .= '	if(navigator.appName.indexOf("Netscape") != -1) {';
		$script .= '		mpg_popup = window.open("", "mpg_popup","toolbar=0,location=0,directories=0,status=1,menubar=0,scrollbars=1,resizable=0,screenX=0,screenY=0,left=0,top=0,width=765,height=440");';
		$script .= '	}';
		$script .= '	else {';
		$script .= '		mpg_popup = window.open("", "mpg_popup","toolbar=0,location=0,directories=0,status=1,menubar=0,scrollbars=1,resizable=1,screenX=0,screenY=0,left=0,top=0,width=765,height=440");';
		$script .= '	}';
		//$script .= '	window.location="checkout/onepage/success";';
		$script .= '	return true;';
		$script .= '}';
		$script .= '</script>';

		return($script);
	}
}
