<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category   Mage
 * @package    Mage_Paypal
 * @copyright  Copyright (c) 2008 Irubin Consulting Inc. DBA Varien (http://www.varien.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


class Weblibre_Visanet_Block_Standard_Info extends Mage_Payment_Block_Info
{
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('visanet/standard/info.phtml');
    }

    public function toPdf()
    {
        $this->setTemplate('visanet/standard/pdf/info.phtml');
        return $this->toHtml();
    }

    /**
     * Retrieve current order model instance
     *
     * @return Mage_Sales_Model_Order
     */
    public function getOrder()
    {
        $order = Mage::registry('current_order');
		
		if (is_null($order) || $order->getId() == 0) {
			if ($this->getInfo() instanceof Mage_Sales_Model_Order_Payment) {
				$order = $this->getInfo()->getOrder();
			}
			else {
				$order = Mage::getSingleton('sales/order')->load(Mage::getSingleton('checkout/session')->getOrderId());
			}
		}
		
		return($order);
    }

    /**
     * Retrieve current order model instance
     *
     * @return Mage_Sales_Model_Order
     */
    public function getQuote()
    {
        return ($this->getStandard()->getQuote($this->getOrder()->getQuoteId()));
    }
	
	public function getStandard()
    {
		return Mage::getModel('visanet/standard');
    }

	public function getAntiFraude()
    {
		return Mage::getModel('wlpayment/antifraude');
    }

	public function getParcelas()
    {
		$quote = $this->getQuote();
		return($quote->getPayment()->getData('cc_parcelas'));
	}

	public function getCcTypeName()
    {
        $ret = '';
		$ccType = $this->getInfo()->getCcType();

		if ($this->getInfo()->getCcType() == 'VI') {
			$ret = 'Visa';
		}
		else if ($this->getInfo()->getCcType() == 'VE') {
			$ret = 'Visa Electron';
		}
		else if ($this->getInfo()->getCcType() == 'MA') {
			$ret = 'Mastercard';
		}
		else if (!empty($ccType)) {
			$ret = $ccType;
		}
		else {
			$ret = 'Visa';
		}
		
		return ($ret);
    }
	
	public function getTransaction($orderId) {
		$dataModel = Mage::getModel('wlpayment/wlpayment');
		
		return($dataModel->getReg($orderId));
	}
}