<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category   Mage
 * @package    Visanet
 * @copyright  Copyright (c) 2010 Weblibre (http://www.weblibre.com.br) - Godutra (godutra@gmail.com)
 */

 /*
 URL de retorno de exemplo para pedido aprovado: 
 http://192.168.0.100/magento/Visanet/standard/recibo?tid=08243004114112941001&orderid=100000174&free=http://192.168.0.100/magento/Visanet/standard/autoriza/
 */

class Weblibre_Visanet_Block_Standard_Recibo extends Mage_Payment_Block_Form
{
    protected function _construct()
    {
        parent::_construct();
    }

    protected function _prepareLayout()
    {
		parent::_construct();
    }

    protected function _toHtml(){
        $standard = Mage::getModel('visanet/standard');
        $urlGo = $standard->getUrlAutoriza();
        
		$html  = '<html>';
		$html .= '<head>';
		$html .= '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
		$html .= '</head>';
		$html .= '<body>';
        $html .= $this->getScriptRetorno();
        $html .= '<script language="javascript">redirect("' . $urlGo . '", 10)</script>';
        $html .= '</body></html>';

        return $html;
    }

	protected function getScriptRetorno() {
		$htmlString = '<script language="Javascript">';
		$htmlString .= 'function redirect(page, time) {';
		$htmlString .= '	window.setTimeout("window.parent.location.href = \'" + page + "\'", time);';
		$htmlString .= '}';
		$htmlString .= '</script>';
		
		return($htmlString);
	}

	private function getHostName($url) {
		// get host name from URL
		preg_match('@^(?:http(s*)://)?([^/]+)@i', $url, $matches);
		
		return($matches[2]);
	}

	private function getURLPath($url) {
		preg_match('@^(?:http(s*)://)?([^/]+)(.*)@i', $url, $matches);

		return($matches[3]);
	}
}