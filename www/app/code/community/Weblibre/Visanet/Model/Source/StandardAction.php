<?php
/**
 * Magento PagSeguro Payment Modulo
 *
 * @category   Mage
 * @package    Mage_Pagseguro
 * @copyright  Author Guilherme Dutra (godutra@gmail.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 *
 * PagSeguro Payment Action Dropdown source
 *
 */
class Weblibre_Visanet_Model_Source_StandardAction
{
    public function toOptionArray()
    {
        return array(
            array('value' => Weblibre_Visanet_Model_Standard::PAYMENT_TYPE_AUTH, 'label' => Mage::helper('Visanet')->__('Authorization')),
            array('value' => Weblibre_Visanet_Model_Standard::PAYMENT_TYPE_SALE, 'label' => Mage::helper('Visanet')->__('Sale')),
        );
    }
}