<?php
/**
 * Magento PagSeguro Payment Modulo
 *
 * @category   Shipping
 * @package    Pagseguro
 * @copyright  Author Guilherme Dutra (godutra@gmail.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 *
 * PagSeguro Payment Action Dropdown source
 *
 */

class Weblibre_Visanet_Model_Source_Autorizacao
{
	// o endereço abaixo não funcionou no Browser de um dos clientes 
	// array('value' => 'https://pirgos.kinghost.net/~weblibre/visanet', 'label' => 'Weblibre'),
	
    public function toOptionArray()
    {
        return array(
            array('value' => '0', 'label' => '0 - Não autorizar transação'),
            array('value' => '1', 'label' => '1 - Autorizar transação somente se autenticada'),
            array('value' => '2', 'label' => '2 - Autorizar autenticada e não autenticada'),
            array('value' => '3', 'label' => '3 - Autorizar direto'),
        );
    }
}
