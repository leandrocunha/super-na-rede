<?php
/**
 * Magento PagSeguro Payment Modulo
 *
 * @category   Shipping
 * @package    Pagseguro
 * @copyright  Author Guilherme Dutra (godutra@gmail.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 *
 * PagSeguro Payment Action Dropdown source
 *
 */

class Weblibre_Visanet_Model_Source_Gateways
{
	// o endereço abaixo não funcionou no Browser de um dos clientes 
	// array('value' => 'https://pirgos.kinghost.net/~weblibre/visanet', 'label' => 'Weblibre'),
	
    public function toOptionArray()
    {
        return array(
            array('value' => 'https://comercio.locaweb.com.br/comercio.comp', 'label' => 'Locaweb'),
        );
    }
}
