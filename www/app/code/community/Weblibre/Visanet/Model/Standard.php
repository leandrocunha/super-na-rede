<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category   Payment Gateway
 * @package    Visanet
 * @copyright  Copyright (c) 2008 WebLibre (http://www.weblibre.com.br) - Guilherme Dutra (godutra@gmail.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 *
 * Visanet Checkout Module
 *
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Weblibre_Visanet_Model_Standard extends Mage_Payment_Model_Method_Abstract
{
    //changing the payment to different from cc payment type and visanet payment type
    const PAYMENT_TYPE_AUTH = 'AUTHORIZATION';
    const PAYMENT_TYPE_SALE = 'SALE';

	protected $_modulo = 'VISAVBV';
    protected $_code  = 'visanet_standard';
    protected $_formBlockType = 'visanet/standard_form';
    protected $_infoBlockType = 'visanet/standard_info';
    protected $_allowCurrencyCode = array('BRL');
    
	/**
     * Availability options
     */
    protected $_isGateway               = true;
    protected $_canAuthorize            = true;
    protected $_canCapture              = true;
    protected $_canCapturePartial       = false;
    protected $_canRefund               = false;
    protected $_canVoid                 = true;
    protected $_canUseInternal          = true;
    protected $_canUseCheckout          = true;
    protected $_canUseForMultishipping  = true;
	protected $_canCancelInvoice        = true;
    protected $_canSaveCc               = true;
    
    /**
     * Get Paypal API Model
     *
     * @return Mage_Paypal_Model_Api_Nvp
     */
    public function getApi()
    {
        return Mage::getSingleton('visanet/api_nvp');
    }

	/**
     * Get visanet session namespace
     *
     * @return Visanet_Model_Session
     */
    public function getSession()
    {
        return Mage::getSingleton('visanet/session');
    }

    /**
     * Get checkout session namespace
     *
     * @return Mage_Checkout_Model_Session
     */
    public function getCheckout()
    {
        return Mage::getSingleton('checkout/session');
    }

    /**
     * Get current quote
     *
     * @return Mage_Sales_Model_Quote
     */
    public function getQuote($quote_id = null) {
		if (!empty($quote_id)) {
			return Mage::getModel('sales/quote')->load($quote_id);
		}
		else {
			return $this->getCheckout()->getQuote();
		}
    }

    /**
     * Using internal pages for input payment data
     *
     * @return bool
     */
    public function canUseInternal()
    {
        return $this->_canUseInternal;
    }

    /**
     * Using for multiple shipping address
     *
     * @return bool
     */
    public function canUseForMultishipping()
    {
        return true;
    }

    public function createFormBlock($name)
    {
        $block = $this->getLayout()->createBlock('visanet/standard_form', $name)
            ->setMethod('visanet_standard')
            ->setPayment($this->getPayment())
            ->setTemplate('visanet/standard/form.phtml');

        return $block;
    }

    /*validate the currency code is avaialable to use for visanet or not*/
    public function validate()
    {
        parent::validate();
        $currency_code = $this->getQuote()->getBaseCurrencyCode();
        if (!empty($currency_code) && !in_array($currency_code,$this->_allowCurrencyCode)) {
            Mage::throwException(Mage::helper('payment')->__('Selected currency code ('.$currency_code.') is not compatabile with Visanet'));
        }
        return $this;
    }

    public function onOrderValidate(Mage_Sales_Model_Order_Payment $payment)
    {
       return $this;
    }

    public function onInvoiceCreate(Mage_Sales_Model_Invoice_Payment $payment)
    {

    }

    public function canCapture()
    {
        return $this->_canCapture;
    }

    public function getSkinUrl($file=null, array $params=array())
    {
        return Mage::getDesign()->getSkinUrl($file, $params);
    }

    public function getOrderPlaceRedirectUrl()
    {
		  $secure = true;
		  if (strpos($this->getVisanetUrl(), 'https://') === false) {
			$secure = false;
		  }		  

		  return Mage::getUrl('visanet/standard/redirect', array('_secure' => $secure));
    }

	public function getUrlRetorno() {
		$secure = true;
		if (strpos($this->getVisanetUrl(), 'locaweb') !== false) { //Gateway Locaweb
			$secure = false; //na Locaweb o retorno nao pode ser https
		}
		
		return Mage::getUrl('visanet/standard/autoriza', array('_secure' => $secure));
		//return Mage::getBaseUrl() . 'Visanet/standard/autoriza';
	}

    public function getUrlRedirectAguarde()
    {
		return Mage::getUrl('visanet/standard/redirectaguarde', array('_secure' => true));
    }

    public function getUrlAguarde()
    {
		$url = Mage::getUrl('visanet/standard/aguarde', array('_secure' => true));
		
		if (strpos($url, 'https://') === false) { //sempre deve ser uma URL segura pois será exibida dentro de um IFrame da Visanet
			$url = "https://cbp.3dsolution.com.br/cbp/aguardeEC.html";
		}
		
		return($url);
    }

	public function getUrlSuccess() {
		return Mage::getUrl('visanet/standard/success', array('_secure' => true));
		//return Mage::getBaseUrl() . 'visanet/standard/success';
	}

	public function getUrlFailure() {
		return Mage::getUrl('checkout/onepage/failure', array('_secure' => true));
		//return Mage::getBaseUrl() . 'checkout/onepage/failure';
	}

    public function getUrlRecibo() {
		return Mage::getUrl('visanet/standard/recibo', array('_secure' => true));
		//return Mage::getBaseUrl() . 'checkout/onepage/failure';
	}

    public function getUrlAutoriza() {
		return Mage::getUrl('visanet/standard/autoriza', array('_secure' => true));
	}

    public function getUrlCapture($orderId = '') {
		$vars = '';
		  
		/*if (!empty($tid)) {
			  $vars  = '?identificacao=' . $this->getConfigData('codigo_gateway');
			  $vars .= '&ambiente='	. $this->getConfigData('ambiente');
			  $vars .= '&modulo=' . 'VISAVBV';
			  $vars .= '&operacao=' . 'Captura';
			  $vars .= '&tid=' . $tid;
		}*/
		
		if (!empty($orderId)) {
			$vars = '?order_id=' . $orderId;
		}

		return Mage::getBaseUrl() . 'Visanet/standard/capture' . $vars;
	}
	
	public function getUrlConsulta($orderId = '') {
		$vars = '';
		  
		/*if (!empty($tid)) {
			  $vars  = '?identificacao=' . $this->getConfigData('codigo_gateway');
			  $vars .= '&ambiente='	. $this->getConfigData('ambiente');
			  $vars .= '&modulo=' . 'VISAVBV';
			  $vars .= '&operacao=' . 'Captura';
			  $vars .= '&tid=' . $tid;
		}*/

		if (!empty($orderId)) {
			$vars = '?order_id=' . $orderId;
		}

		return Mage::getBaseUrl() . 'Visanet/standard/consulta' . $vars;
	}

	public function getOrder($order_id = null) {
		if (empty($order_id)) {
			$order = Mage::registry('current_order');
		}
		else {
			$order = Mage::getModel('sales/order')->load($order_id);
		}
		
		if (empty($order)) {
			$order_id = Mage::getSingleton('checkout/session')->getLastOrderId();
			$order = Mage::getModel('sales/order')->load($order_id);
		}

		return($order);
	}

	public function getOrderByIncrementId($realOrderId) {
		$order = Mage::getModel('sales/order')->loadByIncrementId($realOrderId);

		return($order);
	}
	
	function getNumEndereco($endereco) {
    	$numEndereco = '';

    	//procura por vírgula ou traço para achar o final do logradouro
    	$posSeparador = $this->getPosSeparador($endereco, false);  
    	if ($posSeparador !== false) {
	    $numEndereco = trim(substr($endereco, $posSeparador + 1));
	}

    	//procura por vírgula, traço ou espaço para achar o final do número da residência
      	$posComplemento = $this->getPosSeparador($numEndereco, true);
	if ($posComplemento !== false) {
	    $numEndereco = trim(substr($numEndereco, 0, $posComplemento));
	}

	if ($numEndereco == '') {
	    $numEndereco = '?';
	}
	
	return($numEndereco);
}

function getPosSeparador($endereco, $procuraEspaco = false) {
  	  $posSeparador = strpos($endereco, ',');
      	  if ($posSeparador === false) {
	      $posSeparador = strpos($endereco, '-');
	  }

	  if ($procuraEspaco) {	  
	      if ($posSeparador === false) {
	          $posSeparador = strrpos($endereco, ' ');
	      }
	  }

	  return($posSeparador);
}

	public function getCurrencySymbol($currencyCode) {
		$currencyCode = strtolower($currencyCode);
		
		if ($currencyCode == 'brl') {
			return('R$');
		}
		else if ($currencyCode == 'usd') {
			return('$');
		}
		else {
			return($currencyCode);
		}
	}

    public function getConsultaTransacao() {
        $objOrder = $this->getOrder();
        $wlPayment = Mage::getModel('wlpayment/wlpayment');

        $trans = $wlPayment->getReg($objOrder->getRealOrderId());

        // Monta a variável com os dados para postagem
        $request = 'identificacao=' . $this->getConfigData('codigo_gateway');
        $request .= '&modulo=CIELO';
        $request .= '&operacao=Consulta';
        $request .= '&ambiente=' . strtoupper($this->getConfigData('ambiente'));
        $request .= '&tid=' . $trans['transId'];

        //Faz a postagem para a Cielo
        try {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://comercio.locaweb.com.br/comercio.comp');
            //curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            //curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
            //curl_setopt($ch, CURLOPT_TIMEOUT, 40);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $response = curl_exec($ch);
            curl_close($ch);

            $objResposta = simplexml_load_string($response);

            if (isset($objResposta->autorizacao->codigo)){
                $codigoMensagemCielo = $objResposta->autorizacao->codigo;
                $MensagemCielo = $objResposta->autorizacao->mensagem;
            } else if (isset($objResposta->autenticacao->codigo)) {
                $codigoMensagemCielo = $objResposta->autenticacao->codigo;
                $MensagemCielo = $objResposta->autenticacao->mensagem;
            } else {
                $codigoMensagemCielo = $objResposta->codigo;
                $MensagemCielo = $objResposta->mensagem;
            }

            $wlPayment->saveReg($trans['id'], $trans['transId'], $trans['parcelas'], $trans['transValor'], $trans['tipoPagto'], $codigoMensagemCielo, $MensagemCielo);

            return $objResposta;
        } catch (Exception $e) {
            echo($e);
        }
    }
	
	public function getStandardCheckoutFormFields() {
		$objOrder = $this->getOrder();
		$quote = $this->getQuote($objOrder->getQuoteId());

		$a = $objOrder->getBillingAddress();
		//$b = $objOrder->getShippingAddress();

		$currency_code = $objOrder->getBaseCurrencyCode();
		$currencySymbol = $objOrder->getBaseCurrency();
		//$currencySymbol = $this->getCurrencySymbol($currency_code);
		//$language = Mage::app()->getLocale();
		$language = 'pt';

		//build $str_items
		$str_items = '';
		$items = $objOrder->getAllItems();
		if ($items) {
			$i = 1;
			foreach($items as $item) {
				//nao inclui itens-filho, apenas itens-pai. Por exemplo, os produtos configuraveis tem filhos
				if ($item->getParentItem()) {
					continue;
				}

				$str_items .= $item->getQty() . 'x ' . $item->getName() . ' - ';
			}
		}

		$parcelas = $objOrder->getPayment()->getCcParcelas();
		if ($parcelas == '') {
			$parcelas = $quote->getPayment()->getCcParcelas();
		}
		$numParcelas = intval(strip_tags($parcelas));
		if ($numParcelas == 0) {
			$numParcelas = 1;
		}

		//aplica zeros à esquerda
		$parcelas = sprintf("%03d", $numParcelas); 

		$total = $objOrder->getBaseGrandTotal();
		
		//o valor dos juros deve ser acrescentado na fatura (app\code\local\Mage\Sales_old\Model\Order\Address\Total\Tax.php).
		//Assim não se pode acrescentar os juros novamente
		//corrige o valor total de acordo com as parcelas e taxas de juros
		/*
		$taxaJuros = $this->getTaxaJuros();
		if ($numParcelas > $this->getParcelasSemJuros() && $taxaJuros > 0) {
			$valorParcela = $this->getValorParcela($total, $numParcelas, $taxaJuros);
			$total = $valorParcela * $numParcelas;
		}*/
/*
        $bin_cartao = $objOrder->getPayment()->getData('cc_number');
        if ($$bin_cartao == '') {
			$$bin_cartao = $quote->getPayment()->getData('cc_number');
		}
*/
		$tipoCartao = $objOrder->getPayment()->getData('cc_type');
		if ($tipoCartao == '') {
			$tipoCartao = $quote->getPayment()->getData('cc_type');
		}

        if ($tipoCartao == 'MA'){
            $bandeira = 'mastercard';
        } else if ($tipoCartao == 'VI'){
            $bandeira = 'visa';
        } else if ($tipoCartao == 'VE'){
            $bandeira = 'visa';
        }else if($tipoCartao == 'DC'){
            $bandeira = 'diners';
        }

		$codigoProduto = '1';
		/*if ($tipoCartao == 'MA') {
			$codigoProduto = '1'; //MasterCard Crédito a Vista
            $bandeira = 'mastercard';
		}
		else {
			$codigoProduto = '1'; //VISA Crédito a Vista
            $bandeira = 'visa';
		}

		if ($tipoCartao == 'VE') {
			$codigoProduto = 'A';
		}
		else if ($numParcelas > 1) {
			if ($tipoCartao == 'MA') {
				$codigoProduto = '2'; //MasterCard Parcelado Loja
			}
		}*/

        if ($numParcelas > 1){
            $codigoProduto = $this->getConfigData('parcelado');
        } else if ($tipoCartao == 'VE') {
			$codigoProduto = 'A';
		} else {
            $codigoProduto = 1;
        }

        if ($this->getConfigData('capturar') == 1){
            $capturar = "true";
        } else {
            $capturar = "false";
        }

		// Código de pagamento, formado pelo tipo de pagamento + número de parcelas
		$codpagto = $codigoProduto . $parcelas;

		$cep = substr(preg_replace ("[^0-9]", "", $a->getPostcode()).'00000000',0,8);

		$order = $a->getFirstname() . ' ' . $a->getLastname() . ', ' . $a->getEmail() . ', ' . $a->getTelephone() . ', ';
		$order .= $a->getStreet(1) . ', ' . $a->getStreet(2) . ', ' . $a->getCity() . ', ' . $a->getState() . ', ' . $a->getCountry() . ', ' . $cep;
		$authenttype = '0'; //nunca autoriza compras se a autenticação tenha falhado
		$price = number_format($total, 2, '', '');

		//$tid = $this->gerarTid($this->getConfigData('codigo_visanet'), $codpagto);
		$orderId = $objOrder->getId();
		$realOrderId = $objOrder->getRealOrderId();

        // Monta a variável com os dados para postagem
        $request = 'identificacao='.$this->getConfigData('codigo_gateway');
        $request .= '&modulo=CIELO';
        $request .= '&operacao=Registro';
        $request .= '&ambiente='.strtoupper($this->getConfigData('ambiente'));
        //$request .= '&bin_cartao='.substr($bin_cartao, 0, 6);
        $request .= '&idioma=PT';
        $request .= '&valor=' . str_replace('.', '', number_format($total, 2, '.', ''));
        $request .= '&pedido=' . $realOrderId;
        $request .= '&descricao=teste';
        $request .= '&bandeira=' . $bandeira;
        $request .= '&forma_pagamento=' . $codigoProduto;
        $request .= '&parcelas=' . $numParcelas;
        $request .= '&autorizar=' . ($bandeira == 'diners') ? '3' : $this->getConfigData('autorizar');
        $request .= '&capturar=' . $capturar;
        $request .= '&campo_livre=' . $realOrderId;

        try {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://comercio.locaweb.com.br/comercio.comp');
            //curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            //curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
            //curl_setopt($ch, CURLOPT_TIMEOUT, 40);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $response = curl_exec($ch);
            curl_close($ch);

            $retornoCielo = simplexml_load_string($response);

            $wlPayment = Mage::getModel('wlpayment/wlpayment');
            if (isset($retornoCielo->tid)){
                $wlPayment->saveReg($realOrderId, $retornoCielo->tid, $numParcelas, $total, $bandeira);
            } else {
                $wlPayment->saveReg($realOrderId, $retornoCielo->codigo, $numParcelas, $total, $bandeira, $retornoCielo->codigo, $retornoCielo->mensagem);
            }

            return simplexml_load_string($response);
        } catch (Exception $e) {
            echo($e);
        }

		$sReq = '';
		if ($this->getDebug() && $sReq) {
			$sReq = substr($sReq, 1);
		}
	}
	
	public function getStandardCaptureFormFields($orderId) {
		$trans = $this->getTransaction($orderId); 
		$tid = $trans['transId'];
		if ($tid == '') {
			$tid = $this->getOrder($orderId)->getPayment()->getCcTransId();
		}
		
		$sArr = array(
			'identificacao'    	=> $this->getConfigData('codigo_gateway'),
			'ambiente'			=> $this->getConfigData('ambiente'),
			'modulo'			=> 'VISAVBV',
			'operacao'			=> 'Captura',
			'tid'				=> $tid,
			'free'				=> $orderId
		);
		
		$sReq = '';
		$rArr = array();
		foreach ($sArr as $k=>$v) { /* replacing & char with and. otherwise it will break the post     */
			$value =  str_replace("&","and",$v);
			$rArr[$k] =  $value;
			$sReq .= '&'.$k.'='.$value;
		}

		return $rArr;
	}
	
	public function getStandardConsultaFormFields($realOrderId) {
		//$tid = $this->getOrder($orderId)->getPayment()->getCcTransId();

		$trans = $this->getTransaction($realOrderId); 
		$tid = $trans['transId'];
		$bin = substr($trans['numeroCartao'], 0, 6);
		
		$sArr = array(
			'identificacao'    	=> $this->getConfigData('codigo_gateway'),
			'ambiente'			=> $this->getConfigData('ambiente'),
			'modulo'			=> 'VISAVBV',
			'operacao'			=> 'Consulta',
			'tid'				=> $tid,
			'bin'				=> $bin,
			'free'				=> $realOrderId
		);

		$sReq = '';
		$rArr = array();
		foreach ($sArr as $k=>$v) { /* replacing & char with and. otherwise it will break the post     */
			$value =  str_replace("&","and",$v);
			$rArr[$k] =  $value;
			$sReq .= '&'.$k.'='.$value;
		}

		return $rArr;
	}

    //define a url do Visanet
    public function getVisanetUrl()
    {
		$url = $this->getConfigData('url_gateway');

       return $url;
    }

    //  define a url do Visanet
    public function getGatewayUrl()
    {
		$url = $this->getConfigData('url_gateway');

       return $url;
    }

	public function gerarTid ($shopid, $pagamento) {
		if(strlen($shopid) != 10) {
			echo "Tamanho do shopid deve ser 10 dígitos";
			exit;
		}
		
		if(is_numeric($shopid) != 1) {
			echo "Shopid deve ser numérico";
			exit;
		}

		if(strlen($pagamento) != 4) {
			echo "Tamanho do código de pagamento deve ser 4 dígitos.";
			exit;
		}

		//Número da Maquineta
		$shopid_formatado = substr($shopid, 4, 5);

		//Hora Minuto Segundo e Décimo de Segundo
		$hhmmssd = date("His", Mage::getModel('core/date')->timestamp(time()));
		$hhmmssd .= substr(sprintf("%0.1f",microtime()),-1);

		//Obter Data Juliana
		$datajuliana = sprintf("%03d",(date("z")+1));

		//Último dígito do ano
		$dig_ano = substr(date("y"), 1, 1);

		return $shopid_formatado.$dig_ano.$datajuliana.$hhmmssd.$pagamento;
	}

	public function getDebug()
	{
		$ret = Mage::getStoreConfig('payment/visanet/debug');
		if (!$ret) {
			$ret = $this->getConfigData('debug');
		}
		return $ret;
	}

	public function getLogPath()
	{
		$ret = Mage::getBaseDir() . '/var/log/visanet.log';
		
		if (!file_exists(Mage::getBaseDir() . '/var/log/')) {
			mkdir(Mage::getBaseDir() . '/var/log/');
		}
		
		return $ret;
	}

    public function captura($orderId, $tid, $responseCode, $message, $amount) {
		$ret = '';
		
		if($this->getDebug()) {
			$writer = new Zend_Log_Writer_Stream($this->getLogPath());
			$logger = new Zend_Log($writer);
			$logger->info("Visanet - entrando em captura()");
		}

		$textoCaptura = "Valor Capturado: $amount. Código da Transação: $tid. Código de Resposta da Transação: $responseCode. Mensagem: $message.";

        $order = $this->getOrder($orderId);

		if($this->getDebug()) {
			$logger->info('order: ' . $order->getData());
		}

		if ($responseCode=='0' || $responseCode=='3') {
			if (!$order->canInvoice()) {
				//when order cannot create invoice, need to have some logic to take care
				$ret = 'Este pedido não pode gerar Fatura. Verifique se a fatura já foi criada.';

				$order->addStatusToHistory(
					$order->getStatus(),//continue setting current order status
					$ret . ' - ' . $textoCaptura
				);
				
				$order->save();
			} 
			else {
				$wlPayment = Mage::getModel('wlpayment/wlpayment');
				$wlPayment->geraFatura($order, $tid);
            }
        }
		else if ($responseCode=='1' || $responseCode=='2' || $responseCode=='4') {
			$ret = 'Falha ao capturar o pagamento. ';
			$order->addStatusToHistory(
				$order->getStatus(),//continue setting current order status
				$ret . ' - ' . $textoCaptura
			);
			$order->save();
		}
		
		return($ret);
    }
	
	/**
     * Send authorize request to gateway
     *
     * @param   Varien_Object $payment
     * @param   decimal $amount
     * @return  Mage_Paygate_Model_Authorizenet
     */
    public function autoriza($realOrderId, $tid, $responseCode, $authCode, $message, $amount) {
		$error = false;

        if ($this->getDebug()) {
        }

		$textoAutorizacao = "Valor: $amount. Código da Transação: $tid. Código de Resposta da Transação: $responseCode. Código de Autorização: $authCode. Mensagem: $message.";

		//$order = $this->getOrder($orderId);
		$order = $this->getOrderByIncrementId($realOrderId);
		$payment = $order->getPayment();

		/*if ($amount != $order->getGrandTotal()) {
			//when grand total does not equal, need to have some logic to take care
			$order->addStatusToHistory(
				$order->getStatus(),//continue setting current order status
				'Total do pedido não confere com o valor aprovado pela Visanet. ' . $textoAutorizacao
			);
		}*/
		if ($responseCode === '00' || $responseCode === '11') { //autorizou
			$msg = 'Pagamento autorizado pela Visanet. ' . $textoAutorizacao;
			$notify = false;
			
			$order->setState(
				Mage_Sales_Model_Order::STATE_PROCESSING,
				'processing', 
				$msg,
				$notify
			);
			
			//registra a mudança de status no histórico do pedido
			$order->addStatusToHistory($order->getStatus(), $msg, $notify);

		   if ($payment) {
			   //it needs to save transaction id
			   $payment->setTransactionId($tid);
			   $payment->setAnetTransType('AUTH_ONLY');
			   $payment->setAmount($amount);		

			   $payment->setStatus(self::STATUS_APPROVED)
						->setCcApproval($authCode)
						->setLastTransId($tid)
						->setCcTransId($tid)
						->setCcAvsStatus($message)
						->setCcCidStatus($responseCode);

				if ($this->getConfigData('fatura')) {
					$wlPayment = Mage::getModel('wlpayment/wlpayment');
					$wlPayment->geraFatura($order, $tid);
				}
			}			
		}
		else { //se a transação não foi aprovada
			$order->addStatusToHistory(
				$order->getStatus(), //continue setting current order status
				'Transação não aprovada. ' . $textoAutorizacao
			);
			
			$order->cancel();
		}
		
		if ($error !== false) {
			Mage::throwException($error);
		}
		else {
			//save order
			if ($order->getPayment()) {
				$order->save();	
			}
		}
			
		return $this;
	}

	/*
	public function capture(Varien_Object $payment, $amount)
    {
		if($this->getDebug()) {
			$writer = new Zend_Log_Writer_Stream($this->getLogPath());
			$logger = new Zend_Log($writer);
			$logger->info("Visanet - Redirecionando");
		}
		
		if ($payment->getCcTransId()) {
			$this->setRedirectUrl($this->getUrlCapture());
            return false;	
		}
		else {
			Mage::throwException('Código da transação não identificado');
		}
		return $this;
	}*/

    /*public function capture(Varien_Object $payment, $amount)
    {
		if($this->getDebug()) {
			$writer = new Zend_Log_Writer_Stream($this->getLogPath());
			$logger = new Zend_Log($writer);
			$logger->info("Visanet - entrando em capture()");
		}
	
		if ($payment->getCcTransId()) {
				$this->setAmount($amount)
				->setPayment($payment);

			$result = $this->_callCapture($payment);
			
			if($this->getDebug()) { $logger->info(var_export($result, TRUE)); }

			if($result === false)
			{
				$e = $this->getError();
				if (isset($e['message'])) {
					$message = Mage::helper('dps')->__('There has been an error processing your payment.') . $e['message'];
				} else {
					$message = Mage::helper('dps')->__('There has been an error processing your payment. Please try later or contact us for help.');
				}
				Mage::throwException($message);
			}
			else
			{
				if ($result['lr'] == '0' || $result['lr'] == 0) 
				{
					$payment->setStatus(self::STATUS_APPROVED)
						->setLastTransId($result['tid']);
				}
				else if ($result['lr'] == '1' || $result['lr'] == 1)
				{
					Mage::throwException('Captura negada: ' . $result['ars'] . '. LR: ' . $result['lr'] . '. Cap: ' . $result['cap']);
				}
				else if ($result['lr'] == '2' || $result['lr'] == 2)
				{
					Mage::throwException('Falha na captura. Informação inconsistente: ' . $result['ars'] . '. LR: ' . $result['lr'] . '. Cap: ' . $result['cap']);
				}
				else if ($result['lr'] == '3' || $result['lr'] == 3)
				{
					Mage::throwException('Captura já efetuada: ' . $result['ars'] . '. LR: ' . $result['lr'] . '. Cap: ' . $result['cap']);
				}
				else 
				{
					Mage::throwException('Falha na captura: ' . $result['ars'] . '. LR: ' . $result['lr'] . '. Cap: ' . $result['cap']);
				}
			}
		}
		else {
			Mage::throwException('Código da transação não identificado');
		}
		return $this;
    }*/

	/**
	 *
	 */
	protected function _callCapture(Varien_Object $payment)
	{
		if($this->getDebug())
		{
			$writer = new Zend_Log_Writer_Stream($this->getLogPath());
			$logger = new Zend_Log($writer);
			$logger->info('entering _call()');
			$logger->info('identificacao: ' . $this->getConfigData('codigo_gateway'));
			$logger->info('modulo: ' . $this->_modulo);
			$logger->info('ambiente: ' . $this->getConfigData('ambiente'));
			$logger->info('tid: ' . $payment->getCcTransId());
		}
		
		// Generate any needed values
		$nvpArr = array(
			'identificacao' => $this->getConfigData('codigo_gateway'),
            'operacao'  	=> 'Captura',
            'modulo'   		=> $this->_modulo,
            'ambiente'      => $this->getConfigData('ambiente'),
			'tid'			=> $payment->getCcTransId()
        );
		
		if($this->getDebug())
		{
			$logger->info(var_export($payment->getOrder()->getData(), TRUE));
		}
		
		$nvpReq = '';
        foreach ($nvpArr as $k=>$v) {
            $nvpReq .= '&'.$k.'='.urlencode($v);
        }
        $nvpReq = substr($nvpReq, 1);
		
		// DEBUG
		if($this->getDebug()) { $logger->info($nvpReq); }
		
		// Send the data via HTTP POST and get the response
		$http = new Varien_Http_Adapter_Curl();
		$http->setConfig(array('timeout' => 30));
		
		$http->write(Zend_Http_Client::POST, $this->getGatewayUrl(), '1.1', array(), $nvpReq);
		
		$response = $http->read();
		
		if ($http->getErrno()) {
			$http->close();
			$this->setError(array(
				'message' => $http->getError()
			));
			return false;
		}
		
		// DEBUG
		if($this->getDebug()) { 
			$logger->info($response); 
		}
        
        $http->close();

		// Strip out header tags
        $response = preg_split('/^\r?$/m', $response, 2);
        $response = trim($response[1]);
		
		// Parse the XML object
		$xmlObj = simplexml_load_string($response);

		// Build an associative array with returned values		
		$result = array();

		$xpath = $xmlObj->xpath('/ars');
		$result['ars'] = ($xpath !== FALSE) ? $xpath[0] : '';
		
		$xpath = $xmlObj->xpath('/tid');
		$result['tid'] = ($xpath !== FALSE) ? $xpath[0] : '';
				
		$xpath = $xmlObj->xpath('/lr');
		$result['lr'] = ($xpath !== FALSE) ? $xpath[0] : '';

		$xpath = $xmlObj->xpath('/cap');
		$result['cap'] = ($xpath !== FALSE) ? $xpath[0] : '';

		$xpath = $xmlObj->xpath('/free');
		$result['free'] = ($xpath !== FALSE) ? $xpath[0] : '';

		return $result;
	}
	
	/**
     * Get current quote
     *
     * @return Mage_Sales_Model_Quote
     */
    public function getGrandTotal()
    {
        $ret = $this->getQuote()->getGrandTotal();
		
		/*todo: testar
		$objeto = Mage::getSingleton('checkout/session');
		if (is_null($objeto)) {
			$objeto = Mage::getSingleton('adminhtml/session_quote');
		}*/

		return $ret;
    }

	public function getTransaction($realOrderId) {
		$dataModel = Mage::getModel('wlpayment/wlpayment');
		
		return($dataModel->getReg($realOrderId));
	}
	
    public function getNumParcelas()
    {
        $nParcelas = $this->getConfigData('parcelas');

        $total = $this->getGrandTotal();
		$valor_minimo = $this->getConfigData('valor_minimo');
		
		//verifica o valor mínimo permitido para cada parcela
		if (!empty($valor_minimo) && is_numeric($valor_minimo)) {
			$parcPossiveis = floor($total / $valor_minimo);
			
			if ($parcPossiveis < $nParcelas) {
				$nParcelas = $parcPossiveis;
			}
		}
		
		if (empty($nParcelas) || !is_numeric($nParcelas) || $nParcelas <= 0) {
			$nParcelas = 1;
		}

		return $nParcelas;
    }

	public function getParcelas() {
		if($this->getDebug()) {
			$writer = new Zend_Log_Writer_Stream($this->getLogPath());
			$logger = new Zend_Log($writer);
			$logger->info("Visanet - entrando em getParcelas()");
		}

		$parcelas = array();
		$valorParcela = 0;

		$taxa = $this->getConfigData('taxa_juros');
		if (empty($taxa)) {
			$taxa = 0;
		}
		else {
			$taxa = str_replace(',', '.', $taxa);
		}
		$taxa = (float)$taxa;

		$parcelasSemJuros = $this->getConfigData('sem_juros_ate');
		if (empty($parcelasSemJuros)) {
			$parcelasSemJuros = 1;
		}
		$parcelasSemJuros = (int)$parcelasSemJuros;

		for ($i=1; $i<=$this->getNumParcelas(); $i++) {
			$valorTotal = $this->getGrandTotal();

			$taxaParcela = 0;
			if ($i > $parcelasSemJuros) { //verifica se é uma parcela com juros
			   $taxaParcela = $taxa;
			}
			$valorParcela = $this->getValorParcela($valorTotal, $i, $taxaParcela);
			$valorParcela = sprintf('%01.2f', $valorParcela);
			$valorParcela = str_replace('.', ',', $valorParcela); 

			$parcelas[$i] = "<b>$i</b>x de R$ $valorParcela"; 
		}

		if($this->getDebug()) {
			$logger->info('Visanet - fim getParcelas() - return: ' . $parcelas);
		}

		return($parcelas);
	}
	
	function getValorParcela($valor_pagar, $parcelas, $taxa) {
		if($this->getDebug()) {
			$writer = new Zend_Log_Writer_Stream($this->getLogPath());
			$logger = new Zend_Log($writer);
			$logger->info("Visanet - entrando em getValorParcela()");
		}

		if(!is_numeric($valor_pagar) || $valor_pagar <= 0){
			return(false);
		}
		if((int)$parcelas != $parcelas){
			return(false);
		}
		if(!is_numeric($taxa) || $taxa < 0){
			return(false);
		}

		$taxa = $taxa / 100;
		
		$denominador = 0;
		if($parcelas > 1){
			for($i=1; $i<=$parcelas; $i++){
				$denominador += 1/pow(1+$taxa,$i);
			}
		}else{
			$denominador = 1;
		}

		if($this->getDebug()) {
			$logger->info('Visanet - fim getValorParcela() - return: ' . ($valor_pagar/$denominador));
		}

		$valorParcela = round($valor_pagar / $denominador, 2);
		return($valorParcela);
	}
	
	function getTaxaJuros() {
		$taxa = $this->getConfigData('taxa_juros');
		if (empty($taxa)) {
			$taxa = 0;
		}
		else {
			$taxa = str_replace(',', '.', $taxa);
		}
		$taxa = (float)$taxa;
		
		return($taxa);
	}
	
	function getParcelasSemJuros() {
		$parcelasSemJuros = $this->getConfigData('sem_juros_ate');
		if (empty($parcelasSemJuros)) {
			$parcelasSemJuros = 1;
		}
		$parcelasSemJuros = (int)$parcelasSemJuros;
		
		return($parcelasSemJuros);
	}
}