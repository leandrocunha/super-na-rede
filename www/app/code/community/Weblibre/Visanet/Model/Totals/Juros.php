<?php
class Weblibre_Visanet_Model_Totals_Juros extends Mage_Sales_Model_Quote_Address_Total_Abstract {
    public function __construct(){
        $this->setCode('visanet_juros');
    }

	public function collect(Mage_Sales_Model_Quote_Address $address) {
        $parcelas = 0;
		$parcelas = @$_POST['payment']['cc_parcelas'];
		$parcelas = (int)$parcelas;

		if ($parcelas > 1) {
			$standard = Mage::getSingleton('visanet/standard');
			
			$taxaJuros = $standard->getTaxaJuros();

            $total = $address->getGrandTotal();

            if ($taxaJuros != 0) {
                $valorParcela = $standard->getValorParcela($total, $parcelas, $taxaJuros);

                $valorJuros = ($valorParcela * $parcelas) - $total;

                $address->setData('juros', $valorJuros);

                $address->setGrandTotal($address->getGrandTotal() + $valorJuros);
                $address->setBaseGrandTotal($address->getBaseGrandTotal() + $valorJuros);
            }
		}
		
		return $this;
    }
    
    /*
    * This method adds the values to the totals
    */
    
    public function fetch(Mage_Sales_Model_Quote_Address $address) {
		$amount = $address->getData('juros');
        if ($amount !=0) {
            $address->addTotal(array(
                'code'=> $this->getCode(),
                'title'=> 'Juros do Parcelamento',
                'value'=> $amount,
            ));
        }
    }
}
?>