<?php
/**
 * Magento Visanet Payment Modulo
 *
 * @category   Payment Gateway
 * @package    Visanet
 * @copyright  Author Guilherme Dutra (godutra@gmail.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


class Weblibre_Visanet_Model_Mysql4_Setup extends Mage_Sales_Model_Mysql4_Setup
{
}
