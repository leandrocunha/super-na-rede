<?php
/**
 * Magento
 *
 * JN2
 */


/**
 * Media library data helper
 *
 * @category   Mage
 * @package    Mage_Media
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_Media_Helper_Data extends Mage_Core_Helper_Data
{

    /**
     * Redimensiona imagem
     * @param type $imagem url da imagem (com http)
     * @param type $larg largura
     * @param type $alt  altura
     */
    public function resizeImage($imagem,$larg=960,$alt=960){
        list($width, $height) = getimagesize($imagem); 
        if($width > $larg){
            $imagem_path = realpath(str_replace(Mage::getBaseUrl(), '', $imagem));//limpa o http pra pegar o caminho no diretorio da makina/ftp
            $imageObj = new Varien_Image($imagem_path);
            $imageObj->constrainOnly(TRUE);//garantir q a imagem não será maior que a esperada
            $imageObj->keepAspectRatio(TRUE);//manter proporção, sem distorcer
            $imageObj->keepFrame(FALSE);//garante q a imagem terá as dimensoes abaixo, portanto nao funciona caso o keepAspectRatio for false
            $imageObj->resize($larg, $alt);// W x H
            $imageObj->save($imagem_path);
        }
    }
}
