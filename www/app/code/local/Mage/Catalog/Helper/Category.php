<?php
/**
 *jn2
 * IMPLEMENTANDO O getListaCategorias
 */

/**
 * Catalog category helper
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_Catalog_Helper_Category extends Mage_Core_Helper_Abstract
{
    const XML_PATH_CATEGORY_URL_SUFFIX          = 'catalog/seo/category_url_suffix';
    const XML_PATH_USE_CATEGORY_CANONICAL_TAG   = 'catalog/seo/category_canonical_tag';
    /**
     * Ordenação alfabética
     */
    const ORDEM_ALFABETICA = '1';
    /**
     * Do jeito que foi inserido no banco
     */
    const ORDEM_INSERCAO = '0';
    /**
     * seguindo a posição que ficou no treeview do admin
     */
    const ORDEM_ADMIN = '2';


    /**
     * Store categories cache
     *
     * @var array
     */
    protected $_storeCategories = array();

    /**
     * Cache for category rewrite suffix
     *
     * @var array
     */
    protected $_categoryUrlSuffix = array();

    /**
     * Retrieve current store categories
     *
     * @param   boolean|string $sorted
     * @param   boolean $asCollection
     * @return  Varien_Data_Tree_Node_Collection|Mage_Catalog_Model_Resource_Eav_Mysql4_Category_Collection|array
     */
    public function getStoreCategories($sorted=false, $asCollection=false, $toLoad=true)
    {
        $parent     = Mage::app()->getStore()->getRootCategoryId();
        $cacheKey   = sprintf('%d-%d-%d-%d', $parent, $sorted, $asCollection, $toLoad);
        if (isset($this->_storeCategories[$cacheKey])) {
            return $this->_storeCategories[$cacheKey];
        }

        /**
         * Check if parent node of the store still exists
         */
        $category = Mage::getModel('catalog/category');
        /* @var $category Mage_Catalog_Model_Category */
        if (!$category->checkId($parent)) {
            if ($asCollection) {
                return new Varien_Data_Collection();
            }
            return array();
        }

        $recursionLevel  = max(0, (int) Mage::app()->getStore()->getConfig('catalog/navigation/max_depth'));
        $storeCategories = $category->getCategories($parent, $recursionLevel, $sorted, $asCollection, $toLoad);

        $this->_storeCategories[$cacheKey] = $storeCategories;
        return $storeCategories;
    }

    /**
     * Retrieve category url
     *
     * @param   Mage_Catalog_Model_Category $category
     * @return  string
     */
    public function getCategoryUrl($category)
    {
        if ($category instanceof Mage_Catalog_Model_Category) {
            return $category->getUrl();
        }
        return Mage::getModel('catalog/category')
            ->setData($category->getData())
            ->getUrl();
    }

    /**
     * Check if a category can be shown
     *
     * @param  Mage_Catalog_Model_Category|int $category
     * @return boolean
     */
    public function canShow($category)
    {
        if (is_int($category)) {
            $category = Mage::getModel('catalog/category')->load($category);
        }

        if (!$category->getId()) {
            return false;
        }

        if (!$category->getIsActive()) {
            return false;
        }
        if (!$category->isInRootCategoryList()) {
            return false;
        }

        return true;
    }

/**
     * Retrieve category rewrite sufix for store
     *
     * @param int $storeId
     * @return string
     */
    public function getCategoryUrlSuffix($storeId = null)
    {
        if (is_null($storeId)) {
            $storeId = Mage::app()->getStore()->getId();
        }

        if (!isset($this->_categoryUrlSuffix[$storeId])) {
            $this->_categoryUrlSuffix[$storeId] = Mage::getStoreConfig(self::XML_PATH_CATEGORY_URL_SUFFIX, $storeId);
        }
        return $this->_categoryUrlSuffix[$storeId];
    }

    /**
     * Retrieve clear url for category as parrent
     *
     * @param string $url
     * @param bool $slash
     * @param int $storeId
     *
     * @return string
     */
    public function getCategoryUrlPath($urlPath, $slash = false, $storeId = null)
    {
        if (!$this->getCategoryUrlSuffix($storeId)) {
            return $urlPath;
        }

        if ($slash) {
            $regexp     = '#('.preg_quote($this->getCategoryUrlSuffix($storeId), '#').')/$#i';
            $replace    = '/';
        }
        else {
            $regexp     = '#('.preg_quote($this->getCategoryUrlSuffix($storeId), '#').')$#i';
            $replace    = '';
        }

        return preg_replace($regexp, $replace, $urlPath);
    }

    /**
     * Check if <link rel="canonical"> can be used for category
     *
     * @param $store
     * @return bool
     */
    public function canUseCanonicalTag($store = null)
    {
        return Mage::getStoreConfig(self::XML_PATH_USE_CATEGORY_CANONICAL_TAG, $store);
    }
    
    /**
     * Atraves de uma categorias, busca as subcategorias (somente 1 nivel)
     * 
     * @param type $id_base id da categoria que sera a base para puxar as subcategorias
     * @param type $ordem 0 - insercao, 1 - alfabetica e 2 - seguindo o admin [padrao: Mage_Catalog_Helper_Category::ORDEM_INSERCAO]
     * @param type $como_colecao vai ser Eav_Mysql4_Collection ou Varien_Tree_Node_Collection  [padrao: true pra trabalhar mais proximo da tabela catalog_category_entity]
     * @return array contendo os objetos ($obj->getEntityId(), $obj->getUrl(), $obj->getName(), etc.]
     */
    public function getListaCategorias($id_base=0,$ordem='0',$como_colecao=true){
        $categ_collection = Mage::getModel('catalog/category')->getCategories($id_base, 1, ($ordem != '1') ? false : true, $como_colecao);//params: id, ordenacao e colecao
        $array_categ = array();
        foreach ($categ_collection as $category){
            $category = Mage::getModel('catalog/category')->load($category->getEntityId());
            if($ordem == '2'){
                $array_categ[$category->getPosition()] = $category;
                $flag_sort = true;
            }else{
                $array_categ[] = $category;
                $flag_sort = false;
            }
        }
        if($flag_sort){
            ksort($array_categ);
        }
        return $array_categ;
    }
    
    public function getCategoryLink($id,$label=null,$link=true){
        $categ = Mage::getModel('catalog/category')->load($id);
        ob_start();
        if($categ){
            ?> <a href="<?php echo ($link) ? $categ->getUrl() : '#'.$categ->getUrlKey();?>" class="categ-<?php echo $categ->getUrlKey()?>"><?php echo ($label) ? $label : $categ->getName()?></a><?php
        }
        return ob_get_clean();
    }

    public function getProdutoSubcategorias($produto,$id_categ_pai){
        $helper = $this;
        $children = $helper->getListaCategorias($id_categ_pai, Mage_Catalog_Helper_Category::ORDEM_ALFABETICA);
        $ids_categ = array();
        foreach($children as $child){
            $ids_categ[$child->getId()] = array('name'=>trim($child->getName()),'thumb'=>Mage::getBaseUrl('media').'catalog/category/'.$child->getThumbnail() );
        }
        $categ_existente = array();
        $array_categ_prod = $produto->getCategoryIds();
        foreach($array_categ_prod as $cada){
            if(isset($ids_categ[$cada])){
                $categ_existente[] = $ids_categ[$cada];
            }
        }
        return $categ_existente;
    }
}

