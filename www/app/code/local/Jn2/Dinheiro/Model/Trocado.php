<?php
/**
* Our test CC module adapter
*/
class Jn2_Dinheiro_Model_Trocado extends Mage_Payment_Model_Method_Abstract
{

 protected $_formBlockType = 'dinheiro/form';//vai puxar jn2_dinheiro_block_form
 protected $_infoBlockType = 'dinheiro/info';
  /**
  * unique internal payment method identifier
  *
  * @var string [a-z0-9_]
  */
  protected $_code = 'dinheiro';
  
  /**
   * Here you will need to implement authorize, capture and void public methods
   *
   * @see examples of transaction specific public methods such as
   * authorize, capture and void in Mage_Paygate_Model_Authorizenet
   */
  
 /** 
  * Assign data to info model instance 
  *  
  * @param   mixed $data 
  * @return  Mage_Payment_Model_Method_Checkmo 
  */  
 public function assignData($data)  
 {  
    if (!($data instanceof Varien_Object)) {
        $data = new Varien_Object($data);
    }
    $info = $this->getInfoInstance();
    //neste caso utilizou-se o campo Credit Card Owner como campo para guardar a quantia de dinheiro em mão com o cliente
    $info->setCcOwner($data->getCcOwner());/* setando todos os campos utilizados*/
    return $this;
 }  

    /**
     * Prepare info instance for save
     *
     * @return Mage_Payment_Model_Abstract
     */
    public function prepareSave()
    {
        $info = $this->getInfoInstance();;
        return $this;
    }
    
  public function validate()
    {
        return $this;
    }
   
}
?>