<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Jn2_Simplespgto
 * @copyright   Copyright (c) 2010 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


class Jn2_Simplespgto_Model_Method_Cheque extends Mage_Payment_Model_Method_Abstract
{

    protected $_code  = 'cheque';
    protected $_formBlockType = 'simplespgto/form_cheque';
    protected $_infoBlockType = 'simplespgto/info_cheque';

    /**
     * Assign data to info model instance
     *
     * @param   mixed $data
     * @return  Jn2_Simplespgto_Model_Method_Cheque
     */
    public function assignData($data)
    {
        $details = array();
        if ($this->getObs()) {
            $details['obs'] = $this->getObs();
        }
        if (!empty($details)) {
            $this->getInfoInstance()->setAdditionalData(serialize($details));
        }
        return $this;
    }

    public function getObs()
    {
        return $this->getConfigData('obs');
    }

}
