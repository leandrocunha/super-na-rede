<?php

/**
 * JN2 agencia.interativa - 2011
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL).
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   Frete programado
 * @package    Jn2_Programado
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Jn2_Programado_Model_Carrier_Programado extends Mage_Shipping_Model_Carrier_Abstract implements Mage_Shipping_Model_Carrier_Interface {

    protected $_code = 'programado';
    protected $_result = null;

 
    
    public function collectRates(Mage_Shipping_Model_Rate_Request $request) {
        
        if (!$this->getConfigFlag('active')) {
            //Desabilitado
            return false;
        }

        $result = Mage::getModel('shipping/rate_result');

        /*$error = Mage::getModel('shipping/rate_result_error');
        $error->setCarrier($this->_code);
        $error->setCarrierTitle($this->getConfigData('title'));*/
        
        //pegando o carrinho
        $itens = Mage::getModel('checkout/cart')->getQuote()->getAllVisibleItems();
        
        
        $carrinho = array();
        $base_preco_total = (float)$this->getConfigFlag('total_min');
        $preco_fixo = (float)$this->getConfigFlag('preco');
        $preco_total = 0;
        foreach($itens as $item){
            $_product = $item->getProduct();
            
            /*$array_produto = array();
            $array_produto['nome'] = $_product->getData('name');
            $array_produto['peso'] = $_product->getData('weight');
            $array_produto['altura'] = $_product->getData('volume_altura');
            $array_produto['largura'] = $_product->getData('volume_largura');
            $array_produto['comprimento'] = $_product->getData('volume_comprimento');
            $array_produto['qtde'] = $item->getQty();
            $array_produto['preco'] = $_product->getFinalPrice();//Mage::helper('core')->currency($_product->getFinalPrice())
            
            $carrinho[$_product->getId()] = $array_produto;*/
            $preco_total += (float)$_product->getFinalPrice();
        }
        
        $valor_t = ($preco_total > $base_preco_total) ? 0 : $preco_fixo;
        
        /* opções */
        
        $horarios = explode(';',Mage::getStoreConfig('carriers/'.$this->_code.'/horarios', $this->getStore()));/**/
        $i=0;
        foreach($horarios as $hr){
            $method = Mage::getModel('shipping/rate_result_method');
            $method->setCarrier($this->_code);
            $method->setMethod($this->_code.'_'.++$i);

            $method->setMethodTitle($hr);

            $method->setPrice($valor_t);

            $method->setCost(0);

            $result->append($method);
        }
        
        #################
        $result->sortRatesByPrice();

        $this->_result = $result;

        $this->_updateFreeMethodQuote($request);

        return $this->_result;
    }

    public function getAllowedMethods() {
        return array(
            $this->_code => $this->getConfigData('title')
            );
    }

}
