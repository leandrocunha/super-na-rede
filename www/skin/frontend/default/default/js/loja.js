function setZoomImg(url){
    $j('.product-image img').attr('src',url);
}
function atualizarQty(input,id,link){
    var qtde = 1;
    if(input.value > 1){
        qtde = input.value;
    }else{
        input.value = 1;
    }
    $j("#"+id+" .comprar .btComprar").attr('onclick',"setLocation('"+link+"qty/"+qtde+"')");
}
function obrigatoriedade(id,ativar){
    if(ativar == 1){
        $j('label[for='+id+']').addClass('required');
        $j("#"+id).addClass('required-entry');
        var label = $j('label[for='+id+']').html();
        if(!($j('label[for='+id+'] em').size() >= 1)){
            $j('label[for='+id+']').html('<em>*</em>'+label);
        }
    }else{
        $j('label[for='+id+']').removeClass('required');
        $j("#"+id).removeClass('required-entry');
        $j('label[for='+id+'] em').remove();
    }
}
function trocarParaPF(){
    $j("#titulo-info-pessoal").html('Informações Pessoais');
    obrigatoriedade('customer_pj_cnpj',0);
    $j('#customer_pj_cnpj').parent().hide();
    obrigatoriedade('customer_pj_ie',0);
    $j('#customer_pj_ie').parent().hide();
    obrigatoriedade('customer_pj_razao',0);
    $j('#customer_pj_razao').parent().hide();
    obrigatoriedade('customer_pf_rg',1);
    $j('#customer_pf_rg').parent().show();
}
function trocarParaPJ(){
    $j("#titulo-info-pessoal").html('Informações do Responsável');
    obrigatoriedade('customer_pj_cnpj',1);
    $j('#customer_pj_cnpj').parent().show();
    obrigatoriedade('customer_pj_ie',1);
    $j('#customer_pj_ie').parent().show();
    obrigatoriedade('customer_pj_razao',1);
    $j('#customer_pj_razao').parent().show();
    obrigatoriedade('customer_pf_rg',0);
    $j('#customer_pf_rg').parent().hide();
}
function pulaPara(event,elemento,proximo){
    if($(proximo) == null){
        proximo = "billing:"+proximo;
    }
    if(elemento.value.length >= 2){
        $(proximo).focus();
    }
}
// Numeric only control handler
jQuery.fn.soNumerico = function(){
    return this.each(function(){
        try{
        jQuery(this).keydown(function(e){
            var key = e.charCode || e.keyCode || 0;
            // allow backspace, tab, delete, arrows, numbers and keypad numbers ONLY
            return (
                key == 8 ||
                key == 9 ||
                key == 46 ||
                (key >= 37 && key <= 40) ||
                (key >= 48 && key <= 57) ||
                (key >= 96 && key <= 105));
        });
        }catch(e){}
    });
};

$j(document).ready(function(){
    //jquery spin-button
    $j('input.qty').spin({
        imageBasePath: BASE_URL+'skin/frontend/default/default/images/destaques/',
        timeInterval: 150,
        max: 99,
        /*lock: true, /*isso aki trava edicao, somente poderah clicar*/
        min: 1,
        btnClass: 'qty-incremento'
    });
    //retirando os departamentos de certas áreas
    $j('.my-account,.shopBuy,#checkoutSteps,.menuListasProntas').each(function(){
        $j('#listagem-depto').hide();
    });
    //colocando o base url nos links persolanizados
    $j('a').each(function(){
        var str = $j(this).attr('href');
        if(str.indexOf('/') === 0){
            $j(this).attr('href',BASE_URL+str.substring(1));
        }
    });
    try{
        $('input[name="billing[firstname]"]').keyup(function(){
            $('input[name="shipping[firstname]"]').val($(this).val());
        });
        $('input[name="billing[lastname]"]').keyup(function(){
            $('input[name="shipping[lastname]"]').val($(this).val());
        });
    }catch(e){ }
    
    $j('.input-telefone').mask("(99) 9999-9999",{placeholder:" "});
    
    //abrindo os deptos somente para as areas abaixo (serão as casses do body
    var areas = ['cms-index-index','contacts-index-index','cms-page-view'];

    
});